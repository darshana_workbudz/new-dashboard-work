@extends(isset($isajax) && $isajax==true ? 'layouts-frontend.ajax' : 'layouts-frontend.master')
@section('content')
<!DOCTYPE html>
<html lang="en">



<body class="g-sidenav-show  bg-gray-100">
    <div class="container pb-lg-9 pb-10 pt-10 postion-relative z-index-2">
      <div class="row">
        <div class="col-md-6 mx-auto text-center">
          <h4 class="text-primary">Simple, transparent pricing and options to fit you needs</h4>
          <p class="text-primary text-sm">No contracts, no surprise fees, Cancel anytime</p>
        </div>
      </div>
    </div>
  
  <div class="mt-n8">
    <div class="container">
      <div class="tab-content tab-space">
        <div class="tab-pane active" id="monthly">
          <div class="row">
            <div class="col-lg-4 mb-lg-0 mb-4">
              <div class="card" style="border: 1px solid #cbc7cf;border-radius: 6px;">
                <div class="card-header text-center pt-4 pb-3">
                  <h5 class="text-dark">Standard</h5>
                  <p class="text-sm">Most Popular</p>
                  <h1 class="font-weight-bold mt-2">
                    <p>$59 / monthly</p>
                  </h1>
                </div>
                <div class="row">

                  <div class="col-lg-12 col-md-12 col-12 mx-auto text-center">
                    <div class="nav-wrapper mt-5 position-relative z-index-2">
                      <ul class="nav nav-pills nav-fill flex-row p-1" id="tabs-pricing" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link mb-0 active" id="tabs-iconpricing-tab-1" data-bs-toggle="tab" href="#monthly" role="tab" aria-controls="monthly" aria-selected="true">
                            Monthly billing
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link mb-0" id="tabs-iconpricing-tab-2" data-bs-toggle="tab" href="#annual" role="tab" aria-controls="annual" aria-selected="false">
                            Annual billing
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="card-body text-lg-start text-center pt-0">
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">2 team members</span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">20GB Cloud storage </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check"></i>
                    </div>
                    <div>
                      <span class="ps-3">Integration help </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check"></i>
                    </div>
                    <div>
                      <span class="ps-3">Sketch Files </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check"></i>
                    </div>
                    <div>
                      <span class="ps-3">API Access </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check"></i>
                    </div>
                    <div>
                      <span class="ps-3">Complete documentation </span>
                    </div>
                  </div>
                  <a href="javascript:;" class="btn btn-icon bg-primary d-lg-block mt-3 mb-0">
                    Choose Plan
                    <i class="fas fa-arrow-right ms-1"></i>
                  </a>
                </div>
              </div>
            </div>
            <div class="col-lg-4 mb-lg-0 mb-4">
              <div class="card" style="border: 1px solid #cbc7cf; border-radius: 6px;">
                <div class="card-header text-center pt-4 pb-3">
                  <h5 class="text-dark">Pro</h5>
                  <p class="text-sm">Recommended</p>
                  <h1 class="font-weight-bold mt-2">
                    <p>$59 / monthly</p>
                  </h1>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-12 mx-auto text-center">
                    <div class="nav-wrapper mt-5 position-relative z-index-2">
                      <ul class="nav nav-pills nav-fill flex-row p-1" id="tabs-pricing" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link mb-0 active" id="tabs-iconpricing-tab-1" data-bs-toggle="tab" href="#monthly" role="tab" aria-controls="monthly" aria-selected="true">
                            Monthly billing
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link mb-0" id="tabs-iconpricing-tab-2" data-bs-toggle="tab" href="#annual" role="tab" aria-controls="annual" aria-selected="false">
                            Annual billing
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="card-body text-lg-start text-center pt-0">
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">10 team members</span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">40GB Cloud storage </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">Integration help </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">Sketch Files </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check"></i>
                    </div>
                    <div>
                      <span class="ps-3">API Access </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check"></i>
                    </div>
                    <div>
                      <span class="ps-3">Complete documentation </span>
                    </div>
                  </div>
                  <a href="javascript:;" class="btn btn-icon bg-primary d-lg-block mt-3 mb-0">
                    Choose Plan
                    <i class="fas fa-arrow-right ms-1"></i>
                  </a>
                </div>
              </div>
            </div>
            <div class="col-lg-4 mb-lg-0 mb-4">
              <div class="card" style="border: 1px solid #cbc7cf; border-radius: 6px;">
                <div class="card-header text-center pt-4 pb-3">
                  <h5 class="text-dark">Platinum</h5>
                  <p class="text-sm">Best Value</p>
                  <h1 class="font-weight-bold mt-2">
                    <p>$59 / monthly</p>
                  </h1>
                </div>
                <div class="card-body text-lg-start text-center pt-0">
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">Unlimited team members</span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">100GB Cloud storage </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">Integration help </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">Sketch Files </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">API Access </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">Complete documentation </span>
                    </div>
                  </div>
                  <a href="javascript:;" class="btn btn-icon bg-primary d-lg-block mt-3 mb-0">
                    Choose Plan
                    <i class="fas fa-arrow-right ms-1"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="annual">
          <div class="row">
            <div class="col-lg-4 mb-lg-0 mb-4">
              <div class="card" style="border: 1px solid #cbc7cf; border-radius: 6px;">
                <div class="card-header text-center pt-4 pb-3">
                  <h5 class="text-dark">Standard</h5>
                  <p class="text-sm">Most Popular</p>
                  <h1 class="font-weight-bold mt-2">
                    <p>$59 / yearly</p>
                  </h1>
                </div>
                <div class="card-body text-lg-start text-center pt-0">
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">2 team members</span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">20GB Cloud storage </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check"></i>
                    </div>
                    <div>
                      <span class="ps-3">Integration help </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check"></i>
                    </div>
                    <div>
                      <span class="ps-3">Sketch Files </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check"></i>
                    </div>
                    <div>
                      <span class="ps-3">API Access </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check"></i>
                    </div>
                    <div>
                      <span class="ps-3">Complete documentation </span>
                    </div>
                  </div>
                  <a href="javascript:;" class="btn btn-icon bg-primary d-lg-block mt-3 mb-0">
                    Choose Plan
                    <i class="fas fa-arrow-right ms-1"></i>
                  </a>
                </div>
              </div>
            </div>
            <div class="col-lg-4 mb-lg-0 mb-4">
              <div class="card" style="border: 1px solid #cbc7cf; border-radius: 6px;">
                <div class="card-header text-center pt-4 pb-3">
                  <h5 class="text-dark">Pro</h5>
                  <p class="text-sm">Recommended</p>
                  <h1 class="font-weight-bold mt-2">
                    <p>$59 / yearly</p>
                  </h1>
                </div>
                <div class="card-body text-lg-start text-center pt-0">
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">10 team members</span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">40GB Cloud storage </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">Integration help </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">Sketch Files </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check"></i>
                    </div>
                    <div>
                      <span class="ps-3">API Access </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check"></i>
                    </div>
                    <div>
                      <span class="ps-3">Complete documentation </span>
                    </div>
                  </div>
                  <a href="javascript:;" class="btn btn-icon bg-primary d-lg-block mt-3 mb-0">
                    Choose Plan
                    <i class="fas fa-arrow-right ms-1"></i>
                  </a>
                </div>
              </div>
            </div>
            <div class="col-lg-4 mb-lg-0 mb-4">
              <div class="card" style="border: 1px solid #cbc7cf; border-radius: 6px;">
                <div class="card-header text-center pt-4 pb-3">
                  <h5 class="text-dark">Platinum</h5>
                  <p class="text-sm">Best Value</p>
                  <h1 class="font-weight-bold mt-2">
                    <p>$59 / yearly</p>
                  </h1>
                </div>
                <div class="card-body text-lg-start text-center pt-0">
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">Unlimited team members</span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">100GB Cloud storage </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">Integration help </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">Sketch Files </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">API Access </span>
                    </div>
                  </div>
                  <div class="d-flex justify-content-lg-start justify-content-center p-2">
                    <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                      <i class="fas fa-check opacity-10"></i>
                    </div>
                    <div>
                      <span class="ps-3">Complete documentation </span>
                    </div>
                  </div>
                  <a href="javascript:;" class="btn btn-icon bg-primary d-lg-block mt-3 mb-0">
                    Choose Plan
                    <i class="fas fa-arrow-right ms-1"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container pb-lg-9 pb-10 pt-7 postion-relative z-index-2">
    <div class="row">
      <div class="col-md-6 mx-auto text-center">
        <h4 class="text-primary">Gift a physical reminder of your memory and connection with loved ones and the world</h4>
      </div>
    </div>
    <br>
    <div class="row">

      <!-- Heirloom Keepsake -->
      <div class="col-lg-6 mb-lg-0 mb-4">
        <div class="card" style="border: 1px solid #cbc7cf; border-radius: 6px;">
          <div class="card-header text-center pt-4 pb-3">
            <h5 class="text-dark">Heriloom Keepsake</h5>
            <p class="text-sm">The gift of rememberance</p>
            <h1 class="font-weight-bold mt-2">
              <p class="text-sm">$60</p>
            </h1>
          </div>
          <div class="card-body text-lg-start text-center pt-0">
            <div class="d-flex justify-content-lg-start justify-content-center p-2">
              <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                <i class="fas fa-check opacity-10"></i>
              </div>
              <div>
                <span class="ps-3">Unlimited team members</span>
              </div>
            </div>
            <div class="d-flex justify-content-lg-start justify-content-center p-2">
              <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                <i class="fas fa-check opacity-10"></i>
              </div>
              <div>
                <span class="ps-3">100GB Cloud storage </span>
              </div>
            </div>
            <div class="d-flex justify-content-lg-start justify-content-center p-2">
              <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                <i class="fas fa-check opacity-10"></i>
              </div>
              <div>
                <span class="ps-3">Integration help </span>
              </div>
            </div>
            <div class="d-flex justify-content-lg-start justify-content-center p-2">
              <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                <i class="fas fa-check opacity-10"></i>
              </div>
              <div>
                <span class="ps-3">Sketch Files </span>
              </div>
            </div>
            <div class="d-flex justify-content-lg-start justify-content-center p-2">
              <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                <i class="fas fa-check opacity-10"></i>
              </div>
              <div>
                <span class="ps-3">API Access </span>
              </div>
            </div>
            <div class="d-flex justify-content-lg-start justify-content-center p-2">
              <div class="icon icon-shape icon-xs rounded-circle bg-primary shadow text-center">
                <i class="fas fa-check opacity-10"></i>
              </div>
              <div>
                <span class="ps-3">Complete documentation </span>
              </div>
            </div>
            <a href="javascript:;" class="btn btn-icon bg-primary d-lg-block mt-3 mb-0">
              Choose Plan
              <i class="fas fa-arrow-right ms-1"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  <section style="padding:10%;">
    <div class="row pl-4 ml-5 card card-header" style="border: 1px solid #cbc7cf;border-radius: 6px; background:#FDD5C0">
      <div class="col-lg-6 mb-lg-0 mb-4">

        <div class="text-center pt-4 pb-3">
          <h5 class="text-primary">How would you like to get started</h5>
        </div>

      </div>
      <div class="col-lg-6 mb-lg-0 mb-4">
        <div class="text-center pt-4 pb-3">
          <button class="btn bg-primary mb-0 ms-2" type="button" name="button">Get started now</button>
          <button class="btn mb-0 ms-2" type="button" name="button">Start a free 30 day trial</button>

        </div>
      </div>
    </div>

    </div>

  </section>







  <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
  </script>
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../../assets/js/soft-ui-dashboard.min.js?v=1.0.7"></script>
  <style>
    .card-mid {

      background-color: #FDD5C0;
      width: 90%;
      height: 93px;
    }

    .nav.nav-pills .nav-link.active {
      background: white;
      color: #662d91;
      border: solid 2px #662d91;
      border-radius: 25px;
    }
  </style>
</body>

</html>


@endsection
