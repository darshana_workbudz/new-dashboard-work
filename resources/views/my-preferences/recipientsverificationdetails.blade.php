  <form class="">
    @csrf
    <input type="hidden" name="user_recipient_id" value="{{$user_recipient_id}}"> 
    <div class="col-12 mt-3">
     <p class="text-sm text-primary font-weight-bolder">How would you like this recipient to recieve a message</p>
   </div>

   <div class="col-6">

    <select  name="recieve_message_by" class="form-control @if(isset($error['recieve_message_by'][0]) && !empty($error['recieve_message_by'][0])) {{'is-invalid'}} @endif" style="width: 100%;">
      <option value="">Please Select</option>
      <option value="1">Direct Link from Evaheld</option>
      <option value="2">Link sent by Trusted Party</option>
      
    </select>

  </div>

  <div class="col-12 mt-4">

    <div class="button-row float-end d-flex mt-4">
      <button class="submit-button btn bg-primary btn-sm mb-0 me-2" type="button" name="button">Save Settings</button>
      <a data-route="{{route('manage-recipients.index')}}" class="link-handle btn btn-outline-dark btn-sm mb-0" type="button" name="button">Cancel</a>
    </div>

  </div>

</div>
</form>