@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Preferences')

@section('headercommon')
<x-main.header icon="accessibility" title="My Wellbeing System Preferences" subtitle="Update your wellbeing system preferences" />
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header">
        <div class="d-lg-flex">
          <nav style="--bs-breadcrumb-divider: '›';" aria-label="breadcrumb">
            <ol class="breadcrumb p-0 bg-white">
              <li class="breadcrumb-item"><a data-route="{{route('my-preferences.index')}}" class="link-handle h5 text-sm font-weight-bolder text-primary">My Preferences</a></li>
              <li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page">My Wellbeing System Preferences</li>
            </ol>
          </nav>
        </div>
      </div>
      <div class="card-body col-12 pt-0">
        <div>
          <div class="info-horizontal">
            <div class="icon">
              <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                <g id="Group_1867" data-name="Group 1867" transform="translate(-16342 -14028)">
                  <rect id="Rectangle_648" data-name="Rectangle 648" width="54.083" height="55" rx="16" transform="translate(16342 14028)" fill="#f3eef6"></rect>
                  <path id="Path_3903" data-name="Path 3903" d="M15.668,2a3.59,3.59,0,1,0,3.59,3.59A3.59,3.59,0,0,0,15.668,2ZM27.651,8.486a2.887,2.887,0,0,0-3.751-1.8l-2.308.862a1.077,1.077,0,0,0-.6.547,5.868,5.868,0,0,1-10.607.014,1.077,1.077,0,0,0-.6-.546L7.453,6.688a2.882,2.882,0,0,0-2.1,5.365l4.573,1.881v4.521l-2.832,8.2a2.81,2.81,0,1,0,5.289,1.9l2.651-7.12a.689.689,0,0,1,1.293,0l2.7,7.194a2.787,2.787,0,1,0,5.245-1.889l-2.859-8.3v-4.5L26,12.061A2.887,2.887,0,0,0,27.651,8.486Z" transform="translate(16353.41 14039.002)" fill="#662d91"></path>
                </g>
              </svg>
            </div>
            <div class="description ps-5">
              <h5>My Wellbeing System</h5>
              @if($default_prefrence=='1')
              <span class="badge bg-success text-capitalize">Currently active</span>
              @else
              <span class="badge bg-secondary text-capitalize">Not active</span>
              @endif
            </div>
            <p class="text-dark pt-3">Evaheld's Wellbeing System offers a safe and independent method for ensuring your content is released at the right time.</p>
            <p class="text-dark pt-3">By joining Evaheld, you are automatically enrolled in our Wellbeing System which periodically contacts you. By responding, we know to keep your account active and to continue safeguarding your messages.</p>
            <p class="text-dark pt-3">If you wish to opt-out, you may nominate a Trusted-Third Party who can notify us when to release your content. <a data-route="{{route('my-authorised-3p-access')}}" class="link-handle text-primary icon-move-right">Learn more</a></p>
          </div>
        </div>
        <hr class="my-4" />
        <h5>My Preferences</h5>
        <form action="{{route('save-checkin-process')}}" method="post">
          @csrf
          <input type="hidden" name="user_id" value="{{$user_id}}">
          <div class="row">
            <div class="col-12">
              <p class="text-sm text-primary font-weight-bolder">How often would you like us to send you Check-in emails?</p>
              @if(isset($followup_period) && count($followup_period)>0)

              @foreach($followup_period as $datafollowup)
              <div class="form-check  form-check-inline">

                <input id="followup_period_id" class="@if(isset($error['followup_period_id'][0]) && !empty($error['followup_period_id'][0])) {{'is-invalid'}} @endif form-check-input" type="radio" name="followup_period_id" id="customRadio{{$datafollowup->followup_period_id}}" value="{{$datafollowup->followup_period_id}}"  

                @if($old->followup_period_id==$datafollowup->followup_period_id || 
                (isset($checkinsetting->followup_period_id) && $checkinsetting->followup_period_id==$datafollowup->followup_period_id))
                {{'checked'}} @endif
                >
                <p class="text-dark" for="customRadio{{$datafollowup->followup_period_id}}">{{$datafollowup->name}}</p>
              </div>
              @endforeach

              @endif

              @if(isset($error['followup_period_id'][0]) && !empty($error['followup_period_id'][0]))
              <p class="form-text text-danger text-xs mb-1">
                {{$error['followup_period_id'][0]}}
              </p>
              @endif
            </div>
            <div class="col-12">
              <p class="text-sm text-primary">We rely on you to check for and respond to Evaheld's Wellbeing System's periodic correspondences to safeguard your content and know when to release them.</p>
              <div class="border-dashed border-1 border-secondary border-radius-md p-3">
                <div class="form-check">
                  <input class="@if(isset($error['responsibility_acknowledge'][0]) && !empty($error['responsibility_acknowledge'][0])) {{'is-invalid'}} @endif form-check-input" type="checkbox" name="responsibility_acknowledge"  @if($old->responsibility_acknowledge=='1' || (isset($checkinsetting->responsibility_acknowledge) && $checkinsetting->responsibility_acknowledge=='1'))
                  {{'checked'}} @endif value="1" id="responsibility_acknowledge" >
                  <p class="text-sm text-dark" for="responsibility_acknowledge">I accept responsibility for confirming that my account is still active and confirm that when I stop responding to Evaheld’s Wellbeing System correspondences, Evaheld has my permission to release my content according to my preferences.</p>
                </div>
              </div>
              @if(isset($error['responsibility_acknowledge'][0]) && !empty($error['responsibility_acknowledge'][0]))
              <p class="form-text text-danger text-xs mb-1">
                {{$error['responsibility_acknowledge'][0]}}
              </p>
              @endif

            </div>
            <div class="mt-3" id="showstep2" @if($old->responsibility_acknowledge=='1' || (isset($checkinsetting->responsibility_acknowledge) && $checkinsetting->responsibility_acknowledge=='1')) style="display: block;" @else style="display: none;" @endif>
              <div class="row">
                <div class="col-lg-12 col-sm-12 mt-3">
                  <p class="text-sm text-primary font-weight-bolder">If we don’t receive your response to periodic Wellbeing System correspondences, what would you like us to do before we release your content?</p>
                  @if(isset($failed_response_action) && count($failed_response_action)>0)
                  @foreach($failed_response_action as $datafailed_response_action)
                  <div class="form-check">
                    <input value="{{$datafailed_response_action->failed_response_action_id}}" class="@if(isset($error['failed_response_action_id'][0]) && !empty($error['failed_response_action_id'][0])) {{'is-invalid'}} @endif form-check-input" type="radio" name="failed_response_action_id" id="failed_response_action{{$datafailed_response_action->failed_response_action_id}}" @if($old->failed_response_action_id==$datafailed_response_action->failed_response_action_id || (isset($checkinsetting->failed_response_action_id) && $checkinsetting->failed_response_action_id==$datafailed_response_action->failed_response_action_id))
                    {{'checked'}} @endif
                    >
                    <p class="text-sm text-dark" for="failed_response_action{{$datafailed_response_action->failed_response_action_id}}">{{$datafailed_response_action->description}}</p>
                  </div>
                
                @endforeach

                @endif
                @if(isset($error['failed_response_action_id'][0]) && !empty($error['failed_response_action_id'][0]))
                <p class="form-text text-danger text-xs mb-1">
                  {{$error['failed_response_action_id'][0]}}
                </p>
                @endif
              </div>


            <!--   <div class="col-lg-6 col-sm-12 mt-4">
                <p class="text-sm text-primary">How long after we don't hear back from you would you like us to send out your messages or give access to your authorised person ?</p>
             </div>

             <div class="col-lg-6 col-sm-12 mt-lg-4 mt-sm-0">
              <select  name="wating_period_no_of_days" class="form-control @if(isset($error['wating_period_no_of_days'][0]) && !empty($error['wating_period_no_of_days'][0])) {{'is-invalid'}} @endif" style="width: 100%;">
                @if(isset($wating_period) && !empty($wating_period))
                @foreach($wating_period as $wating_perioddata)
                <option value="{{$wating_perioddata->wating_period_id}}" 

                  @if($old->wating_period_no_of_days==$wating_perioddata->wating_period_id || (isset($checkinsetting->wating_period_no_of_days) && $checkinsetting->wating_period_no_of_days==$wating_perioddata->wating_period_id))
                  {{'selected'}} @endif


                  >{{$wating_perioddata->name}}</option>
                  @endforeach
                  @endif



                </select>

                @if(isset($error['wating_period_no_of_days'][0]) && !empty($error['wating_period_no_of_days'][0]))
                <p class="form-text text-danger text-xs mb-1">
                  {{$error['wating_period_no_of_days'][0]}}
                </p>
                @endif
              </div> -->


            


             
             
              <div class="col-lg-12 col-sm-12">
              <div class="border-dashed border-1 border-secondary border-radius-md p-3">
                <div class="form-check">
                  <input  class="form-check-input @if(isset($error['release_message_acknowledge'][0]) && !empty($error['release_message_acknowledge'][0])) {{'is-invalid'}} @endif" type="checkbox" value="1" id="release_message_acknowledge" name="release_message_acknowledge"

                  @if($old->release_message_acknowledge=='1' || (isset($checkinsetting->release_message_acknowledge) && $checkinsetting->release_message_acknowledge=='1'))
                  {{'checked'}} @endif

                  >
                      <p class="text-sm text-dark">I accept responsibility for confirming that my account is still active and confirm that when I stop responding to Evaheld’s Wellbeing System correspondences, Evaheld has my permission to release my messages according
                        to my message preferences.</p>
                    </div>
                  </div>
                  @if(isset($error['release_message_acknowledge'][0]) && !empty($error['release_message_acknowledge'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['release_message_acknowledge'][0]}}
                  </p>
                  @endif
                </div>
                <div class="col-lg-12 col-sm-12 mt-3">
                  <div class="border-dashed border-1 border-secondary border-radius-md p-3">
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" name="final_acknowledge" value="1" @if($old->final_acknowledge=='1' || (isset($checkinsetting->final_acknowledge) && $checkinsetting->final_acknowledge=='1'))
                      {{'checked'}} @endif>
                      <p class="font-weight-bolder text-md text-dark">I confirm that once Evaheld has completed all of the prerequisites above, they have my permission to release all my messages according to my Message preferences. Evaheld does not need to perform any other actions to verify my death. However, if they choose to, they also have my permission to contact the Department of Births, Deaths and Marriages in the applicable state and country, and to use my personal information to confirm my death.</p>
                    </div>
                  </div>
                  @if(isset($error['final_acknowledge'][0]) && !empty($error['final_acknowledge'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['final_acknowledge'][0]}}
                  </p>
                  @endif
                </div>
              </div>
            </div>
            <div class="col-12 mt-4">
              <div class="button-row float-end d-flex mt-4">
                <button class="submit-button btn bg-primary btn-sm mb-0 me-2" type="button" name="button">Save</button>
                <a data-route="{{route('my-preferences.index')}}" class="link-handle btn btn-outline-dark btn-sm mb-0" type="button" name="button">Cancel</a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@if(isset($authoriseddata) && !empty($authoriseddata) && empty($old->successmessage) && $default_prefrence==2)
<script type="text/javascript">
  swal({
    text: "You have appointed a Trusted Party and therefore cannot enroll in the check-in process",
    icon: "warning",
    buttons: {
      confirm: {
        text: 'Update my Trusted Party',
        className: ' text-white bg-primary'
      },
      cancel: "Cancel"
    },
  }).then((will) => {

     

    if(will)
    {
       window.location="{{route('my-authorised-3p-access')}}";
    }
    else
    {
       window.location="{{route('my-authorised-3p-access')}}";
    }

  });

</script>
@endif
<script type="text/javascript">
  $( ".nav-link-switch" ).click(function() {

    $('.nav-link-switch').removeClass('active');
    $(this).addClass('active');


  });



  $('#responsibility_acknowledge').click(function() {

    $('#showstep2').hide();

    if($(this).is(':checked'))
    {

      $('#showstep2').show();

    }

  });

  $('#release_message_acknowledge').click(function() {

    $('#showstep3').hide();

    if($(this).is(':checked'))
    {

      $('#showstep3').show();

    }

  });

  $('#verification_process_step').change(function() {



    if($('#verification_process_step option:selected').val()=='2' || $('#verification_process_step option:selected').val()=='1')
    {

      $('#showstep4').show();

    }
    else
    {
      $('#showstep4').hide();
    }

  });
</script>
@endsection