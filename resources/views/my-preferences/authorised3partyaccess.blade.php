@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Preferences')

@section('headercommon')
<x-main.header icon="person_available" title="My Trusted Party" subtitle="Update your Trusted Party preferences" />
@endsection
@section('content')
<style type="text/css">
    .card-headline
    {
        background:#f0f8ff94;
        border-radius:8px;
    }
</style>

<div class="row">
    <div class="card">
        <!-- <div class="card-header pb-2">
            <div class="nav-wrapper position-relative end-0">
                <ul class="nav nav-pills nav-fill" role="tablist">

                   <li class="nav-item">
              <a data-route="{{route('my-checkin-process')}}" class="link-handle nav-link nav-link-switch mb-0  px-3 py-2 @if($state=='1'){{'active'}}@endif">
                  My Check-in Process &nbsp;@if($default_prefrence=='1') <i class="fa fa-check-circle-o text-success" aria-hidden="true"></i> @else <i class="fa fa-times-circle-o text-danger" aria-hidden="true"></i> @endif
              </a>
          </li>
          <li class="nav-item ">
              <a data-route="{{route('my-authorised-3p-access')}}" class="link-handle nav-link nav-link-switch mb-0 pm-3 py-2 @if($state=='2'){{'active'}}@endif">
                  My Authorised 3rd party &nbsp;@if($default_prefrence=='2') <i class="fa fa-check-circle-o text-success" aria-hidden="true"></i> @else <i class="fa fa-times-circle-o text-danger" aria-hidden="true"></i> @endif
              </a>
          </li>

                </ul>
            </div>
        </div> -->
        <div class="card-header">
        <div>
              <div class="info-horizontal">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                    <g id="Group_1864" data-name="Group 1864" transform="translate(-16401 -14025)">
                      <rect id="Rectangle_649" data-name="Rectangle 649" width="54.083" height="55" rx="16" transform="translate(16401 14025)" fill="#f3eef6"></rect>
                      <path id="Path_3904" data-name="Path 3904" d="M23.261,15.716a7.545,7.545,0,1,1-7.545,7.545,7.545,7.545,0,0,1,7.545-7.545Zm-7.514,2.743a8.918,8.918,0,0,0,.782,10.646,18.051,18.051,0,0,1-3.556.329c-4.691,0-8.154-1.471-10.269-4.44A3.772,3.772,0,0,1,2,22.807V21.545a3.085,3.085,0,0,1,3.085-3.086H15.748Zm3.885,4.316a.686.686,0,1,0-.97.97L21.4,26.488a.686.686,0,0,0,.97,0L27.861,21a.686.686,0,1,0-.97-.97l-5,5ZM12.972,2A6.859,6.859,0,1,1,6.114,8.863,6.859,6.859,0,0,1,12.972,2Z" transform="translate(16411.637 14036.096)" fill="#662d91"></path>
                    </g>
                  </svg>
                </div>
                <div class="description ps-5">
                  <h5>My Trusted Third Party</h5>
                  @if($default_prefrence=='2') <span class="badge bg-success text-capitalize">Currently Active</span> @else<span class="badge bg-secondary text-capitalize">Not active</span> @endif
                </div>
                <p class="text-dark pt-3">Evaheld's Wellbeing System offers a safe and independent method for ensuring your content is released at the right time.</p>
                <p class="text-dark pt-3">By joining Evaheld, you are automatically enrolled in our Wellbeing System which periodically contacts you. By responding, we know to keep your account active and to continue safeguarding your messages from premature release. <a data-route="{{route('my-checkin-process')}}" class="link-handle text-primary icon-move-right">Learn more about the Wellbeing System.<i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i></a>
                <p class="text-dark pt-3">If you wish to opt-out, you may nominate a Trusted-Third Party who can notify us when to release your content. Complete the form below to get started.</p>
              </div>
            </div>
            <hr class="my-4"/>
            <h5>My Preferences</h5>
            <div class="d-lg-flex">
                <div>
                    <h5 class="font-weight-bolder text-sm text-primary"> <a data-route="{{route('my-preferences.index')}}" class="link-handle"><i class="fa fa-chevron-left text-primary" aria-hidden="true" ></i></a> &nbsp;Trusted Party access @if($default_prefrence=='2') <i class="fa fa-check-circle-o text-success" aria-hidden="true"></i> @else <i class="fa fa-times-circle-o text-danger" aria-hidden="true"></i> @endif</h5>
                    <p class="text-xs text-dark mb-0">
                        You are automatically enrolled in Evaheld’s Check-in Process unless you appoint an Authorised 3rd Party. If you choose to appoint an authorised 3rd party, you must complete all the preferences and contact details below for your appointee and they must accept your appointment. We rely on you solely to ensure that your authorised 3rd party’s details are correct and we do not engage in any investigative or confirmation practices to verify the details that you provide, nor do we take responsibility if the details provided are incorrect and your messages remain undelivered.
                    </p>
                </div>

            </div>
        </div>

        <div class="card-body">
                




                <div class="row">
                    <input type="hidden" id="step1confirmed" name="step1confirmed" value="<?php isset($authoriseddata) && !empty($authoriseddata) ? $authoriseddata->step1confirmed :0;?>" >
                    <input type="hidden" id="step2confirmed" name="step2confirmed" value="<?php isset($authoriseddata) && !empty($authoriseddata) ? $authoriseddata->step2confirmed :0;?>" >
                    <div class="parent-div" id="authorised-profile-details">
                            <form action="{{route('save-authorised-3p-access')}}" method="post">
                            @csrf
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                            @if(isset($authoriseddata) && !empty($authoriseddata))
                            <input type="hidden" name="showdiv" value="1">
                            @endif
                            <div class="row p-3">



                                <div class="col-lg-12 col-sm-12">
                                <p class="text-sm text-primary font-weight-bolder">Would you like to appoint a Trusted Party to advise us of your death?</p>
                                </div>
                                <div class="col-lg-12 col-sm-12">
                                    <div class="form-check form-check-inline">

                                        <input class="nominated_authorised_person_access @if(isset($error['nominated_authorised_person_access'][0]) && !empty($error['nominated_authorised_person_access'][0])) {{'is-invalid'}} @endif form-check-input" type="radio" name="nominated_authorised_person_access" id="customRadio1" value="1" @if($old->nominated_authorised_person_access=='1' || (isset($user->nominated_authorised_person_access) && $user->nominated_authorised_person_access=='1' && $old->nominated_authorised_person_access!=='2') ) {{'checked'}}@endif>
                                        <p class="text-dark" for="customRadio1">Yes</p>

                                    </div>

                                    <div class="form-check form-check-inline">

                                        <input class="nominated_authorised_person_access @if(isset($error['nominated_authorised_person_access'][0]) && !empty($error['nominated_authorised_person_access'][0])) {{'is-invalid'}} @endif form-check-input" type="radio" name="nominated_authorised_person_access" id="customRadio2" value="2" @if($old->nominated_authorised_person_access=='2' || (isset($user->nominated_authorised_person_access) && $user->nominated_authorised_person_access=='2' && $old->nominated_authorised_person_access!=='1')){{'checked'}}@endif>
                                        <p class="text-dark" for="customRadio2">No</p>

                                    </div>

                                    @if(isset($error['nominated_authorised_person_access'][0]) && !empty($error['nominated_authorised_person_access'][0]))
                                    <p class="form-text text-danger text-xs mb-1">
                                        {{$error['nominated_authorised_person_access'][0]}}
                                    </p>
                                    @endif



                                </div>

                            </div>


                            @if(isset($authoriseddata) && !empty($authoriseddata) && $user->nominated_authorised_person_access=='1')

                        <div class="card card-body shadow-lg" id="authorised-profile">

                            <div class="row justify-content-center align-items-center">

                                <div class="col-sm-auto col-lg-6 col-sm-12">
                                    <div class="h-100">

                                        <h5 class="mb-1 text-primary">

                                            {{$authoriseddata->first_name}} {{$authoriseddata->last_name}} <a href="#" class="show-update-form">

                                            </a>

                                        </h5>

                                        <p class="mb-0 text-sm">
                                            {{$authoriseddata->email}}
                                        </p>

                                    </div>

                                </div>

                                <div class="col-lg-4 col-sm-12 ms-sm-auto mt-sm-0 mt-3 d-flex">

                                    @if($authoriseddata->invitation_status==0)
                                    <div class="d-flex align-items-center">
                                        <button class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fi fi_dismiss" aria-hidden="true"></i></button>
                                        <span class="text-xs">Pending to Accept Invitation</span>
                                    </div>
                                    @endif
                                    @if($authoriseddata->invitation_status==1)
                                    <div class="d-flex align-items-center">
                                        <button class="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check" aria-hidden="true"></i></button>
                                        <span class="text-sm">Accepted Invitation </span>
                                    </div>
                                    @endif
                                </div>
                                <hr class="m-2">
                                <p class="text-xs text-dark mb-2 mt-2">If you have not given Evaheld permission to contact your Trusted Party directly, please send this link to your Trusted Party so that they can accept your appointment. While your Trusted Party does not accept their appointment, you will remain enrolled in Evaheld’s Wellbeing System.</p>
                                <div class="d-flex align-items-center">
                                    <div class="form-group w-70">
                                        <div class="input-group bg-gray-200">
                                            <input id="copy-link" class="form-control form-control-sm" value="{{$authoriseddata->invitation_link}}" type="text" disabled="">

                                        </div>
                                    </div>
                                    <span onclick="copyToClipboard('#copy-link')" class="btn btn-sm bg-primary ms-2 px-3">Copy</span>
                                </div>
                               <!--  <p class="text-xs text-primary mb-1">* We will sms and email you link to give to your authorised person you can also access link below - please give this linkto your authorised person immediately so that they can accept their appointment as your Trusted Party </p> -->
                            </div>

                            <hr class="horizontal dark">
                            </div>
                            @endif

                            <div class="col-12 mt-4" id="showbuttondiv" @if($old->nominated_authorised_person_access=='2' || $user->nominated_authorised_person_access=='2' && $old->nominated_authorised_person_access!=='1') @else style="display:none;" @endif>

                                <div class="button-row float-end d-flex mt-4">
                                    <button class="submit-button btn bg-primary btn-sm mb-0 me-2" type="button" name="button">Save</button>
                                    <a data-route="{{route('my-authorised-3p-access')}}" class="link-handle btn btn-outline-dark btn-sm mb-0" type="button" name="button">Cancel</a>
                                </div>

                            </div>

                            <div id="showstep2" class="row mt-2 p-3" @if(($old->nominated_authorised_person_access=='1' ) || ($old->showdiv=='1' ) || ($user->nominated_authorised_person_access=='1' && $old->nominated_authorised_person_access!=='2')) @else style="display:none;" @endif>
                                <div class="d-flex mt-4 card-headline p-3" data-bs-toggle="collapse" data-bs-target="#showauthdetails">
                                    <div class="icon icon-shape icon-sm shadow border-radius-md bg-primary text-center d-flex align-items-center justify-content-center mt-2">
                                    <span class="fi fi_person_available" style="color: #ffffff; font-size: 30px"></span>
                                    </div>
                                    <div class="my-auto ms-3">
                                        <div class="h-100">
                                        <h6 class="mb-0">My Trusted Party's Contact Details &nbsp; <i class="fa fa-edit" style="cursor: pointer;" ></i></h6>
                                            <p class="mb-0 text-sm">Step 1</p>
                                        </div>
                                    </div>

                                    @if(isset($authoriseddata) && !empty($authoriseddata) )  
                                <i data-bs-toggle="collapse" data-bs-target="#showauthdetails" class="fi fi_person-edit text-dark ms-auto p-2" aria-hidden="true"></i>
                                    @endif

                                </div>

                                <div id="showauthdetails" class="row @if(isset($authoriseddata) && !empty($authoriseddata) && $old->showdiv!==1) collapse @endif">
                                <h5 class="text-sm text-primary font-weight-bolder mt-3">Provide details about your Trusted Party</h5>
                                <div class="col-12 col-sm-6  mt-3">
                                    <label class="form-label">Given name <span class="text-danger">*</span></label>
                                    <input placeholder="John" class="form-control @if(isset($error['first_name'][0]) && !empty($error['first_name'][0])) is-invalid @endif" type="text" name="first_name" value="<?php echo isset($authoriseddata->first_name) ? $authoriseddata->first_name : $old->first_name; ?>">
                                    @if(isset($error['first_name'][0]) && !empty($error['first_name'][0]))
                                    <p class="form-text text-danger text-xs mb-1">
                                        {{$error['first_name'][0]}}
                                    </p>
                                    @endif
                                </div>
                                <div class="col-12 col-sm-6  mt-3">
                                    <label class="form-label">Family name <span class="text-danger">*</span></label>
                                    <input placeholder="Smith" class="form-control @if(isset($error['last_name'][0]) && !empty($error['last_name'][0])) is-invalid @endif" type="text" name="last_name" value="<?php echo isset($authoriseddata->last_name) ? $authoriseddata->last_name : $old->last_name; ?>">
                                    @if(isset($error['last_name'][0]) && !empty($error['last_name'][0]))
                                    <p class="form-text text-danger text-xs mb-1">
                                        {{$error['last_name'][0]}}
                                    </p>
                                    @endif
                                </div>
                                <div class="col-12 col-sm-6  mt-1">
                                    <label class="form-label">Email address <span class="text-danger">*</span></label>
                                    <input placeholder="example@email.com" class="form-control @if(isset($error['email'][0]) && !empty($error['email'][0])) is-invalid @endif" type="email" name="email" value="<?php echo isset($authoriseddata->email) ? $authoriseddata->email : $old->email; ?>">
                                    @if(isset($error['email'][0]) && !empty($error['email'][0]))
                                    <p class="form-text text-danger text-xs mb-1">
                                        {{$error['email'][0]}}
                                    </p>
                                    @endif
                                </div>
                                <div class="col-12 col-sm-6 mt-1">
                                    <div class="row">
                                        <label class="form-label">Phone number <span class="text-danger">*</span></label>
                                        <div class="col-3 mt-2 mt-sm-0 pr-0 mr-0">
                                            <select name="phone_code" class="phonecodeselect2 form-control" style="width: 100%;">
                                                @if(isset($phonecodes) && !empty($phonecodes))
                                                @foreach($phonecodes as $data)
                                                <option value="{{$data->id}}" data-code="{{$data->iso2}}" @if(isset($authoriseddata->phone_code) && $authoriseddata->phone_code==$data->id){{"selected"}} @elseif($data->iso2=='AU') {{"selected"}} @endif >+{{$data->phone_code}} {{$data->iso2}} </option>
                                                @endforeach
                                                @endif

                                            </select>
                                        </div>
                                        <div class="col-9 mt-2 mt-sm-0 pl-0 ml-0">
                                            <input placeholder="0412345678" id="phone_number" class="required form-control @if(isset($error['phone_number'][0]) && !empty($error['phone_number'][0])) is-invalid @endif" type="number" name="phone_number" value="<?php echo isset($authoriseddata->phone_number) ? $authoriseddata->phone_number : $old->phone_number; ?>">
                                            @if(isset($error['phone_number'][0]) && !empty($error['phone_number'][0]))
                                            <p class="form-text text-danger text-xs mb-1">
                                                {{$error['phone_number'][0]}}
                                            </p>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 col-sm-6  mt-1">
                                    <label class="form-label">Relation to me</label>
                                    <select name="relationship" id="relationship" class="select2 form-control" style="width: 100%;">
                                        @if(isset($relationship) && !empty($relationship))
                                        <option value="" >Select</option>
                                        @foreach($relationship as $relationshipdata)
                                        <option value="{{$relationshipdata->relationship_id}}" @if(isset($authoriseddata->relationship_id) && $authoriseddata->relationship_id==$relationshipdata->relationship_id){{"selected"}} @endif >{{ucfirst($relationshipdata->relationship_name)}}</option>
                                        @endforeach
                                        @endif

                                    </select>
                                </div>
                                <div class="col-12 col-sm-6  mt-1 otherrelationship" @if($old->relationship=='18' || (isset($authoriseddata->relationship) && $authoriseddata->relationship=='18')) @else style="display:none"; @endif>
                                    <label class="form-label">Other (Relation)</label>
                                    <input placeholder="Daughter-in-law" class="form-control @if(isset($error['other_relationship'][0]) && !empty($error['other_relationship'][0])) is-invalid @endif" name="other_relationship" type="text" value="<?php echo isset($authoriseddata->other_relationship) ? $authoriseddata->other_relationship : $old->other_relationship; ?>">
                                    @if(isset($error['other_relationship'][0]) && !empty($error['other_relationship'][0]))
                                    <p class="form-text text-danger text-xs mb-1">
                                        {{$error['other_relationship'][0]}}
                                    </p>
                                    @endif
                                </div>


                                <div class="col-12 col-sm-6"></div>
                                <div class="row">
                                    <div class="col-12 col-sm-6 field-wrapper address-lookup mt-1">
                                        <label class="form-label">Address </label>
                                        <input class="form-control address-lookup__field @if(isset($error['address_3'][0]) && !empty($error['address_3'][0])) is-invalid @endif" type="text" name="address_line_3" autocomplete="off" value="<?php echo isset($authoriseddata->address_3) ? $authoriseddata->address_3 : ''; ?>">

                                        <a href="javascript:;" class="mt-1 show-manual text-xs float-end text-secondary"><u>Manually enter my address</u></a>

                                        @if(isset($error['address_line_1'][0]) && !empty($error['address_line_1'][0]))
                                        <p class="form-text text-danger text-xs mb-1">
                                            Please search your address or enter it manually
                                        </p>
                                        @endif
                                    </div>
                                </div>


                                <div class="row show-manual-div" style="display: none;">
                                    <input class="form-control" id="manual-address" name="manual" type="hidden" value="0">
                                    <input class="form-control" name="city" type="hidden">
                                    <input class="form-control" name="county" type="hidden">
                                    <div class="col-12 col-sm-6">
                                        <label class="form-label">Address Line 1</label>
                                        <input placeholder="98 Shirley Street" class="form-control @if(isset($error['address_line_1'][0]) && !empty($error['address_line_1'][0])) is-invalid @endif" type="text" name="address_line_1" value="<?php echo isset($authoriseddata->address_1) ? $authoriseddata->address_1 : ''; ?>">
                                        @if(isset($error['address_line_1'][0]) && !empty($error['address_line_1'][0]))
                                        <p class="form-text text-danger text-xs mb-1">
                                            {{$error['address_line_1'][0]}}
                                        </p>
                                        @endif
                                    </div>

                                    <div class="col-12 col-sm-6">
                                        <label class="form-label">Address Line 2</label>
                                        <input placeholder="PIMPAMA" class="form-control @if(isset($error['address_line_2'][0]) && !empty($error['address_line_2'][0])) is-invalid @endif" type="text" name="address_line_2" value="<?php echo isset($authoriseddata->address_2) ? $authoriseddata->address_2 : ''; ?>">
                                        @if(isset($error['address_line_2'][0]) && !empty($error['address_line_2'][0]))
                                        <p class="form-text text-danger text-xs mb-1">
                                            {{$error['address_line_2'][0]}}
                                        </p>
                                        @endif
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label class="form-label">Postcode</label>
                                        <input placeholder="4209" class="form-control @if(isset($error['postcode'][0]) && !empty($error['postcode'][0])) is-invalid @endif" name="postcode" type="number" value="<?php echo isset($authoriseddata->postcode) ? $authoriseddata->postcode : ''; ?>">
                                        @if(isset($error['postcode'][0]) && !empty($error['postcode'][0]))
                                        <p class="form-text text-danger text-xs mb-1">
                                            {{$error['postcode'][0]}}
                                        </p>
                                        @endif
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label class="form-label">State</label>
                                        <input placeholder="QLD" class="form-control @if(isset($error['state'][0]) && !empty($error['state'][0])) is-invalid @endif" name="state" type="text" value="<?php echo isset($authoriseddata->state) ? $authoriseddata->state : ''; ?>">
                                        @if(isset($error['state'][0]) && !empty($error['state'][0]))
                                        <p class="form-text text-danger text-xs mb-1">
                                            {{$error['state'][0]}}
                                        </p>
                                        @endif
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <label class="form-label">Country</label>
                                        <select name="country" id="country" class="select2 form-control @if(isset($error['country'][0]) && !empty($error['country'][0])) is-invalid @endif" style="width: 100%;">
                                            @if(isset($phonecodes) && !empty($phonecodes))
                                            <option value="">Select</option>
                                            @foreach($phonecodes as $data)
                                            <option value="{{$data->iso2}}" @if(isset($authoriseddata->country) && $authoriseddata->country==$data->iso2){{"selected"}} @endif > {{$data->name}} </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <hr class="mt-4" />
                                <div class="row">
                                    <div class="col-12 col-sm-12 mt-3">
                                        <p class="text-sm text-primary font-weight-bolder">Is your Trusted Party also your Solicitor/Lawyer or your Power of Attorney? <span class="text-danger">*</span></p>
                                    </div>

                                    <div class="col-12 col-sm-6">
                                        <div class="form-check form-check-inline">

                                            <input class="islawyer @if(isset($error['islawyer'][0]) && !empty($error['islawyer'][0])) {{'is-invalid'}} @endif form-check-input islawyer" type="radio" name="islawyer" id="customRadio3" value="1" @if($old->islawyer=='1' || ( isset($authoriseddata->islawyer) && $authoriseddata->islawyer=='1' && $old->islawyer!=='2')){{'checked'}}@endif>
                                            <p class="text-dark" for="customRadio3">Yes</p>
                                        </div>

                                        <div class="form-check form-check-inline">

                                            <input class="islawyer @if(isset($error['islawyer'][0]) && !empty($error['islawyer'][0])) {{'is-invalid'}} @endif form-check-input islawyer" type="radio" name="islawyer" id="customRadio4" value="2" @if($old->islawyer=='2' || ( isset($authoriseddata->islawyer) && $authoriseddata->islawyer=='2' && $old->islawyer!=='1')){{'checked'}}@endif>
                                            <p class="text-dark" for="customRadio4">No</p>
                                        </div>

                                        @if(isset($error['islawyer'][0]) && !empty($error['islawyer'][0]))
                                        <p class="form-text text-danger text-xs mb-1">
                                            {{$error['islawyer'][0]}}
                                        </p>
                                        @endif

                                    </div>

                                </div>

                                <div class="row organisation-div" @if($old->islawyer=='1' || ( isset($authoriseddata->islawyer) && $authoriseddata->islawyer=='1')) @else style="display: none;" @endif>
                                    <h5 class="text-sm text-primary font-weight-bolder mt-3"><i class="text-success"></i>Organisation Details</h5>
                                    <div class="col-12 col-sm-6  mt-1">
                                        <label class="form-label">Business name <span class="text-danger">*</span></label></label>
                                        <input placeholder="Acme Solicitors" class="form-control @if(isset($error['organisation'][0]) && !empty($error['organisation'][0])) is-invalid @endif" name="organisation" type="text" value="<?php echo isset($authoriseddata->organisation) ? $authoriseddata->organisation : ''; ?>">
                                        @if(isset($error['organisation'][0]) && !empty($error['organisation'][0]))
                                        <p class="form-text text-danger text-xs mb-1">
                                            {{$error['organisation'][0]}}
                                        </p>
                                        @endif
                                    </div>
                                    <div class="col-12 col-sm-6 mt-1">
                                        <div class="row">
                                            <label class="form-label">Phone number <span class="text-danger">*</span></label>
                                            <div class="col-3 mt-2 mt-sm-0 pr-0 mr-0">
                                                <select name="organisation_phonecode" class="phonecodeselect1 form-control" style="width: 100%;">
                                                    @if(isset($phonecodes) && !empty($phonecodes))
                                                    @foreach($phonecodes as $data)
                                                    <option value="{{$data->id}}" data-code="{{$data->iso2}}" @if(isset($authoriseddata->phone_code) && $authoriseddata->phone_code==$data->id){{"selected"}} @elseif($data->iso2=='AU') {{"selected"}} @endif >+{{$data->phone_code}} {{$data->iso2}} </option>
                                                    @endforeach
                                                    @endif

                                                </select>
                                            </div>
                                            <div class="col-9 mt-2 mt-sm-0 pl-0 ml-0">
                                                <input placeholder="0712345678" id="organisation_phonenumber" class="required form-control @if(isset($error['organisation_phonenumber'][0]) && !empty($error['organisation_phonenumber'][0])) is-invalid @endif" type="number" name="organisation_phonenumber" value="<?php echo isset($authoriseddata->organisation_phonenumber) ? $authoriseddata->organisation_phonenumber : $old->organisation_phonenumber; ?>">
                                                @if(isset($error['organisation_phonenumber'][0]) && !empty($error['organisation_phonenumber'][0]))
                                                <p class="form-text text-danger text-xs mb-1">
                                                    {{$error['organisation_phonenumber'][0]}}
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 field-wrapper address-lookup mt-1">
                                        <label class="form-label">Address <span class="text-danger">*</span></label></label>
                                        <input placeholder="98 Shirley Street PIMPAMA QLD 4209" class="form-control address-lookup-organisation @if(isset($error['organisation_address'][0]) && !empty($error['organisation_address'][0])) is-invalid @endif" type="text" name="organisation_address" autocomplete="off" value="<?php echo isset($authoriseddata->organisation_address) ? $authoriseddata->organisation_address : null; ?>">
                                        @if(isset($error['organisation_address'][0]) && !empty($error['organisation_address'][0]))
                                        <p class="form-text text-danger text-xs mb-1">
                                            {{$error['organisation_address'][0]}}
                                        </p>
                                        @endif



                                    </div>

                                </div>
                                <div class="col-12 mt-4">

                                <div class="button-row float-end d-flex mt-4">
                                    <button class="submit-button btn bg-primary btn-sm mb-0 me-2" type="button" name="button">Save</button>
                                    <a data-route="{{route('my-authorised-3p-access')}}" class="link-handle btn btn-outline-dark btn-sm mb-0" type="button" name="button">Cancel</a>
                                </div>

                                </div>

                            </div>
                            </div>



                            
                        </form>
                    </div>
                    @if(isset($authoriseddata) && !empty($authoriseddata) && $user->nominated_authorised_person_access=='1')
                    <div id="showdiv3" class="sub-div mt-3">

                    <div class="d-flex card-headline p-3" data-bs-toggle="collapse" data-bs-target="#showprovisiondetails">
                        <div class="icon icon-shape icon-sm shadow border-radius-md bg-primary text-center d-flex align-items-center justify-content-center  me-2 mt-2">
                            <span class="fi fi_shield_task" style="color: #ffffff; font-size: 30px"></span>
                        </div>
                        <div class="my-auto ms-3">
                            <div class="h-100">
                                <h6 class="mb-0">My Trusted Party's Permissions &nbsp; <i class="fa fa-edit" style="cursor: pointer;" ></i></h6>
                                <p class="mb-0 text-sm">Step 2</p>
                            </div>
                        </div>
                        @if($authoriseddata->step2confirmed==1)
                        <i class="fi fi_person-edit text-dark ms-auto p-2" data-bs-toggle="collapse" data-bs-target="#showprovisiondetails" aria-hidden="true"></i>
                        @endif
                    </div>
                    <hr class="horizontal dark mt-2">
                    <form action="{{route('update-authorised-3p-access')}}" method="post" id="showprovisiondetails" class="p-3 @if($authoriseddata->step2confirmed==1 && $old->showdiv!==2) collapse @endif">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$user->id}}">

                            <input type="hidden" name="showdiv" value="2">

                        <div class="row mt-3">
                            <div class="col-12 col-sm-12 ">
                                <p class="text-sm text-primary font-weight-bolder">Would you like your Trusted Party to have access to your account after your death?</p>
                            </div>

                                <div class="col-lg-12 col-sm-12">
                                    <div class="form-check form-check-inline">

                                        <input class="giveaccountaccess @if(isset($error['giveaccountaccess'][0]) && !empty($error['giveaccountaccess'][0])) {{'is-invalid'}} @endif form-check-input" type="radio" name="giveaccountaccess" id="customRadio1" value="1" @if($old->giveaccountaccess=='1' || (isset($authoriseddata->giveaccountaccess) && $authoriseddata->giveaccountaccess=='1')) {{'checked'}}@endif>
                                        <p class="text-dark" for="customRadio1">Yes</p>

                                    </div>

                                    <div class="form-check form-check-inline">

                                        <input class="giveaccountaccess @if(isset($error['giveaccountaccess'][0]) && !empty($error['giveaccountaccess'][0])) {{'is-invalid'}} @endif form-check-input" type="radio" name="giveaccountaccess" id="customRadio2" value="2" @if($old->giveaccountaccess=='2' || (isset($authoriseddata->giveaccountaccess) && $authoriseddata->giveaccountaccess=='2')){{'checked'}}@endif>
                                        <p class="text-dark" for="customRadio2">No</p>

                                    </div>
                                    @if(isset($error['giveaccountaccess'][0]) && !empty($error['giveaccountaccess'][0]))
                                    <p class="form-text text-danger text-xs mb-1">
                                        {{$error['giveaccountaccess'][0]}}
                                    </p>
                                    @endif
                                </div>
                            </div>

                        <div class="row" id="authorisedaccessdiv" @if($old->giveaccountaccess=='1' || (isset($authoriseddata->giveaccountaccess) && $authoriseddata->giveaccountaccess=='1' && $old->giveaccountaccess!=='2')) @else style="display: none;" @endif>
                            <div class="col-12 col-sm-12 ">
                                <p class="text-sm text-primary font-weight-bolder">What kind of access would you like your Trusted Party to have?</p>
                            </div>
                            <div class="col-lg-12 col-sm-12  ">
                                <select name="authorised_access_type" id="authorised_access_type" class="select2 form-control select2" style="width: 100%;">
                                    <option value="" @if($old->authorised_access_type=='0'){{"selected"}}@endif>Select</option>
                                    <option value="1" @if($old->authorised_access_type=='1' || (isset($authoriseddata->authorised_access_type) && $authoriseddata->authorised_access_type=='1')){{"selected"}}@endif >They can not have any access to my account upon advising of my death, and at this time Evaheld can release my messages directly to their allocated recipients</option>

                                        <option value="2" @if($old->authorised_access_type=='2' || (isset($authoriseddata->authorised_access_type) && $authoriseddata->authorised_access_type=='2')){{"selected"}}@endif >They can not view any recipient details or messages, they are only allowed to give authorisation for my messages to be released to their allocated recipients directly by Evaheld</option>

                                    <option value="3" @if($old->authorised_access_type=='3' || (isset($authoriseddata->authorised_access_type) && $authoriseddata->authorised_access_type=='3')){{"selected"}}@endif >They can only view recipients and update their details as well as release their message/s, if the recipient is to receive a message that was selected as being able to be released by the Trusted Party</option>

                                        <option value="4" @if($old->authorised_access_type=='4' || (isset($authoriseddata->authorised_access_type) && $authoriseddata->authorised_access_type=='4')){{"selected"}}@endif >They can have full account access, except the ability to view messages and to see the recipients and messages for which i have chosen a direct link to be sent from Evaheld</option>

                                        <option value="5" @if($old->authorised_access_type=='5' || (isset($authoriseddata->authorised_access_type) && $authoriseddata->authorised_access_type=='5')){{"selected"}}@endif >They can have full account access, including viewing messages, except for the recipients and messages for which i have chosen a direct link to be sent from Evaheld</option>

                                        <option value="6" @if($old->authorised_access_type=='6' || (isset($authoriseddata->authorised_access_type) && $authoriseddata->authorised_access_type=='2')){{"selected"}}@endif >They can have full account access, including viewing all messages and all recipients, but they cannot delete any messages or recipients</option>

                                        <option value="7" @if($old->authorised_access_type=='7' || (isset($authoriseddata->authorised_access_type) && $authoriseddata->authorised_access_type=='7')){{"selected"}}@endif >They can have full account access and control, including adding or deleting recipients and messages</option>

                                    </select>
                                    @if(isset($error['authorised_access_type'][0]) && !empty($error['authorised_access_type'][0]))
                                    <p class="form-text text-danger text-xs mb-1">
                                        {{$error['authorised_access_type'][0]}}
                                    </p>
                                    @endif
                                </div>

                            <div class="row mt-3" id="aigcanemaildiv" @if(!empty($old->authorised_access_type) || (isset($authoriseddata->authorised_access_type) && !empty($authoriseddata->authorised_access_type))) @else style="display:none;" @endif>
                                <div class="col-12 col-sm-12 ">
                                    <p class="text-sm text-primary font-weight-bolder">Can Evaheld email your Trusted Party on your behalf?</p>
                                </div>

                                    <div class="col-lg-12 col-sm-12">
                                        <div class="form-check form-check-inline">

                                            <input class="aigcanemail @if(isset($error['aigcanemail'][0]) && !empty($error['aigcanemail'][0])) {{'is-invalid'}} @endif form-check-input" type="radio" name="aigcanemail" id="customRadio1" value="1" @if($old->aigcanemail=='1' || (isset($authoriseddata->aigcanemail) && $authoriseddata->aigcanemail=='1')) {{'checked'}}@endif>
                                            <p class="text-dark" for="customRadio1">Yes</p>

                                        </div>

                                        <div class="form-check form-check-inline">

                                            <input class="aigcanemail @if(isset($error['aigcanemail'][0]) && !empty($error['aigcanemail'][0])) {{'is-invalid'}} @endif form-check-input" type="radio" name="aigcanemail" id="customRadio2" value="2" @if($old->aigcanemail=='2' || (isset($authoriseddata->aigcanemail) && $authoriseddata->aigcanemail=='2')){{'checked'}}@endif>
                                            <p class="text-dark" for="customRadio2">No</p>

                                        </div>

                                        @if(isset($error['aigcanemail'][0]) && !empty($error['aigcanemail'][0]))
                                        <p class="form-text text-danger text-xs mb-1">
                                            {{$error['aigcanemail'][0]}}
                                        </p>
                                        @endif
                                    </div>
                                </div>


                            </div>

                        <div class="row" id="verificationprocessdiv" @if(($old->giveaccountaccess=='2' || ($old->giveaccountaccess=='1' && !empty($authoriseddata->authorised_access_type))) || (isset($authoriseddata->giveaccountaccess) && !empty($authoriseddata->giveaccountaccess))) @else style="display: none;" @endif>
                            <div class="col-lg-6 col-sm-12 mt-4">
                                <p class="text-sm text-primary font-weight-bolder">If your Trusted Party contacts us to advise us of your passing - but does not use the link provided link-what should we do? </p>
                            </div>

                                <div class="col-lg-6 col-sm-12 mt-4">
                                    <select name="verification_process_step_id" id="verification_process_step" class="select2 form-control @if(isset($error['verification_process_step_id'][0]) && !empty($error['verification_process_step_id'][0])) {{'is-invalid'}} @endif" style="width: 100%;">
                                        @if(isset($verification_process_step) && !empty($verification_process_step))
                                        <option value="">Select</option>
                                        @foreach($verification_process_step as $verification_process_stepdata)
                                        <option value="{{$verification_process_stepdata->verification_process_step_id}}" @if($old->verification_process_step_id==$verification_process_stepdata->verification_process_step_id || (isset($authoriseddata->verification_process_step_id) && $authoriseddata->verification_process_step_id==$verification_process_stepdata->verification_process_step_id)) {{'selected'}} @endif

                                            >{{$verification_process_stepdata->description}}</option>
                                        @endforeach
                                        @endif



                                    </select>

                                    @if(isset($error['verification_process_step_id'][0]) && !empty($error['verification_process_step_id'][0]))
                                    <p class="form-text text-danger text-xs mb-1">
                                        {{$error['verification_process_step_id'][0]}}
                                    </p>
                                    @endif
                                </div>
                            </div>

                        <div id="showverificationstep1" class="mt-4" @if(($old->verification_process_step_id=='1' ) || isset($authoriseddata->verification_process_step_id) && ($authoriseddata->verification_process_step_id=='1')) @else style="display: none;" @endif>
                            <p class="text-sm text-primary font-weight-bolder">Set up your Trusted Party’s verification password and don’t forget to give it to them!</p>
                            <div class="row">
                                <div class="col-12 col-sm-6 ">
                                    <label class="form-label">Create Password </label>
                                    <input placeholder="********" class="password form-control @if(isset($error['password'][0]) && !empty($error['password'][0])) is-invalid @endif" type="text" name="password" id="pwd" value="<?php echo isset($authoriseddata->password) && !empty($authoriseddata->password) ? $authoriseddata->password : $old->password; ?>">


                                        <div id="pwd_strength_wrap" class="arrow-top">
                                            <div id="passwordDescription" class="text-sm">Password not entered</div>
                                            <div id="passwordStrength" class="strength0"></div>
                                            <div id="pswd_info">
                                                <p class="text-muted text-xs mb-1">
                                                    Please follow this guide for a strong password:
                                                </p>
                                                <ul class="text-muted ps-4 mb-0 float-start">

                                                    <li>
                                                        <span class="mb-0 text-xs mx-auto" id="length">Min 6 characters</span>
                                                    </li>
                                                    <li>
                                                        <span class="mb-0 text-xs mx-auto" id="spchar">One special characters (@,$,#)</span>
                                                    </li>
                                                    <li>
                                                        <span class="mb-0 text-xs mx-auto" id="capital">One UpperCase Letter & small case Letters (Abcd)</span>
                                                    </li>
                                                    <li>
                                                        <span class="mb-0 text-xs mx-auto" id="pnum">One number (2 are recommended)</span>
                                                    </li>

                                                </ul>
                                            </div><!-- END pswd_info -->
                                        </div>

                                        @if(isset($error['password'][0]) && !empty($error['password'][0]))
                                        <p class="form-text text-danger text-xs mb-1">
                                            {{$error['password'][0]}}
                                        </p>
                                        @endif
                                    </div>

                                <div class="col-12 col-sm-6 ">
                                    <label class="form-label">Confirm Password </label>
                                    <input class="password_confirmation form-control @if(isset($error['password_confirmation'][0]) && !empty($error['password_confirmation'][0])) is-invalid @endif" type="text" name="password_confirmation" value="<?php echo isset($authoriseddata->password) && !empty($authoriseddata->password) ? $authoriseddata->password : $old->password_confirmation; ?>">
                                    @if(isset($error['password_confirmation'][0]) && !empty($error['password_confirmation'][0]))
                                    <p class="form-text text-danger text-xs mb-1">
                                        {{$error['password_confirmation'][0]}}
                                    </p>
                                    @endif
                                </div>
                                <div class="col-12 col-sm-6 ">
                                    <div id="progress-strength">
                                        <div id="progress-strength-bar-individual"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="showverificationstep2" class="row mt-4" @if(($old->verification_process_step_id=='2') || isset($authoriseddata->verification_process_step_id) && ($authoriseddata->verification_process_step_id=='2')) @else style="display: none;" @endif>
                            <p class="text-lg text-dark mb-0">Verification Questions</p>
                            <p class="text-sm text-primary font-weight-bolder mt-2">
                                Set up your recipient’s verification questions and don’t forget to give it to them!</p>
                            <div class="col-12 col-sm-6">
                                <label class="form-label">Verification Question 1</label>
                                <select name="verification_question_1" class="form-control @if(isset($error['verification_question_1'][0]) && !empty($error['verification_question_1'][0])) {{'is-invalid'}} @endif" style="width: 100%;">
                                    @if(isset($verification_questions) && !empty($verification_questions))
                                    <option value="">Please Select</option>
                                    @foreach($verification_questions as $verification_questionsdata)
                                    <option value="{{$verification_questionsdata->verification_question_id}}" @if($old->verification_question_1==$verification_questionsdata->verification_question_id || isset($authoriseddata->verification_question_1) && $authoriseddata->verification_question_1==$verification_questionsdata->verification_question_id){{'selected'}} @endif
                                        >{{$verification_questionsdata->description}}</option>
                                    @endforeach
                                    @endif
                                </select>
                                @if(isset($error['verification_question_1'][0]) && !empty($error['verification_question_1'][0]))
                                <p class="form-text text-danger text-xs mb-1">
                                    {{$error['verification_question_1'][0]}}
                                </p>
                                @endif
                            </div>

                            <div class="col-12 col-sm-6">
                                <label class="form-label">Answer Question 1</label>
                                <input class="form-control @if(isset($error['verification_answer_1'][0]) && !empty($error['verification_answer_1'][0])) {{'is-invalid'}} @endif" type="text" name="verification_answer_1" value="<?php echo isset($authoriseddata->verification_answer_1) && !empty($authoriseddata->verification_answer_1) ? $authoriseddata->verification_answer_1 : $old->verification_answer_1; ?>">
                                @if(isset($error['verification_answer_1'][0]) && !empty($error['verification_answer_1'][0]))
                                <p class="form-text text-danger text-xs mb-1">
                                    {{$error['verification_answer_1'][0]}}
                                </p>
                                @endif
                            </div>
                            <div class="col-12 col-sm-6">
                                <label class="form-label">Verification Question 2</label>
                                <select name="verification_question_2" class="form-control @if(isset($error['verification_question_2'][0]) && !empty($error['verification_question_2'][0])) {{'is-invalid'}} @endif" style="width: 100%;">
                                    @if(isset($verification_questions) && !empty($verification_questions))
                                    <option value="">Please Select</option>
                                    @foreach($verification_questions as $verification_questionsdata2)
                                    <option value="{{$verification_questionsdata2->verification_question_id}}" @if($old->verification_question_2==$verification_questionsdata2->verification_question_id || isset($authoriseddata->verification_question_2) && $authoriseddata->verification_question_2==$verification_questionsdata2->verification_question_id){{'selected'}} @endif>{{$verification_questionsdata2->description}}</option>
                                    @endforeach
                                    @endif
                                </select>
                                @if(isset($error['verification_question_2'][0]) && !empty($error['verification_question_2'][0]))
                                <p class="form-text text-danger text-xs mb-1">
                                    {{$error['verification_question_2'][0]}}
                                </p>
                                @endif
                            </div>
                            <div class="col-12 col-sm-6">
                                <label class="form-label">Answer Question 2</label>
                                <input class="form-control @if(isset($error['verification_answer_2'][0]) && !empty($error['verification_answer_2'][0])) {{'is-invalid'}} @endif" type="text" name="verification_answer_2" value="<?php echo isset($authoriseddata->verification_answer_2) && !empty($authoriseddata->verification_answer_2) ? $authoriseddata->verification_answer_2 : $old->verification_answer_2; ?>">
                                @if(isset($error['verification_answer_2'][0]) && !empty($error['verification_answer_2'][0]))
                                <p class="form-text text-danger text-xs mb-1">
                                    {{$error['verification_answer_2'][0]}}
                                </p>
                                @endif
                            </div>
                            <div class="col-12 col-sm-6">
                                <label class="form-label">Verification Question 3</label>
                                <select name="verification_question_3" class="form-control @if(isset($error['verification_question_3'][0]) && !empty($error['verification_question_3'][0])) {{'is-invalid'}} @endif" style="width: 100%;">
                                    @if(isset($verification_questions) && !empty($verification_questions))
                                    <option value="">Please Select</option>
                                    @foreach($verification_questions as $verification_questionsdata3)
                                    <option value="{{$verification_questionsdata3->verification_question_id}}" @if($old->verification_question_3==$verification_questionsdata3->verification_question_id || isset($authoriseddata->verification_question_3) && $authoriseddata->verification_question_3==$verification_questionsdata3->verification_question_id){{'selected'}} @endif>{{$verification_questionsdata3->description}}</option>
                                    @endforeach
                                    @endif
                                </select>
                                @if(isset($error['verification_question_3'][0]) && !empty($error['verification_question_3'][0]))
                                <p class="form-text text-danger text-xs mb-1">
                                    {{$error['verification_question_3'][0]}}
                                </p>
                                @endif
                            </div>
                            <div class="col-12 col-sm-6">
                                <label class="form-label">Answer Question 3</label>
                                <input class="form-control @if(isset($error['verification_answer_3'][0]) && !empty($error['verification_answer_3'][0])) {{'is-invalid'}} @endif" type="text" name="verification_answer_3" value="<?php echo isset($authoriseddata->verification_answer_3) && !empty($authoriseddata->verification_answer_3) ? $authoriseddata->verification_answer_3 : $old->verification_answer_3; ?>">
                                @if(isset($error['verification_answer_3'][0]) && !empty($error['verification_answer_3'][0]))
                                <p class="form-text text-danger text-xs mb-1">
                                    {{$error['verification_answer_3'][0]}}
                                </p>
                                @endif
                            </div>
                        </div>
                        <div id="verification_to_releasemessagediv" @if(!empty($old->verification_to_releasemessage) || !empty($authoriseddata->verification_to_releasemessage)) @else style="display: none;" @endif >
                            <div class="col-lg-12 col-sm-12 mt-4">
                                <p class="text-sm text-primary font-weight-bolder"> If my Trusted Party is not verified and cannot pass any of the Verification Questions, Evaheld has permission to contact the Department of Births, Deaths and Marriages in the applicable state and country, to confirm my death using my name and date of birth. If my death is confirmed, I agree that Evaheld can release my messages as per the preferences I selected for each message.</p>
                            </div>
                            <div class="col-lg-12 col-sm-12">
                                <div class="form-check form-check-inline">

                                    <input class="verification_to_releasemessage @if(isset($error['verification_to_releasemessage'][0]) && !empty($error['verification_to_releasemessage'][0])) {{'is-invalid'}} @endif form-check-input" type="radio" name="verification_to_releasemessage" id="customRadio1" value="1" @if($old->verification_to_releasemessage=='1' || (isset($authoriseddata->verification_to_releasemessage) && $authoriseddata->verification_to_releasemessage=='1')) {{'checked'}}@endif>
                                    <p class="text-dark" for="customRadio1">Yes</p>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="verification_to_releasemessage @if(isset($error['verification_to_releasemessage'][0]) && !empty($error['verification_to_releasemessage'][0])) {{'is-invalid'}} @endif form-check-input" type="radio" name="verification_to_releasemessage" id="customRadio2" value="2" @if($old->verification_to_releasemessage=='2' || (isset($authoriseddata->verification_to_releasemessage) && $authoriseddata->verification_to_releasemessage=='2')){{'checked'}}@endif>
                                    <p class="text-dark" for="customRadio2">No</p>
                                </div>
                                @if(isset($error['verification_to_releasemessage'][0]) && !empty($error['verification_to_releasemessage'][0]))
                                <p class="form-text text-danger text-xs mb-1">
                                    {{$error['verification_to_releasemessage'][0]}}
                                </p>
                                @endif
                            </div>
                        </div>
                        <div class="col-12 mt-4">
                            <div class="button-row float-end d-flex mt-4">
                                <button class="submit-button btn bg-primary btn-sm mb-0 me-2" type="button" name="button">Save</button>
                                <a data-route="{{route('my-authorised-3p-access')}}" class="link-handle btn btn-outline-dark btn-sm mb-0" type="button" name="button">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

<script src="{{asset('assets/js/js-address-lookup.js')}}?v=1.0.3"></script>
<script type="text/javascript">
    $(document).on('click', '.show-manual', function(e) {
        e.preventDefault();
        $(document).find('.show-manual-div').show();
        $(document).find('#manual-address').val(1);
    });


    $('.nominated_authorised_person_access').change(function() {
        var access = $('input[name="nominated_authorised_person_access"]:checked').val();
        if (access == 1) {

            swal({
                text: "I confirm that I would like to appoint a Trusted Party to advise Evaheld of my death and that I am responsible for providing their correct contact details",
                icon: "warning",
                buttons: {
                    confirm: {
                        text: 'Acknowledge',
                        className: ' text-white bg-primary'
                    },
                    cancel: "Take me back"
                },
            }).then((will) => {

                $('#showstep2').hide();
                


                if (will) {
                    $('#showstep2').show();
                    $('#showbuttondiv').hide();

                } else {

                    $(".nominated_authorised_person_access").prop('checked', false);
                }

            });
        } else {
           

            swal({
                text: "I can confirm that no one can advise Evaheld of my death and that I will be using Evaheld’s Wellbeing System to maintain my account until my death",
                icon: "warning",
                buttons: {
                    confirm: {
                        text: 'Acknowledge',
                        className: ' text-white bg-primary'
                    },
                    cancel: "Take me back"
                },
            }).then((will) => {

                if (will) {

                    $('#showstep2').hide();
                    $('#showdiv3').hide();

                    $('#showbuttondiv').show();
                    $('#authorised-profile').hide();

                } else {

                    $(".nominated_authorised_person_access").prop('checked', false);
                }

            });
        }

    });

    $('.giveaccountaccess').change(function() {
        var access = $('input[name="giveaccountaccess"]:checked').val();
        if (access == 1) {
            clear_form_elements('#verificationprocessdiv');
            swal({
                text: "I confirm that my Trusted Party can have access to my account after my death and that I am responsible for choosing their level of access to my account before my death",
                icon: "warning",
                buttons: {
                    confirm: {
                        text: 'Acknowledge',
                        className: ' text-white bg-primary'
                    },
                    cancel: "Take me back"
                },
            }).then((will) => {

                $('#verificationprocessdiv').hide()
                $('#authorisedaccessdiv').hide();

                if (will) {

                    $('#authorisedaccessdiv').show();
                } else {

                    $(".giveaccountaccess").prop('checked', false);

                }

            });
        } else {
            

            swal({
                text: "I confirm that no account access or details are to be provided to my Trusted Party and that they can only advise you of my death. At that time, Evaheld can release my messages directly to their allocated recipients.",
                icon: "warning",
                buttons: {
                    confirm: {
                        text: 'Acknowledge',
                        className: ' text-white bg-primary'
                    },
                    cancel: "Take me back"
                },
            }).then((will) => {

                if (will) {
                    
                    $('#verificationprocessdiv').show();
                      $('#authorisedaccessdiv').hide();
                    $('#aigcanemaildiv').hide();

                    clear_form_elements('#authorisedaccessdiv');
                    clear_form_elements('#aigcanemaildiv');
                    clear_form_elements('#verificationprocessdiv');
                    clear_form_elements('#showverificationstep1');
                    clear_form_elements('#showverificationstep2');
                    clear_form_elements('#verification_to_releasemessage');

                } else {

                    $(".giveaccountaccess").prop('checked', false);


                }

            });
        }

    });


    $('.aigcanemail').change(function() {
        var access = $('input[name="aigcanemail"]:checked').val();
        if (access == 1) {
            swal({
                text: "I confirm that my Trusted Party can be emailed a request on my behalf to be my Trusted Party. I also confirm that if they do not accept their appointment, I will automatically be enrolled in the Evaheld Check-in process, until I select a Trusted Party that accepts my appointment",
                icon: "warning",
                buttons: {
                    confirm: {
                        text: 'Acknowledge',
                        className: ' text-white bg-primary'
                    },
                    cancel: "Take me back"
                },
            }).then((will) => {

                $('#verificationprocessdiv').hide();

                if (will) {
                    $('#verificationprocessdiv').show();
                } else {
                    $(".aigcanemail").prop('checked', false);
                }

            });
        } else {
            $('#verificationprocessdiv').hide();

            swal({
                text: "I confirm that I will provide my Trusted Party with the direct link to verify themselves",
                icon: "warning",
                buttons: {
                    confirm: {
                        text: 'Acknowledge',
                        className: ' text-white bg-primary'
                    },
                    cancel: "Take me back"
                },
            }).then((will) => {

                if (will) {
                    $('#verificationprocessdiv').show();

                } else {

                    $(".aigcanemail").prop('checked', false);


                }

            });
        }

    });




    function copyToClipboarddata(text) {
        var sampleTextarea = document.createElement("textarea");
        document.body.appendChild(sampleTextarea);
        sampleTextarea.value = text; //save main text in it
        sampleTextarea.select(); //select textarea contenrs
        document.execCommand("copy");
        document.body.removeChild(sampleTextarea);
    }

    function copyToClipboard(eleme) {
        var copyText = document.getElementById("copy-link");
        copyToClipboarddata(copyText.value);
    }

    $('#verification_process_step').change(function() {

        $('#showverificationstep1').hide();
        $('#showverificationstep2').hide();

        stepvalue = $('#verification_process_step option:selected').val();

        if (stepvalue) {

            
            $('#verification_to_releasemessagediv').show();

            if (stepvalue == '3') {

                swal({
                    text: "I acknowledge that it is my responsibility to tell my Trusted Party that upon my death, if they can’t use the link provided, they must provide my full name, date of death, location of death and my death certificate to Evaheld via the email support@evaheld.com .",
                    icon: "warning",
                    buttons: {
                        confirm: {
                            text: 'Acknowledge',
                            className: ' text-white bg-primary'
                        },
                        cancel: "Take me back"
                    },
                }).then((will) => {

                    if (will) {
                        $('#verificationprocessdiv').show();
                        clear_form_elements('#showverificationstep1');
                        clear_form_elements('#showverificationstep2');
                    } else {

                        $(".aigcanemail").prop('checked', false);


                    }

                });

            }

            if (stepvalue == '1') {
                $('#showverificationstep1').show();
                clear_form_elements('#showverificationstep2');
            }

            if (stepvalue == '2') {
                $('#showverificationstep2').show();
                clear_form_elements('#showverificationstep1');
            }

        } else {
            $('#verification_to_releasemessagediv').hide();
        }


    });

    $('#relationship').change(function() {



        if ($('#relationship option:selected').val() == '18') {

            $('.otherrelationship').show();

        } else {
            $('.otherrelationship').hide();
        }

    });


    $('.islawyer').change(function() {
        var value = $('input[name="islawyer"]:checked').val();

        if (value == '1') {

            $('.organisation-div').show();

        } else {
            clear_form_elements('.organisation-div');
            $('.organisation-div').hide();
        }

    });

    $('#authorised_access_type').change(function() {

        var value = $('#authorised_access_type option:selected').val();

        if (value) {
            $('#aigcanemaildiv').show();
        } else {
            $('#aigcanemaildiv').hide();
        }

    });

    $('.show-update-form').click(function(e) {

        e.preventDefault();
        $('#authorised-profile').hide();
        $('#authorised-profile-details').show();



    });

    function clear_form_elements(id) {
        $(id).find(':input').each(function() {
            switch (this.type) {
                case 'password':
                    $(this).val('');
                    break;
                case 'text':
                    $(this).val('');
                    break;
                case 'textarea':
                    $(this).val('');
                    break;
                case 'file':
                case 'select-one':
                    $(this).val('').change();
                    break;
                case 'select-multiple':
                case 'date':
                    $(this).val('');
                    break;
                case 'number':
                    $(this).val('');
                    break;
                case 'tel':
                    $(this).val('');
                    break;
                case 'email':
                    $(this).val('');
                    break;
                case 'checkbox':

                case 'radio':
                    this.checked = false;
                    break;
            }
        });
    }
</script>
@endsection