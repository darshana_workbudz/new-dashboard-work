@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Preferences')

@section('headercommon')
<x-main.header icon="shield_task" title="Recipients verification process" subtitle="Please Select one of the below option to Update" />
@endsection
@section('content')


<div class="row">
  <div class="card mt-1">
    <div class="card-body mt-0 pt-0">
      <div class="nav-wrapper position-relative end-0">
        <ul class="nav nav-pills nav-fill" role="tablist">
          <li class="nav-item">
            <a data-route="{{route('my-recipients-verification-process')}}" class="link-handle nav-link nav-link-switch mb-0 px-0 py-1 @if($state=='1'){{'active'}}@endif">
              Recipients verification process
            </a>
          </li>
          <li class="nav-item">
            <a data-route="{{route('my-checkin-process')}}" class="link-handle nav-link nav-link-switch mb-0 px-0 py-1 @if($state=='2'){{'active'}}@endif" >
              Users check-In process
            </a>
          </li>
          <li class="nav-item">
            <a data-route="{{route('my-authorised-3p-access')}}" class="link-handle nav-link nav-link-switch mb-0 px-0 py-1 @if($state=='3'){{'active'}}@endif" >
              Trusted Party access
            </a>
          </li>
          
        </ul>
      </div>
    </div>
  </div>

  <div class="card mt-1">
    <div class="card-header">
      <div class="d-lg-flex">
        <div>
          <h5 class="mb-0 text-secondary">Recipients Verification process</h5>
          <p class="text-sm text-dark mb-0">
           here you can manage How the Recipients will recieve messages and there verification process.
          </p>
        </div>

      </div>
    </div>
    <div class="card-body">
      <form action="{{route('save-checkin-process')}}" method="post">
        <div class="row">

          <div class="col-12 mt-4">
             <p class="text-sm text-primary font-weight-bolder">Choose a reciepient</p>
           </div>
          <div class="col-6">

            <select id="user_recipient_id" name="user_recipient_id" class="form-control @if(isset($error['user_recipient_id'][0]) && !empty($error['user_recipient_id'][0])) {{'is-invalid'}} @endif" style="width: 100%;">
              @if(isset($recipients) && !empty($recipients))
              @foreach($recipients as $recipientsdata)
              <option value="{{$recipientsdata->user_recipient_id }}" 

                @if($old->user_recipient_id==$recipientsdata->user_recipient_id  || (isset($checkinsetting->user_recipient_id) && $checkinsetting->user_recipient_id==$recipientsdata->user_recipient_id ))
                {{'selected'}} @endif


                >{{$recipientsdata->first_name}} {{$recipientsdata->last_name}}</option>
              @endforeach
              @endif

             </select>

           </div>
         </div>

         <div class="row refresh-card" >
         </div>


          

        </div>
      </form>
    </div>

  </div>
</div>
<script type="text/javascript">

  $( ".nav-link-switch" ).click(function() {

    $('.nav-link-switch').removeClass('active');
    $(this).addClass('active');


  });

  $( "#user_recipient_id" ).change(function() {

    var user_recipient_id=$(this).val();

    if(user_recipient_id)
    {
          $.ajax({
            url: route + '?ajax=true',
            dataType: 'json',
            type: 'POST',
            data: {user_recipient_id:user_recipient_id},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
              jQuery(document).find('.loadingOverlay').show();
              jQuery(document).find('.page-progress-bar').show();
              jQuery(document).find('.loader').show();
          },
          success: function(response) {

            if(response.status=='success')
            {

            }
            
          },
          error: function(xhr) { // if error occured
              alert("Error occured.please try again");
          },
          complete: function() {
              
              jQuery(document).find('.loadingOverlay').hide();
              jQuery(document).find('.page-progress-bar').hide();
              jQuery(document).find('.loader').hide();
  
          },

        });

    }

  });

</script>
@endsection