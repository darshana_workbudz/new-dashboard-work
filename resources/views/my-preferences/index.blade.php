@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Preferences')

@section('headercommon')
<x-main.header 
  icon="shield_task" 
  title="My Preferences" 
  subtitle="Please select My Wellness System or My Trusted Party to update your preferences"
/>
@endsection
@section('content')

<div class="container-fluid ">
        <section class="px-1 py-2">
          <div class="row mt-4">

            <!-- My Wellbeing System Preferences -->
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
              <a data-route="{{route('my-checkin-process')}}" class="link-handle">
                <div class="card h-100 bg-gray-100 shadow-lg border-dark border-radius-xl p-4">
                  <div class="info-horizontal">
                    <div class="icon">
                      <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                        <g id="Group_1867" data-name="Group 1867" transform="translate(-16342 -14028)">
                          <rect id="Rectangle_648" data-name="Rectangle 648" width="54.083" height="55" rx="16" transform="translate(16342 14028)" fill="#f3eef6"></rect>
                          <path id="Path_3903" data-name="Path 3903" d="M15.668,2a3.59,3.59,0,1,0,3.59,3.59A3.59,3.59,0,0,0,15.668,2ZM27.651,8.486a2.887,2.887,0,0,0-3.751-1.8l-2.308.862a1.077,1.077,0,0,0-.6.547,5.868,5.868,0,0,1-10.607.014,1.077,1.077,0,0,0-.6-.546L7.453,6.688a2.882,2.882,0,0,0-2.1,5.365l4.573,1.881v4.521l-2.832,8.2a2.81,2.81,0,1,0,5.289,1.9l2.651-7.12a.689.689,0,0,1,1.293,0l2.7,7.194a2.787,2.787,0,1,0,5.245-1.889l-2.859-8.3v-4.5L26,12.061A2.887,2.887,0,0,0,27.651,8.486Z" transform="translate(16353.41 14039.002)" fill="#662d91"></path>
                        </g>
                      </svg>
                    </div>
                    <div class="description ps-5">
                      <h5>My Wellbeing System Preferences</h5>
                      <span class="text-primary icon-move-right">Manage my preferences
                        <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                      </span>
                      <br/>
                      @if($default_prefrence=='1')
                      <span class="badge bg-success mt-2 text-capitalize">Currently active</span>
                      @endif
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <!-- Nominate a Trusted Third-Party -->
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
              <a data-route="{{route('my-authorised-3p-access')}}" class="link-handle">
                <div class="card h-100 bg-gray-100 shadow-lg border-dark border-radius-xl p-4">
                  <div class="info-horizontal">
                    <div class="icon">
                      <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                        <g id="Group_1864" data-name="Group 1864" transform="translate(-16401 -14025)">
                          <rect id="Rectangle_649" data-name="Rectangle 649" width="54.083" height="55" rx="16" transform="translate(16401 14025)" fill="#f3eef6"></rect>
                          <path id="Path_3904" data-name="Path 3904" d="M23.261,15.716a7.545,7.545,0,1,1-7.545,7.545,7.545,7.545,0,0,1,7.545-7.545Zm-7.514,2.743a8.918,8.918,0,0,0,.782,10.646,18.051,18.051,0,0,1-3.556.329c-4.691,0-8.154-1.471-10.269-4.44A3.772,3.772,0,0,1,2,22.807V21.545a3.085,3.085,0,0,1,3.085-3.086H15.748Zm3.885,4.316a.686.686,0,1,0-.97.97L21.4,26.488a.686.686,0,0,0,.97,0L27.861,21a.686.686,0,1,0-.97-.97l-5,5ZM12.972,2A6.859,6.859,0,1,1,6.114,8.863,6.859,6.859,0,0,1,12.972,2Z" transform="translate(16411.637 14036.096)" fill="#662d91"></path>
                        </g>
                      </svg>
                    </div>
                    <div class="description ps-5">
                      <h5>My Trusted Third-Party</h5>
                      <span class="text-primary icon-move-right">Manage my preferences
                        <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                      </span>
                      <br/>
                      @if($default_prefrence=='2')
                      <span class="badge bg-success mt-2 text-capitalize">Currently active</span>
                      @endif
                    </div>
                  </div>
                </div>
              </a>
            </div>

          </div>
        </section>
      </div>


<!-- <div class="container-fluid ">
  
<section class="px-1 py-2">
       
<div class="row mt-4">


<div class="col-lg-6 col-md-6 d-flex flex-column mt-3 border-radius-lg">
<a data-route="{{route('my-checkin-process')}}" class="link-handle">
<div class="card h-100 card-background align-items-start">
  <div class="full-background bg-gradient-primary-new"></div>
  <div class="card-body z-index-3">
   <span class="text-end float-end"> @if($default_prefrence=='1') <i class="fa fa-check-circle-o fa-2x text-success" aria-hidden="true"></i> @else <i class="fa fa-times-circle-o fa-2x text-primary" aria-hidden="true"></i> @endif</span>

  </div>
  <div class="card-footer pb-3 pt-2 z-index-3">

 <h6 class="text-white mb-1">My Check-in Process </h6> 
  </div>
  <span class="mask bg-gradient-info border-radius-xl z-index-2 opacity-6"></span>
   
</div>
</a>
</div>

<div class="col-lg-6 col-md-6 d-flex flex-column mt-3 border-radius-lg">
<a data-route="{{route('my-authorised-3p-access')}}" class="link-handle">
<div class="card h-100 card-background align-items-start">
  <div class="full-background bg-gradient-primary-new"></div>
  <div class="card-body z-index-3">
   <span class="text-end float-end"> @if($default_prefrence=='2') <i class="fa fa-check-circle-o fa-2x text-success" aria-hidden="true"></i> @else <i class="fa fa-times-circle-o fa-2x text-danger" aria-hidden="true"></i> @endif</span>

  </div>
  <div class="card-footer pb-3 pt-2 z-index-3">
  	 
    <h6 class="text-white mb-1">My Authorised 3rd party</h6> 

  </div>
  <span class="mask bg-gradient-info border-radius-xl z-index-2 opacity-6"></span>

</div>
</a>
</div>


</div>

</section>

</div> -->
@endsection