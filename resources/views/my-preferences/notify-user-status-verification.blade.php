@extends(isset($isajax) && $isajax==true ? 'layouts-frontend.ajax' : 'layouts-frontend.master')
@section('content')
 
  <header class="header-2">
    <div class="page-header min-vh-50 relative" style="background-image: url('{{asset('assets/img/cover/grandparents-laughing-with-grandchildren.jpg')}}'); background-size:cover;">
      <span class="mask bg-gradient-primary"></span>
      <div class="container">
        <div class="row">
          <div class="col-lg-7 text-center mx-auto">
            <a class="navbar-brand font-weight-bolder link-handle" data-route="{{route('home')}}" rel="tooltip" data-placement="bottom" target="_blank">
                <img src="{{asset('assets/img/evaheld-logo-white.png')}}" style="width:45%" alt="Evaheld logo" />
            </a>
            
          </div>
        </div>
      </div>
      <div class="position-absolute w-100 z-index-1 bottom-0">
        <svg class="waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 40" preserveAspectRatio="none" shape-rendering="auto">
          <defs>
            <path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
          </defs>
          <g class="moving-waves">
            <use xlink:href="#gentle-wave" x="48" y="-1" fill="rgba(255,255,255,0.40"></use>
            <use xlink:href="#gentle-wave" x="48" y="3" fill="rgba(255,255,255,0.35)"></use>
            <use xlink:href="#gentle-wave" x="48" y="5" fill="rgba(255,255,255,0.25)"></use>
            <use xlink:href="#gentle-wave" x="48" y="8" fill="rgba(255,255,255,0.20)"></use>
            <use xlink:href="#gentle-wave" x="48" y="13" fill="rgba(255,255,255,0.15)"></use>
            <use xlink:href="#gentle-wave" x="48" y="16" fill="rgba(255,255,255,0.95"></use>
          </g>
        </svg>
      </div>
    </div>
  </header>
    <div class="container">
     

      <div class="row blur shadow-blur mt-n6 border-radius-md pb-4 p-3 mx-sm-0 mx-1 position-relative z-index-3">
        
        <div class="card-body">

          <!-- @if($userdata->verification_process_step_id=='3' || $showform==true || true) -->

          <h5 class="text-primary text-gradient text-center font-weight-bolder mt-3 mb-3">Report your friends account to be Memorialize</h5>
          <p class="text-center text-sm">To report your friends account to be memorialized, please contact us. We require proof of death, such as a death certificate,link to an obituary or news article, to memorialize an account</p>


        <form action="{{route('user-report.update-status',['id'=>$userdata->user_id])}}" method="POST">
          @csrf
          <input type="hidden" name="code" value="{{$code}}">
          <span class="text-sm"><b class="text-purple">Email</b> : {{$userdata->email}}</span><br>
          <span class="text-sm"><b class="text-purple">Name</b> : {{$userdata->first_name}} {{$userdata->last_name}}</span>
          <div class="row mt-4">
            <div class="col-12 col-sm-6">
              <label>Advise of Death</label>
              <div id="datepicker" class="input-group-alternative date custom-datepicker" data-date-format="dd-mm-yyyy">
                <input required type="text" id="passed_date-date" value="<?php echo isset($userdata->passed_date) && !empty($userdata->passed_date) ? date('d-m-Y', strtotime($userdata->passed_date)) : '' ?>" name="passed_date" class=" date-validation form-control  @if(isset($error['passed_date'][0]) && !empty($error['passed_date'][0])) is-invalid @endif" placeholder="dd-mm-yyyy" />
                <span class="input-group-addon">
                </span>
              </div>
              @if(isset($error['passed_date'][0]) && !empty($error['passed_date'][0]))
              <p class="form-text text-danger text-xs mb-1">
                {{$error['passed_date'][0]}}
              </p>
              @endif
            </div>
            
            

          </div>
          <button class="btn bg-primary mb-0 submit-button mt-3 mb-3" type="button"> Save & Send Notification to  Evaheld </button>
        </form>
        <div class="row mx-auto">
              <label>Upload Documents</label>


                <form action="{{route('user-report.upload-files',$userdata->user_id)}}" class="form-control dropzone mt-2" id="dropzone">
                   <input type="hidden" name="type" value="">
                  @csrf
                  <div class="dz-message" data-dz-message><span><strong>Drop files here</strong> OR <strong>Upload files from my device</strong> </span></div>
                </form>
                
                

           </div>
        <div class="row mx-auto">
            
            @if(isset($documents) && count($documents)>0)
            <label class="p-3">Uploaded Documents list</label>
            @foreach($documents as $document)
            <div class="col-lg-2 col-sm-6 text-center shadow-lg border-radius-lg mx-2 p-2 mt-4 ">
              
              @if($document->capture_type=='uploaded' && $document->filetype=='pdf')
                
                <a href="{{route('user-report.stream-files', ['id'=>$userdata->user_id,'filename'=>$document->uploadedfilename])}}" class="text-center" target="blank">
                
                  <i class="fa fa-file text-purple p-2 mt-4" style="font-size: 50px" aria-hidden="true" ></i>
               
                </a>
              
              
              
              @else
              <a href="{{route('user-report.stream-files', ['id'=>$userdata->user_id,'filename'=>$document->uploadedfilename])}}" target="blank" >
              <img  src="{{route('user-report.stream-files',['id'=>$userdata->user_id,'filename'=>$document->uploadedfilename])}}" style="width: 95px" class="p-2">
              </a>
              
              @endif
              <p class="text-xs">{{$document->title}}</p>
            
            </div>
            @endforeach
            @endif
          </div>
        

          <!-- @else
          @if(isset($userdata->verification_process_step_id) && !empty($userdata->verification_process_step_id))
          <form action="{{route('notify-user-status-verification-post',$code)}}" method="Post">
            @csrf
            <input type="hidden" name="code" value="{{$code}}">
            
            
              <h3 class="text-primary text-gradient text-center font-weight-bolder mt-3 mb-3">Welcome, we are so glad you're here </h3>
              <p class="text-center h6"> Please Complete the Verification Process </p>
              

              @if($userdata->verification_process_step_id=='1')

                  <div class="row">
                  <div class="col-12 col-sm-6 ">
                  <label> Password  Verification</label>
                  <input class="password form-control @if(isset($error['password'][0]) && !empty($error['password'][0])) is-invalid @endif" type="text" name="password" id="pwd"  value="">
                  @if(isset($error['password'][0]) && !empty($error['password'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['password'][0]}}
                  </p>
                  @endif
                </div>

              
                
                </div>

              @endif


              @if($userdata->verification_process_step_id=='2')
              <div id="showverificationstep2" class="row  mt-4" >  
               <p class="text-sm text-primary">
               Please Complete the verification Process </p>

               <div class="col-12 col-sm-6">
                 <label>Verification Question 1</label>
                 <select disabled name="verification_question_1" class="form-control @if(isset($error['verification_question_1'][0]) && !empty($error['verification_question_1'][0])) {{'is-invalid'}} @endif" style="width: 100%;">
                  @if(isset($verification_questions) && !empty($verification_questions))
                  <option value="">Please Select</option>
                  @foreach($verification_questions as $verification_questionsdata)
                  <option value="{{$verification_questionsdata->verification_question_id}}" 
                    @if($verification_question_1==$verification_questionsdata->verification_question_id){{'selected'}} @endif
                    >{{$verification_questionsdata->description}}</option>
                    @endforeach
                    @endif
                  </select>
                  @if(isset($error['verification_question_1'][0]) && !empty($error['verification_question_1'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['verification_question_1'][0]}}
                  </p>
                  @endif
                </div>

                <div class="col-12 col-sm-6">
                  <label>Answer Question 1</label>
                  <input class="form-control @if(isset($error['verification_answer_1'][0]) && !empty($error['verification_answer_1'][0])) {{'is-invalid'}} @endif" type="text" name="verification_answer_1" 
                 
                  >
                  @if(isset($error['verification_answer_1'][0]) && !empty($error['verification_answer_1'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['verification_answer_1'][0]}}
                  </p>
                  @endif
                </div>

                <div class="col-12 col-sm-6">
                 <label>Verification Question 2</label>
                 <select disabled name="verification_question_2" class="form-control @if(isset($error['verification_question_2'][0]) && !empty($error['verification_question_2'][0])) {{'is-invalid'}} @endif" style="width: 100%;" >
                  @if(isset($verification_questions) && !empty($verification_questions))
                  <option value="">Please Select</option>
                  @foreach($verification_questions as $verification_questionsdata2)
                  <option 
                    @if($verification_question_2==$verification_questionsdata2->verification_question_id)
                    {{'selected'}} @endif> {{$verification_questionsdata2->description}}</option>
                    @endforeach
                    @endif
                  </select>
                  @if(isset($error['verification_question_2'][0]) && !empty($error['verification_question_2'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['verification_question_2'][0]}}
                  </p>
                  @endif
                </div>

                <div class="col-12 col-sm-6">
                  <label>Answer Question 2</label>
                  <input class="form-control @if(isset($error['verification_answer_2'][0]) && !empty($error['verification_answer_2'][0])) {{'is-invalid'}} @endif" type="text" name="verification_answer_2" >
                  @if(isset($error['verification_answer_2'][0]) && !empty($error['verification_answer_2'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['verification_answer_2'][0]}}
                  </p>
                  @endif
                </div>

                <div class="col-12 col-sm-6">
                 <label>Verification Question 3</label>
                 <select disabled name="verification_question_3" class="form-control @if(isset($error['verification_question_3'][0]) && !empty($error['verification_question_3'][0])) {{'is-invalid'}} @endif" style="width: 100%;">
                  @if(isset($verification_questions) && !empty($verification_questions))
                  <option value="">Please Select</option>
                  @foreach($verification_questions as $verification_questionsdata3)
                  <option value="{{$verification_questionsdata3->verification_question_id}}"  @if($verification_question_3==$verification_questionsdata3->verification_question_id)
                    {{'selected'}} @endif>{{$verification_questionsdata3->description}}</option>
                    @endforeach
                    @endif
                  </select>
                  @if(isset($error['verification_question_3'][0]) && !empty($error['verification_question_3'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['verification_question_3'][0]}}
                  </p>
                  @endif
                </div>

                <div class="col-12 col-sm-6">
                  <label>Answer Question 3</label>
                  <input class="form-control @if(isset($error['verification_answer_3'][0]) && !empty($error['verification_answer_3'][0])) {{'is-invalid'}} @endif" type="text" name="verification_answer_3"   >
                  @if(isset($error['verification_answer_3'][0]) && !empty($error['verification_answer_3'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['verification_answer_3'][0]}}
                  </p>
                  @endif
                </div>
              </div>
              @endif

              <div class="text-center">
          <button type="submit" class="btn btn-lg bg-purple text-white w-100 mt-5 mb-0">Confirm Verification  </button>
           </div>
         </form>
            
           
          @endif

          @endif -->
          

        </div>
      </div>
    </div>


<script type="text/javascript">
  var gallery="{{route('user-report.get-uploaded-files',$userdata->user_id)}}";
  var delete_url="{{route('user-report.delete-files',$userdata->user_id)}}"
  var routetoredirect="{{$redirecturl}}"

</script>  
<script type="text/javascript">
  var startDate = new Date();
  var endDate = new Date();
  

  $('body').find(".custom-datepicker").datepicker({ 
    autoclose: true, 
    todayHighlight: true,
    format: 'dd/mm/yyyy',
    startDate: '01-01-1920',
    endDate: endDate
  });
</script>

<script src="{{asset('assets/js/plugins/quill.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/dropzone.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/dropzone-script.js')}}"></script>
  <script type="text/javascript">

  
  Dropzone.discover();

  function LoadURL(obj,route,id)
  {
                var refreshdiv=$('body').find('.refresh-div');
                  jQuery.ajax({
                    url:routetoredirect+'?ajax=true',
                    method: 'GET',
                    dataType: "json",
                    beforeSend: function() {
                        AjaxbeforeSend();
                    },
                  success: function(response) {

                       if(response.event=='refresh')
                        {
                          refreshdiv.html(response.html);
                          routetoredirect=removeParam('ajax',routetoredirect);
                          routetoredirect=removeParam('_token',routetoredirect);
                          window.history.pushState('page',"After I Go",routetoredirect);
                          swal({

                          title: "Well done!",  
                          text: "Documents Uploaded Successfully",
                          icon: "success",
                          buttons: {
                              confirm: {
                                  text: 'ok',
                            
                              }
                          },
                          });
                        }
                    },
                    error: function(xhr) { // if error occured
                        alert("Error occured.please try again");
                    },
                    complete: function() {
                        Ajaxcomplete();
                    },
                });
                
  }

  

</script>
@endsection
