<!-- LAYOUT -->
@extends(isset($isajax) && $isajax==true ? 'layouts-frontend.ajax' : 'components.layout.memorial')
@section('page-type','list-page')

<!-- HEAD.INFO -->
@section('pageTitle', 'My Memorial Page')
@section('pageDescription', '')

<!-- HEADER.BREADCRUMBS -->
@section('page-breadcrumb','My Memorial Page')

<!-- MAIN.HEADER -->
@section('headercommon')
<x-main.header icon="broad_activity_feed" title="My Memorial Page" subtitle="Be remembered how you want to be remembered." />
@endsection

<!-- MAIN.CONTENT -->
@section('content')
<!-- ALERT -->
<section class="col-12 mt-4">
  <div class="alert bg-purple-pale py-4 px-md-5 px-4" role="alert">
    <div class="row">
      <div class="col-auto">
        <h4 class="mb-1 text-purple">Coming soon</h4>
        <p class="text-sm text-purple mb-0">A memorial page where you and others can contribute to your memory.</p>
      </div>
      <div class="col align-self-end text-md-end text-start d-flex align-items-center">
        <div class="col-12 pt-3 pt-lg-0">
          <a data-route="{{route('memorial-page')}}" class="btn btn-lg btn-primary link-handle mb-1" type="button">
            View an example
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- IMAGE PREVIEW -->
<section class="col-8 offset-md-2 mt-4">
  <div class="card card-blog card-plain">
    <div class="card card-header card-plain pb-0 p-3">
      <h5 class="mb-1">Memorial Pages</h5>
      <p class="text-sm mb-4">Create and customise your memorial page - and we'll publish it at the right time. Be remembered how you want to be remembered.</p>
    </div>
    <div class="position-relative">
      <a class="d-block shadow-xl border-radius-xl">
        <img src="{{asset('assets/img/evaheld-memorial-preview-new.jpeg')}}" alt="img-blur-shadow" class="img-fluid shadow border-radius-xl">
      </a>
    </div>
  </div>
</section>
<!-- FEATURES -->
<section class="col-8 offset-md-2 mt-4"> 
  <div class="card-body">
    <div class="card card-header card-plain pb-0">
      <h6 class="mb-3">Features</h6>
    </div>
    <div class="row">
      <div class="col-4">
        <div class="d-flex pb-2">
          <div>
            <i class="fas fa-check text-success text-sm" aria-hidden="true"></i>
          </div>
          <div class="ps-3">
            <span class="text-sm">Complete control over what is public</span>
          </div>
        </div>
        <div class="d-flex pb-2">
          <div>
            <i class="fas fa-check text-success text-sm" aria-hidden="true"></i>
          </div>
          <div class="ps-3">
            <span class="text-sm">Safe secure and easy to manage</span>
          </div>
        </div>
        <div class="d-flex pb-2">
          <div>
            <i class="fas fa-check text-success text-sm" aria-hidden="true"></i>
          </div>
          <div class="ps-3">
            <span class="text-sm">Automatically generated slideshow</span>
          </div>
        </div>
      </div>
      <div class="col-4">
        <div class="d-flex pb-2">
          <div>
            <i class="fas fa-check text-success text-sm" aria-hidden="true"></i>
          </div>
          <div class="ps-3">
            <span class="text-sm">Friends and family can share tributes</span>
          </div>
        </div>
        <div class="d-flex pb-2">
          <div>
            <i class="fas fa-check text-success text-sm" aria-hidden="true"></i>
          </div>
          <div class="ps-3">
            <span class="text-sm">Loved-ones can share memories</span>
          </div>
        </div>
        <div class="d-flex pb-2">
          <div>
            <i class="fas fa-check text-success text-sm" aria-hidden="true"></i>
          </div>
          <div class="ps-3">
            <span class="text-sm">Share messages to the world</span>
          </div>
        </div>
      </div>
      <div class="col-4">
        <div class="d-flex pb-2">
          <div>
            <i class="fas fa-check text-success text-sm" aria-hidden="true"></i>
          </div>
          <div class="ps-3">
            <span class="text-sm">Only published after you're gone</span>
          </div>
        </div>
        <div class="d-flex pb-2">
          <div>
            <i class="fas fa-check text-success text-sm" aria-hidden="true"></i>
          </div>
          <div class="ps-3">
            <span class="text-sm">A centralised place to say farewell</span>
          </div>
        </div>
        <div class="d-flex pb-2">
          <div>
            <i class="fas fa-check text-success text-sm" aria-hidden="true"></i>
          </div>
          <div class="ps-3">
            <span class="text-sm">Create a timeline of your life</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- FORM -->
<section class="col-12 mt-4">
  <div class="card py-4 px-md-5 px-4 bg-purple-pale shadow-none">
    <h4 class="mb-1 text-purple">Get early access</h4>
    <p class="text-sm text-purple mb-0">Sign up now to get early access to this feature.</p>
    <form class="mt-3">
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="givenName">Given name</label>
            <input type="email" class="form-control" id="givenName" placeholder="Jane">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="emailAddress">Email address</label>
            <input type="email" class="form-control" id="emailAddress" placeholder="name@example.com">
          </div>
        </div>
      </div>
    </form>
    <div class="col align-self-start text-md-end text-start d-flex align-items-center mt-2">
      <div class="col-12">
        <a data-route="{{route('memorial-page')}}" class="btn btn-lg btn-primary link-handle mb-1" type="submit">
          Request early access
        </a>
      </div>
    </div>
  </div>
</section>
@endsection