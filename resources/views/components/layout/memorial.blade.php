<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <x-head.analytics />
    <x-head.metatags />
    <x-head.favicons />
    <x-head.styles />
    <x-head.scripts/>
</head>

<body>
    <x-body.scripts.start/>
    <header>
        <x-header.navbar />
    </header>
    <main>
        <x-main.header />
        @yield('content')
    </main>
    <footer>
        <x-footer />
    </footer>
    <x-body.scripts.end />
</body>

</html>