<footer id="footer" class="footer pt-3 pb-4">
  <div class="container-fluid">
    <div class="row align-items-center justify-content-lg-between">
      <div class="col-lg-6 mb-lg-0 mb-4">
        <div class="copyright text-center text-sm text-muted text-lg-start">
          © <?php echo date('Y'); ?>, made with <i class="fi fi_heart" style="color: #662D91; font-size: 14px"></i> by <span class="font-weight-bold" target="_blank">Evaheld Dev Team</span>
        </div>
      </div>
    </div>
  </div>
</footer>