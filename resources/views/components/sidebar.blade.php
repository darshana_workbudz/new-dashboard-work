<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 " id="sidenav-main">
    <i class="fi fi_dismiss p-4 cursor-pointer opacity-5 end-0 top-0 d-none d-xl-none float-end"   aria-hidden="true" id="iconSidenav"></i>
  <div class="sidenav-header">
    
    <a class="navbar-brand m-0"  >
      <img src="{{asset('assets/img/evaheld-logo.svg')}}" class="navbar-brand-img h-100  e" alt="Evaheld logo">
    </a>
    
  </div>
  
  <hr class="horizontal dark mt-0">
  <div class="collapse navbar-collapse w-auto h-auto h-100 ps" id="sidenav-collapse-main">
    <ul class="navbar-nav">

      @if(null!==Session::get('trustedparty-delegateaccess') && !empty(Session::get('trustedparty-delegateaccess')))

       <li class="nav-item mb-2">
        <h6 class="text-center">Login as : {{auth()->user()->first_name}} {{auth()->user()->last_name}}</h6>
          <a class="nav-link link-handle bg-purple border-radius-md"  data-route="{{route('my-account.back-to-my-account')}}">
            <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_sign_out" style="color: white; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1 text-white">Exit Access</span>
          </a>
        </li> 
      @endif

      <li class="nav-item">
        <a class="nav-link  link-handle active" data-route="{{route('dashboard')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_home" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">Home</span>
        </a>
      </li>

      @hasrole('Admin')

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('b2c-users-clients.index')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-circle-08  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">B2C User Clients</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('our-partners.index')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-circle-08  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">B2C Partners</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('b2b-clients.index')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-circle-08  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">B2B Clients</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('list-roles')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-ui-04  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">Manage User roles</span>
        </a>
      </li>


      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('manage-coupons.index')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-circle-08  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">Generate Coupon Codes</span>
        </a>
      </li>

     <!--  <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('maintenance-dashboard')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-atom  text-lg top-0" aria-hidden="true" id="sidenavCardIcon"></i>
          </div>
          <span class="nav-link-text ms-1">QR Codes</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('maintenance-dashboard')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-delivery-fast  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">Hand Delivered Product</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('maintenance-dashboard')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-single-02  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">Our Profile</span>
        </a>
      </li> -->

      <li class="nav-item">
        <hr class="horizontal dark">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6"> Other Details</h6>
      </li>

      <li class="nav-item">
        <a class="nav-link  " data-route="{{route('maintenance-dashboard')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-button-play  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">Ziggeo</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  " data-route="{{route('maintenance-dashboard')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="fi fi_payment" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">Stripe</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  " data-route="{{route('maintenance-dashboard')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="fi fi_send  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">SendInBlue</span>
        </a>
      </li>


      @endhasrole

      @hasrole('Client')
      
      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('create-content')}}">
          <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_video" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">Create Content</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('my-content')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_library" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">My Content</span>
        </a>
      </li>


      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('manage-recipients.index')}}">
          <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_people" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">My Recipients</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('memorial-page')}}">
          <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_broad_activity_feed" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">My Memorial Page</span>
        </a>
      </li>

      <!-- <li class="nav-item">
          <a class="nav-link "  href="{{route('new-pricing-packages')}}">
            <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/icons/create-message.png')}}" style='height: 20px;'>
            </div>
            <span class="nav-link-text ms-1">Our Pricing</span>
          </a>
        </li> -->
      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('my-preferences.index')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_shield_task" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">My Preferences</span>
        </a>
      </li>

      <!-- <li class="nav-item">
          <a class="nav-link  link-handle"  data-route="{{route('maintenance-dashboard')}}">
            <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-atom  text-lg top-0" aria-hidden="true" id="sidenavCardIcon"></i>
            </div>
            <span class="nav-link-text ms-1">My QR Code</span>
          </a>
        </li> -->
      <li class="nav-item">
        <hr class="horizontal dark">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Account Details</h6>
      </li>
      <li class="nav-item">
        <a class="nav-link link-handle" data-route="{{route('my-account')}}" aria-controls="MyAccountTab" role="button">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_contact_card" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">My Account</span>
        </a>
        <div class="collapse" id="MyAccountTab" style="">
          <ul class="nav ms-4 ps-3">
            <li class="nav-item ">
              <a class="nav-link link-handle" data-route="{{route('my-account.add-on-features')}}">
                <span class="sidenav-mini-icon"> K </span>
                <span class="sidenav-normal"> Add-on Features </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link link-handle" data-route="{{route('my-account.upgrade-plan')}}">
                <span class="sidenav-mini-icon"> W </span>
                <span class="sidenav-normal"> Upgrade My Plan </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link link-handle" data-route="{{route('my-account.free-up-minutes')}}">
                <span class="sidenav-mini-icon"> D </span>
                <span class="sidenav-normal"> Minutes Tracker </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link link-handle" data-route="{{route('my-account.my-billing')}}">
                <span class="sidenav-mini-icon"> C </span>
                <span class="sidenav-normal"> My Subscription </span>
              </a>
            </li>

          </ul>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('my-profile')}}">
          <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_person" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">My Profile</span>
        </a>
      </li>
      <!-- <li class="nav-item">
          <a class="nav-link  link-handle"  data-route="{{route('my-account')}}">
            <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-circle-08  text-lg top-0" aria-hidden="true"></i>
            </div>
            <span class="nav-link-text ms-1">My Account New</span>
          </a>
        </li> -->
      <?php $checkaccess=\App\Models\HasAccessLogin::GetByUser(auth()->user()->email);?>
          <?php if(isset($checkaccess) && count($checkaccess)>0):?>
           <li class="nav-item">
            <a class="nav-link  link-handle" data-route="{{route('my-account.delegate-access')}}">
              <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
                <span class="fi fi_people" style="color: #662D91; font-size: 20px"></span>
              </div>
              <span class="nav-link-text ms-1">Delegate Access</span>
            </a>
          </li>
        <?php endif;?>
     

      <li class="nav-item">
        <hr class="horizontal dark">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6"> </h6>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('get-support')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_chat_help" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">Get Support</span>
        </a>
      </li>

      <!-- <li class="nav-item">
          <a class="nav-link  link-handle"  data-route="{{route('maintenance-dashboard')}}">
            <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-diamond  text-lg top-0" aria-hidden="true"></i>
            </div>
            <span class="nav-link-text ms-1">Coming Soon</span>
          </a>
        </li> -->


      @endhasrole

     

      @hasrole('B2B Partner')

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('partner.our-members')}}">
          <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_people" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">Our Members</span>
        </a>
      </li>

      <li class="nav-item">
        <hr class="horizontal dark">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Account Details</h6>
      </li>
      <li class="nav-item">
        <a class="nav-link link-handle" data-route="{{route('partner.our-account')}}" aria-controls="MyAccountTab" role="button">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_contact_card" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">My Account</span>
        </a>
      </li>



       <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('partner.our-profile')}}">
          <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_person" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">Our Profile</span>
        </a>
      </li>



      <li class="nav-item">
        <hr class="horizontal dark">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6"> </h6>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('get-support')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_chat_help" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">Get Support</span>
        </a>
      </li>

      <!-- <li class="nav-item">
          <a class="nav-link  link-handle"  data-route="{{route('maintenance-dashboard')}}">
            <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-diamond  text-lg top-0" aria-hidden="true"></i>
            </div>
            <span class="nav-link-text ms-1">Coming Soon</span>
          </a>
        </li> -->

      @endhasrole

      @hasrole('Member')

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('member.our-clients')}}">
          <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_people" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">My Clients</span>
        </a>
      </li>

      <li class="nav-item">
        <hr class="horizontal dark">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Account Details</h6>
      </li>
      <li class="nav-item">
        <a class="nav-link link-handle" data-route="{{route('member.our-account')}}" aria-controls="MyAccountTab" role="button">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_contact_card" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">My Account</span>
        </a>
      </li>



       <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('member.our-profile')}}">
          <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_person" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">My Profile</span>
        </a>
      </li>



      <li class="nav-item">
        <hr class="horizontal dark">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6"> </h6>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('get-support')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_chat_help" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">Get Support</span>
        </a>
      </li>

      @endhasrole








    </ul>
  </div>
  @hasrole('B2B Partner')
  <div class="sidenav-footer mx-3 mt-3 pt-3">
    <div class="card card-background shadow-none card-background-mask-primary" id="sidenavCard">
      <div class="full-background" style="background-image: url('{{asset('assets/img/curved-images/white-curved.jpg')}}"></div>
      <div class="card-body text-start p-3 w-100">
        <div class="icon icon-shape bg-white icon-sm shadow text-center mb-3 d-flex align-items-center justify-content-center border-radius-md">
          <span class="fi fi_gift" style="color: #662D91; font-size: 20px;"></span>
        </div>
        <div class="docs-info">
          <h6 class="text-white up mb-0">Want a Custom Package?</h6>
          <p class="text-xs font-weight-bold">We’d love to chat</p>
          <a data-route="{{route('get-support')}}" class="btn btn-white btn-sm w-100 mb-0 link-handle">Send Enquiry</a>
        </div>
      </div>
    </div>
  </div>
  @endhasrole



  @hasrole('Member')
  <div class="sidenav-footer mx-3 mt-3 pt-3">
    <div class="card card-background shadow-none card-background-mask-primary" id="sidenavCard">
      <div class="full-background" style="background-image: url('{{asset('assets/img/curved-images/white-curved.jpg')}}"></div>
      <div class="card-body text-start p-3 w-100">
        <div class="icon icon-shape bg-white icon-sm shadow text-center mb-3 d-flex align-items-center justify-content-center border-radius-md">
          <span class="fi fi_gift" style="color: #662D91; font-size: 20px;"></span>
        </div>
        <div class="docs-info">
          <h6 class="text-white up mb-0">Want a Custom Package?</h6>
          <p class="text-xs font-weight-bold">We’d love to chat</p>
          <a data-route="{{route('get-support')}}" class="btn btn-white btn-sm w-100 mb-0 link-handle">Send Enquiry</a>
        </div>
      </div>
    </div>
  </div>
  @endhasrole

  @hasrole('Client')
  <div class="sidenav-footer mx-3 mt-3 pt-3">
    <div class="card card-background shadow-none card-background-mask-primary" id="sidenavCard">
      <div class="full-background" style="background-image: url('{{asset('assets/img/curved-images/white-curved.jpg')}}"></div>
      <div class="card-body text-start p-3 w-100">
        <div class="icon icon-shape bg-white icon-sm shadow text-center mb-3 d-flex align-items-center justify-content-center border-radius-md">
          <span class="fi fi_heart" style="color: #662D91; font-size: 20px;"></span>
        </div>
        <div class="docs-info">
          <h6 class="text-white up mb-0">Leave less unsaid</h6>
          <p class="text-xs font-weight-bold">Explore more features available to you</p>
          <a data-route="{{route('my-account.add-on-features')}}" class="btn btn-white btn-sm w-100 mb-0 link-handle">Show me more</a>
        </div>

      </div>
    </div>
  </div>
  @endhasrole




</aside>