<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
<link href="{{asset('assets/css/fluent-icons.css')}}" rel="stylesheet" />
<link href="{{asset('assets/css/nucleo-icons.css')}}" rel="stylesheet" />
<link href="{{asset('assets/css/nucleo-svg.css')}}" rel="stylesheet" />
<script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
<link href="{{asset('assets/css/nucleo-svg.css')}}" rel="stylesheet" />
<link id="pagestyle" href="{{asset('assets/css/soft-ui-dashboard.css?v=1.1.4')}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('assets/js/plugins/datepicker.css')}}">
<link href="{{asset('assets/css/common-style.css')}}?v=1.1.4" rel="stylesheet" />
<link href="{{asset('assets/js/plugins/sweetalert/css/sweetalert.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/js/plugins/select2/css/select2.min.css')}}">
<link href="{{asset('assets/css/backend-basics.css')}}?v=1.1.4" rel="stylesheet" />
<link href="{{asset('assets/css/progressbar.css')}}?v=1.1.4" rel="stylesheet" />