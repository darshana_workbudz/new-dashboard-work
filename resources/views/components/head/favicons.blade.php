<link rel="icon" type="image/x-icon" href="{{asset('assets/favicons//favicon.ico')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/favicons/apple-touch-icon.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/favicons/favicon-32x32.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/favicons/favicon-16x16.png')}}">
<link rel="manifest" href="{{asset('assets/favicons/site.webmanifest')}}">
<link rel="mask-icon" href="{{asset('assets/favicons/safari-pinned-tab.svg')}}" color="#662d91">
<meta name="msapplication-TileColor" content="#662d91">
<meta name="theme-color" content="#662d91">
