<header>
@if(isset($pagetype) && $pagetype=='list-page')
@include('layouts-backend.navbar-header')
<x-header.navbar/>
@else
<x-header.navbar-with-background/>
@endif
</header>