<script src="{{asset('assets/js/soft-ui-dashboard.js?v=1.1.5')}}"></script>
<script type="text/javascript" async="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOIifNM3p7o0Xu9L2tIudLGhN4CQDc0Sw&libraries=places&callback="></script>
<script type="text/javascript">
  $('.select2').select2();

  function formatState(state) {
    if (!state.id) {
      return state.text;
    }
    console.log(state);
    var baseUrl = "{{Request::root()}}/assets/img/flag";
    var $state = $(
      '<span><img class="img-flag " /> <span class="text-sm"></span></span>'
    );

    var code = $('.phonecodeselect2 option:selected').data('code');


    // Use .text() instead of HTML string concatenation to avoid script injection issues
    $state.find("span").text(state.text);
    $state.find("img").attr("src", baseUrl + "/" + code + "-32.png");

    return $state;
  };

  function formatState2(state) {
    if (!state.id) {
      return state.text;
    }
    console.log(state);
    var baseUrl = "{{Request::root()}}/assets/img/flag";
    var $state = $(
      '<span><img class="img-flag " /> <span class="text-sm"></span></span>'
    );

    var code = $('.phonecodeselect1 option:selected').data('code');


    // Use .text() instead of HTML string concatenation to avoid script injection issues
    $state.find("span").text(state.text);
    $state.find("img").attr("src", baseUrl + "/" + code + "-32.png");

    return $state;
  };

  $(".phonecodeselect2").select2({

    templateSelection: formatState
  });
  $(".phonecodeselect1").select2({

    templateSelection: formatState2
  });
</script>

@yield('script')