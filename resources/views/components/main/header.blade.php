<div class="container-fluid">
        <div class="page-header min-height-200 border-radius-xl mt-4" style="background-image: url('{{asset('assets/img/curved-images/curved0.jpg')}}'); background-position-y: 50%;">
                <span class="mask bg-primary opacity-9"></span>
        </div>
        <div class="card card-body blur shadow-blur mx-3 mt-n6 overflow-hidden">
                <div class="row gx-4">
                        <div class="col-auto mb-1">
                                <!-- Avatar Large -->
                                <div class="avatar avatar-xl position-relative border-radius-xl bg-purple d-none d-lg-block d-xl-block">
                                        <div class="card-body z-index-3 text-center p-3">
                                                <i class="fi fi_{{ $icon }} text-white" style="font-size: 42px;" aria-hidden="true"></i>
                                        </div>
                                </div>
                                <!-- Avatar Small -->
                                <div class="avatar avatar-lg position-relative border-radius-xl bg-purple d-block d-lg-none d-xl-none">
                                        <div class="card-body z-index-3 text-center p-2 py-3">
                                                <i class="fi fi_{{ $icon }} text-white" style="font-size: 28px" aria-hidden="true"></i>
                                        </div>
                                </div>
                        </div>
                        <div class="col-auto col-sm-8 col-md-6 mb-2"> <div class="h-100">
                                <h5 class="mb-1">
                                        {{ $title }}
                                </h5>
                                <p class="mb-0 font-weight-bold text-sm">
                                        {{ $subtitle }}
                                </p>
                                <?php $link = Session::get('invitation_link');?>
                                @if(isset($link) && !empty($link) && (Route::currentRouteName()=="member.our-clients" || Route::currentRouteName()=="our-partners.index" || Route::currentRouteName()=="partner.our-members")  )
                                <div class="mt-2 d-flex align-items-center">
                                        <div class="form-group w-100">
                                                <div class="input-group bg-gray-200">
                                                    <input id="copy-link" class="form-control form-control-sm" value="<?php echo isset($link) && !empty($link) ? $link : ''; ?>" type="text" disabled="">

                                            </div>
                                        </div>


                                    
                                </div>
                                @endif
                    </div>
            </div>
            @if (isset($button) && $button === true)
            <div class="col align-self-end text-md-end text-start d-flex align-items-center">
                <div class="col-12">
                        <a @if(isset($link) && !empty($link) && (Route::currentRouteName()=="member.our-clients" || Route::currentRouteName()=="our-partners.index" || Route::currentRouteName()=="partner.our-members")) onclick="copyToClipboard('#copy-link')" @endif {{ $attributes->merge(['class' => 'btn btn-lg btn-primary link-handle']) }} type="button">
                                @if ($buttonIcon === '')
                                @else
                                <i class="fi fi_{{ $buttonIcon }} text-white pe-2" aria-hidden="true"></i>
                                @endif
                                {{ $buttonText }}
                        </a>
                </div>
        </div>
        @else
        @endif
</div>
</div>
</div>