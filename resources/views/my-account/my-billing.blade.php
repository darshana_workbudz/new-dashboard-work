@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Billing')

@section('headercommon')
<x-main.header icon="payment" title="My Billing" subtitle="Manage Your Billing Details" />
@endsection

@section('content')
<div class="row mb-5">
  <div class="col-lg-12 mt-lg-0">
    <div class="card  mt-3" id="myaccount-billing">
      <div class="card-header">
        <div class="d-lg-flex">
          <nav style="--bs-breadcrumb-divider: '›';" aria-label="breadcrumb">
            <ol class="breadcrumb p-0 bg-white">
              <li class="breadcrumb-item"><a data-route="{{route('my-account')}}" class="link-handle h5 text-sm font-weight-bolder text-primary">My Account</a></li>
              <li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page">My Billing</li>
            </ol>
          </nav>
        </div>
        <p class="text-sm mb-0 font-weight-bold">Billing History</p>
      </div>
      <div class="card-body pt-0 ps-5">
        @if(isset($receipts) && count($receipts)>0)
        <div class="row">

        
              @foreach($receipts as $receiptdata)
            <div class="col-5 card card shadow-lg mx-2 mt-2">

         
            <ul class="list-group">
               <div class="card-body">
              <li class="list-group-item border-0 justify-content-between p-0 m-0 border-radius-lg">
                <div class="row">
                  <div class="col-auto">
                    
                    
                      <h6 class="mb-1 text-dark text-sm" ><i class="fa fa-check-circle" aria-hidden="true"></i> Receipt #{{$receiptdata->number}}</h6>
                      <span class="text-xs"><?php echo date('d F Y', strtotime($receiptdata->created_at)); ?>, at <?php echo date('h:m a', strtotime($receiptdata->created_at)); ?></span>
                      @if($receiptdata->status=='succeeded')
                      <span class="text-sm text-primary mt-1 ">
                        $ {{$receiptdata->amount}}.00 AUD <span class="mb-1 badge badge-sm bg-success mb-0 mt-2 ms-1">{{$receiptdata->status}} </span> </span>
                      @endif
                      @if($receiptdata->status!=='succeeded')
                      <span class="text-sm text-primary mt-1 ">
                        $ {{$receiptdata->amount_remaining}}.00 AUD <span class="mb-1 badge badge-sm bg-danger mb-0 mt-2 ms-1">Due amount</span> </span>
                      @endif
                    </div>
                  
                  <div class="col-auto">
                    <div class="row">
                      @if($receiptdata->status=='succeeded')
                      <div class="col-12"> <a href="{{$receiptdata->receipt_url}}"> <span class="badge badge-lg bg-primary mb-0 mt-2">View</span> </a> </div>
                      @endif
                      @if($receiptdata->status!=='succeeded')
                      <div class="col-12"> <a href="{{$receiptdata->receipt_url}}"> <span class="badge badge-lg bg-primary mb-0 mt-2">Pay Now</span> </a> </div>
                      @endif
                      
                    </div>
                  </div>
                </div>
              </li>
              </div>
              </ul>
          </div>
              @endforeach
            
        
        </div>
        @else
        <blockquote class="text-sm"> - Sorry no Receipts Found !</blockquote>
        @endif
      </div>
</div>
  </div>
  
</div>

<script type="text/javascript">

  $('.scrolltodiv').click(function (e) {
    var scrollto=$(this).data('scroll');
    $('html, body').animate({
      scrollTop: $(scrollto).offset().top
    }, 2000);
  });

  $(function() {
    var target = $('#myaccount-billing');
    if (target.length) {


      $('html,body').animate({
        scrollTop: target.offset().top
      }, 1000);
      return false;
    }
  });
</script>
@endsection