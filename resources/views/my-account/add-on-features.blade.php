@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Add-on Features')
@section('headercommon')
<x-main.header icon="gift" title="Add-on Features" subtitle="Explore add-on features available to you." />
@endsection

@section('content')

<div class="row mb-5">

    <div class="col-lg-12 mt-lg-0">



        <div class="card" id="myaccount-addon">
            <div class="card-header">
                 <nav style="--bs-breadcrumb-divider: '›';" aria-label="breadcrumb">
                  <ol class="breadcrumb p-0 bg-white">
                    <li class="breadcrumb-item"><a data-route="{{route('my-account')}}" class="link-handle h5 text-sm font-weight-bolder text-primary">My Account</a></li>
                    <li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page">Add-on Features</li>
                  </ol>
                </nav>
            </div>

            <div class="card-body pt-0">
                
                <div class="row">
                    <h6 class="p-2 ps-3">Personalised Add-ons</h6>
                    <?php if (isset($heliroomaddon) && !empty($heliroomaddon) && !empty($usersubscription)) : ?>

                        <div class="col-lg-6 col-md-6 col-sm-6 mt-2">

                            <form method="post" action="{{route('dashboard.purchase-subscription',$heliroomaddon->package_id)}}">
                                <input type="hidden" name="package_id" value="{{$heliroomaddon->package_id}}">
                                <input type="hidden" name="purchasesubscription" value="notrial">
                                <input type="hidden" name="redirecturl" value="{{route('my-account.add-on-features')}}">
                                @csrf

                                <div class="card  bg-white-100 shadow-lg border-dark border-radius-xl ">

                                    <div class="card-body px-3py-0 ">
                                        <div class="d-flex">
                                            <div class="avatar avatar-lg">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                                                    <g id="Group_1854" data-name="Group 1854" transform="translate(-17101 -14322)">
                                                        <rect id="Rectangle_655" data-name="Rectangle 655" width="54.083" height="55" rx="16" transform="translate(17101 14322)" fill="#f3eef6" />
                                                        <path id="Path_3905" data-name="Path 3905" d="M14.917,17.89v13H9.139A4.7,4.7,0,0,1,4.444,26.2V17.89Zm12.639,0V26.2a4.7,4.7,0,0,1-4.695,4.695H17.084v-13ZM19.611,2a4.695,4.695,0,0,1,3.955,7.225l3.629,0A1.676,1.676,0,0,1,29,10.728V14.94a1.676,1.676,0,0,1-1.806,1.505H17.084V9.223H14.917v7.221H4.806A1.676,1.676,0,0,1,3,14.94V10.728A1.676,1.676,0,0,1,4.806,9.223l3.629,0A4.695,4.695,0,0,1,16,3.7,4.68,4.68,0,0,1,19.611,2ZM12.389,4.167a2.528,2.528,0,0,0-.208,5.047l.208.009h2.528V6.695l-.009-.208A2.528,2.528,0,0,0,12.389,4.167Zm7.222,0a2.528,2.528,0,0,0-2.528,2.528V9.223h2.528a2.528,2.528,0,1,0,0-5.056Z" transform="translate(17112 14333.001)" fill="#662d91" />
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="ms-2 my-auto">
                                                <h6 class="mb-0">{{$heliroomaddon->name}}</h6>
                                            </div>

                                        </div>
                                        <p class="p-2 text-sm"> Nearly all recipients tell us that this is the best gift they’ve ever received.</p>
                                        <hr>


                                        <div class="d-flex">
                                            <button class="btn btn-icon btn-3 btn-primary mb-0" type="submit">
                                                <span class="btn-inner--icon">
                                                    <i class="fas fa-plus text-white" aria-hidden="true"></i>
                                                </span>
                                                <span class="btn-inner--text ps-1">Add item</span>
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </form>
                        </div>
                    <?php elseif (isset($usersubscription) && !empty($usersubscription) && $usersubscription->demopackage == 1) : ?>
                        <div class="col-lg-6 col-md-6 col-sm-6 mt-2">
                            <div class="card  bg-white-100 shadow-lg border-dark border-radius-xl ">

                                <div class="card-body px-3py-0 ">
                                    <div class="d-flex">
                                        <div class="avatar avatar-lg">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                                                <g id="Group_1854" data-name="Group 1854" transform="translate(-17101 -14322)">
                                                    <rect id="Rectangle_655" data-name="Rectangle 655" width="54.083" height="55" rx="16" transform="translate(17101 14322)" fill="#f3eef6" />
                                                    <path id="Path_3905" data-name="Path 3905" d="M14.917,17.89v13H9.139A4.7,4.7,0,0,1,4.444,26.2V17.89Zm12.639,0V26.2a4.7,4.7,0,0,1-4.695,4.695H17.084v-13ZM19.611,2a4.695,4.695,0,0,1,3.955,7.225l3.629,0A1.676,1.676,0,0,1,29,10.728V14.94a1.676,1.676,0,0,1-1.806,1.505H17.084V9.223H14.917v7.221H4.806A1.676,1.676,0,0,1,3,14.94V10.728A1.676,1.676,0,0,1,4.806,9.223l3.629,0A4.695,4.695,0,0,1,16,3.7,4.68,4.68,0,0,1,19.611,2ZM12.389,4.167a2.528,2.528,0,0,0-.208,5.047l.208.009h2.528V6.695l-.009-.208A2.528,2.528,0,0,0,12.389,4.167Zm7.222,0a2.528,2.528,0,0,0-2.528,2.528V9.223h2.528a2.528,2.528,0,1,0,0-5.056Z" transform="translate(17112 14333.001)" fill="#662d91" />
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="ms-2 my-auto">
                                            <h6 class="mb-0">Hand Delivered Heirloom </h6>

                                        </div>
                                    </div>
                                    <p class="p-3 text-sm">  Add your content to hand delivered Heirloom Keepsake.The perfect gift for your family.</p>
                                    <hr />
                                    <div class="d-flex">
                                        <a class="btn bg-purple-pale text-purple btn-lg showheirloom-package-popup" style="box-shadow:none;">Choose</a>
                                    </div>
                                </div>





                            </div>
                        </div>


                    <?php endif; ?>
                    <?php if (isset($qraddon) && !empty($qraddon) && !empty($usersubscription)) : ?>
                       <div class="col-lg-6 col-md-6 col-sm-6 mt-2">

                            <form method="post" action="{{route('dashboard.purchase-subscription',$qraddon->package_id)}}">
                                <input type="hidden" name="package_id" value="{{$qraddon->package_id}}">
                                <input type="hidden" name="purchasesubscription" value="notrial">
                                <input type="hidden" name="redirecturl" value="{{route('my-account.add-on-features')}}">
                                @csrf

                                <div class="card  bg-white-100 shadow-lg border-dark border-radius-xl ">

                                    <div class="card-body px-3py-0 ">
                                        <div class="d-flex">
                                            <div class="avatar avatar-lg">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                                                    <g id="Group_1855" data-name="Group 1855" transform="translate(-17175 -14322)">
                                                        <rect id="Rectangle_654" data-name="Rectangle 654" width="54.083" height="55" rx="16" transform="translate(17175 14322)" fill="#f3eef6"></rect>
                                                        <g id="Group_1852" data-name="Group 1852" transform="translate(17189 14337)">
                                                            <path id="Path_3906" data-name="Path 3906" d="M8.889,6H6V8.889H8.889Z" transform="translate(-1.667 -1.667)" fill="#662d91"></path>
                                                            <path id="Path_3907" data-name="Path 3907" d="M3,6.611A3.611,3.611,0,0,1,6.611,3h4.333a3.611,3.611,0,0,1,3.611,3.611v4.333a3.611,3.611,0,0,1-3.611,3.611H6.611A3.611,3.611,0,0,1,3,10.945Zm3.611-.722a.722.722,0,0,0-.722.722v4.333a.722.722,0,0,0,.722.722h4.333a.722.722,0,0,0,.722-.722V6.611a.722.722,0,0,0-.722-.722Zm.722,15.889h2.889v2.889H7.333Z" transform="translate(-3 -3)" fill="#662d91"></path>
                                                            <path id="Path_3908" data-name="Path 3908" d="M3,19.722a3.611,3.611,0,0,1,3.611-3.611h4.333a3.611,3.611,0,0,1,3.611,3.611v4.333a3.611,3.611,0,0,1-3.611,3.611H6.611A3.611,3.611,0,0,1,3,24.056ZM6.611,19a.722.722,0,0,0-.722.722v4.333a.722.722,0,0,0,.722.722h4.333a.722.722,0,0,0,.722-.722V19.722A.722.722,0,0,0,10.945,19ZM24.667,6H21.778V8.889h2.889Z" transform="translate(-3 -1.667)" fill="#662d91"></path>
                                                            <path id="Path_3909" data-name="Path 3909" d="M16.611,3A3.611,3.611,0,0,0,13,6.611v4.333a3.611,3.611,0,0,0,3.611,3.611h4.333a3.611,3.611,0,0,0,3.611-3.611V6.611A3.611,3.611,0,0,0,20.945,3Zm-.722,3.611a.722.722,0,0,1,.722-.722h4.333a.722.722,0,0,1,.722.722v4.333a.722.722,0,0,1-.722.722H16.611a.722.722,0,0,1-.722-.722ZM13,17.445h3.972v3.972H13Zm7.583,3.972H16.972v3.611H13V29h3.972V25.028h3.611V29h3.972V25.028H20.583Zm0,0V17.445h3.972v3.972Z" transform="translate(1.445 -3)" fill="#662d91"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="ms-2 my-auto">
                                                <h6 class="mb-0">{{$qraddon->name}}</h6>

                                            </div>
                                        </div>
                                        <p class="p-2 text-sm">  Be remembered how you want to be remembered.</p>
                                        <hr />
                                        <div class="d-flex">
                                            <button class="btn btn-icon btn-3 btn-primary mb-0" type="submit">
                                                <span class="btn-inner--icon">
                                                    <i class="fas fa-plus text-white" aria-hidden="true"></i>
                                                </span>
                                                <span class="btn-inner--text ps-1">Add item</span>
                                            </button>
                                        </div>
                                    </div>



                                </div>
                            </form>
                        </div>
                    <?php elseif (isset($usersubscription) && !empty($usersubscription) && $usersubscription->demopackage == 1) : ?>
                        <div class="col-lg-6 col-md-6 col-sm-6 mt-2">
                            <div class="card  bg-white-100 shadow-lg border-dark border-radius-xl ">

                                <div class="card-body px-3py-0 ">
                                    <div class="d-flex">
                                        <div class="avatar avatar-lg">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                                                <g id="Group_1855" data-name="Group 1855" transform="translate(-17175 -14322)">
                                                    <rect id="Rectangle_654" data-name="Rectangle 654" width="54.083" height="55" rx="16" transform="translate(17175 14322)" fill="#f3eef6"></rect>
                                                    <g id="Group_1852" data-name="Group 1852" transform="translate(17189 14337)">
                                                        <path id="Path_3906" data-name="Path 3906" d="M8.889,6H6V8.889H8.889Z" transform="translate(-1.667 -1.667)" fill="#662d91"></path>
                                                        <path id="Path_3907" data-name="Path 3907" d="M3,6.611A3.611,3.611,0,0,1,6.611,3h4.333a3.611,3.611,0,0,1,3.611,3.611v4.333a3.611,3.611,0,0,1-3.611,3.611H6.611A3.611,3.611,0,0,1,3,10.945Zm3.611-.722a.722.722,0,0,0-.722.722v4.333a.722.722,0,0,0,.722.722h4.333a.722.722,0,0,0,.722-.722V6.611a.722.722,0,0,0-.722-.722Zm.722,15.889h2.889v2.889H7.333Z" transform="translate(-3 -3)" fill="#662d91"></path>
                                                        <path id="Path_3908" data-name="Path 3908" d="M3,19.722a3.611,3.611,0,0,1,3.611-3.611h4.333a3.611,3.611,0,0,1,3.611,3.611v4.333a3.611,3.611,0,0,1-3.611,3.611H6.611A3.611,3.611,0,0,1,3,24.056ZM6.611,19a.722.722,0,0,0-.722.722v4.333a.722.722,0,0,0,.722.722h4.333a.722.722,0,0,0,.722-.722V19.722A.722.722,0,0,0,10.945,19ZM24.667,6H21.778V8.889h2.889Z" transform="translate(-3 -1.667)" fill="#662d91"></path>
                                                        <path id="Path_3909" data-name="Path 3909" d="M16.611,3A3.611,3.611,0,0,0,13,6.611v4.333a3.611,3.611,0,0,0,3.611,3.611h4.333a3.611,3.611,0,0,0,3.611-3.611V6.611A3.611,3.611,0,0,0,20.945,3Zm-.722,3.611a.722.722,0,0,1,.722-.722h4.333a.722.722,0,0,1,.722.722v4.333a.722.722,0,0,1-.722.722H16.611a.722.722,0,0,1-.722-.722ZM13,17.445h3.972v3.972H13Zm7.583,3.972H16.972v3.611H13V29h3.972V25.028h3.611V29h3.972V25.028H20.583Zm0,0V17.445h3.972v3.972Z" transform="translate(1.445 -3)" fill="#662d91"></path>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="ms-2 my-auto">
                                            <h6 class="mb-0">QR Code</h6>

                                        </div>
                                    </div>
                                    <p class="p-3 text-sm">  Add your content to QR Code to make sharing your memory easy and hassle free.</p>
                                    <hr />
                                    <div class="d-flex">
                                        <a class="btn bg-purple-pale text-purple btn-lg showqr-package-popup" style="box-shadow:none;">Choose</a>
                                    </div>
                                </div>





                            </div>
                        </div>

                    <?php endif; ?>

                    <h6 class="p-2 ps-3 mt-3">Additional Add-ons</h6>

                    @if(isset($extraaddons) && !empty($extraaddons))
                    <div class="col-lg-6 col-md-6 col-sm-6 mt-2">
                        <form method="post" action="{{route('dashboard.purchase-subscription',$extraaddons->package_id)}}">
                            <input type="hidden" name="package_id" value="{{$extraaddons->package_id}}">
                            <input type="hidden" name="purchasesubscription" value="notrial">
                            <input type="hidden" name="redirecturl" value="{{route('my-account.add-on-features')}}">
                            @csrf
                            <div class="card  bg-white-100 shadow-lg border-dark border-radius-xl ">
                                <div class="card-body ">
                                    <div class="d-flex">
                                        <div class="avatar avatar-lg">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                                                <g id="Group_1842" data-name="Group 1842" transform="translate(-17096 -14051)">
                                                    <rect id="Rectangle_654" data-name="Rectangle 654" width="54.083" height="55" rx="16" transform="translate(17175 14322)" fill="#f3eef6"></rect>
                                                    <path id="Path_3872" data-name="Path 3872" d="M24.835,19.169a3.334,3.334,0,0,1,3.324,3.114l.007.219v2.672a3.334,3.334,0,0,1-6.658.22l-.007-.22V22.5A3.334,3.334,0,0,1,24.835,19.169ZM14.83,5.834a11.335,11.335,0,0,1,11.3,12.185,4.65,4.65,0,0,0-4.767,1.357A2.336,2.336,0,0,0,18.622,17.9l-.2.056-2,.672-.209.083-.192.1A2.346,2.346,0,0,0,14.9,21.385l.057.2.083.209.1.192.131.2a2.4,2.4,0,0,0,1.247.895l.211.053.105.016V27.5l.015.24v.587A11.335,11.335,0,1,1,14.828,5.835Zm5.331,14.2.008.133V27.5a1,1,0,0,1-1.991.136l-.009-.136V21.557l-.684.231a1,1,0,0,1-1.213-.5l-.053-.127a1,1,0,0,1,.505-1.213l.125-.053,2-.671a1,1,0,0,1,1.309.813Zm4.671,1.133a1.333,1.333,0,0,0-1.323,1.179L23.5,22.5v2.672a1.333,1.333,0,0,0,2.656.156l.008-.156V22.5a1.333,1.333,0,0,0-1.332-1.333Zm-10-11.332a1,1,0,0,0-.991.864l-.009.136v6l.009.136a1,1,0,0,0,1.982,0l.009-.136v-6L15.82,10.7a1,1,0,0,0-.991-.864ZM24.391,6l.108.083,1.534,1.332A1,1,0,0,1,24.828,9l-.109-.081L23.187,7.589A1,1,0,0,1,24.391,6ZM17.83,2.5a1,1,0,0,1,.136,1.991L17.83,4.5h-6a1,1,0,0,1-.136-1.991l.136-.009Z" transform="translate(17107.496 14062.5)" fill="#662d91"></path>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="ms-2 my-auto">
                                            <h6 class="mb-0">Purchase more minutes </h6>
                                            <p class="text-xs mb-0"> <span class="text-dark mt-2 mb-0 h5 text-md">
                                                    <small>$</small>{{$extraaddons->price}} <span class="text-sm h6 mt-2 mb-0">/ minute</span></span> </p>
                                        </div>
                                        <!-- <div class="ms-2 my-auto">
                      <h6 class="mb-0">Purchase More Minutes </h6>
                      <p class="text-xs mb-0"> <span class="text-dark mt-2 mb-0 h5">
                          <small>$</small>{{$extraaddons->price}} <span class="h6 mt-2 mb-0">/ minutes</span></span> </p>
                        </div> -->
                                    </div>
                                    
                                    <hr />
                                    <div class="d-flex row">
                                        <div class="col-lg-5 col-sm-12">
                                            <div class="input-group mb-3">
                                                <input type="number" class="form-control" name="quantity" value="1" aria-label="Quantity of minutes to purchase" aria-describedby="Quantity of minutes to purchase" />
                                                <span class="input-group-text" id="unit">minutes</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-12">
                                            <button type="submit" class="btn btn-icon btn-3 btn-primary mb-0">
                                                <span class="btn-inner--icon">
                                                    <i class="fa fa-plus pe-2" aria-hidden="true"></i>
                                                </span>
                                                <span class="btn-inner--text ps-1">Purchase minutes</span>
                                            </button>
                                        </div>
                                    </div>

                                    <!-- <div class="d-flex row">
                    <div class="col-lg-4">

                      <input type="number" class="form-control" name="quantity" value="1"> <label> minutes </label>
                    </div>
                    <div class="col-lg-8">


                      <button type="submit" class="btn btn-sm bg-gradient-primary mb-0">
                        <i class="fas fa-plus pe-2" aria-hidden="true"></i> purchase
                      </button>
                    </div>

                  </div> -->
                                </div>


                            </div>
                        </form>
                    </div>
                    @endif
                    @if(isset($pagesaddons) && !empty($pagesaddons))
                    <div class="col-lg-6 col-md-6 col-sm-6 mt-2">
                        <form method="post" action="{{route('dashboard.purchase-subscription',$pagesaddons->package_id)}}">
                            <input type="hidden" name="package_id" value="{{$pagesaddons->package_id}}">
                            <input type="hidden" name="purchasesubscription" value="notrial">
                            <input type="hidden" name="redirecturl" value="{{route('my-account.add-on-features')}}">
                            @csrf
                            <div class="card  bg-white-100 shadow-lg border-dark border-radius-xl ">
                                <div class="card-body ">
                                    <div class="d-flex">
                                        <div class="avatar avatar-lg">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                                                <g id="Group_1842" data-name="Group 1842" transform="translate(-17096 -14051)">
                                                    <rect id="Rectangle_654" data-name="Rectangle 654" width="54.083" height="55" rx="16" transform="translate(17175 14322)" fill="#f3eef6"></rect>
                                                    <path id="Path_3872" data-name="Path 3872" d="M24.835,19.169a3.334,3.334,0,0,1,3.324,3.114l.007.219v2.672a3.334,3.334,0,0,1-6.658.22l-.007-.22V22.5A3.334,3.334,0,0,1,24.835,19.169ZM14.83,5.834a11.335,11.335,0,0,1,11.3,12.185,4.65,4.65,0,0,0-4.767,1.357A2.336,2.336,0,0,0,18.622,17.9l-.2.056-2,.672-.209.083-.192.1A2.346,2.346,0,0,0,14.9,21.385l.057.2.083.209.1.192.131.2a2.4,2.4,0,0,0,1.247.895l.211.053.105.016V27.5l.015.24v.587A11.335,11.335,0,1,1,14.828,5.835Zm5.331,14.2.008.133V27.5a1,1,0,0,1-1.991.136l-.009-.136V21.557l-.684.231a1,1,0,0,1-1.213-.5l-.053-.127a1,1,0,0,1,.505-1.213l.125-.053,2-.671a1,1,0,0,1,1.309.813Zm4.671,1.133a1.333,1.333,0,0,0-1.323,1.179L23.5,22.5v2.672a1.333,1.333,0,0,0,2.656.156l.008-.156V22.5a1.333,1.333,0,0,0-1.332-1.333Zm-10-11.332a1,1,0,0,0-.991.864l-.009.136v6l.009.136a1,1,0,0,0,1.982,0l.009-.136v-6L15.82,10.7a1,1,0,0,0-.991-.864ZM24.391,6l.108.083,1.534,1.332A1,1,0,0,1,24.828,9l-.109-.081L23.187,7.589A1,1,0,0,1,24.391,6ZM17.83,2.5a1,1,0,0,1,.136,1.991L17.83,4.5h-6a1,1,0,0,1-.136-1.991l.136-.009Z" transform="translate(17107.496 14062.5)" fill="#662d91"></path>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="ms-2 my-auto">
                                            <h6 class="mb-0">Purchase more pages </h6>
                                            <p class="text-xs mb-0"> <span class="text-dark mt-2 mb-0 h5 text-md">
                                                    <small>$</small>{{$pagesaddons->price}} <span class="text-sm h6 mt-2 mb-0">/ pages</span></span> </p>
                                        </div>
                                        </div>
                                    
                                    <hr />

                                    <div class="d-flex row">
                                       <div class="col-lg-5 col-sm-12">
                                            <div class="input-group mb-3">
                                                <input type="number" class="form-control" name="quantity" value="1" aria-label="Quantity of minutes to purchase" aria-describedby="Quantity of minutes to purchase" />
                                                <span class="input-group-text" id="unit">pages</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-sm-12">
                                            <button type="submit" class="btn btn-icon btn-3 btn-primary mb-0">
                                                <span class="btn-inner--icon">
                                                    <i class="fa fa-plus pe-2" aria-hidden="true"></i>
                                                </span>
                                                <span class="btn-inner--text ps-1">Purchase pages</span>
                                            </button>
                                        </div>
                                    </div>

                                    <!-- <div class="d-flex row">
                    <div class="col-lg-4">

                      <input type="number" class="form-control" name="quantity" value="1"> <label> minutes </label>
                    </div>
                    <div class="col-lg-8">


                      <button type="submit" class="btn btn-sm bg-gradient-primary mb-0">
                        <i class="fas fa-plus pe-2" aria-hidden="true"></i> purchase
                      </button>
                    </div>

                  </div> -->
                                </div>


                            </div>
                        </form>
                    </div>
                    @endif
                </div>
            </div>
        </div>


    </div>
</div>
@if(isset($usersubscription) && !empty($usersubscription) && $usersubscription->demopackage==1)

<div class="modal fade" id="qr-package-popup" tabindex="-1" role="dialog" aria-labelledby="qr-popup-label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg
  " role="document">
        <div class="modal-content p-0">
            <div class="modal-body p-0" id="package-popup-body">

                <section class="pricing-box-con">
                    <div class="container">
                        <h5 class="text-center">Upgrade your package to add QR Code</h5>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">


                                    <!-- tab 1 content start-->
                                    <div class="tab-pane fade show active" id="nav-ConnectionContent" data-toggle="tab-content">
                                        <div class="pricing-box-div">
                                            <div class="row">
                                                @if(isset($qrcodestandalone) && count($qrcodestandalone)>0)
                                                @foreach($qrcodestandalone as $up_key=> $dataup)

                                                <div class="col-md-6">
                                                    <form action="{{route('dashboard.purchase-subscription',$dataup->package_id)}}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="redirecturl" value="{{Request::url()}}">
                                                        <input type="hidden" name="upgradepackage" value="true">
                                                        <input type="hidden" name="package_id" value="{{$dataup->package_id}}">
                                                        <div class="pricing-box">

                                                            <div class="icon"><svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M8 6H6v2h2V6Z" fill="#773eb1" />
                                                                    <path d="M3 5.5A2.5 2.5 0 0 1 5.5 3h3A2.5 2.5 0 0 1 11 5.5v3A2.5 2.5 0 0 1 8.5 11h-3A2.5 2.5 0 0 1 3 8.5v-3ZM5.5 5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3ZM6 16h2v2H6v-2Z" fill="#773eb1" />
                                                                    <path d="M3 15.5A2.5 2.5 0 0 1 5.5 13h3a2.5 2.5 0 0 1 2.5 2.5v3A2.5 2.5 0 0 1 8.5 21h-3A2.5 2.5 0 0 1 3 18.5v-3Zm2.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3ZM18 6h-2v2h2V6Z" fill="#773eb1" />
                                                                    <path d="M15.5 3A2.5 2.5 0 0 0 13 5.5v3a2.5 2.5 0 0 0 2.5 2.5h3A2.5 2.5 0 0 0 21 8.5v-3A2.5 2.5 0 0 0 18.5 3h-3ZM15 5.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-3ZM13 13h2.75v2.75H13V13ZM18.25 15.75h-2.5v2.5H13V21h2.75v-2.75h2.5V21H21v-2.75h-2.75v-2.5ZM18.25 15.75V13H21v2.75h-2.75Z" fill="#773eb1" />
                                                                </svg>
                                                            </div>
                                                            <h4>{{$dataup->name}}</h4>

                                                            <p>{{$dataup->saleheadline}}</p>

                                                            <h5>${{$dataup->price}}</h5>

                                                            <h6><span>{{$dataup->salequotes}}</span></h6>

                                                            <div class="content text-start">
                                                                <p>{{$dataup->quotes}}</p>
                                                            </div>

                                                            <div class="list">
                                                                <br>
                                                                <p><strong>Package Inclusions</strong></p>
                                                                <ul>
                                                                    @if(isset($dataup->inclusions) && !empty($dataup->inclusions))
                                                                    @foreach($dataup->inclusions as $inclusiondata)
                                                                    @if($inclusiondata->level==1)
                                                                    <li class="text-sm"><?php echo $inclusiondata->inclusion_name; ?></li>
                                                                    @endif
                                                                    @if($inclusiondata->level==2)
                                                                    <ul>
                                                                        <li class="text-sm"><?php echo $inclusiondata->inclusion_name; ?></li>
                                                                    </ul>
                                                                    @endif
                                                                    @endforeach
                                                                    @endif
                                                                </ul>

                                                                @if(isset($community_packages_coming_soon[$dataup->package_id]) &&
                                                                count($community_packages_coming_soon[$dataup->package_id])>0)
                                                                <p><strong>Coming soon</strong></p>
                                                                @foreach($community_packages_coming_soon[$dataup->package_id] as $package_comming_soon)
                                                                <div class="row mt-3" @if($package_comming_soon->level == 2)
                                                                    style="padding-left:30px" @endif>
                                                                    <div class="col-2">
                                                                        <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                            <path d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2Zm3.22 6.97-4.47 4.47-1.97-1.97a.75.75 0 0 0-1.06 1.06l2.5 2.5a.75.75 0 0 0 1.06 0l5-5a.75.75 0 1 0-1.06-1.06Z" fill="#abb0b4" />
                                                                        </svg>
                                                                    </div>

                                                                    <span class="text-sm ps-2 col-10">{{$package_comming_soon->name}}</span>

                                                                </div>
                                                                @endforeach
                                                                @endif
                                                            </div>
                                                            <div class="button-con">


                                                                <button type="submit" name="purchasesubscription" value="notrial">Purchase Now</button>

                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>


                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="heirloom-package-popup" tabindex="-1" role="dialog" aria-labelledby="heirloom-popup-label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md
  " role="document">
        <div class="modal-content p-0">
            <div class="modal-body p-0" id="package-popup-body">

                <section class="pricing-box-con">
                    <div class="container">
                        <h6 class="text-center">Upgrade your package to add Heirloom Keepsake</h6>
                        <div class="row">
                            <div class="col-md-12">

                                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">


                                    <!-- tab 1 content start-->
                                    <div class="tab-pane fade show active" id="nav-ConnectionContent" data-toggle="tab-content">
                                        <div class="pricing-box-div">
                                            <div class="row">
                                                @if(isset($handdeliveredstandalone) && count($handdeliveredstandalone)>0)
                                                @foreach($handdeliveredstandalone as $h_key=> $hdataup)

                                                <div class="col-md-12">
                                                    <form action="{{route('dashboard.purchase-subscription',$hdataup->package_id)}}" method="post">
                                                        @csrf
                                                        <input type="hidden" name="upgradepackage" value="true">
                                                        <input type="hidden" name="redirecturl" value="{{Request::url()}}">
                                                        <input type="hidden" name="package_id" value="{{$hdataup->package_id}}">
                                                        <div class="pricing-box">

                                                            <div class="icon"><svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M11.25 13v9h-4A3.25 3.25 0 0 1 4 18.75V13h7.25ZM20 13v5.75A3.25 3.25 0 0 1 16.75 22h-4v-9H20ZM14.5 2a3.25 3.25 0 0 1 2.738 5.002L19.75 7c.69 0 1.25.466 1.25 1.042v2.916c0 .576-.56 1.042-1.25 1.042l-7-.001V7h-1.5v4.999l-7 .001C3.56 12 3 11.534 3 10.958V8.042C3 7.466 3.56 7 4.25 7l2.512.002A3.25 3.25 0 0 1 12 3.174 3.24 3.24 0 0 1 14.5 2Zm-5 1.5a1.75 1.75 0 0 0-.144 3.494L9.5 7h1.75V5.25l-.006-.144A1.75 1.75 0 0 0 9.5 3.5Zm5 0a1.75 1.75 0 0 0-1.75 1.75V7h1.75a1.75 1.75 0 1 0 0-3.5Z" fill="#773eb1" />
                                                                </svg>
                                                            </div>
                                                            <h4>{{$hdataup->name}}</h4>



                                                            <h5>${{$hdataup->price}}</h5>



                                                            <div class="list">
                                                                <br>
                                                                <p><strong>Package Inclusions</strong></p>
                                                                <ul>
                                                                    @if(isset($hdataup->inclusions) && !empty($hdataup->inclusions))
                                                                    @foreach($hdataup->inclusions as $inclusiondata)
                                                                    @if($inclusiondata->level==1)
                                                                    <li class="text-sm"><?php echo $inclusiondata->inclusion_name; ?></li>
                                                                    @endif
                                                                    @if($inclusiondata->level==2)
                                                                    <ul>
                                                                        <li class="text-sm"><?php echo $inclusiondata->inclusion_name; ?></li>
                                                                    </ul>
                                                                    @endif
                                                                    @endforeach
                                                                    @endif
                                                                </ul>

                                                                @if(isset($community_packages_coming_soon[$dataup->package_id]) &&
                                                                count($community_packages_coming_soon[$dataup->package_id])>0)
                                                                <p><strong>Coming soon</strong></p>
                                                                @foreach($community_packages_coming_soon[$dataup->package_id] as $package_comming_soon)
                                                                <div class="row mt-3" @if($package_comming_soon->level == 2)
                                                                    style="padding-left:30px" @endif>
                                                                    <div class="col-2">
                                                                        <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                            <path d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2Zm3.22 6.97-4.47 4.47-1.97-1.97a.75.75 0 0 0-1.06 1.06l2.5 2.5a.75.75 0 0 0 1.06 0l5-5a.75.75 0 1 0-1.06-1.06Z" fill="#abb0b4" />
                                                                        </svg>
                                                                    </div>

                                                                    <span class="text-sm ps-2 col-10">{{$package_comming_soon->name}}</span>

                                                                </div>
                                                                @endforeach
                                                                @endif
                                                            </div>
                                                            <div class="button-con">


                                                                <button type="submit" name="purchasesubscription" value="notrial">Purchase Now</button>

                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>


                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .pricing-page-wraper {
        width: 100%;
        background: #f3eef6;
        padding: 0 0 60px;
        float: left;
    }

    .pricing-page-menu {
        width: 100%;
        float: left;
        text-align: center;
        background: #fff;
        box-shadow: 0 3px 8px #0000003b;
        margin: 0;
        padding: 19px 0;
    }

    .pricing-page-menu nav {
        display: inline-block;
    }

    .pricing-page-menu ul li a,
    .pricing-page-menu nav .nav-fill .nav-item {
        display: inline-block;
        position: relative;
        padding: 0 0 1px;
        margin: 0 15px;
        color: #662d91;
        transition: all ease-in-out 1.0s;
        border: none;
    }

    .pricing-page-menu .nav-tabs {
        border-bottom: none;
    }

    .pricing-page-menu ul li a:before,
    .pricing-page-menu nav .nav-fill .nav-item:before {
        content: '';
        width: 0;
        height: 2px;
        position: absolute;
        background: #662d91;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        transition: all ease-in-out 1.0s;
    }

    .pricing-page-menu ul li a:hover,
    .pricing-page-menu nav .nav-fill .nav-item:before {
        color: #662d91;
        text-decoration: none;
    }

    .pricing-page-menu ul li:hover a:before,
    .pricing-page-menu ul li.active a:before,
    .pricing-page-menu nav .nav-fill .nav-item:hover:before,
    .pricing-page-menu nav .nav-fill .nav-item.active:before {
        width: 100%;
    }

    .pricing-page-menu ul {
        display: inline-block;
    }

    .pricing-page-menu ul li {
        display: inline-block;
    }

    section.pricing-box-con {
        text-align: center;
    }

    section.pricing-box-con {
        text-align: center;
        width: 100%;
        float: left;
        padding: 30px 0 0;
        position: relative;
        background: url(../assets/img/BackgroundLineWhite.svg) no-repeat;
        background-size: 60%;
    }

    .pricing-box-wrapper {
        width: 100%;
        float: left;
        background: #fff;
        padding: 30px;
        border-radius: 10px;
        box-shadow: 0 0 8px #0000003b;
        margin-bottom: 30px;
    }

    .heirloom-keepsake {
        margin-top: 50px;
    }



    .pricing-box-wrapper:before {
        content: '';
        position: absolute;
        top: 5%;
        right: 20%;
        background: url(../assets/img/element-1.png) no-repeat;
        width: 95px;
        height: 77px;
    }

    #nav-HeirloomKeepsake .pricing-box-wrapper:before {
        display: none;
    }

    .heading-group {
        width: 100%;
        float: left;
    }

    .heading-group h5 {
        font-size: 18px;
        color: #662d91;
        margin: 20px 0;
    }

    .heading-group h6 {
        margin: 0 0 30px;
    }

    .heading-group h6 span {
        display: inline-block;
        padding: 10px 21px;
        background: #f3eef6;
        border-radius: 10px;
        font-size: 14px;
        color: #662d91;
    }

    .heading-group h2 {
        margin-bottom: 0;
    }

    section.pricing-box-con nav {
        display: inline-block;
        border: 1px solid #f2f3f4;
        border-radius: 30px;
        overflow: hidden;
        background: #fbfbfc;
    }

    section.pricing-box-con nav .nav-tabs .nav-link {
        border-radius: 30px !important;
        color: #979899;
    }

    section.pricing-box-con nav .nav-tabs .nav-link span {
        background: #eef0f2;
        padding: 2px 5px;
        border-radius: 5px;
        font-size: 11px;
    }

    section.pricing-box-con.nav-tabs .nav-item.show .nav-link,
    section.pricing-box-con .nav-tabs .nav-link.active,
    section.pricing-box-con.nav-tabs .nav-item.show .nav-link:hover {
        color: #7d4da1;
        background-color: #fff;
        border-color: #7d4da1;
    }

    section.pricing-box-con-2 {
        width: 100%;
        float: left;
        padding: 60px 0;
    }

    section.pricing-box-con-2 .left {
        padding: 60px 0 0;
        position: relative;
    }

    section.pricing-box-con-2 .left .btn {
        background: #662d91;
        color: #fff;
    }

    section.pricing-box-con-2 .left .btn.btn-2 {
        background: #fff;
        color: #662d91;
        margin-right: 10px;
        padding: 7px 22px;
        border-radius: 30px;
    }

    section.pricing-box-con-2 h2 {
        color: #662d91;
        margin-bottom: 30px;
        background: url(../assets/img/b1.jpg) no-repeat;
        background-position: left bottom;
        padding-bottom: 0px;
    }

    .pricing-box {
        width: 100%;
        border: 2px solid #edeff1;
        padding: 30px;
        border-radius: 10px;
        margin-bottom: 30px;
    }

    .pricing-box {
        width: 100%;
        border: 2px solid #edeff1;
        padding: 30px;
        border-radius: 10px;
        margin-bottom: 30px;
        text-align: center;
        transition: all ease-in-out 0.5s;
    }

    .pricing-box:hover,
    .pricing-box.active {
        border-color: #662d91;
    }

    .pricing-box .button-con button:hover {
        background: #662d91;
        color: #fff;
    }

    .pricing-box .button-con button:first-child {
        background: #662d91;
        color: #fff;
    }

    .pricing-box .button-con button:first-child:hover {
        background: #f3eef6;
        color: #662d91;
    }

    .pricing-box .icon i {
        color: #662d91;
        font-size: 37px;
    }

    .pricing-box h4,
    .pricing-box h5 {
        font-family: 'Poppins', sans-serif;
        margin: 20px 0;
        font-size: 20px;
    }

    .pricing-box h4 span {
        display: inline-block;
        background: #f3eef6;
        padding: 9px 15px;
        border-radius: 10px;
        margin: 0 0 0 13px;
        color: #662d91;
        font-size: 14px;
        float: right;
    }

    .pricing-box h5 {
        font-size: 33px;
        color: #662d91;
        margin: 0 0;
    }

    .pricing-box h6 span {
        display: inline-block;
        background: #f3eef6;
        padding: 9px 22px;
        border-radius: 10px;
        margin: 10px 0;
        color: #662d91;
        font-size: 14px;
    }

    .pricing-box .list {
        text-align: left;
    }

    .pricing-box .list ul li {
        position: relative;
        padding-left: 24px;
        margin-bottom: 10px;
        list-style: none;
    }

    .pricing-box .list ul li ul {
        margin: 10px 0;
    }

    .pricing-box p {
        margin-top: 25px;
        margin-bottom: 5px;
    }

    .pricing-box .list ul li:before {
        font: normal normal normal 14px/1 FontAwesome;
        content: "\f00c";
        width: 15px;
        height: 15px;
        border-radius: 20px;
        background: #662d91;
        color: #fff;
        position: absolute;
        left: 0;
        top: 2px;
        font-size: 11px;
        text-align: center;
        line-height: 15px;
    }

    .pricing-box .list ul.sub li:before {
        background: #8b9197 !important;
    }

    .pricing-box .button-con {
        margin-top: 30px;
    }

    .pricing-box .button-con button {
        background: #f3eef6;
        border: 1px solid #f3eef6;
        width: 100%;
        padding: 10px;
        border-radius: 23px;
        color: #662d91;
        margin-top: 13px;
    }

    .pricing-expand-con {
        text-align: left;
        color: #9aa0a5;
    }

    .more,
    #more_2,
    #more_3,
    #more_4 {
        display: none;
    }

    .pricing-expand-content button span {
        font-size: 47px;
        font-weight: normal;
        position: absolute;
        top: -10px;
        right: 0;
        height: 10px;
    }

    .pricing-expand-content {
        position: relative;
        width: 100%;
        padding-top: 21px;
    }

    .pricing-expand-con p {
        margin-bottom: 8px;
        color: #9aa0a5;
    }

    .expand-content {
        border-top: 1px solid #edeeef;
        width: 100%;
        float: left;
        padding-top: 10px;
        margin: 10px 0 0;
    }

    .pricing-expand-content button {
        position: absolute;
        background: none;
        border: none;
        padding: 0;
        margin: 0;
        top: 0;
        left: 0;
        color: #9aa0a5;

    }

    .pricing-expand-content button:before {
        content: '';
    }

    .pricing-expand-content button:focus {
        border: none;
        outline: none;
        stroke: none;
    }

    .pricing-expand-content button {
        position: absolute;
        background: none;
        border: none;
        padding: 0;
        margin: 0;
        top: 0;
        left: 0;
        padding-right: 20px;
    }
</style>
@endif
<script type="text/javascript">
    $('.scrolltodiv').click(function(e) {
        var scrollto = $(this).data('scroll');
        $('html, body').animate({
            scrollTop: $(scrollto).offset().top
        }, 2000);
    });

    $(function() {
        var target = $('#myaccount-billing');
        if (target.length) {


            $('html,body').animate({
                scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    });

    $(document).on('click', '.showqr-package-popup', function(e) {

        $('#qr-package-popup').modal('show');


    });

    $(document).on('click', '.showheirloom-package-popup', function(e) {

        $('#heirloom-package-popup').modal('show');


    });
</script>

@endsection