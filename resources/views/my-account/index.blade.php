@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Account')

@section('headercommon')
<x-main.header icon="contact_card" title="My Account" subtitle="Manage Your Account Details" />
@endsection
@section('content')
<div>
  <div class="row mb-5">
    <div class="col-lg-3">
      <div class="card position-sticky top-1">
        <ul class="nav flex-column bg-white border-radius-lg p-3">
          <li class="nav-item mt-2">
            <a class="nav-link text-body scrolltodiv" data-scroll="#myaccount-addon">
              <div class="icon me-2">
               <i class="ni ni-diamond text-dark" ></i>
             </div>
             <span class="text-sm">Add-on Features</span>
           </a>
         </li>
         <li class="nav-item mt-2">
          <a class="nav-link text-body scrolltodiv" data-scroll="#myaccount-upgrade">
            <div class="icon me-2">
                <i class="fi fi_rocket text-dark"></i>
           </div>
           <span class="text-sm">Upgrade My Account</span>
         </a>
       </li>
       <li class="nav-item mt-2">
        <a class="nav-link text-body scrolltodiv" data-scroll="#myaccount-minutes">
          <div class="icon me-2">
                <i class="fi fi_clock-o text-dark"></i>
         </div>
         <span class="text-sm">Add Minutes</span>
       </a>
     </li>
     <li class="nav-item mt-2">
      <a class="nav-link text-body scrolltodiv" data-scroll="#myaccount-billing">
        <div class="icon me-2">
                <i class="fi fi_payment text-dark"></i>
       </div>
       <span class="text-sm">My Billing</span>
     </a>
   </li>
 </ul>
</div>
</div>
<div class="col-lg-9 mt-lg-0">



  <div class="card" id="myaccount-addon">
    <div class="card-header">
      <h5 class="p-0">  Add-on Features  &nbsp; <i class="ni ni-diamond text-dark mt-2" ></i> </h5>
      <p class="text-sm">Explore add-on features available to you</p>
    </div>

    <div class="card-body pt-0">

      <div class="row">
        <?php if(isset($heliroomaddon) && !empty($heliroomaddon)):?>

        <div class="col-md-6">
          @if(isset($usersubscription) && !empty($usersubscription))
          <form method="post" action="{{route('dashboard.purchase-subscription',$heliroomaddon->package_id)}}">
            <input type="hidden" name="package_id" value="{{$heliroomaddon->package_id}}">
            <input type="hidden" name="purchasesubscription" value="notrial">
            <input type="hidden" name="redirecturl" value="{{route('my-account')}}">
            @csrf
            @endif
            <div class="card mt-4">
                    @if(empty($usersubscription)) <span class="mask bg-orange border-radius-lg"></span>@endif
             <div class="card-body p-3">
              <div class="d-flex">
                <div class="avatar avatar-lg">
                  <img alt="" src="{{asset('assets/img/small-logos/logo-slack.svg')}}">
                </div>
                <div class="ms-2 my-auto">
                  <h6 class="mb-0">{{$heliroomaddon->name}}</h6>
                  <p class="text-xs mb-0"></p>
                </div>
              </div>
              <p class="mt-3"> Nearly all recipients tell us that this is the best gift they’ve ever received</p>
              
              <hr class="horizontal dark">
              <div class="d-flex">
                        <button type="submit" class="avatar avatar-sm border-1 rounded-circle bg-primary">
                          <i class="fi fi_add text-white" aria-hidden="true"></i>
              </button>
              <div class="rating ms-auto">
                          <i class="text-primary fi fi_star" aria-hidden="true"></i>
                          <i class="text-primary fi fi_star" aria-hidden="true"></i>
                          <i class="text-primary fi fi_star" aria-hidden="true"></i>
                          <i class="text-primary fi fi_star" aria-hidden="true"></i>
                          <i class="text-primary fi fi_star-half-alt" aria-hidden="true"></i>
              </div>
            </div>
          </div>
          @if(empty($usersubscription))
          <div class="position-relative d-flex flex-column justify-content-center" >
            <div class="container">
              <p class="text-white">Buy subscription plan</p>
              <button class="btn btn-icon btn-3 btn-white" type="button">
                <span class="btn-inner--icon"><i class="ni ni-lock-circle-open"></i></span>
                <span class="btn-inner--text">Unlock this feature Now</span>
              </button>
            </div>
          </div>

          @endif

        </div>
      </form>
    </div>

  <?php endif;?>
  <?php if(isset($qraddon) && !empty($qraddon)):?>
  <div class="col-md-6">
    @if(isset($usersubscription) && !empty($usersubscription))
    <form method="post" action="{{route('dashboard.purchase-subscription',$qraddon->package_id)}}">
      <input type="hidden" name="package_id" value="{{$qraddon->package_id}}">
      <input type="hidden" name="purchasesubscription" value="notrial">
      <input type="hidden" name="redirecturl" value="{{route('my-account')}}">
      @csrf
      @endif
      <div class="card mt-4">
                    @if(empty($usersubscription)) <span class="mask bg-orange border-radius-lg"></span>@endif
        <div class="card-body p-3">
          <div class="d-flex">
            <div class="avatar avatar-lg">
              <img alt="Image placeholder" src="{{asset('assets/img/small-logos/logo-slack.svg')}}">
            </div>
            <div class="ms-2 my-auto">
              <h6 class="mb-0">{{$qraddon->name}}</h6>
              <p class="text-xs mb-0"></p>
            </div>
          </div>
          <p class="mt-3"> Be remembered how you want to be remembered</p>

          <hr class="horizontal dark">
          <div class="d-flex">
                        <button type="submit" class="avatar avatar-sm border-1 rounded-circle bg-primary">
                          <i class="fi fi_add text-white" aria-hidden="true"></i>
          </button>
          <div class="rating ms-auto">
                          <i class="text-primary fi fi_star" aria-hidden="true"></i>
                          <i class="text-primary fi fi_star" aria-hidden="true"></i>
                          <i class="text-primary fi fi_star" aria-hidden="true"></i>
                          <i class="text-primary fi fi_star" aria-hidden="true"></i>
                          <i class="text-primary fi fi_star-half-alt" aria-hidden="true"></i>
          </div>
        </div>
      </div>
      @if(empty($usersubscription))
      <div class="position-relative d-flex flex-column justify-content-center" >
        <div class="container">
          <p class="text-white">Buy subscription plan</p>
          <button class="btn btn-icon btn-3 btn-white" type="button">
            <span class="btn-inner--icon"><i class="ni ni-lock-circle-open"></i></span>
            <span class="btn-inner--text">Unlock this feature Now</span>
          </button>
        </div>
      </div>

      @endif

    </div> 
  </form>
</div>

<?php endif;?>

</div>
</div>
</div>

<div class="card  mt-3" id="myaccount-upgrade">
  <div class="card-header">
          <h5 class="font-weight-bolder text-sm text-primary"> Upgrade My Account &nbsp; <i class="fi fi_rocket text-dark mt-2"></i> </h5>
    <p class="text-sm">Now Upgrade your subscription according to your requirements .</p>
  </div>

  <div class="card-body pt-0">
    <div class="row">

      @if(isset($usersubscription) && !empty($usersubscription))
      <div class="col-lg-6  mb-4 z-index-2">
        <form action="{{route('my-account.upgrade',$usersubscription->package_id)}}" method="post">

        <div class="card">
          <div class="card-header text-center pt-4 pb-3">
            <h6 class="text-dark opacity-8 text mb-0 mt-2">{{$usersubscription->name}}</h6>
            <h1 class="font-weight-bolder">
              <small>$</small>{{$usersubscription->plan_amount}}
            </h1>
          </div>
          <div class="card-body mx-auto pt-0">
            <div class="justify-content-start d-flex px-2 py-1">
              <div>
                <i class="fas fa-check text-info text-sm" aria-hidden="true"></i>
              </div>
              <div class="ps-2">
                <span class="text-sm">{{$usersubscription->message_item}}</span>
              </div>
            </div>
            <div class="justify-content-start d-flex px-2 py-1">
              <div>
                <i class="fas fa-check text-info text-sm" aria-hidden="true"></i>
              </div>
              <div class="ps-2">
                <span class="text-sm">{{$usersubscription->recipient_item}}</span>
              </div>
            </div>
            <div class="justify-content-start d-flex px-2 py-1">
              <div>
                <i class="fas fa-check text-info text-sm" aria-hidden="true"></i>
              </div>
              <div class="ps-2">
                <span class="text-sm">{{$usersubscription->written_item}}</span>
              </div>
            </div>

            @if(isset($useraddons) && count($useraddons)>0)

            <hr>
              <h6 class="text-dark opacity-8 text-center mb-4 mt-2">Your Extra Add-ons</h6>
              <div class="row">
              @foreach($useraddons as $useraddonsdata)
                <div class="col-4 mb-lg-auto mb-4 z-index-2 mt-2">
                <span class="position-absolute badge badge-secondary ">{{$useraddonsdata->quantity}}</span>
                <a href="javascript:;" class="avatar avatar-lg rounded-circle border border-primary">
                 <img alt="Image placeholder" src="{{asset('assets/img/small-logos/logo-slack.svg')}}">
                </a>
                <p class="mb-0 text-xs">{{$useraddonsdata->name}}</p>
                </div>
              @endforeach
              </div>

            @endif
            
          </div>
          @if(isset($usersubscription->upgradable) && $usersubscription->upgradable==1)
          <div class="card-footer pt-0">
                    <a href="javascript:;" class="submit-button btn btn-icon bg-info d-block mb-0">
              <i class="fas fa-arrow-right ms-1" aria-hidden="true"></i> &nbsp; Upgrade
            </a>
          </div>
          @else
          
          @endif
        </div>
      </form>
      </div>
      
      @else
            <span class="mask bg-orange border-radius-lg"></span>
      <div class="position-relative d-flex flex-column justify-content-center" >
        <div class="container">
          <p class="text-white">Click on the button below to subscribe to one of our packages</p>
          <a href="{{route('subscription-packages')}}" class="btn btn-icon btn-3 btn-white" type="button">
            <span class="btn-inner--icon"><i class="ni ni-credit-card"></i></span>
            <span class="btn-inner--text"> &nbsp; Buy subscrition plan</span>
          </a>
        </div>
      </div>
      @endif
    </div>
  </div>



</div>

<div class="card  mt-3" id="myaccount-minutes">
  <div class="card-header">
          <h5 class="p-0"> Minutes Tracker &nbsp; <i class="fi fi_clock-o text-dark mt-2"></i> </h5>
    <p class="text-sm">Details of how many minutes have left.</p>
  </div>



  <div class="card-body pt-0">
    <div class="row">
    <div class="col-md-6">
    <div class="card mt-4">
       
        <div class="card-body p-3">
          <div class="d-flex">
            <div class="avatar avatar-lg">
              <img alt="Image placeholder" src="{{asset('assets/img/small-logos/logo-slack.svg')}}">
            </div>
            <div class="ms-2 my-auto">
              <h6 class="mb-0">Free Up Minutes</h6>
              <p class="text-xs mb-0"></p>
            </div>
          </div>
          <hr class="horizontal dark">
          <div class="row ">
          <div class="col-lg-6">
                      <span class="text-primary text-xs "> Used : {{$usedtotalminutes}} minutes <i class="fi fi_clock-o text-dark "></i> </span>
          </div>
          <div class="col-lg-6">
                      <span class="text-dark text-xs"> Total : {{$gettotalminutes}} minutes <i class="fi fi_clock-o text-dark "></i> </span>
          </div>
          <div class="col-lg-12 mt-3">
                      <a data-route="{{route('my-messages.index')}}" type="button" class="link-handle btn btn-sm bg-primary mb-0">
                        <i class="fi fi_trash pe-2" aria-hidden="true"></i> free up minutes
           </a>
         </div>

          
        </div>
      </div>
     

    </div> 
  </div>
  @if(isset($extraaddons) && !empty($extraaddons))
  <div class="col-md-6">
    <form method="post" action="{{route('dashboard.purchase-subscription',$extraaddons->package_id)}}">
    <input type="hidden" name="package_id" value="{{$heliroomaddon->package_id}}">
    <input type="hidden" name="purchasesubscription" value="notrial">
    <input type="hidden" name="redirecturl" value="{{route('my-account')}}">
    @csrf
    <div class="card mt-4">
       
        <div class="card-body p-3">
          <div class="d-flex">
            <div class="avatar avatar-lg">
              <img alt="Image placeholder" src="{{asset('assets/img/small-logos/logo-slack.svg')}}">
            </div>
            <div class="ms-2 my-auto">
              <h6 class="mb-0">Purchase More Minutes </h6>
              <p class="text-xs mb-0"> <span class="text-dark mt-2 mb-0 h5">
                    <small>$</small>{{$extraaddons->price}} <span class="h6 mt-2 mb-0">/ minutes</span></span> </p>
            </div>
          </div>
          <hr class="horizontal dark">
          <div class="d-flex row">
            <div class="col-lg-4">

                        <input type="number" class="form-control" name="quantity" value="1"> <label> minutes </label>
                      </div>
                      <div class="col-lg-8">
                        <button type="submit" class="btn btn-sm bg-primary mb-0">
                          <i class="fi fi_add pe-2" aria-hidden="true"></i> purchase
                </button>
              </div>
          
            </div>
      </div>
     

    </div> 
  </form>
  </div>
  @endif



  </div>
</div>
</div>

<div class="card  mt-3" id="myaccount-billing">
  <div class="card-header">
          <h5 class="p-0"> My Billing &nbsp; <i class="fi fi_payment text-dark mt-2"></i> </h5>
    <p class="text-sm">Here you can manage your invoice details.</p>
  </div>
  <div class="card-body">
    @if(isset($userinvoices) && count($userinvoices)>0)
  <div class="card">
    <div class="card-body">
              
              <ul class="list-group">
                
                @foreach($userinvoices as $invoicedata)
                <li class="list-group-item border-0 justify-content-between p-0 m-0 border-radius-lg">
                  <div class="d-flex">
                    <div class="d-flex align-items-center">
                      <button class="btn btn-icon-only btn-rounded  @if($invoicedata->status=='paid') btn-outline-success @else btn-outline-danger @endif mb-0 me-3 p-3  d-flex align-items-center justify-content-center">
                         @if($invoicedata->status=='paid')
                         <i class="fas fa-check " aria-hidden="true"></i>
                         @else
                        <i class="fi fi_dismiss " aria-hidden="true"></i>
                         @endif
                       </button>
                      <div class="d-flex flex-column">
                        <h6 class="mb-1 text-dark text-sm">Invoice #{{$invoicedata->number}}</h6>
                        <span class="text-xs"><?php echo date('d F Y',strtotime($invoicedata->created_at));?>, at <?php echo date('h:m a',strtotime($invoicedata->created_at));?></span>
                        @if($invoicedata->status=='paid')
                        <span class="text-sm text-primary mt-1 ">
                          $ {{$invoicedata->amount_due}}.00 AUD <span class="mb-1 badge badge-sm bg-success mb-0 mt-2 ms-1">{{$invoicedata->status}} </span> </span>
                        @endif
                        @if($invoicedata->status!=='paid')
                        <span class="text-sm text-primary mt-1 ">
                          $ {{$invoicedata->amount_remaining}}.00 AUD <span class="mb-1 badge badge-sm bg-danger mb-0 mt-2 ms-1">Due amount</span> </span>
                        @endif
                        
                      </div>
                      
                    </div>
                    
                    @if($invoicedata->status=='paid')
                    <div class="d-flex align-items-center ms-6"> <a href="{{$invoicedata->hosted_invoice_url}}"> <span class="badge badge-lg bg-primary mb-0 mt-2">View</span> </a> </div>
                    @endif
                    @if($invoicedata->status!=='paid')
                    <div class="d-flex align-items-center ms-6"> <a href="{{$invoicedata->hosted_invoice_url}}"> <span class="badge badge-lg bg-primary mb-0 mt-2">Pay Now</span> </a> </div>
                    @endif
                    <div class="d-flex align-items-center ms-3"><a href="{{$invoicedata->invoice_pdf}}"><span class="badge badge-lg bg-orange mb-0 mt-2">Download</span></a></div>
                  </div>
                </li>
                @endforeach
                
              </ul>
              
            </div>
          </div>
          @else
                <blockquote class="text-sm"> - Sorry no Invoices Found !</blockquote> 
                @endif
      </div>
</div>




</div>
</div>
</div>
<script type="text/javascript">
  $('.scrolltodiv').click(function (e) {
    var scrollto=$(this).data('scroll');
    $('html, body').animate({
      scrollTop: $(scrollto).offset().top
    }, 2000);
  });

  $(function() {
    var target = $('#myaccount-billing');
    if (target.length) {


      $('html,body').animate({
        scrollTop: target.offset().top
      }, 1000);
      return false;
    }
  });
</script>
@endsection