@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb',' Minutes Tracker')

@section('headercommon')
<x-main.header icon="timer" title="Minutes Tracker" subtitle="Manage Your Minutes Details of how many minutes have left." />
@endsection

@section('content')
<div class="row mb-5">
  <div class="col-lg-12 mt-lg-0">
    <div class="card " id="myaccount-minutes">
      <div class="card-header pb-0">
        <nav style="--bs-breadcrumb-divider: '›';" aria-label="breadcrumb">
          <ol class="breadcrumb p-0 bg-white">
            <li class="breadcrumb-item"><a data-route="{{route('my-account')}}" class="link-handle h5 text-sm font-weight-bolder text-primary">My Account</a></li>
            <li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page">Minutes Tracker</li>
          </ol>
        </nav>
        <!-- <h5 class="font-weight-bolder text-sm text-primary">  <a data-route="{{route('my-account')}}" class="link-handle"><i class="fa fa-chevron-left text-primary" aria-hidden="true" ></i></a>  &nbsp; Minutes Tracker  &nbsp; <i class="fa fa-clock-o text-dark mt-2" ></i> </h5>
    <p class="text-sm">Details of how many minutes have left</p> -->

      </div>
      <div class="card-body pt-0">
        <div class="row">
          @if(isset($gettotalminutes) && ($gettotalminutes===0))
          <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="card h-100 bg-gray-100 shadow-lg border-dark border-radius-xl">


              <span class="mask bg-gradient-dark border-radius-lg"></span>
              <div class="card-body p-3">

                <div class="d-flex">
                  <div class="avatar avatar-lg">
                    <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                      <g id="Group_1868" data-name="Group 1868" transform="translate(-16342 -14028)">
                        <rect id="Rectangle_648" data-name="Rectangle 648" width="54.083" height="55" rx="16" transform="translate(16342 14028)" fill="#fef3f1"></rect>
                        <path id="Path_3923" data-name="Path 3923" d="M14.639,5.776A11.139,11.139,0,1,1,3.5,16.915,11.139,11.139,0,0,1,14.639,5.776Zm0,3.931a.983.983,0,0,0-.974.849l-.009.134v5.9l.009.134a.983.983,0,0,0,1.947,0l.009-.134v-5.9l-.009-.134a.983.983,0,0,0-.974-.849Zm9.4-3.77.107.08,1.506,1.31a.983.983,0,0,1-1.185,1.563l-.106-.08L22.852,7.5a.983.983,0,0,1,1.183-1.563ZM17.587,2.5a.983.983,0,0,1,.134,1.957l-.134.009h-5.9a.983.983,0,0,1-.134-1.957L11.69,2.5Z" transform="translate(16354.298 14040.223)" fill="#f58675"></path>
                      </g>
                    </svg>
                  </div>
                  <div class="ms-2 my-auto">
                    <h6 class="mb-0">Free Up Minutes</h6>
                    <p class="text-xs mb-0"></p>
                    <div class="row py-2">
                      <div class="col-auto">
                        <span class="text-dark text-xs "> Used : {{$usedtotalminutes}} minutes <i class="fa fa-clock-o text-dark " aria-hidden="true"></i> </span>
                      </div>
                      <div class="col-auto">
                        <span class="text-dark text-xs"> Total : {{$gettotalminutes}} minutes <i class="fa fa-clock-o text-dark " aria-hidden="true"></i> </span>
                      </div>
                    </div>
                  </div>
                </div>

                  <span class="mask bg-gradient-dark border-radius-lg"></span>
                  

                    <div class="position-relative d-flex flex-column justify-content-center" >
                    <div class="container">
                      <p class="text-white">Buy Plan</p>
                      <a  href="{{route('subscription-packages')}}" class="btn btn-icon btn-3 btn-white" type="button">
                        <span class="btn-inner--icon"><i class="ni ni-lock-circle-open"></i></span>
                        <span class="btn-inner--text">Unlock this feature Now</span>
                      </a>
                    </div>
                  </div>

                  </div>

                </div>
              </div>

         

          @else
          <div class="col-lg-4 col-md-6 col-sm-12">
            <div class="card mt-4">

              <div class="card-body p-3">
                <div class="d-flex">
                  <div class="avatar avatar-lg">
                    <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                      <g id="Group_1868" data-name="Group 1868" transform="translate(-16342 -14028)">
                        <rect id="Rectangle_648" data-name="Rectangle 648" width="54.083" height="55" rx="16" transform="translate(16342 14028)" fill="#fef3f1"></rect>
                        <path id="Path_3923" data-name="Path 3923" d="M14.639,5.776A11.139,11.139,0,1,1,3.5,16.915,11.139,11.139,0,0,1,14.639,5.776Zm0,3.931a.983.983,0,0,0-.974.849l-.009.134v5.9l.009.134a.983.983,0,0,0,1.947,0l.009-.134v-5.9l-.009-.134a.983.983,0,0,0-.974-.849Zm9.4-3.77.107.08,1.506,1.31a.983.983,0,0,1-1.185,1.563l-.106-.08L22.852,7.5a.983.983,0,0,1,1.183-1.563ZM17.587,2.5a.983.983,0,0,1,.134,1.957l-.134.009h-5.9a.983.983,0,0,1-.134-1.957L11.69,2.5Z" transform="translate(16354.298 14040.223)" fill="#f58675"></path>
                      </g>
                    </svg>
                  </div>
                  <div class="ms-2 my-auto">
                    <h6 class="mb-0">Free Up Minutes</h6>
                    <p class="text-xs mb-0"></p>
                    <div class="row py-2">
                      <div class="col-auto">
                        <span class="text-dark text-xs "> Used : {{$usedtotalminutes}} minutes <i class="fa fa-clock-o text-dark " aria-hidden="true"></i> </span>
                      </div>
                      <div class="col-auto">
                        <span class="text-dark text-xs"> Total : {{$gettotalminutes}} minutes <i class="fa fa-clock-o text-dark " aria-hidden="true"></i> </span>
                      </div>
                    </div>
                  </div>
                </div>
                <hr />
                <div class="row ">
                  <div class="col-lg-12">
                    <a data-route="{{route('my-messages.index')}}" type="button" class="link-handle btn btn-icon btn-3 btn-primary mb-0">
                      <span class="btn-inner--icon">
                        <i class="fa fa-trash pe-2" aria-hidden="true"></i>
                      </span>
                      <span class="btn-inner--text ps-1">Free up minutes</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endif

          @if(isset($extraaddons) && !empty($extraaddons))
          <div class="col-md-6">
            <form method="post" action="{{route('dashboard.purchase-subscription',$extraaddons->package_id)}}">
              <input type="hidden" name="package_id" value="{{$extraaddons->package_id}}">
              <input type="hidden" name="purchasesubscription" value="notrial">
              <input type="hidden" name="redirecturl" value="{{route('my-account')}}">
              @csrf
              <div class="card mt-4">
                    <div class="card-body p-3">
                  <div class="d-flex">
                  <div class="avatar avatar-lg">
                          <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                            <g id="Group_1842" data-name="Group 1842" transform="translate(-17096 -14051)">
                              <rect id="Rectangle_617" data-name="Rectangle 617" width="54.083" height="55" rx="16" transform="translate(17096 14051)" fill="#fef3f1"></rect>
                              <path id="Path_3872" data-name="Path 3872" d="M24.835,19.169a3.334,3.334,0,0,1,3.324,3.114l.007.219v2.672a3.334,3.334,0,0,1-6.658.22l-.007-.22V22.5A3.334,3.334,0,0,1,24.835,19.169ZM14.83,5.834a11.335,11.335,0,0,1,11.3,12.185,4.65,4.65,0,0,0-4.767,1.357A2.336,2.336,0,0,0,18.622,17.9l-.2.056-2,.672-.209.083-.192.1A2.346,2.346,0,0,0,14.9,21.385l.057.2.083.209.1.192.131.2a2.4,2.4,0,0,0,1.247.895l.211.053.105.016V27.5l.015.24v.587A11.335,11.335,0,1,1,14.828,5.835Zm5.331,14.2.008.133V27.5a1,1,0,0,1-1.991.136l-.009-.136V21.557l-.684.231a1,1,0,0,1-1.213-.5l-.053-.127a1,1,0,0,1,.505-1.213l.125-.053,2-.671a1,1,0,0,1,1.309.813Zm4.671,1.133a1.333,1.333,0,0,0-1.323,1.179L23.5,22.5v2.672a1.333,1.333,0,0,0,2.656.156l.008-.156V22.5a1.333,1.333,0,0,0-1.332-1.333Zm-10-11.332a1,1,0,0,0-.991.864l-.009.136v6l.009.136a1,1,0,0,0,1.982,0l.009-.136v-6L15.82,10.7a1,1,0,0,0-.991-.864ZM24.391,6l.108.083,1.534,1.332A1,1,0,0,1,24.828,9l-.109-.081L23.187,7.589A1,1,0,0,1,24.391,6ZM17.83,2.5a1,1,0,0,1,.136,1.991L17.83,4.5h-6a1,1,0,0,1-.136-1.991l.136-.009Z" transform="translate(17107.496 14062.5)" fill="#f58675"></path>
                            </g>
                          </svg>
                        </div>
                        <div class="ms-2 my-auto">
                          <h6 class="mb-0">Purchase More Minutes </h6>
                          <p class="text-xs mb-0"> <span class="text-dark mt-2 mb-0 h5">
                            <small>$</small>{{$extraaddons->price}} <span class="h6 mt-2 mb-0">/ minute</span></span> </p>
                        </div>
                    <!-- <div class="ms-2 my-auto">
                      <h6 class="mb-0">Purchase More Minutes </h6>
                      <p class="text-xs mb-0"> <span class="text-dark mt-2 mb-0 h5">
                          <small>$</small>{{$extraaddons->price}} <span class="h6 mt-2 mb-0">/ minutes</span></span> </p>
                    </div> -->
                  </div>
                  <hr />
                  <div class="d-flex row">
                        <div class="col-auto">
                          <div class="input-group mb-3">
                            <input type="number" class="form-control" name="quantity" value="1" aria-label="Quantity of minutes to purchase" aria-describedby="Quantity of minutes to purchase"/>
                            <span class="input-group-text" id="unit">minutes</span>
                          </div>
                        </div>
                        <div class="col-auto">
                          <button type="submit" class="btn btn-icon btn-3 btn-primary mb-0" >
                            <span class="btn-inner--icon">
                              <i class="fa fa-plus pe-2" aria-hidden="true"></i>
                            </span>
                            <span class="btn-inner--text ps-1">Purchase minutes</span>
                          </button>
                        </div>
                      </div>

                  <!-- <div class="d-flex row">
                    <div class="col-lg-4">

                      <input type="number" class="form-control" name="quantity" value="1"> <label> minutes </label>
                    </div>
                    <div class="col-lg-8">


                      <button type="submit" class="btn btn-sm bg-gradient-primary mb-0">
                        <i class="fas fa-plus pe-2" aria-hidden="true"></i> purchase
                      </button>
                    </div>

                  </div> -->
                </div>


              </div>
            </form>
          </div>
          @endif



        </div>
      </div>
    </div>








  </div>
</div>
<script type="text/javascript">
  $('.scrolltodiv').click(function(e) {
    var scrollto = $(this).data('scroll');
    $('html, body').animate({
      scrollTop: $(scrollto).offset().top
    }, 2000);
  });

  $(function() {
    var target = $('#myaccount-billing');
    if (target.length) {


      $('html,body').animate({
        scrollTop: target.offset().top
      }, 1000);
      return false;
    }
  });
</script>
@endsection