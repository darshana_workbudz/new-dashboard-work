@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Delegate Access')

@section('headercommon')
<x-main.header 
  icon="people" 
  title="Delegate Access" 
  subtitle="Manage Account access You have as Trusted third-party" 
/>
@endsection


@section('content')
<section class="px-3 py-2">
      <div class="row mt-4">

        <!-- Add-On Features -->
        <?php if(isset($allaccess) && count($allaccess)>0):?>
        <?php foreach($allaccess as $datavalue):?>
          <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <a data-route="{{route('my-account.switch-account-access',$datavalue->user_authorised_person_id)}}" class="link-handle" class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon">
                 <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                        <g id="Group_1864" data-name="Group 1864" transform="translate(-16401 -14025)">
                          <rect id="Rectangle_649" data-name="Rectangle 649" width="54.083" height="55" rx="16" transform="translate(16401 14025)" fill="#f3eef6"></rect>
                          <path id="Path_3904" data-name="Path 3904" d="M23.261,15.716a7.545,7.545,0,1,1-7.545,7.545,7.545,7.545,0,0,1,7.545-7.545Zm-7.514,2.743a8.918,8.918,0,0,0,.782,10.646,18.051,18.051,0,0,1-3.556.329c-4.691,0-8.154-1.471-10.269-4.44A3.772,3.772,0,0,1,2,22.807V21.545a3.085,3.085,0,0,1,3.085-3.086H15.748Zm3.885,4.316a.686.686,0,1,0-.97.97L21.4,26.488a.686.686,0,0,0,.97,0L27.861,21a.686.686,0,1,0-.97-.97l-5,5ZM12.972,2A6.859,6.859,0,1,1,6.114,8.863,6.859,6.859,0,0,1,12.972,2Z" transform="translate(16411.637 14036.096)" fill="#662d91"></path>
                        </g>
                      </svg>
                </div>
                <div class="description ps-5">
                  <h6 class="text-lg">{{$datavalue->ufirstname}} {{$datavalue->ulastname}}</h6>
                  
                  <p>Click here to access Account Now</p>

                 <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-lg ms-1" aria-hidden="true"></i>
                 </span>

                </div>
                </div>
              </div>
            </div>
          </a>
        </div>
        <?php endforeach;?>
        
      <?php endif;?>



        
      </div>
    </section>

@endsection