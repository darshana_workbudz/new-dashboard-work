@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Upgrade my plan')

@section('headercommon')
<x-main.header icon="heart" title="Upgrade my plan" subtitle="Manage Your Package Details" />
@endsection
@section('content')

<div class="row mb-5">

    <div class="col-lg-12 mt-lg-0">


        <div class="card " id="myaccount-upgrade">
            <div class="card-header pb-0">
                @if(isset($usersubscription) && !empty($usersubscription))
                <nav style="--bs-breadcrumb-divider: '›';" aria-label="breadcrumb">
                  <ol class="breadcrumb p-0 bg-white">
                    <li class="breadcrumb-item"><a data-route="{{route('my-account')}}" class="link-handle h5 text-sm font-weight-bolder text-primary">My Account</a></li>
                    <li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page">Upgrade My Plan</li>

                </ol>
            </nav>

            @else
            <nav style="--bs-breadcrumb-divider: '›';" aria-label="breadcrumb">
              <ol class="breadcrumb p-0 bg-white">
                <li class="breadcrumb-item"><a data-route="{{route('my-account')}}" class="link-handle h5 text-sm font-weight-bolder text-primary">My Account</a></li>
                <li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page"> My Plan</li>

            </ol>
        </nav>

        @endif
    </div>

    <div class="card-body pt-0">
        <div class="row">

            @if(isset($usersubscription) && !empty($usersubscription))
            <section class="pricing-box-con pt-0">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

                                @error('purchasesubscription')
                                <h5 class="form-text text-danger text-center text-xs ms-1">
                                    {{ $message }}
                                </h5>
                                @endif
                                <!-- tab 1 content start-->
                                <div class="tab-pane fade show active" id="nav-ConnectionContent" data-toggle="tab-content">
                                    <div class="pricing-box-div">
                                        <div class="row">


                                            <div class="col-md-6">

                                                <div class="pricing-box">

                                                    <div class="icon"><svg width=" 24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="m12.82 5.58-.82.822-.824-.824a5.375 5.375 0 1 0-7.601 7.602l7.895 7.895a.75.75 0 0 0 1.06 0l7.902-7.897a5.376 5.376 0 0 0-.001-7.599 5.38 5.38 0 0 0-7.611 0Z" fill="#662d91" />
                                                    </svg>
                                                </div>
                                                <h4>{{$usersubscription->name}}</h4>
                                                <div class="content">
                                                    <p>{{$usersubscription->saleheadline}}</p>
                                                </div>  


                                                <h5>${{$usersubscription->price}}</h5>
                                                <h6><span><strong>{{$usersubscription->salequotes}}</strong></span></h6>

                                                <p>{{$usersubscription->quotes}}</p>


                                                <div class="list">
                                                    <br>
                                                    <p><strong>Inclusions</strong></p>
                                                    <ul>
                                                        @if(isset($usersubscription->inclusions) && !empty($usersubscription->inclusions))
                                                        @foreach($usersubscription->inclusions as $inclusiondata)
                                                        @if($inclusiondata->level==1)
                                                        <li class="text-sm"><?php echo $inclusiondata->inclusion_name; ?></li>
                                                        @endif
                                                        @if($inclusiondata->level==2)
                                                        <ul>
                                                            <li class="text-sm"><?php echo $inclusiondata->inclusion_name; ?></li>
                                                        </ul>
                                                        @endif
                                                        @endforeach
                                                        @endif
                                                    </ul>


                                                    @if(isset($community_packages_coming_soon[$usersubscription->package_id]) &&
                                                    count($community_packages_coming_soon[$usersubscription->package_id])>0)
                                                    <p><strong>Coming soon</strong></p>
                                                    @foreach($community_packages_coming_soon[$usersubscription->package_id] as $package_comming_soon)
                                                    <div class="row mt-3" @if($package_comming_soon->level == 2)
                                                        style="padding-left:30px" @endif>
                                                        <div class="col-2">
                                                            <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2Zm3.22 6.97-4.47 4.47-1.97-1.97a.75.75 0 0 0-1.06 1.06l2.5 2.5a.75.75 0 0 0 1.06 0l5-5a.75.75 0 1 0-1.06-1.06Z" fill="#abb0b4" />
                                                            </svg>
                                                        </div>

                                                        <span class="text-sm ps-2 col-10">{{$package_comming_soon->name}}</span>

                                                    </div>
                                                    @endforeach
                                                    @endif
                                                </div>
                                                @if(isset($useraddons) && count($useraddons)>0)
                                                <br>
                                                <hr>

                                                <h6 class="text-dark opacity-8 text-center mb-4 mt-2">Your Extra Add-ons</h6>
                                                <div class="row">
                                                    @foreach($useraddons as $useraddonsdata)
                                                    <div class="col-4 mb-lg-auto mb-4 z-index-2 mt-2">
                                                        <span class="position-absolute badge badge-secondary ">{{$useraddonsdata->quantity}}</span>
                                                        
                                                        @if($useraddonsdata->package_id==6)
                                                        <a href="javascript:;" class="avatar avatar-lg">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                                                                <g id="Group_1854" data-name="Group 1854" transform="translate(-17101 -14322)">
                                                                    <rect id="Rectangle_655" data-name="Rectangle 655" width="54.083" height="55" rx="16" transform="translate(17101 14322)" fill="#f3eef6"></rect>
                                                                    <path id="Path_3905" data-name="Path 3905" d="M14.917,17.89v13H9.139A4.7,4.7,0,0,1,4.444,26.2V17.89Zm12.639,0V26.2a4.7,4.7,0,0,1-4.695,4.695H17.084v-13ZM19.611,2a4.695,4.695,0,0,1,3.955,7.225l3.629,0A1.676,1.676,0,0,1,29,10.728V14.94a1.676,1.676,0,0,1-1.806,1.505H17.084V9.223H14.917v7.221H4.806A1.676,1.676,0,0,1,3,14.94V10.728A1.676,1.676,0,0,1,4.806,9.223l3.629,0A4.695,4.695,0,0,1,16,3.7,4.68,4.68,0,0,1,19.611,2ZM12.389,4.167a2.528,2.528,0,0,0-.208,5.047l.208.009h2.528V6.695l-.009-.208A2.528,2.528,0,0,0,12.389,4.167Zm7.222,0a2.528,2.528,0,0,0-2.528,2.528V9.223h2.528a2.528,2.528,0,1,0,0-5.056Z" transform="translate(17112 14333.001)" fill="#662d91"></path>
                                                                </g>
                                                            </svg>
                                                        </a>
                                                        @elseif($useraddonsdata->package_id==7)
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                                                        <g id="Group_1855" data-name="Group 1855" transform="translate(-17175 -14322)">
                                                          <rect id="Rectangle_654" data-name="Rectangle 654" width="54.083" height="55" rx="16" transform="translate(17175 14322)" fill="#f3eef6"></rect>
                                                          <g id="Group_1852" data-name="Group 1852" transform="translate(17189 14337)">
                                                            <path id="Path_3906" data-name="Path 3906" d="M8.889,6H6V8.889H8.889Z" transform="translate(-1.667 -1.667)" fill="#662d91"></path>
                                                            <path id="Path_3907" data-name="Path 3907" d="M3,6.611A3.611,3.611,0,0,1,6.611,3h4.333a3.611,3.611,0,0,1,3.611,3.611v4.333a3.611,3.611,0,0,1-3.611,3.611H6.611A3.611,3.611,0,0,1,3,10.945Zm3.611-.722a.722.722,0,0,0-.722.722v4.333a.722.722,0,0,0,.722.722h4.333a.722.722,0,0,0,.722-.722V6.611a.722.722,0,0,0-.722-.722Zm.722,15.889h2.889v2.889H7.333Z" transform="translate(-3 -3)" fill="#662d91"></path>
                                                            <path id="Path_3908" data-name="Path 3908" d="M3,19.722a3.611,3.611,0,0,1,3.611-3.611h4.333a3.611,3.611,0,0,1,3.611,3.611v4.333a3.611,3.611,0,0,1-3.611,3.611H6.611A3.611,3.611,0,0,1,3,24.056ZM6.611,19a.722.722,0,0,0-.722.722v4.333a.722.722,0,0,0,.722.722h4.333a.722.722,0,0,0,.722-.722V19.722A.722.722,0,0,0,10.945,19ZM24.667,6H21.778V8.889h2.889Z" transform="translate(-3 -1.667)" fill="#662d91"></path>
                                                            <path id="Path_3909" data-name="Path 3909" d="M16.611,3A3.611,3.611,0,0,0,13,6.611v4.333a3.611,3.611,0,0,0,3.611,3.611h4.333a3.611,3.611,0,0,0,3.611-3.611V6.611A3.611,3.611,0,0,0,20.945,3Zm-.722,3.611a.722.722,0,0,1,.722-.722h4.333a.722.722,0,0,1,.722.722v4.333a.722.722,0,0,1-.722.722H16.611a.722.722,0,0,1-.722-.722ZM13,17.445h3.972v3.972H13Zm7.583,3.972H16.972v3.611H13V29h3.972V25.028h3.611V29h3.972V25.028H20.583Zm0,0V17.445h3.972v3.972Z" transform="translate(1.445 -3)" fill="#662d91"></path>
                                                          </g>
                                                        </g>
                                                      </svg>
                                                        @else
                                                        <a href="javascript:;" class="avatar avatar-lg">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                                                          <g id="Group_1842" data-name="Group 1842" transform="translate(-17096 -14051)">
                                                            <rect id="Rectangle_617" data-name="Rectangle 617" width="54.083" height="55" rx="16" transform="translate(17096 14051)" fill="#fef3f1"></rect>
                                                            <path id="Path_3872" data-name="Path 3872" d="M24.835,19.169a3.334,3.334,0,0,1,3.324,3.114l.007.219v2.672a3.334,3.334,0,0,1-6.658.22l-.007-.22V22.5A3.334,3.334,0,0,1,24.835,19.169ZM14.83,5.834a11.335,11.335,0,0,1,11.3,12.185,4.65,4.65,0,0,0-4.767,1.357A2.336,2.336,0,0,0,18.622,17.9l-.2.056-2,.672-.209.083-.192.1A2.346,2.346,0,0,0,14.9,21.385l.057.2.083.209.1.192.131.2a2.4,2.4,0,0,0,1.247.895l.211.053.105.016V27.5l.015.24v.587A11.335,11.335,0,1,1,14.828,5.835Zm5.331,14.2.008.133V27.5a1,1,0,0,1-1.991.136l-.009-.136V21.557l-.684.231a1,1,0,0,1-1.213-.5l-.053-.127a1,1,0,0,1,.505-1.213l.125-.053,2-.671a1,1,0,0,1,1.309.813Zm4.671,1.133a1.333,1.333,0,0,0-1.323,1.179L23.5,22.5v2.672a1.333,1.333,0,0,0,2.656.156l.008-.156V22.5a1.333,1.333,0,0,0-1.332-1.333Zm-10-11.332a1,1,0,0,0-.991.864l-.009.136v6l.009.136a1,1,0,0,0,1.982,0l.009-.136v-6L15.82,10.7a1,1,0,0,0-.991-.864ZM24.391,6l.108.083,1.534,1.332A1,1,0,0,1,24.828,9l-.109-.081L23.187,7.589A1,1,0,0,1,24.391,6ZM17.83,2.5a1,1,0,0,1,.136,1.991L17.83,4.5h-6a1,1,0,0,1-.136-1.991l.136-.009Z" transform="translate(17107.496 14062.5)" fill="#773eb1"></path>
                                                          </g>
                                                        </svg>
                                                        </a>
                                                        @endif
                                                        
                                                        
                                                      <p class="mb-0 text-xs">{{$useraddonsdata->name}}</p>
                                                  </div>
                                                  @endforeach
                                              </div>

                                              @endif

                                              @if(isset($usersubscription->upgradable) && $usersubscription->upgradable==1)
                                              <div class="card-footer pt-5">
                                                <a href="javascript:;" class="show-package-popup btn btn-icon bg-purple d-block mb-0 text-white">
                                                 Upgrade Package
                                             </a>
                                         </div>
                                         @endif

                                     </div>

                                 </div>

                             </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


        </section>

     @else
     <!-- <span class="mask bg-orange border-radius-lg"></span> -->
     <div class="position-relative d-flex flex-column justify-content-center">
        <div class="container">
            <!-- <p class="text-primary">Click the button below to buy one of our packages.</p> -->
            <a href="{{route('subscription-packages')}}" class="btn btn-icon btn-3 btn-white" type="button">
                <span class="btn-inner--icon"><i class="ni ni-credit-card"></i></span>
                <span class="btn-inner--text text-primary"> &nbsp; Buy subscrition plan</span>
            </a>
        </div>
    </div>
    @endif
</div>


</div>



</div>


<div class="modal fade" id="package-popup" tabindex="-1" role="dialog" aria-labelledby="package-popup-label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered @if(isset($upgradablepackages) && count($upgradablepackages)==1)
    modal-md @else modal-lg
    @endif " role="document">
    <div class="modal-content p-0">
        <div class="modal-body p-0" id="package-popup-body">

            <section class="pricing-box-con">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

                                @error('purchasesubscription')
                                <h5 class="form-text text-danger text-center text-xs ms-1">
                                    {{ $message }}
                                </h5>
                                @endif
                                <!-- tab 1 content start-->
                                <div class="tab-pane fade show active" id="nav-ConnectionContent" data-toggle="tab-content">
                                    <div class="pricing-box-wrapper">
                                        <div class="row">
                                            @if(isset($upgradablepackages) && count($upgradablepackages)>0)
                                            @foreach($upgradablepackages as $up_key=> $dataup)
                                            @if(isset($dataup) && !empty($dataup))


                                            @if(count($upgradablepackages)<=3) <?php $class = 'col-md-4' ?> @endif @if(count($upgradablepackages)<=2) <?php $class = 'col-md-6' ?> @endif @if(count($upgradablepackages)==1) <?php $class = 'col-md-12' ?> @endif <div class="{{$class}}">
                                                <form action="{{route('dashboard.purchase-subscription',$dataup->package_id)}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="upgradepackage" value="true">
                                                    <input type="hidden" name="package_id" value="{{$dataup->package_id}}">
                                                    <div class="pricing-box">

                                                        <div class="icon"><svg width=" 24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="m12.82 5.58-.82.822-.824-.824a5.375 5.375 0 1 0-7.601 7.602l7.895 7.895a.75.75 0 0 0 1.06 0l7.902-7.897a5.376 5.376 0 0 0-.001-7.599 5.38 5.38 0 0 0-7.611 0Z" fill="#662d91" />
                                                        </svg>
                                                    </div>
                                                    <h4>{{$dataup->name}}</h4>

                                                    @if($dataup->package_id == 1)
                                                    <p>Join the Evaheld Community</p>
                                                    @elseif($dataup->package_id == 2)
                                                    <p>Recommended</p>
                                                    @else($dataup->package_id == 3)
                                                    <p>Best Value</p>
                                                    @endif

                                                    <h5>${{$dataup->price}}</h5>
                                                    <h6><span>{{$dataup->salequotes}}</span></h6>

                                                    <p>{{$dataup->quotes}}</p>


                                                    <div class="list">
                                                        <br>
                                                        <p><strong>Inclusions</strong></p>
                                                        <ul>
                                                            @if(isset($dataup->inclusions) && !empty($dataup->inclusions))
                                                            @foreach($dataup->inclusions as $inclusiondata)
                                                            @if($inclusiondata->level==1)
                                                            <li class="text-sm"><?php echo $inclusiondata->inclusion_name; ?></li>
                                                            @endif
                                                            @if($inclusiondata->level==2)
                                                            <ul>
                                                                <li class="text-sm"><?php echo $inclusiondata->inclusion_name; ?></li>
                                                            </ul>
                                                            @endif
                                                            @endforeach
                                                            @endif
                                                        </ul>

                                                        @if(isset($community_packages_coming_soon[$dataup->package_id]) &&
                                                        count($community_packages_coming_soon[$dataup->package_id])>0)
                                                        <p><strong>Coming soon</strong></p>
                                                        @foreach($community_packages_coming_soon[$dataup->package_id] as $package_comming_soon)
                                                        <div class="row mt-3" @if($package_comming_soon->level == 2)
                                                            style="padding-left:30px" @endif>
                                                            <div class="col-2">
                                                                <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2Zm3.22 6.97-4.47 4.47-1.97-1.97a.75.75 0 0 0-1.06 1.06l2.5 2.5a.75.75 0 0 0 1.06 0l5-5a.75.75 0 1 0-1.06-1.06Z" fill="#abb0b4" />
                                                                </svg>
                                                            </div>

                                                            <span class="text-sm ps-2 col-10">{{$package_comming_soon->name}}</span>

                                                        </div>
                                                        @endforeach
                                                        @endif
                                                    </div>
                                                    <div class="button-con">


                                                        <button type="submit" name="purchasesubscription" value="notrial">Purchase Now</button>

                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        @endif
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
</div>
</div>
</div>




</div>
</div>

<script type="text/javascript">
    $('.scrolltodiv').click(function(e) {
        var scrollto = $(this).data('scroll');
        $('html, body').animate({
            scrollTop: $(scrollto).offset().top
        }, 2000);
    });

    $(function() {
        var target = $('#myaccount-billing');
        if (target.length) {


            $('html,body').animate({
                scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    });

    $('.show-package-popup').click(function(e) {

        $('#package-popup').modal('show');

    });
</script>

@endsection