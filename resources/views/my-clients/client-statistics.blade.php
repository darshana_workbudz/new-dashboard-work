@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Clients')

@section('headercommon')
<x-main.header 
  icon="person" 
  title="My Clients" 
  subtitle="Select the content type you’d like to manage and then select Update Preferences to manage your recipient, delivery date, authorisation and delivery style preferences"
  button
  buttonText="Invite more clients"
  buttonIcon="add"
  href="javascript:;"
/>
@endsection

@section('content')
<div class="row my-4">
  <div class="col-12">
    <div class="card">
      <div class="table-responsive">
        <table class="table align-items-center mb-0">
          <thead>
            <tr>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">First Name</th>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Last Name</th>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Email</th>
              <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Phone</th>
              <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Check-in Preference Status</th>
              <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Account Status</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <div class="d-flex px-2 py-1">
                  <div>
                    <img src="../../../assets/img/team-2.jpg" class="avatar avatar-sm me-3" alt="avatar image">
                  </div>
                  <div class="d-flex flex-column justify-content-center">
                    <h6 class="mb-0 text-sm">John</h6>
                  </div>
                </div>
              </td>
              <td>
                <p class="text-sm text-secondary mb-0">Michael</p>
              </td>
              <td>
                <span class="badge badge-dot me-4">
                  <i class="bg-info"></i>
                  <span class="text-dark text-xs">john@user.com</span>
                </span>
              </td>
              <td class="align-middle text-center text-sm">
                <p class="text-secondary mb-0 text-sm">1234567890</p>
              </td>
              <td class="align-middle text-center">
                <span class="text-secondary text-sm">Pending</span>
              </td>
              <td class="align-middle text-center">
                <span class="text-secondary text-sm">Invitation Sent</span>
              </td>
            </tr>
            <tr>
              <td>
                <div class="d-flex px-2 py-1">
                  <div>
                    <img src="../../../assets/img/team-2.jpg" class="avatar avatar-sm me-3" alt="avatar image">
                  </div>
                  <div class="d-flex flex-column justify-content-center">
                    <h6 class="mb-0 text-sm">John</h6>
                  </div>
                </div>
              </td>
              <td>
                <p class="text-sm text-secondary mb-0">Michael</p>
              </td>
              <td>
                <span class="badge badge-dot me-4">
                  <i class="bg-info"></i>
                  <span class="text-dark text-xs">john@user.com</span>
                </span>
              </td>
              <td class="align-middle text-center text-sm">
                <p class="text-secondary mb-0 text-sm">1234567890</p>
              </td>
              <td class="align-middle text-center">
                <span class="text-secondary text-sm">Pending</span>
              </td>
              <td class="align-middle text-center">
                <span class="text-secondary text-sm">Invitation Sent</span>
              </td>
            </tr>
            <tr>
              <td>
                <div class="d-flex px-2 py-1">
                  <div>
                    <img src="../../../assets/img/team-2.jpg" class="avatar avatar-sm me-3" alt="avatar image">
                  </div>
                  <div class="d-flex flex-column justify-content-center">
                    <h6 class="mb-0 text-sm">John</h6>
                  </div>
                </div>
              </td>
              <td>
                <p class="text-sm text-secondary mb-0">Michael</p>
              </td>
              <td>
                <span class="badge badge-dot me-4">
                  <i class="bg-info"></i>
                  <span class="text-dark text-xs">john@user.com</span>
                </span>
              </td>
              <td class="align-middle text-center text-sm">
                <p class="text-secondary mb-0 text-sm">1234567890</p>
              </td>
              <td class="align-middle text-center">
                <span class="text-secondary text-sm">Pending</span>
              </td>
              <td class="align-middle text-center">
                <span class="text-secondary text-sm">Invitation Sent</span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection