@extends(isset($isajax) && $isajax==true ? 'layouts-frontend.ajax' : 'layouts-frontend.master')
@section('content')
  <header class="header-2">
    <div class="page-header min-vh-65 relative" style="background-image: url('{{asset('assets/img/cover/grandparents-laughing-with-grandchildren.jpg')}}'); background-size:cover;">
    <span class="mask bg-primary"></span>
      <div class="container">
        <div class="row">
          <div class="col-lg-7 text-center mx-auto">
            <a class="navbar-brand font-weight-bolder link-handle" data-route="{{route('home')}}" rel="tooltip" data-placement="bottom" target="_blank">
                <img src="{{asset('assets/img/evaheld-logo-white.png')}}" style="width: 300px;">
            </a>
            <h1 class="text-white pt-3 ">Create Your Account</h1>
            <p class="lead mb-0 text-sm mx-auto text-white">Privately create and safe-keep messages and memorialisation content, resting assured that they will be securely and independently delivered to your loved ones and memorialised as intended, posthumously.</p>
          </div>
        </div>
      </div>
      <div class="position-absolute w-100 z-index-1 bottom-0">
        <svg class="waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 40" preserveAspectRatio="none" shape-rendering="auto">
          <defs>
            <path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
          </defs>
          <g class="moving-waves">
            <use xlink:href="#gentle-wave" x="48" y="-1" fill="rgba(255,255,255,0.40"></use>
            <use xlink:href="#gentle-wave" x="48" y="3" fill="rgba(255,255,255,0.35)"></use>
            <use xlink:href="#gentle-wave" x="48" y="5" fill="rgba(255,255,255,0.25)"></use>
            <use xlink:href="#gentle-wave" x="48" y="8" fill="rgba(255,255,255,0.20)"></use>
            <use xlink:href="#gentle-wave" x="48" y="13" fill="rgba(255,255,255,0.15)"></use>
            <use xlink:href="#gentle-wave" x="48" y="16" fill="rgba(255,255,255,0.95"></use>
          </g>
        </svg>
      </div>
    </div>
  </header>
    <div class="container">
      <?php $key=(isset($old->key) &&  !empty($old->key)) ? $old->key : Request::get('key'); ?>
      <div class="row blur shadow-blur mt-n6 border-radius-md pb-4 p-3 mx-sm-0 mx-1 position-relative z-index-3">
        
        <div class="card-body">
      <h3 class="text-primary text-gradient text-center font-weight-bolder mt-3 mb-3">Welcome, we're so glad you're here! </h3>

              
              
              <form  class="mt-5 validateForm individual-form" role="form" action="{{route('submit-registeration-client-form')}}" method="POST">
              
              <input type="hidden" name="isorganisation" value="0">
              <input type="hidden" name="key" value="{{$key}}">
              <h5 class="font-weight-bolder text-primary">Basic Information</h5>
              <div class="row" >
                <div class="col-12 col-sm-6" style="display: none;">
                  <label>Your Organisation Code<span class="text-danger">*</span></label>
                  <input class="form-control" type="text"  name="organisation_code" value="{{$userdata->organisation_code}}" readonly="">
                  
               </div>
               <div class="col-12 col-sm-6"></div>
             </div>
              <div class="row" >
               <div class="col-12 col-sm-6 mt-1">
                  <label>First Name <span class="text-danger">*</span></label>
                  <input class="form-control @if(isset($error['first_name'][0]) && !empty($error['first_name'][0])) is-invalid @endif" type="text"  name="first_name" value="{{$old->first_name}}" >
                  @if(isset($error['first_name'][0]) && !empty($error['first_name'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                  {{$error['first_name'][0]}}
                  </p>
                  @endif
                </div>
               <div class="col-12 col-sm-6 mt-1">
                  <label>Last Name <span class="text-danger">*</span></label>
                  <input class="form-control @if(isset($error['last_name'][0]) && !empty($error['last_name'][0])) is-invalid @endif" type="text" name="last_name" value="{{$old->last_name}}" >
                  @if(isset($error['last_name'][0]) && !empty($error['last_name'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                  {{$error['last_name'][0]}}
                  </p>
                  @endif
                </div>
              </div>
              <div class="row">
               <div class="col-12 col-sm-6 mt-1  mt-1">
                  <label>Email Address <span class="text-danger">*</span></label>
                  <input class="form-control @if(isset($error['email'][0]) && !empty($error['email'][0])) is-invalid @endif" type="text" name="email"  value="{{$old->email}}" >
                  @if(isset($error['email'][0]) && !empty($error['email'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                  {{$error['email'][0]}}
                  </p>
                  @endif
                </div>
                <div class="col-12 col-sm-6 mt-1">
                  <label>Date of Birth<span class="text-danger">*</span></label>
                  <div id="datepicker" class="input-group-alternative date custom-datepicker" data-date-format="dd-mm-yyyy">
                  <input required type="text" id="dob-date" value="{{$old->dob}}" name="dob" class=" date-validation form-control  @if(isset($error['dob'][0]) && !empty($error['dob'][0])) is-invalid @endif"  placeholder="dd-mm-yyyy"  />
                  
                  <span class="input-group-addon">
                  
                  </span>
                  </div>
                  @if(isset($error['dob'][0]) && !empty($error['dob'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                  {{$error['dob'][0]}}
                  </p>
                  @endif
                </div>
                
               <div class="col-12 col-sm-6 mt-1">
                  <label>Password <span class="text-danger">*</span></label>
                  <input class="password form-control @if(isset($error['password'][0]) && !empty($error['password'][0])) is-invalid @endif" type="password" name="password" id="pwd" value="{{$old->password}}" >
            <span toggle="#pwd" class="fi fi_eye_show field-icon toggle-password"></span>
                  <div id="pwd_strength_wrap" class="arrow-top">
                      <div id="passwordDescription" class="text-sm">Password not entered</div>
                      <div id="passwordStrength" class="strength0"></div>
                      <div id="pswd_info">
                          <p class="text-muted text-xs mb-1">
                              Please follow this guide for a strong password:
                              </p>
                             <ul class="text-muted ps-4 mb-0 float-start">

                                <li>
                                  <span class="mb-0 text-xs mx-auto" id="length">Min 6 characters</span>
                                </li>
                                <li>
                                  <span class="mb-0 text-xs mx-auto" id="spchar">One special characters (@,$,#)</span>
                                </li>
                                <li>
                                  <span class="mb-0 text-xs mx-auto" id="capital">One UpperCase Letter & small case Letters (Abcd)</span>
                                </li>
                                <li>
                                  <span class="mb-0 text-xs mx-auto" id="pnum">One number (2 are recommended)</span>
                                </li>
                                
                              </ul>
                      </div><!-- END pswd_info -->
                 </div>
                  @if(isset($error['password'][0]) && !empty($error['password'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                  {{$error['password'][0]}}
                  </p>
                  @endif
                </div>
                 
               <div class="col-12 col-sm-6 mt-1">
                  <label>Confirm Password <span class="text-danger">*</span></label>
                  <input class="password_confirmation form-control @if(isset($error['password_confirmation'][0]) && !empty($error['password_confirmation'][0])) is-invalid @endif" type="password" name="password_confirmation"  value="{{$old->password_confirmation}}" >
                  @if(isset($error['password_confirmation'][0]) && !empty($error['password_confirmation'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                  {{$error['password_confirmation'][0]}}
                  </p>
                  @endif
                </div>
              
              
              </div>
             
            <h6 class="font-weight-bolder text-sm mx-auto text-primary mt-4 mb-0 p-0">Some extra details would be great</h6>
            <p class="text-dark text-xs pt-1 ">Given the precious messages you’re entrusting us with, we want to make sure we can always reach you</p>
              <div class="row mt-1">

                <div class="col-12 col-sm-6 mt-1">
                  <div class="row">
                  <label>Phone Number</label>
                 <div class="col-4 mt-3 mt-sm-0 pr-0 mr-0">
                  <select name="phone_code" class="phonecodeselect2 form-control" style="width: 100%;">
                    @if(isset($phonecodes) && !empty($phonecodes))
                    @foreach($phonecodes as $data)
                    <option value="{{$data->id}}" data-code="{{$data->iso2}}" @if(isset($old->phone_code) && $old->phone_code==$data->id){{"selected"}} @elseif($data->iso2=='AU') {{"selected"}} @endif  >+{{$data->phone_code}} {{$data->iso2}} </option>
                    @endforeach
                    @endif
                    
                  </select>
                </div>
                <div class="col-8 mt-3 mt-sm-0 pl-0 ml-0">
                  <input id="phone_number" class="required form-control @if(isset($error['phone_number'][0]) && !empty($error['phone_number'][0])) is-invalid @endif" type="number" name="phone_number"  value="{{$old->phone_number}}">
                  @if(isset($error['phone_number'][0]) && !empty($error['phone_number'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                  {{$error['phone_number'][0]}}
                  </p>
                  @endif
                </div>
              </div>
              </div>

               <div class="col-12 col-sm-6 mt-1"></div>
               <div class="col-12 col-sm-6 mt-1">
                  <label>Reason I am creating my account</label>
                  <select name="reason" id="reason" class="select2 form-control" style="width: 100%;">
                      @if(isset($reason) && count($reason)>0)
                        @foreach($reason as $reasondata)
                        <option value="{{$reasondata->reason_id}}" @if(isset($old) && $old->reason==$reasondata->reason_id) {{'selected'}} @endif>{{$reasondata->description}}</option>
                        @endforeach
                      @endif
                    </select>
                </div>
              
                
                
                <div class="col-12 col-sm-6 mt-1">
                  <label>My Status</label>
                  <select name="mystatus" id="mystatus" class="select2 form-control" style="width: 100%;">
                      @if(isset($mystatus) && count($mystatus)>0)
                        @foreach($mystatus as $mystatusdata)
                        <option value="{{$mystatusdata->status_id}}" @if(isset($old) && $old->mystatus==$mystatusdata->status_id) {{'selected'}} @endif>{{$mystatusdata->description}}</option>
                        @endforeach
                      @endif
                    </select>
                </div>

                <div class="col-12 col-sm-6 mt-1 other_reason" @if($old->reason=='7') style="display: block;" @else style="display: none;" @endif >
                  <label>Other (Reason for creating the Account)</label>
                  <textarea class="form-control @if(isset($error['other_reason'][0]) && !empty($error['other_reason'][0])) is-invalid @endif" type="text" name="other_reason"   > {{$old->other_reason}}</textarea>
                  @if(isset($error['other_reason'][0]) && !empty($error['other_reason'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                  {{$error['other_reason'][0]}}
                  </p>
                  @endif
                </div>


                <div class="col-12 col-sm-6 mt-1 other_mystatus" @if($old->mystatus=='7') style="display: block;" @else style="display: none;" @endif >
                  <label>Other Option (My Status)</label>
                  <textarea class="form-control @if(isset($error['other_mystatus'][0]) && !empty($error['other_mystatus'][0])) is-invalid @endif" type="text" name="other_mystatus"  col="2" > {{$old->other_mystatus}}</textarea>
                  @if(isset($error['other_mystatus'][0]) && !empty($error['other_mystatus'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                  {{$error['other_mystatus'][0]}}
                  </p>
                  @endif
                </div>
              </div>

               <div class="row">
               <div class="col-12 col-sm-6 mt-1 field-wrapper address-lookup mt-1">
                <label> Your Address </label>
                <input class="form-control address-lookup__field" type="text" name="jls" autocomplete="off"  placeholder="&nbsp;">

                 <a  href="javascript:;" class="mt-1 show-manual text-xs float-end text-secondary" ><u>Manually enter my address</u></a>
                </div>
       

                

              

                </div>
                <div class="row show-manual-div" style="display: none;">
                  <input class="form-control" id="manual-address" name="manual" type="hidden" value="0">
                  <input class="form-control" name="city" type="hidden">
                  <input class="form-control" name="county" type="hidden">

                  <div class="col-12 col-sm-6 mt-1">
                    <label>Address Line 1 </label>
                    <input class="form-control @if(isset($error['address_line_1'][0]) && !empty($error['address_line_1'][0])) is-invalid @endif" type="text" name="address_line_1"  value="{{$old->address_line_1}}" >
                    @if(isset($error['address_line_1'][0]) && !empty($error['address_line_1'][0]))
                    <p class="form-text text-danger text-xs mb-1">
                    {{$error['address_line_1'][0]}}
                    </p>
                    @endif
                  </div>

                  <div class="col-12 col-sm-6 mt-1">
                    <label>Address Line 2</label>
                    <input class="form-control @if(isset($error['address_line_2'][0]) && !empty($error['address_line_2'][0])) is-invalid @endif" type="text" name="address_line_2"  value="{{$old->address_line_2}}" >
                    @if(isset($error['address_line_2'][0]) && !empty($error['address_line_2'][0]))
                    <p class="form-text text-danger text-xs mb-1">
                    {{$error['address_line_2'][0]}}
                    </p>
                    @endif
                  </div>

                 <div class="col-12 col-sm-6 mt-1">
                    <label>Postcode</label>
                    <input class="form-control @if(isset($error['postcode'][0]) && !empty($error['postcode'][0])) is-invalid @endif" name="postcode" type="number"  value="{{$old->postcode}}" >
                    @if(isset($error['postcode'][0]) && !empty($error['postcode'][0]))
                    <p class="form-text text-danger text-xs mb-1">
                    {{$error['postcode'][0]}}
                    </p>
                    @endif
                  </div>

                 <div class="col-12 col-sm-6 mt-1">
                    <label>State</label>
                    <input class="form-control @if(isset($error['state'][0]) && !empty($error['state'][0])) is-invalid @endif" name="state" type="text"  value="{{$old->state}}" >
                    @if(isset($error['state'][0]) && !empty($error['state'][0]))
                    <p class="form-text text-danger text-xs mb-1">
                    {{$error['state'][0]}}
                    </p>
                    @endif
                  </div>

                 <div class="col-12 col-sm-6 mt-1">
                    <label>Country</label>
                    <select name="country" id="country" class="select2 form-control @if(isset($error['country'][0]) && !empty($error['country'][0])) is-invalid @endif" style="width: 100%;">
                    @if(isset($phonecodes) && !empty($phonecodes))
                    <option value=""></option>
                    @foreach($phonecodes as $data)
                    <option value="{{$data->id}}"  @if(isset($old->country) && $old->country==$data->id){{"selected"}} @endif  > {{$data->name}} </option>
                    @endforeach
                    @endif
                    
                  </select>
                  </div>
                </div>
                
               
             
              <div class="text-center">
          <button type="button" class="submit-button btn btn-lg bg-primary w-100 mt-4 mb-0">Join Now</button>
               </div>
              

            </form>
           
             
              
          </div>

          
                  
        </div>
      </div>
    

  <script src="{{asset('assets/js/js-address-lookup.js')}}?v=1.0.3"></script>
  
  <script type="text/javascript">

   

    var startDate= new Date();
    startDate.setFullYear( startDate.getFullYear() - 100 );
    var endDate= new Date();
    endDate.setFullYear( endDate.getFullYear() - 22 );

    $('body').find(".custom-datepicker").datepicker({ 
      autoclose: true, 
      todayHighlight: true,
      format: 'mm/dd/yyyy',
      startDate: '01-01-1920',
      endDate: endDate
    });


     $('body').find('.password_confirmation').focusout(function(){
        form =$(this).closest('form');
        form.find('.validationmessage').remove();
        var pass = form.find('.password').val();
        var pass2 = form.find('.password_confirmation').val();
        if(pass != pass2){
            $(form.find('.password')).next().remove("p");
            $(form.find('.password')).addClass('is-invalid');
            $(this).addClass('is-invalid');
            $('<p class="validationmessage form-text text-danger text-xs mb-1">The password confirmation does not match</p>').insertAfter(form.find('.password'));
        }

    });



    $(document).on('change',"#reason" ,function() {
     
      $('.other_reason').hide();

      if($("#reason option:selected").text()=='Other')
      {
        $('.other_reason').show();
      }

    });

     $(document).on('change',"#mystatus" ,function() {
     
      $('.other_mystatus').hide();

      if($("#mystatus option:selected").text()=='Other')
      {
        $('.other_mystatus').show();
      }

    });

     $(document).on('change',"#organisation_role" ,function() {
     
      $('.other_role').hide();

      if($("#organisation_role option:selected").text()=='Other')
      {
        $('.other_role').show();
      }

    });

     $(document).on('click','.show-manual', function(e) {
      e.preventDefault();
        $(document).find('.show-manual-div').show();
        $(document).find('#manual-address').val(1);
    });


    $(document).on('focus keyup','#pwd', function(e) {
            $(document).find('#pwd_strength_wrap').fadeIn(100);
            var score = 0;
            var a = $(this).val();
            var desc = new Array();
     
            // strength desc
            desc[0] = "Too short";
            desc[1] = "Weak";
            desc[2] = "Good";
            desc[3] = "Strong";
            desc[4] = "Best";
             
            // password length
            if (a.length >= 6) {
                $("#length").removeClass("text-danger").addClass("text-success");
                score++;
            } else {
                $("#length").removeClass("text-success").addClass("text-danger");
            }
     
            // at least 1 digit in password
            if (a.match(/\d/)) {
                $("#pnum").removeClass("text-danger").addClass("text-success");
                score++;
            } else {
                $("#pnum").removeClass("text-success").addClass("text-danger");
            }
     
            // at least 1 capital & lower letter in password
            if (a.match(/[A-Z]/) && a.match(/[a-z]/)) {
                $("#capital").removeClass("text-danger").addClass("text-success");
                score++;
            } else {
                $("#capital").removeClass("text-success").addClass("text-danger");
            }
     
            // at least 1 special character in password {
            if ( a.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) ) {
                    $("#spchar").removeClass("text-danger").addClass("text-success");
                    score++;
            } else {
                    $("#spchar").removeClass("text-success").addClass("text-danger");
            }
     
     
            if(a.length > 0) {
                    //show strength text
                    $("#passwordDescription").text(desc[score]);
                    // show indicator
                    $("#passwordStrength").removeClass().addClass("strength"+score);
            } else {
                    $("#passwordDescription").text("Password not entered");
                    $("#passwordStrength").removeClass().addClass("strength"+score);
            }
            });
     
        
     
    $(document).on('blur','#pwd', function(e) {
        $("#pwd_strength_wrap").fadeOut(400);
    });
    

    $(document).on('click','.toggle-password', function(e) {


      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
      
    });
    

    
  
</script>
  
@endsection
