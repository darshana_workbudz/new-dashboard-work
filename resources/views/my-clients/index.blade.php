@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Clients')

@section('headercommon')
<x-main.header 
icon="people" 
title="My Clients"
subtitle="Copy link and invite your clients"
button
buttonText="Invite Clients"
buttonIcon="copy"
href="javascript:;"
link={{$invitation_link}}/>
@endsection

@section('content')

<div class="row">
  <div class="col-lg-12 mt-lg-0">
      <div class="row mt-3">
          @if(count($clients)>0)
          @foreach ($clients as $rkey => $clientdata)
          <div class="col-lg-4 col-md-6 mb-4">
            <div class="card bg-white-100 shadow-lg">
              <div class="card-body p-3">
                <div class="d-flex">
                  <!-- Avatar Large -->
                  <div class="avatar avatar-xl position-relative border-radius-xl bg-blue-pale d-none d-lg-block d-xl-block cursor-pointer" >
                    <div class="card-body z-index-3 text-center p-3">
                      <i class="fi fi_person_available text-blue" style="font-size: 42px;" aria-hidden="true"></i>
                    </div>
                  </div>
                  <!-- Avatar Small -->
                  <div class="avatar avatar-lg position-relative border-radius-xl bg-blue-pale d-block d-lg-none d-xl-none cursor-pointer" >
                    <div class="card-body z-index-3 text-center p-2 py-3">
                      <i class="fi fi_person_available text-blue" style="font-size: 28px" aria-hidden="true"></i>
                    </div>
                  </div>
                  <div class="ms-3 my-auto">
                    <div class="d-flex flex-column justify-content-center">

                      <h6 class="mb-0 ">{{ucfirst($clientdata->first_name
                        )}} {{ucfirst($clientdata->last_name)}}</h6>


                      </div>
                      <div class="avatar-group">




                       <p class="text-sm text-primary mb-1">{{$clientdata->email}}</p>


                       
                       <h6 class="text-xs text-dark mb-0">Total Coupons : {{$clientdata->couponcount}}</h6>
                       

                       
                     </div>
                   </div>
                   <div class="ms-auto">
                    <div class="dropdown">
                      <button onclick="ShowCouponDetails({{$clientdata->id}})" class="btn text-secondary  border-shadow p-2"  >
                        <i class="fa fa-tag text-lg text-primary" aria-hidden="true"></i>
                      </button>

                      <!-- <button onclick="ShowProfileDetails({{$clientdata->id}})" class=" btn text-secondary border-shadow p-2"  >
                        <i class="fa fa-info-circle text-lg text-primary" aria-hidden="true"></i>
                      </button> -->

                    </div>
                  </div>
                </div>
                <p class="text-sm mt-3">{{$clientdata->address_1}} {{$clientdata->address_2}} {{$clientdata->address_3}}  {{$clientdata->postcode}} {{$clientdata->country}}</p>
                <hr class="horizontal dark">
                <div class="row">
                  <div class="col-6 ">

                    @if(isset($clientdata->phone_number) && !empty($clientdata->phone_number))

                    <h6 class="text-xs mb-0 mx-1"><i class="fa fa-mobile" aria-hidden="true"></i>&nbsp; +{{$clientdata->phone_code}} {{$clientdata->phone_number}} </h6>

                    @endif

                    @if($clientdata->isactive==1)

                    <i class="fa fa-check-circle-o text-success mt-1" aria-hidden="true"></i>
                    <span class="text-xs text-dark">Active</span>

                    @else

                    <i class="fa fa-times-circle-o text-danger mt-1" aria-hidden="true"></i>
                    <span class="text-xs text-dark">In Active</span>

                    @endif

                      


                  </div>
                  <div class="col-6 text-end">
                    <h6 class="text-xs mb-0">{{date('d.m.Y',strtotime($clientdata->created_at))}}</h6>
                    <p class="text-primary text-xs font-weight-bold mb-0">Created date</p>
                    
                   
                  </div>




                </div>
                @if(isset($clientdata->default_prefrence) && !empty($clientdata->default_prefrence) && $clientdata->default_prefrence=='1') 

                    <?php $checkindate=date('d-M-Y');?>
                    @if($clientdata->stage1_completed=='0')
                    <?php $checkindate=$clientdata->stage1_date;?>
                    @elseif($clientdata->stage2_completed=='0')
                    <?php $checkindate=$clientdata->stage2_date;?>
                    @elseif($clientdata->stage3_completed=='0')
                    <?php $checkindate=$clientdata->stage3_date;?>
                    @endif
                 <div class="row ">

                  <div class="col-7">
                   <h6 class="text-xs mb-0 my-1 p-1"><i class="fa fa-clock"></i> Check-in Response
                    @if(isset($checkinemail) && !empty($checkinemail) && $checkinemail->isprocessed=='1' && strtotime(date('Y-m-d',strtotime($checkinemail->dispatch_date)))< strtotime(date('Y-m-d')) && $checkinemail->acknowledge=='0')
                    <br>
                    <span class="text-xs text-danger">{{'Missed'}} ({{date('d.m.Y',strtotime($checkinemail->date))}})</span>
                    @else
                    <span class="text-xs text-dark">{{'Pending'}} </span>
                    @endif
                   </span></h6>
                    
                  </div>

                  <div class="col-5 text-end">
                     @if(isset($checkindate) && !empty($checkindate))
                     <h6 class="text-xs mb-0 mt-1">{{date('d.m.Y',strtotime($checkindate))}}</h6>
                      <p class="text-primary text-xs font-weight-bold mb-0 ">Next Check In Date</p>
                      @endif
                  </div>

                  </div> 
                  @else
                  <div class="row ">

                  <div class="col-12">
                   <h6 class="text-xs mb-0 my-1"><i class="fa fa-clock"></i> Check-in Type : <span class="text-xs text-dark">My Trusted Third-Party</span></h6>
                    
                  </div>
                </div>

                @endif

                <div class="row ">
                  <div class="col-6">
                  <button type="button" class="mt-2 btn btn-outline-primary btn-sm mb-0 p-2" onclick="ShowAcknowledgment('{{$clientdata->id}}','{{$clientdata->first_name}}','{{$clientdata->last_name}}')">Advise of Death <i class="fa fa-external-link  mt-1" aria-hidden="true"></i></button>
                </div>

                  <div class="col-6 text-end">
                    @if(isset($clientdata->passed_date) && !empty($clientdata->passed_date))
                   <h6 class="text-xs mb-0 mt-2 ">{{date('d.m.Y',strtotime($clientdata->passed_date))}}</h6>
                    <p class="text-primary text-xs font-weight-bold mb-0 ">Passed Out date</p>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach

          @else
          <section class="px-3 py-2">

            <div class="row">


              <div class="col-lg-12 col-md-12 d-flex flex-column mt-3 border-radius-lg text-center">

                <div class="card h-100 card-background">
                  <div class="full-background"></div>
                  <div class="card-body z-index-3 text-center">
                    <h6 class="text-dark mb-4 mt-3">No Client's Found</h6> 

                  </div>

                </div>
              </div>
            </div>
          </section>

          @endif



        </div>
      </div>

    </div>


<div class="modal fade" id="CouponsDetails" tabindex="-1" role="dialog" aria-labelledby="CouponsDetailsLabel" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-centered modal-md" role="document">
    <div class="modal-content p-0" >
      <div class="modal-body p-0" id="CouponsDetailsBody">


      </div>
    </div>  
  </div>
</div>
<script type="text/javascript">
  function ShowCouponDetails(userid)
  {

    $.ajax({
      url: "{{route('shared-coupons-clients.details')}}",
      dataType: 'json',
      type: 'GET',
      data: {
        user_id:userid
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend: function() {
        jQuery(document).find('.loadingOverlay').show();

        jQuery(document).find('.loader').show();
      },
      success: function(response) {

        if(response.status=='success')
        {
          $("#CouponsDetailsBody").html(response.html);

          $('#CouponsDetails').modal('show');



        }
      },
        error: function(xhr) { // if error occured
          alert("Error occured.please try again");
        },
        complete: function() {

          $('.select2').select2();
          jQuery(document).find('.loadingOverlay').hide();
          jQuery(document).find('.loader').hide();


        },
      });

  }


  function ShowProfileDetails(userid)
  {

    $.ajax({
      url: "{{route('member-clients.statistics')}}",
      dataType: 'json',
      type: 'GET',
      data: {
        user_id:userid
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend: function() {
        jQuery(document).find('.loadingOverlay').show();

        jQuery(document).find('.loader').show();
      },
      success: function(response) {

        if(response.status=='success')
        {
          $("#CouponsDetailsBody").html(response.html);

          $('#CouponsDetails').modal('show');



        }
      },
        error: function(xhr) { // if error occured
          alert("Error occured.please try again");
        },
        complete: function() {

          $('.select2').select2();
          jQuery(document).find('.loadingOverlay').hide();
          jQuery(document).find('.loader').hide();


        },
      });

  }



  function HideCouponDetails()
  {
    $('#CouponsDetails').modal('hide');
  }

  function copyToClipboarddata(text) {
        var sampleTextarea = document.createElement("textarea");
        document.body.appendChild(sampleTextarea);
        sampleTextarea.value = text; //save main text in it
        sampleTextarea.select(); //select textarea contenrs
        document.execCommand("copy");
        document.body.removeChild(sampleTextarea);
    }

    function copyToClipboard(eleme) {
        var copyText = document.getElementById("copy-link");
        copyToClipboarddata(copyText.value);
    }


    function ShowAcknowledgment(id,firstname,lastname)
    {
      swal({
      text: "I confirm that "+firstname+" "+lastname+"  has passed away and that their Evaheld account and content can now be managed and distributed as per their personal account preferences",
      icon: "warning",

      buttons: {
       confirm: {
                  text: 'Acknowledge',
                  className: ' text-white bg-primary'
                },
       cancel: "Take me back"

      },
    }).then((will) => {

      if (will)
      {
        window.location="{{route('member-clients.statistics')}}?id="+id+"&acknowledge=1";
      }
      

      });
    }
    
</script>
@stop
