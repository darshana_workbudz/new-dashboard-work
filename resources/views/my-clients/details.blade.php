@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Client Details Statistics')

@section('headercommon')
<x-main.header 
icon="people" 
title="{{$clientdata->first_name}} {{$clientdata->last_name}}"
subtitle="My Client Details Statistics "

href="javascript:;"
/>
@endsection
<style type="text/css">
.ql-container {
  min-height: 10rem;
  height: 100%;
  flex: 1;
  display: flex;
  flex-direction: column;
}

.ql-editor {
  height: 100%;
  flex: 1;
  overflow-y: auto;
  width: 100%;
}
.ql-editor>p {
  color:black !important;
}
.custom-style-input {
  width: 100%;
  border:0;
  border-radius:0px;
  border-bottom:1px solid black;
  padding: 15px;
  font-size: 30px;
}
#descriptionbox
{
  height: 500px !important;
}

#counter {
  border: 1px solid #ccc;
  border-width: 0px 1px 1px 1px;
  color: #aaa;
  padding: 5px 15px;
  text-align: right;
}


</style>
@section('content')

<div class="row">
	<div class="col-lg-12 mt-lg-0">
		<div class="card">
			<div class="card-header pb-0">
				<nav style="--bs-breadcrumb-divider: '›';" aria-label="breadcrumb">
					<ol class="breadcrumb p-0 bg-white">
						<li class="breadcrumb-item"><a data-route="{{route('member.our-clients')}}" class="link-handle h5 text-sm font-weight-bolder text-primary">My Clients</a></li>
						<li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page">My Client Details Statistics</li>
					</ol>
				</nav>
			</div>
			<div class="card-body pt-0">  
				<form action="{{route('member-clients.update-status',$clientdata->user_id)}}" method="POST">
					@csrf
					<div class="row mt-4">
						<div class="col-12 col-sm-6">
							<label>Advise of Death</label>
							<div id="datepicker" class="input-group-alternative date custom-datepicker" data-date-format="dd-mm-yyyy">
								<input required type="text" id="passed_date-date" value="<?php echo isset($clientdata->passed_date) && !empty($clientdata->passed_date) ? date('d/m/Y', strtotime($clientdata->passed_date)) : '' ?>" name="passed_date" class=" date-validation form-control  @if(isset($error['passed_date'][0]) && !empty($error['passed_date'][0])) is-invalid @endif" placeholder="dd-mm-yyyy" />
								<span class="input-group-addon">
								</span>
							</div>
							@if(isset($error['passed_date'][0]) && !empty($error['passed_date'][0]))
							<p class="form-text text-danger text-xs mb-1">
								{{$error['passed_date'][0]}}
							</p>
							@endif
						</div>
						
						

					</div>
					<button class="btn bg-primary mb-0 submit-button mt-3 mb-3" type="button"> Update Date</button>
				</form>
				<div class="row mx-auto">
							<label>Upload Documents</label>


    						<form action="{{route('member-clients.upload-files',$clientdata->user_id)}}" class="form-control dropzone mt-2" id="dropzone">
				           <input type="hidden" name="type" value="">
				          @csrf
				          <div class="dz-message" data-dz-message><span><strong>Drop files here</strong> OR <strong>Upload files from my device</strong> </span></div>
				        </form>
				        
				        

				   </div>
				<div class="row mx-auto">
						
						@if(isset($clientdocuments) && count($clientdocuments)>0)
						<label class="p-3">Uploaded Documents list</label>
						@foreach($clientdocuments as $document)
						<div class="col-lg-2 col-sm-6 text-center shadow-lg border-radius-lg mx-2">
							<form action="{{ route('member-clients.delete-files', $clientdata->user_id)}}" method="post">
							@csrf
							<input type="hidden" name="filename" value="{{$document->uploadedfilename}}">
							<a class="float-end submit-button"><i class="fa fa-trash text-purple p-2" aria-hidden="true"></i>
							</a>
							</form>
							@if($document->capture_type=='uploaded' && $document->filetype=='pdf')
								
								<a href="{{route('member-clients.stream-files', ['id'=>$clientdata->user_id,'filename'=>$document->uploadedfilename])}}"  target="blank">
                
                  <i class="fi fi_document_one_page text-blue" style="font-size: 100px" aria-hidden="true"></i>
               
              	</a>
              
							
							
							@else
							<a href="{{route('member-clients.stream-files', ['id'=>$clientdata->user_id,'filename'=>$document->uploadedfilename])}}" target="blank" >
							<img  src="{{route('member-clients.stream-files',['id'=>$clientdata->user_id,'filename'=>$document->uploadedfilename])}}" style="width: 95px" class="p-2">
							</a>
							
							@endif
							<p class="text-xs">{{$document->title}}</p>
						
						</div>
						@endforeach
						@endif
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
  var gallery="{{route('member-clients.get-uploaded-files',$clientdata->user_id)}}";
  var delete_url="{{route('member-clients.delete-files',$clientdata->user_id)}}"
  var routetoredirect="{{route('member-clients.statistics')}}?id={{$clientdata->user_id}}&acknowledge=1&updated=1"

</script>
<script type="text/javascript">
  var startDate = new Date();
  var endDate = new Date();
  

  $('body').find(".custom-datepicker").datepicker({ 
    autoclose: true, 
    todayHighlight: true,
    format: 'dd/mm/yyyy',
    startDate: '01-01-1920',
    endDate: endDate
  });
</script>

<script src="{{asset('assets/js/plugins/quill.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/dropzone.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/dropzone-script.js')}}"></script>
<script type="text/javascript">

  
  Dropzone.discover();

  function LoadURL(obj,route,id)
	{
								var refreshdiv=$('body').find('.refresh-div');
									jQuery.ajax({
                    url:routetoredirect+'&ajax=true',
                    method: 'GET',
                    dataType: "json",
                    beforeSend: function() {
                        AjaxbeforeSend();
                    },
                  success: function(response) {

                       if(response.event=='refresh')
                        {
                          refreshdiv.html(response.html);
                          routetoredirect=removeParam('ajax',routetoredirect);
                          routetoredirect=removeParam('_token',routetoredirect);
                          window.history.pushState('page',"After I Go",routetoredirect);
                          swal({

									        title: "Well done!",  
									        text: "Documents Uploaded Successfully",
									        icon: "success",
									        buttons: {
									            confirm: {
									                text: 'ok',
									          
									            }
									        },
									        });
                        }
                    },
                    error: function(xhr) { // if error occured
                        alert("Error occured.please try again");
                    },
                    complete: function() {
                        Ajaxcomplete();
                    },
                });
                
	}

	

</script>
@stop
