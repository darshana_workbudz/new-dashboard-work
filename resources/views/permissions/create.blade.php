@extends('layouts.master')
@section('title', 'Create Permissions | SOS Admin Panel')

@section('styles')
@endsection

@section('breadcrumb-title', 'Create Permissions')
@section('breadcrumb-items')
<li class="breadcrumb-item">Permissions</li>
<li class="breadcrumb-item active">Permission List</li>
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
        <h5>Add Permissions </h5>
      </div>
      <div class="card-body">
        <form role="form" method="post" action="{{ action('PermissionController@store') }}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="form-group">
              <label for="name">Name:</label>
              <input type="text" class="form-control" id="name" name="name">
            </div>
          </div>
          @if(!$roles->isEmpty())
          <div class="header">
            <h4>Assign Permission to Roles</h4>
          </div>
          @foreach ($roles as $role)
          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="form-group">
              <div class="form-check">
                <input type="checkbox" id="{{$role->name}}" class="form-check-input" name="roles[]" value="<?php echo $role->id ?>">
                <label class="form-check-label" for="{{$role->name}}">{{$role->name}}</label>
              </div>
            </div>
          </div>
          @endforeach
          @endif
          <div class="col-lg-12">
            <div class="card">
              <button class="btn btn-primary" type="submit">Submit form</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

  @endsection
