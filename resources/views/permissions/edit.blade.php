@extends('layouts.master')
@section('title', 'Edit Permissions | SOS Admin Panel')

@section('styles')
@endsection

@section('breadcrumb-title', 'Edit Permissions')
@section('breadcrumb-items')
<li class="breadcrumb-item">Permissions</li>
<li class="breadcrumb-item active">Permission List</li>
@endsection
@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card">

      <div class="card-header">
        <h5>Edit Permissions </h5>
      </div>
      <div class="card-body">
        <form role="form" method="post" action="{{Request::root()}}/permissions/edit-permissions-post" enctype="multipart/form-data">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" value="<?php echo $permission->id ?>" name="permission_id">
          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="form-group">
              <label for="description">Permission Name :</label>
              <input type="text" value="<?php echo $permission->name ?>" class="form-control" id="description" name="name">
            </div>
          </div>
          <br>
          <div class="col-lg-12">
            <div class="card">
              <button class="btn btn-primary" type="submit">Submit form</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection
