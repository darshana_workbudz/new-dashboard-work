@extends('layouts.master')
@section('title', 'List Permissions | SOS Admin Panel')
@section('styles')

@endsection

@section('breadcrumb-title', 'List Permissions')
@section('breadcrumb-items')
<li class="breadcrumb-item">Permissions</li>
<li class="breadcrumb-item active">Permission List</li>
@endsection
@section('content')

<div class="row">
  <div class="col-sm-12">
    <div class="card">

      <div class="card-header">
        <h5>Manage Permissions </h5><span>Search and Manage Permissions details </span>
      </div>
      @if(count($permissions)>0)
      <div class="card-body">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
               <th scope="col">Permission Name</th>
               
               <th scope="col">Actions</th>
             </tr>
           </thead>

           <tbody>
            @foreach ($permissions as $permission)
            <tr>
              <td>{{ $permission->name }}</td>

              <td>
                  <a href="{{Request::root()}}/permissions/edit-permissions/{{$permission->id }}"><button class="btn btn-sm btn-danger" data-original-title="" title=""><i class="fi fi_edit"></i></button></a>
                   
                </td>   

              </tr>
              @endforeach
            </tbody>
          </table>

        </div>



      </div>
      @endif
    </div>
  </div>
</div>
@endsection
