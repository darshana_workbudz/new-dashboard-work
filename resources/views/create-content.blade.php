@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Create Messages')

@section('headercommon')
<x-main.header icon="video" title="What would you like to do today?" subtitle="To get started, choose the type of content you’d like to create" />
@endsection

@section('content')
<div>
  <div class="row mt-4">
    <div class="col-lg-4 col-md-4 d-flex flex-column mt-3 border-radius-lg">
      <a data-route="{{route('my-messages.create')}}" class="link-handle">
        <div class="card h-100 card-background align-items-start">
          <div class="full-background bg-primary"></div>
          <div class="card-body z-index-3">
          </div>
          <div class="card-footer pb-3 pt-2 z-index-3">
            <h6 class="text-white mb-1">Create a Personalised Message</h6>
          </div>
          <span class="mask bg-info border-radius-xl z-index-2 opacity-6"></span>
        </div>
      </a>
    </div>
    <div class="col-lg-4 col-md-4 d-flex flex-column mt-3 border-radius-lg">
      <a data-route="{{route('my-messages.create')}}" class="link-handle">
        <div class="card h-100 card-background align-items-start">
          <div class="full-background bg-primary"></div>
          <div class="card-body z-index-3">
          </div>
          <div class="card-footer pb-3 pt-2 z-index-3">
            <h6 class="text-white mb-1">Create QR Code Content</h6>
          </div>
          <span class="mask bg-info border-radius-xl z-index-2 opacity-6"></span>
        </div>
      </a>
    </div>
    <div class="col-lg-4 col-md-4 d-flex flex-column mt-3 border-radius-lg">
      <a data-route="{{route('my-messages.create')}}" class="link-handle">
        <div class="card h-100 card-background align-items-start">
          <div class="full-background bg-primary"></div>
          <div class="card-body z-index-3">
          </div>
          <div class="card-footer pb-3 pt-2 z-index-3">
            <h6 class="text-white mb-1">Create an Heirloom Keepsake</h6>
          </div>
          <span class="mask bg-info border-radius-xl z-index-2 opacity-6"></span>
        </div>
      </a>
    </div>
  </div>
</div>
@endsection