@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Account')

@section('headercommon')
<x-main.header icon="contact_card" title="My Account" subtitle="Manage Your Account Details" />
@endsection
@section('content')
<div>
  <div class="row mb-5">
    <div class="col-lg-3">
      <div class="card position-sticky top-1">
        <ul class="nav flex-column bg-white border-radius-lg p-3">
          <li class="nav-item mt-2">
            <a class="nav-link text-body scrolltodiv" data-scroll="#myaccount-addon">
              <div class="icon me-2">
               <i class="ni ni-diamond text-dark" ></i>
             </div>
             <span class="text-sm">Add-on Features</span>
           </a>
         </li>
         <li class="nav-item mt-2">
          <a class="nav-link text-body scrolltodiv" data-scroll="#myaccount-upgrade">
            <div class="icon me-2">
                <i class="fi fi_rocket text-dark"></i>
           </div>
           <span class="text-sm">Upgrade My Account</span>
         </a>
       </li>
       <li class="nav-item mt-2">
        <a class="nav-link text-body scrolltodiv" data-scroll="#myaccount-minutes">
          <div class="icon me-2">
                <i class="fi fi_clock-o text-dark"></i>
         </div>
         <span class="text-sm">Add Minutes</span>
       </a>
     </li>
     <li class="nav-item mt-2">
      <a class="nav-link text-body scrolltodiv" data-scroll="#myaccount-billing">
        <div class="icon me-2">
                <i class="fi fi_payment text-dark"></i>
       </div>
       <span class="text-sm">My Billing</span>
     </a>
   </li>
 </ul>
</div>
</div>







</div>

@endsection