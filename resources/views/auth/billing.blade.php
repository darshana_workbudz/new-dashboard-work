@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Recipients List')

@section('headercommon')
<x-main.header 
  icon="people" 
  title="My Recipients List" 
  subtitle="Add and manage recipients"
  button
  buttonText="Add Recipients"
  buttonIcon="add"
  data-route="{{route('manage-recipients.create')}}"
/>
@endsection

@section('content')
<section class="py-3">
  <div class="row mt-2">

  </div>
</section>
@endsection