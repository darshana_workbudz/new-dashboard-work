@extends(isset($isajax) && $isajax==true ? 'layouts-frontend.ajax' : 'layouts-frontend.master')

@section('content')
<div class="page-header min-vh-100">
  <div class="container">
    <div class="row">
      <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
        <div class="card card-plain mt-2">
          <div class="card-header pb-3 text-center">
              <img src="{{asset('assets/img/evaheld-logo.svg')}}" style="width:45%" alt="Evaheld logo" />
            <h4 class="font-weight-bolder mt-3">Welcome Back</h4>
            <p class="mb-0 text-sm mx-auto text-dark">Create and memorialise content effortlessly</p>
          </div>
          <div class="card-body pt-0 pb-0">
            <form role="form" method="POST" action="{{ route('login') }}">
              @csrf
              <label>Email</label>
              <div class="mb-1">
                <input type="email" class="form-control @if(isset($error['email'][0]) && !empty($error['email'][0])) is-invalid @endif @if(Session::has('alert')) is-invalid @endif" placeholder="your.email@domain.com" aria-label="Email" aria-describedby="email-addon" name="email" value="{{ $old->email }}">
                @if(Session::has('alert'))
                <p class="form-text text-danger text-xs mb-1">{{ Session::get('alert') }}</p>
                @endif
                @if(isset($error['email'][0]) && !empty($error['email'][0]))
                <p class="form-text text-danger text-xs mb-1">
                  {{$error['email'][0]}}
                </p>
                @endif
              </div>
              <label>Password</label>
              <div class="mb-3">
                <input type="password" class="form-control @if(isset($error['password'][0]) && !empty($error['password'][0])) is-invalid @endif" placeholder="Password" aria-label="Password" aria-describedby="password-addon" name="password">
                @if(isset($error['password'][0]) && !empty($error['password'][0]))
                <p class="form-text text-danger text-xs mb-1">
                  {{$error['password'][0]}}
                </p>
                @endif
              </div>
              <div class="form-check">
                <input class="form-check-input" type="checkbox" id="rememberMe" checked="" name="remember">
                <label class="form-check-label" for="rememberMe">Remember me</label>
              </div>
              <div class="text-center">
                <button type="button" class="submit-button btn bg-primary w-100 mt-1 mb-0 text-uppercase">Sign in</button>
              </div>
              <div class="mt-1 position-relative text-center">
                <p class="text-sm font-weight-bold mb-2 text-secondary text-border d-inline z-index-2 bg-white px-3">
                  OR
                </p>
              </div>
              <div class="text-center">
                <a type="button" href="{{route('auth.redirect')}}" class="btn w-100 mt-2 mb-0 btn-rounded btn-outline-dark mr-2 text-uppercase">
                  <img class="pe-2" src="{{asset('assets/img/logos/google.svg')}}" style="height: 22px;" alt="Google logo" />Sign Up with Google
                </a>
                <a type="button" href="{{route('facebook.auth.redirect')}}" class="btn w-100 mt-2 mb-0 btn-rounded btn-outline-dark text-uppercase">
                  <img class="pe-2" src="{{asset('assets/img/logos/facebook.svg')}}" style="height: 22px;" alt="Facebook logo" />Sign Up with Facebook
                </a>
              </div>
            </form>
          </div>
          <div class="card-footer text-center pt-2 p-0">
            <span class="text-sm mx-auto">
              <a data-route="{{Request::root()}}/password/reset" class="link-handle text-primary font-weight-bold">Reset my password</a>
            </span><br>
          </div>
          <div class="card-footer text-center pt-0 px-lg-2 px-1">
            <p class="text-sm mx-auto text-grey mb-1">
              Don't have an account?
              <a type="button" data-route="{{('verification-process')}}" class="link-handle text-primary font-weight-bold">Sign up</a>
            </p>
          </div>
        </div>
      </div>
      <div class="col-7 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 end-0 text-center justify-content-center flex-column" style="background-image: url('{{asset('assets/img/cover/portrait-of-smiling-seniors-enjoying-spending-time-together-SBI-300734000.jpg')}}'); background-size:cover;">
        <span class="mask bg-primary "></span>
        <div class="position-relative  m-3 px-7 d-flex flex-column justify-content-center">
          <div class="container mt-10">
            <h2 class="mt-10 text-white font-weight-bolder">Leave less unsaid</h2>
            <p class="text-white text-lg">Privately create and safe-keep messages and memorialisation content, resting assured that they will be securely and independently delivered to your loved ones and memorialised as intended, posthumously.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
