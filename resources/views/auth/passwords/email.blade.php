@extends(isset($isajax) && $isajax==true ? 'layouts-frontend.ajax' : 'layouts-frontend.master')
@section('content')
<div class="page-header min-vh-100">
      <div class="container">
        <div class="row">
           <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
            
            @if(isset($success) && !empty($success))
                <div class="card card-plain ">
                
                <div class="card-header pb-0 text-center">
                  <a class="navbar-brand font-weight-bolder m-0 link-handle" data-route="{{route('home')}}" rel="tooltip" data-placement="bottom" target="_blank">
            <img src="{{asset('assets/img/evaheld-logo.svg')}}" style="width:45%" alt="Evaheld logo" />
                  </a>
                <h4 class="font-weight-bolder mt-4">{{$success}}</h4>
                 <p class="mb-0 text-sm mx-auto">Please follow the password reset link emailed to you within 24 hours  to reset your password.</p> 
                </div>
                <div class="card-body">

                 
            
                  
                  <div class="buttons">

              <button type="button" data-route="{{route('password.request')}}"  class="resend-email btn btn-sm bg-primary w-100 mt-4 p-3 mb-0 link-handle">
                    Please resend my reset link  
              </button>
              </div>

              </div>
            </div>
            @else
            <div class="card card-plain mt-2">
          <div class="card-header pb-0 text-center">
                  <a class="navbar-brand font-weight-bolder m-0 link-handle" data-route="{{route('home')}}" rel="tooltip" data-placement="bottom" target="_blank">
              <img src="{{asset('assets/img/evaheld-logo.svg')}}" style="width:45%" alt="Evaheld logo" />
                  </a>
            <h4 class="font-weight-bolder mt-4">Reset My Password</h4>
            <p class="mb-0 text-sm mx-auto">Please provide your account email address and we’ll send you a link to reset your password.
</p>
                </div>
              <div class="card-body pb-3">
                <form method="POST" action="{{ route('password.email') }}">
                  @csrf   
                  <label>Email</label>
                  <div class="mb-3">
                <input id="email" type="email" class="form-control @if(isset($error['email'][0]) && !empty($error['email'][0])) is-invalid @endif @if(Session::has('alert')) is-invalid @endif" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="your.email@domain.com" >
                    @if(isset($error['email'][0]) && !empty($error['email'][0]))
                      <p class="form-text text-danger text-xs mb-1">
                      {{$error['email'][0]}}
                      </p>
                      @endif
                  </div>
                  <div class="text-center">
                <button type="button" class="submit-button btn bg-primary w-100 mt-1 mb-0">Send me a password reset link
</button>
                  </div>
                </form>
              </div>
            </div>
            @endif

        <div class="card-footer text-center pt-0 p-0">
          <span class="mb-4 text-sm mx-auto">
            Go back to <a data-route="{{route('login')}}" class="link-handle text-primary font-weight-bold">Login Page</a>
          </span><br>

        </div>
      </div>
      <div class="col-7 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 end-0 text-center justify-content-center flex-column" style="background-image: url('{{asset('assets/img/cover/happy-seniors-networking-together-at-home.jpg')}}'); background-size:cover;">
        <span class="mask bg-primary"></span>
              <div class="position-relative h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center " >
                
                <div class="container mt-10">
                <h2 class="mt-10 text-white font-weight-bolder">Leave less unsaid</h2>
                <p class="text-white text-lg">Privately create and safe-keep messages and memorialisation content, resting assured that they will be securely and independently delivered to your loved ones and memorialised as intended, posthumously.</p>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>

@endsection
