<section>
      <div class="page-header min-vh-100">
        <div class="container">
          <div class="row">
            <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
              <div class="card card-plain">
                <div class="card-header pb-0 text-start">
                  <h4 class="font-weight-bolder">Sign In</h4>
                  <p class="mb-0">Please confirm your email to proceed</p>
                </div>
                <div class="card-body">
                  <form role="form">
                    <div class="mb-3">
                      <input type="email" class="form-control form-control-lg" placeholder="Email" aria-label="Email">
                    </div>
                    
                   
                    <div class="text-center">
                      <button type="button" class="btn btn-lg bg-primary btn-lg w-100 mt-4 mb-0">Confirm Email</button>
                    </div>
                  </form>
                </div>
                <div class="card-footer text-center pt-0 px-lg-2 px-1">
                  <p class="mb-4 text-sm mx-auto">
                    Do you already have an account?
                    <a href="javascript:;" class="text-primary text-gradient font-weight-bold">Login In</a>
                  </p>
                </div>
              </div>
            </div>
             <div class="col-7 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 end-0 text-center justify-content-center flex-column" style="background-image: url('{{asset('assets/img/cover/happy-seniors-networking-together-at-home.jpg')}}'); background-size:cover;">
              <span class="mask bg-primary"></span>
              <div class="position-relative h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center " >
                
                <div class="container mt-10">
                <h2 class="mt-10 text-white font-weight-bolder">Leave less unsaid</h2>
                <p class="text-white text-lg">Privately create and safe-keep messages and memorialisation content, resting assured that they will be securely and independently delivered to your loved ones and memorialised as intended, posthumously.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>