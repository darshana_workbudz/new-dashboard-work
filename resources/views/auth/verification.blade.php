@extends(isset($isajax) && $isajax==true ? 'layouts-frontend.ajax' : 'layouts-frontend.master')

@section('content')
<section>
  <div class="page-header min-vh-100">
    <div class="container">
      <div class="row">
        <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
          <div class="card card-plain">
            <div class="card-header pb-3 text-center">
              <a class="navbar-brand font-weight-bolder m-0 link-handle" data-route="{{route('home')}}" rel="tooltip" data-placement="bottom" target="_blank">
                <img src="{{asset('assets/img/evaheld-logo.svg')}}" style="width:45%" alt="Evaheld logo" />
              </a>
              <h4 class="font-weight-bolder mt-4">Create your account</h4>
              <p class="mb-0 text-sm mx-auto text-dark">We’ll email you a magic link to sign in</p>
            </div>
            <div class="card-body pt-0 pb-0">
              <form role="form" action="{{route('submit-for-verification')}}" method="POST">
                 <input type="hidden" value="{{Request::get('package')}}" name="package_id" >
                <div class="mb-2">
                  <label>Email</label>
                  <input type="email" value="{{ $old->email }}" class="form-control @if(isset($error['email'][0]) && !empty($error['email'][0])) is-invalid @endif" placeholder="Email" aria-label="Email" name="email">
                  @if(isset($error['email'][0]) && !empty($error['email'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['email'][0]}}
                  </p>
                  @endif
                </div>
                <div class="text-center">
                  <button type="button" class="submit-button btn bg-primary w-100 mt-1 mb-0 text-uppercase">Get my magic link</button>
                </div>
                <div class="mt-1 position-relative text-center">
                  <p class="text-sm font-weight-bold mb-2 text-secondary text-border d-inline z-index-2 bg-white px-3">
                    OR
                  </p>
                </div>
                <div class="text-center">
                  <a type="button" href="{{route('auth.redirect')}}?package={{Request::get('package')}}" class="btn w-100 mt-2 mb-0 btn-rounded btn-outline-dark mr-2 text-uppercase">
                    <img class="pe-2" src="{{asset('assets/img/logos/google.svg')}}" style="height: 22px;" alt="Google logo" />Sign Up with Google
                  </a>
                  <a type="button" href="{{route('facebook.auth.redirect')}}?package={{Request::get('package')}}" class="btn w-100 mt-2 mb-0 btn-rounded btn-outline-dark text-uppercase">
                    <img class="pe-2" src="{{asset('assets/img/logos/facebook.svg')}}" style="height: 22px;" alt="Facebook logo" />Sign Up with Facebook
                  </a>
                </div>
              </form>
            </div>
            <p class="text-dark text-sm mx-auto mb-2 pt-4">
              Already have an account?
              <a type="button" data-route="{{('login')}}" class="link-handle text-primary font-weight-bold">Login</a>
            </p>
            <div class="card-footer text-center pt-3 px-lg-3 px-2">
              <p class="text-xs mx-auto text-dark mb-0">
                By creating an account, you agree to Evaheld’s <a href="https://evaheld.com/terms-of-service.html" class="text-primary ">Terms of Service</a> and <a href="https://evaheld.com/privacy-policy.html" class="text-primary ">Privacy Policy</a>.
              </p>
              <p class="text-xs mx-auto text-dark mb-1 pt-1">
                This site is protected by reCAPTCHA and Google <a href="https://policies.google.com/privacy" class="text-primary ">Privacy Policy</a> and <a href="https://policies.google.com/terms" class="text-primary ">Terms of Service</a> apply
              </p>
            </div>
          </div>
        </div>
        <div class="col-7 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 end-0 text-center justify-content-center flex-column" style="background-image: url('{{asset('assets/img/cover/unrecognizable-grandmother-and-her-granddaughter-holding-hands.jpg')}}'); background-size:cover;">
          <span class="mask bg-primary "></span>
          <div class="position-relative  m-3 px-7 d-flex flex-column justify-content-center">
            <div class="container mt-10">
              <h2 class="mt-10 text-white font-weight-bolder">Leave less unsaid</h2>
              <p class="text-white text-lg">Privately create and safe-keep messages and memorialisation content, resting assured that they will be securely and independently delivered to your loved ones and memorialised as intended, posthumously.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection