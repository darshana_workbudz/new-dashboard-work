<div class="card h-100">
    <div class="card-header pb-0">
        @if(isset($actiontype) && $actiontype=='update-recipients')
        <h5 class="text-sm text-primary mb-0">Message Recipient</h5>
        @endif

        @if(isset($actiontype) && $actiontype=='update-deliverdate')
        <h5 class="text-sm text-primary mb-0">Message Delivery Date</h5>
        @endif

        @if(isset($actiontype) && $actiontype=='update-deliverystyle')
        <h5 class="text-sm text-primary mb-0">Message Delivery Style</h5>

        <p class="text-xs pt-0 mt-0">
            Currently, your account includes digital delivery only. However, you can add a hand delivered option for any of your messages.
            <hr>
        </p>
        @endif

        @if(isset($actiontype) && $actiontype=='update-authorisation')
        <h5 class="text-sm text-primary mb-0">Message Delivery Authorisation</h5>
        @endif

        @if(isset($actiontype) && $actiontype=='update-message')
        <h5 class="text-sm text-primary mb-0">Update Message </h5>

        <p class="text-xs pt-0 mt-0">
            please select one of the below option to proceed
            <hr>
        </p>
        @endif

        @if(isset($actiontype) && $actiontype=='clone-message')
        <h5 class="text-sm text-primary mb-0">Duplicate the Message </h5>
        @endif

        @if(isset($actiontype) && $actiontype=='delete-message')
        <h5 class="text-sm text-primary mb-0">Delete Message Permanently</h5>
        @endif

        @if(isset($actiontype) && $actiontype=='update-qrcode')
        <h5 class="text-sm text-primary mb-0">Message Delivery Style</h5>

        <p class="text-xs pt-0 mt-0">
            Currently, your account includes digital delivery only. However, you can add a QR Code to your account.
            <hr>
        </p>
        @endif




    </div>

    @if(isset($actiontype) && $actiontype=='update-recipients')
    <form method="POST" action="{{route('my-messages.take-action-message-details')}}">
        <input type="hidden" name="actiontype" value="update-recipients">
        <input type="hidden" name="messagetype" value="{{$messagetype}}">
        <input type="hidden" name="id" value="{{$messageid}}">
        <div class="card-body px-4 pt-0 mt-0">
            <div class="row">
                <div class="col-10 ">


                    <label class="mt-4 mb-2 text-primary text-sm">Please select the recipient of this message</label>
                    <select name="recipient" id="recipient" class="select2 form-control @if(isset($error['recipient'][0]) && !empty($error['recipient'][0])) {{'is-invalid'}} @endif" style="width: 100%;">
                        @if(isset($recipients) && count($recipients)>0)
                        <option value="">Please Select</option>

                        @foreach($recipients as $recipientsdata)
                        <option value="{{$recipientsdata->user_recipient_id}}" @if($messagedata->user_recipient_id==$recipientsdata->user_recipient_id) {{'selected'}} @endif

                            >{{$recipientsdata->first_name}} {{$recipientsdata->last_name}}</option>
                        @endforeach
                        @endif



                    </select>

                    @if(isset($error['recipient'][0]) && !empty($error['recipient'][0]))
                    <p class="form-text text-danger text-xs mb-1">
                        {{$error['recipient'][0]}}
                    </p>
                    @endif

                </div>
            </div>
        </div>

        <div class="card-footer  d-flex">

            <div class="ms-auto">
                <button type="button" class="btn bg-gradient-primary btn-xs mx-2 TakeActionsMessage">Save changes</button>
                <button type="button" class="btn bg-gradient-secondary btn-xs" onclick="OpenMessageDetails('{{$messagetype}}','{{$messageid}}')">Go Back</button>
            </div>
        </div>
        @endif


        @if(isset($actiontype) && $actiontype=='update-deliverdate')
        <form method="POST" action="{{route('my-messages.take-action-message-details')}}">
            <input type="hidden" name="actiontype" value="update-deliverdate">
            <input type="hidden" name="messagetype" value="{{$messagetype}}">
            <input type="hidden" name="id" value="{{$messageid}}">
            <div class="card-body px-4 pt-0 mt-0">
                <div class="row">
                    <div class="col-10 ">


                        <label class="mt-4 mb-2 text-primary text-sm">When would you like us to deliver your message?</label>
                        <div class="form-check form-check-inline mb-2">
                            <input class="form-check-input @if(isset($error['deliverytype'][0]) && !empty($error['deliverytype'][0])) is-invalid @endif " type="radio" name="deliverytype" id="deliverytype1" value="1" @if(isset($messagedata->delivery_type) && $messagedata->delivery_type=='1') checked @endif )>
                            <label class="text-xs text-dark " for="deliverytype1">On a specific date</label>
                        </div>

                        @if($hasdemopackage!==1)
                        <div class="form-check  form-check-inline mb-2">
                            <input class="form-check-input @if(isset($error['deliverytype'][0]) && !empty($error['deliverytype'][0])) is-invalid @endif " type="radio" name="deliverytype" id="deliverytype12" value="2" @if(isset($messagedata->delivery_type) && $messagedata->delivery_type=='2') checked @endif>
                            <label class="text-xs text-dark" for="deliverytype12">After I pass away</label>
                        </div>
                        @endif

                        @if(isset($error['deliverytype'][0]) && !empty($error['deliverytype'][0]))
                        <p class="form-text text-danger text-xs mb-1">
                            {{$error['deliverytype'][0]}}
                        </p>
                        @endif


                        <div class="deliverydatebox" id="deliverydatebox" @if(isset($messagedata->delivery_type) && $messagedata->delivery_type=='1') @else style="display: none;" @endif>
                            <label class="text-sm text-primary">Select Delivery Date</label>
                            <div id="deliverydate" class="input-group-alternative date datepicker">
                                <input required type="text" name="deliverydate" class=" date-validation form-control  @if(isset($error['deliverydate'][0]) && !empty($error['deliverydate'][0])) is-invalid @endif" placeholder="mm-dd-yyyy" @if(isset($messagedata->delivery_date) && !empty($messagedata->delivery_date)) value="{{date('m/d/Y',strtotime($messagedata->delivery_date))}}" @endif />

                                <span class="input-group-addon">

                                </span>
                            </div>
                            @if(isset($error['deliverydate'][0]) && !empty($error['deliverydate'][0]))
                            <p class="form-text text-danger text-xs mb-1">
                                {{$error['deliverydate'][0]}}
                            </p>
                            @endif
                        </div>


                    </div>
                </div>
            </div>

            <div class="card-footer  d-flex">

                <div class="ms-auto">
                    <button type="button" class="btn bg-gradient-primary btn-xs mx-2 TakeActionsMessage">Save changes</button>
                    <button type="button" class="btn bg-gradient-secondary btn-xs" onclick="OpenMessageDetails('{{$messagetype}}','{{$messageid}}')">Go Back</button>
                </div>
            </div>
            @endif


            @if(isset($actiontype) && $actiontype=='update-authorisation')
           
            <form method="POST" action="{{route('my-messages.take-action-message-details')}}">
                <input type="hidden" name="actiontype" value="update-authorisation">
                <input type="hidden" name="messagetype" value="{{$messagetype}}">
                <input type="hidden" name="id" value="{{$messageid}}">
                <div class="card-body px-4 pt-0 mt-0">
                    <div class="row">
                        <div class="col-10 ">


                            <label class="mt-4 mb-2 text-primary text-sm">Would you like this message to be directly delivered to its selected recipient only?</label>
                            <div class="form-check mb-2">
                                <input class="form-check-input @if(isset($error['authorisationtype'][0]) && !empty($error['authorisationtype'][0])) is-invalid @endif" type="radio" name="authorisationtype" id="authorisationtype1" value="1" @if(isset($messagedata->authorisation_type) && $messagedata->authorisation_type=='1') checked @endif>
                                <label class="text-xs text-dark " for="authorisationtype1">Yes, this message can only be seen by its chosen recipient. Do not release this to my authorised third party.
                                </label>
                            </div>

                            <div class="form-check mb-2">
                                <input class="form-check-input @if(isset($error['authorisationtype'][0]) && !empty($error['authorisationtype'][0])) is-invalid @endif" type="radio" name="authorisationtype" id="authorisationtype12" value="2" @if(isset($messagedata->authorisation_type) && $messagedata->authorisation_type=='2') checked @endif>
                                <label class="text-xs text-dark" for="authorisationtype12">I don’t mind, you can release it to my authorised 3rd party too</label>
                            </div>

                            @if(isset($error['authorisationtype'][0]) && !empty($error['authorisationtype'][0]))
                            <p class="form-text text-danger text-xs mb-1">
                                {{$error['authorisationtype'][0]}}
                            </p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="card-footer  d-flex">

                    <div class="ms-auto">
                        <button type="button" class="btn bg-gradient-primary btn-xs mx-2 TakeActionsMessage">Save changes</button>
                        <button type="button" class="btn bg-gradient-secondary btn-xs" onclick="OpenMessageDetails('{{$messagetype}}','{{$messageid}}')">Go Back</button>
                    </div>
                </div>
                </form>
                @endif
                

                @if(isset($actiontype) && $actiontype=='update-deliverystyle')
               
               <form method="POST" action="{{route('my-messages.take-action-message-details')}}">
                    @csrf
                    <input type="hidden" name="actiontype" value="update-deliverystyle">
                    <input type="hidden" name="messagetype" value="{{$messagetype}}">
                    <input type="hidden" name="id" value="{{$messageid}}">
                    <input type="hidden" name="messageid" value="{{$messageid}}">
                    @if(isset($heliroomaddon->package_id))<input type="hidden" class="package_id" name="package_id" value="{{$heliroomaddon->package_id}}">@endif
                    <input type="hidden" name="purchasesubscription" value="notrial">
                    @if(isset($heliroomaddon->package_id))<input type="hidden" class="actionpackage" name="actionpackage" value="{{route('dashboard.purchase-subscription',$heliroomaddon->package_id)}}">@endif
                    <input type="hidden" class="actionform" name="actionform" value="{{route('my-messages.take-action-message-details')}}">
                    <input type="hidden" name="redirecturl" value="{{route('my-messages.index')}}?type={{$messagedata->contenttype}}&messagetype={{$messagetype}}">
                    <div class="card-body px-4 pt-0 mt-0">
                        <div class="row">
                            <div class="col-10 ">
                                @if(empty($messagedata->heilroom_package_id))
                                @if((isset($handdeliveredaddon) && count($handdeliveredaddon)>0))
                                <label class="mt-4 mb-2 text-primary text-sm ">Would you like this content to be included in your hand-delivered heirloom product ?

                                </label>

                                <div class="form-check mb-2">
                                    <input class="form-check-input @if(isset($error['deliverystyle'][0]) && !empty($error['deliverystyle'][0])) is-invalid @endif" type="radio" name="deliverystyle" id="deliverystyle12" value="2" @if(isset($messagedata->delivery_style) && $messagedata->delivery_style=='2') checked @endif>
                                    <label class="text-xs text-dark" for="deliverystyle12">Yes, please include this message
                                    </label>
                                </div>

                                <div class="form-check mb-2">
                                    <input class="form-check-input  @if(isset($error['deliverystyle'][0]) && !empty($error['deliverystyle'][0])) is-invalid @endif" type="radio" name="deliverystyle" id="deliverystyle1" value="1" @if(isset($messagedata->delivery_style) && $messagedata->delivery_style=='1') checked @endif>
                                    <label class="text-xs text-dark" for="deliverystyle1">No, I’m happy with digital delivery</label>
                                </div>

                                

                                @else

                                <label class="mt-4 mb-2 text-primary text-sm ">Would you like to purchase a hand-delivered heirloom product for this content?
                                </label>
                                <div class="form-check mb-2">
                                    <input class="form-check-input changeactionpackage1 @if(isset($error['deliverystyle'][0]) && !empty($error['deliverystyle'][0])) is-invalid @endif" type="radio" name="deliverystyle" id="deliverystyle12" value="2">
                                    <label class="text-xs text-dark" for="deliverystyle12">Yes, please take me to the payment portal
                                    </label>
                                </div>

                                <div class="form-check mb-2">
                                    <input class="form-check-input changeactionpackage2 @if(isset($error['deliverystyle'][0]) && !empty($error['deliverystyle'][0])) is-invalid @endif" type="radio" name="deliverystyle" id="deliverystyle1" value="1" @if(isset($messagedata->delivery_style) && $messagedata->delivery_style=='1') checked @endif>
                                    <label class="text-xs text-dark" for="deliverystyle1">No, I’m happy with digital delivery only</label>
                                </div>


                                @if(isset($error['deliverystyle'][0]) && !empty($error['deliverystyle'][0]))
                                <p class="form-text text-danger text-xs mb-1">
                                    {{$error['deliverystyle'][0]}}
                                </p>
                                @endif

                                @endif

                                @else

                                <div class="form-check mb-2">
                                    <input class="form-check-input @if(isset($error['deliverystyle'][0]) && !empty($error['deliverystyle'][0])) is-invalid @endif" type="radio" name="deliverystyle" id="deliverystyle12" value="2" @if(isset($messagedata->delivery_style) && $messagedata->delivery_style=='2') checked @endif>
                                    <label class="text-xs text-dark" for="deliverystyle12">Yes, please include this message
                                    </label>
                                </div>

                                <div class="form-check mb-2">
                                    <input class="form-check-input  @if(isset($error['deliverystyle'][0]) && !empty($error['deliverystyle'][0])) is-invalid @endif" type="radio" name="deliverystyle" id="deliverystyle1" value="1" @if(isset($messagedata->delivery_style) && $messagedata->delivery_style=='1') checked @endif>
                                    <label class="text-xs text-dark" for="deliverystyle1">No, I’m happy with digital delivery only</label>
                                </div>

                                @endif


                                
                            </div>
                        </div>
                    </div>

                    <div class="card-footer  d-flex">
                         @if(empty($messagedata->heilroom_package_id))
                         @if((isset($handdeliveredaddon) && count($handdeliveredaddon)>0))
                         <div class="ms-auto">
                            <button type="button" class="btn bg-gradient-primary btn-xs mx-2 TakeActionsMessage">Save changes</button>
                            <button type="button" class="btn bg-gradient-secondary btn-xs" onclick="OpenMessageDetails('{{$messagetype}}','{{$messageid}}')">Go Back</button>
                        </div>
                         @else
                        <div class="ms-auto">
                            <button type="button" class="change-action  btn bg-gradient-primary btn-xs mx-2 ">Save changes</button>
                            <button type="button" class="btn bg-gradient-secondary btn-xs" onclick="OpenMessageDetails('{{$messagetype}}','{{$messageid}}')">Go Back</button>
                        </div>
                        @endif
                        @else
                        <div class="ms-auto">
                            <button type="button" class="btn bg-gradient-primary btn-xs mx-2 TakeActionsMessage">Save changes</button>
                            <button type="button" class="btn bg-gradient-secondary btn-xs" onclick="OpenMessageDetails('{{$messagetype}}','{{$messageid}}')">Go Back</button>
                        </div>
                        @endif
                    </div>
                </form>
                @endif
               


                @if(isset($actiontype) && $actiontype=='update-message')
                <form method="POST" action="{{route('my-messages.take-action-message-details')}}">
                    <input type="hidden" name="actiontype" value="update-message">
                    <input type="hidden" name="messagetype" value="{{$messagetype}}">
                    <input type="hidden" name="id" value="{{$messageid}}">
                    <div class="card-body px-4 pt-0 mt-0 text-center">
                        <div class="swal-icon swal-icon--warning">
                            <span class="swal-icon--warning__body">
                                <span class="swal-icon--warning__dot"></span>
                            </span>
                        </div>
                        <div class="swal-text text-center" style="">Are you sure you want to Update Message ?</div>
                    </div>

                    <div class="card-footer  d-flex">
                        <div class="ms-auto">
                            <button type="button" class="btn bg-gradient-primary btn-xs mx-2 PerformActionsMessage">Update Message</button>
                            <button type="button" class="btn bg-gradient-secondary btn-xs" onclick="OpenMessageDetails('{{$messagetype}}','{{$messageid}}')">Go Back</button>
                        </div>
                    </div>
                </form>
                @endif

                @if(isset($actiontype) && $actiontype=='clone-message')
                <form method="POST" action="{{route('my-messages.take-action-message-details')}}">
                    <input type="hidden" name="actiontype" value="clone-message">
                    <input type="hidden" name="messagetype" value="{{$messagetype}}">
                    <input type="hidden" name="id" value="{{$messageid}}">
                    <div class="card-body px-4 pt-0 mt-0 text-center">
                        <div class="swal-icon swal-icon--warning">
                            <span class="swal-icon--warning__body">
                                <span class="swal-icon--warning__dot"></span>
                            </span>
                        </div>
                        <div class="swal-text text-center" style="">Are you sure your want to duplicate this message?</div>
                    </div>
                    <div class="card-footer  d-flex">
                        <div class="ms-auto">
                            <button type="button" class="btn bg-gradient-primary btn-xs mx-2 PerformActionsMessage">Duplicate</button>
                            <button type="button" class="btn bg-gradient-secondary btn-xs" onclick="OpenMessageDetails('{{$messagetype}}','{{$messageid}}')">Go Back</button>
                        </div>
                    </div>
                </form>
                @endif

                @if(isset($actiontype) && $actiontype=='delete-message')
                <form method="POST" action="{{route('my-messages.take-action-message-details')}}">
                    <input type="hidden" name="actiontype" value="delete-message">
                    <input type="hidden" name="messagetype" value="{{$messagetype}}">
                    <input type="hidden" name="id" value="{{$messageid}}">
                    <div class="card-body px-4 pt-0 mt-0 text-center">
                        <div class="swal-icon swal-icon--warning">
                            <span class="swal-icon--warning__body">
                                <span class="swal-icon--warning__dot"></span>
                            </span>
                        </div>
                        <div class="swal-text text-center text-center" style="">Are you sure you want to Delete Message ?</div>
                    </div>

                    <div class="card-footer  d-flex">
                        <div class="ms-auto">
                            <button type="button" class="btn bg-gradient-primary btn-xs mx-2 PerformActionsMessage">Delete</button>
                            <button type="button" class="btn bg-gradient-secondary btn-xs" onclick="OpenMessageDetails('{{$messagetype}}','{{$messageid}}')">Go Back</button>
                        </div>
                    </div>
                </form>
                @endif

                @if(isset($actiontype) && $actiontype=='update-qrcode')
                
                <form method="POST" action="{{route('my-messages.take-action-message-details')}}">
                    @csrf
                    <input type="hidden" name="actiontype" value="update-qrcode">
                    <input type="hidden" name="messagetype" value="{{$messagetype}}">
                    <input type="hidden" name="id" value="{{$messageid}}">
                    <input type="hidden" name="messageid" value="{{$messageid}}">
                     @if(isset($qraddon->package_id))<input type="hidden" name="package_id" class="package_id" value="{{$qraddon->package_id}}">@endif
                    <input type="hidden" name="purchasesubscription" value="notrial">
                     @if(isset($qraddon->package_id))<input type="hidden" class="actionpackage" name="actionpackage" value="{{route('dashboard.purchase-subscription',$qraddon->package_id)}}">@endif
                    <input type="hidden" class="actionform" name="actionform" value="{{route('my-messages.take-action-message-details')}}">
                    <input type="hidden" name="redirecturl" value="{{route('my-messages.index')}}?type={{$messagedata->contenttype}}&messagetype={{$messagetype}}">
                    <div class="card-body px-4 pt-0 mt-0">
                        <div class="row">
                            <div class="col-10 ">
                                @if(empty($messagedata->qr_package_id))
                                @if((isset($qrcodeaddon) && count($qrcodeaddon)>0) )
                                <label class="mt-4 mb-2 text-primary text-sm ">Would you like this content to be linked to your QR code ?

                                </label>

                                <div class="form-check mb-2">
                                    <input class="form-check-input @if(isset($error['qrdeliverystyle'][0]) && !empty($error['qrdeliverystyle'][0])) is-invalid @endif" type="radio" name="qrdeliverystyle" id="qrdeliverystyle12" value="2" @if(isset($messagedata->qrdeliverystyle) && $messagedata->qrdeliverystyle=='2')) checked @endif>
                                    <label class="text-xs text-dark" for="qrdeliverystyle12">Yes, please link this content to my QR code
                                    </label>
                                </div>

                                <div class="form-check mb-2">
                                    <input class="form-check-input  @if(isset($error['qrdeliverystyle'][0]) && !empty($error['qrdeliverystyle'][0])) is-invalid @endif" type="radio" name="qrdeliverystyle" id="qrdeliverystyle1" value="1" @if(isset($messagedata->qrdeliverystyle) && $messagedata->qrdeliverystyle=='1')) checked @endif> 
                                    <label class="text-xs text-dark" for="qrdeliverystyle1"> No, I’m happy with digital delivery only</label>
                                </div>

                                

                                @else

                                <label class="mt-4 mb-2 text-primary text-sm ">Would you like to purchase a QR Code to link to this content?
                                </label>
                                <div class="form-check mb-2">
                                    <input class="form-check-input changeqractionpackage1 @if(isset($error['qrdeliverystyle'][0]) && !empty($error['qrdeliverystyle'][0])) is-invalid @endif" type="radio" name="qrdeliverystyle" id="qrdeliverystyle12" value="2">
                                    <label class="text-xs text-dark" for="qrdeliverystyle12">Yes, please take me to the payment portal
                                    </label>
                                </div>

                                <div class="form-check mb-2">
                                    <input class="form-check-input changeqractionpackage2 @if(isset($error['qrdeliverystyle'][0]) && !empty($error['qrdeliverystyle'][0])) is-invalid @endif" type="radio" name="qrdeliverystyle" id="qrdeliverystyle1" value="1" @if(isset($messagedata->qrdeliverystyle) && $messagedata->qrdeliverystyle=='1') checked @endif>
                                    <label class="text-xs text-dark" for="qrdeliverystyle1">No, I’m happy with digital delivery only</label>
                                </div>


                                @if(isset($error['qrdeliverystyle'][0]) && !empty($error['qrdeliverystyle'][0]))
                                <p class="form-text text-danger text-xs mb-1">
                                    {{$error['qrdeliverystyle'][0]}}
                                </p>
                                @endif

                                @endif

                                @else

                                <div class="form-check mb-2">
                                    <input class="form-check-input @if(isset($error['qrdeliverystyle'][0]) && !empty($error['qrdeliverystyle'][0])) is-invalid @endif" type="radio" name="qrdeliverystyle" id="qrdeliverystyle12" value="2" @if(isset($messagedata->qrdeliverystyle) && $messagedata->qrdeliverystyle=='2')) checked @endif>
                                    <label class="text-xs text-dark" for="qrdeliverystyle12">Yes, please link this content to my QR code
                                    </label>
                                </div>

                                <div class="form-check mb-2">
                                    <input class="form-check-input  @if(isset($error['qrdeliverystyle'][0]) && !empty($error['qrdeliverystyle'][0])) is-invalid @endif" type="radio" name="qrdeliverystyle" id="qrdeliverystyle1" value="1" @if(isset($messagedata->qrdeliverystyle) && $messagedata->qrdeliverystyle=='1')) checked @endif>
                                    <label class="text-xs text-dark" for="qrdeliverystyle1">No, I’m happy with digital delivery</label>
                                </div>

                                @endif


                                
                            </div>
                        </div>
                    </div>

                    <div class="card-footer  d-flex">
                        @if(empty($messagedata->qr_package_id))
                         @if((isset($qrcodeaddon) && count($qrcodeaddon)>0) )
                         <div class="ms-auto">
                            <button type="button" class="btn bg-gradient-primary btn-xs mx-2 TakeActionsMessage">Save changes</button>
                            <button type="button" class="btn bg-gradient-secondary btn-xs" onclick="OpenMessageDetails('{{$messagetype}}','{{$messageid}}')">Go Back</button>
                        </div>
                         @else
                        <div class="ms-auto">
                            <button type="button" class="change-action  btn bg-gradient-primary btn-xs mx-2 ">Save changes</button>
                            <button type="button" class="btn bg-gradient-secondary btn-xs" onclick="OpenMessageDetails('{{$messagetype}}','{{$messageid}}')">Go Back</button>
                        </div>
                        @endif
                         @else
                        <div class="ms-auto">
                            <button type="button" class="btn bg-gradient-primary btn-xs mx-2 TakeActionsMessage">Save changes</button>
                            <button type="button" class="btn bg-gradient-secondary btn-xs" onclick="OpenMessageDetails('{{$messagetype}}','{{$messageid}}')">Go Back</button>
                        </div>
                        @endif
                    </div>
                </form>
                
                @endif

</div>

<script type="text/javascript">
    $('input[name=deliverytype]').change(function(e) {
        e.preventDefault();

        var deliverytype = $('input[name="deliverytype"]:checked').val();

        $("#deliverydatebox").hide();

        if (deliverytype == 1) {
            $("#deliverydatebox").show();
        }

    });


    $('.changeactionpackage1').change(function(e) {
     
        e.preventDefault();
        form = $(this).closest('form');
        var package_id=form.find('.package_id').val();
        if(package_id)
        {
        var actionform=$('.actionform').val();
        var actionpackage=$('.actionpackage').val();
        
        var deliverystyle = $('input[name="deliverystyle"]:checked').val();

        if (deliverystyle == 1) {
           form.attr('action',actionform);
           $('.change-action').addClass('TakeActionsMessage');
           $('.change-action').attr('type','button');
        }
        if(deliverystyle == 2)
        {
          form.attr('action',actionpackage);
         
          $('.change-action').removeClass('TakeActionsMessage');
          $('.change-action').removeClass('showheirloom-package-popup');
          $('.change-action').attr('type','submit');
        }
        }
        else
        {
            $('.change-action').addClass('showheirloom-package-popup');
            $('#heirloom-package-popup').modal('show');
            $('#MessageDetails').animate({ opacity: 0 }, 100);
        }

    });


    $('.changeactionpackage2').change(function(e) {
     
        e.preventDefault();
        form = $(this).closest('form');
        var actionform=$('.actionform').val();
        var actionpackage=$('.actionpackage').val();
        
        var deliverystyle = $('input[name="deliverystyle"]:checked').val();

        if (deliverystyle == 1) {
           form.attr('action',actionform);
           $('.change-action').addClass('TakeActionsMessage');
           $('.change-action').attr('type','button');
        }
        if(deliverystyle == 2)
        {
          form.attr('action',actionpackage);
         
          $('.change-action').removeClass('TakeActionsMessage');
          $('.change-action').attr('type','submit');
        }

    });

    $('.changeqractionpackage1').change(function(e) {
     
        e.preventDefault();
        form = $(this).closest('form');
        var actionform=$('.actionform').val();
        var actionpackage=$('.actionpackage').val();
        
        var deliverystyle = $('input[name="qrdeliverystyle"]:checked').val();

        if (deliverystyle == 1) {
           form.attr('action',actionform);
           $('.change-action').addClass('TakeActionsMessage');
           $('.change-action').attr('type','button');
        }
        if(deliverystyle == 2)
        {
          form.attr('action',actionpackage);
         
          $('.change-action').removeClass('TakeActionsMessage');
          $('.change-action').attr('type','submit');
        }

    });


    $('.changeqractionpackage2').change(function(e) {
     
        e.preventDefault();
        form = $(this).closest('form');
        var actionform=$('.actionform').val();
        var actionpackage=$('.actionpackage').val();
        
        var deliverystyle = $('input[name="qrdeliverystyle"]:checked').val();

        if (deliverystyle == 1) {
           form.attr('action',actionform);
           $('.change-action').addClass('TakeActionsMessage');
           $('.change-action').attr('type','button');
        }
        if(deliverystyle == 2)
        {
          form.attr('action',actionpackage);
         
          $('.change-action').removeClass('TakeActionsMessage');
          $('.change-action').attr('type','submit');
        }

    });

    $('body').find(".datepicker").datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'mm/dd/yyyy',
        startDate: "{{date('d-m-Y')}}"

    });
</script>