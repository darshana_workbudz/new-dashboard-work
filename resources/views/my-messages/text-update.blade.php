@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Update Text')

@section('headercommon')
<x-main.header icon="document_one_page_2" title="Update Written Message" subtitle="Let's start with the basic information" />
@endsection
@section('content')
<style type="text/css">
.ql-container {
  min-height: 10rem;
  height: 100%;
  flex: 1;
  display: flex;
  flex-direction: column;
}

.ql-editor {
  height: 100%;
  flex: 1;
  overflow-y: auto;
  width: 100%;
}
.ql-editor>p {
  color:black !important;
}
.custom-style-input {
  width: 100%;
  border:0;
  border-radius:0px;
  border-bottom:1px solid black;
  padding: 15px;
  font-size: 30px;
}
#descriptionbox
{
  height: 500px !important;
}

#counter {
  border: 1px solid #ccc;
  border-width: 0px 1px 1px 1px;
  color: #aaa;
  padding: 5px 15px;
  text-align: right;
}
</style>
<div class="row">
  <div class="card">
   <div class="card-body">
   
      <form method="POST" action="{{route('my-messages.text-verify')}}">
        <input type="hidden" name="ziggeo_user_text_id" value="{{$textdata->ziggeo_user_text_id}}">
        
          @csrf
          <div class="row ">
            
           

            <div class="col-sm-12">
              <input type="hidden" name="descriptionhtml" value="{{$textdata->descriptioncontenthtml}}">
              <input type="hidden" name="descriptioncontent" value="{{$textdata->descriptioncontent}}">
              <label class="mb-3">Written Message</label>
              <p class="form-text text-muted text-xs ms-1 d-inline">
               
              </p>
              <div id="descriptionbox" class="h-50">
              
              </div>
               <div id="counter">0 characters</div>
            </div>

            <div class="col-12 col-sm-12 mt-4  text-end">                
            <button class="submit-button btn bg-primary btn-sm mb-0 me-2">Update Letter</button>
          </div>
            
           
            
          </div>
        </form>
 </div>
</div>
</div>
  
<script src="{{asset('assets/js/plugins/quill.min.js')}}"></script>

<script type="text/javascript">
  
  var html='{!! addslashes($textdata->descriptioncontenthtml) !!}';
  init(html);
  function init(html) {
      var quill = new Quill('#descriptionbox', {});

      var delta = quill.clipboard.convert(html);
      quill.setContents(delta, 'silent');
  }
  
  var toolbarOptions = [

    ['bold', 'italic', 'underline'],        // toggled buttons
    ['blockquote'],
    [{
      'list': 'ordered'
    }, {
      'list': 'bullet'
    }],
    [{
      'indent': '-1'
    }, {
      'indent': '+1'
    }], // outdent/indent
    [{
      'header': [1, 2, 3, 4, 5, 6, false]
    }],

    [{
      'color': []
    }, {
      'background': []
    }], // dropdown with defaults from theme
    [{
      'align': []
    }],
    ['clean'] // remove formatting button
  ];

Quill.register('modules/counter', function(quill, options) {
  var container = document.querySelector(options.container);
  quill.on('text-change', function() {
    var text = quill.getText();
    if (options.unit === 'word') {
      container.innerText = text.split(/\s+/).length-1 + ' words';
    } else {
      container.innerText = text.length-1 + ' characters';
    }
    var descriptioncontent = document.querySelector('input[name=descriptioncontent]');
    descriptioncontent.value = JSON.stringify(quill.getContents());

    var descriptionhtml = document.querySelector('input[name=descriptionhtml]');
    descriptionhtml.value = quill.root.innerHTML;;                 
    
  });
});

var quill = new Quill('#descriptionbox', { 
  modules: { 
    toolbar: toolbarOptions,
    counter: {
      container: '#counter',
      unit: 'character'
    }
  },
  theme: 'snow' 
});


 


    



</script>
@endsection