<div class="card h-100">
  <div class="card-header pb-0">
    <h5 class="text-sm text-primary mb-0">My Content Preferences</h5>
    <p class="text-xs pt-0 mt-0">
      If any of the preferences below remain unallocated, your content may not be deliverable
      <hr>
    </p>
  </div>
  <div class="card-body p-3">
    <div class="timeline timeline-one-side">
      <div class="timeline-block mb-3">
        <span class="timeline-step">
         <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M11 17.5a6.47 6.47 0 0 1 1.023-3.5h-7.77a2.249 2.249 0 0 0-2.25 2.25v.919c0 .572.18 1.13.511 1.596C4.056 20.929 6.58 22 10 22c.932 0 1.797-.08 2.592-.24A6.475 6.475 0 0 1 11 17.502ZM15 7.005a5 5 0 1 0-10 0 5 5 0 0 0 10 0Z" fill="#773eb1"/><path d="M23 17.5a5.5 5.5 0 1 0-11 0 5.5 5.5 0 0 0 11 0Zm-5.59-3.492L17.5 14l.09.008a.5.5 0 0 1 .402.402l.008.09V17h2.504l.09.008a.5.5 0 0 1 .402.402l.008.09-.008.09a.5.5 0 0 1-.402.402l-.09.008H18L18 20.5l-.008.09a.5.5 0 0 1-.402.402L17.5 21l-.09-.008a.5.5 0 0 1-.402-.402L17 20.5V18h-2.496l-.09-.008a.5.5 0 0 1-.402-.402l-.008-.09.008-.09a.5.5 0 0 1 .402-.402l.09-.008H17L17 14.5l.008-.09a.5.5 0 0 1 .402-.402Z" fill="#773eb1"/></svg>
       </span>
       <div class="timeline-content">
        <a class="manage-action" data-action="update-recipients" onclick="ManageActionMessageDetails('{{$messagetype}}','{{$messageid}}','update-recipients')"> <h6 class="text-dark text-sm font-weight-bold mb-0">Recipients &nbsp; <i class="fa fa-edit text-sm text-alternate-primary" aria-hidden="true"></i></h6></a>
       
        @if(isset($messagedata->user_recipient_id) && !empty($messagedata->user_recipient_id))
        <p class="text-grey font-weight-bold text-xs mt-1 mb-1">{{$messagedata->first_name}} {{$messagedata->last_name}}</p>
        <span class="badge bg-success mt-1">Complete</span>
        @else
        <p class="text-danger font-weight-bold text-xs mt-1 mb-1"> Select a recipient </p>
         <span class="badge bg-secondary mt-1">Unallocated</span>
        @endif



      </div>
    </div>
    <div class="timeline-block mb-3">
      <span class="timeline-step">
       <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M21 8.5v9.25A3.25 3.25 0 0 1 17.75 21H6.25A3.25 3.25 0 0 1 3 17.75V8.5h18ZM7.25 15a1.25 1.25 0 1 0 0 2.5 1.25 1.25 0 0 0 0-2.5ZM12 15a1.25 1.25 0 1 0 0 2.5 1.25 1.25 0 0 0 0-2.5Zm-4.75-4.5a1.25 1.25 0 1 0 0 2.5 1.25 1.25 0 0 0 0-2.5Zm4.75 0a1.25 1.25 0 1 0 0 2.5 1.25 1.25 0 0 0 0-2.5Zm4.75 0a1.25 1.25 0 1 0 0 2.5 1.25 1.25 0 0 0 0-2.5Zm1-7.5A3.25 3.25 0 0 1 21 6.25V7H3v-.75A3.25 3.25 0 0 1 6.25 3h11.5Z" fill="#773eb1"/></svg>
      </span>
      <div class="timeline-content">
        <a class="manage-action" data-action="update-deliverdate" onclick="ManageActionMessageDetails('{{$messagetype}}','{{$messageid}}','update-deliverdate')"> <h6 class="text-dark text-sm font-weight-bold mb-0">Delivery date &nbsp; <i class="fa fa-edit text-sm text-alternate-primary" aria-hidden="true"></i></h6></a>
        @if(isset($messagedata->delivery_type) && !empty($messagedata->delivery_type))
        <p class="text-grey font-weight-bold text-xs mt-1 mb-0">  Delivery date chosen</p>
        <span class="badge bg-success mt-1">Complete</span>
        @else
        <p class="text-danger font-weight-bold text-xs mt-1 mb-1"> Select a delivery date  </p>
         <span class="badge bg-secondary mt-1">Unallocated</span>
        @endif
        @if(isset($actiontype) && $actiontype=='update-deliverdate')
        @endif
      </div>
    </div>

    <div class="timeline-block mb-3">
      <span class="timeline-step">
        <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M10 2a4 4 0 0 1 4 4v2h2.5A1.5 1.5 0 0 1 18 9.5V11c-.319 0-.637.11-.896.329l-.107.1c-.812.845-1.656 1.238-2.597 1.238-.783 0-1.4.643-1.4 1.416v2.501c0 2.374.924 4.22 2.68 5.418L3.5 22A1.5 1.5 0 0 1 2 20.5v-11A1.5 1.5 0 0 1 3.5 8H6V6a4 4 0 0 1 4-4Zm8.284 10.122c.992 1.036 2.091 1.545 3.316 1.545.193 0 .355.143.392.332l.008.084v2.501c0 2.682-1.313 4.506-3.873 5.395a.385.385 0 0 1-.253 0c-2.476-.86-3.785-2.592-3.87-5.13L14 16.585v-2.5c0-.23.18-.417.4-.417 1.223 0 2.323-.51 3.318-1.545a.389.389 0 0 1 .566 0ZM10 13.5a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3ZM10 4a2 2 0 0 0-2 2v2h4V6a2 2 0 0 0-2-2Z" fill="#773eb1"/></svg>
      </span>
      <div class="timeline-content">
        <a class="manage-action" data-action="update-authorisation" onclick="ManageActionMessageDetails('{{$messagetype}}','{{$messageid}}','update-authorisation')"> <h6 class="text-dark text-sm font-weight-bold mb-0">Authorisation &nbsp; <i class="fa fa-edit text-sm text-alternate-primary" aria-hidden="true"></i></h6></a>
        @if(isset($messagedata->authorisation_type) && !empty($messagedata->authorisation_type))
        <p class="text-grey font-weight-bold text-xs mt-1 mb-0"> Authorisation method chosen</p>
        <span class="badge bg-success mt-1">Complete</span>
        @else
        <p class="text-danger font-weight-bold text-xs mt-1 mb-1"> Select a Authorisation method </p>
         <span class="badge bg-secondary mt-1">Unallocated</span>
        @endif
        @if(isset($actiontype) && $actiontype=='update-authorisation')
        @endif
      </div>
    </div>
    
    <div class="timeline-block mb-3">
      <span class="timeline-step">
         <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="m12.815 12.197-7.532 1.255a.5.5 0 0 0-.386.318L2.3 20.728c-.248.64.421 1.25 1.035.942l7.674-3.837a6.5 6.5 0 0 1 10.589-5.38.752.752 0 0 0-.263-1.124l-18-9c-.614-.307-1.283.303-1.035.942l2.598 6.958a.5.5 0 0 0 .386.318l7.532 1.255a.2.2 0 0 1 0 .395ZM17.5 12a5.5 5.5 0 1 1 0 11 5.5 5.5 0 0 1 0-11Zm2 5.5h-2V15a.5.5 0 0 0-1 0v3a.5.5 0 0 0 .5.5h2.5a.5.5 0 0 0 0-1Z" fill="#773eb1"/></svg>
      </span>
      <div class="timeline-content">
        <a class="manage-action" data-action="update-deliverystyle" onclick="ManageActionMessageDetails('{{$messagetype}}','{{$messageid}}','update-deliverystyle')"> <h6 class="text-dark text-sm font-weight-bold mb-0">Delivery style &nbsp; <i class="fa fa-edit text-sm text-alternate-primary" aria-hidden="true"></i></h6></a>
        @if(isset($messagedata->delivery_style) && !empty($messagedata->delivery_style))
        <p class="text-grey font-weight-bold text-xs mt-1 mb-0"> Delivery style updated   </p>
        <span class="badge bg-success mt-1">Complete</span>
        @else
        <p class="text-danger font-weight-bold text-xs mt-1 mb-1"> Select a Delivery style</p>
         <span class="badge bg-secondary mt-1">Unallocated</span>
        @endif
        @if(isset($actiontype) && $actiontype=='update-deliverystyle')
        @endif
      </div>
    </div>
    @if(isset($usersubscription) && !empty($usersubscription) && $usersubscription->demopackage!=='1')
    <div class="timeline-block mb-3">
      <span class="timeline-step">
        <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M8 6H6v2h2V6Z" fill="#773eb1"/><path d="M3 5.5A2.5 2.5 0 0 1 5.5 3h3A2.5 2.5 0 0 1 11 5.5v3A2.5 2.5 0 0 1 8.5 11h-3A2.5 2.5 0 0 1 3 8.5v-3ZM5.5 5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3ZM6 16h2v2H6v-2Z" fill="#773eb1"/><path d="M3 15.5A2.5 2.5 0 0 1 5.5 13h3a2.5 2.5 0 0 1 2.5 2.5v3A2.5 2.5 0 0 1 8.5 21h-3A2.5 2.5 0 0 1 3 18.5v-3Zm2.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3ZM18 6h-2v2h2V6Z" fill="#773eb1"/><path d="M15.5 3A2.5 2.5 0 0 0 13 5.5v3a2.5 2.5 0 0 0 2.5 2.5h3A2.5 2.5 0 0 0 21 8.5v-3A2.5 2.5 0 0 0 18.5 3h-3ZM15 5.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-3ZM13 13h2.75v2.75H13V13ZM18.25 15.75h-2.5v2.5H13V21h2.75v-2.75h2.5V21H21v-2.75h-2.75v-2.5ZM18.25 15.75V13H21v2.75h-2.75Z" fill="#773eb1"/></svg>
      </span>
      <div class="timeline-content">





        <a class="manage-action" data-action="update-qrcode" onclick="ManageActionMessageDetails('{{$messagetype}}','{{$messageid}}','update-qrcode')"> <h6 class="text-dark text-sm font-weight-bold mb-0">My Qr Code &nbsp; <i class="fa fa-edit text-sm text-alternate-primary" aria-hidden="true"></i></h6></a>


        @if(isset($messagedata->qrid) && !empty($messagedata->qrid))
        <p class="text-grey font-weight-bold text-xs mt-1 mb-0">   QR : #{{$messagedata->qrid}}  &nbsp;  <a class="" onclick="OpenMessageQRDetails('{{$messagetype}}','{{$messageid}}')" ><i class="fa fa-qrcode shadow p-1  fa-1x text-grey opacity-10" aria-hidden="true"></i></a>
        </p>
        
        @else
        <p class="text-danger font-weight-bold text-xs mt-1 mb-1"> Link to QR Code </p>
        <span class="badge bg-secondary mt-1">Unallocated</span>
        @endif
        @if(isset($actiontype) && $actiontype=='update-qrcode')
        @endif
      </div>
    </div>
    @else

    <div class="timeline-block mb-3">
      <span class="timeline-step">
        <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M8 2H5a3 3 0 0 0-3 3v3h2.708A2.5 2.5 0 0 1 8 4.708V2ZM2 9.5V14a3 3 0 0 0 3 3h3v-6.44l-1.72 1.72a.75.75 0 0 1-1.06-1.06L6.94 9.5H2ZM9.5 17h1.519A6.5 6.5 0 0 1 22 12.81V9.5H10.56l1.72 1.72a.75.75 0 1 1-1.06 1.06L9.5 10.56V17ZM22 8h-9.208A2.5 2.5 0 0 0 9.5 4.708V2H19a3 3 0 0 1 3 3v3ZM10.5 8h-1V7a1 1 0 1 1 1 1ZM8 8H7a1 1 0 1 1 1-1.008V8Zm15 9.5a5.5 5.5 0 1 0-11 0 5.5 5.5 0 0 0 11 0Zm-5 .5.001 2.503a.5.5 0 1 1-1 0V18h-2.505a.5.5 0 0 1 0-1H17v-2.5a.5.5 0 1 1 1 0V17h2.497a.5.5 0 0 1 0 1H18Z" fill="#773eb1"/></svg>
      </span>
      <div class="timeline-content">
        <a class="manage-action" data-action="upgrade-plan" > <h6 class="text-dark text-sm font-weight-bold mb-0 ">Personalised add-ons &nbsp; </h6></a>
        <p class="showpesonalisedaddondiv text-dark font-weight-bold text-xs mt-1 mb-2">Explore Pesonalised addon &nbsp;<i class="fa fa-angle-down fa-lg iconupdown" aria-hidden="true"></i></p>

        <div class="row pesonalisedaddondiv" style="display: none;">
        <div class="col-md-6 mb-4">
          <div class="card shadow-xl mt-3 p-3">
            
              <div class="text-center mt-2">
                      <div class="avatar avatar-sm">
                        <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M8 6H6v2h2V6Z" fill="#773eb1"/><path d="M3 5.5A2.5 2.5 0 0 1 5.5 3h3A2.5 2.5 0 0 1 11 5.5v3A2.5 2.5 0 0 1 8.5 11h-3A2.5 2.5 0 0 1 3 8.5v-3ZM5.5 5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3ZM6 16h2v2H6v-2Z" fill="#773eb1"/><path d="M3 15.5A2.5 2.5 0 0 1 5.5 13h3a2.5 2.5 0 0 1 2.5 2.5v3A2.5 2.5 0 0 1 8.5 21h-3A2.5 2.5 0 0 1 3 18.5v-3Zm2.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3ZM18 6h-2v2h2V6Z" fill="#773eb1"/><path d="M15.5 3A2.5 2.5 0 0 0 13 5.5v3a2.5 2.5 0 0 0 2.5 2.5h3A2.5 2.5 0 0 0 21 8.5v-3A2.5 2.5 0 0 0 18.5 3h-3ZM15 5.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-3ZM13 13h2.75v2.75H13V13ZM18.25 15.75h-2.5v2.5H13V21h2.75v-2.75h2.5V21H21v-2.75h-2.75v-2.5ZM18.25 15.75V13H21v2.75h-2.75Z" fill="#773eb1"/></svg>
                      </div>
                      
                    
                  <h6 class="mb-0">QR Code</h6>
                  <p class="text-dark font-weight-bold text-xs mt-2 "> Add your content to QR Code  to make sharing your memory easy and hassle free.</p>

                  

                </div>
                <a  class="btn bg-purple-pale text-purple btn-lg showqr-package-popup" style="box-shadow:none;padding:0.8rem">Choose</a> 
            </div>
          </div>
          <div class="col-md-6 mb-4">
          <div class="card shadow-xl mt-3 p-3">
            
              <div class="text-center mt-2">
                      <div class="avatar avatar-sm">
                        <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M11.25 13v9h-4A3.25 3.25 0 0 1 4 18.75V13h7.25ZM20 13v5.75A3.25 3.25 0 0 1 16.75 22h-4v-9H20ZM14.5 2a3.25 3.25 0 0 1 2.738 5.002L19.75 7c.69 0 1.25.466 1.25 1.042v2.916c0 .576-.56 1.042-1.25 1.042l-7-.001V7h-1.5v4.999l-7 .001C3.56 12 3 11.534 3 10.958V8.042C3 7.466 3.56 7 4.25 7l2.512.002A3.25 3.25 0 0 1 12 3.174 3.24 3.24 0 0 1 14.5 2Zm-5 1.5a1.75 1.75 0 0 0-.144 3.494L9.5 7h1.75V5.25l-.006-.144A1.75 1.75 0 0 0 9.5 3.5Zm5 0a1.75 1.75 0 0 0-1.75 1.75V7h1.75a1.75 1.75 0 1 0 0-3.5Z" fill="#773eb1"/></svg>
                      </div>
                      
                    
                  <h6 class="mb-0">Heirloom Keepsake</h6>
                  <p class="text-dark font-weight-bold text-xs mt-2 "> Add your content to hand delivered Heirloom Keepsake.The perfect gift for your family.</p>

                  

                </div>

                <a  class="btn bg-purple-pale text-purple btn-lg showheirloom-package-popup" style="box-shadow:none;padding:0.8rem">Choose</a>  
            </div>
          </div>
        </div>
        

        
        @endif

    </div>
     @if(isset($usersubscription) && !empty($usersubscription) && $usersubscription->demopackage=='1')
   </div>
 </div>
     @endif

    <hr>
    <div class="d-flex mx-2  mb-3 ">
     <div>

      <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10Zm3.27-11.25H14a.75.75 0 0 0 0 1.5h2.75a.75.75 0 0 0 .75-.75V8.25a.75.75 0 0 0-1.5 0V9a4.991 4.991 0 0 0-4-2c-1.537 0-2.904.66-3.827 1.77a.75.75 0 0 0 1.154.96C9.963 8.963 10.907 8.5 12 8.5c1.492 0 2.767.934 3.27 2.25Zm-7.27 5V15a5.013 5.013 0 0 0 7.821.237.75.75 0 1 0-1.142-.972 3.513 3.513 0 0 1-5.842-.765H10a.75.75 0 0 0 0-1.5H7.25a.75.75 0 0 0-.75.75v3a.75.75 0 0 0 1.5 0Z" fill="#3ea5dd"/></svg>

    </div>
    <div class="ms-3">
      <div class="numbers">
        <a class="manage-action" data-action="update-message" onclick="ManageActionMessageDetails('{{$messagetype}}','{{$messageid}}','update-message')"> <h6 class="mb-1 text-dark text-sm">Update or Replace</h6></a>
        <p class="text-grey font-weight-bold text-xs mt-1 mb-0"> Replace or update this message</p>
      </div>
    </div>
  </div>
  <div class="d-flex mx-2  mb-3">
    <div>

      <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M20.496 5.627A2.25 2.25 0 0 1 22 7.75v10A4.25 4.25 0 0 1 17.75 22h-10a2.25 2.25 0 0 1-2.123-1.504l2.097.004H17.75a2.75 2.75 0 0 0 2.75-2.75v-10l-.004-.051V5.627ZM17.246 2a2.25 2.25 0 0 1 2.25 2.25v12.997a2.25 2.25 0 0 1-2.25 2.25H4.25A2.25 2.25 0 0 1 2 17.247V4.25A2.25 2.25 0 0 1 4.25 2h12.997ZM10.75 6.75a.75.75 0 0 0-.743.648L10 7.5V10H7.5a.75.75 0 0 0-.102 1.493l.102.007H10V14a.75.75 0 0 0 1.493.102L11.5 14v-2.5H14a.75.75 0 0 0 .102-1.493L14 10h-2.5V7.5a.75.75 0 0 0-.75-.75Z" fill="#3ea5dd"/></svg>

    </div>
    <div class="ms-3">
      <div class="numbers">
        <a class="manage-action" data-action="clone-message" onclick="ManageActionMessageDetails('{{$messagetype}}','{{$messageid}}','clone-message')"> <h6 class="mb-1 text-dark text-sm">Duplicate</h6></a>
        <p class="text-grey font-weight-bold text-xs mt-1 mb-0">Duplicate to send this message to oher recipient</p>
      </div>
    </div>
  </div>
  <div class="d-flex mx-2  mb-3">
    <div>

      <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M21.5 6a1 1 0 0 1-.883.993L20.5 7h-.845l-1.231 12.52A2.75 2.75 0 0 1 15.687 22H8.313a2.75 2.75 0 0 1-2.737-2.48L4.345 7H3.5a1 1 0 0 1 0-2h5a3.5 3.5 0 1 1 7 0h5a1 1 0 0 1 1 1Zm-7.25 3.25a.75.75 0 0 0-.743.648L13.5 10v7l.007.102a.75.75 0 0 0 1.486 0L15 17v-7l-.007-.102a.75.75 0 0 0-.743-.648Zm-4.5 0a.75.75 0 0 0-.743.648L9 10v7l.007.102a.75.75 0 0 0 1.486 0L10.5 17v-7l-.007-.102a.75.75 0 0 0-.743-.648ZM12 3.5A1.5 1.5 0 0 0 10.5 5h3A1.5 1.5 0 0 0 12 3.5Z" fill="#3ea5dd"/></svg>

    </div>
    <div class="ms-3">
      <div class="numbers">
        <a class="manage-action" data-action="delete-message" onclick="ManageActionMessageDetails('{{$messagetype}}','{{$messageid}}','delete-message')"> <h6 class="mb-1 text-dark text-sm">Delete</h6></a>
        <p class="text-grey font-weight-bold text-xs mt-1 mb-0"> Permanently delete this message</p>
      </div>
    </div>
  </div>
</div>
</div>



@if(isset($stepcount) && $stepcount==4)
<script type="text/javascript">
  swal({
    title: "Well done!",
    text: "Preferences completed successfully",
    icon: "success",
    button: "Ok",
  });


</script>
@endif

<script type="text/javascript">
  $(document).on('click','.showpesonalisedaddondiv', function(e) {
    e.preventDefault();  
    e.stopImmediatePropagation();
debugger;
          $(document).find('.pesonalisedaddondiv').toggle(); 
         if ($(document).find(".iconupdown").hasClass("fa-angle-up"))
         {
          
           $(document).find('.iconupdown').removeClass('fa-angle-up');
           $(document).find('.iconupdown').addClass('fa-angle-down');
         }
         else
         {
           
           $(document).find('.iconupdown').addClass('fa-angle-up');
           $(document).find('.iconupdown').removeClass('fa-angle-down');
         }
         
       
       
  });
</script>

