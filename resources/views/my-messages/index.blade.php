@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','All My Content')

@section('content')
<script>
  
  var ziggeoApp = new ZiggeoApi.V2.Application({
    token:"<?php echo env('ZIGGEOTOKEN');?>",
    auth:true
  });
</script>
<style type="text/css">
  .ba-videorecorder-chooser-button-1 {
    background-color: #eb6a74 !important;
    color: #FFF !important;
    margin-top: 20px;
    padding: 12px;
    border-radius: 4px;
    ;
  }

  .nav.nav-pills .nav-link.active {
    background: #344767 !important;
    color: white !important;
  }

  .nav.nav-pills .nav-link.active>svg {

    fill: white !important;
  }

  .btn-outline-primary {
    color: #344767;
    border-color: #344767;
    text-transform: none;
  }

  .moving-tab.position-absolute.nav-link {
    display: none;
  }

  .ba-videoplayer-container {
    display: inline-block;
    position: unset !important;
    overflow: hidden;
    z-index: 1;
  }

  .ba-audioplayer-container {
    display: inline-block;
    position: unset !important;
    overflow: hidden;
    z-index: 1;
  }


  .ba-videoplayer-container.ba-player-size-small.ba-videoplayer-noie8.ba-player-theme-modern.ba-player-normal-view.ba-player-common-browser.ba-player-red-color.ba-player-device-type-mobile {
    position: absolute;
    max-width: 100% !important;
    height: auto;
    display: block;
    margin: 0 auto;
  }

  .ba-audioplayer-container {
    max-width: 100% !important;
    height: auto;
    display: block;
    margin: 0 auto;
  }

  .manage-action {
    cursor: pointer;
  }
</style>


@section('headercommon')
<div class="container-fluid">
  <div class="page-header min-height-200 border-radius-xl mt-4" style="background-image: url('{{asset('assets/img/curved-images/curved0.jpg')}}'); background-position-y: 50%;">
    <span class="mask bg-primary opacity-9"></span>
  </div>
  <div class="card card-body blur shadow-blur mx-3 mt-n6 overflow-hidden">
    <div class="row gx-4">
      <div class="col-auto">
        <div class="avatar avatar-xl position-relative border-radius-xl bg-purple">
          <div class="card-body z-index-3">
            <i class="fi fi_library text-white" style="font-size: 34px" aria-hidden="true"></i>
          </div>
        </div>
      </div>

      @if($contenttype=='all-content')
      <div class="col-auto my-auto">
        <div class="h-100">
          <h5 class="mb-1 p-1 mt-2">
            All My Content
          </h5>
          <p class="mx-1 mb-0 font-weight-bold text-sm">
            To create new Content click on create new button option
          </p>
        </div>
      </div>
      <div class="col-lg-12 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto ">
        <div class="nav-wrapper position-relative end-0 mt-4">
          <ul class="nav nav-pills nav-fill p-1 bg-transparent" role="tablist">
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 @if($messagetype=='all') active @endif link-handle" data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=all" role="tab" aria-selected="true">
                <i class="fi fi_mail" aria-hidden="true"></i>
                <span class="ms-1">All Content</span>
              </a>
            </li>
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 link-handle @if($messagetype=='video') active @endif " data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=video" role="tab" aria-selected="false">
                <i class="fi fi_video" aria-hidden="true"></i>
                <span class="ms-1">Video Content</span>
              </a>
            </li>
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 link-handle @if($messagetype=='audio') active @endif" data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=audio" role="tab" aria-selected="false">
                <i class="fi fi_mic_on" aria-hidden="true"></i>
                <span class="ms-1">Audio Content</span>
              </a>
            </li>
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 link-handle @if($messagetype=='letter') active @endif" data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=letter" role="tab" aria-selected="false">
                <i class="fi fi_document_one_page" style="font-size: 20px;" aria-hidden="true"></i>
                <span class="ms-1">Written Content</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      @endif

      @if($contenttype=='personalised-content')
      <div class="col-auto my-auto">
        <div class="h-100">
          <h5 class="mb-1 p-1 mt-2">
            My Personalised Messages
          </h5>
          <p class="mx-1 mb-0 font-weight-bold text-sm">
            To create new personalised message click on create new button option
          </p>
        </div>
      </div>
      <div class="col-lg-12 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto ">
        <div class="nav-wrapper position-relative end-0 mt-4">
          <ul class="nav nav-pills nav-fill p-1 bg-transparent" role="tablist">
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 @if($messagetype=='all') active @endif link-handle" data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=all" role="tab" aria-selected="true">
                <i class="fi fi_mail" aria-hidden="true"></i>
                <span class="ms-1">All Messages</span>
              </a>
            </li>
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 link-handle @if($messagetype=='video') active @endif " data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=video" role="tab" aria-selected="false">
                <i class="fi fi_video" aria-hidden="true"></i>
                <span class="ms-1">Video Messages</span>
              </a>
            </li>
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 link-handle @if($messagetype=='audio') active @endif" data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=audio" role="tab" aria-selected="false">
                <i class="fi fi_mic_on" aria-hidden="true"></i>
                <span class="ms-1">Audio Messages</span>
              </a>
            </li>
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 link-handle @if($messagetype=='letter') active @endif" data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=letter" role="tab" aria-selected="false">
                <i class="fi fi_document_one_page" aria-hidden="true"></i>
                <span class="ms-1">Written Messages</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      @endif

      @if($contenttype=='qrcode-content')
      <div class="col-auto my-auto">
        <div class="h-100">
          <h5 class="mb-1 p-1 mt-2">
            My QR Code Content
          </h5>
          <p class="mx-1 mb-0 font-weight-bold text-sm">
            To create new QR code content click on create new button option
          </p>
        </div>
      </div>
      <div class="col-lg-12 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto ">
        <div class="nav-wrapper position-relative end-0 mt-4">
          <ul class="nav nav-pills nav-fill p-1 bg-transparent" role="tablist">
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 @if($messagetype=='all') active @endif link-handle" data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=all" role="tab" aria-selected="true">
                <i class="fi fi_mail" aria-hidden="true"></i>
                <span class="ms-1">All Messages</span>
              </a>
            </li>
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 link-handle @if($messagetype=='video') active @endif " data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=video" role="tab" aria-selected="false">
                <i class="fi fi_video" aria-hidden="true"></i>
                <span class="ms-1">Video Messages</span>
              </a>
            </li>
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 link-handle @if($messagetype=='audio') active @endif" data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=audio" role="tab" aria-selected="false">
                <i class="fi fi_mic_on" aria-hidden="true"></i>
                <span class="ms-1">Audio Messages</span>
              </a>
            </li>
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 link-handle @if($messagetype=='letter') active @endif" data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=letter" role="tab" aria-selected="false">
                <i class="fi fi_document_one_page" aria-hidden="true"></i>
                <span class="ms-1">Written Messages</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      @endif

      @if($contenttype=='heirloom-content')
      <div class="col-auto my-auto">
        <div class="h-100">
          <h5 class="mb-1 p-1 mt-2">
            My Heirloom Keepsake Content
          </h5>
          <p class="mx-1 mb-0 font-weight-bold text-sm">
            Create content to be hand delivered as a Heirloom Keepsake
          </p>
        </div>
      </div>
      <div class="col-lg-12 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto ">
        <div class="nav-wrapper position-relative end-0 mt-4">
          <ul class="nav nav-pills nav-fill p-1 bg-transparent" role="tablist">
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 @if($messagetype=='all') active @endif link-handle" data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=all" role="tab" aria-selected="true">
                <i class="fi fi_mail" aria-hidden="true"></i>
                <span class="ms-1">All Messages</span>
              </a>
            </li>
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 link-handle @if($messagetype=='video') active @endif " data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=video" role="tab" aria-selected="false">
                <i class="fi fi_video" aria-hidden="true"></i>
                <span class="ms-1">Video Messages</span>
              </a>
            </li>
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 link-handle @if($messagetype=='audio') active @endif" data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=audio" role="tab" aria-selected="false">
                <i class="fi fi_mic_on" aria-hidden="true"></i>
                <span class="ms-1">Audio Messages</span>
              </a>
            </li>
            <li class="nav-item mx-2 my-2">
              <a class="nav-link mb-0 px-0 py-1 link-handle @if($messagetype=='letter') active @endif" data-bs-toggle="tab" data-route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=letter" role="tab" aria-selected="false">
                <i class="fi fi_document_one_page" aria-hidden="true"></i>
                <span class="ms-1">Written Messages</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      @endif

    </div>
  </div>
</div>
@endsection

<div class="modal fade" id="MessagePlayer" tabindex="-1" role="dialog" aria-labelledby="MessagePlayerLabel" aria-hidden="true" >
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content " >
          <div class="modal-body p-6" id="popupplayer">
            
  </div>
</div>
</div>
</div>

<div class="modal fade" id="MessageDetails" tabindex="-1" role="dialog" aria-labelledby="MessageDetailsLabel" aria-hidden="true" >
      <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content p-0" >
          <div class="modal-body p-0" id="MessageDetailsBody">
        
    
  </div>
</div>  
  </div>
</div>

<div class="modal fade" id="MessageQRDetails" tabindex="-1" role="dialog" aria-labelledby="MessageQRDetailsLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-md" role="document">
    <div class="modal-content p-0">
      <div class="modal-body p-0" id="MessageQRDetailsBody">
      </div>
    </div>
  </div>
</div>



@if(isset($usersubscription) && !empty($usersubscription) && $usersubscription->demopackage==1)

<div class="modal fade" id="qr-package-popup" tabindex="-1" role="dialog" aria-labelledby="qr-popup-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg
" role="document">
            <div class="modal-content p-0">
                <div class="modal-body p-0" id="package-popup-body">

                    <section class="pricing-box-con">
                        <div class="container">
                            <h5 class="text-center">Upgrade your package to add QR Code</h5>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

                                        
                                        <!-- tab 1 content start-->
                                        <div class="tab-pane fade show active" id="nav-ConnectionContent" data-toggle="tab-content">
                                            <div class="pricing-box-div">
                                                <div class="row">
                                                    @if(isset($qrcodestandalone) && count($qrcodestandalone)>0)
                                                    @foreach($qrcodestandalone as $up_key=> $dataup)
                                                    
                                                       <div class="col-md-6">
                                                        <form action="{{route('dashboard.purchase-subscription',$dataup->package_id)}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="redirecturl" value="{{Request::url()}}">
                                                            <input type="hidden" name="upgradepackage" value="true">
                                                            <input type="hidden" name="package_id" value="{{$dataup->package_id}}">
                                                            <div class="pricing-box">

                                                                <div class="icon"><svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M8 6H6v2h2V6Z" fill="#773eb1"/><path d="M3 5.5A2.5 2.5 0 0 1 5.5 3h3A2.5 2.5 0 0 1 11 5.5v3A2.5 2.5 0 0 1 8.5 11h-3A2.5 2.5 0 0 1 3 8.5v-3ZM5.5 5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3ZM6 16h2v2H6v-2Z" fill="#773eb1"/><path d="M3 15.5A2.5 2.5 0 0 1 5.5 13h3a2.5 2.5 0 0 1 2.5 2.5v3A2.5 2.5 0 0 1 8.5 21h-3A2.5 2.5 0 0 1 3 18.5v-3Zm2.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3ZM18 6h-2v2h2V6Z" fill="#773eb1"/><path d="M15.5 3A2.5 2.5 0 0 0 13 5.5v3a2.5 2.5 0 0 0 2.5 2.5h3A2.5 2.5 0 0 0 21 8.5v-3A2.5 2.5 0 0 0 18.5 3h-3ZM15 5.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-3ZM13 13h2.75v2.75H13V13ZM18.25 15.75h-2.5v2.5H13V21h2.75v-2.75h2.5V21H21v-2.75h-2.75v-2.5ZM18.25 15.75V13H21v2.75h-2.75Z" fill="#773eb1"/></svg>
                                                                </div>
                                                                <h4>{{$dataup->name}}</h4>

                                                                <p>{{$dataup->saleheadline}}</p>

                                                                <h5>${{$dataup->price}}</h5>
                                                               
                                                                <h6><span>{{$dataup->salequotes}}</span></h6>

                                                                <div class="content text-start">
                                                                <p>{{$dataup->quotes}}</p>
                                                            </div>
                                                                
                                                                <div class="list">
                                                                    <br>
                                                                    <p><strong>Package Inclusions</strong></p>
                                                                    <ul>
                                                                        @if(isset($dataup->inclusions) && !empty($dataup->inclusions))
                                                                        @foreach($dataup->inclusions as $inclusiondata)
                                                                        @if($inclusiondata->level==1)
                                                                        <li class="text-sm"><?php echo $inclusiondata->inclusion_name; ?></li>
                                                                        @endif
                                                                        @if($inclusiondata->level==2)
                                                                        <ul>
                                                                            <li class="text-sm"><?php echo $inclusiondata->inclusion_name; ?></li>
                                                                        </ul>
                                                                        @endif
                                                                        @endforeach
                                                                        @endif
                                                                    </ul>
                                                                
                                                                @if(isset($community_packages_coming_soon[$dataup->package_id]) &&
                                                                count($community_packages_coming_soon[$dataup->package_id])>0)
                                                                <p><strong>Coming soon</strong></p>
                                                                @foreach($community_packages_coming_soon[$dataup->package_id] as $package_comming_soon)
                                                                <div class="row mt-3" @if($package_comming_soon->level == 2)
                                                                    style="padding-left:30px" @endif>
                                                                    <div class="col-2">
                                                                        <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                            <path d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2Zm3.22 6.97-4.47 4.47-1.97-1.97a.75.75 0 0 0-1.06 1.06l2.5 2.5a.75.75 0 0 0 1.06 0l5-5a.75.75 0 1 0-1.06-1.06Z" fill="#abb0b4" />
                                                                        </svg>
                                                                    </div>

                                                                    <span class="text-sm ps-2 col-10">{{$package_comming_soon->name}}</span>

                                                                </div>
                                                                @endforeach
                                                                @endif
                                                                </div>
                                                                <div class="button-con">


                                                                    <button type="submit" name="purchasesubscription" value="notrial">Purchase Now</button>

                                                                </div>
                                                            </div>
                                                        </form>
                                                  </div>

                                                
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>

                </section>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="heirloom-package-popup" tabindex="-1" role="dialog" aria-labelledby="heirloom-popup-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md
" role="document">
            <div class="modal-content p-0">
                <div class="modal-body p-0" id="package-popup-body">

                    <section class="pricing-box-con">
                        <div class="container">
                             <h6 class="text-center">Upgrade your package to add Heirloom Keepsake</h6>
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

                                       
                                        <!-- tab 1 content start-->
                                        <div class="tab-pane fade show active" id="nav-ConnectionContent" data-toggle="tab-content">
                                            <div class="pricing-box-div">
                                                <div class="row">
                                                    @if(isset($handdeliveredstandalone) && count($handdeliveredstandalone)>0)
                                                    @foreach($handdeliveredstandalone as $h_key=> $hdataup)
                                                    
                                                       <div class="col-md-12">
                                                        <form action="{{route('dashboard.purchase-subscription',$hdataup->package_id)}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="upgradepackage" value="true">
                                                            <input type="hidden" name="redirecturl" value="{{Request::url()}}">
                                                            <input type="hidden" name="package_id" value="{{$hdataup->package_id}}">
                                                            <div class="pricing-box">

                                                                <div class="icon"><svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M11.25 13v9h-4A3.25 3.25 0 0 1 4 18.75V13h7.25ZM20 13v5.75A3.25 3.25 0 0 1 16.75 22h-4v-9H20ZM14.5 2a3.25 3.25 0 0 1 2.738 5.002L19.75 7c.69 0 1.25.466 1.25 1.042v2.916c0 .576-.56 1.042-1.25 1.042l-7-.001V7h-1.5v4.999l-7 .001C3.56 12 3 11.534 3 10.958V8.042C3 7.466 3.56 7 4.25 7l2.512.002A3.25 3.25 0 0 1 12 3.174 3.24 3.24 0 0 1 14.5 2Zm-5 1.5a1.75 1.75 0 0 0-.144 3.494L9.5 7h1.75V5.25l-.006-.144A1.75 1.75 0 0 0 9.5 3.5Zm5 0a1.75 1.75 0 0 0-1.75 1.75V7h1.75a1.75 1.75 0 1 0 0-3.5Z" fill="#773eb1"/></svg>
                                                                </div>
                                                                <h4>{{$hdataup->name}}</h4>

                                                                

                                                                <h5>${{$hdataup->price}}</h5>
                                                               

                                                                
                                                                <div class="list">
                                                                    <br>
                                                                    <p><strong>Package Inclusions</strong></p>
                                                                    <ul>
                                                                        @if(isset($hdataup->inclusions) && !empty($hdataup->inclusions))
                                                                        @foreach($hdataup->inclusions as $inclusiondata)
                                                                        @if($inclusiondata->level==1)
                                                                        <li class="text-sm"><?php echo $inclusiondata->inclusion_name; ?></li>
                                                                        @endif
                                                                        @if($inclusiondata->level==2)
                                                                        <ul>
                                                                            <li class="text-sm"><?php echo $inclusiondata->inclusion_name; ?></li>
                                                                        </ul>
                                                                        @endif
                                                                        @endforeach
                                                                        @endif
                                                                    </ul>
                                                                
                                                                @if(isset($community_packages_coming_soon[$dataup->package_id]) &&
                                                                count($community_packages_coming_soon[$dataup->package_id])>0)
                                                                <p><strong>Coming soon</strong></p>
                                                                @foreach($community_packages_coming_soon[$dataup->package_id] as $package_comming_soon)
                                                                <div class="row mt-3" @if($package_comming_soon->level == 2)
                                                                    style="padding-left:30px" @endif>
                                                                    <div class="col-2">
                                                                        <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                            <path d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2Zm3.22 6.97-4.47 4.47-1.97-1.97a.75.75 0 0 0-1.06 1.06l2.5 2.5a.75.75 0 0 0 1.06 0l5-5a.75.75 0 1 0-1.06-1.06Z" fill="#abb0b4" />
                                                                        </svg>
                                                                    </div>

                                                                    <span class="text-sm ps-2 col-10">{{$package_comming_soon->name}}</span>

                                                                </div>
                                                                @endforeach
                                                                @endif
                                                                </div>
                                                                <div class="button-con">


                                                                    <button type="submit" name="purchasesubscription" value="notrial">Purchase Now</button>

                                                                </div>
                                                            </div>
                                                        </form>
                                                  </div>

                                                
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>

                </section>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .pricing-page-wraper {
        width: 100%;
        background: #f3eef6;
        padding: 0 0 60px;
        float: left;
    }

    .pricing-page-menu {
        width: 100%;
        float: left;
        text-align: center;
        background: #fff;
        box-shadow: 0 3px 8px #0000003b;
        margin: 0;
        padding: 19px 0;
    }

    .pricing-page-menu nav {
        display: inline-block;
    }

    .pricing-page-menu ul li a,
    .pricing-page-menu nav .nav-fill .nav-item {
        display: inline-block;
        position: relative;
        padding: 0 0 1px;
        margin: 0 15px;
        color: #662d91;
        transition: all ease-in-out 1.0s;
        border: none;
    }

    .pricing-page-menu .nav-tabs {
        border-bottom: none;
    }

    .pricing-page-menu ul li a:before,
    .pricing-page-menu nav .nav-fill .nav-item:before {
        content: '';
        width: 0;
        height: 2px;
        position: absolute;
        background: #662d91;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        transition: all ease-in-out 1.0s;
    }

    .pricing-page-menu ul li a:hover,
    .pricing-page-menu nav .nav-fill .nav-item:before {
        color: #662d91;
        text-decoration: none;
    }

    .pricing-page-menu ul li:hover a:before,
    .pricing-page-menu ul li.active a:before,
    .pricing-page-menu nav .nav-fill .nav-item:hover:before,
    .pricing-page-menu nav .nav-fill .nav-item.active:before {
        width: 100%;
    }

    .pricing-page-menu ul {
        display: inline-block;
    }

    .pricing-page-menu ul li {
        display: inline-block;
    }

    section.pricing-box-con {
        text-align: center;
    }

    section.pricing-box-con {
        text-align: center;
        width: 100%;
        float: left;
        padding: 30px 0 0;
        position: relative;
        background: url(../assets/img/BackgroundLineWhite.svg) no-repeat;
        background-size: 60%;
    }

    .pricing-box-wrapper {
        width: 100%;
        float: left;
        background: #fff;
        padding: 30px;
        border-radius: 10px;
        box-shadow: 0 0 8px #0000003b;
        margin-bottom: 30px;
    }

    .heirloom-keepsake {
        margin-top: 50px;
    }



    .pricing-box-wrapper:before {
        content: '';
        position: absolute;
        top: 5%;
        right: 20%;
        background: url(../assets/img/element-1.png) no-repeat;
        width: 95px;
        height: 77px;
    }

    #nav-HeirloomKeepsake .pricing-box-wrapper:before {
        display: none;
    }

    .heading-group {
        width: 100%;
        float: left;
    }

    .heading-group h5 {
        font-size: 18px;
        color: #662d91;
        margin: 20px 0;
    }

    .heading-group h6 {
        margin: 0 0 30px;
    }

    .heading-group h6 span {
        display: inline-block;
        padding: 10px 21px;
        background: #f3eef6;
        border-radius: 10px;
        font-size: 14px;
        color: #662d91;
    }

    .heading-group h2 {
        margin-bottom: 0;
    }

    section.pricing-box-con nav {
        display: inline-block;
        border: 1px solid #f2f3f4;
        border-radius: 30px;
        overflow: hidden;
        background: #fbfbfc;
    }

    section.pricing-box-con nav .nav-tabs .nav-link {
        border-radius: 30px !important;
        color: #979899;
    }

    section.pricing-box-con nav .nav-tabs .nav-link span {
        background: #eef0f2;
        padding: 2px 5px;
        border-radius: 5px;
        font-size: 11px;
    }

    section.pricing-box-con.nav-tabs .nav-item.show .nav-link,
    section.pricing-box-con .nav-tabs .nav-link.active,
    section.pricing-box-con.nav-tabs .nav-item.show .nav-link:hover {
        color: #7d4da1;
        background-color: #fff;
        border-color: #7d4da1;
    }

    section.pricing-box-con-2 {
        width: 100%;
        float: left;
        padding: 60px 0;
    }

    section.pricing-box-con-2 .left {
        padding: 60px 0 0;
        position: relative;
    }

    section.pricing-box-con-2 .left .btn {
        background: #662d91;
        color: #fff;
    }

    section.pricing-box-con-2 .left .btn.btn-2 {
        background: #fff;
        color: #662d91;
        margin-right: 10px;
        padding: 7px 22px;
        border-radius: 30px;
    }

    section.pricing-box-con-2 h2 {
        color: #662d91;
        margin-bottom: 30px;
        background: url(../assets/img/b1.jpg) no-repeat;
        background-position: left bottom;
        padding-bottom: 0px;
    }

    .pricing-box {
        width: 100%;
        border: 2px solid #edeff1;
        padding: 30px;
        border-radius: 10px;
        margin-bottom: 30px;
    }

    .pricing-box {
        width: 100%;
        border: 2px solid #edeff1;
        padding: 30px;
        border-radius: 10px;
        margin-bottom: 30px;
        text-align: center;
        transition: all ease-in-out 0.5s;
    }

    .pricing-box:hover,
    .pricing-box.active {
        border-color: #662d91;
    }

    .pricing-box .button-con button:hover {
        background: #662d91;
        color: #fff;
    }

    .pricing-box .button-con button:first-child {
        background: #662d91;
        color: #fff;
    }

    .pricing-box .button-con button:first-child:hover {
        background: #f3eef6;
        color: #662d91;
    }

    .pricing-box .icon i {
        color: #662d91;
        font-size: 37px;
    }

    .pricing-box h4,
    .pricing-box h5 {
        font-family: 'Poppins', sans-serif;
        margin: 20px 0;
        font-size: 20px;
    }

    .pricing-box h4 span {
        display: inline-block;
        background: #f3eef6;
        padding: 9px 15px;
        border-radius: 10px;
        margin: 0 0 0 13px;
        color: #662d91;
        font-size: 14px;
        float: right;
    }

    .pricing-box h5 {
        font-size: 33px;
        color: #662d91;
        margin: 0 0;
    }

    .pricing-box h6 span {
        display: inline-block;
        background: #f3eef6;
        padding: 9px 22px;
        border-radius: 10px;
        margin: 10px 0;
        color: #662d91;
        font-size: 14px;
    }

    .pricing-box .list {
        text-align: left;
    }

    .pricing-box .list ul li {
        position: relative;
        padding-left: 24px;
        margin-bottom: 10px;
        list-style: none;
    }

    .pricing-box .list ul li ul {
        margin: 10px 0;
    }

    .pricing-box p {
        margin-top: 25px;
        margin-bottom: 5px;
    }

    .pricing-box .list ul li:before {
        font: normal normal normal 14px/1 FontAwesome;
        content: "\f00c";
        width: 15px;
        height: 15px;
        border-radius: 20px;
        background: #662d91;
        color: #fff;
        position: absolute;
        left: 0;
        top: 2px;
        font-size: 11px;
        text-align: center;
        line-height: 15px;
    }

    .pricing-box .list ul.sub li:before {
        background: #8b9197 !important;
    }

    .pricing-box .button-con {
        margin-top: 30px;
    }

    .pricing-box .button-con button {
        background: #f3eef6;
        border: 1px solid #f3eef6;
        width: 100%;
        padding: 10px;
        border-radius: 23px;
        color: #662d91;
        margin-top: 13px;
    }

    .pricing-expand-con {
        text-align: left;
        color: #9aa0a5;
    }

    .more,
    #more_2,
    #more_3,
    #more_4 {
        display: none;
    }

    .pricing-expand-content button span {
        font-size: 47px;
        font-weight: normal;
        position: absolute;
        top: -10px;
        right: 0;
        height: 10px;
    }

    .pricing-expand-content {
        position: relative;
        width: 100%;
        padding-top: 21px;
    }

    .pricing-expand-con p {
        margin-bottom: 8px;
        color: #9aa0a5;
    }

    .expand-content {
        border-top: 1px solid #edeeef;
        width: 100%;
        float: left;
        padding-top: 10px;
        margin: 10px 0 0;
    }

    .pricing-expand-content button {
        position: absolute;
        background: none;
        border: none;
        padding: 0;
        margin: 0;
        top: 0;
        left: 0;
        color: #9aa0a5;

    }

    .pricing-expand-content button:before {
        content: '';
    }

    .pricing-expand-content button:focus {
        border: none;
        outline: none;
        stroke: none;
    }

    .pricing-expand-content button {
        position: absolute;
        background: none;
        border: none;
        padding: 0;
        margin: 0;
        top: 0;
        left: 0;
        padding-right: 20px;
    }
</style>
 @endif 



  <section>
       
      
      

  <div class="row">
           @if($contenttype=='all-content')
    <!-- CREATE CONTENT BUTTON -->
            <div class="col-lg-4 col-md-6 mb-4">
        <div class="card btn h-100 py-4 bg-purple shadow-xl">
              <div class="card-body d-flex flex-column justify-content-center text-center">
                <a data-route="{{route('create-content')}}" class="link-handle">
            <i class="fi fi_add text-white" style="font-size: 32px" aria-hidden="true"></i>
            <h6 class="text-white">Create new content</h6>
                </a>
              </div>
            </div>
          </div>
           @else
          <div class="col-lg-4 col-md-6 mb-4">
            <div class="card h-100 py-4">
              <div class="card-body d-flex flex-column justify-content-center text-center">
                <a data-route="{{route('my-messages.create')}}?type={{$contenttype}}" class="link-handle">
                  <i class="fa fa-plus text-dark mb-3" aria-hidden="true"></i>
            <h5 class="text-white"> Create a new message </h5>
                </a>
              </div>
            </div>
          </div>
          @endif

    <!-- VIDEO CONTENT -->
    @if(isset($videolist) && count($videolist)>0)
    @foreach($videolist as $videodata)
    <div class="col-lg-4 col-md-6 mb-4">
      <div class="card shadow-xl">
        <div class="card-body">
          <div class="row gx-4">
            <div class="col-auto mb-1">
              <!-- Avatar Large -->
              <div class="avatar avatar-xl position-relative border-radius-xl bg-purple-pale d-none d-lg-block d-xl-block cursor-pointer" onclick="VideoMessagePlayer('{{$videodata->tags}}','{{$videodata->video_token_id}}','{{$ziggeoauthtoken}}')">
                <div class="card-body z-index-3 text-center p-3">
                  <i class="fi fi_video text-purple" style="font-size: 42px;" aria-hidden="true"></i>
                </div>
              </div>
              <!-- Avatar Small -->
              <div class="avatar avatar-lg position-relative border-radius-xl bg-purple-pale d-block d-lg-none d-xl-none cursor-pointer" onclick="VideoMessagePlayer('{{$videodata->tags}}','{{$videodata->video_token_id}}','{{$ziggeoauthtoken}}')">
                <div class="card-body z-index-3 text-center p-2 py-3">
                  <i class="fi fi_video text-purple" style="font-size: 28px" aria-hidden="true"></i>
                </div>
              </div>
            </div>
            <div class="col-auto">
              @if(isset($videodata->user_recipient_id) && !empty($videodata->user_recipient_id))
              <p class="text-secondary mb-0 text-dark font-weight-bold">Message for <span class="text-primary font-weight-bold">{{$videodata->first_name}} {{$videodata->last_name}}</span></p>
              <span class="badge bg-purple">Video Message</span>
              <span class="badge bg-success">Allocated</span>
              @else
              <p class="mb-0 text-danger font-weight-bold">Unallocated Message</p>
              <span class="badge bg-purple">Video Message</span>
              @endif
              @if(isset($videodata->qrid) && !empty($videodata->qrid))
              <div class="row mt-2">
                <div class="col-auto">
                  <a class="cursor-pointer" onclick="OpenMessageQRDetails('video','{{$videodata->ziggeo_user_video_id}}')">
                    <span class="fi fi_qr_code text-dark" style="font-size: 18px;" aria-hidden="true"></span>
                  </a>
                </div>
                <div class="col-auto p-0">
                  <a class="cursor-pointer" onclick="OpenMessageQRDetails('video','{{$videodata->ziggeo_user_video_id}}')">
                    <p class="text-sm mb-0">
                      View QR Code
                      <i class="fi fi_chevron_right text-dark" style="font-size: 10px;" aria-hidden="true"></i>
                    </p>
                  </a>
                </div>
              </div>
              @endif
              @if(isset($videodata->heilroom_package_id) && !empty($videodata->heilroom_package_id))
              <div class="row mt-2">
                <div class="col-auto">
                  <span class="fi fi_gift text-dark" style="font-size: 18px;" aria-hidden="true"></span>
                </div>
                <div class="col-auto p-0">
                  <p class="text-sm mb-0">
                    Heirloom Keepsake
                  </p>
                </div>
              </div>
              @endif
            </div>
          </div>
          <hr class="horizontal dark mt-2" />
          <div class="row">
            <div class="col-6 ">
              @if(isset($videodata->preference_completed) && $videodata->preference_completed=='1' )
              <button type="button" class="btn btn-outline-primary btn-xs mb-0 p-2" onclick="OpenMessageDetails('video','{{$videodata->ziggeo_user_video_id}}')">Update Preferences</button>
              @else
              <button type="button" class="btn btn-outline-primary btn-xs mb-0 p-2" onclick="OpenMessageDetails('video','{{$videodata->ziggeo_user_video_id}}')">Choose Preferences</button>
              @endif
            </div>
            <div class="col-6 text-end">
              <h6 class="text-xs mb-0"><?php echo isset($videodata->created_at) && !empty($videodata->created_at) ? date('d F Y', strtotime($videodata->created_at)) : ''; ?></h6>
              <p class="text-primary text-xs font-weight-bold mb-0 pt-1"><i class="fi fi_calendar_ltr pe-2" aria-hidden="true"></i>Created date </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach
    @endif


    <!-- AUDIO CONTENT -->
    @if(isset($audiolist) && count($audiolist)>0)
    @foreach($audiolist as $audiodata)
    <div class="col-lg-4 col-md-6 mb-4">
      <div class="card shadow-xl">
        <div class="card-body">
          <div class="row gx-4">
            <div class="col-auto mb-1">
              <!-- Avatar Large -->
              <div class="avatar avatar-xl position-relative border-radius-xl bg-orange-pale d-none d-lg-block d-xl-block cursor-pointer" onclick="AudioMessagePlayer('{{$audiodata->tags}}','{{$audiodata->audio_token_id}}','{{$ziggeoauthtoken}}')">
                <div class="card-body z-index-3 text-center p-3">
                  <i class="fi fi_mic_on text-orange" style="font-size: 42px;" aria-hidden="true"></i>
                </div>
              </div>
              <!-- Avatar Small -->
              <div class="avatar avatar-lg position-relative border-radius-xl bg-orange-pale d-block d-lg-none d-xl-none cursor-pointer" onclick="AudioMessagePlayer('{{$audiodata->tags}}','{{$audiodata->audio_token_id}}','{{$ziggeoauthtoken}}')">
                <div class="card-body z-index-3 text-center p-2 py-3">
                  <i class="fi fi_mic_on text-orange" style="font-size: 28px" aria-hidden="true"></i>
                </div>
              </div>
            </div>
            <div class="col-auto">
              @if(isset($audiodata->user_recipient_id) && !empty($audiodata->user_recipient_id))
              <p class="text-secondary mb-0 text-dark font-weight-bold">Message for <span class="text-primary font-weight-bold">{{$audiodata->first_name}} {{$audiodata->last_name}}</span></p>
              <span class="badge bg-purple">Audio Message</span>
              <span class="badge bg-success">Allocated</span>
              @else
              <p class="mb-0 text-danger font-weight-bold">Unallocated Message</p>
              <span class="badge bg-purple">Audio Message</span>
              @endif
              @if(isset($audiodata->qrid) && !empty($audiodata->qrid))
              <div class="row mt-2">
                <div class="col-auto">
                  <a class="cursor-pointer" onclick="OpenMessageQRDetails('audio','{{$audiodata->ziggeo_user_audio_id}}')">
                    <span class="fi fi_qr_code text-dark" style="font-size: 18px;" aria-hidden="true"></span>
                  </a>
                </div>
                <div class="col-auto p-0">
                  <a class="cursor-pointer" onclick="OpenMessageQRDetails('audio','{{$audiodata->ziggeo_user_audio_id}}')">
                    <p class="text-sm mb-0">
                      View QR Code
                      <i class="fi fi_chevron_right text-dark" style="font-size: 10px;" aria-hidden="true"></i>
                    </p>
                  </a>
                </div>
              </div>
              @endif
              @if(isset($audiodata->heilroom_package_id) && !empty($audiodata->heilroom_package_id))
              <div class="row mt-2">
                <div class="col-auto">
                  <span class="fi fi_gift text-dark" style="font-size: 18px;" aria-hidden="true"></span>
                </div>
                <div class="col-auto p-0">
                  <p class="text-sm mb-0">
                    Heirloom Keepsake
                  </p>
                </div>
              </div>
              @endif
            </div>
          </div>
          <hr class="horizontal dark mt-2" />
          <div class="row">
            <div class="col-6 ">
              @if(isset($audiodata->preference_completed) && $audiodata->preference_completed=='1' )
              <button type="button" class="btn btn-outline-primary btn-xs mb-0 p-2" onclick="OpenMessageDetails('audio','{{$audiodata->ziggeo_user_audio_id}}')">Update Preferences</button>
              @else
              <button type="button" class="btn btn-outline-primary btn-xs mb-0 p-2" onclick="OpenMessageDetails('audio','{{$audiodata->ziggeo_user_audio_id}}')">Choose Preferences</button>
              @endif
            </div>
            <div class="col-6 text-end">
              <h6 class="text-xs mb-0"><?php echo isset($audiodata->created_at) && !empty($audiodata->created_at) ? date('d F Y', strtotime($audiodata->created_at)) : ''; ?></h6>
              <p class="text-primary text-xs font-weight-bold mb-0 pt-1"><i class="fi fi_calendar_ltr pe-2" aria-hidden="true"></i>Created date </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach
    @endif

    <!-- WRITTEN CONTENT -->
     @if(isset($textlist) && count($textlist)>0)
        @foreach($textlist as $textdata)
        <?php $type='';?> 
         @if($textdata->capture_type=='written')
         <?php $type='Notes'; ?>
         
         @endif
         
         @if($textdata->capture_type=='uploaded')  
         <?php $type='Pdf'; ?> 
        
         @endif
          <div class="col-lg-4 col-md-6 mb-4">
      <div class="card shadow-xl">
        <div class="card-body">
          <div class="row gx-4">
            <div class="col-auto mb-1">
              <!-- Avatar Large -->
              <div class="avatar avatar-xl position-relative border-radius-xl bg-blue-pale d-none d-lg-block d-xl-block cursor-pointer link-handle" data-route="{{route('my-messages.text-show',$textdata->ziggeo_user_text_id)}}">
                <div class=" card-body z-index-3 text-center p-3">
                  <i class="fi fi_document_one_page text-blue" style="font-size: 42px;" aria-hidden="true"></i>
                </div>
              </div>
              <!-- Avatar Small -->
              <div class="avatar avatar-lg position-relative border-radius-xl bg-blue-pale d-block d-lg-none d-xl-none cursor-pointer link-handle" data-route="{{route('my-messages.text-show',$textdata->ziggeo_user_text_id)}}">
                <div class=" card-body z-index-3 text-center p-2 py-3">
                  <i class="fi fi_document_one_page text-blue" style="font-size: 28px" aria-hidden="true"></i>
                </div>
              </div>
            </div>
            <div class="col-auto">
              @if(isset($textdata->user_recipient_id) && !empty($textdata->user_recipient_id))
              <p class="text-secondary mb-0 text-dark font-weight-bold">Message for <span class="text-primary font-weight-bold">{{$textdata->first_name}} {{$textdata->last_name}}</span></p>
              <span class="badge bg-purple">Written Message</span>
              <span class="badge bg-success">Allocated</span>
              @else
              <p class="mb-0 text-danger font-weight-bold">Unallocated Message</p>
              <span class="badge bg-purple">Written Message</span>
              @endif
              @if(isset($textdata->qrid) && !empty($textdata->qrid))
              <div class="row mt-2">
                <div class="col-auto">
                  <a class="cursor-pointer" onclick="OpenMessageQRDetails('letter','{{$textdata->ziggeo_user_text_id}}')"><span class="fi fi_qr_code text-dark" style="font-size: 18px;" aria-hidden="true"></span></a>
                </div>
                <div class="col-auto p-0">
                  <a class="cursor-pointer" onclick="OpenMessageQRDetails('letter','{{$textdata->ziggeo_user_text_id}}')">
                    <p class="text-sm mb-0">
                      View QR Code
                      <i class="fi fi_chevron_right text-dark" style="font-size: 10px;" aria-hidden="true"></i>
                    </p>
                  </a>
                </div>
              </div>
              @endif
              @if(isset($textdata->heilroom_package_id) && !empty($textdata->heilroom_package_id))
              <div class="row mt-2">
                <div class="col-auto">
                  <span class="fi fi_gift text-dark" style="font-size: 18px;" aria-hidden="true"></span>
                </div>
                <div class="col-auto p-0">
                  <p class="text-sm mb-0">
                    Heirloom Keepsake
                  </p>
                </div>
              </div>
              @endif
            </div>
          </div>
          <hr class="horizontal dark mt-2" />
          <div class="row">
            <div class="col-6 ">
              @if(isset($textdata->preference_completed) && $textdata->preference_completed=='1' )
              <button type="button" class="btn btn-outline-primary btn-xs mb-0 p-2" onclick="OpenMessageDetails('letter','{{$textdata->ziggeo_user_text_id}}')">Update Preferences</button>
              @else
              <button type="button" class="btn btn-outline-primary btn-xs mb-0 p-2" onclick="OpenMessageDetails('letter','{{$textdata->ziggeo_user_text_id}}')">Choose Preferences</button>
              @endif
            </div>
            <div class="col-6 text-end">
              <h6 class="text-xs mb-0"><?php echo isset($textdata->created_at) && !empty($textdata->created_at) ? date('d F Y', strtotime($textdata->created_at)) : ''; ?></h6>
              <p class="text-primary text-xs font-weight-bold mb-0 pt-1"><i class="fi fi_calendar_ltr pe-2" aria-hidden="true"></i>Created date </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach
    @endif


   </div>
 </section>
 <script type="text/javascript">

  var popstateroute="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype={{$messagetype}}";

  var requestshowpop="{{$old->showpopup}}";
  if(requestshowpop=='true')
  {
    $('#MessageDetails').modal('show');
  }

  var showpopupnewmessagecreated="{{$showpopupnew}}";
  var hasdemopackage="{{$hasdemopackage}}";
  var messagedataid="<?php echo isset($ziggeomessagedata->id) ? $ziggeomessagedata->id : null;?>"
  
  if(hasdemopackage=='1') 
  {
     // OpenSubscriptionWarning();
  }

  if(showpopupnewmessagecreated==true && messagedataid)
  {
    OpenMessageDetails('{{$messagetype}}',messagedataid);
  }



  // function OpenSubscriptionWarning()
  // {
  //   swal({
  //       text: "Hi! Please Purchase Subscription to Unlock & Update Preferences of Your Message",
  //       icon: "warning",
  //       buttons: {
  //           confirm: {
  //               text: 'Buy Subscription',
  //               className: ' text-white bg-gradient-primary'
  //           },
  //           cancel: "Cancel"
  //       },
  //       }).then((will) => {

  //           if (will)
  //           {
  //               window.location="{{route('subscription-packages')}}";
  //           }
  //           else 
  //           {
  //               window.location="{{route('my-content')}}";
  //           }

  //       });
  // }

  $( ".nav-link-switch" ).click(function() {

    $('.nav-link-switch').removeClass('active');
    $(this).addClass('active');


  });

  $(document).on('click','.showqr-package-popup', function(e) {

        $('#qr-package-popup').modal('show');
        $('#MessageDetails').animate({ opacity: 0 }, 100);
       
       
  });

  $(document).on('click','.showheirloom-package-popup', function(e) {

        $('#heirloom-package-popup').modal('show');
        $('#MessageDetails').animate({ opacity: 0 }, 100);
       
       
  });

  

  function OpenMessageDetails(messagetype,id)
  {

    $.ajax({
        url: "{{route('my-messages.show-message-details')}}",
        dataType: 'json',
        type: 'GET',
        data: {
          messagetype:messagetype,
          id:id
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function() {
          jQuery(document).find('.loadingOverlay').show();
          
          jQuery(document).find('.loader').show();
      },
      success: function(response) {

        if(response.status=='success')
        {
          $("#MessageDetailsBody").html(response.html);

          if(!$('#MessageDetails').hasClass('show'))
          {
             $('#MessageDetails').modal('show');
             window.history.pushState('page',"After I Go",popstateroute);
          }
          

         
        }
      },
      error: function(xhr) { // if error occured
          alert("Error occured.please try again");
      },
      complete: function() {

          $('.select2').select2();
          jQuery(document).find('.loadingOverlay').hide();
           jQuery(document).find('.loader').hide();
          

      },
    });

  }

  function OpenMessageQRDetails(messagetype,id)
  {

    $.ajax({
        url: "{{route('my-messages.show-message-qr')}}",
        dataType: 'json',
        type: 'POST',
        data: {
          messagetype:messagetype,
          id:id
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function() {
          jQuery(document).find('.loadingOverlay').show();
          
          jQuery(document).find('.loader').show();
      },
      success: function(response) {

        if(response.status=='success')
        {
          $("#MessageDetailsBody").html(response.html);

          if(!$('#MessageDetails').hasClass('show'))
          {
             $('#MessageDetails').modal('show');
          }
          
         
        }
      },
      error: function(xhr) { // if error occured
          alert("Error occured.please try again");
      },
      complete: function() {

          $('.select2').select2();
          jQuery(document).find('.loadingOverlay').hide();
           jQuery(document).find('.loader').hide();
          

      },
    });

  }

  function ManageActionMessageDetails(messagetype,id,actiontype)
  {

    $.ajax({
        url: "{{route('my-messages.manage-action-message-details')}}",
        dataType: 'json',
        type: 'POST',
        data: {
          messagetype:messagetype,
          id:id,
          actiontype:actiontype
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function() {
          jQuery(document).find('.loadingOverlay').show();
          
          jQuery(document).find('.loader').show();
      },
      success: function(response) {

        if(response.status=='success')
        {
          $("#MessageDetailsBody").html(response.html);
          
        }
      },
      error: function(xhr) { // if error occured
          alert("Error occured.please try again");
      },
      complete: function() {

          $('.select2').select2();
          jQuery(document).find('.loadingOverlay').hide();
          jQuery(document).find('.loader').hide();
          

      },
    });

  }

  

 
  $(document).on('click','.PerformActionsMessage', function(e) {
    e.preventDefault();  
    e.stopImmediatePropagation();
    var obj =$(this);
    form = obj.closest('form');
    var route = form.attr('action');
    var data = form.serialize();
    var method = form.attr('method');

    var isrefresh=$(this).val();
    if(isrefresh=='reset'){data=[];}

    var refreshdiv=$('body').find('.refresh-div');
    $.ajax({
        url: route + '?ajax=true',
        dataType: 'json',
        type: method,
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function() {
           AjaxbeforeSend();
      },
      success: function(response) {

          if(response.status=='success')
          {
             $('#MessageDetails').modal('hide');

              refreshdiv.html(response.html);

              window.history.pushState('page',"After I Go",response.routetoredirect);
              
            
          }
      },
      error: function(xhr) { // if error occured
          alert("Error occured.please try again");
      },
      complete: function() {
         $('.select2').select2();
         Ajaxcomplete();
      },
});
});


  $(document).on('click','.TakeActionsMessage', function(e) 
  {
      e.preventDefault();
      e.stopImmediatePropagation();
      var obj =$(this);
        form = obj.closest('form');
        var route = form.attr('action');
        var data = form.serialize();
        var method = form.attr('method');

        var isrefresh=$(this).val();
        if(isrefresh=='reset'){data=[];}

        var refreshdiv=$('body').find('.refresh-div');
        $.ajax({
            url: route + '?ajax=true',
            dataType: 'json',
            type: method,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
               AjaxbeforeSend();
          },
          success: function(response) {

              if(response.status=='success')
              {
                 $("#MessageDetailsBody").html(response.html);
                
              }
          },
          error: function(xhr) { // if error occured
              alert("Error occured.please try again");
          },
          complete: function() {
             $('.select2').select2();
             Ajaxcomplete();
          },
    });

  });

  jQuery(function($) {

    $('#MessageDetails').on('hidden.bs.modal', function(e) {

      e.stopImmediatePropagation();
      var refreshdiv=$('body').find('.refresh-div');
      var route="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype={{$messagetype}}&ajax=true"  
        jQuery.ajax({
            url: route,
            method: 'GET',
            beforeSend: function() {
                AjaxbeforeSend();
            },
            success: function(response) {

              if(response.event=='refresh')
                {
                  refreshdiv.html(response.html);
                  
                }
            },
            error: function(xhr) { // if error occured
                alert("Error occured.please try again");
            },
            complete: function() {
                Ajaxcomplete();
            },
        });


        });

  });


  $('#heirloom-package-popup').on('hidden.bs.modal', function(e) {
      e.stopImmediatePropagation();
      $('#MessageDetails').animate({ opacity: 100 }, 100);
    });

  $('#qr-package-popup').on('hidden.bs.modal', function(e) {
      e.stopImmediatePropagation();
      $('#MessageDetails').animate({ opacity: 100 }, 100);
    });


  

  function AudioMessagePlayer(tags,token,auth) { 
        $("#popupplayer").html('<ziggeoaudioplayer ziggeo-responsive style="width:100%;  height:100%; margin:0 auto;"  server-auth="'+auth+'" ziggeo-audio="'+token+'" ziggeo-tags="'+tags+'" ziggeo-theme="modern" ziggeo-volume = "1" ziggeo-themecolor="red"     ziggeo-visualeffectvisible="true" ziggeo-visualeffectheight="120" ziggeo-visualeffectminheight="120"  ziggeo-visualeffecttheme="red-bars"></ziggeoplayer>')
        $('#MessagePlayer').modal('toggle');
    }

     function VideoMessagePlayer(tags,token,auth) { 
        $("#popupplayer").html('<ziggeoplayer  ziggeo-tags="'+tags+'" server-auth="'+auth+'" ziggeo-video="'+token+'" ziggeo-theme="modern" ziggeo-volume = "1" ziggeo-themecolor="red"></ziggeoplayer>')
        $('#MessagePlayer').modal('toggle');
    }




</script>
@endsection