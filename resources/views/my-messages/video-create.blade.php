@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Create Video')

@section('headercommon')
<x-main.header icon="video" title="Create a video message" subtitle="Film a video message by clicking on the film now button below. After you’ve finished filming, you will automatically be able to choose your recipient and preferences for your message" />
@endsection
@section('content')
@if(isset($ziggeowidgetcreation) && !empty($ziggeowidgetcreation))
<script>
  var ziggeoApp = new ZiggeoApi.V2.Application({
    token:"<?php echo env('ZIGGEOTOKEN');?>",
    webrtc_streaming_if_necessary: true,
    webrtc_on_mobile: true,
    auth:true
  });

  
</script>
<style type="text/css">
  .ba-popup-helper-overlay
  {
    background-color: #fff;
    border:none;
  }
  .ba-videorecorder-chooser-container
  {
    background-color: #f2f3f4;
  }
  
</style>
@endif
<div class="row">
  
        @if(isset($ziggeowidgetcreation) && !empty($ziggeowidgetcreation) && $hasminutesseconds>0)
  <div class="card p-0">
        <div class="card-body">
        <div class="row">
        <div class="col-12 mt-3 mt-sm-0 border-1 border-secondary border-radius-md py-3 text-center">
        {!! $ziggeowidgetcreation !!}
        </div>
        </div>

        </div>
        </div>
          @else
          <script type="text/javascript">
            OpenSubscriptionWarning();
            function OpenSubscriptionWarning()
            {
              swal({
                  text: "Hi! you Have exhausted your minutes limit",
                  icon: "warning",
                  buttons: {
                      confirm: {
                          text: 'Buy More Minutes',
                          className: ' text-white bg-gradient-primary'
                      },
                      cancel: "Cancel"
                  },
                  }).then((will) => {

                      if (will)
                      {
                          window.location="{{route('my-account.free-up-minutes')}}";
                      }
                      else 
                      {
                          window.location="{{route('my-content')}}";
                      }

                  });
            }
          </script>
          @endif
        
</div>
  @if(isset($ziggeowidgetcreation) && !empty($ziggeowidgetcreation)) 
  <script type="text/javascript">

    $(document).ready(function() {

      var element = $(document).find('#ziggeorecorderID');
      var embedding = ZiggeoApi.V2.Recorder.findByElement(element);



      embedding.on("recording", function () {
        
      });


      embedding.on("uploading", function () {
        console.log('uploading');
        
      });

      embedding.on("attached", function () {
       console.log('attached');
     });

      embedding.on("access_forbidden", function (error_type, error_code) {
        console.log('access_forbidden');
      });

      embedding.on("error", function (error_type, error_code) {
        console.log('error');
      });

      embedding.on("no_microphone", function () {
        console.log('no_microphone');
      });

      embedding.on("upload_selected", function (file) {
        console.log('upload_selected');
        
      });

      embedding.on("verified", function () {
       
        
      });

      embedding.on("uploaded", function () {
        console.log('uploaded');
        
      });


      embedding.on("processing", function (percentage) {
        console.log('processing');  
        
        console.log(percentage);
      });

      embedding.on("processed", function () {
        
        

      });

      embedding.on("ready_to_play", function () {
       
       var obj;
       LoadURL(obj,'{{route("my-messages.index")}}?type={{$contenttype}}&messagetype=video&token='+embedding.get('video'));
    
    });

    

    function VideoInsertUser(token) 
    {
        var videoname=$('#videoname').val();
        $.ajax({
        url: "{{route('my-messages.process-created-video')}}"+ '?ajax=true',
        dataType: 'json',
        type: 'POST',
        data: {
          token:token,
          videoname:videoname,
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
          beforeSend: function() {
            AjaxbeforeSend();
        },
        success: function(response) {

         
        },
        error: function(xhr) { // if error occured
            alert("Error occured.please try again");
        },
        complete: function() {
            Ajaxcomplete();
        },
          
        });

    }


    function LoadURL(obj,route)
    {

        swal({

        title: "Well done!",  
        text: "Video Message Created Successfully!",
        icon: "success",
        buttons: {
            confirm: {
                text: 'Preview Video File',
                className: 'text-white bg-primary'
            },
            cancel:'Choose Preferences'
        },
        }).then((will) => {

            if (will)
            {

             
                
            }
            else 
            {
                route=removeParam('ajax',route);
                route=removeParam('_token',route);
                window.history.pushState('page',"After I Go",route);
                var refreshdiv=$('body').find('.refresh-div');

                if (route.indexOf('?')== -1) {
                    route = route + '?ajax=true';
                }
                else {
                    route = route + '&ajax=true';
                }

                jQuery.ajax({
                    url: route,
                    method: 'GET',
                    dataType: "json",
                    beforeSend: function() {
                        AjaxbeforeSend();
                    },
                  success: function(response) {

                       if(response.event=='refresh')
                        {
                          refreshdiv.html(response.html);
                         
                        }
                    },
                    error: function(xhr) { // if error occured
                        alert("Error occured.please try again");
                    },
                    complete: function() {
                        Ajaxcomplete();
                    },
                });
            }

        });


        
    }


    });

  </script>
  @endif
  @endsection