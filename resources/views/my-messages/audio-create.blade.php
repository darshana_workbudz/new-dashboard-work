@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Create Audio')

@section('headercommon')
<x-main.header icon="mic_on" title="Create an audio message" subtitle="Record an audio message by clicking on the record now button below. After you’ve finished recording, you will automatically be able to choose your recipient and preferences for your message." />
@endsection
@section('content')
@if(isset($ziggeowidgetcreation) && !empty($ziggeowidgetcreation))
<script>
  var ziggeoApp = new ZiggeoApi.V2.Application({
    token:"<?php echo env('ZIGGEOTOKEN');?>",
    webrtc_streaming_if_necessary: true,
    webrtc_on_mobile: true,
    auth:true
  });
</script>
<style type="text/css">
  .ba-videorecorder-container{
    width: 100% !important;
  }
  .ba-videorecorder-container ~ div{
     width: 100% !important;
  }
  .ba-audioplayer-container{
     width: 100% !important;
  }

  .ba-videorecorder-chooser-container {
    background-color: #f2f3f4;
  }
</style>
@endif
<div class="row">
  
        @if(isset($ziggeowidgetcreation) && !empty($ziggeowidgetcreation) && $hasminutesseconds>0)
        <div class="card">
        <div class="card-body">
        <div class="row">
        <div class="col-12 mt-3 mt-sm-0 border-1 border-secondary border-radius-md py-3 text-center">
        {!! $ziggeowidgetcreation !!}
        </div>
        </div>

        </div>
        </div>
          @else
          <script type="text/javascript">
            OpenSubscriptionWarning();
            function OpenSubscriptionWarning()
            {
              swal({
                  text: "Hi! you Have exhausted your minutes limit",
                  icon: "warning",
                  buttons: {
                      confirm: {
                          text: 'Buy More Minutes',
                          className: ' text-white bg-gradient-primary'
                      },
                      cancel: "Cancel"
                  },
                  }).then((will) => {

                      if (will)
                      {
                          window.location="{{route('my-account.free-up-minutes')}}";
                      }
                      else 
                      {
                          window.location="{{route('my-content')}}";
                      }

                  });
            }
          </script>
          @endif
        
</div>
@if(isset($ziggeowidgetcreation) && !empty($ziggeowidgetcreation)) 
<script type="text/javascript">

  ziggeoApp.on("ready", function (status_code) {
      
  });

  $(document).ready(function() {

    var element = $(document).find('#ziggeorecorderID');
    var embedding = ZiggeoApi.V2.AudioRecorder.findByElement(element);

    var processed=0;

    embedding.on("recording", function () {
     
    });

    embedding.on("recording_stopped", function (embedding) {
    
    });


    embedding.on("uploading", function () {
      lock = true;
    });

    embedding.on("uploaded", function (uploaded) {
      lock = false;
    });

    embedding.on("attached", function () {
    
    });

    embedding.on("access_forbidden", function (error_type, error_code) {
   
    });

    embedding.on("error", function (error_type, error_code) {
   
    });

    embedding.on("no_microphone", function () {
   
    });

    embedding.on("upload_selected", function (file) {
   
   
    });

    embedding.on("verified", function () {
   
    });

    embedding.on("uploaded", function () {
   
   
    });


    embedding.on("processing", function (percentage) {
   
   
   
    });

    embedding.on("processed", function () {
    
     
    
    });

    

    embedding.on("ready_to_play", function () {

      
        AudioInsertUser(embedding.get('audio'));
     
       
       
       
    
    });

      function AudioInsertUser(token) 
      {
        var audioname=$('#audioname').val();
        $.ajax({
        url: "{{route('my-messages.process-created-audio')}}"+ '?ajax=true',
        dataType: 'json',
        type: 'POST',
        data: {
          token:token,
          audioname:audioname,
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
          beforeSend: function() {
            
        },
        success: function(response) {

          if(response.status=='success')
           {
            var obj;
            LoadURL(obj,'{{route("my-messages.index")}}?type={{$contenttype}}&messagetype=audio&token='+token);
           }
         
        },
        error: function(xhr) { // if error occured
            alert("Error occured.please try again");
        },
        complete: function() {
            
        },
          
        });

      }

});

function LoadURL(obj,route)
{

        swal({

        title: "Well done!",  
        text: "Audio Message Created Successfully!",
        icon: "success",
        buttons: {
            confirm: {
                text: 'Preview Audio File',
                className: 'text-white bg-primary'
            },
            cancel:'Choose Preferences'
        },
        }).then((will) => {

            if (will)
            {

             
                
            }
            else 
            {
                route=removeParam('ajax',route);
                route=removeParam('_token',route);
                window.history.pushState('page',"After I Go",route);
                var refreshdiv=$('body').find('.refresh-div');

                if (route.indexOf('?')== -1) {
                    route = route + '?ajax=true';
                }
                else {
                    route = route + '&ajax=true';
                }

                jQuery.ajax({
                    url: route,
                    method: 'GET',
                    dataType: "json",
                    beforeSend: function() {
                        AjaxbeforeSend();
                    },
                  success: function(response) {

                       if(response.event=='refresh')
                        {
                          refreshdiv.html(response.html);
                         
                        }
                    },
                    error: function(xhr) { // if error occured
                        alert("Error occured.please try again");
                    },
                    complete: function() {
                        Ajaxcomplete();
                    },
                });
            }

        });


        
}

</script>
@endif
@endsection