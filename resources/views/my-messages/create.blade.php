@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Create Messages')

@section('headercommon')
<x-main.header icon="video" title="What kind of message would you like to create today?" subtitle="To get started, choose the type of content you’d like to create" />
@endsection
@section('content')

<div class="container-fluid py-4">
    <section class="px-3 py-2">

      <div class="row mt-4">

        <div class="col-lg-4 col-md-6 d-flex flex-column mt-3 border-radius-lg ">
          <div class="card">
            <div class="card-body">
              <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                <g id="Group_1831" data-name="Group 1831" transform="translate(0 0.308)">
                  <rect id="Rectangle_538" data-name="Rectangle 538" width="54.083" height="55" rx="16" transform="translate(0 -0.308)" fill="#f3eef6"/>
                  <path id="Path_3771" data-name="Path 3771" d="M18.616,18.446A3.857,3.857,0,0,1,14.759,22.3h-8.9A3.857,3.857,0,0,1,2,18.446V8.357A3.857,3.857,0,0,1,5.857,4.5h8.9a3.857,3.857,0,0,1,3.857,3.857ZM25.455,6.153a1.187,1.187,0,0,1,.282.769V19.881a1.187,1.187,0,0,1-1.956.9L19.8,17.4v-8l3.978-3.381a1.187,1.187,0,0,1,1.673.135Z" transform="translate(13.173 13.79)" fill="#662d91"/>
                </g>
              </svg>
              <h5 class="card-title pt-3">Create video content</h5>
              <hr/>
              <p class="card-text">Record or upload a video with your mobile, tablet, or desktop device.</p>
              <button class="btn btn-icon btn-3 btn-primary" type="button" >
                <span class="btn-inner--icon">
              <svg width="14" height="14" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path d="M16 16.25a3.25 3.25 0 0 1-3.25 3.25h-7.5A3.25 3.25 0 0 1 2 16.25v-8.5A3.25 3.25 0 0 1 5.25 4.5h7.5A3.25 3.25 0 0 1 16 7.75v8.5Zm5.762-10.357a1 1 0 0 1 .238.648v10.918a1 1 0 0 1-1.648.762L17 15.37V8.628l3.352-2.849a1 1 0 0 1 1.41.114Z" fill="#ffffff"></path>
              </svg>
                </span>
                <a data-route="{{route('my-messages.video-create')}}?type={{$contenttype}}" class="link-handle btn-inner--text ps-1 text-white">Record a video</a>
                <!-- <span class="btn-inner--text ps-1" >Record a video</span> -->
              </button>
              <button class="btn btn-icon btn-3 btn-primary" type="button">
              	<span class="btn-inner--icon">
              <svg width="14" height="14" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path d="M5.25 3.495h13.498a.75.75 0 0 0 .101-1.493l-.101-.007H5.25a.75.75 0 0 0-.102 1.493l.102.007Zm6.633 18.498L12 22a1 1 0 0 0 .993-.884L13 21V8.41l3.294 3.292a1 1 0 0 0 1.32.083l.094-.083a1 1 0 0 0 .083-1.32l-.083-.094-4.997-4.997a1 1 0 0 0-1.32-.083l-.094.083-5.004 4.996a1 1 0 0 0 1.32 1.499l.094-.083L11 8.415V21a1 1 0 0 0 .883.993Z" fill="#ffffff"></path>
              </svg>
                </span>
                <!-- <span class="btn-inner--text ps-1">Upload</span> -->
                <a data-route="{{route('my-messages.video-upload')}}?type={{$contenttype}}" class="link-handle btn-inner--text ps-1 text-white">Upload</a>
              </button>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 d-flex flex-column mt-3 border-radius-lg ">
          <div class="card">
            <div class="card-body">
              <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                <g id="Group_1869" data-name="Group 1869" transform="translate(-725 -328)">
                  <rect id="Rectangle_658" data-name="Rectangle 658" width="54.083" height="55" rx="16" transform="translate(725 328)" fill="#f3eef6"/>
                  <path id="Path_3924" data-name="Path 3924" d="M23.863,14.813a1.068,1.068,0,0,1,1.058.922l.01.145v.712a9.61,9.61,0,0,1-8.9,9.584V29.4a1.068,1.068,0,0,1-2.125.145L13.9,29.4V26.176a9.609,9.609,0,0,1-8.892-9.248L5,16.592V15.88a1.068,1.068,0,0,1,2.125-.145l.01.145v.712A7.474,7.474,0,0,0,14.3,24.061l.307.006h.712A7.474,7.474,0,0,0,22.79,16.9l.006-.307V15.88A1.068,1.068,0,0,1,23.863,14.813ZM14.965,2A5.7,5.7,0,0,1,20.66,7.695v8.542a5.695,5.695,0,1,1-11.389,0V7.695A5.7,5.7,0,0,1,14.965,2Z" transform="translate(737 339)" fill="#662d91"/>
                </g>
              </svg>
              <h5 class="card-title pt-3">Create audio content</h5>
              <hr/>
              <p class="card-text">Record or upload audio with your mobile, tablet, or desktop device.</p>
              <button class="btn btn-icon btn-3 btn-primary" type="button" >
                <span class="btn-inner--icon">
                  <svg width="14" height="14" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M18.25 11a.75.75 0 0 1 .743.648l.007.102v.5a6.75 6.75 0 0 1-6.249 6.732l-.001 2.268a.75.75 0 0 1-1.493.102l-.007-.102v-2.268a6.75 6.75 0 0 1-6.246-6.496L5 12.25v-.5a.75.75 0 0 1 1.493-.102l.007.102v.5a5.25 5.25 0 0 0 5.034 5.246l.216.004h.5a5.25 5.25 0 0 0 5.246-5.034l.004-.216v-.5a.75.75 0 0 1 .75-.75ZM12 2a4 4 0 0 1 4 4v6a4 4 0 0 1-8 0V6a4 4 0 0 1 4-4Z" fill="#ffffff"/></svg>
                </span>
                <!-- <span class="btn-inner--text ps-1">Record audio</span> -->
                <a data-route="{{route('my-messages.audio-create')}}?type={{$contenttype}}" class="link-handle btn-inner--text ps-1 text-white">Record audio</a>

              </button>
              <button class="btn btn-icon btn-3 btn-primary" type="button" data-route="{{route('my-messages.audio-upload')}}?type={{$contenttype}}" class="link-handle">
              	<span class="btn-inner--icon">
                  <svg width="14" height="14" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M5.25 3.495h13.498a.75.75 0 0 0 .101-1.493l-.101-.007H5.25a.75.75 0 0 0-.102 1.493l.102.007Zm6.633 18.498L12 22a1 1 0 0 0 .993-.884L13 21V8.41l3.294 3.292a1 1 0 0 0 1.32.083l.094-.083a1 1 0 0 0 .083-1.32l-.083-.094-4.997-4.997a1 1 0 0 0-1.32-.083l-.094.083-5.004 4.996a1 1 0 0 0 1.32 1.499l.094-.083L11 8.415V21a1 1 0 0 0 .883.993Z" fill="#ffffff"/></svg>
                </span>
                <!-- <span class="btn-inner--text ps-1">Upload</span> -->
                <a data-route="{{route('my-messages.audio-upload')}}?type={{$contenttype}}" class="link-handle btn-inner--text ps-1 text-white">Upload</a>

              </button>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 d-flex flex-column mt-3 border-radius-lg ">
          <div class="card">
            <div class="card-body">
              <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                <g id="Group_1870" data-name="Group 1870" transform="translate(-725 -268)">
                  <rect id="Rectangle_659" data-name="Rectangle 659" width="54.083" height="55" rx="16" transform="translate(725 268)" fill="#f3eef6"/>
                  <path id="Path_3925" data-name="Path 3925" d="M22.106,2.007a2.963,2.963,0,0,1,2.956,2.76l.007.2V25.378a2.963,2.963,0,0,1-2.76,2.956l-.2.007H6.963a2.963,2.963,0,0,1-2.956-2.76L4,25.378V4.97A2.963,2.963,0,0,1,6.76,2.014l.2-.007ZM8.938,8.582a.988.988,0,0,0,0,1.975H20.131a.988.988,0,1,0,0-1.975ZM7.95,14.837a.988.988,0,0,0,.988.988H20.131a.988.988,0,1,0,0-1.975H8.938A.988.988,0,0,0,7.95,14.837Zm.988,4.28a.988.988,0,0,0,0,1.975H20.131a.988.988,0,0,0,0-1.975Z" transform="translate(738 279.993)" fill="#662d91"/>
                </g>
              </svg>
              <h5 class="card-title pt-3">Create written content</h5>
              <hr/>
              <p class="card-text">Create a written message or simply upload one you've already written.</p>
              <button class="btn btn-icon btn-3 btn-primary" type="button" data-route="{{route('my-messages.text-create')}}?type={{$contenttype}}" class="link-handle">
                <span class="btn-inner--icon">
                  <svg width="14" height="14" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M19.745 5a2.25 2.25 0 0 1 2.25 2.25v9.505a2.25 2.25 0 0 1-2.25 2.25H4.25A2.25 2.25 0 0 1 2 16.755V7.25A2.25 2.25 0 0 1 4.25 5h15.495Zm-2.495 9.5H6.75l-.102.007a.75.75 0 0 0 0 1.486L6.75 16h10.5l.102-.007a.75.75 0 0 0 0-1.486l-.102-.007ZM16.5 11a1 1 0 1 0 0 2 1 1 0 0 0 0-2Zm-2.995 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2Zm-3 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2Zm-3 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2ZM6 8a1 1 0 1 0 0 2 1 1 0 0 0 0-2Zm2.995 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2Zm3 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2Zm3 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2Zm3 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2Z" fill="#ffffff"/></svg>
                </span>
                <!-- <span class="btn-inner--text ps-1">Write content</span> -->
                <a data-route="{{route('my-messages.text-create')}}?type={{$contenttype}}" class="link-handle btn-inner--text ps-1 text-white">Write content</a>

              </button>
              <button class="btn btn-icon btn-3 btn-primary" type="button"  class="link-handle">
              	<span class="btn-inner--icon">
                  <svg width="14" height="14" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M5.25 3.495h13.498a.75.75 0 0 0 .101-1.493l-.101-.007H5.25a.75.75 0 0 0-.102 1.493l.102.007Zm6.633 18.498L12 22a1 1 0 0 0 .993-.884L13 21V8.41l3.294 3.292a1 1 0 0 0 1.32.083l.094-.083a1 1 0 0 0 .083-1.32l-.083-.094-4.997-4.997a1 1 0 0 0-1.32-.083l-.094.083-5.004 4.996a1 1 0 0 0 1.32 1.499l.094-.083L11 8.415V21a1 1 0 0 0 .883.993Z" fill="#ffffff"/></svg>
                </span>
                <!-- <span class="btn-inner--text ps-1">Upload</span> -->
                <a data-route="{{route('my-messages.text-upload')}}?type={{$contenttype}}" class="link-handle btn-inner--text ps-1 text-white">Upload</a>

              </button>
            </div>
          </div>
        </div>

      </div>
    </section>
  </div>







<!-- <section class="px-3 py-2">
       
    <div class="row mt-4">
       
      <div class="col-lg-4 d-none d-md-block"><p class="h6  text-center"> <i class="text-primary fa fa-video-camera fa-lg" aria-hidden="true"></i> &nbsp; Create a Video Message </p></div>
      <div class="col-lg-4 d-none d-md-block"><p class="h6  text-center"> <i class="text-primary fa fa-microphone fa-lg" aria-hidden="true"></i> &nbsp; Create an Audio Message </p></div>
      <div class="col-lg-4 d-none d-md-block"><p class="h6  text-center"> <i class="text-primary fa fa-pencil-square fa-lg" aria-hidden="true"></i> &nbsp; Create a Written Message </p></div>

      <div class="col-lg-4 col-md-6 d-flex flex-column mt-1 border-radius-lg ">
         <div class="col-lg-12 col-md-12 d-flex flex-column mt-3 border-radius-lg shadow-blur">
         <a data-route="{{route('my-messages.video-create')}}?type={{$contenttype}}" class="link-handle">
        <div class="card h-100 card-background align-items-start">
          <div class="full-background bg-gradient-primary-new"></div>
          <div class="card-body z-index-3">
            <i class="fa fa-film fa-lg"></i>

          </div>
          <div class="card-footer pb-3 pt-2 z-index-3">

            <h6 class="text-white mb-1">Film a message now</h6>
            <p class="text-white text-xs font-weight-bolder mx-1 opacity-8">Video Message</p>
            
          </div>
          <span class="mask bg-gradient-info border-radius-xl z-index-2 opacity-6"></span>
        </div>
        </a>
        
      </div>

      <div class="col-lg-12 col-md-12 d-flex flex-column mt-3 border-radius-lg shadow-blur">

        <a data-route="{{route('my-messages.video-upload')}}?type={{$contenttype}}" class="link-handle">
        <div class="card h-100 card-background align-items-start">
          <div class="full-background bg-gradient-primary-new"></div>
          <div class="card-body z-index-3">
            <i class="fa fa-file-video-o fa-lg"></i>

          </div>
          <div class="card-footer pb-3 pt-2 z-index-3">

            <h6 class="text-white mb-1">Upload a video message</h6>
            <p class="text-white text-xs font-weight-bolder mx-1 opacity-8">Video Message</p>
            
          </div>
          <span class="mask bg-gradient-info border-radius-xl z-index-2 opacity-6"></span>
        </div>
        </a>
        
      </div>
        
      </div>

     <div class="col-lg-4 col-md-6 d-flex flex-column mt-1 border-radius-lg ">
       <div class="col-lg-12 col-md-12 d-flex flex-column mt-3 border-radius-lg shadow-blur">
       <a data-route="{{route('my-messages.audio-create')}}?type={{$contenttype}}" class="link-handle">
        <div class="card h-100 card-background align-items-start">
          <div class="full-background bg-gradient-primary-new"></div>
          <div class="card-body z-index-3">
           <i class="fa fa-microphone fa-lg"></i>

          </div>
          <div class="card-footer pb-3 pt-2 z-index-3">

            <h6 class="text-white mb-1">Record a message now</h6>
            <p class="text-white text-xs font-weight-bolder mx-1 opacity-8">Audio Message</p>
            
          </div>
          <span class="mask bg-gradient-primary border-radius-xl z-index-2 opacity-6"></span>
        </div>
      </a>
        
      </div>
      <div class="col-lg-12 col-md-12 d-flex flex-column mt-3 border-radius-lg shadow-blur">
       <a data-route="{{route('my-messages.audio-upload')}}?type={{$contenttype}}" class="link-handle">
        <div class="card h-100 card-background align-items-start">
          <div class="full-background bg-gradient-primary-new"></div>
          <div class="card-body z-index-3">
           <i class="fa fa-file-audio-o fa-lg"></i>

          </div>
          <div class="card-footer pb-3 pt-2 z-index-3">

            <h6 class="text-white mb-1">Upload an audio message</h6>
            <p class="text-white text-xs font-weight-bolder mx-1 opacity-8">Audio Message</p>
            
          </div>
          <span class="mask bg-gradient-primary border-radius-xl z-index-2 opacity-6"></span>
        </div>
      </a>
        
      </div>
        
      </div>
      <div class="col-lg-4 col-md-6 d-flex flex-column mt-1 border-radius-lg ">
        <div class="col-lg-12 col-md-12 d-flex flex-column mt-3 border-radius-lg shadow-blur">
        <a data-route="{{route('my-messages.text-create')}}?type={{$contenttype}}" class="link-handle">
        <div class="card h-100 card-background align-items-start">
          <div class="full-background bg-gradient-primary-new"></div>
          <div class="card-body z-index-3">
            <i class="fa fa-pencil fa-lg" aria-hidden="true"></i>

          </div>
          <div class="card-footer pb-3 pt-2 z-index-3">

            <h6 class="text-white mb-1">Write a message now</h6>
            <p class="text-white text-xs font-weight-bolder mx-1 opacity-8">Written Message</p>
            
          </div>
          <span class="mask bg-gradient-primary border-radius-xl z-index-2 opacity-6"></span>
        </div>
      </a>
        
      </div>
      <div class="col-lg-12 col-md-12 d-flex flex-column mt-3 border-radius-lg shadow-blur">
        <a data-route="{{route('my-messages.text-upload')}}?type={{$contenttype}}" class="link-handle">
        <div class="card h-100 card-background align-items-start">
          <div class="full-background bg-gradient-primary-new"></div>
          <div class="card-body z-index-3">
            <i class="fa fa-file-pdf-o fa-lg" aria-hidden="true"></i>

          </div>
          <div class="card-footer pb-3 pt-2 z-index-3">

            <h6 class="text-white mb-1">Upload a written message</h6>
            <p class="text-white text-xs font-weight-bolder mx-1 opacity-8">Written Message</p>
            
          </div>
          <span class="mask bg-gradient-primary border-radius-xl z-index-2 opacity-6"></span>
        </div>
      </a>
        
      </div>
        
      </div>


   </div>
</section> -->


@endsection