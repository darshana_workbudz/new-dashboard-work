@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','View Text')

@section('headercommon')
<x-main.header icon="document_one_page_2" title="View Written Messages" subtitle="View your written message content" />
@endsection

@section('content')
<div class="row">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <label class="h6 mt-4"> <i class="fi fi_lock_closed" aria-hidden="true"></i> &nbsp;Written Message</label>
                @if($textdata->capture_type=='written')
                <div class="border-radius-lg p-5 text-dark" style="border:1px solid grey">
                    <?php echo $textdata->descriptioncontenthtml; ?>
                </div>
                @elseif($textdata->capture_type=='uploaded' && $textdata->filetype=='pdf')
                <div id="pdf-viewer"></div>
                @else
                <div id="image-viewer" class="text-center"><img src="{{ route('my-messages.text-stream', $textdata->ziggeo_user_text_id) }}" style="width:50%"></div>
                @endif
            </div>
        </div>
    </div>
</div>
  
  @if($textdata->capture_type=='uploaded' && $textdata->filetype=='pdf')
  <script src="{{asset('assets/js/plugins/pdfobject.min.js')}}"></script>

  <script type="text/javascript">
    
   
  PDFObject.embed("{{ route('my-messages.text-stream', $textdata->ziggeo_user_text_id) }}", "#pdf-viewer");


  </script>
  @endif

  <script type="text/javascript">
    var loadurl="<?php echo isset($loadurl) && !empty($loadurl) ? $loadurl : 0;?>";
    var token="{{$textdata->text_token_id}}";
    

    if(loadurl==1)
    {
      LoadURL('{{route("my-messages.index")}}?type={{$textdata->contenttype}}&messagetype=letter&token='+token);
    }

    function LoadURL(route)
    {

        swal({

        title: "Well done!",  
        text: "Message Created Successfully!",
        icon: "success",
        buttons: {
            confirm: {
                text: 'Preview Message File',
                    className: 'text-white bg-primary'
            },
            cancel:'Choose Preferences'
        },
        }).then((will) => {

            if (will)
            {

             
                
            }
            else 
            {
                route=removeParam('ajax',route);
                route=removeParam('_token',route);
                window.history.pushState('page',"After I Go",route);
                var refreshdiv=$('body').find('.refresh-div');

                if (route.indexOf('?')== -1) {
                    route = route + '?ajax=true';
                }
                else {
                    route = route + '&ajax=true';
                }

                jQuery.ajax({
                    url: route,
                    method: 'GET',
                    dataType: "json",
                    beforeSend: function() {
                        AjaxbeforeSend();
                    },
                  success: function(response) {

                       if(response.event=='refresh')
                        {
                          refreshdiv.html(response.html);
                         
                        }
                    },
                    error: function(xhr) { // if error occured
                        alert("Error occured.please try again");
                    },
                    complete: function() {
                        Ajaxcomplete();
                    },
                });
            }

        });


        
    }
  </script>


@endsection
