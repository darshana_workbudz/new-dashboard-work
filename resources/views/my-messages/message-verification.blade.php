@extends(isset($isajax) && $isajax==true ? 'layouts-frontend.ajax' : 'layouts-frontend.master')
@section('content')
 <script>
    var ziggeoApp = new ZiggeoApi.V2.Application({
      token:"<?php echo env('ZIGGEOTOKEN');?>",
      webrtc_streaming_if_necessary: true,
      webrtc_on_mobile: true,
      auth:true
    });
  </script>
  <header class="header-2">
    <div class="page-header min-vh-50 relative" style="background-image: url('{{asset('assets/img/cover/grandparents-laughing-with-grandchildren.jpg')}}'); background-size:cover;">
      <span class="mask bg-gradient-primary"></span>
      <div class="container">
        <div class="row">
          <div class="col-lg-7 text-center mx-auto">
            <a class="navbar-brand font-weight-bolder link-handle" data-route="{{route('home')}}" rel="tooltip" data-placement="bottom" target="_blank">
                <img src="{{asset('assets/img/evaheld-logo-white.png')}}" style="width:45%" alt="Evaheld logo" />
            </a>
            
          </div>
        </div>
      </div>
      <div class="position-absolute w-100 z-index-1 bottom-0">
        <svg class="waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 40" preserveAspectRatio="none" shape-rendering="auto">
          <defs>
            <path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
          </defs>
          <g class="moving-waves">
            <use xlink:href="#gentle-wave" x="48" y="-1" fill="rgba(255,255,255,0.40"></use>
            <use xlink:href="#gentle-wave" x="48" y="3" fill="rgba(255,255,255,0.35)"></use>
            <use xlink:href="#gentle-wave" x="48" y="5" fill="rgba(255,255,255,0.25)"></use>
            <use xlink:href="#gentle-wave" x="48" y="8" fill="rgba(255,255,255,0.20)"></use>
            <use xlink:href="#gentle-wave" x="48" y="13" fill="rgba(255,255,255,0.15)"></use>
            <use xlink:href="#gentle-wave" x="48" y="16" fill="rgba(255,255,255,0.95"></use>
          </g>
        </svg>
      </div>
    </div>
  </header>
    <div class="container">
     

      <div class="row blur shadow-blur mt-n6 border-radius-md pb-4 p-3 mx-sm-0 mx-1 position-relative z-index-3">
        
        <div class="card-body">
          @if(isset($showmessage) && $showmessage==true)

           <h3 class="text-primary text-gradient text-center font-weight-bolder mt-3 mb-3">Welcome, we’re so glad you’re here! </h3>
           <p class="text-center h6"> Verification Completed Successfully </p>

           @if($type==1)

           <div class="col-12 mt-3 mt-sm-0 border-1 border-secondary border-radius-md py-3 text-center">
           <ziggeoplayer  ziggeo-tags="'+tags+'" server-auth="{{$ziggeoauthtoken}}" ziggeo-video="{{$messagetoken}}" ziggeo-theme="modern" ziggeo-volume = "1" ziggeo-themecolor="red"></ziggeoplayer>
          </div>

           @endif

           @if($type==2)

            <div class="col-12 mt-3 mt-sm-0 border-1 border-secondary border-radius-md py-3 text-center">
             <ziggeoaudioplayer ziggeo-responsive style="width:100%;  height:100%; margin:0 auto;"  server-auth="{{$ziggeoauthtoken}}" ziggeo-audio="{{$messagetoken}}" ziggeo-tags="'+tags+'" ziggeo-theme="modern" ziggeo-volume = "1" ziggeo-themecolor="red"     ziggeo-visualeffectvisible="true" ziggeo-visualeffectheight="120" ziggeo-visualeffectminheight="120"  ziggeo-visualeffecttheme="red-bars"></ziggeoplayer>
          </div>

           @endif

           @if($type==3)

            <div class="col-12 mt-3 mt-sm-0 border-1 border-secondary border-radius-md py-3 text-center">
        <label class="h6 mt-4"> <i class="fi fi_lock_closed" aria-hidden="true"></i> &nbsp;Written Message</label>
              <div class="border-radius-lg p-5 text-dark" style="border:1px solid grey">
              <?php echo $messagedata->descriptioncontenthtml;?>
            </div>
          </div>

           @endif

          @else
          @if(isset($verification_process_step_id) && !empty($verification_process_step_id))
          <form action="{{route('message-verification-post')}}" method="Post">
            @csrf
            <input type="hidden" name="code" value="{{$code}}">
            
            
              <h3 class="text-primary text-gradient text-center font-weight-bolder mt-3 mb-3">Welcome, we’re so glad you’re here! </h3>
              <p class="text-center h6"> Please Complete the verification Process to See the message </p>
              

              @if($verification_process_step_id=='5' || ($verification_process_step_id=='1' && $usertype=='thirdperson'))

                  <div class="row">
                  <div class="col-12 col-sm-6 ">
                  <label> Password  Verification</label>
                  <input class="password form-control @if(isset($error['password'][0]) && !empty($error['password'][0])) is-invalid @endif" type="text" name="password" id="pwd"  value="">
                  @if(isset($error['password'][0]) && !empty($error['password'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['password'][0]}}
                  </p>
                  @endif
                </div>

              
                
                </div>

              @endif


              @if($verification_process_step_id=='6' || ($verification_process_step_id=='2' && $usertype=='thirdperson'))
              <div id="showverificationstep2" class="row  mt-4" >  
               <p class="text-sm text-primary">
               Please Complete the verification Process to to See the message</p>

               <div class="col-12 col-sm-6">
                 <label>Verification Question 1</label>
                 <select disabled name="verification_question_1" class="form-control @if(isset($error['verification_question_1'][0]) && !empty($error['verification_question_1'][0])) {{'is-invalid'}} @endif" style="width: 100%;">
                  @if(isset($verification_questions) && !empty($verification_questions))
                  <option value="">Please Select</option>
                  @foreach($verification_questions as $verification_questionsdata)
                  <option value="{{$verification_questionsdata->verification_question_id}}" 
                    @if($verification_question_1==$verification_questionsdata->verification_question_id){{'selected'}} @endif
                    >{{$verification_questionsdata->description}}</option>
                    @endforeach
                    @endif
                  </select>
                  @if(isset($error['verification_question_1'][0]) && !empty($error['verification_question_1'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['verification_question_1'][0]}}
                  </p>
                  @endif
                </div>

                <div class="col-12 col-sm-6">
                  <label>Answer Question 1</label>
                  <input class="form-control @if(isset($error['verification_answer_1'][0]) && !empty($error['verification_answer_1'][0])) {{'is-invalid'}} @endif" type="text" name="verification_answer_1" 
                 
                  >
                  @if(isset($error['verification_answer_1'][0]) && !empty($error['verification_answer_1'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['verification_answer_1'][0]}}
                  </p>
                  @endif
                </div>

                <div class="col-12 col-sm-6">
                 <label>Verification Question 2</label>
                 <select disabled name="verification_question_2" class="form-control @if(isset($error['verification_question_2'][0]) && !empty($error['verification_question_2'][0])) {{'is-invalid'}} @endif" style="width: 100%;" >
                  @if(isset($verification_questions) && !empty($verification_questions))
                  <option value="">Please Select</option>
                  @foreach($verification_questions as $verification_questionsdata2)
                  <option 
                    @if($verification_question_2==$verification_questionsdata2->verification_question_id)
                    {{'selected'}} @endif> {{$verification_questionsdata2->description}}</option>
                    @endforeach
                    @endif
                  </select>
                  @if(isset($error['verification_question_2'][0]) && !empty($error['verification_question_2'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['verification_question_2'][0]}}
                  </p>
                  @endif
                </div>

                <div class="col-12 col-sm-6">
                  <label>Answer Question 2</label>
                  <input class="form-control @if(isset($error['verification_answer_2'][0]) && !empty($error['verification_answer_2'][0])) {{'is-invalid'}} @endif" type="text" name="verification_answer_2" >
                  @if(isset($error['verification_answer_2'][0]) && !empty($error['verification_answer_2'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['verification_answer_2'][0]}}
                  </p>
                  @endif
                </div>

                <div class="col-12 col-sm-6">
                 <label>Verification Question 3</label>
                 <select disabled name="verification_question_3" class="form-control @if(isset($error['verification_question_3'][0]) && !empty($error['verification_question_3'][0])) {{'is-invalid'}} @endif" style="width: 100%;">
                  @if(isset($verification_questions) && !empty($verification_questions))
                  <option value="">Please Select</option>
                  @foreach($verification_questions as $verification_questionsdata3)
                  <option value="{{$verification_questionsdata3->verification_question_id}}"  @if($verification_question_3==$verification_questionsdata3->verification_question_id)
                    {{'selected'}} @endif>{{$verification_questionsdata3->description}}</option>
                    @endforeach
                    @endif
                  </select>
                  @if(isset($error['verification_question_3'][0]) && !empty($error['verification_question_3'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['verification_question_3'][0]}}
                  </p>
                  @endif
                </div>

                <div class="col-12 col-sm-6">
                  <label>Answer Question 3</label>
                  <input class="form-control @if(isset($error['verification_answer_3'][0]) && !empty($error['verification_answer_3'][0])) {{'is-invalid'}} @endif" type="text" name="verification_answer_3"   >
                  @if(isset($error['verification_answer_3'][0]) && !empty($error['verification_answer_3'][0]))
                  <p class="form-text text-danger text-xs mb-1">
                    {{$error['verification_answer_3'][0]}}
                  </p>
                  @endif
                </div>
              </div>
              @endif

              <div class="text-center">
          <button type="submit" class="btn btn-lg bg-purple text-white w-100 mt-5 mb-0">Confirm Verification  </button>
           </div>
         </form>
            @else

            <h6 class="text-primary text-gradient text-center font-weight-bolder mt-3 mb-3">Sorry, You don't have required permission to view the message</h6>

             
            @endif
          @endif
        </div>
      </div>
    </div>


    
  
</script>
  
@endsection
