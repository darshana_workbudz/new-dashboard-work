<div class="card h-100">
    <div class="card-header pb-0">
      <h5 class="text-sm text-primary mb-0">Scan Qr / Download QR</h5>
      <p class="text-xs pt-0 mt-0">
          Manage your QR Code Details
         <hr>
      </p>
    </div>
    <form action="{{route('my-messages.download-message-qr')}}" method="Post">
      @csrf
    <div class="card-body p-3">
    	<div class="row">
    		<div class="col-12" style="background:url('https://qr-codes-png.s3.amazonaws.com/{{$messagedata->qrid}}.png');">
    		  <input type="hidden" name="link" value="https://qr-codes-png.s3.amazonaws.com/{{$messagedata->qrid}}.png">
    		</div>
    </div>
    </div>
    <div class="card-footer  d-flex">
                    <div class="ms-auto">
                    <button type="submit" class="btn bg-primary btn-xs mx-2 " > Download</button>
                    
                    </div>
            </div>
    </div>
