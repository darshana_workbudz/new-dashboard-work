@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Record Video Content')

@section('headercommon')
<x-main.header icon="video" title="Record Video Content" subtitle="Let's start with the basic Video information" />
@endsection

@section('content')
<link rel="stylesheet" href="https://assets.ziggeo.com/v2-stable/ziggeo.css" />
<script src="https://assets.ziggeo.com/v2-stable/ziggeo.js"></script>
<script>
  var ziggeoApp = new ZiggeoApi.V2.Application({
    token:"<?php echo env('ZIGGEOTOKEN');?>",
    webrtc_streaming_if_necessary: true,
    webrtc_on_mobile: true
  });
</script>
<style type="text/css">
  .ba-videorecorder-chooser-button-1 {
    background-color: #eb6a74 !important;
    color: #FFF !important;
    margin-top: 20px;
    padding: 12px;
    border-radius: 4px;;
  }
</style>
<div class="row">
  <div class="card">
    <div class="card-body">

      <div class="row mt-3">

       

      </div>
   </div>
 </div>
</div>

@endsection