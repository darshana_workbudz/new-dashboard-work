@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Upload a written message')

@section('headercommon')
<x-main.header icon="document_one_page" title="Upload a written message" subtitle="Upload a written message by clicking on the upload button or dropping your file into the box below. After it's finished uploading, you will automatically be able to choose your recipient and preferences for your message." />
@endsection
@section('content')
<style type="text/css">
.ql-container {
  min-height: 10rem;
  height: 100%;
  flex: 1;
  display: flex;
  flex-direction: column;
}

.ql-editor {
  height: 100%;
  flex: 1;
  overflow-y: auto;
  width: 100%;
}
.ql-editor>p {
  color:black !important;
}
.custom-style-input {
  width: 100%;
  border:0;
  border-radius:0px;
  border-bottom:1px solid black;
  padding: 15px;
  font-size: 30px;
}
#descriptionbox
{
  height: 500px !important;
}

#counter {
  border: 1px solid #ccc;
  border-width: 0px 1px 1px 1px;
  color: #aaa;
  padding: 5px 15px;
  text-align: right;
}


</style>

<div class="row">
  <div class="card">
   <div class="card-body">

  <div class="row">
    
        <form action="{{route('my-messages.upload-files')}}" class="form-control dropzone mt-2" id="dropzone">
           <input type="hidden" name="type" value="{{$contenttype}}">
          @csrf
          <div class="dz-message" data-dz-message><span><strong>Drop files here</strong> OR <strong>Upload files from my device</strong> </span></div>
        </form>
        
        

   </div>
 </div>
</div>
</div>
<script type="text/javascript">
  var gallery="{{route('my-messages.get-uploaded-files',0)}}";
  var delete_url="{{route('my-messages.delete-uploaded-files')}}"
  var routetoredirect="{{route('my-messages.index')}}?type={{$contenttype}}&messagetype=letter"

</script>
<script src="{{asset('assets/js/plugins/quill.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/dropzone.min.js')}}"></script>
<script src="{{asset('assets/js/plugins/dropzone-script.js')}}"></script>
<script type="text/javascript">

  
  Dropzone.discover();




function LoadURL(obj,route,messageid)
{
        var previewroute=BASEURL +'/my-content/text-show/'+messageid;
        swal({

        title: "Well done!",  
        text: "Message Uploaded Successfully",
        icon: "success",
        buttons: {
            confirm: {
                text: 'Preview Message File',
          className: 'text-white bg-primary'
            },
            cancel:'Update Preferences'
        },
        }).then((will) => {

            if (will)
            {

                var refreshdiv=$('body').find('.refresh-div');

                if (previewroute.indexOf('?')== -1) {
                    previewroute = previewroute + '?ajax=true';
                }
                else {
                    previewroute = previewroute + '&ajax=true';
                }  

               jQuery.ajax({
                    url:previewroute,
                    method: 'GET',
                    dataType: "json",
                    beforeSend: function() {
                        AjaxbeforeSend();
                    },
                  success: function(response) {

                       if(response.event=='refresh')
                        {
                          refreshdiv.html(response.html);
                          previewroute=removeParam('ajax',previewroute);
                          previewroute=removeParam('_token',previewroute);
                          window.history.pushState('page',"After I Go",previewroute);
                        }
                    },
                    error: function(xhr) { // if error occured
                        alert("Error occured.please try again");
                    },
                    complete: function() {
                        Ajaxcomplete();
                    },
                });
                
            }
            else 
            {
                
                
                var refreshdiv=$('body').find('.refresh-div');

                if (route.indexOf('?')== -1) {
                    route = route + '?ajax=true';
                }
                else {
                    route = route + '&ajax=true';
                }

                jQuery.ajax({
                    url: route,
                    method: 'GET',
                    dataType: "json",
                    beforeSend: function() {
                        AjaxbeforeSend();
                    },
                  success: function(response) {

                       if(response.event=='refresh')
                        {
                          refreshdiv.html(response.html);
                          route=removeParam('ajax',route);
                          route=removeParam('_token',route);
                          window.history.pushState('page',"After I Go",route);
                        }
                    },
                    error: function(xhr) { // if error occured
                        alert("Error occured.please try again");
                    },
                    complete: function() {
                        Ajaxcomplete();
                    },
                });
            }

        });


}


</script>
@endsection