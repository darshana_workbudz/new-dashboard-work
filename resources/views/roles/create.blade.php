@extends('layouts.master')
@section('title', 'Create Role | SOS Admin Panel')

@section('styles')
@endsection

@section('breadcrumb-title', 'Create Role')
@section('breadcrumb-items')
<li class="breadcrumb-item">Roles</li>
<li class="breadcrumb-item active">Create Role</li>
@endsection

@section('content')
@include('layouts.alert')
<div class="container-fluid select2-drpdwn">
    <div class="row">
        <div class="col-sm-12 col-xl-12">
            <div class="card">
                <form action="{{ Request::root() }}/roles/store" method="POST">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-12 col-xl-4">
                                <div class="mb-2">
                                    <div class="col-form-label">Enter Role Name <span class="madatory">*</span> </div>
                                    <input type="text" class="form-control NameWithSpace required_field" name="name" id="Role_name" placeholder="Enter Role Name" autocomplete="off" value="{{old('name')}}" />
                                    @if ($errors->has('name'))
                                    <span class="invalid-feedback-dropdown" role="alert">
                                        <label for="teamname">{{ $errors->first('name') }}</label>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="mb-0 m-t-30">
                            <div>
                                @if ($errors->has('permission'))
                                <span class="invalid-feedback-dropdown" role="alert">
                                    <label for="teamname">{{ $errors->first('permission') }}</label>
                                </span>
                                @endif
                                <ul class="treeview">
                                    @foreach($permission as $value)
                                    @if($value->moduleid != 0)
                                    <li>
                                        <input type="checkbox" class="" name="permission[]" value="{{$value->id}}">
                                        <label for="tall" class="custom-unchecked">{{$value->name}}</label>
                                        @foreach($permission as $value1)
                                        @if($value1->master_module_id == $value->moduleid)
                                        <ul>
                                            <li>
                                                <input type="checkbox" class="" name="permission[]" value="{{$value1->id}}">
                                                <label for="tall-2-1" class="custom-unchecked">{{$value1->name}}</label>
                                                <ul>
                                                    @foreach($permission as $value2)
                                                    @if($value2->sub_module_id == $value1->id)
                                                    <li>
                                                        <input type="checkbox" class="" name="permission[]" class="" value="{{$value2->id}}">
                                                        <label for="tall-2-1" class="custom-unchecked">{{$value2->name}}</label>
                                                    </li>
                                                    @endif
                                                    @endforeach
                                                </ul>
                                            </li>
                                        </ul>
                                        @endif
                                        @endforeach
                                    </li>
                                    @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
