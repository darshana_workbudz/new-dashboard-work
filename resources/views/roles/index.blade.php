@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Role')

@section('headercommon')
<x-main.header 
  icon="people" 
  title="All Roles List" 
  subtitle="Manage Permissions of each Role by clicking on edit button"
  button
  buttonText="Reset Filter"
  buttonIcon="reset"
  href="javascript:;"
/>
@endsection

@section('content')
<div class="row mt-2">
  <div class="col-lg-12 col-sm-12">
    <div class="card">
      <div class="card-body px-0 pb-0">
        @if(count($roles)>0)
        <div class="table-responsive">
          <table class="table align-items-center mb-0">
            <thead class="thead-light">
              <tr>
                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-center">Id</th>
                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">ROLE NAME</th>
                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Status</th>
                <th class="text-uppercase text-secondary text-center text-xxs font-weight-bolder opacity-7">ACTION</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($roles as $key => $role)
              <tr>
                <td class="text-sm text-center">
                  {{ $role->id }}
                </td>
                <td class="text-sm">
                  <div class="d-flex px-2">
                    <div class="my-auto">
                      <i class="fi fi_person text-primary p-2"></i>{{ $role->name }}
                    </div>
                  </div>
                </td>
                <td>
                  <span class="badge badge-dot me-4">
                    <i class="bg-success"></i>
                    <span class="text-dark text-xs">Active</span>
                  </span>
                </td>
                <td class="text-sm text-center">
                  <a data-route="{{route('edit-roles',['id'=>$role->id])}}" data-original-title="Edit User" class="link-handle">
                    <span class="icon-size"><i class="fi fi_person-edit text-dark"></i></span>
                  </a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        @else
        <blockquote class="blockquote text-whitep-2">
          <p class="text-primary ms-3">Sorry ! No Roles Found</p>
          <footer class="blockquote-footer text-gradient text-info text-sm ms-3">Roles list</footer>
        </blockquote>
        @endif
        <!-- DivTable.com -->
      </div>
    </div>
  </div>
</div>

@stop
