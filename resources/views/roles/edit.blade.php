@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Role')

@section('headercommon')
<x-main.header icon="person" title="Manage Permission for - User Role" subtitle="Here you can manage the module and permission of selected Module." />
@endsection

@section('content')
<div class="container-fluid select2-drpdwn">
  <div class="row">
    <div class="col-sm-12 col-xl-12">
      <div class="card mt-4" id="accounts">
        <div class="card-header">
          <h5>Manage Permission for - {{$role->name}} Role</h5>
          <p class="text-sm">Here you can manage the module and permission of selected Module.</p>
        </div>
        <form method="POST" action="{{route('edit-roles-post')}}">
          <input type="hidden" name="role_id" value="{{$role->id}}">
          <div class="card-body pt-0">
            @if(isset($Modules) && !empty($Modules))
            @foreach($Modules as $moduledata)
            <div class="d-flex">
              <div class="my-auto ms-3">
                <div class="h-100">
                  <h5 class="mb-0"><i class="ni ni-ui-04 fa-lg text-primary p-2"></i> {{$moduledata->module_name}}
                    <p class="ms-5 mb-0 text-sm">Permissions</p>
                  </h5>
                </div>
              </div>
            </div>
            @if(isset($moduledata->allpermission) && !empty($moduledata->allpermission))
            @foreach($moduledata->allpermission as $key=> $pdata)
            <div class="ps-2 pt-3 ms-2">
              <div class="d-flex align-items-center mb-sm-0 mb-4">
                <div>
                  <div class="form-check form-switch mb-0">
                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault0{{$pdata['id']}}" name="permissions[]" @if(in_array($pdata['id'],$rolePermissions)){{'checked'}} @endif value="{{$pdata['id']}}">
                  </div>
                </div>
                <div class="ms-2">
                  <span class="text-dark font-weight-bold d-block text-sm"> {{$pdata['name']}}</span>
                </div>
              </div>
            </div>
            @endforeach
            @endif
            <hr class="horizontal dark">
            @endforeach
            @endif
            <button class="submit-button btn bg-primary btn-sm float-end">Update Role Permisions</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@stop
