<div class="card h-100">
  <div class="card-header pb-0">
    <h5 class="text-sm text-primary mb-0">Share Promotional Codes</h5>
    <p class="text-xs pt-0 mt-0">
      Promotional Code can be used to discount invoices.
      <hr>
    </p>
  </div>
   <form method="POST" action="{{route('shared-coupons-b2b-clients.store')}}">
    <input type="hidden" name="user_id" value="{{$user_id}}">
    
    <div class="card-body p-3">
  	@if(isset($codes) && count($codes)>0)
  	<div class="row">
  	@foreach($codes as $codedata)
  		<div class="col-md-6 ">
		<div class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row @if(isset($sharedcouponscodes) && in_array($codedata->promotional_code_id,$sharedcouponscodes)) bg-blue-pale @endif">
		<i class="fa fa-tag text-dark " ></i>
		<h6 class="mb-0 px-2 text-sm">{{$codedata->code}}</h6>

		@if(isset($sharedcouponscodes) && in_array($codedata->promotional_code_id,$sharedcouponscodes))
		<i class="fa fa-check-circle-o text-primary p-1" aria-hidden="true"></i>
        <span class="text-sm text-dark"></span>
		@else
		<div class="form-check">
                  <input class="form-check-input" name="promotionalcodes[]" type="checkbox" value="{{$codedata->promotional_code_id}}">
                  <p class="text-sm text-dark" for="responsibility_acknowledge"></p>
                </div>
		
		@endif
		</div>
		<p class="text-xs text-center text-dark">{{$codedata->name}}</p>
		</div>
  	@endforeach
  </div>
  	 @else
  	<p class="text-xs text-center text-dark">Sorry you don't have any Coupons</p>
  	@endif
  </div>

  <div class="card-footer  d-flex">
	<div class="ms-auto">
	    <button type="button" class="submit-button btn bg-primary btn-xs " onclick="HideCouponDetails()">Share Coupons <i class="fa fa-share text-white mx-2" ></i> </button>
	    <button type="button" class="btn bg-danger btn-xs text-white" onclick="HideCouponDetails()" >Cancel</button>
	</div>
	</div>
	</form>
</div>
