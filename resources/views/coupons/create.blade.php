@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Add New Coupon')

@section('headercommon')
<x-main.header icon="people" title="Create a coupon" subtitle="Coupons can be used to discount invoices or entire customer accounts." />
@endsection

@section('content')

<style type="text/css">
  .swal-footer {
    text-align: center;
  }
</style>

<div class="row">
  <div class="card">
    <div class="card-body">
      <h5 class="font-weight-bolder text-sm text-primary"> <a data-route="{{route('manage-coupons.index')}}" class="link-handle"><i class="fi fi_chevron_left text-primary" aria-hidden="true"></i></a> &nbsp; Coupon Details </h5>

      <form method="post" action="{{route('manage-coupons.store')}}">
        @csrf
        <div class="row mt-5" >
          <div class="col-12 col-sm-6 ">
            <label>Coupon Name <span class="text-danger">*</span></label>
            <input class="form-control @if(isset($error['name'][0]) && !empty($error['name'][0])) is-invalid @endif" type="text" name="name" value="{{$old->name}}"  />
            @if(isset($error['name'][0]) && !empty($error['name'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['name'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 mt-4">
            <label >Coupon Type<span class="text-danger">*</span></label>
            <div class="form-check form-check-inline px-5">

                <input checked class="coupontype @if(isset($error['type'][0]) && !empty($error['type'][0])) {{'is-invalid'}} @endif form-check-input" type="radio" name="type" id="customRadio1" value="1" @if($old->type=='1') {{'checked'}}@endif>
                <p class="text-dark" for="customRadio1">Percentage Discount</p>

           </div>

           <div class="form-check form-check-inline ">
           	<input class="coupontype @if(isset($error['type'][0]) && !empty($error['type'][0])) {{'is-invalid'}} @endif form-check-input" type="radio" name="type" id="customRadio2" value="2" @if($old->type=='2') {{'checked'}}@endif>
            <p class="text-dark" for="customRadio2">Fixed Amount Discount</p>
            </div>
            @if(isset($error['type'][0]) && !empty($error['type'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['type'][0]}}
            </p>
            @endif
          </div>

          <div class="col-12 col-sm-6 mt-4" id="percenttype">
            <label>Percentage off %<span class="text-danger">*</span></label>
            <input  class="form-control @if(isset($error['percent_off'][0]) && !empty($error['percent_off'][0])) is-invalid @endif" type="number" name="percent_off" value="{{$old->percent_off}}"  />
            @if(isset($error['percent_off'][0]) && !empty($error['percent_off'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['percent_off'][0]}}
            </p>
            @endif
          </div>

          <div class="col-12 col-sm-6 mt-4" id="discounttype" style="display: none;">
            <label>Amount off <span class="text-danger">*</span></label>
            <input class="form-control @if(isset($error['amount_off'][0]) && !empty($error['amount_off'][0])) is-invalid @endif" type="number" name="amount_off" value="{{$old->amount_off}}"  />
            @if(isset($error['amount_off'][0]) && !empty($error['amount_off'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['amount_off'][0]}}
            </p>
            @endif
          </div>
          
      </div>
 

          
 <div class="col-12 mt-4 ">
  <div class="button-row float-end d-flex mt-4">
    <button class="submit-button btn bg-primary btn-sm mb-0 me-2" type="button" name="button">Save</button>
    <a data-route="{{route('manage-coupons.index')}}" class="link-handle btn btn-outline-dark btn-sm mb-0" type="button" name="button">Back</a>
  </div>
</div>
</form>
</div>
</div>

</div>


<script type="text/javascript">
	$('.coupontype').change(function() {
        var coupontype = $('input[name="type"]:checked').val();
        if (coupontype == 1) {

        	$('#percenttype').show();
        	$('#discounttype').hide();
        }
        if (coupontype == 2) {

        	$('#discounttype').show();
        	$('#percenttype').hide();
        }
    });
    $('.coupontype').trigger('change');
</script>

@stop
