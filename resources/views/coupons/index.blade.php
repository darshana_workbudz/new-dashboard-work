@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My coupons')

@section('headercommon')
<x-main.header 
  icon="people" 
  title="My coupons" 
  subtitle="Add and manage coupons"
  button
  buttonText="Add coupons"
  buttonIcon="add"
  data-route="{{route('manage-coupons.create')}}"
/>
@endsection

@section('content')
<div class="row mt-2">
  @if(count($coupons)>0)
  @foreach ($coupons as $rkey => $coupons)
  <div class="col-lg-4 col-md-6 mb-4">
    <div class="card">
      <div class="card-body p-3">
        <div class="d-flex">
          <!-- Avatar Large -->
          <div class="avatar avatar-xl position-relative border-radius-xl bg-blue-pale d-none d-lg-block d-xl-block cursor-pointer" >
            <div class="card-body z-index-3 text-center p-3">
              <i class="fi fi_tag text-blue" style="font-size: 42px;" aria-hidden="true"></i>
            </div>
          </div>
          <!-- Avatar Small -->
          <div class="avatar avatar-lg position-relative border-radius-xl bg-blue-pale d-block d-lg-none d-xl-none cursor-pointer" >
            <div class="card-body z-index-3 text-center p-2 py-3">
              <i class="fi fi_tag text-blue" style="font-size: 28px" aria-hidden="true"></i>
            </div>
          </div>
          <div class="ms-3 my-auto">
            <div class="d-flex flex-column justify-content-center">
              <h6 class="mb-0 ">{{$coupons->name
                  }}</h6>

                    
                  </div>
                    <div class="avatar-group">
                      
                      <a data-route="{{route('manage-promotional-codes.index',$coupons->coupon_id)}}" class="link-handle"><p>View Promotional Codes</p></a>
                      
                    </div>
                  </div>
                  <div class="ms-auto">
                    <div class="dropdown">
                     
                    </div>
                  </div>
                </div>
               
                <hr class="horizontal dark">
                <div class="row">
                  <div class="col-6 ">
                    
                    @if($coupons->valid==1)
                    
                      <i class="fa fa-check-circle-o text-success mt-1" aria-hidden="true"></i>
                      <span class="text-sm text-dark">Active</span>
                    
                    @else
                    
                      <i class="fa fa-imes-circle-o text-danger" aria-hidden="true"></i>
                      <span class="text-sm text-dark">In Active</span>
                    
                    @endif
                  </div>
                  <div class="col-6 text-end">
                    <h6 class="text-sm mb-0">{{date('d.m.Y',strtotime($coupons->created_at))}}</h6>
                    <p class="text-secondary text-sm font-weight-bold mb-0">Created date</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach

          @else
          <section class="px-3 py-2">
       
       <div class="row mt-4">
       
       
       <div class="col-lg-12 col-md-12 d-flex flex-column mt-3 border-radius-lg text-center">
      
       <div class="card h-100 card-background">
         <div class="full-background"></div>
         <div class="card-body z-index-3 text-center">
         <h6 class="text-dark mb-4 mt-3">No Coupons Found</h6> 
       
         </div>
       
        </div>
       </div>
       </div></section>

          @endif
          
         
        
        </div>
      </section>
@stop
