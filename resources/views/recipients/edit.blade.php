@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Recipients')

@section('headercommon')
<x-main.header icon="person" title="Update Recipient Details" subtitle="To update recipients details fill the below required details click on save button" />
@endsection
@section('content')
<style type="text/css">
  .swal-footer {
    text-align: center;
  }
</style>
<div class="row">
  <div class="card">
    <div class="card-header pb-0">
      <div class="d-lg-flex">
        <div>
          <p class="text-xs mb-0">
           Evaheld relies on you solely to provide the correct details for your recipients. We do not investigate or confirm any of the details provided by you and we take no responsibility for messages that are sent to the wrong recipients due to incorrect details being provided. However, before we send your messages to your recipients, we will ensure they undergo your verification process if one is provided. This means that your preferences here are really important to ensure that your messages are only sent to whom they’re meant to be.
         </p>
       </div>
       
     </div>
   </div>
   <div class="card-body">

    <h5 class="font-weight-bolder text-sm text-primary"><a data-route="{{route('manage-recipients.index')}}" class="link-handle"><i class="fa fa-chevron-left text-primary" aria-hidden="true" ></i></a> &nbsp;Recipient’s contact details</h5>
    <form method="post" action="{{route('manage-recipients.update',$recipientsdata->user_recipient_id)}}">
      @csrf
      <input type="hidden" name="user_id" value="{{$user_id}}">
      <input type="hidden" name="user_recipient_id" value="{{$recipientsdata->user_recipient_id}}">
    <div class="row">
      <div class="row">
        <div class="col-12 col-sm-6 ">
          <label>First Name <span class="text-danger">*</span></label>
          <input class="form-control @if(isset($error['first_name'][0]) && !empty($error['first_name'][0])) is-invalid @endif" type="text"  name="first_name" value="{{$recipientsdata->first_name}}" >
          @if(isset($error['first_name'][0]) && !empty($error['first_name'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['first_name'][0]}}
          </p>
          @endif
        </div>
        <div class="col-12 col-sm-6 ">
          <label>Last Name <span class="text-danger">*</span></label>
          <input class="form-control @if(isset($error['last_name'][0]) && !empty($error['last_name'][0])) is-invalid @endif" type="text" name="last_name" value="{{$recipientsdata->last_name}}" >
          @if(isset($error['last_name'][0]) && !empty($error['last_name'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['last_name'][0]}}
          </p>
          @endif
        </div>
      </div>
      <div class="col-12 col-sm-6 mt-2">
        <label>Email Address <span class="text-danger">*</span></label>
        <input class="form-control @if(isset($error['email'][0]) && !empty($error['email'][0])) is-invalid @endif" type="text" name="email" value="{{$recipientsdata->email}}">
        @if(isset($error['email'][0]) && !empty($error['email'][0]))
        <p class="form-text text-danger text-xs mb-1">
          {{$error['email'][0]}}
        </p>
        @endif
      </div>
      <div class="col-12 col-sm-6 mt-2">
        <div class="row">
          <label>Phone Number </label>
          <div class="col-3 mt-3 mt-sm-0 pr-0 mr-0">
                  <select name="phone_code" class="phonecodeselect2 form-control" style="width: 100%;">
                    @if(isset($phonecodes) && !empty($phonecodes))
                    @foreach($phonecodes as $data)
                    <option value="{{$data->id}}" data-code="{{$data->iso2}}" @if(isset($old->phone_code) && $old->id==$data->id ||  $recipientsdata->phone_code==$data->id){{"selected"}} @elseif($data->iso2=='AU') {{"selected"}} @endif  >+{{$data->phone_code}} {{$data->iso2}} </option>
                    @endforeach
                    @endif
                    
                  </select>
                </div>
          <div class="col-9 mt-2 mt-sm-0 pl-0 ml-0">
            <input id="phone_number" class="required form-control @if(isset($error['phone_number'][0]) && !empty($error['phone_number'][0])) is-invalid @endif" type="number" name="phone_number"  value="{{$recipientsdata->phone_number}}">
            @if(isset($error['phone_number'][0]) && !empty($error['phone_number'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['phone_number'][0]}}
            </p>
            @endif
          </div>
          
        </div>
      </div>
      <div class="col-12 col-sm-6">
        <label>Relation to Me</label>
        <select name="relationship" class="form-control" style="width: 100%;">
          @if(isset($relationship) && !empty($relationship))
          <option value="">Please Select</option>
          @foreach($relationship as $relationshipdata)
          <option value="{{$relationshipdata->relationship_id}}" @if(isset($recipientsdata->relationship_id) && $recipientsdata->relationship_id==$relationshipdata->relationship_id){{"selected"}} @endif  >{{ucfirst($relationshipdata->relationship_name)}}</option>
          @endforeach
          @endif

        </select>
      </div>
      <h5 class="font-weight-bolder text-sm text-primary mt-3"><i class="text-success"></i>Address Information</h5>
              <div class="row">
                <div class="col-12 col-sm-6 field-wrapper address-lookup mt-1">
                <label> Recipient’s Address </label>
                <input class="form-control address-lookup__field @if(isset($error['address_line_1'][0]) && !empty($error['address_line_1'][0])) is-invalid @endif" type="text" name="jls" autocomplete="off"  placeholder="&nbsp;">

                 <a  href="javascript:;" class="mt-1 show-manual text-xs float-end text-secondary" ><u>Manually enter my address</u></a>

                 @if(isset($error['address_line_1'][0]) && !empty($error['address_line_1'][0]))
                    <p class="form-text text-danger text-xs mb-1">
                    Please search your address or enter it manually
                    </p>
                    @endif
                </div>
       

                

              

                </div>
                <div class="row show-manual-div" style="display: none;">
                  <input class="form-control" id="manual-address" name="manual" type="hidden" value="0">
                  <input class="form-control" name="city" type="hidden">
                  <input class="form-control" name="county" type="hidden">

                  <div class="col-12 col-sm-6">
                    <label>Address Line 1  </label>
                    <input class="form-control @if(isset($error['address_line_1'][0]) && !empty($error['address_line_1'][0])) is-invalid @endif" type="text" name="address_line_1"  value="{{$recipientsdata->address_1}}" >
                    @if(isset($error['address_line_1'][0]) && !empty($error['address_line_1'][0]))
                    <p class="form-text text-danger text-xs mb-1">
                    {{$error['address_line_1'][0]}}
                    </p>
                    @endif
                  </div>

                  <div class="col-12 col-sm-6">
                    <label>Address Line 2</label>
                    <input class="form-control @if(isset($error['address_line_2'][0]) && !empty($error['address_line_2'][0])) is-invalid @endif" type="text" name="address_line_2"  value="{{$recipientsdata->address_2}}" >
                    @if(isset($error['address_line_2'][0]) && !empty($error['address_line_2'][0]))
                    <p class="form-text text-danger text-xs mb-1">
                    {{$error['address_line_2'][0]}}
                    </p>
                    @endif
                  </div>

                 <div class="col-12 col-sm-6">
                    <label>Postcode </label>
                    <input class="form-control @if(isset($error['postcode'][0]) && !empty($error['postcode'][0])) is-invalid @endif" name="postcode" type="number"  value="{{$recipientsdata->postcode}}" >
                    @if(isset($error['postcode'][0]) && !empty($error['postcode'][0]))
                    <p class="form-text text-danger text-xs mb-1">
                    {{$error['postcode'][0]}}
                    </p>
                    @endif
                  </div>

                 <div class="col-12 col-sm-6">
                    <label>State </label>
                    <input class="form-control @if(isset($error['state'][0]) && !empty($error['state'][0])) is-invalid @endif" name="state" type="text"  value="{{$recipientsdata->state}}" >
                    @if(isset($error['state'][0]) && !empty($error['state'][0]))
                    <p class="form-text text-danger text-xs mb-1">
                    {{$error['state'][0]}}
                    </p>
                    @endif
                  </div>

                 <div class="col-12 col-sm-6">
                    <label>Country </label>
                    <select name="country" id="country" class="select2 form-control @if(isset($error['country'][0]) && !empty($error['country'][0])) is-invalid @endif" style="width: 100%;">
                    @if(isset($phonecodes) && !empty($phonecodes))
                    <option value=""></option>
                    @foreach($phonecodes as $data)
                    <option value="{{$data->iso2}}"  @if(isset($recipientsdata->country) && $recipientsdata->country==$data->iso2){{"selected"}} @endif  > {{$data->name}} </option>
                    @endforeach
                    @endif
                    
                  </select>
                  </div>
                </div>

    


      <div class="row mt-4" id="showstep1">

       <div class="col-12">

        <div class="col-lg-12 col-sm-12">
          <p class="font-weight-bolder text-sm text-primary">Would you like this recipient to complete a verification process?</p>
        </div>

        <div class="col-lg-12 col-sm-12">
          <div class="form-check form-check-inline">

            <input class="require_verification @if(isset($error['require_verification'][0]) && !empty($error['require_verification'][0])) {{'is-invalid'}} @endif form-check-input" type="radio" name="require_verification" id="customRadio1" value="1"  @if($old->require_verification=='1' || $recipientsdata->require_verification=='1'){{'checked'}}@endif>
            <p class="text-dark" for="customRadio1">Yes</p>
          </div>

          <div class="form-check form-check-inline">

            <input class="require_verification @if(isset($error['require_verification'][0]) && !empty($error['require_verification'][0])) {{'is-invalid'}} @endif form-check-input" type="radio" name="require_verification" id="customRadio2" value="2"  @if($old->require_verification=='2' || $recipientsdata->require_verification=='2'){{'checked'}}@endif>
            <p class="text-dark" for="customRadio2">No</p>
          </div>

          @if(isset($error['require_verification'][0]) && !empty($error['require_verification'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['require_verification'][0]}}
          </p>
          @endif

        </div>

        <div class="col-lg-12 col-sm-12 " @if($old->require_verification=='2' || $recipientsdata->require_verification=='2' )  style="display: block;" @else style="display: none;" @endif  id="checboxacknowlegdediv" style="display: none;">
          <div class="border-dashed border-1 border-secondary border-radius-md p-3">
            <div class="form-check">
              <input class="@if(isset($error['accept_recipient_responsibility'][0]) && !empty($error['accept_recipient_responsibility'][0])) {{'is-invalid'}} @endif form-check-input" type="checkbox" name="accept_recipient_responsibility"  value="1" id="accept_recipient_responsibility" @if($old->accept_recipient_responsibility=='1' || $recipientsdata->accept_recipient_responsibility=='1'){{'checked'}}@endif >
              <p class=" text-dark" for="accept_recipient_responsibility">I confirm that I have provided the accurate details for this recipient and I accept full responsibility if my messages are not sent to this recipient and/or are sent to someone else, because I have not provided this recipient’s correct and current contact details</p>
            </div>
          </div>
          @if(isset($error['accept_recipient_responsibility'][0]) && !empty($error['accept_recipient_responsibility'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['accept_recipient_responsibility'][0]}}
          </p>
          @endif

        </div>

      </div>
    </div>
        <div class="col-lg-12 col-sm-12" id="showverificationstepprocess" @if($old->require_verification=='1' || $recipientsdata->require_verification=='1') @else style="display: none;" @endif">
        <div class="col-lg-6 col-sm-12 mt-4">
          <p class="font-weight-bolder text-sm text-primary">Would you like to set up a password or three (3) verification questions?</p>
        </div>

        <div class="col-lg-6 col-sm-12 mt-2">
          <select name="verification_process_step_id" id="verification_process_step" class="select2 form-control @if(isset($error['verification_process_step_id'][0]) && !empty($error['verification_process_step_id'][0])) {{'is-invalid'}} @endif" style="width: 100%;" >
            @if(isset($verification_process_step) && !empty($verification_process_step))
            <option value="">Please Select</option>
            @foreach($verification_process_step as $verification_process_stepdata)
            <option value="{{$verification_process_stepdata->verification_process_step_id}}"
             @if($old->verification_process_step_id==$verification_process_stepdata->verification_process_step_id || (isset($recipientsdata->verification_process_step_id) && $recipientsdata->verification_process_step_id==$verification_process_stepdata->verification_process_step_id)) {{'selected'}} @endif

             >{{$verification_process_stepdata->description}}</option>
             @endforeach
             @endif



           </select>

           @if(isset($error['verification_process_step_id'][0]) && !empty($error['verification_process_step_id'][0]))
           <p class="form-text text-danger text-xs mb-1">
            {{$error['verification_process_step_id'][0]}}
          </p>
          @endif
        </div>
      </div>



    <div id="showverificationstep1" class=" mt-4" @if($old->verification_process_step_id=='5' || $recipientsdata->verification_process_step_id=='5' && $recipientsdata->require_verification=='1') style="display: block;" @else style="display: none;" @endif>   
      <p class="font-weight-bolder text-sm text-primary">
      Set up your recipient’s verification password and don’t forget to give it to them!</p>
      <div class="row">
        <div class="col-12 col-sm-6 ">
          <label>Create Password </label>
          <input class="password form-control @if(isset($error['password'][0]) && !empty($error['password'][0])) is-invalid @endif" type="text" name="password" id="pwd"  value="{{$recipientsdata->password}}">
          
                  <div id="pwd_strength_wrap" class="arrow-top">
                      <div id="passwordDescription" class="text-sm">Password not entered</div>
                      <div id="passwordStrength" class="strength0"></div>
                      <div id="pswd_info">
                          <p class="text-muted text-xs mb-1">
                              Please follow this guide for a strong password:
                              </p>
                             <ul class="text-muted ps-4 mb-0 float-start">

                                <li>
                                  <span class="mb-0 text-xs mx-auto" id="length">Min 6 characters</span>
                                </li>
                                <li>
                                  <span class="mb-0 text-xs mx-auto" id="spchar">One special characters (@,$,#)</span>
                                </li>
                                <li>
                                  <span class="mb-0 text-xs mx-auto" id="capital">One UpperCase Letter & small case Letters (Abcd)</span>
                                </li>
                                <li>
                                  <span class="mb-0 text-xs mx-auto" id="pnum">One number (2 are recommended)</span>
                                </li>
                              </ul>
                            </div>
                          </div>

                                
                         
          @if(isset($error['password'][0]) && !empty($error['password'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['password'][0]}}
          </p>
          @endif
        </div>

        <div class="col-12 col-sm-6 ">
          <label>Confirm Password </label>
          <input class="password_confirmation form-control @if(isset($error['password_confirmation'][0]) && !empty($error['password_confirmation'][0])) is-invalid @endif" type="text" name="password_confirmation" value="{{$recipientsdata->password}}"  >
          @if(isset($error['password_confirmation'][0]) && !empty($error['password_confirmation'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['password_confirmation'][0]}}
          </p>
          @endif
        </div>
        <div class="col-12 col-sm-6 ">
          <div id="progress-strength">
            <div id="progress-strength-bar-individual"></div>
          </div>
        </div>
      </div>
    </div>
     <div id="showverificationstep2" class="row  mt-4" @if($old->verification_process_step_id=='6' || $recipientsdata->verification_process_step_id=='6' && $recipientsdata->require_verification=='1')  @else style="display: none;" @endif>  
       <p class="font-weight-bolder text-sm text-primary">
      Set up your recipient’s verification questions and don’t forget to give it to them!</p>

        <div class="col-12 col-sm-6">
         <label>Verification Question 1</label>
         <select name="verification_question_1" class="form-control @if(isset($error['verification_question_1'][0]) && !empty($error['verification_question_1'][0])) {{'is-invalid'}} @endif" style="width: 100%;">
          @if(isset($verification_questions) && !empty($verification_questions))
           <option value="">Please Select</option>
          @foreach($verification_questions as $verification_questionsdata)
          <option value="{{$verification_questionsdata->verification_question_id}}" 
            @if($old->verification_question_1==$verification_questionsdata->verification_question_id || $recipientsdata->verification_question_1==$verification_questionsdata->verification_question_id){{'selected'}} @endif
                >{{$verification_questionsdata->description}}</option>
          @endforeach
          @endif
        </select>
        @if(isset($error['verification_question_1'][0]) && !empty($error['verification_question_1'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['verification_question_1'][0]}}
          </p>
          @endif
      </div>

      <div class="col-12 col-sm-6">
        <label>Answer Question 1</label>
        <input class="form-control @if(isset($error['verification_answer_1'][0]) && !empty($error['verification_answer_1'][0])) {{'is-invalid'}} @endif" type="text" name="verification_answer_1" 
        value="{{$recipientsdata->verification_answer_1}}" 
        >
        @if(isset($error['verification_answer_1'][0]) && !empty($error['verification_answer_1'][0]))
        <p class="form-text text-danger text-xs mb-1">
          {{$error['verification_answer_1'][0]}}
        </p>
        @endif
      </div>

      <div class="col-12 col-sm-6">
       <label>Verification Question 2</label>
       <select name="verification_question_2" class="form-control @if(isset($error['verification_question_2'][0]) && !empty($error['verification_question_2'][0])) {{'is-invalid'}} @endif" style="width: 100%;" >
        @if(isset($verification_questions) && !empty($verification_questions))
         <option value="">Please Select</option>
        @foreach($verification_questions as $verification_questionsdata2)
        <option value="{{$verification_questionsdata2->verification_question_id}}" 
          @if($old->verification_question_2==$verification_questionsdata2->verification_question_id || $recipientsdata->verification_question_2==$verification_questionsdata2->verification_question_id)
                {{'selected'}} 
          @endif>{{$verification_questionsdata2->description}}</option>
        @endforeach
        @endif
      </select>
      @if(isset($error['verification_question_2'][0]) && !empty($error['verification_question_2'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['verification_question_2'][0]}}
          </p>
          @endif
    </div>

    <div class="col-12 col-sm-6">
      <label>Answer Question 2</label>
      <input class="form-control @if(isset($error['verification_answer_2'][0]) && !empty($error['verification_answer_2'][0])) {{'is-invalid'}} @endif" type="text" name="verification_answer_2" value="{{$recipientsdata->verification_answer_2}}" >
      @if(isset($error['verification_answer_2'][0]) && !empty($error['verification_answer_2'][0]))
      <p class="form-text text-danger text-xs mb-1">
        {{$error['verification_answer_2'][0]}}
      </p>
      @endif
    </div>

    <div class="col-12 col-sm-6">
     <label>Verification Question 3</label>
     <select name="verification_question_3" class="form-control @if(isset($error['verification_question_3'][0]) && !empty($error['verification_question_3'][0])) {{'is-invalid'}} @endif" style="width: 100%;">
      @if(isset($verification_questions) && !empty($verification_questions))
      <option value="">Please Select</option>
      @foreach($verification_questions as $verification_questionsdata3)
      <option value="{{$verification_questionsdata3->verification_question_id}}"  @if($old->verification_question_3==$verification_questionsdata3->verification_question_id || $recipientsdata->verification_question_3==$verification_questionsdata3->verification_question_id)
                {{'selected'}} 
          @endif>{{$verification_questionsdata3->description}}</option>
      @endforeach
      @endif
    </select>
    @if(isset($error['verification_question_3'][0]) && !empty($error['verification_question_3'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['verification_question_3'][0]}}
          </p>
          @endif
  </div>

  <div class="col-12 col-sm-6">
    <label>Answer Question 3</label>
    <input class="form-control @if(isset($error['verification_answer_3'][0]) && !empty($error['verification_answer_3'][0])) {{'is-invalid'}} @endif" type="text" name="verification_answer_3" value="{{$recipientsdata->verification_answer_3}}"  >
    @if(isset($error['verification_answer_3'][0]) && !empty($error['verification_answer_3'][0]))
    <p class="form-text text-danger text-xs mb-1">
      {{$error['verification_answer_3'][0]}}
    </p>
    @endif
  </div>
</div>

</div>
<div class="col-12 mt-4 ">
  <div class="button-row float-end d-flex mt-4">
    <button class="submit-button btn bg-primary btn-sm mb-0 me-2" type="button" name="button">Save</button>
    <a data-route="{{route('manage-recipients.index')}}" class="link-handle btn btn-outline-dark btn-sm mb-0" type="button" name="button">Back</a>
  </div>
</div>
</form>
</div>
</div>

</div>

<script src="{{asset('assets/js/js-address-lookup.js')}}?v=1.0.3"></script>
<script src="{{asset('assets/js/recipients.js')}}?v=1.0.3"></script>


@stop
