@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Account')

@section('headercommon')
<x-main.header 
  icon="contact_card" 
  title="My Account" 
  subtitle="Manage your account details" 
/>
@endsection


@section('content')
<section class="px-3 py-2">
      <div class="row mt-4">

        <!-- Add-On Features -->
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <a data-route="{{route('my-account.add-on-features')}}" class="link-handle" class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                    <g id="Group_1840" data-name="Group 1840" transform="translate(-16871 -14051)">
                      <rect id="Rectangle_614" data-name="Rectangle 614" width="54.083" height="55" rx="16" transform="translate(16871 14051)" fill="#fef3f1"></rect>
                      <path id="Path_3870" data-name="Path 3870" d="M10,2H6A4,4,0,0,0,2,6v4H5.611A3.334,3.334,0,0,1,10,5.611ZM2,12v6a4,4,0,0,0,4,4h4V13.415L7.707,15.708a1,1,0,0,1-1.413-1.413L8.587,12ZM12,22h2.026A8.668,8.668,0,0,1,28.67,16.415V12H13.415l2.294,2.294a1,1,0,1,1-1.413,1.414L12,13.415ZM28.67,10H16.391A3.334,3.334,0,0,0,12,5.611V2H24.669a4,4,0,0,1,4,4ZM13.335,10H12V8.667A1.333,1.333,0,1,1,13.335,10ZM10,10H8.667A1.333,1.333,0,1,1,10,8.657ZM30,22.669A7.334,7.334,0,1,0,22.669,30,7.334,7.334,0,0,0,30,22.669Zm-6.667.667v3.338a.667.667,0,0,1-1.333,0V23.336h-3.34a.667.667,0,0,1,0-1.333H22V18.669a.667.667,0,0,1,1.333,0V22h3.33a.667.667,0,0,1,0,1.333Z" transform="translate(16882 14062)" fill="#f58675"></path>
                    </g>
                  </svg>
                </div>
                <div class="description ps-5">
                  <h5>Add-On Features</h5>
                  <span class="text-primary icon-move-right">
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </a>
        </div>

        <!-- Upgrade My Plan -->
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <a data-route="{{route('my-account.upgrade-plan')}}" class="link-handle" class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                    <g id="Group_1841" data-name="Group 1841" transform="translate(-17021 -14051)">
                      <rect id="Rectangle_616" data-name="Rectangle 616" width="54.083" height="55" rx="16" transform="translate(17021 14051)" fill="#fef3f1"></rect>
                      <path id="Path_3901" data-name="Path 3901" d="M16.574,6.128l-1.1,1.107-1.11-1.11A7.24,7.24,0,1,0,4.121,16.365L14.756,27a1.01,1.01,0,0,0,1.428,0L26.827,16.362A7.244,7.244,0,0,0,16.574,6.126Z" transform="translate(17033 14062.998)" fill="#f58675"></path>
                    </g>
                  </svg>
                </div>
                <div class="description ps-5">
                  <h5>Upgrade My Plan</h5>
                  <span class="text-primary icon-move-right">
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </a>
        </div>

        <!-- Free up minutes -->
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <a data-route="{{route('my-account.free-up-minutes')}}" class="link-handle" class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                    <g id="Group_1868" data-name="Group 1868" transform="translate(-16342 -14028)">
                      <rect id="Rectangle_648" data-name="Rectangle 648" width="54.083" height="55" rx="16" transform="translate(16342 14028)" fill="#fef3f1"/>
                      <path id="Path_3923" data-name="Path 3923" d="M14.639,5.776A11.139,11.139,0,1,1,3.5,16.915,11.139,11.139,0,0,1,14.639,5.776Zm0,3.931a.983.983,0,0,0-.974.849l-.009.134v5.9l.009.134a.983.983,0,0,0,1.947,0l.009-.134v-5.9l-.009-.134a.983.983,0,0,0-.974-.849Zm9.4-3.77.107.08,1.506,1.31a.983.983,0,0,1-1.185,1.563l-.106-.08L22.852,7.5a.983.983,0,0,1,1.183-1.563ZM17.587,2.5a.983.983,0,0,1,.134,1.957l-.134.009h-5.9a.983.983,0,0,1-.134-1.957L11.69,2.5Z" transform="translate(16354.298 14040.223)" fill="#f58675"/>
                    </g>
                  </svg>
                </div>
                <div class="description ps-5">
                  <h5>Free up minutes</h5>
                  <span class="text-primary icon-move-right">
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </a>
        </div>

        <!-- My Billing &amp; Subscription -->
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <a data-route="{{route('my-account.my-billing')}}" class="link-handle" class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                    <g id="Group_1843" data-name="Group 1843" transform="translate(-17246 -14051)">
                      <rect id="Rectangle_619" data-name="Rectangle 619" width="54.083" height="55" rx="16" transform="translate(17246 14051)" fill="#fef3f1"></rect>
                      <path id="Path_3874" data-name="Path 3874" d="M2,8.667A3.667,3.667,0,0,1,5.667,5H25A3.667,3.667,0,0,1,28.67,8.667V11H2ZM2,13H28.67v7A3.667,3.667,0,0,1,25,23.669H5.667A3.667,3.667,0,0,1,2,20Zm18.335,4.667a1,1,0,1,0,0,2h3.334a1,1,0,0,0,0-2Z" transform="translate(17258 14064)" fill="#f58675"></path>
                    </g>
                  </svg>
                </div>
                <div class="description ps-5">
                  <h5>My Billing & Package Details</h5>
                  <span class="text-primary icon-move-right">
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <a  onclick="OpenRedeemModal()" class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                    <g id="Group_1840" data-name="Group 1840" transform="translate(-16871 -14051)">
                      <rect id="Rectangle_614" data-name="Rectangle 614" width="54.083" height="55" rx="16" transform="translate(16871 14051)" fill="#fef3f1"></rect>
                      <path id="Path_3870" data-name="Path 3870" d="M10,2H6A4,4,0,0,0,2,6v4H5.611A3.334,3.334,0,0,1,10,5.611ZM2,12v6a4,4,0,0,0,4,4h4V13.415L7.707,15.708a1,1,0,0,1-1.413-1.413L8.587,12ZM12,22h2.026A8.668,8.668,0,0,1,28.67,16.415V12H13.415l2.294,2.294a1,1,0,1,1-1.413,1.414L12,13.415ZM28.67,10H16.391A3.334,3.334,0,0,0,12,5.611V2H24.669a4,4,0,0,1,4,4ZM13.335,10H12V8.667A1.333,1.333,0,1,1,13.335,10ZM10,10H8.667A1.333,1.333,0,1,1,10,8.657ZM30,22.669A7.334,7.334,0,1,0,22.669,30,7.334,7.334,0,0,0,30,22.669Zm-6.667.667v3.338a.667.667,0,0,1-1.333,0V23.336h-3.34a.667.667,0,0,1,0-1.333H22V18.669a.667.667,0,0,1,1.333,0V22h3.33a.667.667,0,0,1,0,1.333Z" transform="translate(16882 14062)" fill="#f58675"></path>
                    </g>
                  </svg>
                </div>
                <div class="description ps-5">
                  <h5>Redeem Code</h5>
                  <span class="text-primary icon-move-right">
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </a>
        </div>

      </div>
    </section>
    <div class="modal fade" id="RedeemModal" tabindex="-1" role="dialog" aria-labelledby="MessageDetailsLabel" aria-hidden="true" >
      <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content p-0" >
          <div class="modal-body p-5" id="RedeemModalBody" >

          
          <h5 class="text-dark">Redeem Code Now<i class="fa fa-gift text-sm ms-1" aria-hidden="true"></i></h5>
           
          <div class="description mt-4  mb-0">
            <input type="text" name="code" id="evaheld-code" class="code form-control p-2 mt-2">
          </div>
          <p class="text-sm mt-3 p-0 mb-0 mt-0">
              please enter your code to reedem your gift 
          </p>


          </div>

          <div class="modal-footer  d-flex" style="border:none;">

            <div class="ms-auto">
                <button type="button" class="btn bg-secondary btn-md mx-2 text-white" onclick="HideRedeemModal()">Cancel</button>
                <button type="button" class="btn bg-purple btn-md text-white" onclick="RedeemCodeSubmit()" >Redeem Now <i class="fa fa-gift text-sm ms-1" aria-hidden="true"></i></button>
            </div>
        </div>
</div>  
</div>
</div>
<script type="text/javascript">

  $(document).ready(function () {
  $("#confimationdelete").keyup(function () {
    $('.invalid-validation').remove();
  $(this).val($(this).val().toUpperCase());
  });
  });

  function  OpenRedeemModal()
  {
    $('#RedeemModal').modal('show');
  }


  function  HideRedeemModal()
  {
    $('#RedeemModal').modal('hide');
  }

  function RedeemCodeSubmit()
  {         
                $('.invalid-validation').remove();
                var code=$('#evaheld-code').val();
                $.ajax({
                          url: "{{route('my-account.redeem-code-submit')}}",
                          dataType: 'json',
                          type: 'POST',
                          data:{code:code},
                          headers: {
                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                          beforeSend: function() {
                            jQuery(document).find('.loadingOverlay').show();
                            
                            jQuery(document).find('.loader').show();
                        },
                        success: function(response) {

                          if(response.status=='success')
                          {
                            HideRedeemModal();
                            swal({
                              title: "Well done!",
                              text: "Code Redeemed Successfully",
                              icon: "success",
                              button: "Ok",
                            });
                            
                          }

                          else{
                           $('#evaheld-code').after('<span class="text-danger text-xs invalid-validation" > '+response.custommessage+' </span>'); 
                          }
                        },
                        error: function(xhr) { // if error occured
                            alert("Error occured.please try again");
                        },
                        complete: function() {

                            $('.select2').select2();
                            jQuery(document).find('.loadingOverlay').hide();
                             jQuery(document).find('.loader').hide();
                            

                        },
                      });
                       
      

  }

  </script>
@endsection