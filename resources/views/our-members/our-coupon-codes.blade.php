@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Promotional Codes')

@section('headercommon')
<x-main.header 
icon="people" 
title="Our Coupon Codes" 
subtitle="Discount Codes - Use this Coupons to get Discount"

/>
@endsection

@section('content')
<div class="row">
  <div class="col-lg-12 mt-lg-0">
    <div class="card">
      <div class="card-header pb-0">
        <nav style="--bs-breadcrumb-divider: '›';" aria-label="breadcrumb">
          <ol class="breadcrumb p-0 bg-white">
            <li class="breadcrumb-item"><a data-route="{{route('member.our-account')}}" class="link-handle h5 text-sm font-weight-bolder text-primary">Our Account</a></li>
            <li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page">Our Coupon Codes</li>
          </ol>
        </nav>
      </div>
      <div class="card-body pt-0">
      <div class="row mt-3">
      @if(count($promotionalcodes)>0)
      @foreach ($promotionalcodes as $rkey => $promotionalcodes)
      <div class="col-lg-4 col-md-6 mb-4 ">
        <div class="card bg-white-100 shadow-lg">
          <div class="card-body p-3 ">
            <div class="d-flex">
              <!-- Avatar Large -->
              <div class="avatar avatar-xl position-relative border-radius-xl bg-blue-pale d-none d-lg-block d-xl-block cursor-pointer" >
                <div class="card-body z-index-3 text-center p-3">
                  <i class="fi fi_tag text-blue" style="font-size: 42px;" aria-hidden="true"></i>
                </div>
              </div>
              <!-- Avatar Small -->
              <div class="avatar avatar-lg position-relative border-radius-xl bg-blue-pale d-block d-lg-none d-xl-none cursor-pointer" >
                <div class="card-body z-index-3 text-center p-2 py-3">
                  <i class="fi fi_tag text-blue" style="font-size: 28px" aria-hidden="true"></i>
                </div>
              </div>
              <div class="ms-3 my-auto">
                <div class="d-flex flex-column justify-content-center">
                  <h6 class="mb-0 ">{{$promotionalcodes->code
                  }}</h6>


                </div>
                <div class="avatar-group">

                  <p class="text-sm">Reedem Count: {{$promotionalcodes->times_redeemed}}</p>

                </div>

              </div>

            </div>

            <hr class="horizontal dark">
            <div class="row">
              <div class="col-6 ">

                @if($promotionalcodes->active==1)

                <i class="fa fa-check-circle-o text-success mt-1" aria-hidden="true"></i>
                <span class="text-sm text-dark">Active</span>

                @else

                <i class="fa fa-times-circle-o text-danger" aria-hidden="true"></i>
                <span class="text-sm text-dark">In Active</span>

                @endif
              </div>
              <div class="col-6 text-end">
                <h6 class="text-sm mb-0">{{date('d.m.Y',strtotime($promotionalcodes->created_at))}}</h6>
                <p class="text-secondary text-sm font-weight-bold mb-0">Created date</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endforeach

      @else
      <section class="px-3 py-2">

       <div class="row mt-4">


         <div class="col-lg-12 col-md-12 d-flex flex-column mt-3 border-radius-lg text-center">

           <div class="card h-100 card-background">
             <div class="full-background"></div>
             <div class="card-body z-index-3 text-center">
               <h6 class="text-dark mb-4 mt-3">No Promotional codes Found</h6> 

             </div>

           </div>
         </div>
       </div>
     </section>

      @endif
      </div>
      </div>

   </div>
 </div>
</div>
@stop
