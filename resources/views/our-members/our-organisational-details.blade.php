@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Our Organisation details')

@section('headercommon')
<x-main.header icon="person" title="Our Organisation details" subtitle="Keep your information up to date" />
@endsection

@section('content')

<div class="row ">

 <div class="col-lg-12 mt-lg-0">
   <div class="card">
    <div class="card-header pb-0">
     <nav style="--bs-breadcrumb-divider: '›';" aria-label="breadcrumb">
      <ol class="breadcrumb p-0 bg-white">
        <li class="breadcrumb-item"><a data-route="{{route('member.our-profile')}}" class="link-handle h5 text-sm font-weight-bolder text-primary">Our Profile</a></li>
        <li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page">Our Organisation details</li>
      </ol>
    </nav>

  </div>
  <div class="card-body pt-0">
    <form action="{{route('member.our-organisational-details.update')}}" method="POST">
      @csrf
      <input type="hidden" name="isorganisation" value="1">
      <div class="row">
        <div class="col-12 col-sm-6  mt-1">
          <label>Organisation Name <span class="text-danger">*</span></label>
          <input class="form-control" type="text" name="organisation_name" value="{{$userdata->organisation_name}}" readonly=""  />
          @if(isset($error['organisation_name'][0]) && !empty($error['organisation_name'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['organisation_name'][0]}}
          </p>
          @endif
        </div>

        <div class="col-12 col-sm-6  mt-1">
          <label>Organisation Code <span class="text-danger">*</span></label>
          <input class="form-control" type="text" name="organisation_code" value="{{$userdata->organisation_code}}" readonly="" placeholder="example@organisation_code.com" />
          @if(isset($error['organisation_code'][0]) && !empty($error['organisation_code'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['organisation_code'][0]}}
          </p>
          @endif
        </div>
      </div>

      <h5 class="font-weight-bolder text-sm text-primary mt-3"> &nbsp;Basic Info</h5>  

      <div class="row">
        <div class="col-12 col-sm-6">
          <label>First name <span class="text-danger">*</span></label>
          <input class="form-control @if(isset($error['first_name'][0]) && !empty($error['first_name'][0])) is-invalid @endif" type="text" name="first_name" value="{{$userdata->first_name}}" placeholder="Jane" />
          @if(isset($error['first_name'][0]) && !empty($error['first_name'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['first_name'][0]}}
          </p>
          @endif
        </div>
        <div class="col-12 col-sm-6">
          <label>Last name <span class="text-danger">*</span></label>
          <input class="form-control @if(isset($error['last_name'][0]) && !empty($error['last_name'][0])) is-invalid @endif" type="text" name="last_name" value="{{$userdata->last_name}}" placeholder="Smith" />
          @if(isset($error['last_name'][0]) && !empty($error['last_name'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['last_name'][0]}}
          </p>
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-sm-6  mt-1">
          <label>Email address <span class="text-danger">*</span></label>
          <input class="form-control" type="text" name="email" value="{{$userdata->email}}" readonly="" placeholder="example@email.com" />
          @if(isset($error['email'][0]) && !empty($error['email'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['email'][0]}}
          </p>
          @endif
        </div>
        <div class="col-12 col-sm-6  mt-1">
          <div class="row">
            <label>Phone number</label>
            <div class="col-3 mt-3 mt-sm-0 pr-0 mr-0">
              <select name="phone_code" class="phonecodeselect2 form-control" style="width: 100%;">
                @if(isset($phonecodes) && !empty($phonecodes))
                @foreach($phonecodes as $data)
                <option value="{{$data->id}}" data-code="{{$data->iso2}}" @if(isset($userdata->phone_code) && $userdata->phone_code==$data->id){{"selected"}}  @endif >+{{$data->phone_code}} {{$data->iso2}} </option>
                @endforeach
                @endif

              </select>
            </div>
            <div class="col-9 mt-3 mt-sm-0 pl-0 ml-0 mb-4">
              <input id="phone_number" class="required form-control @if(isset($error['phone_number'][0]) && !empty($error['phone_number'][0])) is-invalid @endif" type="number" name="phone_number" value="{{$userdata->phone_number}}" placeholder="0412345678" />
              @if(isset($error['phone_number'][0]) && !empty($error['phone_number'][0]))
              <p class="form-text text-danger text-xs mb-1">
                {{$error['phone_number'][0]}}
              </p>
              @endif
            </div>
          </div>
        </div>
      </div>
      <hr class="horizontal dark" />
      <h6 class="font-weight-bolder text-sm mx-auto text-primary mt-4 mb-0 p-0">Some extra details would be great</h6>
      <p class="text-dark text-sm pt-1 ">Given the precious messages you’re entrusting us with, we want to make sure we can always reach you</p>
      <div class="row mt-1">
        <div class="col-12 col-sm-6">
          <label>Date of birth</label>
          <div id="datepicker" class="input-group-alternative date custom-datepicker" data-date-format="dd-mm-yyyy">
            <input required type="text" id="dob-date" value="<?php echo isset($userdata->dob) && !empty($userdata->dob) ? date('d/m/Y', strtotime($userdata->dob)) : '' ?> " name="dob" class=" date-validation form-control  @if(isset($error['dob'][0]) && !empty($error['dob'][0])) is-invalid @endif" placeholder="dd-mm-yyyy" />
            <span class="input-group-addon">
            </span>
          </div>
          @if(isset($error['dob'][0]) && !empty($error['dob'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['dob'][0]}}
          </p>
          @endif
        </div>
        <div class="col-12 col-sm-6"></div>
        <div class="col-12 col-sm-6">
          <label>Your Role*</label>
          <select name="organisation_role" id="organisation_role" class="select2 form-control @if(isset($error['organisation_role'][0]) && !empty($error['organisation_role'][0])) is-invalid @endif" style="width: 100%;">
            @if(isset($organisation_roles) && count($organisation_roles)>0)
            @foreach($organisation_roles as $rolekey=>$rolelabel)
            <optgroup label="{{$rolekey}}">{{$rolekey}}</optgroup>
            @foreach($rolelabel as $organisationrole)  
            <option value="{{$organisationrole->organisation_role_id}}" @if(isset($userdata) && $userdata->organisation_role==$organisationrole->organisation_role_id) {{'selected'}} @endif>{{$organisationrole->description}}</option>
            @endforeach
            @endforeach
            @endif
          </select>
          @if(isset($error['organisation_role'][0]) && !empty($error['organisation_role'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['organisation_role'][0]}}
          </p>
          @endif
        </div>
        <div class="col-12 col-sm-6">
          <label>Organisation Departments</label>
          <select name="organisation_department" id="organisation_department" class="select2 form-control" style="width: 100%;">
            @if(isset($organisation_department) && count($organisation_department)>0)
            @foreach($organisation_department as $departmentdata)
            <option value="{{$departmentdata->department_id}}" @if(isset($userdata) && $userdata->organisation_department==$departmentdata->department_id) {{'selected'}} @endif>{{$departmentdata->name}}</option>
            @endforeach
            @endif
          </select>
        </div>
        <div class="col-12 col-sm-6 other_organisation_roles" @if($userdata->reason=='7') style="display: block;" @else style="display: none;" @endif >
          <label>Other Role</label>
          <textarea class="form-control @if(isset($error['other_organisation_roles'][0]) && !empty($error['other_organisation_roles'][0])) is-invalid @endif" type="text" name="other_organisation_roles"> {{$userdata->other_organisation_roles}}</textarea>
          @if(isset($error['other_organisation_roles'][0]) && !empty($error['other_organisation_roles'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['other_organisation_roles'][0]}}
          </p>
          @endif
        </div>

      </div>
      <div class="row">
        <div class="col-12 col-sm-6 field-wrapper address-lookup mt-1">
          <label> Your address </label>
          <input class="form-control address-lookup__field" type="text" name="jls" autocomplete="off" placeholder="&nbsp;">
          <a href="javascript:;" class="mt-1 show-manual text-xs float-end text-secondary"><u>Manually enter my address</u></a>

          @if(isset($error['address_line_1'][0]) && !empty($error['address_line_1'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['address_line_1'][0]}}
          </p>
          @endif
          @if(isset($error['address_line_2'][0]) && !empty($error['address_line_2'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['address_line_2'][0]}}
          </p>
          @endif
          @if(isset($error['state'][0]) && !empty($error['state'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['state'][0]}}
          </p>
          @endif
          @if(isset($error['postcode'][0]) && !empty($error['postcode'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['postcode'][0]}}
          </p>
          @endif

        </div>
      </div>
      <div class="row show-manual-div" style="display: none;">
        <input class="form-control" id="manual-address" name="manual" type="hidden" value="0">
        <input class="form-control" name="city" type="hidden">
        <input class="form-control" name="county" type="hidden">
        <div class="col-12 col-sm-6">
          <label>Address Line 1 </label>
          <input class="form-control @if(isset($error['address_line_1'][0]) && !empty($error['address_line_1'][0])) is-invalid @endif" type="text" name="address_line_1" value="{{$userdata->address_1}}">
          @if(isset($error['address_line_1'][0]) && !empty($error['address_line_1'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['address_line_1'][0]}}
          </p>
          @endif
        </div>
        <div class="col-12 col-sm-6">
          <label>Address Line 2</label>
          <input class="form-control @if(isset($error['address_line_2'][0]) && !empty($error['address_line_2'][0])) is-invalid @endif" type="text" name="address_line_2" value="{{$userdata->address_2}}">
          @if(isset($error['address_line_2'][0]) && !empty($error['address_line_2'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['address_line_2'][0]}}
          </p>
          @endif
        </div>
        <div class="col-12 col-sm-6">
          <label>Postcode</label>
          <input class="form-control @if(isset($error['postcode'][0]) && !empty($error['postcode'][0])) is-invalid @endif" name="postcode" type="number" value="{{$userdata->postcode}}">
          @if(isset($error['postcode'][0]) && !empty($error['postcode'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['postcode'][0]}}
          </p>
          @endif
        </div>
        <div class="col-12 col-sm-6">
          <label>State</label>
          <input class="form-control @if(isset($error['state'][0]) && !empty($error['state'][0])) is-invalid @endif" name="state" type="text" value="{{$userdata->state}}">
          @if(isset($error['state'][0]) && !empty($error['state'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['state'][0]}}
          </p>
          @endif
        </div>
        <div class="col-12 col-sm-6 mb-2">
          <label>Country</label>
          <select name="country" id="country" class="select2 form-control @if(isset($error['country'][0]) && !empty($error['country'][0])) is-invalid @endif" style="width: 100%;">
            @if(isset($phonecodes) && !empty($phonecodes))
            <option value=""></option>
            @foreach($phonecodes as $data)
            <option value="{{$data->iso2}}" @if(isset($userdata->country) && $userdata->country==$data->iso2){{"selected"}} @endif > {{$data->name}} </option>
            @endforeach
            @endif
          </select>
          @if(isset($error['country'][0]) && !empty($error['country'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['country'][0]}}
          </p>
          @endif
        </div>
      </div>
      <button class="btn bg-primary mb-0 submit-button mb-3" type="button">Save Changes</button>
    </form>
  </div>
</div>
</div>
</div>




<script src="{{asset('assets/js/js-address-lookup.js')}}?v=1.0.3"></script>

<script type="text/javascript">
  var startDate = new Date();
  startDate.setFullYear(startDate.getFullYear() - 100);
  var endDate = new Date();
  endDate.setFullYear(endDate.getFullYear() - 22);

  $('body').find(".custom-datepicker").datepicker({ 
    autoclose: true, 
    todayHighlight: true,
    format: 'dd/mm/yyyy',
    startDate: '01-01-1920',
    endDate: endDate
  });

  $(document).on('change',"#organisation_roles" ,function() {

    $('.other_organisation_roles').hide();

    if($("#organisation_roles option:selected").text()=='Other')
    {
      $('.other_organisation_roles').show();
    }

  });

  $(document).on('change',"#organisation_department" ,function() {

    $('.other_organisation_department').hide();

    if($("#organisation_department option:selected").text()=='Other')
    {
      $('.other_organisation_department').show();
    }

  });

  $(document).on('change',"#organisation_role" ,function() {

    $('.other_role').hide();

    if($("#organisation_role option:selected").text()=='Other')
    {
      $('.other_role').show();
    }

  });
  $(document).on('click','.show-manual', function(e) {
    e.preventDefault();
    $(document).find('.show-manual-div').show();
    $(document).find('#manual-address').val(1);
  });
</script>
@endsection