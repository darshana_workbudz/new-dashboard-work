@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Our Billing')

@section('headercommon')
<x-main.header icon="payment" title="Our Billing" subtitle="Manage Your Billing Details" />
@endsection

@section('content')
<div class="row mb-5">
  <div class="col-lg-12 mt-lg-0">
    <div class="card  mt-3" id="myaccount-billing">
      <div class="card-header">
        <div class="d-lg-flex">
          <nav style="--bs-breadcrumb-divider: '›';" aria-label="breadcrumb">
            <ol class="breadcrumb p-0 bg-white">
              <li class="breadcrumb-item"><a data-route="{{route('member.our-account')}}" class="link-handle h5 text-sm font-weight-bolder text-primary">Our Account</a></li>
              <li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page">Our Billing</li>
            </ol>
          </nav>
        </div>
        <p class="text-sm mb-0 font-weight-bold">Billing History</p>
      </div>
      <div class="card-body pt-0">
        @if(isset($userinvoices) && count($userinvoices)>0)
        <div class="card card shadow-lg">
          <div class="card-body">
            <ul class="list-group">
              @foreach($userinvoices as $invoicedata)
              <li class="list-group-item border-0 justify-content-between p-0 m-0 border-radius-lg">
                <div class="row">
                  <div class="col-auto">
                    <button class="btn btn-icon-only btn-rounded  @if($invoicedata->status=='paid') btn-outline-success @else btn-outline-danger @endif mb-0 me-3 p-3  d-flex align-items-center justify-content-center">
                      @if($invoicedata->status=='paid')
                      <i class="fas fa-check " aria-hidden="true"></i>
                      @else
                      <i class="fi fi_dismiss " aria-hidden="true"></i>
                      @endif
                    </button>
                    <div class="d-flex flex-column">
                      <h6 class="mb-1 text-dark text-sm">Invoice #{{$invoicedata->number}}</h6>
                      <span class="text-xs"><?php echo date('d F Y', strtotime($invoicedata->created_at)); ?>, at <?php echo date('h:m a', strtotime($invoicedata->created_at)); ?></span>
                      @if($invoicedata->status=='paid')
                      <span class="text-sm text-primary mt-1 ">
                        $ {{$invoicedata->amount_due}}.00 AUD <span class="mb-1 badge badge-sm bg-success mb-0 mt-2 ms-1">{{$invoicedata->status}} </span> </span>
                      @endif
                      @if($invoicedata->status!=='paid')
                      <span class="text-sm text-primary mt-1 ">
                        $ {{$invoicedata->amount_remaining}}.00 AUD <span class="mb-1 badge badge-sm bg-danger mb-0 mt-2 ms-1">Due amount</span> </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-auto d-flex align-items-center">
                    <div class="row">
                      @if($invoicedata->status=='paid')
                      <div class="col-12"> <a href="{{$invoicedata->hosted_invoice_url}}"> <span class="badge badge-lg bg-primary mb-0 mt-2">View</span> </a> </div>
                      @endif
                      @if($invoicedata->status!=='paid')
                      <div class="col-12"> <a href="{{$invoicedata->hosted_invoice_url}}"> <span class="badge badge-lg bg-primary mb-0 mt-2">Pay Now</span> </a> </div>
                      @endif
                      <div class="col-12"><a href="{{$invoicedata->invoice_pdf}}"><span class="badge badge-lg bg-orange mb-0 mt-2">Download</span></a></div>
                    </div>
                  </div>
                </div>
              </li>
              @endforeach
            </ul>
          </div>
        </div>
        @else
        <blockquote class="text-sm"> - Sorry no Invoices Found !</blockquote>
        @endif
      </div>
</div>
  </div>
  
</div>

<script type="text/javascript">

  $('.scrolltodiv').click(function (e) {
    var scrollto=$(this).data('scroll');
    $('html, body').animate({
      scrollTop: $(scrollto).offset().top
    }, 2000);
  });

  $(function() {
    var target = $('#myaccount-billing');
    if (target.length) {


      $('html,body').animate({
        scrollTop: target.offset().top
      }, 1000);
      return false;
    }
  });
</script>
@endsection