@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Pre Purchase Packages')

@section('headercommon')
<x-main.header 
icon="heart" 
title="Request New Packages" 
subtitle="Now Request New packages To your Organisation"

/>
@endsection

@section('content')
<div class="row ">

 <div class="col-lg-12 mt-lg-0">
  <div class="card">
    <div class="card-header pb-0">
        <nav style="--bs-breadcrumb-divider: '›';" aria-label="breadcrumb">
          <ol class="breadcrumb p-0 bg-white">
            <li class="breadcrumb-item"><a data-route="{{route('member.our-account')}}" class="link-handle h5 text-sm font-weight-bolder text-primary">Our Account</a></li>
            <li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page">Request New Packages</li>
          </ol>
        </nav>
        </div>
    <div class="card-body pt-0">
      <div class="row">
        @if(isset($allonceoffplans) && !empty($allonceoffplans))
        <h5 class="p-2 ps-4 mt-3">Connection Content Plans</h5>
        <section class="pricing-box-con" style="padding-top:0px !important">
          <div class="container">
            <div class="row">

              <div class="col-md-12">
                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

                  @error('purchasesubscription')
                  <h5 class="form-text text-danger text-center text-xs ms-1">
                    {{ $message }}
                  </h5>
                  @endif
                  <!-- tab 1 content start-->
                  <div class="tab-pane fade show active" id="nav-ConnectionContent" data-toggle="tab-content">
                    <div class="pricing-box-div">
                      <div class="row">


                        @foreach($allonceoffplans as $onceoffpackagesdata)
                        <div class="col-md-6">

                          <div class="pricing-box h-100 bg-white-100 shadow-lg">

                            <div class="icon"><svg width=" 24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                              <path d="m12.82 5.58-.82.822-.824-.824a5.375 5.375 0 1 0-7.601 7.602l7.895 7.895a.75.75 0 0 0 1.06 0l7.902-7.897a5.376 5.376 0 0 0-.001-7.599 5.38 5.38 0 0 0-7.611 0Z" fill="#662d91" />
                            </svg>
                          </div>
                          <h4>{{$onceoffpackagesdata->name}}</h4>
                          <div class="content">
                            <p>{{$onceoffpackagesdata->saleheadline}}</p>
                          </div>  


                          <h5>${{$onceoffpackagesdata->price}}</h5>
                          <h6><span><strong>{{$onceoffpackagesdata->salequotes}}</strong></span></h6>

                          <p class="text-sm">{{$onceoffpackagesdata->quotes}}</p>


                          <form method="post" action="{{route('member.request-packages-post',$onceoffpackagesdata->package_id)}}">
                            <input type="hidden" name="package_id" value="{{$onceoffpackagesdata->package_id}}">
                            <input type="hidden" name="purchasesubscription" value="notrial">
                            <input type="hidden" name="redirecturl" value="{{route('member.pre-purchase-packages')}}">
                            @csrf

                          <div class="d-flex row mt-3 justify-content-center">
                            <div class="col-3">
                              <div class="input-group mb-3">
                                <input type="number" class="form-control" name="quantity" value="1" aria-label="Quantity of pre purchase packages" aria-describedby="Quantity of pre purchase packages" />

                              </div>
                            </div>
                            <div class="col-5">
                              <button type="submit" class="submit-button btn btn-block btn-md btn-primary mb-0">
                                <span class="btn-inner--icon">
                                  <i class="fa fa-plus pe-2" aria-hidden="true"></i>
                                </span>
                                <span class="btn-inner--text ps-1">Request Now</span>
                              </button>
                            </div>
                          </div>
                        </form>

                        </div>



                      </div>
                      @endforeach

                    </div>



                  </div>
                </div>
              </div>
            </div>

            </div>
          </div>
        </section>
      @endif

      @if(isset($allstandaloneqrplans) && !empty($allstandaloneqrplans))
      <hr class="horizontal dark">
      <h5 class="p-2 ps-4">Memorial QR Codes</h5>
      <section class="pricing-box-con" style="padding-top:0px !important">
        <div class="container">
          <div class="row">

            <div class="col-md-12">
              <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

               <!-- tab 1 content start-->
                <div class="tab-pane fade show active" id="nav-ConnectionContent" data-toggle="tab-content">
                  <div class="pricing-box-div">
                    <div class="row">


                      @foreach($allstandaloneqrplans as $standalonepackagesdata)
                      <div class="col-md-6">

                        <div class="pricing-box h-100">

                          <div class="icon"><div class="avatar avatar-lg">
                        <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M8 6H6v2h2V6Z" fill="#773eb1"></path><path d="M3 5.5A2.5 2.5 0 0 1 5.5 3h3A2.5 2.5 0 0 1 11 5.5v3A2.5 2.5 0 0 1 8.5 11h-3A2.5 2.5 0 0 1 3 8.5v-3ZM5.5 5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3ZM6 16h2v2H6v-2Z" fill="#773eb1"></path><path d="M3 15.5A2.5 2.5 0 0 1 5.5 13h3a2.5 2.5 0 0 1 2.5 2.5v3A2.5 2.5 0 0 1 8.5 21h-3A2.5 2.5 0 0 1 3 18.5v-3Zm2.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3ZM18 6h-2v2h2V6Z" fill="#773eb1"></path><path d="M15.5 3A2.5 2.5 0 0 0 13 5.5v3a2.5 2.5 0 0 0 2.5 2.5h3A2.5 2.5 0 0 0 21 8.5v-3A2.5 2.5 0 0 0 18.5 3h-3ZM15 5.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v3a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-3ZM13 13h2.75v2.75H13V13ZM18.25 15.75h-2.5v2.5H13V21h2.75v-2.75h2.5V21H21v-2.75h-2.75v-2.5ZM18.25 15.75V13H21v2.75h-2.75Z" fill="#773eb1"></path></svg>
                      </div></i>
                          
                        </div>
                        <h4>{{$standalonepackagesdata->name}}</h4>
                        <div class="content">
                          <p>{{$standalonepackagesdata->saleheadline}}</p>
                        </div>  


                        <h5>${{$standalonepackagesdata->price}}</h5>
                        <h6><span><strong>{{$standalonepackagesdata->salequotes}}</strong></span></h6>

                        <p class="text-sm">{{$standalonepackagesdata->quotes}}</p>


                         <form method="post" action="{{route('member.request-packages-post',$standalonepackagesdata->package_id)}}">
                            <input type="hidden" name="package_id" value="{{$standalonepackagesdata->package_id}}">
                            <input type="hidden" name="purchasesubscription" value="notrial">
                            <input type="hidden" name="redirecturl" value="{{route('member.pre-purchase-packages')}}">
                            @csrf

                        <div class="d-flex row mt-3 justify-content-center">
                          <div class="col-3">
                            <div class="input-group mb-3">
                              <input type="number" class="form-control" name="quantity" value="1" aria-label="Quantity of pre purchase packages" aria-describedby="Quantity of pre purchase packages" />

                            </div>
                          </div>
                          <div class="col-5">
                            <button type="submit" class="submit-button btn btn-block btn-md btn-primary mb-0">
                              <span class="btn-inner--icon">
                                <i class="fa fa-plus pe-2" aria-hidden="true"></i>
                              </span>
                              <span class="btn-inner--text ps-1">Request Now</span>
                            </button>
                          </div>
                        </div>
                      </form>
                      </div>



                    </div>
                    @endforeach

                  </div>



                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </section>
    @endif

    @if(isset($allstandaloneheirloomplans) && !empty($allstandaloneheirloomplans))
    <hr class="horizontal dark">
    <h5 class="p-2 ps-4 ">Heirloom Keepsake</h5>
    <section class="pricing-box-con" style="padding-top:0px !important">
      <div class="container">
        <div class="row">

          <div class="col-md-12">
            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

              @error('purchasesubscription')
              <h5 class="form-text text-danger text-center text-xs ms-1">
                {{ $message }}
              </h5>
              @endif
              <!-- tab 1 content start-->
              <div class="tab-pane fade show active" id="nav-ConnectionContent" data-toggle="tab-content">
                <div class="pricing-box-div">
                  <div class="row">


                    @foreach($allstandaloneheirloomplans as $standalonepackagesdata)
                    <div class="col-md-6">

                      <div class="pricing-box h-100">

                         <div class="icon"><div class="avatar avatar-lg">
                        <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M11.25 13v9h-4A3.25 3.25 0 0 1 4 18.75V13h7.25ZM20 13v5.75A3.25 3.25 0 0 1 16.75 22h-4v-9H20ZM14.5 2a3.25 3.25 0 0 1 2.738 5.002L19.75 7c.69 0 1.25.466 1.25 1.042v2.916c0 .576-.56 1.042-1.25 1.042l-7-.001V7h-1.5v4.999l-7 .001C3.56 12 3 11.534 3 10.958V8.042C3 7.466 3.56 7 4.25 7l2.512.002A3.25 3.25 0 0 1 12 3.174 3.24 3.24 0 0 1 14.5 2Zm-5 1.5a1.75 1.75 0 0 0-.144 3.494L9.5 7h1.75V5.25l-.006-.144A1.75 1.75 0 0 0 9.5 3.5Zm5 0a1.75 1.75 0 0 0-1.75 1.75V7h1.75a1.75 1.75 0 1 0 0-3.5Z" fill="#773eb1"></path></svg>
                      </div>
                        </div>
                        <h4>{{$standalonepackagesdata->name}}</h4>
                        <div class="content">
                          <p>{{$standalonepackagesdata->saleheadline}}</p>
                        </div>  


                        <h5>${{$standalonepackagesdata->price}}</h5>
                        <h6><span><strong>{{$standalonepackagesdata->salequotes}}</strong></span></h6>

                        <p class="text-sm">{{$standalonepackagesdata->quotes}}</p>


                         <form method="post" action="{{route('member.request-packages-post',$standalonepackagesdata->package_id)}}">
                            <input type="hidden" name="package_id" value="{{$standalonepackagesdata->package_id}}">
                            <input type="hidden" name="purchasesubscription" value="notrial">
                            <input type="hidden" name="redirecturl" value="{{route('member.pre-purchase-packages')}}">
                            @csrf


                        <div class="d-flex row mt-3 justify-content-center">
                          <div class="col-3">
                            <div class="input-group mb-3">
                              <input type="number" class="form-control" name="quantity" value="1" aria-label="Quantity of pre purchase packages" aria-describedby="Quantity of pre purchase packages" />

                            </div>
                          </div>
                          <div class="col-5">
                            <button type="submit" class="submit-button btn btn-block btn-md btn-primary mb-0">
                              <span class="btn-inner--icon">
                                <i class="fa fa-plus pe-2" aria-hidden="true"></i>
                              </span>
                              <span class="btn-inner--text ps-1">Request Now</span>
                            </button>
                          </div>
                        </div>
                      </form>

                      </div>



                    </div>
                    @endforeach

                  </div>



                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </section>
    @endif

  </div>
</div>
</div>
</div>



</div>

@stop
