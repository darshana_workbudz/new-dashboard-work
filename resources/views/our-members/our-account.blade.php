@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Account')

@section('headercommon')
<x-main.header 
  icon="contact_card" 
  title="My Account" 
  subtitle="Manage your account details" 
/>
@endsection


@section('content')
<section class="px-3 py-2">
      <div class="row mt-4">

        <!-- Add-On Features -->
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <a data-route="{{route('member.coupon-codes')}}" class="link-handle" class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                    <g id="Group_1840" data-name="Group 1840" transform="translate(-16871 -14051)">
                      <rect id="Rectangle_614" data-name="Rectangle 614" width="54.083" height="55" rx="16" transform="translate(16871 14051)" fill="#fef3f1"></rect>
                      <path id="Path_3870" data-name="Path 3870" d="M10,2H6A4,4,0,0,0,2,6v4H5.611A3.334,3.334,0,0,1,10,5.611ZM2,12v6a4,4,0,0,0,4,4h4V13.415L7.707,15.708a1,1,0,0,1-1.413-1.413L8.587,12ZM12,22h2.026A8.668,8.668,0,0,1,28.67,16.415V12H13.415l2.294,2.294a1,1,0,1,1-1.413,1.414L12,13.415ZM28.67,10H16.391A3.334,3.334,0,0,0,12,5.611V2H24.669a4,4,0,0,1,4,4ZM13.335,10H12V8.667A1.333,1.333,0,1,1,13.335,10ZM10,10H8.667A1.333,1.333,0,1,1,10,8.657ZM30,22.669A7.334,7.334,0,1,0,22.669,30,7.334,7.334,0,0,0,30,22.669Zm-6.667.667v3.338a.667.667,0,0,1-1.333,0V23.336h-3.34a.667.667,0,0,1,0-1.333H22V18.669a.667.667,0,0,1,1.333,0V22h3.33a.667.667,0,0,1,0,1.333Z" transform="translate(16882 14062)" fill="#f58675"></path>
                    </g>
                  </svg>
                </div>
                <div class="description ps-5">
                  <h5>Our Coupon Codes</h5>
                  <span class="text-primary icon-move-right">
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </a>
        </div>

        <!-- Upgrade My Plan -->
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <a data-route="{{route('member.pre-purchase-packages')}}" class="link-handle" class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                    <g id="Group_1841" data-name="Group 1841" transform="translate(-17021 -14051)">
                      <rect id="Rectangle_616" data-name="Rectangle 616" width="54.083" height="55" rx="16" transform="translate(17021 14051)" fill="#fef3f1"></rect>
                      <path id="Path_3901" data-name="Path 3901" d="M16.574,6.128l-1.1,1.107-1.11-1.11A7.24,7.24,0,1,0,4.121,16.365L14.756,27a1.01,1.01,0,0,0,1.428,0L26.827,16.362A7.244,7.244,0,0,0,16.574,6.126Z" transform="translate(17033 14062.998)" fill="#f58675"></path>
                    </g>
                  </svg>
                </div>
                <div class="description ps-5">
                  <h5>My Pre Purchase Packages</h5>
                  <span class="text-primary icon-move-right">
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <a data-route="{{route('member.request-packages')}}" class="link-handle" class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                    <g id="Group_1841" data-name="Group 1841" transform="translate(-17021 -14051)">
                      <rect id="Rectangle_616" data-name="Rectangle 616" width="54.083" height="55" rx="16" transform="translate(17021 14051)" fill="#fef3f1"></rect>
                      <path id="Path_3901" data-name="Path 3901" d="M16.574,6.128l-1.1,1.107-1.11-1.11A7.24,7.24,0,1,0,4.121,16.365L14.756,27a1.01,1.01,0,0,0,1.428,0L26.827,16.362A7.244,7.244,0,0,0,16.574,6.126Z" transform="translate(17033 14062.998)" fill="#f58675"></path>
                    </g>
                  </svg>
                </div>
                <div class="description ps-5">
                  <h5>Request New Packages</h5>
                  <span class="text-primary icon-move-right">
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </a>
        </div>



        

        <!-- My Billing &amp; Subscription -->
        <!-- div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <a data-route="{{route('member.billings')}}" class="link-handle" class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                    <g id="Group_1843" data-name="Group 1843" transform="translate(-17246 -14051)">
                      <rect id="Rectangle_619" data-name="Rectangle 619" width="54.083" height="55" rx="16" transform="translate(17246 14051)" fill="#fef3f1"></rect>
                      <path id="Path_3874" data-name="Path 3874" d="M2,8.667A3.667,3.667,0,0,1,5.667,5H25A3.667,3.667,0,0,1,28.67,8.667V11H2ZM2,13H28.67v7A3.667,3.667,0,0,1,25,23.669H5.667A3.667,3.667,0,0,1,2,20Zm18.335,4.667a1,1,0,1,0,0,2h3.334a1,1,0,0,0,0-2Z" transform="translate(17258 14064)" fill="#f58675"></path>
                    </g>
                  </svg>
                </div>
                <div class="description ps-5">
                  <h5>My Billing & Package Details</h5>
                  <span class="text-primary icon-move-right">
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </a>
        </div> -->
      </div>
    </section>

@endsection