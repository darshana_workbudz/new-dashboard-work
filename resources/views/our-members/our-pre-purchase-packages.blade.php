@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Our Pre Purchase Packages')

@section('headercommon')
<x-main.header 
icon="heart" 
title="Our Pre Purchase Packages" 
subtitle="Share packages to your Client's"
button
buttonText="Request New Packages"
buttonIcon="add"
data-route="{{route('member.request-packages')}}"
/>
@endsection

@section('content')
<div class="row">
  <div class="col-lg-12 mt-lg-0">
    <div class="card">
      <div class="card-header pb-0">
        <nav style="--bs-breadcrumb-divider: '›';" aria-label="breadcrumb">
          <ol class="breadcrumb p-0 bg-white">
            <li class="breadcrumb-item"><a data-route="{{route('member.our-account')}}" class="link-handle h5 text-sm font-weight-bolder text-primary">Our Account</a></li>
            <li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page">Our Pre Purchase Packages</li>
          </ol>
        </nav>
      </div>
      <?php $hasprepurchasepackages=0;?>
      @if(isset($prepurchasepackages) && count($prepurchasepackages)>0)
      <?php $hasprepurchasepackages=1;?>
      <h5 class="p-2 ps-4 mt-3">Connection Content Plans ({{count($prepurchasepackages)}})</h5>
      <section class="pricing-box-con" style="padding-top:0px !important">
        <div class="container">
          <div class="row">

            <div class="col-md-12">
              <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

                <!-- tab 1 content start-->
                <div class="tab-pane fade show active" id="nav-ConnectionContent" data-toggle="tab-content">
                  <div class="pricing-box-div">
                    <div class="row">

                      @foreach ($prepurchasepackages as $rkey => $prepurchasepackagesdata)
                      <div class="col-md-4">

                        <div class="pricing-box bg-white-100 shadow-lg">

                          <div class="d-flex">
                            <div class="ms-auto">
                              <div class="dropdown">
                                <button onclick="ShowPackageDetails({{$prepurchasepackagesdata->user_package_id}})" class="btn-lg btn btn-link text-secondary ps-0 p-0"  >
                                  <!-- <i class="fa fa-share-alt text-lg text-alternate-primary" aria-hidden="true"></i> -->
                                </button>

                              </div>
                            </div>
                          </div>
                          <div class="icon"><svg width=" 24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path d="m12.82 5.58-.82.822-.824-.824a5.375 5.375 0 1 0-7.601 7.602l7.895 7.895a.75.75 0 0 0 1.06 0l7.902-7.897a5.376 5.376 0 0 0-.001-7.599 5.38 5.38 0 0 0-7.611 0Z" fill="#662d91"></path>
                          </svg>
                        </div>
                        <h4><h6 class="mb-0 ">{{$prepurchasepackagesdata->name}}</h6></h4>
                        <div class="content">
                        </div>  


                        <h5>${{$prepurchasepackagesdata->price}}</h5>
                        <div class="row mt-2 mb-0 pb-0">
                            <div class="col-11">
                                        <div class="form-group mb-0">
                                                <div class="input-group bg-gray-200">
                                                    <input id="copy-link-{{$prepurchasepackagesdata->user_package_id}}" class="form-control" value="<?php echo isset($prepurchasepackagesdata->package_code) && !empty($prepurchasepackagesdata->package_code) ? $prepurchasepackagesdata->package_code : ''; ?>" type="text" disabled="">

                                                </div>

                                        </div>

                            </div>            
                              <div class="col-1 mt-2 p-0 cursor-pointer" onclick="copyToClipboard('copy-link-{{$prepurchasepackagesdata->user_package_id}}')" ><i class="fa-lg fi fi_copy text-primary" aria-hidden="true"></i></div>       
                            

                            
                        </div>
                        <p></p>
                        <hr class="horizontal dark">
                        <div class="row">
                          <div class="col-6 ">
                            <p class="text-xs font-weight-bold text-dark mt-0 mb-0">Client Assigned : @if(isset($prepurchasepackagesdata->user_id) && !empty($prepurchasepackagesdata->user_id)) <span class="badge bg-success">Yes</span> @else <span class="badge bg-danger">No</span> @endif</p>
                            <!-- <p class="font-weight-bold text-primary text-xs mt-2 mb-0">Shared To Members : {{$prepurchasepackagesdata->memberscount}}</p> -->

                          </div>

                          <div class="col-6 text-end">
                            <p class="text-xs mb-0 mt-0">{{date('d.m.Y',strtotime($prepurchasepackagesdata->created_at))}}</p>
                            <p class="text-primary text-xs font-weight-bold mb-0 mt-2">Purchased date</p>
                          </div>
                        </div>







                      </div>
                    </div>
                    @endforeach





                  </div>



                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>

    @endif


    @if(isset($prepurchaseqrpackages) && count($prepurchaseqrpackages)>0)
    <hr class="horizontal dark">
    <?php $hasprepurchaseqrpackages=1;?>
    <h5 class="p-2 ps-4">Memorial QR Codes ({{count($prepurchaseqrpackages)}})</h5>
    <section class="pricing-box-con" style="padding-top:0px !important">
      <div class="container">
        <div class="row">

          <div class="col-md-12">
            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

              <!-- tab 1 content start-->
              <div class="tab-pane fade show active" id="nav-ConnectionContent" data-toggle="tab-content">
                <div class="pricing-box-div">
                  <div class="row">

                    @foreach ($prepurchaseqrpackages as $rkey => $prepurchaseqrpackagesdata)
                    <div class="col-md-4">

                      <div class="pricing-box bg-white-100 shadow-lg">

                        <div class="d-flex">
                          <div class="ms-auto">
                            <div class="dropdown">
                              <button onclick="ShowPackageDetails({{$prepurchaseqrpackagesdata->user_package_id}})" class="btn-lg btn btn-link text-secondary ps-0 p-0"  >
                                <!-- <i class="fa fa-share-alt text-lg text-alternate-primary" aria-hidden="true"></i> -->
                              </button>

                            </div>
                          </div>
                        </div>
                        <div class="icon"><svg width=" 24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                          <path d="m12.82 5.58-.82.822-.824-.824a5.375 5.375 0 1 0-7.601 7.602l7.895 7.895a.75.75 0 0 0 1.06 0l7.902-7.897a5.376 5.376 0 0 0-.001-7.599 5.38 5.38 0 0 0-7.611 0Z" fill="#662d91"></path>
                        </svg>
                      </div>
                      <h4><h6 class="mb-0 ">{{$prepurchaseqrpackagesdata->name}}</h6></h4>
                      <div class="content">
                      </div>  


                      <h5>${{$prepurchaseqrpackagesdata->price}}</h5>
                      <div class="row mt-2 mb-0 pb-0">
                            <div class="col-11">
                                        <div class="form-group mb-0">
                                                <div class="input-group bg-gray-200">
                                                    <input id="copy-link-{{$prepurchaseqrpackagesdata->user_package_id}}" class="form-control" value="<?php echo isset($prepurchaseqrpackagesdata->package_code) && !empty($prepurchaseqrpackagesdata->package_code) ? $prepurchaseqrpackagesdata->package_code : ''; ?>" type="text" disabled="">

                                                </div>

                                        </div>

                            </div>            
                              <div class="col-1 mt-2 p-0 cursor-pointer" onclick="copyToClipboard('copy-link-{{$prepurchaseqrpackagesdata->user_package_id}}')" ><i class="fa-lg fi fi_copy text-primary" aria-hidden="true"></i></div>       
                            

                            
                      </div>
                      <p></p>
                      <hr class="horizontal dark">
                      <div class="row">
                        <div class="col-6 ">
                          <p class="text-xs font-weight-bold text-dark mt-0 mb-0">Client Assigned : @if(isset($prepurchaseqrpackagesdata->user_id) && !empty($prepurchaseqrpackagesdata->user_id)) <span class="badge bg-success">Yes</span> @else <span class="badge bg-danger">No</span> @endif</p>
                          <!-- <p class="font-weight-bold text-primary text-xs mt-2 mb-0">Shared To Members : {{$prepurchaseqrpackagesdata->memberscount}}</p> -->
                          
                        </div>

                        <div class="col-6 text-end">
                          <p class="text-xs mb-0 mt-0">{{date('d.m.Y',strtotime($prepurchaseqrpackagesdata->created_at))}}</p>
                          <p class="text-primary text-xs font-weight-bold mb-0 mt-2">Purchased date</p>
                        </div>
                      </div>







                    </div>
                  </div>
                  @endforeach





                </div>



              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>

  @endif

  @if(isset($prepurchaseheirloompackages) && count($prepurchaseheirloompackages)>0)
  <hr class="horizontal dark">
  
  <?php $hasprepurchaseheirloompackages=1;?>
  <h5 class="p-2 ps-4 ">Heirloom Keepsake ({{count($prepurchaseheirloompackages)}})</h5>
  <section class="pricing-box-con" style="padding-top:0px !important">
    <div class="container">
      <div class="row">

        <div class="col-md-12">
          <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

            <!-- tab 1 content start-->
            <div class="tab-pane fade show active" id="nav-ConnectionContent" data-toggle="tab-content">
              <div class="pricing-box-div">
                <div class="row">

                  @foreach ($prepurchaseheirloompackages as $rkey => $prepurchaseheirloompackagesdata)
                  <div class="col-md-4">

                    <div class="pricing-box bg-white-100 shadow-lg">

                      <div class="d-flex">
                        <div class="ms-auto">
                          <div class="dropdown">
                            <button onclick="ShowPackageDetails({{$prepurchaseheirloompackagesdata->user_package_id}})" class="btn-lg btn btn-link text-secondary ps-0 p-0"  >
                              <!-- <i class="fa fa-share-alt text-lg text-alternate-primary" aria-hidden="true"></i> -->
                            </button>

                          </div>
                        </div>
                      </div>
                      <div class="icon"><svg width=" 24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path d="m12.82 5.58-.82.822-.824-.824a5.375 5.375 0 1 0-7.601 7.602l7.895 7.895a.75.75 0 0 0 1.06 0l7.902-7.897a5.376 5.376 0 0 0-.001-7.599 5.38 5.38 0 0 0-7.611 0Z" fill="#662d91"></path>
                      </svg>
                    </div>
                    <h4><h6 class="mb-0 ">{{$prepurchaseheirloompackagesdata->name}}</h6></h4>
                    <div class="content">
                    </div>  


                    <h5>${{$prepurchaseheirloompackagesdata->price}}</h5>
                    <div class="row mt-2 mb-0 pb-0">
                            <div class="col-11">
                                        <div class="form-group mb-0">
                                                <div class="input-group bg-gray-200">
                                                    <input id="copy-link-{{$prepurchaseheirloompackagesdata->user_package_id}}" class="form-control" value="<?php echo isset($prepurchaseheirloompackagesdata->package_code) && !empty($prepurchaseheirloompackagesdata->package_code) ? $prepurchaseheirloompackagesdata->package_code : ''; ?>" type="text" disabled="">

                                                </div>

                                        </div>

                            </div>            
                              <div class="col-1 mt-2 p-0 cursor-pointer" onclick="copyToClipboard('copy-link-{{$prepurchaseheirloompackagesdata->user_package_id}}')" ><i class="fa-lg fi fi_copy text-primary" aria-hidden="true"></i></div>       
                            

                            
                        </div>
                    <p></p>
                    <hr class="horizontal dark">
                    <div class="row">
                      <div class="col-6 ">
                        <p class="text-xs font-weight-bold text-dark mt-0 mb-0">Client Assigned : @if(isset($prepurchaseheirloompackagesdata->user_id) && !empty($prepurchaseheirloompackagesdata->user_id)) <span class="badge bg-success">Yes</span> @else <span class="badge bg-danger">No</span> @endif</p>
                        <!-- <p class="font-weight-bold text-primary text-xs mt-2 mb-0">Shared To Members : {{$prepurchaseheirloompackagesdata->memberscount}}</p> -->

                      </div>

                      <div class="col-6 text-end">
                        <p class="text-xs mb-0 mt-0">{{date('d.m.Y',strtotime($prepurchaseheirloompackagesdata->created_at))}}</p>
                        <p class="text-primary text-xs font-weight-bold mb-0 mt-2">Purchased date</p>
                      </div>
                    </div>





                    

                  </div>
                </div>
                @endforeach





              </div>



            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>

@endif

</div>
</div>
</div>
<div class="modal fade" id="PackagesDetails" tabindex="-1" role="dialog" aria-labelledby="PackagesDetailsLabel" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-centered modal-md" role="document">
    <div class="modal-content p-0" >
      <div class="modal-body p-0" id="PackagesDetailsBody">


      </div>
    </div>  
  </div>
</div>
<script type="text/javascript">

  var hasprepurchasepackages="{{$hasprepurchasepackages}}";
  if(hasprepurchasepackages<=0)
  {
    OpenPlanWarning();
  }
  function OpenPlanWarning()
  {
    swal({
      text: "You don't have any Pre Purchase Packages ",
      icon: "warning",
      buttons: {
        confirm: {
          text: 'Request Packages',
          className: 'text-white bg-primary'
        }

      },
    }).then((will) => {

      if (will)
      {
        window.location="{{route('member.request-packages')}}";
      }
      else 
      {
        window.location="{{route('member.request-packages')}}";
      }

    });
  }


  function ShowPackageDetails(user_package_id)
  {

    $.ajax({
      url: "{{route('member.shared-packages-details')}}",
      dataType: 'json',
      type: 'GET',
      data: {
        user_package_id:user_package_id
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      beforeSend: function() {
        jQuery(document).find('.loadingOverlay').show();

        jQuery(document).find('.loader').show();
      },
      success: function(response) {

        if(response.status=='success')
        {
          $("#PackagesDetailsBody").html(response.html);

          $('#PackagesDetails').modal('show');



        }
      },
        error: function(xhr) { // if error occured
          alert("Error occured.please try again");
        },
        complete: function() {

          $('.select2').select2();
          jQuery(document).find('.loadingOverlay').hide();
          jQuery(document).find('.loader').hide();


        },
      });

  }
  function HideCouponDetails()
  {
    $('#PackagesDetails').modal('hide');
  }

  function copyToClipboarddata(text) {
        var sampleTextarea = document.createElement("textarea");
        document.body.appendChild(sampleTextarea);
        sampleTextarea.value = text; //save main text in it
        sampleTextarea.select(); //select textarea contenrs
        document.execCommand("copy");
        document.body.removeChild(sampleTextarea);
    }

    function copyToClipboard(eleme) {
        var copyText = document.getElementById(eleme);
        copyToClipboarddata(copyText.value);
    }
</script>

@stop
