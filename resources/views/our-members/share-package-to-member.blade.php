<div class="card h-100">
  <div class="card-header pb-0">
    <h5 class="text-sm text-primary mb-0">Share Package To Members</h5>
    <p class="text-xs pt-0 mt-0">
      Share Packages to Below Member's to get Utilized By Clients
      <hr>
    </p>
  </div>
   <form method="POST" action="{{route('member.shared-packages-details.store')}}">
    <input type="hidden" name="user_package_id" value="{{$user_package_id}}">
    
    <div class="card-body p-3">
  	@if(isset($members) && !empty($members))
  	<div class="row">
  	@foreach($members as $membersdata)
  		<div class="col-md-6 ">
		<div class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row @if(isset($sharedpackages) && in_array($membersdata->id,$sharedpackages)) bg-blue-pale @endif">
		<i class="fi fi_person text-dark " ></i>
		<h6 class="mb-0 px-2 text-sm">{{$membersdata->first_name}} {{$membersdata->last_name}}</h6>

		@if(isset($sharedpackages) && in_array($membersdata->id,$sharedpackages))
		<i class="fa fa-check-circle-o text-primary p-1" aria-hidden="true"></i>
        <span class="text-sm text-dark"></span>
		@else
		<div class="form-check">
                  <input class="form-check-input" name="members[]" type="checkbox" value="{{$membersdata->id}}">
                  <p class="text-sm text-dark" for="responsibility_acknowledge"></p>
                </div>
		
		@endif
		</div>
		
		</div>
  	@endforeach
  </div>
  	@endif
  </div>

  <div class="card-footer  d-flex">
	<div class="ms-auto">
	    <button type="button" class="submit-button btn bg-primary btn-xs " onclick="HideCouponDetails()">Share Packages <i class="fa fa-share text-white mx-2" ></i> </button>
	    <button type="button" class="btn bg-danger btn-xs text-white" onclick="HideCouponDetails()" >Cancel</button>
	</div>
	</div>
	</form>
</div>
