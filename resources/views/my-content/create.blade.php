@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Create Messages')

@section('headercommon')
<x-main.header icon="video" title="What would you like to do today?" subtitle="To get started, choose the type of content you’d like to create" />
@endsection

@section('content')
<div class="row mb-2">
      <!-- Create a Personalised Message -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('my-messages.create')}}?type=personalised-content" class="link-handle" class="text-primary icon-move-right">
      <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">
            <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                    <g id="Group_1831" data-name="Group 1831" transform="translate(0 0.308)">
                      <rect id="Rectangle_538" data-name="Rectangle 538" width="54.083" height="55" rx="16" transform="translate(0 -0.308)" fill="#f3eef6"></rect>
                      <path id="Path_3771" data-name="Path 3771" d="M18.616,18.446A3.857,3.857,0,0,1,14.759,22.3h-8.9A3.857,3.857,0,0,1,2,18.446V8.357A3.857,3.857,0,0,1,5.857,4.5h8.9a3.857,3.857,0,0,1,3.857,3.857ZM25.455,6.153a1.187,1.187,0,0,1,.282.769V19.881a1.187,1.187,0,0,1-1.956.9L19.8,17.4v-8l3.978-3.381a1.187,1.187,0,0,1,1.673.135Z" transform="translate(13.173 13.79)" fill="#662d91"></path>
                    </g>
                  </svg>
                </div>
              <div class="description ps-5">
                <h5>Create a Personalised Message</h5>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>
            </div>
          </div>
        </a>
      </div>
      <!-- Create QR Code Content -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('my-messages.create')}}?type=qrcode-content" class="link-handle" class="text-primary icon-move-right">
      <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">
              <div class="icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1855" data-name="Group 1855" transform="translate(-17175 -14322)">
                    <rect id="Rectangle_654" data-name="Rectangle 654" width="54.083" height="55" rx="16" transform="translate(17175 14322)" fill="#f3eef6"/>
                    <g id="Group_1852" data-name="Group 1852" transform="translate(17189 14337)">
                      <path id="Path_3906" data-name="Path 3906" d="M8.889,6H6V8.889H8.889Z" transform="translate(-1.667 -1.667)" fill="#662d91"/>
                      <path id="Path_3907" data-name="Path 3907" d="M3,6.611A3.611,3.611,0,0,1,6.611,3h4.333a3.611,3.611,0,0,1,3.611,3.611v4.333a3.611,3.611,0,0,1-3.611,3.611H6.611A3.611,3.611,0,0,1,3,10.945Zm3.611-.722a.722.722,0,0,0-.722.722v4.333a.722.722,0,0,0,.722.722h4.333a.722.722,0,0,0,.722-.722V6.611a.722.722,0,0,0-.722-.722Zm.722,15.889h2.889v2.889H7.333Z" transform="translate(-3 -3)" fill="#662d91"/>
                      <path id="Path_3908" data-name="Path 3908" d="M3,19.722a3.611,3.611,0,0,1,3.611-3.611h4.333a3.611,3.611,0,0,1,3.611,3.611v4.333a3.611,3.611,0,0,1-3.611,3.611H6.611A3.611,3.611,0,0,1,3,24.056ZM6.611,19a.722.722,0,0,0-.722.722v4.333a.722.722,0,0,0,.722.722h4.333a.722.722,0,0,0,.722-.722V19.722A.722.722,0,0,0,10.945,19ZM24.667,6H21.778V8.889h2.889Z" transform="translate(-3 -1.667)" fill="#662d91"/>
                      <path id="Path_3909" data-name="Path 3909" d="M16.611,3A3.611,3.611,0,0,0,13,6.611v4.333a3.611,3.611,0,0,0,3.611,3.611h4.333a3.611,3.611,0,0,0,3.611-3.611V6.611A3.611,3.611,0,0,0,20.945,3Zm-.722,3.611a.722.722,0,0,1,.722-.722h4.333a.722.722,0,0,1,.722.722v4.333a.722.722,0,0,1-.722.722H16.611a.722.722,0,0,1-.722-.722ZM13,17.445h3.972v3.972H13Zm7.583,3.972H16.972v3.611H13V29h3.972V25.028h3.611V29h3.972V25.028H20.583Zm0,0V17.445h3.972v3.972Z" transform="translate(1.445 -3)" fill="#662d91"/>
                    </g>
                  </g>
                </svg>
              </div>
              <div class="description ps-5">
                <h5>Create QR Code Content</h5>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>
            </div>
          </div>
        </a>
      </div>
  <!-- Create an Heirloom Keepsake -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('my-messages.create')}}?type=heirloom-content" class="link-handle" class="text-primary icon-move-right">
      <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">
              <div class="icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1854" data-name="Group 1854" transform="translate(-17101 -14322)">
                    <rect id="Rectangle_655" data-name="Rectangle 655" width="54.083" height="55" rx="16" transform="translate(17101 14322)" fill="#f3eef6"/>
                    <path id="Path_3905" data-name="Path 3905" d="M14.917,17.89v13H9.139A4.7,4.7,0,0,1,4.444,26.2V17.89Zm12.639,0V26.2a4.7,4.7,0,0,1-4.695,4.695H17.084v-13ZM19.611,2a4.695,4.695,0,0,1,3.955,7.225l3.629,0A1.676,1.676,0,0,1,29,10.728V14.94a1.676,1.676,0,0,1-1.806,1.505H17.084V9.223H14.917v7.221H4.806A1.676,1.676,0,0,1,3,14.94V10.728A1.676,1.676,0,0,1,4.806,9.223l3.629,0A4.695,4.695,0,0,1,16,3.7,4.68,4.68,0,0,1,19.611,2ZM12.389,4.167a2.528,2.528,0,0,0-.208,5.047l.208.009h2.528V6.695l-.009-.208A2.528,2.528,0,0,0,12.389,4.167Zm7.222,0a2.528,2.528,0,0,0-2.528,2.528V9.223h2.528a2.528,2.528,0,1,0,0-5.056Z" transform="translate(17112 14333.001)" fill="#662d91"/>
                  </g>
                </svg>
              </div>
              <div class="description ps-5">
            <h5>Create an Heirloom Keepsake</h5>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>



@endsection