@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Content')

@section('headercommon')
<x-main.header 
  icon="library" 
  title="My Content" 
  subtitle="Select the content style to manage your recipient, delivery date, authorisation and delivery style preferences."
  button
  buttonText="Create new content"
  buttonIcon="add"
  data-route="{{route('create-content')}}"
/>
@endsection

@section('content')
<div>
  <div class="row mt-4">
    <!-- All My Content -->
    <div class="col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
      <a data-route="{{route('my-messages.index')}}?type=all-content" class="link-handle" class="text-primary icon-move-right">
        <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
          <div class="info-horizontal">
            <div class="icon">
              <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                <g id="Group_1837" data-name="Group 1837" transform="translate(-16570.999 -13976)">
                  <rect id="Rectangle_538" data-name="Rectangle 538" width="54.083" height="55" rx="16" transform="translate(16571 13976)" fill="#f3eef6"></rect>
                  <path id="Path_3852" data-name="Path 3852" d="M6.667,3a2,2,0,0,1,2,2V25a2,2,0,0,1-2,2H4a2,2,0,0,1-2-2V5A2,2,0,0,1,4,3Zm8,0a2,2,0,0,1,2,2V25a2,2,0,0,1-2,2H12a2,2,0,0,1-2-2V5a2,2,0,0,1,2-2Zm9.709,4.166L28.663,23.86a2,2,0,0,1-1.44,2.435l-2.5.64a2,2,0,0,1-2.435-1.44L18,8.806a2,2,0,0,1,1.44-2.435l2.5-.644a2,2,0,0,1,2.435,1.44Z" transform="translate(16583 13988)" fill="#662d91"></path>
                </g>
              </svg>
            </div>
            <div class="description ps-5">
              <h5>All My Content</h5>
              <span class="text-primary icon-move-right">
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
              </span>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!-- My Personalised Messages -->
    <div class="col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
      <a data-route="{{route('my-messages.index')}}?type=personalised-content" class="link-handle" class="text-primary icon-move-right">
        <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
          <div class="info-horizontal">
            <div class="icon">
              <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                <g id="Group_1831" data-name="Group 1831" transform="translate(0 0.308)">
                  <rect id="Rectangle_538" data-name="Rectangle 538" width="54.083" height="55" rx="16" transform="translate(0 -0.308)" fill="#f3eef6" />
                  <path id="Path_3771" data-name="Path 3771" d="M18.616,18.446A3.857,3.857,0,0,1,14.759,22.3h-8.9A3.857,3.857,0,0,1,2,18.446V8.357A3.857,3.857,0,0,1,5.857,4.5h8.9a3.857,3.857,0,0,1,3.857,3.857ZM25.455,6.153a1.187,1.187,0,0,1,.282.769V19.881a1.187,1.187,0,0,1-1.956.9L19.8,17.4v-8l3.978-3.381a1.187,1.187,0,0,1,1.673.135Z" transform="translate(13.173 13.79)" fill="#662d91" />
                </g>
              </svg>
            </div>
            <div class="description ps-5">
              <h5>My Personalised Messages</h5>
              <span class="text-primary icon-move-right">
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
              </span>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!-- My QR Code Content -->
    <div class="col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
      <a data-route="{{route('my-messages.index')}}?type=qrcode-content" class="link-handle" class="text-primary icon-move-right">
        <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
          <div class="info-horizontal">
            <div class="icon">
              <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                <g id="Group_1855" data-name="Group 1855" transform="translate(-17175 -14322)">
                  <rect id="Rectangle_654" data-name="Rectangle 654" width="54.083" height="55" rx="16" transform="translate(17175 14322)" fill="#f3eef6"></rect>
                  <g id="Group_1852" data-name="Group 1852" transform="translate(17189 14337)">
                    <path id="Path_3906" data-name="Path 3906" d="M8.889,6H6V8.889H8.889Z" transform="translate(-1.667 -1.667)" fill="#662d91"></path>
                    <path id="Path_3907" data-name="Path 3907" d="M3,6.611A3.611,3.611,0,0,1,6.611,3h4.333a3.611,3.611,0,0,1,3.611,3.611v4.333a3.611,3.611,0,0,1-3.611,3.611H6.611A3.611,3.611,0,0,1,3,10.945Zm3.611-.722a.722.722,0,0,0-.722.722v4.333a.722.722,0,0,0,.722.722h4.333a.722.722,0,0,0,.722-.722V6.611a.722.722,0,0,0-.722-.722Zm.722,15.889h2.889v2.889H7.333Z" transform="translate(-3 -3)" fill="#662d91"></path>
                    <path id="Path_3908" data-name="Path 3908" d="M3,19.722a3.611,3.611,0,0,1,3.611-3.611h4.333a3.611,3.611,0,0,1,3.611,3.611v4.333a3.611,3.611,0,0,1-3.611,3.611H6.611A3.611,3.611,0,0,1,3,24.056ZM6.611,19a.722.722,0,0,0-.722.722v4.333a.722.722,0,0,0,.722.722h4.333a.722.722,0,0,0,.722-.722V19.722A.722.722,0,0,0,10.945,19ZM24.667,6H21.778V8.889h2.889Z" transform="translate(-3 -1.667)" fill="#662d91"></path>
                    <path id="Path_3909" data-name="Path 3909" d="M16.611,3A3.611,3.611,0,0,0,13,6.611v4.333a3.611,3.611,0,0,0,3.611,3.611h4.333a3.611,3.611,0,0,0,3.611-3.611V6.611A3.611,3.611,0,0,0,20.945,3Zm-.722,3.611a.722.722,0,0,1,.722-.722h4.333a.722.722,0,0,1,.722.722v4.333a.722.722,0,0,1-.722.722H16.611a.722.722,0,0,1-.722-.722ZM13,17.445h3.972v3.972H13Zm7.583,3.972H16.972v3.611H13V29h3.972V25.028h3.611V29h3.972V25.028H20.583Zm0,0V17.445h3.972v3.972Z" transform="translate(1.445 -3)" fill="#662d91"></path>
                  </g>
                </g>
              </svg>
            </div>
            <div class="description ps-5">
              <h5>My QR Code Content</h5>
              <span class="text-primary icon-move-right">
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
              </span>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!-- My Heirloom Keepsake Content -->
    <div class="col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
      <a data-route="{{route('my-messages.index')}}?type=heirloom-content" class="link-handle" class="text-primary icon-move-right">
        <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
          <div class="info-horizontal">
            <div class="icon">
              <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                <g id="Group_1854" data-name="Group 1854" transform="translate(-17101 -14322)">
                  <rect id="Rectangle_655" data-name="Rectangle 655" width="54.083" height="55" rx="16" transform="translate(17101 14322)" fill="#f3eef6" />
                  <path id="Path_3905" data-name="Path 3905" d="M14.917,17.89v13H9.139A4.7,4.7,0,0,1,4.444,26.2V17.89Zm12.639,0V26.2a4.7,4.7,0,0,1-4.695,4.695H17.084v-13ZM19.611,2a4.695,4.695,0,0,1,3.955,7.225l3.629,0A1.676,1.676,0,0,1,29,10.728V14.94a1.676,1.676,0,0,1-1.806,1.505H17.084V9.223H14.917v7.221H4.806A1.676,1.676,0,0,1,3,14.94V10.728A1.676,1.676,0,0,1,4.806,9.223l3.629,0A4.695,4.695,0,0,1,16,3.7,4.68,4.68,0,0,1,19.611,2ZM12.389,4.167a2.528,2.528,0,0,0-.208,5.047l.208.009h2.528V6.695l-.009-.208A2.528,2.528,0,0,0,12.389,4.167Zm7.222,0a2.528,2.528,0,0,0-2.528,2.528V9.223h2.528a2.528,2.528,0,1,0,0-5.056Z" transform="translate(17112 14333.001)" fill="#662d91" />
                </g>
              </svg>
            </div>
            <div class="description ps-5">
              <h5>My Heirloom Keepsake Content</h5>
              <span class="text-primary icon-move-right">
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
              </span>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>
  <!-- COMING SOON LINKS -->
  <div class="row mt-4">
    <div class="col-12">
      <div class="card mb-4">
        <div class="card-header pb-0 p-3">
          <h4 class="mb-1">Coming Soon</h4>
        </div>
        <div class="card-body p-3">
          <div class="row">
            <!-- My Memorialisation Page Content -->
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-3 mt-lg-0 mt-4 mb-3">
              <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
                <div class="info-horizontal">
                  <div class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                      <g id="Group_1858" data-name="Group 1858" transform="translate(-16153 -13649)">
                        <rect id="Rectangle_656" data-name="Rectangle 656" width="54.083" height="55" rx="16" transform="translate(16153 13649)" fill="#eeeff0" />
                        <g id="Group_1857" data-name="Group 1857" transform="translate(16166 13665)">
                          <path id="Path_3916" data-name="Path 3916" d="M2,6.881A2.881,2.881,0,0,1,4.881,4H6.322V27.048H4.881A2.881,2.881,0,0,1,2,24.167Zm20.167,3.6H16.405a.72.72,0,0,0-.72.72v1.441a.72.72,0,0,0,.72.72h5.762a.72.72,0,0,0,.72-.72V11.2A.72.72,0,0,0,22.167,10.482Z" transform="translate(-2 -4)" fill="#8b9197" />
                          <path id="Path_3917" data-name="Path 3917" d="M6.5,27.048H25.947a2.881,2.881,0,0,0,2.881-2.881V6.881A2.881,2.881,0,0,0,25.947,4H6.5ZM14.423,8.322h5.762A2.881,2.881,0,0,1,23.066,11.2v1.441a2.881,2.881,0,0,1-2.881,2.881H14.423a2.881,2.881,0,0,1-2.881-2.881V11.2A2.881,2.881,0,0,1,14.423,8.322Z" transform="translate(-0.018 -4)" fill="#8b9197" />
                        </g>
                      </g>
                    </svg>
                  </div>
                  <div class="description ps-5">
                    <h5>My Memorialisation Page Content</h5>
                    <p>Coming soon</p>
                  </div>
                </div>
              </div>
            </div>
            <!-- My Funeral Content -->
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-3 mt-lg-0 mt-4 mb-3">
              <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
                <div class="info-horizontal">
                  <div class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                      <g id="Group_1863" data-name="Group 1863" transform="translate(-16003 -13649)">
                        <rect id="Rectangle_656" data-name="Rectangle 656" width="54.083" height="55" rx="16" transform="translate(16003 13649)" fill="#eeeff0" />
                        <g id="Group_1856" data-name="Group 1856" transform="translate(16018 13662)">
                          <path id="Path_3914" data-name="Path 3914" d="M12.465,10.107a.977.977,0,0,1-.735.534l-2.095.305,1.515,1.477a.979.979,0,0,1,.281.864l-.359,2.086,1.873-.985a.977.977,0,0,1,.91,0l1.873.985-.359-2.086a.977.977,0,0,1,.281-.864l1.515-1.477-2.093-.305a.977.977,0,0,1-.735-.533L13.4,8.21Z" transform="translate(-1.517 0.736)" fill="#8b9197" />
                          <path id="Path_3915" data-name="Path 3915" d="M4,5.6A3.6,3.6,0,0,1,7.6,2H24.167a3.6,3.6,0,0,1,3.6,3.6V26.129a1.08,1.08,0,0,1-1.08,1.08H6.161A1.441,1.441,0,0,0,7.6,28.65H26.688a1.08,1.08,0,0,1,0,2.161H7.6a3.6,3.6,0,0,1-3.6-3.6Zm9.424,5.918-3.544.514A.977.977,0,0,0,9.337,13.7L11.9,16.2,11.3,19.729a.977.977,0,0,0,1.417,1.031l3.171-1.667,3.169,1.667a.977.977,0,0,0,1.419-1.03L19.867,16.2l2.564-2.5a.977.977,0,0,0-.542-1.667l-3.544-.514L16.762,8.307a.977.977,0,0,0-1.753,0l-1.585,3.212Z" transform="translate(-4 -2)" fill="#8b9197" />
                        </g>
                      </g>
                    </svg>
                  </div>
                  <div class="description ps-5">
                    <h5>My Funeral Content</h5>
                    <p>Coming soon</p>
                  </div>
                </div>
              </div>
            </div>
            <!-- My Timeline -->
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-3 mt-lg-0 mt-4 mb-3">
              <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
                <div class="info-horizontal">
                  <div class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                      <g id="Group_1861" data-name="Group 1861" transform="translate(-16078 -13574)">
                        <rect id="Rectangle_656" data-name="Rectangle 656" width="54.083" height="55" rx="16" transform="translate(16078 13574)" fill="#eeeff0" />
                        <path id="Path_3911" data-name="Path 3911" d="M21.089,17.407a2.521,2.521,0,0,1,2.521,2.521v6.487a2.521,2.521,0,0,1-2.521,2.521H4.523A2.521,2.521,0,0,1,2,26.414V19.928a2.52,2.52,0,0,1,2.521-2.521Zm8.283,2.73V27.85A1.08,1.08,0,0,1,27.22,28l-.009-.147v-7.7a4.326,4.326,0,0,0,2.161-.014Zm-1.08-6.919a2.749,2.749,0,1,1-2.749,2.749A2.749,2.749,0,0,1,28.292,13.218ZM21.082,3A2.52,2.52,0,0,1,23.6,5.518V12a2.521,2.521,0,0,1-2.521,2.521H4.516A2.521,2.521,0,0,1,1.995,12V5.518A2.521,2.521,0,0,1,4.31,3L4.516,3H21.082Zm7.21,0a1.08,1.08,0,0,1,1.07.933l.01.147v7.7a4.327,4.327,0,0,0-2.161-.014V4.082A1.08,1.08,0,0,1,28.292,3Z" transform="translate(16089.005 13586.003)" fill="#8b9197" />
                      </g>
                    </svg>
                  </div>
                  <div class="description ps-5">
                    <h5>My Timeline</h5>
                    <p>Coming soon</p>
                  </div>
                </div>
              </div>
            </div>
            <!-- My Family History -->
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-3 mt-lg-0 mt-4 mb-3">
              <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
                <div class="info-horizontal">
                  <div class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                      <g id="Group_1859" data-name="Group 1859" transform="translate(-16153 -13574)">
                        <rect id="Rectangle_656" data-name="Rectangle 656" width="54.083" height="55" rx="16" transform="translate(16153 13574)" fill="#eeeff0" />
                        <path id="Path_3919" data-name="Path 3919" d="M12.232,4.127a5.762,5.762,0,1,0-8.106,8.106,12.99,12.99,0,0,0,2.38,12.546L3.758,27.527a1.08,1.08,0,1,0,1.527,1.527l2.749-2.749a12.966,12.966,0,0,0,16.745,0l2.749,2.749a1.08,1.08,0,1,0,1.527-1.527l-2.749-2.749a12.99,12.99,0,0,0,2.38-12.546,5.762,5.762,0,1,0-8.106-8.106,13.042,13.042,0,0,0-8.346,0Zm-4.47.035a3.587,3.587,0,0,1,2.374.893,13.025,13.025,0,0,0-5.082,5.082A3.6,3.6,0,0,1,7.762,4.161Zm14.912.893a3.6,3.6,0,0,1,5.082,5.082,13.025,13.025,0,0,0-5.082-5.082ZM15.325,9.2a1.08,1.08,0,0,1,1.08,1.08v6.122h3.961a1.08,1.08,0,1,1,0,2.161H15.325a1.08,1.08,0,0,1-1.08-1.08v-7.2a1.08,1.08,0,0,1,1.08-1.08Z" transform="translate(16164.001 13586.001)" fill="#8b9197" />
                      </g>
                    </svg>
                  </div>
                  <div class="description ps-5">
                    <h5>My Family History</h5>
                    <p>Coming soon</p>
                  </div>
                </div>
              </div>
            </div>
            <!-- My Digital Photo Albums -->
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-3 mt-lg-0 mt-4 mb-3">
              <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
                <div class="info-horizontal">
                  <div class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                      <g id="Group_1860" data-name="Group 1860" transform="translate(-16078 -13649)">
                        <rect id="Rectangle_656" data-name="Rectangle 656" width="54.083" height="55" rx="16" transform="translate(16078 13649)" fill="#eeeff0" />
                        <path id="Path_3918" data-name="Path 3918" d="M19.178,2.5a3.241,3.241,0,0,1,2.795,1.6l1.172,2h2.983a4.682,4.682,0,0,1,4.682,4.682V24.467a4.682,4.682,0,0,1-4.682,4.682H6.682A4.682,4.682,0,0,1,2,24.467V10.782A4.682,4.682,0,0,1,6.682,6.1h3l1.26-2.051A3.241,3.241,0,0,1,13.7,2.5h5.478Zm-2.773,7.919A6.482,6.482,0,1,0,22.888,16.9a6.482,6.482,0,0,0-6.482-6.482Zm0,2.161A4.322,4.322,0,1,1,12.084,16.9,4.322,4.322,0,0,1,16.405,12.582Z" transform="translate(16089 13660.497)" fill="#8b9197" />
                      </g>
                    </svg>
                  </div>
                  <div class="description ps-5">
                    <h5>My Digital Photo Albums</h5>
                    <p>Coming soon</p>
                  </div>
                </div>
              </div>
            </div>
            <!-- My Storybook -->
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-3 mt-lg-0 mt-4 mb-3">
              <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
                <div class="info-horizontal">
                  <div class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                      <g id="Group_1862" data-name="Group 1862" transform="translate(-16003 -13574)">
                        <rect id="Rectangle_656" data-name="Rectangle 656" width="54.083" height="55" rx="16" transform="translate(16003 13574)" fill="#eeeff0" />
                        <path id="Path_3913" data-name="Path 3913" d="M7.6,2A3.6,3.6,0,0,0,4,5.6V27.209a3.6,3.6,0,0,0,3.6,3.6H26.688a1.08,1.08,0,0,0,0-2.161H7.6a1.441,1.441,0,0,1-1.441-1.441H26.688a1.08,1.08,0,0,0,1.08-1.08V5.6a3.6,3.6,0,0,0-3.6-3.6ZM20.566,16.765v.72c0,1.441-1.992,2.521-4.682,2.521S11.2,18.926,11.2,17.486v-.72a1.08,1.08,0,0,1,1.08-1.08h7.2a1.08,1.08,0,0,1,1.08,1.08Zm-2.161-5.049A2.521,2.521,0,1,1,15.884,9.2,2.521,2.521,0,0,1,18.405,11.716Z" transform="translate(16014 13585)" fill="#8b9197" />
                      </g>
                    </svg>
                  </div>
                  <div class="description ps-5">
                    <h5>My Storybook</h5>
                    <p>Coming soon</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection