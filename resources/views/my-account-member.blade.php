@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Content')

@section('headercommon')
<x-main.header icon="contact_card" title="My Account" subtitle="Manage your account details" />
@endsection

@section('content')
<div>
  <div class="row mt-4">
    <div class="col-lg-4 col-md-4 d-flex flex-column mt-3 border-radius-lg">
      <a data-route="{{route('my-account.add-on-features')}}" class="link-handle">
        <div class="card h-100 card-background align-items-start">
          <div class="full-background bg-primary"></div>
          <div class="card-body z-index-3">
          </div>
          <div class="card-footer pb-3 pt-2 z-index-3">
            <h6 class="text-white mb-1">My Codes</h6>
          </div>
          <span class="mask bg-info border-radius-xl z-index-2 opacity-6"></span>
        </div>
      </a>
    </div>
    <div class="col-lg-4 col-md-4 d-flex flex-column mt-3 border-radius-lg">
      <a data-route="{{route('my-account.upgrade-plan')}}" class="link-handle">
        <div class="card h-100 card-background align-items-start">
          <div class="full-background bg-primary"></div>
          <div class="card-body z-index-3">
          </div>
          <div class="card-footer pb-3 pt-2 z-index-3">
            <h6 class="text-white mb-1">Pre-Purchase packages</h6>
          </div>
          <span class="mask bg-info border-radius-xl z-index-2 opacity-6"></span>
        </div>
      </a>
    </div>
    <div class="col-lg-4 col-md-4 d-flex flex-column mt-3 border-radius-lg">
      <a data-route="{{route('my-account.free-up-minutes')}}" class="link-handle">
        <div class="card h-100 card-background align-items-start">
          <div class="full-background bg-primary"></div>
          <div class="card-body z-index-3">
          </div>
          <div class="card-footer pb-3 pt-2 z-index-3">
            <h6 class="text-white mb-1">Billing</h6>
          </div>
          <span class="mask bg-info border-radius-xl z-index-2 opacity-6"></span>
        </div>
      </a>
    </div>
  </div>
</div>
@endsection