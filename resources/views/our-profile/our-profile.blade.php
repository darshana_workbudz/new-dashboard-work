@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Our Profile')

@section('headercommon')
<x-main.header icon="shield_keyhole" title="Our Profile" subtitle="Manage your profile details" />
@endsection

@section('content')
<section class="px-3 py-2">
  <div class="row mt-4">
    <div class="col-lg-6 col-md-6 d-flex flex-column mt-3 border-radius-lg">
      <a data-route="{{route('my-profile.my-basic-info')}}" class="link-handle">
        <div class="card h-100 card-background align-items-start">
          <div class="full-background bg-primary"></div>
          <div class="card-body z-index-3">
          </div>
          <div class="card-footer pb-3 pt-2 z-index-3">
            <h6 class="text-white mb-1">Our Organisation Details</h6>
          </div>
          <span class="mask bg-info border-radius-xl z-index-2 opacity-6"></span>
        </div>
      </a>
    </div>
    <div class="col-lg-6 col-md-6 d-flex flex-column mt-3 border-radius-lg">
      <a data-route="{{route('my-profile.my-change-password')}}" class="link-handle">
        <div class="card h-100 card-background align-items-start">
          <div class="full-background bg-primary"></div>
          <div class="card-body z-index-3">
          </div>
          <div class="card-footer pb-3 pt-2 z-index-3">
            <h6 class="text-white mb-1">Our Password</h6>
          </div>
          <span class="mask bg-info border-radius-xl z-index-2 opacity-6"></span>
        </div>
      </a>
    </div>
  </div>
</section>
@endsection