@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Profile')

@section('headercommon')
<x-main.header icon="person" title="My Profile" subtitle="Here you can change your profile details" />
@endsection

@section('content')
<div>
  <div class="card mt-4" id="basic-info">
    <div class="card-header">
      <h5 class="font-weight-bolder text-sm text-primary"> <a data-route="{{route('my-profile')}}" class="link-handle"><i class="fi fi_chevron_left text-primary" aria-hidden="true"></i></a> &nbsp;Basic Info</h5>
    </div>
    <div class="card-body pt-0">
      <form action="{{route('my-profile.update')}}" method="POST">
        @csrf
        <input type="hidden" name="isorganisation" value="0">
        <div class="row">
          <div class="col-12 col-sm-6">
            <label>Given name <span class="text-danger">*</span></label>
            <input class="form-control @if(isset($error['first_name'][0]) && !empty($error['first_name'][0])) is-invalid @endif" type="text" name="first_name" value="{{$userdata->first_name}}" placeholder="Jane" />
            @if(isset($error['first_name'][0]) && !empty($error['first_name'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['first_name'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 col-sm-6">
            <label>Family name <span class="text-danger">*</span></label>
            <input class="form-control @if(isset($error['last_name'][0]) && !empty($error['last_name'][0])) is-invalid @endif" type="text" name="last_name" value="{{$userdata->last_name}}" placeholder="Smith" />
            @if(isset($error['last_name'][0]) && !empty($error['last_name'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['last_name'][0]}}
            </p>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-sm-6  mt-1">
            <label>Email address <span class="text-danger">*</span></label>
            <input class="form-control" type="text" name="email" value="{{$userdata->email}}"  placeholder="example@email.com" />
            @if(isset($error['email'][0]) && !empty($error['email'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['email'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 col-sm-6  mt-1">
            <div class="row">
              <label>Phone number</label>
              <div class="col-3 mt-3 mt-sm-0 pr-0 mr-0">
                <select name="phone_code" class="phonecodeselect2 form-control" style="width: 100%;">
                  @if(isset($phonecodes) && !empty($phonecodes))
                  @foreach($phonecodes as $data)
                  <option value="{{$data->id}}" data-code="{{$data->iso2}}" @if(isset($userdata->phone_code) && $userdata->phone_code==$data->iso2){{"selected"}}  >+{{$data->phone_code}} {{$data->iso2}} </option>
                  @endforeach
                  @endif

                </select>
              </div>
              <div class="col-9 mt-3 mt-sm-0 pl-0 ml-0 mb-4">
                <input id="phone_number" class="required form-control @if(isset($error['phone_number'][0]) && !empty($error['phone_number'][0])) is-invalid @endif" type="number" name="phone_number" value="{{$userdata->phone_number}}" placeholder="0412345678" />
                @if(isset($error['phone_number'][0]) && !empty($error['phone_number'][0]))
                <p class="form-text text-danger text-xs mb-1">
                  {{$error['phone_number'][0]}}
                </p>
                @endif
              </div>
            </div>
          </div>
        </div>
        <hr class="horizontal dark" />
        <h6 class="font-weight-bolder text-sm mx-auto text-primary mt-4 mb-0 p-0">Some extra details would be great</h6>
        <p class="text-dark text-sm pt-1 ">Given the precious messages you’re entrusting us with, we want to make sure we can always reach you</p>
        <div class="row mt-1">
          <div class="col-12 col-sm-6">
            <label>Date of birth</label>
            <div id="datepicker" class="input-group-alternative date custom-datepicker" data-date-format="dd-mm-yyyy">
              <input required type="text" id="dob-date" value="<?php echo isset($userdata->dob) && !empty($userdata->dob) ? date('d-m-Y', strtotime($userdata->dob)) : '' ?>" name="dob" class=" date-validation form-control  @if(isset($error['dob'][0]) && !empty($error['dob'][0])) is-invalid @endif" placeholder="dd-mm-yyyy" />
              <span class="input-group-addon">
              </span>
            </div>
            @if(isset($error['dob'][0]) && !empty($error['dob'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['dob'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 col-sm-6"></div>
          <div class="col-12 col-sm-6">
            <label>Reason I am creating my account</label>
            <select name="reason" id="reason" class="select2 form-control" style="width: 100%;">
              @if(isset($reason) && count($reason)>0)
              @foreach($reason as $reasondata)
              <option value="{{$reasondata->reason_id}}" @if(isset($userdata) && $userdata->reason==$reasondata->reason_id) {{'selected'}} @endif>{{$reasondata->description}}</option>
              @endforeach
              @endif
            </select>
          </div>
          <div class="col-12 col-sm-6">
            <label>My status</label>
            <select name="mystatus" id="mystatus" class="select2 form-control" style="width: 100%;">
              @if(isset($mystatus) && count($mystatus)>0)
              @foreach($mystatus as $mystatusdata)
              <option value="{{$mystatusdata->status_id}}" @if(isset($userdata) && $userdata->mystatus==$mystatusdata->status_id) {{'selected'}} @endif>{{$mystatusdata->description}}</option>
              @endforeach
              @endif
            </select>
          </div>
          <div class="col-12 col-sm-6 other_reason" @if($userdata->reason=='7') style="display: block;" @else style="display: none;" @endif >
            <label>Other (Reason for creating the Account)</label>
            <textarea class="form-control @if(isset($error['other_reason'][0]) && !empty($error['other_reason'][0])) is-invalid @endif" type="text" name="other_reason"> {{$userdata->other_reason}}</textarea>
            @if(isset($error['other_reason'][0]) && !empty($error['other_reason'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['other_reason'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 col-sm-6 other_mystatus" @if($userdata->mystatus=='7') style="display: block;" @else style="display: none;" @endif >
            <label>Other Option (My Status)</label>
            <textarea class="form-control @if(isset($error['other_mystatus'][0]) && !empty($error['other_mystatus'][0])) is-invalid @endif" type="text" name="other_mystatus" col="2"> {{$userdata->other_mystatus}}</textarea>
            @if(isset($error['other_mystatus'][0]) && !empty($error['other_mystatus'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['other_mystatus'][0]}}
            </p>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-sm-6 field-wrapper address-lookup mt-1">
            <label> Your address </label>
            <input class="form-control address-lookup__field" type="text" name="jls" autocomplete="off" placeholder="&nbsp;">
            <a href="javascript:;" class="mt-1 show-manual text-xs float-end text-secondary"><u>Manually enter my address</u></a>
          </div>
        </div>
        <div class="row show-manual-div" style="display: none;">
          <input class="form-control" id="manual-address" name="manual" type="hidden" value="0">
          <input class="form-control" name="city" type="hidden">
          <input class="form-control" name="county" type="hidden">
          <div class="col-12 col-sm-6">
            <label>Address Line 1 </label>
            <input class="form-control @if(isset($error['address_line_1'][0]) && !empty($error['address_line_1'][0])) is-invalid @endif" type="text" name="address_line_1" value="{{$userdata->address_1}}">
            @if(isset($error['address_line_1'][0]) && !empty($error['address_line_1'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['address_line_1'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 col-sm-6">
            <label>Address Line 2</label>
            <input class="form-control @if(isset($error['address_line_2'][0]) && !empty($error['address_line_2'][0])) is-invalid @endif" type="text" name="address_line_2" value="{{$userdata->address_2}}">
            @if(isset($error['address_line_2'][0]) && !empty($error['address_line_2'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['address_line_2'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 col-sm-6">
            <label>Postcode</label>
            <input class="form-control @if(isset($error['postcode'][0]) && !empty($error['postcode'][0])) is-invalid @endif" name="postcode" type="number" value="{{$userdata->postcode}}">
            @if(isset($error['postcode'][0]) && !empty($error['postcode'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['postcode'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 col-sm-6">
            <label>State</label>
            <input class="form-control @if(isset($error['state'][0]) && !empty($error['state'][0])) is-invalid @endif" name="state" type="text" value="{{$userdata->state}}">
            @if(isset($error['state'][0]) && !empty($error['state'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['state'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 col-sm-6">
            <label>Country</label>
            <select name="country" id="country" class="select2 form-control @if(isset($error['country'][0]) && !empty($error['country'][0])) is-invalid @endif" style="width: 100%;">
              @if(isset($phonecodes) && !empty($phonecodes))
              <option value=""></option>
              @foreach($phonecodes as $data)
              <option value="{{$data->iso2}}" @if(isset($userdata->country) && $userdata->country==$data->iso2){{"selected"}} @endif > {{$data->name}} </option>
              @endforeach
              @endif
            </select>
            @if(isset($error['country'][0]) && !empty($error['country'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['country'][0]}}
          </p>
          @endif
          </div>
        </div>
        <button class="btn bg-primary mb-0 submit-button mb-3" type="button">Save Changes</button>
      </form>
    </div>
  </div>
</div>




<script src="{{asset('assets/js/js-address-lookup.js')}}?v=1.0.3"></script>

<script type="text/javascript">
  var startDate = new Date();
  startDate.setFullYear(startDate.getFullYear() - 100);
  var endDate = new Date();
  endDate.setFullYear(endDate.getFullYear() - 22);

    $('body').find(".custom-datepicker").datepicker({ 
      autoclose: true, 
      todayHighlight: true,
      format: 'dd/mm/yyyy',
      startDate: '01-01-1920',
      endDate: endDate
    });

     $(document).on('change',"#reason" ,function() {
     
      $('.other_reason').hide();

      if($("#reason option:selected").text()=='Other')
      {
        $('.other_reason').show();
      }

    });

     $(document).on('change',"#mystatus" ,function() {
     
      $('.other_mystatus').hide();

      if($("#mystatus option:selected").text()=='Other')
      {
        $('.other_mystatus').show();
      }

    });

     $(document).on('change',"#organisation_role" ,function() {
     
      $('.other_role').hide();

      if($("#organisation_role option:selected").text()=='Other')
      {
        $('.other_role').show();
      }

    });
       $(document).on('click','.show-manual', function(e) {
      e.preventDefault();
        $(document).find('.show-manual-div').show();
        $(document).find('#manual-address').val(1);
    });
  </script>
@endsection