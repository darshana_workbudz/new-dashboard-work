@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Change Your Password')

@section('headercommon')
<x-main.header icon="shield_keyhole" title="Change Your Password" subtitle="Update your Password" />
@endsection
@section('content')
<div>
  <form action="{{route('my-profile.update-password')}}" method="POST">
    @csrf
    <div class="card mt-4" id="password">
      <div class="card-header">
        <h5 class="font-weight-bolder text-sm text-primary"> <a data-route="{{route('my-profile')}}" class="link-handle"><i class="fi fi_chevron_left text-primary" aria-hidden="true"></i></a> &nbsp;Change Password</h5>
      </div>
      <div class="card-body pt-0">
        <label class="form-label">Current password</label><span class="ps-2 text-danger">*</span>
        <div class="form-group">
          <input class="form-control" type="password" name="current_password" placeholder="Current password">
          @if(isset($error['current_password'][0]) && !empty($error['current_password'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['current_password'][0]}}
          </p>
          @endif
        </div>
        <label class="form-label">New password</label><span class="ps-2 text-danger">*</span>
        <div class="form-group">
          <input class="form-control" type="password" name="new_password" placeholder="New password">
          @if(isset($error['new_password'][0]) && !empty($error['new_password'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['new_password'][0]}}
          </p>
          @endif
        </div>
        <label class="form-label">Confirm new password</label><span class="ps-2 text-danger">*</span>
        <div class="form-group">
          <input class="form-control" type="password" name="new_confirm_password" placeholder="Confirm password">
          @if(isset($error['new_confirm_password'][0]) && !empty($error['new_confirm_password'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['new_confirm_password'][0]}}
          </p>
          @endif
        </div>
        <h5 class="mt-5">Password requirements</h5>
        <p class="text-muted mb-2">
          Please follow this guide for a strong password:
        </p>
        <ul class="text-muted ps-4 mb-0 float-start">
          <li>
            <span class="text-sm">One special characters</span>
          </li>
          <li>
            <span class="text-sm">Min 6 characters</span>
          </li>
          <li>
            <span class="text-sm">One number (2 are recommended)</span>
          </li>
          <li>
            <span class="text-sm">Change it often</span>
          </li>
        </ul>
      </div>
      <div class="card-body d-sm-flex pt-0">
        <button class="btn bg-primary mb-0 ms-2 submit-button" type="button" name="button">Save Changes</button>
      </div>
    </div>
  </form>
</div>
@endsection