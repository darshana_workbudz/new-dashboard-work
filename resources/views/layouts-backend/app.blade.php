<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <x-head.analytics/>
  <x-head.metatags/>
  <x-head.favicons/>
  <x-head.styles/>
  <x-head.scripts/>
</head>

<body class="g-sidenav-show  bg-gray-100">
  <x-body.scripts.start/>

  <div class="loadingOverlay"></div>
  <div class="loader loader-6">
    <div class="loader-inner"></div>
  </div>

  <!-- <div class="progress page-progress-bar" style="display: none;">
    <div class="indeterminate"></div>
  </div> -->

  <x-sidebar/>
  <main id="main" class="main-content position-relative bg-gray-100 max-height-vh-100 h-100 refresh-div">
    <x-header/>
    @yield('headercommon')
    <x-main.container>
      @yield('content')
    </x-main.container>
    <x-footer/>
  </main>
  <x-body.scripts.end/>
</body>
</html>
