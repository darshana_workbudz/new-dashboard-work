
@if(isset($old->successmessage) && !empty($old->successmessage))
	<script type="text/javascript">
		swal({
		  title: "Well done!",
		  text: "<?php echo $old->successmessage;?>",
		  icon: "success",
		  button: "Ok",
		});
	</script>
@endif

@if(isset($old->failuremessage) && !empty($old->failuremessage))
	<script type="text/javascript">
		swal({
		  title: "Oops!",
		  text: "<?php echo $old->failuremessage;?>",
		  icon: "warning",
		  button: "Ok",
		});
	</script>
@endif