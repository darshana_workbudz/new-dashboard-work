 <nav class="navbar navbar-main navbar-expand-lg  mt-4 top-1 px-0 mx-4 shadow-none border-radius-xl " id="navbarBlur" >
      <div class="container-fluid py-1 px-3">
        <div class="sidenav-toggler sidenav-toggler-inner d-xl-block d-none me-sm-3">
          <a href="javascript:;" class="nav-link text-body p-0">
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
            </div>
          </a>
        </div>
        <!-- <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="{{route('home')}}"><i class="m-2 fi fi_home cursor-pointer"></i></a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">@yield('page-title')</li>
          </ol>
           <h6 class="font-weight-bolder mb-0">@yield('page-title')</h6>
        </nav> -->
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            <!-- <div class="input-group">
              <span class="input-group-text text-body"><i class="fi fi_search" aria-hidden="true"></i></span>
              <input type="text" class="form-control" placeholder="Type here...">
            </div> -->
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-flex align-items-center">
              <a class="nav-link text-body font-weight-bold px-0">
                <i class="fi fi_person me-sm-1"></i>
                <span class="d-sm-inline d-none">{{ auth()->user()->first_name }} {{ auth()->user()->last_name }} </span>
                @if(auth()->user()->user_role_type)
                @endif
              </a>
            </li>
            <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </a>
            </li>
            <li class="nav-item dropdown px-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Logout">
                  <!-- <i class="fi fi_settings fixed-plugin-button-nav cursor-pointer"></i> -->
                  <i class="m-2 fi fi_sign_out fixed-plugin-button-nav cursor-pointer"></i>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                      </form>
              </a>
              <ul class="dropdown-menu  dropdown-menu-end  px-2 me-sm-n4" aria-labelledby="dropdownMenuButton">
                <li >
                  <a class="dropdown-item border-radius-md" href="javascript:;">
                    
                      
                      <div class="d-flex flex-column justify-content-center" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <h6 class="text-sm font-weight-normal mb-1">
                          <span class="font-weight-bold">Logout<i class="m-2 fi fi_sign_out"></i></span>
                        </h6>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                      </form>
                       
                     
                    </div>
                  </a>
                </li>
                
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>