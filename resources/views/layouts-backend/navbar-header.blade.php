<nav class="navbar navbar-main navbar-expand-lg bg-transparent shadow-none position-absolute px-4 w-100 z-index-2 mt-4">
      <div class="container-fluid py-1">
        <div class="sidenav-toggler sidenav-toggler-inner d-xl-block d-none me-sm-3">
          <a href="javascript:;" class="nav-link text-white p-0">
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line bg-white"></i>
              <i class="sidenav-toggler-line bg-white"></i>
              <i class="sidenav-toggler-line bg-white"></i>
            </div>
          </a>
        </div>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-1 pt-0 ps-2 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="text-white opacity-8" href="{{route('home')}}">Home</a></li>
            <li class="breadcrumb-item text-sm text-white active" aria-current="page">@yield('page-breadcrumb')</li>
          </ol>
          <!-- <h6 class="text-white font-weight-bolder ms-2">@yield('page-title')</h6> -->
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            <!-- <div class="input-group">
              <span class="input-group-text text-white"><i class="text-dark fi fi_search" aria-hidden="true"></i></span>
              <input type="text" class="form-control" placeholder="Type here...">
            </div> -->
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-flex align-items-center">
              <a class="nav-link text-white font-weight-bold px-0">
                <i class="fi fi_person me-sm-1"></i>
                <span class="d-sm-inline d-none ">{{ auth()->user()->first_name }} {{ auth()->user()->last_name }} </span>
                @if(auth()->user()->user_role_type)
                @endif
              </a>
            </li>
            <li class="nav-item d-xl-none ps-3 d-flex align-items-center ">
              <a href="javascript:;" class="nav-link  p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line bg-white"></i>
                  <i class="sidenav-toggler-line bg-white"></i>
                  <i class="sidenav-toggler-line bg-white"></i>
                </div>
              </a>
            </li>
            <li class="nav-item dropdown px-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-white p-0" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Logout">
                  <!-- <i class="fi fi_settings fixed-plugin-button-nav cursor-pointer"></i> -->
                  <i class="m-2 fi fi_sign_out fixed-plugin-button-nav cursor-pointer"></i>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                      </form>
              </a>
              <ul class="dropdown-menu  dropdown-menu-end  px-2 me-sm-n4" aria-labelledby="dropdownMenuButton">
                <li >
                  <a class="dropdown-item border-radius-md" href="javascript:;">
                    
                      
                      <div class="d-flex flex-column justify-content-center" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <h6 class="text-sm font-weight-normal mb-1">
                          <span class="font-weight-bold">Logout<i class="m-2 fi fi_sign_out"></i></span>
                        </h6>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                      </form>
                       
                     
                    </div>
                  </a>
                </li>
                
              </ul>
            </li>
            <li class="nav-item nav-mobile dropdown px-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-white p-0">
                  <!-- <i class="fi fi_settings fixed-plugin-button-nav cursor-pointer"></i> -->
              </a>
              <ul class="dropdown-menu  dropdown-menu-end  px-2 me-sm-n4" aria-labelledby="dropdownMenuButton">
                <li >
                  <a class="dropdown-item border-radius-md" href="javascript:;">
                    
                      
                      <div class="d-flex flex-column justify-content-center" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <h6 class="text-sm font-weight-normal mb-1">
                          <span class="font-weight-bold">Logout<i class="m-2 fi fi_sign_out"></i></span>
                        </h6>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                      </form>
                       
                     
                    </div>
                  </a>
                </li>
                
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>