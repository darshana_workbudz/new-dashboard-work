@include('layouts-backend.alerts')

<?php $pagetype= e($__env->yieldContent('page-type'));?>

    @if(isset($pagetype) && $pagetype=='list-page')

      @include('layouts-backend.navbar-header')

    @else

      @include('layouts-backend.navbar')
      
    @endif
    
    
@yield('headercommon') 

<div class="container-fluid py-4">
  @yield('content')
  @include('layouts-backend.footer')
</div>

<script type="text/javascript">
	PREVIOUSPAGESESSIONURL = '<?php echo Session::get('previouspageurl');?>';

	$('.select2').select2();

	function formatState (state) {
    if (!state.id) {
      return state.text;
    }
    console.log(state);
    var baseUrl = "{{Request::root()}}/assets/img/flag";
    var $state = $(
      '<span><img class="img-flag " /> <span class="text-sm"></span></span>'
    );

     var code=$('.phonecodeselect2 option:selected').data('code');

    // Use .text() instead of HTML string concatenation to avoid script injection issues
    $state.find("span").text(state.text);
    $state.find("img").attr("src", baseUrl + "/" + code + "-32.png");

    return $state;
  };

  function formatState2 (state) {
    if (!state.id) {
      return state.text;
    }
    console.log(state);
    var baseUrl = "{{Request::root()}}/assets/img/flag";
    var $state = $(
      '<span><img class="img-flag " /> <span class="text-sm"></span></span>'
    );

     var code=$('.phonecodeselect1 option:selected').data('code');


    // Use .text() instead of HTML string concatenation to avoid script injection issues
    $state.find("span").text(state.text);
    $state.find("img").attr("src", baseUrl + "/" + code + "-32.png");

    return $state;
  };

  $(".phonecodeselect2").select2({
    
    templateSelection: formatState
  }); 
   $(".phonecodeselect1").select2({
    
    templateSelection: formatState2
  }); 

  $(document).find('.tooltip').remove();
	
</script>