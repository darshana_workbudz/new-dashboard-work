<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 " id="sidenav-main">
  <div class="sidenav-header">
    <i class="fi fi_dismiss p-3 cursor-pointer opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
    <a class="navbar-brand m-0">
      <img src="{{asset('assets/img/evaheld-logo.svg')}}" class="navbar-brand-img h-100" alt="Evaheld logo">
    </a>
  </div>
  <hr class="horizontal dark mt-0">
  <div class="collapse navbar-collapse w-auto h-auto h-100 ps" id="sidenav-collapse-main">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link  link-handle active" data-route="{{route('dashboard')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_home" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">Home</span>
        </a>
      </li>

      @hasrole('Admin')

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('admin.b2c-user-clients')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-circle-08  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">B2C User Clients</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('admin.b2c-partner')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-circle-08  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">B2C Partners</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('admin.b2b-client')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-circle-08  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">B2B Clients</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('list-roles')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-ui-04  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">Manage User roles</span>
        </a>
      </li>


      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('maintenance-dashboard')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-circle-08  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">Generate Coupon Codes</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('maintenance-dashboard')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-atom  text-lg top-0" aria-hidden="true" id="sidenavCardIcon"></i>
          </div>
          <span class="nav-link-text ms-1">QR Codes</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('maintenance-dashboard')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-delivery-fast  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">Hand Delivered Product</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('maintenance-dashboard')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-single-02  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">Our Profile</span>
        </a>
      </li>

      <li class="nav-item">
        <hr class="horizontal dark">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6"> Other Details</h6>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('maintenance-dashboard')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-button-play  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">Ziggeo</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('maintenance-dashboard')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="fi fi_payment" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">Stripe</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('maintenance-dashboard')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="fi fi_send  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">SendInBlue</span>
        </a>
      </li>


      @endhasrole

      @hasrole('Client')

      <!-- <li class="nav-item">
          <a class="nav-link link-handle"  data-route="{{route('my-messages.create')}}">
            <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/icons/create-message.png')}}" style='height: 20px;'>
            </div>
            <span class="nav-link-text ms-1">Create Messages</span>
          </a>
        </li> -->
      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('create-content')}}">
          <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_video" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">Create Content</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('my-content')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_library" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">My Content</span>
        </a>
      </li>


      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('manage-recipients.index')}}">
          <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_people" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">My Recipients</span>
        </a>
      </li>

      <!-- <li class="nav-item">
          <a class="nav-link "  href="{{route('new-pricing-packages')}}">
            <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/icons/create-message.png')}}" style='height: 20px;'>
            </div>
            <span class="nav-link-text ms-1">Our Pricing</span>
          </a>
        </li> -->
      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('my-preferences.index')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_shield_task" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">My Preferences</span>
        </a>
      </li>

      <!-- <li class="nav-item">
          <a class="nav-link  link-handle"  data-route="{{route('maintenance-dashboard')}}">
            <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-atom  text-lg top-0" aria-hidden="true" id="sidenavCardIcon"></i>
            </div>
            <span class="nav-link-text ms-1">My QR Code</span>
          </a>
        </li> -->
      <li class="nav-item">
        <hr class="horizontal dark">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Account Details</h6>
      </li>
      <li class="nav-item">
        <a class="nav-link link-handle" data-route="{{route('my-account')}}" aria-controls="MyAccountTab" role="button">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_contact_card" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">My Account</span>
        </a>
        <div class="collapse" id="MyAccountTab" style="">
          <ul class="nav ms-4 ps-3">
            <li class="nav-item ">
              <a class="nav-link link-handle" data-route="{{route('my-account.add-on-features')}}">
                <span class="sidenav-mini-icon"> K </span>
                <span class="sidenav-normal"> Add-on Features </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link link-handle" data-route="{{route('my-account.upgrade-plan')}}">
                <span class="sidenav-mini-icon"> W </span>
                <span class="sidenav-normal"> Upgrade My Plan </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link link-handle" data-route="{{route('my-account.free-up-minutes')}}">
                <span class="sidenav-mini-icon"> D </span>
                <span class="sidenav-normal"> Minutes Tracker </span>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link link-handle" data-route="{{route('my-account.my-billing')}}">
                <span class="sidenav-mini-icon"> C </span>
                <span class="sidenav-normal"> My Subscription </span>
              </a>
            </li>

          </ul>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('my-profile')}}">
          <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_person" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">My Profile</span>
        </a>
      </li>
      <!-- <li class="nav-item">
          <a class="nav-link  link-handle"  data-route="{{route('my-account-new')}}">
            <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-circle-08  text-lg top-0" aria-hidden="true"></i>
            </div>
            <span class="nav-link-text ms-1">My Account New</span>
          </a>
        </li> -->
      <li class="nav-item">
        <hr class="horizontal dark">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6"> </h6>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('get-support')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <span class="fi fi_chat_help" style="color: #662D91; font-size: 20px"></span>
          </div>
          <span class="nav-link-text ms-1">Get Support</span>
        </a>
      </li>

      <!-- <li class="nav-item">
          <a class="nav-link  link-handle"  data-route="{{route('maintenance-dashboard')}}">
            <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-diamond  text-lg top-0" aria-hidden="true"></i>
            </div>
            <span class="nav-link-text ms-1">Coming Soon</span>
          </a>
        </li> -->


      @endhasrole

      @hasrole('B2B Client')

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('our-members.index')}}">
          <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/icons/our-members.png')}}" style="height:20px;">
          </div>
          <span class="nav-link-text ms-1">Our Members</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('our-profile.index')}}">
          <div class="icon icon-shape active icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/icons/my-recipients.png')}}" style="height:20px;">
          </div>
          <span class="nav-link-text ms-1">Our Profile</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('our-account.index')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">

            <img src="{{asset('assets/icons/my-account.png')}}" style="height:20px;">
          </div>
          <span class="nav-link-text ms-1">Our Account</span>
        </a>
      </li>







      <li class="nav-item">
        <hr class="horizontal dark">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6"> </h6>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('get-support')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/icons/support.png')}}" style="height:20px;">
          </div>
          <span class="nav-link-text ms-1">Get Support</span>
        </a>
      </li>

      <!-- <li class="nav-item">
          <a class="nav-link  link-handle"  data-route="{{route('maintenance-dashboard')}}">
            <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-diamond  text-lg top-0" aria-hidden="true"></i>
            </div>
            <span class="nav-link-text ms-1">Coming Soon</span>
          </a>
        </li> -->

      @endhasrole

      @hasrole('B2B Partner')

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('partner.our-members')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-circle-08  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">Our Members</span>
        </a>
      </li>

      <li class="nav-item">
        <hr class="horizontal dark">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Account Details</h6>
      </li>
      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('partner.our-account')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <!-- <svg width="12px" height="12px" viewBox="0 0 43 36" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                            <title>credit-card</title>
                            <g id="Basic-Elements" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g id="Rounded-Icons" transform="translate(-2169.000000, -745.000000)" fill="#FFFFFF" fill-rule="nonzero">
                                    <g id="Icons-with-opacity" transform="translate(1716.000000, 291.000000)">
                                        <g id="credit-card" transform="translate(453.000000, 454.000000)">
                                            <path class="color-background" d="M43,10.7482083 L43,3.58333333 C43,1.60354167 41.3964583,0 39.4166667,0 L3.58333333,0 C1.60354167,0 0,1.60354167 0,3.58333333 L0,10.7482083 L43,10.7482083 Z" id="Path" opacity="0.593633743"></path>
                                            <path class="color-background" d="M0,16.125 L0,32.25 C0,34.2297917 1.60354167,35.8333333 3.58333333,35.8333333 L39.4166667,35.8333333 C41.3964583,35.8333333 43,34.2297917 43,32.25 L43,16.125 L0,16.125 Z M19.7083333,26.875 L7.16666667,26.875 L7.16666667,23.2916667 L19.7083333,23.2916667 L19.7083333,26.875 Z M35.8333333,26.875 L28.6666667,26.875 L28.6666667,23.2916667 L35.8333333,23.2916667 L35.8333333,26.875 Z" id="Shape"></path>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg> -->
            <img src="{{asset('assets/icons/my-account.png')}}" style="height:20px;">
          </div>
          <span class="nav-link-text ms-1">Our Account</span>
        </a>
      </li>



      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('partner.our-profile')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <i class="ni ni-circle-08  text-lg top-0" aria-hidden="true"></i>
          </div>
          <span class="nav-link-text ms-1">Our Profile</span>
        </a>
      </li>



      <li class="nav-item">
        <hr class="horizontal dark">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6"> </h6>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('get-support')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <svg width="12px" height="12px" viewBox="0 0 46 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <title>customer-support</title>
              <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <g transform="translate(-1717.000000, -291.000000)" fill="#FFFFFF" fill-rule="nonzero">
                  <g transform="translate(1716.000000, 291.000000)">
                    <g transform="translate(1.000000, 0.000000)">
                      <path class="color-background opacity-6" d="M45,0 L26,0 C25.447,0 25,0.447 25,1 L25,20 C25,20.379 25.214,20.725 25.553,20.895 C25.694,20.965 25.848,21 26,21 C26.212,21 26.424,20.933 26.6,20.8 L34.333,15 L45,15 C45.553,15 46,14.553 46,14 L46,1 C46,0.447 45.553,0 45,0 Z"></path>
                      <path class="color-background" d="M22.883,32.86 C20.761,32.012 17.324,31 13,31 C8.676,31 5.239,32.012 3.116,32.86 C1.224,33.619 0,35.438 0,37.494 L0,41 C0,41.553 0.447,42 1,42 L25,42 C25.553,42 26,41.553 26,41 L26,37.494 C26,35.438 24.776,33.619 22.883,32.86 Z"></path>
                      <path class="color-background" d="M13,28 C17.432,28 21,22.529 21,18 C21,13.589 17.411,10 13,10 C8.589,10 5,13.589 5,18 C5,22.529 8.568,28 13,28 Z"></path>
                    </g>
                  </g>
                </g>
              </g>
            </svg>
          </div>
          <span class="nav-link-text ms-1">Get Support</span>
        </a>
      </li>

      <!-- <li class="nav-item">
          <a class="nav-link  link-handle"  data-route="{{route('maintenance-dashboard')}}">
            <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-diamond  text-lg top-0" aria-hidden="true"></i>
            </div>
            <span class="nav-link-text ms-1">Coming Soon</span>
          </a>
        </li> -->

      @endhasrole

      @hasrole('Member')

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('my-clients.index')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/icons/our-members.png')}}" style="height:20px;">
          </div>
          <span class="nav-link-text ms-1">My Clients</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('my-profile-member')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/icons/USER.png')}}" style="height:20px;">
          </div>
          <span class="nav-link-text ms-1">My Profile</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link link-handle" data-route="{{route('my-account-member')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/icons/my-account.png')}}" style="height:20px;">
          </div>
          <span class="nav-link-text ms-1">My Account</span>
        </a>
      </li>







      <li class="nav-item">
        <hr class="horizontal dark">
        <h6 class="ps-4  ms-2 text-uppercase text-xs font-weight-bolder opacity-6"> </h6>
      </li>

      <li class="nav-item">
        <a class="nav-link  link-handle" data-route="{{route('get-support')}}">
          <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
            <img src="{{asset('assets/icons/support.png')}}" style="height:20px;">
          </div>
          <span class="nav-link-text ms-1">Get Support</span>
        </a>
      </li>

      <!--  <li class="nav-item">
          <a class="nav-link  link-handle"  data-route="{{route('maintenance-dashboard')}}">
            <div class="icon icon-shape icon-sm shadow border-radius-md text-center me-2 d-flex align-items-center justify-content-center">
              <i class="ni ni-diamond  text-lg top-0" aria-hidden="true"></i>
            </div>
            <span class="nav-link-text ms-1">Coming Soon</span>
          </a>
        </li> -->

      @endhasrole








    </ul>
  </div>
  @hasrole('B2B Partner')
  <div class="sidenav-footer mx-3 mt-3 pt-3">
    <div class="card card-background shadow-none card-background-mask-primary" id="sidenavCard">
      <div class="full-background" style="background-image: url('{{asset('assets/img/curved-images/white-curved.jpg')}}"></div>
      <div class="card-body text-start p-3 w-100">
        <div class="icon icon-shape bg-white icon-sm shadow text-center mb-3 d-flex align-items-center justify-content-center border-radius-md">
          <span class="fi fi_gift" style="color: #662D91; font-size: 20px;"></span>
        </div>
        <div class="docs-info">
          <h6 class="text-white up mb-0">Want a Custom Package?</h6>
          <p class="text-xs font-weight-bold">We’d love to chat</p>
          <a data-route="{{route('get-support')}}" class="btn btn-white btn-sm w-100 mb-0 link-handle">Send Enquiry</a>
        </div>
      </div>
    </div>
  </div>
  @endhasrole

  @hasrole('B2B Client')
  <div class="sidenav-footer mx-3 mt-3 pt-3">
    <div class="card card-background shadow-none card-background-mask-primary" id="sidenavCard">
      <div class="full-background" style="background-image: url('{{asset('assets/img/curved-images/white-curved.jpg')}}"></div>
      <div class="card-body text-start p-3 w-100">
        <div class="icon icon-shape bg-white icon-sm shadow text-center mb-3 d-flex align-items-center justify-content-center border-radius-md">
          <span class="fi fi_gift" style="color: #662D91; font-size: 20px;"></span>
        </div>
        <div class="docs-info">
          <h6 class="text-white up mb-0">Want to share some feedback?</h6>
          <p class="text-xs font-weight-bold">We’d love to chat</p>
          <a data-route="{{route('get-support')}}" class="btn btn-white btn-sm w-100 mb-0 link-handle">Send us a Message</a>
        </div>
      </div>
    </div>
  </div>
  @endhasrole

  @hasrole('Member')
  <div class="sidenav-footer mx-3 mt-3 pt-3">
    <div class="card card-background shadow-none card-background-mask-primary" id="sidenavCard">
      <div class="full-background" style="background-image: url('{{asset('assets/img/curved-images/white-curved.jpg')}}"></div>
      <div class="card-body text-start p-3 w-100">
        <div class="icon icon-shape bg-white icon-sm shadow text-center mb-3 d-flex align-items-center justify-content-center border-radius-md">
          <span class="fi fi_gift" style="color: #662D91; font-size: 20px;"></span>
        </div>
        <div class="docs-info">
          <h6 class="text-white up mb-0">Want a Custom Package?</h6>
          <p class="text-xs font-weight-bold">We’d love to chat</p>
          <a data-route="{{route('get-support')}}" class="btn btn-white btn-sm w-100 mb-0 link-handle">Send Enquiry</a>
        </div>
      </div>
    </div>
  </div>
  @endhasrole

  @hasrole('Client')
  <div class="sidenav-footer mx-3 mt-3 pt-3">
    <div class="card card-background shadow-none card-background-mask-primary" id="sidenavCard">
      <div class="full-background" style="background-image: url('{{asset('assets/img/curved-images/white-curved.jpg')}}"></div>
      <div class="card-body text-start p-3 w-100">
        <div class="icon icon-shape bg-white icon-sm shadow text-center mb-3 d-flex align-items-center justify-content-center border-radius-md">
          <span class="fi fi_heart" style="color: #662D91; font-size: 20px;"></span>
        </div>
        <div class="docs-info">
          <h6 class="text-white up mb-0">Leave less unsaid</h6>
          <p class="text-xs font-weight-bold">Explore more features available to you</p>
          <a data-route="{{route('my-account.add-on-features')}}" class="btn btn-white btn-sm w-100 mb-0 link-handle">Show me more</a>
        </div>

      </div>
    </div>
  </div>
  @endhasrole




</aside>