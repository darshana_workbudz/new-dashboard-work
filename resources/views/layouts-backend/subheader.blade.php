<div class="container-fluid">
        <div class="page-header min-height-200 border-radius-xl mt-4" style="background-image: url('{{asset('assets/img/curved-images/curved0.jpg')}}'); background-position-y: 50%;">
                <span class="mask bg-primary opacity-9"></span>

        </div>
        <div class="card card-body blur shadow-blur mx-3 mt-n6 overflow-hidden">
                <div class="row gx-4">
                        <div class="col-auto">
                                <div class="avatar avatar-xl position-relative border-radius-xl bg-purple">
                                        <div class="card-body z-index-3">
                                                <i class="fi @yield('page-icon') text-white" style="font-size: 34px" aria-hidden="true"></i>
                                        </div>
                                </div>
                        </div>
                        <div class="col-lg-10 my-auto">
                                <div class="h-100">
                                        <h5 class="mb-1">
                                                @yield('page-title')
                                        </h5>
                                        <p class="mb-0 font-weight-bold text-sm">
                                                @yield('page-subtitle')
                                        </p>
                                </div>
                        </div>
                </div>
        </div>
</div>