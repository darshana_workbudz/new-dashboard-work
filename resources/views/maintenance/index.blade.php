@extends(isset($isajax) && $isajax==true ? 'layouts-frontend.ajax' : 'layouts-frontend.master')
@section('content')
<section class="min-vh-100 d-flex align-items-center">
        <div class="container">
          <div class="row mt-lg-0 mt-8">
            <div class="col-lg-6 my-auto">
              <h3 class="display-1 text-bolder text-gradient text-danger fadeIn1 fadeInBottom mt-5">Under Construction</h3>
              <p class="lead opacity-6 fadeIn2 fadeInBottom">We suggest you to go to the login while we develop this page.</p>
        <button type="button" data-route="{{route('login')}}" class="btn btn-lg bg-danger btn-lg w-100 mt-4 mb-0 link-handle">Go to login</button>
            </div>
            <div class="col-lg-6 my-auto">
             <img class="w-100 position-relative" src="{{asset('assets/img/error-maintenance.png')}}" alt="maintenance-error">
            </div>
          </div>
        </div>
      </section>
@endsection      