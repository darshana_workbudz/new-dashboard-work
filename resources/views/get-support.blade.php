@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Get Support')

@section('headercommon')
<x-main.header icon="chat_help" title="Get support" subtitle="Send us your query/feedback by submitting the form below" />
@endsection

@section('content')
<div>
  <form action="{{route('request-support')}}" method="POST">
    @csrf
    <div class="card mt-4" id="basic-info">
      <div class="card-header">
        <h5>Get Support</h5>
      </div>
      <div class="card-body pt-0">
        <div class="row">
          <div class="col-6">
            <label class="form-label">Given Name</label><i class="text-danger">*</i>
            <div class="input-group">
              <input id="firstName" name="firstName" class="form-control" type="text" placeholder="Jane" required="required">
            </div>
            @if(isset($error['firstName'][0]) && !empty($error['firstName'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['firstName'][0]}}
            </p>
            @endif
          </div>
          <div class="col-6">
            <label class="form-label">Family Name</label><i class="text-danger">*</i>
            <div class="input-group">
              <input id="lastName" name="lastName" class="form-control" type="text" placeholder="Smith" required="required">
            </div>
            @if(isset($error['lastName'][0]) && !empty($error['lastName'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['lastName'][0]}}
            </p>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <label class="form-label mt-4">Email Address</label><i class="text-danger">*</i>
            <div class="input-group">
              <input id="email" name="email" class="form-control" type="email" placeholder="email@example.com" required="required">
            </div>
            @if(isset($error['email'][0]) && !empty($error['email'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['email'][0]}}
            </p>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <label class="form-label mt-4">Message</label><i class="text-danger">*</i>
            <div>
              <textarea id="message" name="message" class="form-control" type="textarea" rows="3" placeholder="Your message ..." required="required"></textarea>
            </div>
            @if(isset($error['message'][0]) && !empty($error['message'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['message'][0]}}
            </p>
            @endif
          </div>
        </div>
      </div>
      <div class="card-body d-sm-flex pt-0">
        <button class="btn bg-primary mb-0 ms-2 submit-button" type="button" name="button">Submit support request</button>
      </div>
    </div>
  </form>
</div>
@endsection