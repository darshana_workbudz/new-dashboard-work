@extends(isset($isajax) && $isajax==true ? 'layouts-frontend.ajax' : 'layouts-frontend.master')
@section('content')

<!-- <header class="header-2">
  <nav class="navbar navbar-expand-lg navbar-dark navbar-absolute bg-transparent shadow-none" style="z-index:2;">
    <div class="container">
      <a class="navbar-brand font-weight-bolder link-handle" data-route="{{route('home')}}" rel="tooltip" data-placement="bottom" target="_blank">
                <img src="{{asset('assets/img/logo-w.png')}}">
      </a>
    </div>
  </nav>
    <div class="page-header min-vh-90 vc">
      <div class="container" style="z-index:2;">
        <div class="row">
          
          <div class="col-lg-7 mt-4">
            <h1 class="text-white pt-3 mt-n5">How do I choose the right plan for me ?</h1>
            <p class="lead text-white mt-3">Find and pick the best plan according to your needs.</p>
            <div class="buttons" >
            <a type="button" class="btn btn-lg btn-white text-primary" data-bs-toggle="modal" data-bs-target="#exampleModalSignUp">Show Me</a>
            </div>
          </div>
        </div>
      </div>
      <span class="overlay-video"></span>
      <video playsinline="playsinline" autoplay muted loop="true">
        <source src="{{asset('assets/video/bg-video.mp4')}}" type="video/mp4">
      </video>
      <div class="position-absolute w-100 z-index-1 bottom-0">
        <svg class="waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 40" preserveAspectRatio="none" shape-rendering="auto">
          <defs>
            <path id="gentle-wave" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z"></path>
          </defs>
          <g class="moving-waves">
            <use xlink:href="#gentle-wave" x="48" y="-1" fill="rgba(255,255,255,0.40"></use>
            <use xlink:href="#gentle-wave" x="48" y="3" fill="rgba(255,255,255,0.35)"></use>
            <use xlink:href="#gentle-wave" x="48" y="5" fill="rgba(255,255,255,0.25)"></use>
            <use xlink:href="#gentle-wave" x="48" y="8" fill="rgba(255,255,255,0.20)"></use>
            <use xlink:href="#gentle-wave" x="48" y="13" fill="rgba(255,255,255,0.15)"></use>
            <use xlink:href="#gentle-wave" x="48" y="16" fill="rgba(255,255,255,0.95"></use>
          </g>
        </svg>
      </div>
    </div>
  </header> -->
  <header>
    <div class="row">
      <div class="col-md-6 mx-auto text-center mb-5 mt-4">
        <div class="container">
          <a class="navbar-brand font-weight-bolder link-handle" data-route="{{route('home')}}" rel="tooltip" data-placement="bottom" target="_blank">
            <img src="{{asset('assets/img/evaheld-logo.png')}}" style="height: 50px;">
          </a>
        </div>
      </div>
    </div> 
  </header>
  <section>
    <div class="container pb-lg-4 pb-4 pt-3 postion-relative z-index-2">

      <div class="row">
        <div class="col-md-6 mx-auto text-center">
          <h4 class="text-primary">Simple, transparent pricing and options to fit your needs</h4>
          <p class="text-primary text-sm">No contracts, no surprise fees, Cancel anytime</p>
        </div>
      </div>
    </div>
  </section>
  <div class="mt-1">
    <div class="container">
      <div class="tab-content tab-space">
        <div class="tab-pane active" id="monthly">
          <div class="row">
            <h3 class="text-primary text-center">See our packages</h3>
            <p class="text-center text-primary">Choose package that suits your needs</p>
            @if(isset($packages) && count($packages)>0)

            @foreach($packages as $key=>$packagedata)
            @if($packagedata->issubscription==1)
            <div class="col-lg-4 mb-lg-0 mt-5 package-div">
              <form action="{{route('purchase-subscription',$packagedata->package_id)}}" method="post">
                @csrf
                <input type="hidden" name="package_id" value="{{$packagedata->package_id}}">

                <div class="card border h-100 " style="border-radius: 0px;">


                  @if($key==1)
                <span class="bg-warning text-xs text-dark text-center p-2"> <i class="fi fi_star" aria-hidden="true"></i> RECOMMENDED </span>
                  @else
                  <span class="bg-warning text-sm text-dark text-center mb-4"></span>
                  @endif
                  <div class="card-header  text-center pt-4 pb-5 position-relative">

                    <div class="z-index-1 position-relative">
                      <h5 class="text-gradient text-dark">
                        {{$packagedata->name}}
                      </h5>
                      <div class="row">
                 <!--  <div class="col-lg-12 justify-content-center monthly-price">
                  <span class="text-dark mt-2 mb-0 h3">
                    <small>$</small>{{$packagedata->pricemonthly}} <span class="h6 mt-2 mb-0">/ month</span></span>

                  </div> -->
                  <div class="col-lg-12  justify-content-center anually-price" >
                    <span class="text-dark mt-2 mb-0 h3">
                      <small>$</small>{{$packagedata->priceannually}} <span class="h6 mt-2 mb-0">/ year</span></span>

                    </div>
                  <!-- <div class="justify-content-center mt-3 d-flex">
                    <label class="form-check-label mb-0">
                      <small id="profileVisibility" class="font-weight-bolder monthly-price-text">
                        Bill monthly
                      </small>
                    </label>
                    <div class="form-check form-switch ms-2">
                      <input class="form-check-input switch-plan-price" name="isanually" type="checkbox" id="flexSwitchCheckDefault23" value="1" >
                    </div>
                    <label class="form-check-label mb-0 ">
                      <small id="profileVisibility" class="anually-price-text">
                        Bill yearly
                      </small>
                    </label>
                  </div> -->
                </div>
              </div>
            </div>

            <div class="card-body text-lg-start text-center pt-3">
              <div class="row mt-3">
                <div class="col-2">
                  <a class="btn btn-icon-only btn-rounded btn-outline-primary mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check" aria-hidden="true"></i></a>
                </div>
                
                <span class="text-sm ps-2 col-10" >{{$packagedata->message_item}}</span>
                
              </div>
              <div class="row  mt-3">
               <div class="col-2">
                <a class="btn btn-icon-only btn-rounded btn-outline-primary mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check" aria-hidden="true"></i></a>
              </div>

              <span class="text-sm ps-2 col-10" >{{$packagedata->written_item}}</span>

            </div>
            <div class="row  mt-3">
              <div class="col-2">
                <a class="btn btn-icon-only btn-rounded btn-outline-primary mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check" aria-hidden="true"></i></a>
              </div>

              <span class="text-sm ps-2 col-10" >{{$packagedata->recipient_item}}</span>

            </div>


            <button type="submit" name="purchasesubscription" class="purchase-subscription btn btn-icon text-white bg-alternate-primary d-lg-block mt-5 mb-0 w-100" value="notrial">
              Purchase Now
              <i class="fas fa-arrow-right ms-1" aria-hidden="true"></i>
            </button>
            @if($packagedata->demopackage!==1)
                  <button type="submit" name="purchasesubscription" class="purchase-subscription btn btn-icon bg-orange d-lg-block mt-3 mb-0 w-100" value="freetrial">
              Try for free for 14 days
              <i class="fas fa-arrow-right ms-1" aria-hidden="true"></i>
            </button>
            @endif
          </div>
        </div>
      </form>
    </div>
    @endif
    @endforeach

    @endif

  </div>
  @error('purchasesubscription')
  <h5 class="form-text text-danger text-center text-xs ms-1">
    {{ $message }}
  </h5>
  @endif
</div>




</div>
</div>

<section class="py-5 position-relative overflow-hidden"> 
  <!-- <img src="{{asset('assets/img/shapes/pattern-lines.svg')}}" alt="pattern-lines" class="position-absolute start-0 top-0 w-100 h-100 opacity-6">
  <div class="position-absolute w-100 z-inde-1 top-0 mt-n3">
    <svg width="100%" viewBox="0 -2 1920 157" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <title>wave-down</title>
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g fill="#FFFFFF" fill-rule="nonzero">
                <g id="wave-down">
                    <path d="M0,60.8320331 C299.333333,115.127115 618.333333,111.165365 959,47.8320321 C1299.66667,-15.5013009 1620.66667,-15.2062179 1920,47.8320331 L1920,156.389409 L0,156.389409 L0,60.8320331 Z" id="Path-Copy-2" transform="translate(960.000000, 78.416017) rotate(180.000000) translate(-960.000000, -78.416017) "></path>
                </g>
            </g>
        </g>
    </svg>
  </div> -->
  <div class="container py-7 postion-relative z-index-2">


    <div class="col-lg-6 col-md-8 mx-auto text-center mb-5">
      <h4 class="text-primary">Gift a physical reminder of your memory and connection with loved ones and the world</h4>
      <!-- <p class="text-primary">Choose gift that suits your needs.</p> -->
    </div>
    <div class="row justify-content-center">
      @if(isset($qrpackages) && !empty($qrpackages))
      @foreach($qrpackages as $dataqr)
     <div class="col-lg-6 col-md-6 mt-5 package-div">
       <form action="{{route('purchase-subscription',$dataqr->package_id)}}" method="post">
        @csrf
        <input type="hidden" name="package_id" value="{{$dataqr->package_id}}">
        <div class="card border h-100 card-blog card-plain p-4" style="border-radius: 0px;background-color: #e7c1f773;">
          <div class="card-header  text-center pt-4 pb-3 position-relative" style="background: transparent;">

            <div class="z-index-1 position-relative">
              <h4 class="text-gradient text-dark font-weight-bolder">
                {{$dataqr->name}}
              </h4>
              <span class="text-dark mt-2 mb-0 h3">
                <small>${{$dataqr->price}}</small>
              </span>
            </div>
          </div>
          <div class="position-relative">
            <a class="d-block blur-shadow-image">
              <img  src="{{asset('assets/img/qrstandalone.jpg')}}" alt="img-blur-shadow" class="img-fluid shadow border-radius-lg">
            </a>
            <div class="colored-shadow" style="background-image: url('{{asset('assets/img/qrstandalone.jpg')}}');"></div></div>
            <div class="card-body px-1 pt-3">

              <a href="javascript:;">
               <h5 class="text-alternate-primary text-primary text-center">
                Memorialise a story or message
              </h5>

            </a>
            <div class="row mt-3">
              <div class="col-2">
                <a class="btn btn-icon-only btn-rounded btn-outline-primary mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check" aria-hidden="true"></i></a>
              </div>

              <span class="text-sm ps-2 col-10 text-dark">Place a story, a eulogy or a thank you message onto a QR Code and have it printed anywhere you or your loved ones want you to be memorialised.</span>

            </div>
            <div class="row mt-3">
              <div class="col-2">
                <a class="btn btn-icon-only btn-rounded btn-outline-primary mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check" aria-hidden="true"></i></a>
              </div>

              <span class="text-sm ps-2 col-10 text-dark">Be remembered how you want to be remembered.</span>

            </div>

            <span class="p-1"></span>
            <button  type="submit" name="purchasesubscription" class="purchase-subscription btn btn-icon text-white bg-alternate-primary d-lg-block w-100 mt-6" value="notrial">
              Purchase Now &nbsp;<span class="h5 text-white"></span>
              <i class="fas fa-arrow-right ms-1" aria-hidden="true"></i>
            </button>

          </div>
        </div>
      </form>
    </div>
    @endforeach
    @endif

    @if(isset($heirloompackages) && !empty($heirloompackages))
      @foreach($heirloompackages as $datahl)
    <div class="col-lg-6 col-md-6 mt-5 package-div">
    <form action="{{route('purchase-subscription',$datahl->package_id)}}" method="post">
      @csrf
      <input type="hidden" name="package_id" value="{{$datahl->package_id}}">
      <div class="card border h-100 card-blog card-plain p-4" style="border-radius: 0px;background-color: #e7c1f773;">
        <div class="card-header  text-center pt-4 pb-3 position-relative" style="background: transparent;">

          <div class="z-index-1 position-relative">
            <h4 class="text-gradient text-dark font-weight-bolder">
              {{$datahl->name}}
            </h4>
            <span class="text-dark mt-2 mb-0 h3">
              <small>${{$datahl->price}}</small>
            </span>

          </div>
        </div>

        <div class="position-relative">
          <a class="d-block blur-shadow-image">
            <img src="{{asset('assets/img/deliveredstandalone.jpg')}}" alt="img-blur-shadow" class="img-fluid  shadow border-radius-lg">
          </a>
          <div class="colored-shadow" style="background-image: url('{{asset('assets/img/deliveredstandalone.jpg')}}');"></div></div>
          <div class="card-body px-1 pt-3">

            <a href="javascript:;">
              <h5 class="text-alternate-primary text-center">
                Gift the Memory of you
              </h5>
            </a>

            <div class="row mt-3">
              <div class="col-2">
                <a class="btn btn-icon-only btn-rounded btn-outline-primary mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check" aria-hidden="true"></i></a>
              </div>

              <span class="text-sm ps-2 col-10 text-dark" >Gift your loved ones a gift they will cherish forever, your memory. We place your messages and stories on a USB, which is then hand delivered to your loved ones in a beautiful heirloom style box that can be kept and cherished from generation to generation.</span>

            </div>

            <div class="row mt-3">
              <div class="col-2">
                <a class="btn btn-icon-only btn-rounded btn-outline-primary mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check" aria-hidden="true"></i></a>
              </div>

              <span class="text-sm ps-2 col-10 text-dark" >Nearly all recipients tell us that this is the best gift they’ve ever received.</span>

            </div>



            <button  type="submit" name="purchasesubscription" class="purchase-subscription btn btn-icon text-white bg-alternate-primary d-lg-block w-100 mt-6" value="notrial">
              Purchase Now &nbsp;<span class="h5 text-white" ></span>
              <i class="fas fa-arrow-right ms-1" aria-hidden="true"></i>
            </button>

          </div>
        </div>
      </form>
    </div>
    @endforeach
    @endif




  </div>
</div>
  <!-- <div class="position-absolute w-100 bottom-0">
    <svg width="100%" viewBox="0 -1 1920 166" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <title>wave-up</title>
        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g transform="translate(0.000000, 5.000000)" fill="#FFFFFF" fill-rule="nonzero">
                <g id="wave-up" transform="translate(0.000000, -5.000000)">
                    <path d="M0,70 C298.666667,105.333333 618.666667,95 960,39 C1301.33333,-17 1621.33333,-11.3333333 1920,56 L1920,165 L0,165 L0,70 Z"></path>
                </g>
            </g>
        </g>
    </svg>
  </div> -->
</section>

<!-- <section class="py-lg-3">
  <div class="container">
    <div class="row">
      <div class="col-md-6 mx-auto text-center mb-5">


      </div>
    </div>
    <div class="row">
      <div class="card">
        <div class="row">
          <div class="col-lg-8">
            <div class="card-body">
              <h3 class="text-primary">Want to try a Demo Account?</h3>
              <p>We understand if you’re not ready to commit, so feel free to set up an account and have a look around so you can see what it’s like. And don’t worry if you do decide to save and allocate any messages while you’re there, you can always upgrade to a package within your account or start a free trial.</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="card-body text-center">


              <a href="{{route('dashboard')}}" class="text-white btn btn-icon btn-lg mt-3 mb-0 w-100 bg-primary">
                Get started - it’s free
                <i class="fas fa-arrow-right ms-1" aria-hidden="true"></i>
              </a>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section> -->
</div>
<div class="modal fade" id="exampleModalSignUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalSignTitle" aria-hidden="true" >
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content" style="border-radius: 0px;">
      <div class="modal-body p-0">
        <div class="card card-plain  p-0" >

          <div class="card-body p-0">

            <div class="row">

              <div class="col-lg-7 position-relative bg-cover p-0">
                <div class="text-center d-flex h-100 w-100 d-flex m-auto justify-content-center" style="background-image: url('{{asset('assets/img/package-find.jpeg')}}');background-size: cover;">
                  <span class="mask bg-cover-primary "></span>
                  <div class="position-relative text-start my-auto">
                    <h3 class="text-white px-3">How do I choose what's right for me?</h3>
                    <p class="text-white px-3">Fill up the form and see what package suits your needs.
                    </p>
                  </div>
                </div>
              </div>
              <div class="col-lg-5">
                <div class="card-body">
                  <form role="form"  name="packagepredictionform">
                    <label class="text-dark">How many people would you like to send messages to?</label>
                    <div class="mb-3">
                      <select class="form-control " id="peoplecount">
                        <option value="2">Up to 2</option>
                        <option value="4">Up to 4</option>
                        <option value="8">Up to 8</option>
                        <option value="notsure">I'm not sure</option>
                      </select>
                    </div>
                    <label class="text-dark">Would you like to send video, audio or written messages?</label>
                    <div class="mb-3">

                      <select class="form-control " id="messageformat">
                        <option value="video">Video</option>
                        <option value="audio">Audio</option>
                        <option value="written">Written</option>
                        <option value="all">Combination of 3</option>

                      </select>
                    </div>
                    <label class="text-dark">What kind of messages would you like to send?</label>
                    <div class="mb-3">

                      <select class="form-control " id="messagetype">
                        <option value="direct_message_to_loved_ones">Direct messages to loved ones and friends-
                        videos/audio/written</option>
                        <option value="history_life_story">History/life story videos/audio/written</option>
                        <option value="biggest_moments">biggest moments videos/audio/written</option>
                        <option value="life_lessons">life lessons videos/audio/written/option&gt;
                        </option><option value="message_for_world">messages for the world videos/audio/written</option>
                        <option value="message_for_children">messgaes for children/grandchildren videos/audio/written
                        </option>
                        <option value="message_for_unborn">messages for unborn grandchildren videos/audio/written</option>
                        <option value="book_readings">Book readings for relatives &amp; unborn relatives videos/audio/written
                        </option>
                        <option value="euology_messages">eulogy/ funeral messages -videos/audio/written</option>
                        <option value="direct_messages_for_groups">direct messages for groups and organisations that the
                        person was a member/patron of - videos/audio/written</option>
                      </select>
                    </div>

                    <div class="col-md-6 text-end ms-auto">
                      <button type="submit" class="btn btn-round bg-gradient-primary mb-0">Find Plan!</button>
                    </div>
                  </form>
                </div>
              </div>

            </div>

          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
  $(document).on('click',".switch-plan-price" ,function() {

      // if (this.checked) {
      //   $(this).closest('.package-div').find('.monthly-price').hide();
      //   $(this).closest('.package-div').find('.monthly-price-text').removeClass('font-weight-bolder');

      //   $(this).closest('.package-div').find('.anually-price').show();
      //   $(this).closest('.package-div').find('.anually-price-text').addClass('font-weight-bolder');
      // }
      // else
      // {
      //   $(this).closest('.package-div').find('.monthly-price').show();
      //   $(this).closest('.package-div').find('.monthly-price-text').addClass('font-weight-bolder');
      //   $(this).closest('.package-div').find('.anually-price').hide();
      //   $(this).closest('.package-div').find('.anually-price-text').removeClass('font-weight-bolder');
      // }


    });
  </script>

  @endsection        

