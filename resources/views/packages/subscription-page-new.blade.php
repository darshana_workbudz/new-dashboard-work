@extends(isset($isajax) && $isajax==true ? 'layouts-frontend.ajax' : 'layouts-frontend.master')
@section('content')

<header>
    <div class="pricing-page-heading">
        <figure> <img alt="" title="" /> </figure>
        <div class="pricing-heading-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <h1 class="text-white">Limited time only introductory offers!</h1>
                        <p>Memorialisation happens in so many different ways, we’re here to support that journey for you
                            and your loved ones.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="pricing-page-wraper">
    <div class="pricing-page-menu">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" data-toggle="tab" data-content="nav-ConnectionContent"
                                href="javascript:" onclick="return switchPackage(this);">Connection Content Plans </a>
                            <a class="nav-item nav-link" data-toggle="tab" data-content="nav-MemorialQR"
                                href="javascript:" onclick="return switchPackage(this);">Memorial QR Codes</a>
                            <a class="nav-item nav-link" data-toggle="tab" data-content="nav-HeirloomKeepsake"
                                href="javascript:" onclick="return switchPackage(this);">Heirloom Keepsake</a>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <section class="pricing-box-con">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">

                 @error('purchasesubscription')
                <h5 class="form-text text-danger text-center text-xs ms-1">
                    {{ $message }} 
                </h5>
                @endif
                        <!-- tab 1 content start-->
                        <div class="tab-pane fade show active" id="nav-ConnectionContent" data-toggle="tab-content">
                            <div class="pricing-box-wrapper">
                                <div class="heading-group">
                                    <h4>Connection Content Plans</h4>
                                    <h5>Empowering you to leave less unsaid</h5>
                                    <h6><span><strong>LIMITED TIME OFFERS</strong></span></h6>
                                </div>

                                <div class="row">
                                    @if(isset($subscriptionpackages) && count($subscriptionpackages)>0)

                                    @foreach($subscriptionpackages as $key=>$packagedata)
                                    @if(isset($packagedata) && count($packagedata)>0)
                                    @if($packagedata[0]->issubscription==1)
                                    
                                    <div class="col-md-4">
                                        <form action="{{route('purchase-subscription',$packagedata[0]->package_id)}}" method="post">
                                        @csrf
                                        <input type="hidden" name="package_id" value="{{$packagedata[0]->package_id}}">
                                        <div class="pricing-box">

                                            <div class="icon"><svg width=" 24" height="24" fill="none"
                                                    viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="m12.82 5.58-.82.822-.824-.824a5.375 5.375 0 1 0-7.601 7.602l7.895 7.895a.75.75 0 0 0 1.06 0l7.902-7.897a5.376 5.376 0 0 0-.001-7.599 5.38 5.38 0 0 0-7.611 0Z"
                                                        fill="#662d91" />
                                                </svg>
                                            </div>
                                            <h4>{{$packagedata[0]->name}}</h4>
                                            <p>Join the Evaheld Community</p>
                                            <h5>${{$packagedata[0]->priceannually}}</h5>
                                            <h6><span><strong>FREE</strong></span></h6>
                                            <div class="content text-start">
                                                <p>Perfect for creating messages for that special Someone.</p>
                                            </div>
                                            <div class="list">
                                                <br>
                                                <p><strong>Inclusions</strong></p>
                                                @foreach($packagedata as $p)

                                                <div class="row mt-3" @if($p->level == 2) style="padding-left:30px"
                                                    @endif>
                                                    <div class="col-2">
                                                        <svg width="24" height="24" fill="none" viewBox="0 0 24 24"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2Zm3.22 6.97-4.47 4.47-1.97-1.97a.75.75 0 0 0-1.06 1.06l2.5 2.5a.75.75 0 0 0 1.06 0l5-5a.75.75 0 1 0-1.06-1.06Z"
                                                                fill="#662d91" />
                                                        </svg>
                                                    </div>

                                                    <span class="text-sm ps-2 col-10">{{$p->inclusion_name}}</span>

                                                </div>
                                                @endforeach
                                                <br>

                                                @if(isset($community_packages_coming_soon[$key]) &&
                                                count($community_packages_coming_soon[$key])>0)
                                                <p><strong>Coming soon</strong></p>
                                                @foreach($community_packages_coming_soon[$key] as $package_comming_soon)
                                                <div class="row mt-3" @if($package_comming_soon->level == 2)
                                                    style="padding-left:30px" @endif>
                                                    <div class="col-2">
                                                        <svg width="24" height="24" fill="none" viewBox="0 0 24 24"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2Zm3.22 6.97-4.47 4.47-1.97-1.97a.75.75 0 0 0-1.06 1.06l2.5 2.5a.75.75 0 0 0 1.06 0l5-5a.75.75 0 1 0-1.06-1.06Z"
                                                                fill="#abb0b4" />
                                                        </svg>
                                                    </div>

                                                    <span
                                                        class="text-sm ps-2 col-10">{{$package_comming_soon->name}}</span>

                                                </div>
                                                @endforeach
                                                @endif

                                                <div class="button-con">
                                                  <button type="submit" name="purchasesubscription"
                                                        value="notrial">Get started now</button>

                                                        @if($packagedata[0]->demopackage!==1)
                                                         <button type="submit" name="purchasesubscription"
                                                        value="freetrial">Start a 14-day free trial</button>
                                                        @endif
                                                </div>
                                            </div>

                                        </div>
                                        <div class="pricing-expand-con">
                                            <p>Community Package Terms</p>
                                            <div class="pricing-expand-content">
                                                <div><span id="dots"></span>
                                                    <div class="expand-content" id="more">
                                                        <h6>Community Package Terms</h6>
                                                        <p><sup>1</sup> Until the date of delivery of the first message,
                                                            which must be within 1 year.</p>
                                                        <p><sup>2</sup> Must indicate future date</p>
                                                    </div>
                                                </div>
                                                <button onclick="myFunction()" id="myBtn">Click to expand</button>
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                    
                                    @endif
                                    @endif
                                    @endforeach

                                    @endif
                                </div>


                            </div>
                        </div>
                        <!-- tab 1 content ends -->
                        <!-- tab2start  -->
                        <div class="tab-pane fade" role="tabpanel" id="nav-MemorialQR" data-toggle="tab-content">
                            <div class="pricing-box-wrapper">
                                <div class="heading-group">
                                    <h2>Memorial QR Codes</h2>
                                    <h5>Be remembered how you want to be remembered</h5>
                                    <h6><span>LIMITED TIME OFFERS</span></h6>
                                </div>
                                <div class="row">

                                    @if(isset($qrpackages) && !empty($qrpackages))
                                    @foreach($qrpackages as $qr_key=> $dataqr)
                                    @if(isset($dataqr) && count($dataqr)>0)

                                        <div class="col-md-6">
                                               <form action="{{route('purchase-subscription',$dataqr[0]->package_id)}}" method="post">
                        @csrf
                        <input type="hidden" name="package_id" value="{{$dataqr[0]->package_id}}">

                                        <div class="pricing-box">
                                            <div class="icon"><i class="fa fa-gift" aria-hidden="true"></i></div>
                                            <h4>   {{$dataqr[0]->name}}</h4>
                                            <p>Most popular</p>
                                            <h5>${{$dataqr[0]->price}}</h5>
                                            <h6><span>SAVE! Now $199, will be $399</span></h6>
                                            <div class="content">
                                                <p>Linked to a Connections Package, the Connections QR code…</p>
                                            </div>
                                            <div class="list">
                                                <p><strong>Inclusions</strong></p>
                                                

                                                  @foreach($dataqr as $qr)
                                                  <div class="row mt-3" @if($qr->level == 2)
                                                    style="padding-left:30px" @endif>
                                                    <div class="col-2">
                                                        <svg width="24" height="24" fill="none" viewBox="0 0 24 24"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2Zm3.22 6.97-4.47 4.47-1.97-1.97a.75.75 0 0 0-1.06 1.06l2.5 2.5a.75.75 0 0 0 1.06 0l5-5a.75.75 0 1 0-1.06-1.06Z"
                                                                fill="#662d91" />
                                                        </svg>
                                                    </div>

                                                    <span
                                                        class="text-sm ps-2 col-10">{{$qr->inclusion_name}}</span>

                                                </div>

                                                  
                                               
                                                  @endforeach
                                                  <br>

                                                   @if(isset($community_packages_coming_soon[$qr_key]) &&
                                                count($community_packages_coming_soon[$qr_key])>0)
                                                <p><strong>Coming soon</strong></p>
                                                @foreach($community_packages_coming_soon[$qr_key] as $package_comming_soon)
                                                <div class="row mt-3" @if($package_comming_soon->level == 2)
                                                    style="padding-left:30px" @endif>
                                                    <div class="col-2">
                                                        <svg width="24" height="24" fill="none" viewBox="0 0 24 24"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2Zm3.22 6.97-4.47 4.47-1.97-1.97a.75.75 0 0 0-1.06 1.06l2.5 2.5a.75.75 0 0 0 1.06 0l5-5a.75.75 0 1 0-1.06-1.06Z"
                                                                fill="#abb0b4" />
                                                        </svg>
                                                    </div>

                                                    <span
                                                        class="text-sm ps-2 col-10">{{$package_comming_soon->name}}</span>

                                                </div>
                                                @endforeach
                                                @endif                                                    
                                                
                                                
                                                
                                                    
                                                    
                                            
                                            </div>
                                            <div class="button-con">
                                                 <button type="submit" name="purchasesubscription"
                                    
                                    value="notrial">
                                    Add item &nbsp;<span class="h5 text-white"></span>
                                    <i class="fas fa-arrow-right ms-1" aria-hidden="true"></i>
                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>


                                    @endif
                                    @endforeach
                                    @endif


                 
                                </div>
                            </div>

                        </div>
                        <!-- tab2 end -->
                        <!-- tab 3 start -->

                        <div class="tab-pane fade " id="nav-HeirloomKeepsake" data-toggle="tab-content">
                            <div class="pricing-box-wrapper">
                            <div class="row">

                                    @if(isset($heirloompackages) && !empty($heirloompackages))
                @foreach($heirloompackages as $hl_key => $datahl)
                @if(isset($datahl) && count($datahl)>0)
                                


                  <div class="col-md-6">
                    <form action="{{route('purchase-subscription',$dataqr[0]->package_id)}}" method="post">
                        @csrf
                        <input type="hidden" name="package_id" value="{{$dataqr[0]->package_id}}">
                                <div class="pricing-box text-left">
                                  <div class="icon"><i class="fa fa-gift" aria-hidden="true"></i></div>
                                  <h4>  {{$datahl[0]->name}}<span>LIMITED TIME OFFER</span></h4>
                                  <p>The gift of remembrance</p>
                                  <h5>${{$datahl[0]->price}}</h5>
                                  <h6><span>SAVE! Now $349, will be $449</span></h6>
                                  <div class="content">
                                    <p>Give your loved ones a beautiful heirloom keepsake that will keep you connected to your family for generations to come.</p>
                                  </div>
                                  <div class="list">
                                    <p><strong>Inclusions</strong></p>
                                    @foreach($datahl as $hl)
                                                  <div class="row mt-3" @if($hl->level == 2)
                                                    style="padding-left:30px" @endif>
                                                    <div class="col-2">
                                                        <svg width="24" height="24" fill="none" viewBox="0 0 24 24"
                                                            xmlns="http://www.w3.org/2000/svg">
                                                            <path
                                                                d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2Zm3.22 6.97-4.47 4.47-1.97-1.97a.75.75 0 0 0-1.06 1.06l2.5 2.5a.75.75 0 0 0 1.06 0l5-5a.75.75 0 1 0-1.06-1.06Z"
                                                                fill="#662d91" />
                                                        </svg>
                                                    </div>

                                                    <span
                                                        class="text-sm ps-2 col-10">{{$hl->inclusion_name}}</span>

                                                </div>

                                                  
                                               
                                                  @endforeach
                                                  <br>


                                      </div>
                                  <div class="button-con">
                                          <button type="submit" name="purchasesubscription"
                                    
                                    value="notrial">
                                Add item &nbsp;<span class="h5 text-white"></span>
                                    <i class="fas fa-arrow-right ms-1" aria-hidden="true"></i>
                                </button>

                                  </div>
                                </div>
                                <div class="pricing-expand-con">
                                    <p>Legacy Package Terms</p>
                                    <!--<div class="pricing-expand-content">
                                        <div><span id="dots_4"></span>
                                            <div class="expand-content" id="more_4">
                                                <h6>Heirloom Keepsake Terms</h6>
                                                <p><sup>1</sup> Until the date of delivery of the first message, which must be within 1 year.</p>
                                                <p><sup>2</sup> Must indicate future date</p>
                                            </div>
                                        </div>
                                        <button onclick="myFunction()" id="myBtn_4">Click to expand</button>
                                    </div>-->
                                </div>
                              </div>
                          </form>
                              <div class="col-md-6">
                                <div class="heirloom-keepsake">
                                    <img src="{{asset('assets/img/img-4.jpg')}}" alt="" title=""/>
                                </div>
                              </div>

                            @endif
                            @endforeach
                            @endif

                            </div>
                          </div>
                            
                        </div>

                        <!-- tab3 end -->
                    </div>
                </div>
            </div>
        </div>
        <div class="buttom-bg">
            <figure>
                <img src="{{asset('assets/img/faq-top-bg.png')}}" alt="" title="" />
            </figure>
        </div>
    </section>
    <section class="faq-section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <h3 class="heading-1 text-center">Faq<h3>
                            <div class="pxlr-club-faq">
                                <div class="content">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" id="headingOne" role="tab">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse"
                                                        data-parent="#accordion" href="#collapseOne"
                                                        aria-expanded="false" aria-controls="collapseOne">Community
                                                        Package<i class="pull-right fa fa-plus"></i></a>
                                                </h4>
                                            </div>
                                            <div class="panel-collapse collapse" id="collapseOne" role="tabpanel"
                                                aria-labelledby="headingOne">
                                                <div class="panel-body pxlr-faq-body">
                                                    <p>You can keep your free account forever, although you do have to
                                                        send the first message you create within one year or it may be
                                                        deleted. However, you can always upgrade or buy more minutes,
                                                        which will keep your content safe forever. This account gives
                                                        you the opportunity to explore the different content types and
                                                        set up recipients, so you get a better idea of how the platform
                                                        works.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" id="headingTwo" role="tab">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse"
                                                        data-parent="#accordion" href="#collapseTwo"
                                                        aria-expanded="false" aria-controls="collapseTwo">Connection
                                                        Package<i class="pull-right fa fa-plus"></i></a>
                                                </h4>
                                            </div>
                                            <div class="panel-collapse collapse" id="collapseTwo" role="tabpanel"
                                                aria-labelledby="headingTwo">
                                                <div class="panel-body pxlr-faq-body">
                                                    <p>You can create as many messages and pieces of content within the
                                                        time allocation of your account before you settle on your final
                                                        ten messages. In this account you can allocate content to up
                                                        three recipients and you have access to all the special
                                                        features, such as access to the wellbeing system and exclusive
                                                        community benefits and discounts on QR Codes, Heirloom Keepsakes
                                                        and extra minutes. This account will also give you access to the
                                                        Evaheld Connections Memorialisation platform which is coming
                                                        soon.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" id="headingThree" role="tab">
                                                <h4 class="panel-title"><a class="collapsed" role="button"
                                                        data-toggle="collapse" data-parent="#accordion"
                                                        href="#collapseThree" aria-expanded="false"
                                                        aria-controls="collapseThree">Legacy Package <i
                                                            class="pull-right fa fa-plus"></i></a></h4>
                                            </div>
                                            <div class="panel-collapse collapse" id="collapseThree" role="tabpanel"
                                                aria-labelledby="headingThree">
                                                <div class="panel-body pxlr-faq-body">
                                                    <p>You can create unlimited messages and pieces of content within
                                                        the time allocation of your account and you can allocate them to
                                                        up to ten recipients. to In this account can allocate content to
                                                        three recipients and you have access to all the special
                                                        features, such as access to the wellbeing system and exclusive
                                                        community benefits and discounts on QR Codes, Heirloom Keepsakes
                                                        and extra minutes. This account will also give you access to the
                                                        Evaheld Connections Memorialisation platform which is coming
                                                        soon and a host of other features which are in the pipeline!
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" id="headingFour" role="tab">
                                                <h4 class="panel-title"><a class="collapsed" role="button"
                                                        data-toggle="collapse" data-parent="#accordion"
                                                        href="#collapseFour" aria-expanded="false"
                                                        aria-controls="collapseFour">Connections QR Code<i
                                                            class="pull-right fa fa-plus"></i></a></h4>
                                            </div>
                                            <div class="panel-collapse collapse" id="collapseFour" role="tabpanel"
                                                aria-labelledby="headingFour">
                                                <div class="panel-body pxlr-faq-body">
                                                    <p>The connection QR Code includes all the features of the
                                                        Connection Package, this means that you have all the timing and
                                                        content features of that package, which you can link to your
                                                        personalised QR Code. This QR code can be printed on
                                                        memorialisation items, such as funeral booklets or headstones.
                                                        It also includes full access to the Evaheld Connections
                                                        Memorialisation platform once that’s launched.

                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" id="headingFour" role="tab">
                                                <h4 class="panel-title"><a class="collapsed" role="button"
                                                        data-toggle="collapse" data-parent="#accordion"
                                                        href="#collapseFour" aria-expanded="false"
                                                        aria-controls="collapseFour">Legacy QR Code
                                                        <i class="pull-right fa fa-plus"></i></a></h4>
                                            </div>
                                            <div class="panel-collapse collapse" id="collapseFour" role="tabpanel"
                                                aria-labelledby="headingFour">
                                                <div class="panel-body pxlr-faq-body">
                                                    <p>The Legacy QR Code includes all the features of the Legacy
                                                        Package, this means that you have all the timing and content
                                                        features of that package, which you can link to your
                                                        personalised QR Code. This QR code can be printed on
                                                        memorialisation items, such as funeral booklets or headstones.
                                                        It also includes full access to the Evaheld Connections
                                                        Memorialisation platform once that’s launched.

                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" id="headingFour" role="tab">
                                                <h4 class="panel-title"><a class="collapsed" role="button"
                                                        data-toggle="collapse" data-parent="#accordion"
                                                        href="#collapseFour" aria-expanded="false"
                                                        aria-controls="collapseFour">Heirloom Keepsake
                                                        <i class="pull-right fa fa-plus"></i></a></h4>
                                            </div>
                                            <div class="panel-collapse collapse" id="collapseFour" role="tabpanel"
                                                aria-labelledby="headingFour">
                                                <div class="panel-body pxlr-faq-body">
                                                    <p>The Heirloom Keepsake is one of our most special gifts of
                                                        remembrance, because it allows you to give a special someone a
                                                        digital and tangible keepsake which they can cherish forever and
                                                        pass onto future generations. Your content will be placed on a
                                                        customised USB with special packaging which reflects its
                                                        sentimental value and will be securely courier delivered. The
                                                        perfect gift for loved ones, the gift of you.

                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pricing-box-con-2">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="left">
                        <h2 class="heading-1">Even if I miss significant life events, I know my my messages and presence
                            will be felt and everlasting in those moments.</h2>
                        <div class="btn-group"> <a href="#" class="btn btn-2">Our story</a> <a href="#"
                                class="btn btn-2">How we help</a> </div>
                    </div>
                </div>
                <div class="col-md-6 text-center">
                    <figure>
                        <img src="{{asset('assets/img/EvenifImiss.jpg')}}" alt="" title="" />
                    </figure>
                </div>
            </div>
        </div>
    </section>

</div>
<footer>
    <div class="footer-1">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="f-box">
                        <div class="f-logo">
                            <img src="{{asset('assets/f-logo.png')}}" alt="" title="" />
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        <div class="fsocialmedia">
                            <a href="#" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="#" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href="#" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                            <a href="#" title="Youtube"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="f-box">
                                <h4>Product </h4>
                                <ul>
                                    <li><a href="#">How it works</a></li>
                                    <li><a href="#">Features</a></li>
                                    <li><a href="#">Pricing</a></li>
                                    <li><a href="#">Security</a></li>
                                    <li><a href="#">Product tour</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-box">
                                <h4>Resources </h4>
                                <ul>
                                    <li><a href="#">How it works</a></li>
                                    <li><a href="#">Features</a></li>
                                    <li><a href="#">Pricing</a></li>
                                    <li><a href="#">Security</a></li>
                                    <li><a href="#">Product tour</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="f-box">
                                <h4>Company</h4>
                                <ul>
                                    <li><a href="#">How it works</a></li>
                                    <li><a href="#">Features</a></li>
                                    <li><a href="#">Pricing</a></li>
                                    <li><a href="#">Security</a></li>
                                    <li><a href="#">Product tour</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="f-box">
                        <h4>Suscribe to receive our newsletter</h4>
                        <p>The latest news, articles, and resources, sent to your inbox weekly</p>
                        <div class="news-letter">
                            <p>Enter your email address</p>
                            <form>
                                <input type="mail" placeholder="example@gmsil.com" />
                                <input type="submit" value="submit" />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer-2">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="left">
                                <ul>
                                    <li>Copyright 2022 Evaheld</li>
                                    <li>ABN 37123456789</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="right">
                                <ul>
                                    <li><a href="#">Diversity & Inclusion</a></li>
                                    <li><a href="#">Term of services</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Sitemap</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- <div class="mt-1">
    <div class="container">
        <div class="tab-content tab-space">
            <div class="tab-pane active" id="monthly">
                <div class="row">
                    <h3 class="text-primary text-center">See our packages</h3>
                    <p class="text-center text-primary">Choose package that suits your needs</p>
                    @if(isset($subscriptionpackages) && count($subscriptionpackages)>0)

                    @foreach($subscriptionpackages as $key=>$packagedata)
                    @if(isset($packagedata) && count($packagedata)>0)
                    @if($packagedata[0]->issubscription==1)
                    <div class="col-lg-4 mb-lg-0 mt-5 package-div">
                        <form action="{{route('purchase-subscription',$packagedata[0]->package_id)}}" method="post">
                            @csrf
                            <input type="hidden" name="package_id" value="{{$packagedata[0]->package_id}}">

                            <div class="card border h-100 " style="border-radius: 0px;">


                                @if($key==2)
                                <span class="bg-warning text-xs text-dark text-center p-2"> <i class="fa fa-star" aria-hidden="true"></i> RECOMMENDED </span>
                                @else
                                <span class="bg-warning text-sm text-dark text-center mb-4"></span>
                                @endif
                                <div class="card-header  text-center pt-4 pb-5 position-relative">

                                    <div class="z-index-1 position-relative">
                                        <h5 class="text-gradient text-dark">
                                            {{$packagedata[0]->name}}
                                        </h5>
                                        <div class="row">

                                            <div class="col-lg-12  justify-content-center anually-price">
                                                <span class="text-dark mt-2 mb-0 h3">
                                                    <small>$</small>{{$packagedata[0]->priceannually}} <span class="h6 mt-2 mb-0">/ year</span></span>

                                            </div>
                                          
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body text-lg-start text-center pt-3">
                                    <br>
                                    <h5>Inclusions</h5>
                                    @foreach($packagedata as $p)

                                    <div class="row mt-3" @if($p->level == 2) style="padding-left:30px" @endif>
                                        <div class="col-2">
                                            <a class="btn btn-icon-only btn-rounded btn-outline-primary mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check" aria-hidden="true"></i></a>
                                        </div>

                                        <span class="text-sm ps-2 col-10">{{$p->inclusion_name}}</span>

                                    </div>
                                    @endforeach
                                    <br>

                                    @if(isset($community_packages_coming_soon[$key]) &&
                                    count($community_packages_coming_soon[$key])>0)
                                    <h5>Comming Soon</h5>
                                    @foreach($community_packages_coming_soon[$key] as $package_comming_soon)
                                    <div class="row mt-3" @if($package_comming_soon->level == 2)
                                        style="padding-left:30px" @endif>
                                        <div class="col-2">
                                            <a class="btn btn-icon-only btn-rounded btn-outline-primary mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check" aria-hidden="true"></i></a>
                                        </div>

                                        <span class="text-sm ps-2 col-10">{{$package_comming_soon->name}}</span>

                                    </div>
                                    @endforeach
                                    @endif



                                    <button type="submit" name="purchasesubscription" class="purchase-subscription btn btn-icon text-white bg-alternate-primary d-lg-block mt-5 mb-0 w-100" value="notrial">
                                        Purchase Now
                                        <i class="fas fa-arrow-right ms-1" aria-hidden="true"></i>
                                    </button>
                                    @if($packagedata[0]->demopackage!==1)
                                    <button type="submit" name="purchasesubscription" class="purchase-subscription btn btn-icon bg-gradient-dark d-lg-block mt-3 mb-0 w-100" value="freetrial">
                                        Try for free for 14 days
                                        <i class="fas fa-arrow-right ms-1" aria-hidden="true"></i>
                                    </button>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                    @endif
                    @endif
                    @endforeach

                    @endif

                </div>
                @error('purchasesubscription')
                <h5 class="form-text text-danger text-center text-xs ms-1">
                    {{ $message }}
                </h5>
                @endif
            </div>




        </div>
    </div>

    <section class="py-5 position-relative overflow-hidden">
    
        <div class="container py-7 postion-relative z-index-2">


            <div class="col-lg-6 col-md-8 mx-auto text-center mb-5">
                <h4 class="text-primary">Gift a physical reminder of your memory and connection with loved ones and the
                    world</h4>
             </div>
            <div class="row justify-content-center">
                @if(isset($qrpackages) && !empty($qrpackages))
                @foreach($qrpackages as $qr_key=> $dataqr)
                @if(isset($dataqr) && count($dataqr)>0)

                <div class="col-lg-6 col-md-6 mt-5 package-div">
                    <form action="{{route('purchase-subscription',$dataqr[0]->package_id)}}" method="post">
                        @csrf
                        <input type="hidden" name="package_id" value="{{$dataqr[0]->package_id}}">
                        <div class="card border h-100 card-blog card-plain p-4" style="border-radius: 0px;background-color: #e7c1f773;">
                            <div class="card-header  text-center pt-4 pb-3 position-relative" style="background: transparent;">

                                <div class="z-index-1 position-relative">
                                    <h4 class="text-gradient text-dark font-weight-bolder">
                                        {{$dataqr[0]->name}}
                                    </h4>
                                    <span class="text-dark mt-2 mb-0 h3">
                                        <small>${{$dataqr[0]->price}}</small>
                                    </span>
                                </div>
                            </div>
                      
                            <div class="card-body px-1 pt-3">

                                <a href="javascript:;">
                                    <h5 class="text-alternate-primary text-primary text-center">
                                        Memorialise a story or message
                                    </h5>

                                </a>

                                <br>
                                <h5>Inclusions</h5>
                                @foreach($dataqr as $qr)
                                <div class="row mt-3" @if($qr->level == 2) style="padding-left:30px" @endif>
                                    <div class="col-2">
                                        <a class="btn btn-icon-only btn-rounded btn-outline-primary mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check" aria-hidden="true"></i></a>
                                    </div>

                                    <span class="text-sm ps-2 col-10 text-dark">{{$qr->inclusion_name}}</span>

                                </div>

                                @endforeach
                                <br><br>
                                @if(isset($community_packages_coming_soon[$qr_key]) &&
                                count($community_packages_coming_soon[$qr_key])>0)
                                <h5>Comming Soon</h5>
                                @foreach($community_packages_coming_soon[$qr_key] as $package_comming_soon)
                                <div class="row mt-3" @if($package_comming_soon->level == 2) style="padding-left:30px"
                                    @endif>
                                    <div class="col-2">
                                        <a class="btn btn-icon-only btn-rounded btn-outline-primary mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check" aria-hidden="true"></i></a>
                                    </div>

                                    <span class="text-sm ps-2 col-10">{{$package_comming_soon->name}}</span>

                                </div>

                                @endforeach
                                @endif

                                <span class="p-1"></span>
                                <button type="submit" name="purchasesubscription" class="purchase-subscription btn btn-icon text-white bg-alternate-primary d-lg-block w-100 mt-6" value="notrial">
                                    Purchase Now &nbsp;<span class="h5 text-white"></span>
                                    <i class="fas fa-arrow-right ms-1" aria-hidden="true"></i>
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
                @endif
                @endforeach
                @endif

                @if(isset($heirloompackages) && !empty($heirloompackages))
                @foreach($heirloompackages as $hl_key => $datahl)
                @if(isset($datahl) && count($datahl)>0)
                <div class="col-lg-6 col-md-6 mt-5 package-div">
                    <form action="{{route('purchase-subscription',$datahl[0]->package_id)}}" method="post">
                        @csrf
                        <input type="hidden" name="package_id" value="{{$datahl[0]->package_id}}">
                        <div class="card border h-100 card-blog card-plain p-4" style="border-radius: 0px;background-color: #e7c1f773;">
                            <div class="card-header  text-center pt-4 pb-3 position-relative" style="background: transparent;">

                                <div class="z-index-1 position-relative">
                                    <h4 class="text-gradient text-dark font-weight-bolder">
                                        {{$datahl[0]->name}}
                                    </h4>
                                    <span class="text-dark mt-2 mb-0 h3">
                                        <small>${{$datahl[0]->price}}</small>
                                    </span>

                                </div>
                            </div>

                            <div class="position-relative">
                                <a class="d-block blur-shadow-image">
                                    <img src="{{asset('assets/img/deliveredstandalone.jpg')}}" alt="img-blur-shadow" class="img-fluid  shadow border-radius-lg">
                                </a>
                                <div class="colored-shadow" style="background-image: url('{{asset('assets/img/deliveredstandalone.jpg')}}');">
                                </div>
                            </div>
                            <div class="card-body px-1 pt-3">

                                <a href="javascript:;">
                                    <h5 class="text-alternate-primary text-center">
                                        Gift the Memory of you
                                    </h5>
                                </a>



                                <h5>Inclusions</h5>
                                @foreach($datahl as $hl)
                                <div class="row mt-3" @if($hl->level == 2) style="padding-left:30px" @endif >
                                    <div class="col-2">
                                        <a class="btn btn-icon-only btn-rounded btn-outline-primary mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check" aria-hidden="true"></i></a>
                                    </div>
                                    <span class="text-sm ps-2 col-10 text-dark">{{$hl->inclusion_name}}</span>
                                </div>
                                @endforeach
                                <br>

                                @if(isset($community_packages_coming_soon[$hl_key]) &&
                                count($community_packages_coming_soon[$hl_key])>0)
                                <h5>Comming Soon</h5>
                                @foreach($community_packages_coming_soon[$hl_key] as $package_comming_soon)
                                <div class="row mt-3" @if($package_comming_soon->level == 2) style="padding-left:30px"
                                    @endif>
                                    <div class="col-2">
                                        <a class="btn btn-icon-only btn-rounded btn-outline-primary mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check" aria-hidden="true"></i></a>
                                    </div>

                                    <span class="text-sm ps-2 col-10">{{$package_comming_soon->name}}</span>

                                </div>
                                @endforeach
                                @endif





                                <button type="submit" name="purchasesubscription" class="purchase-subscription btn btn-icon text-white bg-alternate-primary d-lg-block w-100 mt-6" value="notrial">
                                    Purchase Now &nbsp;<span class="h5 text-white"></span>
                                    <i class="fas fa-arrow-right ms-1" aria-hidden="true"></i>
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
                @endif
                @endforeach
                @endif




            </div>
        </div>
       
    </section>

   
</div> -->




<style>
    header {
        width: 100%;
        float: left;
    }

    .top-1 {
        width: 100%;
        float: left;
        padding: 10px 0;
        box-shadow: 0 0 2px #00000069;
    }

    .logo a img {
        max-width: 160px;
    }

    /*Nav Menu*/
    .menu-con {
        display: inline-block;
        width: auto;
        float: right;
    }

    .menu-con .navbar-dark .navbar-nav .nav-link {
        padding: 10px 15px;
    }

    .menu-con nav.navbar.navbar-expand-lg.navbar-dark {
        display: inline-block;
    }

    .menu-con .dropdown-toggle::after {
        display: none;
    }


    .menu-con .navbar-dark .navbar-nav .nav-link {
        color: rgb(0 0 0);
        text-transform: uppercase;
        font-size: 15px;
        font-weight: 400;
    }

    .menu-con .navbar-toggler {
        background: #f16b23;
    }

    .menu-con .navbar-dark .navbar-nav .nav-link:hover {
        color: #09ad8b;
    }

    .menu-con .dropdown-menu ul li a {
        text-transform: none !important;
    }

    /*Hide drop down arrow*/
    .menu-con .dropdown-toggle::after {
        display: none;
    }

    /**/
    .menu-con .navbar .dropdown-menu div[class*="col"] {
        margin-bottom: 0;
    }

    /* breakpoint and up - mega dropdown styles */
    @media screen and (min-width: 992px) {

        /* remove the padding from the navbar so the dropdown hover state is not broken */
        .menu-con .navbar {
            padding-top: 0px;
            padding-bottom: 0px;
        }

        /* remove the padding from the nav-item and add some margin to give some breathing room on hovers */
        .menu-con .navbar .nav-item {
            padding: .5rem .5rem;
            margin: 0 .25rem;
        }

        /* makes the dropdown full width  */
        .menu-con .navbar .dropdown {
            position: static;
        }

        .menu-con .navbar .dropdown-menu {
            visibility: hidden;
            opacity: 0;
            position: absolute;
            top: 98%;
            left: 0;
            width: 100%;
            transform: translateY(-2em);
            z-index: -1;
            transition: all 0.3s ease-in-out 0s, visibility 0s linear 0.3s, z-index 0s linear 0.01s;
            display: inline-block;
            border-radius: 5px;
            box-shadow: 0 3px 6px #00000026;
        }

        /* shows the dropdown menu on hover */
        .menu-con .navbar .dropdown:hover .dropdown-menu,
        .navbar .dropdown .dropdown-menu:hover {
            visibility: visible;
            opacity: 1;
            z-index: 1;
            transform: translateY(0%);
            transition-delay: 0s, 0s, 0.3s;
        }

        .menu-con .navbar .dropdown-menu {
            border: none;
            background-color: #fffffff2;
        }
    }

    /*M I L A N*/
    .dropdown-menu ul li {
        padding: 0 !important;
        margin: 0 !important;
    }

    .dropdown-menu ul li a {
        font-size: 13px !important;
        display: inline-block;
        padding: 4px !important;
    }

    .menu-con .navbar .nav-item a {
        padding: 6px 0;
        display: inline-block;
        width: 100%;
        color: #545656;
        text-decoration: none;
        position: relative;
    }

    .menu-con .navbar .nav-item a:hover {
        color: #8c61ac;
    }

    .menu-con .navbar .nav-item a:before {
        content: '';
        width: 0;
        height: 2px;
        position: absolute;
        background: #d0bedd;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        transition: all ease-in-out 1.0s;
    }

    .menu-con .navbar .nav-item:hover a:before {
        width: 100%;
    }

    span.menu-heading {
        color: #568800;
        margin-bottom: 8px;
        padding-bottom: 6px;
        display: inline-block;
        position: relative;
        font-weight: 500;
    }

    span.menu-heading:before {
        content: '';
        width: 70px;
        background: #86553e;
        height: 2px;
        position: absolute;
        bottom: 0;
    }

    .menu-con .menu-con .dropdown-menu li.nav-item {
        padding: 5px 0 0;
    }

    .menu-con .menu-con .dropdown-menu li.nav-item a {
        padding: 0;
    }

    /* adds some margin below the link sets  */
    .navbar .dropdown-menu div[class*="col"] {
        margin-bottom: 1rem;
    }

    .navbar .dropdown-menu {
        border: 1px solid #ebebeb !important;
        background-color: #ffffff !important;
    }

    .menu-con .dropdown-menu .nav-link {
        text-transform: none !important;
        padding: 6px 0 !important;
        line-height: 15px;
    }

    .dropdown-menu span.text-uppercase {
        color: #09ad8b;
        display: inline-block;
        margin-bottom: 11px;
    }

    /* breakpoint and up - mega dropdown styles */
    @media screen and (min-width: 992px) {

        /* remove the padding from the navbar so the dropdown hover state is not broken */
        .navbar {
            padding-top: 0px;
            padding-bottom: 0px;
        }

        /* remove the padding from the nav-item and add some margin to give some breathing room on hovers */
        .dropdown-menu .nav-item {
            font-size: 14px;
            text-transform: none;
        }

        /* makes the dropdown full width  */
        .navbar .dropdown {
            position: static;
        }

        .navbar .dropdown-menu {
            width: 100%;
            left: 0;
            right: 0;
            /*  height of nav-item  */
            top: 45px;
        }

        /* shows the dropdown menu on hover */
        .navbar .dropdown:hover .dropdown-menu,
        .navbar .dropdown .dropdown-menu:hover {
            display: block !important;
        }

        .navbar .dropdown-menu {
            border: 1px solid rgba(0, 0, 0, .15);
            background-color: #fff;
        }

    }

    .btn-1 {
        background: #f3eff6;
        color: #662d91;
        padding: 10px 17px;
        border: 1px solid #f3eff6;
        border-radius: 30px;
    }

    .btn-1:hover {
        background: none;
        color: #662d91;
        text-decoration: none;
    }

    /*Nav Menu End*/
    /*=========
Header End
============*/
    /*Pricing Page*/
    .pricing-page-heading {
        width: 100%;
        float: left;
        padding: 0;
        color: #ffffff;
        position: relative;
        background: url('../assets/img/inner-page-banner.jpg');
        background-size: cover;
    }

    .pricing-heading-content {
        width: 100%;
        padding: 100px 0;
    }

    .pricing-heading-content h1 {
        font-size: 28px;
    }

    .pricing-page-heading figure {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
    }

    .pricing-page-heading figure img {
        object-fit: cover;
        height: 100%;
        min-width: 100%;
    }

    /*Pricing Page End*/
    .pricing-page-wraper {
        width: 100%;
        background: #f3eef6;
        padding: 0 0 60px;
        float: left;
    }

    .pricing-page-menu {
        width: 100%;
        float: left;
        text-align: center;
        background: #fff;
        box-shadow: 0 3px 8px #0000003b;
        margin: 0;
        padding: 19px 0;
    }

    .pricing-page-menu nav {
        display: inline-block;
    }

    .pricing-page-menu ul li a,
    .pricing-page-menu nav .nav-fill .nav-item {
        display: inline-block;
        position: relative;
        padding: 0 0 1px;
        margin: 0 15px;
        color: #662d91;
        transition: all ease-in-out 1.0s;
        border: none;
    }

    .pricing-page-menu .nav-tabs {
        border-bottom: none;
    }

    .pricing-page-menu ul li a:before,
    .pricing-page-menu nav .nav-fill .nav-item:before {
        content: '';
        width: 0;
        height: 2px;
        position: absolute;
        background: #662d91;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        transition: all ease-in-out 1.0s;
    }

    .pricing-page-menu ul li a:hover,
    .pricing-page-menu nav .nav-fill .nav-item:before {
        color: #662d91;
        text-decoration: none;
    }

    .pricing-page-menu ul li:hover a:before,
    .pricing-page-menu ul li.active a:before,
    .pricing-page-menu nav .nav-fill .nav-item:hover:before,
    .pricing-page-menu nav .nav-fill .nav-item.active:before {
        width: 100%;
    }

    .pricing-page-menu ul {
        display: inline-block;
    }

    .pricing-page-menu ul li {
        display: inline-block;
    }

    section.pricing-box-con {
        text-align: center;
    }

    section.pricing-box-con {
        text-align: center;
        width: 100%;
        float: left;
        padding: 30px 0 0;
        position: relative;
        background: url(../assets/img/BackgroundLineWhite.svg) no-repeat;
        background-size: 60%;
    }

    .pricing-box-wrapper {
        width: 100%;
        float: left;
        background: #fff;
        padding: 30px;
        border-radius: 10px;
        box-shadow: 0 0 8px #0000003b;
        margin-bottom: 30px;
    }

    .heirloom-keepsake {
        margin-top: 50px;
    }

    div#nav-ConnectionContent .pricing-box-wrapper {
        background-image: url(../assets/img/bg1.jpg);
        background-repeat: no-repeat;
        background-position: bottom left;
        background-size: 519px;
        position: relative;
    }

    .pricing-box-wrapper:before {
        content: '';
        position: absolute;
        top: 5%;
        right: 20%;
        background: url(../assets/img/element-1.png) no-repeat;
        width: 95px;
        height: 77px;
    }

    #nav-HeirloomKeepsake .pricing-box-wrapper:before {
        display: none;
    }

    .heading-group {
        width: 100%;
        float: left;
    }

    .heading-group h5 {
        font-size: 18px;
        color: #662d91;
        margin: 20px 0;
    }

    .heading-group h6 {
        margin: 0 0 30px;
    }

    .heading-group h6 span {
        display: inline-block;
        padding: 10px 21px;
        background: #f3eef6;
        border-radius: 10px;
        font-size: 14px;
        color: #662d91;
    }

    .heading-group h2 {
        margin-bottom: 0;
    }

    section.pricing-box-con nav {
        display: inline-block;
        border: 1px solid #f2f3f4;
        border-radius: 30px;
        overflow: hidden;
        background: #fbfbfc;
    }

    section.pricing-box-con nav .nav-tabs .nav-link {
        border-radius: 30px !important;
        color: #979899;
    }

    section.pricing-box-con nav .nav-tabs .nav-link span {
        background: #eef0f2;
        padding: 2px 5px;
        border-radius: 5px;
        font-size: 11px;
    }

    section.pricing-box-con.nav-tabs .nav-item.show .nav-link,
    section.pricing-box-con .nav-tabs .nav-link.active,
    section.pricing-box-con.nav-tabs .nav-item.show .nav-link:hover {
        color: #7d4da1;
        background-color: #fff;
        border-color: #7d4da1;
    }

    section.pricing-box-con-2 {
        width: 100%;
        float: left;
        padding: 60px 0;
    }

    section.pricing-box-con-2 .left {
        padding: 60px 0 0;
        position: relative;
    }

    section.pricing-box-con-2 .left .btn {
        background: #662d91;
        color: #fff;
    }

    section.pricing-box-con-2 .left .btn.btn-2 {
        background: #fff;
        color: #662d91;
        margin-right: 10px;
        padding: 7px 22px;
        border-radius: 30px;
    }

    section.pricing-box-con-2 h2 {
        color: #662d91;
        margin-bottom: 30px;
        background: url(../assets/img/b1.jpg) no-repeat;
        background-position: left bottom;
        padding-bottom: 0px;
    }

    .pricing-box {
        width: 100%;
        border: 2px solid #edeff1;
        padding: 30px;
        border-radius: 10px;
        margin-bottom: 30px;
    }

    .pricing-box {
        width: 100%;
        border: 2px solid #edeff1;
        padding: 30px;
        border-radius: 10px;
        margin-bottom: 30px;
        text-align: center;
        transition: all ease-in-out 0.5s;
    }

    .pricing-box:hover,
    .pricing-box.active {
        border-color: #662d91;
    }

    .pricing-box .button-con button:hover {
        background: #662d91;
        color: #fff;
    }

    .pricing-box .button-con button:first-child {
        background: #662d91;
        color: #fff;
    }

    .pricing-box .button-con button:first-child:hover {
        background: #f3eef6;
        color: #662d91;
    }

    .pricing-box .icon i {
        color: #662d91;
        font-size: 37px;
    }

    .pricing-box h4,
    .pricing-box h5 {
        font-family: 'Poppins', sans-serif;
        margin: 20px 0;
        font-size: 20px;
    }

    .pricing-box h4 span {
        display: inline-block;
        background: #f3eef6;
        padding: 9px 15px;
        border-radius: 10px;
        margin: 0 0 0 13px;
        color: #662d91;
        font-size: 14px;
        float: right;
    }

    .pricing-box h5 {
        font-size: 33px;
        color: #662d91;
        margin: 0 0;
    }

    .pricing-box h6 span {
        display: inline-block;
        background: #f3eef6;
        padding: 9px 22px;
        border-radius: 10px;
        margin: 10px 0;
        color: #662d91;
        font-size: 14px;
    }

    .pricing-box .list {
        text-align: left;
    }

    .pricing-box .list ul li {
        position: relative;
        padding-left: 24px;
        margin-bottom: 10px;
    }

    .pricing-box .list ul li ul {
        margin: 10px 0;
    }

    .pricing-box p {
        margin-top: 25px;
        margin-bottom: 5px;
    }

    .pricing-box .list ul li:before {
        font: normal normal normal 14px/1 FontAwesome;
        content: "\f00c";
        width: 15px;
        height: 15px;
        border-radius: 20px;
        background: #662d91;
        color: #fff;
        position: absolute;
        left: 0;
        top: 2px;
        font-size: 11px;
        text-align: center;
        line-height: 15px;
    }

    .pricing-box .list ul.sub li:before {
        background: #8b9197 !important;
    }

    .pricing-box .button-con {
        margin-top: 30px;
    }

    .pricing-box .button-con button {
        background: #f3eef6;
        border: 1px solid #f3eef6;
        width: 100%;
        padding: 10px;
        border-radius: 23px;
        color: #662d91;
        margin-top: 13px;
    }

    .pricing-expand-con {
        text-align: left;
        color: #9aa0a5;
    }

    #more,
    #more_2,
    #more_3,
    #more_4 {
        display: none;
    }

    .pricing-expand-content {
        position: relative;
        width: 100%;
        padding-top: 21px;
    }

    .pricing-expand-con p {
        margin-bottom: 8px;
        color: #9aa0a5;
    }

    .expand-content {
        border-top: 1px solid #edeeef;
        width: 100%;
        float: left;
        padding-top: 10px;
        margin: 10px 0 0;
    }

    .pricing-expand-content button {
        position: absolute;
        background: none;
        border: none;
        padding: 0;
        margin: 0;
        top: 0;
        left: 0;
        color: #9aa0a5;
    }

    .pricing-expand-content button:before {
        content: '';
    }

    .pricing-expand-content button:focus {
        border: none;
        outline: none;
        stroke: none;
    }

    .pricing-expand-content button {
        position: absolute;
        background: none;
        border: none;
        padding: 0;
        margin: 0;
        top: 0;
        left: 0;
    }

    /*FAQ Section*/
    .buttom-bg figure {
        padding: 0;
        margin: 0;
    }

    .buttom-bg figure img {
        width: 100%;
    }

    section.faq-section {
        width: 100%;
        float: left;
        padding: 60px 0 50px;
        background: #fff;
    }

    .panel-body.pxlr-faq-body p {
        font-size: 14px;
        text-align: justify;
        color: #4e4f50;
        padding-left: 60px;
        line-height: 23px;
        font-weight: normal;
    }

    .heading-1 {
        font-size: 22px;
        color: #662d91;
        margin-bottom: 30px;
    }


    /**/

    section.faq-section .panel-group {
        margin-bottom: 0;
    }

    section.faq-section .panel-group .panel {
        border-radius: 0;
        box-shadow: none;
    }

    section.faq-section .panel-group .panel .panel-heading {
        padding: 0;
    }

    section.faq-section .panel-group .panel .panel-heading h4 a {
        background: #ffffff;
        display: block;
        font-size: 15px;
        line-height: 20px;
        padding: 15px;
        text-decoration: none;
        transition: 0.15s all ease-in-out;
        border-top: 1px solid #f7f8f8;
        color: #662d91;
        position: relative;
    }

    section.faq-section .panel-group .panel .panel-heading h4 a:before {
        content: '';
        position: absolute;
        left: 0;
        top: 21px;
        background: #662d91;
        width: 8px;
        height: 8px;
        border-radius: 30px;
    }

    section.faq-section .panel-group .panel:last-child .panel-heading h4 a {
        border-bottom: 1px solid #f7f8f8;
    }

    section.faq-section .panel-group .panel .panel-heading h4 a:hover,
    .panel-group .panel .panel-heading h4 a:not(.collapsed) {
        /*background: #fff;*/
        transition: 0.15s all ease-in-out;
    }

    section.faq-section .panel-group .panel .panel-heading h4 a:not(.collapsed) i:before {
        content: "-";
        font-size: 30px;
        line-height: 10px;
    }

    section.faq-section .panel-group .panel .panel-heading h4 a i {
        color: #999;
        font-size: 12px;
        float: right;
    }

    section.faq-section .panel-group .panel .panel-body {
        padding-top: 0;
    }

    section.faq-section .panel-group .panel .panel-heading+.panel-collapse>.list-group,
    section.faq-section .panel-group .panel .panel-heading+.panel-collapse>.panel-body {
        border-top: none;
    }

    section.faq-section .panel-group .panel+.panel {
        border-top: none;
        margin-top: 0;
    }

    /*FAQ Section End*/
    /*getstarted section*/

    /*Have aq section*/
    section.have-aq-section {
        width: 100%;
        float: left;
        padding: 160px 0;
        background: #f3eef6;
        text-align: center;
        color: #662d91;
    }

    section.have-aq-section h2 {
        width: 100%;
        float: left;
    }

    button.btn {
        color: #662d91;
        background: #fff;
        margin-top: 30px;
    }

    /*Have aq section End*/
    /*Footer*/
    footer {
        width: 100%;
        float: left;
        padding: 0;
        margin: 0;
        background: #662d91;
        color: #f4f0f7;
    }

    .footer-1 {
        width: 100%;
        float: left;
        padding: 130px 0 115px;
    }

    .f-logo {
        margin-bottom: 10px;
    }

    .f-logo img {
        width: 140px;
    }

    .footer-1 a {
        color: #f4f0f7;
    }

    .footer-1 h4 {
        font-family: 'Poppins', sans-serif;
        font-size: 16px;
        margin-bottom: 25px;
        color: #fff;
    }

    .footer-1 .news-letter {
        position: relative;
        width: 100%;
    }

    .news-letter form {
        position: relative;
        width: 100%;
        padding-right: 127px;
    }

    .news-letter form:before {
        content: '';
        position: absolute;
        width: 30px;
        height: 22px;
        right: 0;
        top: -25px;
        background: url(../assets/img/nl-shape.png);

    }

    .news-letter input {
        border-radius: 5px;
        padding: 10px;
        width: 100%;
    }

    .f-box ul li {
        width: 100%;
        margin-bottom: 10px;
    }

    .news-letter form input[type="submit"] {
        width: 123px;
        position: absolute;
        top: 1px;
        right: 0;
        background: #f58675;
        color: #fff;
        border: 1px solid #f58675;
        cursor: pointer;
    }

    .fsocialmedia a {
        display: inline-block;
        background: #fff;
        width: 30px;
        height: 30px;
        text-align: center;
        color: #662d91;
        border-radius: 30px;
        margin-right: 6px;
        align-content: center;
        line-height: 31px;
    }

    .fsocialmedia a:hover {
        opacity: 0.8;
    }

    .news-letter input {
        border-radius: 5px;
        padding: 10px;
        background: #fff;
        border-color: transparent;
    }

    .news-letter input {
        border-radius: 5px;
        padding: 10px;
    }

    .footer-2 {
        width: 100%;
        float: left;
        padding: 30px 0;
        border-top: 1px solid #8558a8;
    }

    .footer-2 ul {
        display: inline-block;
    }

    .footer-2 ul li {
        display: inline-block;
        margin-right: 12px;
        font-size: 13px;
    }

    .footer-2 ul li a {
        color: #fff;
    }

    .footer-2 ul li:last-child {
        margin-right: 0;
    }

    .footer-2 .right {
        text-align: right;
    }

    /*Footer End*/

    /*Responsive*/
    @media (min-width: 1200px) {
        .container {
            max-width: 1200px;
        }
    }
</style>

<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
    $(document).on('click', ".switch-plan-price", function() {

        // if (this.checked) {
        //   $(this).closest('.package-div').find('.monthly-price').hide();
        //   $(this).closest('.package-div').find('.monthly-price-text').removeClass('font-weight-bolder');

        //   $(this).closest('.package-div').find('.anually-price').show();
        //   $(this).closest('.package-div').find('.anually-price-text').addClass('font-weight-bolder');
        // }
        // else
        // {
        //   $(this).closest('.package-div').find('.monthly-price').show();
        //   $(this).closest('.package-div').find('.monthly-price-text').addClass('font-weight-bolder');
        //   $(this).closest('.package-div').find('.anually-price').hide();
        //   $(this).closest('.package-div').find('.anually-price-text').removeClass('font-weight-bolder');
        // }


    });

    function switchPackage(e){
        $('a[data-toggle="tab"]').removeClass('active');
        $('div[data-toggle="tab-content"]').removeClass('active show');
        $(e).addClass('active');
        $('#'+ $(e).attr('data-content') ).addClass('active show')

    }

</script>

@endsection