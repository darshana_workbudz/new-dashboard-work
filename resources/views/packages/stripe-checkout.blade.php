@extends(isset($isajax) && $isajax==true ? 'layouts-frontend.ajax' : 'layouts-frontend.master')

@section('content')
<div class="page-header min-vh-100 bg-primary">
  <span class="mask bg-orange"></span>
  <img src="{{asset('assets/img/shapes/waves-white.svg')}}" alt="pattern-lines" class="position-absolute opacity-6 start-0 top-0 w-100">
  <div class="container pb-lg-9 pb-10 pt-7 postion-relative z-index-2">
    <div class="row">
      <div class="col-md-6 mx-auto text-center">
        <h3 class="text-white">Stripe Payment Processing</h3>
      </div>
    </div>

  </div>
</div>
@endsection
@section('script')
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
  jQuery(document).find('.page-progress-bar').show();
  jQuery(document).find('.loader').show();
  var stripe = Stripe("<?php echo $pkkey; ?>");
  var session = "<?php echo $checkout_session; ?>";
  stripe.redirectToCheckout({
      sessionId: session
    })
    .then(function(result) {

      if (result.error) {
        swal(result.error.message);
      }
    })
    .catch(function(error) {
      console.error('Error:', error);
    });
</script>
@stop