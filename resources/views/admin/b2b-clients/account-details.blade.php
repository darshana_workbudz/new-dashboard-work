@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','B2B Clients Account Details')

@section('headercommon')
<x-main.header icon="contact_card" title="B2B Clients Account Details" subtitle="Here you can manage B2B Clients Account Details" />
@endsection

@section('content')
<div>
  <div class="card mt-4" id="basic-info">
    <div class="card-header pb-0">
        <nav style="--bs-breadcrumb-divider: '›';" aria-label="breadcrumb">
          <ol class="breadcrumb p-0 bg-white">
            <li class="breadcrumb-item"><a data-route="{{route('b2b-clients.index')}}" class="link-handle h5 text-sm font-weight-bolder text-primary">Our B2B Clients</a></li>
            <li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page">B2B Clients Account Details</li>
          </ol>
        </nav>
      </div>
    <div class="card-body pt-0">
         <div class="row">
        <div class="col-12 col-sm-6  mt-1">
          <label>Organisation Name <span class="text-danger">*</span></label>
          <input class="form-control" type="text" name="organisation_name" value="{{$userdata->organisation_name}}" disabled  />
          @if(isset($error['organisation_name'][0]) && !empty($error['organisation_name'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['organisation_name'][0]}}
          </p>
          @endif
        </div>

        <div class="col-12 col-sm-6  mt-1">
          <label>Organisation Code <span class="text-danger">*</span></label>
          <input class="form-control" type="text" name="organisation_code" value="{{$userdata->organisation_code}}" disabled placeholder="example@organisation_code.com" />
          @if(isset($error['organisation_code'][0]) && !empty($error['organisation_code'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['organisation_code'][0]}}
          </p>
          @endif
        </div>
      </div>
       <h5 class="font-weight-bolder text-sm text-primary mt-3"> &nbsp;Basic Info</h5> 
        <div class="row">
          <div class="col-12 col-sm-6">
            <label>Given name <span class="text-danger">*</span></label>
            <input class="form-control @if(isset($error['first_name'][0]) && !empty($error['first_name'][0])) is-invalid @endif" type="text" name="first_name" value="{{$userdata->first_name}}" placeholder="Jane" disabled />
            @if(isset($error['first_name'][0]) && !empty($error['first_name'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['first_name'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 col-sm-6">
            <label>Family name <span class="text-danger">*</span></label>
            <input class="form-control @if(isset($error['last_name'][0]) && !empty($error['last_name'][0])) is-invalid @endif" type="text" name="last_name" value="{{$userdata->last_name}}" placeholder="Smith" disabled />
            @if(isset($error['last_name'][0]) && !empty($error['last_name'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['last_name'][0]}}
            </p>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-sm-6  mt-1">
            <label>Email address <span class="text-danger">*</span></label>
            <input class="form-control" type="text" name="email" value="{{$userdata->email}}"  placeholder="example@email.com" disabled />
            @if(isset($error['email'][0]) && !empty($error['email'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['email'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 col-sm-6  mt-1">
            <div class="row">
              <label>Phone number</label>
              <div class="col-3 mt-3 mt-sm-0 pr-0 mr-0">
                <select name="phone_code" class="phonecodeselect2 form-control" style="width: 100%;" disabled>
                  @if(isset($phonecodes) && !empty($phonecodes))
                  @foreach($phonecodes as $data)
                  <option value="{{$data->id}}" data-code="{{$data->iso2}}" @if(isset($userdata->phone_code) && $userdata->phone_code==$data->iso2){{"selected"}} @elseif($data->iso2=='AU') {{"selected"}} @endif >+{{$data->phone_code}} {{$data->iso2}} </option>
                  @endforeach
                  @endif

                </select>
              </div>
              <div class="col-9 mt-3 mt-sm-0 pl-0 ml-0 mb-4">
                <input disabled id="phone_number" class="required form-control @if(isset($error['phone_number'][0]) && !empty($error['phone_number'][0])) is-invalid @endif" type="number" name="phone_number" value="{{$userdata->phone_number}}" placeholder="0412345678" />
                @if(isset($error['phone_number'][0]) && !empty($error['phone_number'][0]))
                <p class="form-text text-danger text-xs mb-1">
                  {{$error['phone_number'][0]}}
                </p>
                @endif
              </div>
            </div>
          </div>
        </div>
        <hr class="horizontal dark" />
        
        <div class="row mt-1">
          <div class="col-12 col-sm-6">
            <label>Date of birth</label>
            <div id="datepicker" class="input-group-alternative date custom-datepicker" data-date-format="dd-mm-yyyy">
              <input required type="text" id="dob-date" value="<?php echo isset($userdata->dob) && !empty($userdata->dob) ? date('d-m-Y', strtotime($userdata->dob)) : '' ?>" name="dob" class=" date-validation form-control  @if(isset($error['dob'][0]) && !empty($error['dob'][0])) is-invalid @endif" placeholder="dd-mm-yyyy" / disabled>
              <span class="input-group-addon">
              </span>
            </div>
            @if(isset($error['dob'][0]) && !empty($error['dob'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['dob'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 col-sm-6"></div>
          <div class="col-12 col-sm-6">
          <label>Your Role*</label>
          <select name="organisation_role" id="organisation_role" class="select2 form-control @if(isset($error['organisation_role'][0]) && !empty($error['organisation_role'][0])) is-invalid @endif" style="width: 100%;" disabled>
            @if(isset($organisation_roles) && count($organisation_roles)>0)
            @foreach($organisation_roles as $rolekey=>$rolelabel)
            <optgroup label="{{$rolekey}}">{{$rolekey}}</optgroup>
            @foreach($rolelabel as $organisationrole)  
            <option value="{{$organisationrole->organisation_role_id}}" @if(isset($userdata) && $userdata->organisation_role==$organisationrole->organisation_role_id) {{'selected'}} @endif>{{$organisationrole->description}}</option>
            @endforeach
            @endforeach
            @endif
          </select>
          @if(isset($error['organisation_role'][0]) && !empty($error['organisation_role'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['organisation_role'][0]}}
          </p>
          @endif
        </div>
        <div class="col-12 col-sm-6">
          <label>Organisation Departments</label>
          <select name="organisation_department" id="organisation_department" class="select2 form-control" style="width: 100%;" disabled>
            @if(isset($organisation_department) && count($organisation_department)>0)
            @foreach($organisation_department as $departmentdata)
            <option value="{{$departmentdata->department_id}}" @if(isset($userdata) && $userdata->organisation_department==$departmentdata->department_id) {{'selected'}} @endif>{{$departmentdata->name}}</option>
            @endforeach
            @endif
          </select>
        </div>
        <div class="col-12 col-sm-6 other_organisation_roles" @if($userdata->reason=='7') style="display: block;" @else style="display: none;" @endif >
          <label>Other Role</label>
          <textarea class="form-control @if(isset($error['other_organisation_roles'][0]) && !empty($error['other_organisation_roles'][0])) is-invalid @endif" type="text" name="other_organisation_roles"> {{$userdata->other_organisation_roles}}</textarea>
          @if(isset($error['other_organisation_roles'][0]) && !empty($error['other_organisation_roles'][0]))
          <p class="form-text text-danger text-xs mb-1">
            {{$error['other_organisation_roles'][0]}}
          </p>
          @endif
        </div>
        </div>
        <div class="row">
          <div class="col-12 col-sm-6 field-wrapper address-lookup mt-1">
            <label> Your address </label>
            <p class="text-sm">
            {{$userdata->address_1}}, {{$userdata->address_2}} {{$userdata->address_3}}, {{$userdata->postcode}}, {{$userdata->country}}
            </p>
          </div>
        </div>
        
        
      
    </div>
  </div>
</div>

  @if(isset($activepackages) && count($activepackages)>0)
 <hr class="horizontal dark" />
<div>
  <div class="card mt-4" id="billing-info">
    <div class="card-header">
      <p class="text-sm mb-0 font-weight-bold">Pre Purchased Plans</p>
    </div>
    <div class="card-body pt-0 ps-5">
        

        
        <div class="row mt-4">

            <blockquote class="text-sm"> <i class="fa fa-times-circle text-success" aria-hidden="true"></i> &nbsp; Packages History </blockquote>
            @foreach($activepackages as $ikey=>$packagesdata)
            <div class="col-3 card card shadow-lg mx-2 mt-2">

         
            <ul class="list-group">
               <div class="card-body">
              <li class="list-group-item border-0 justify-content-between p-0 m-0 border-radius-lg">
                <div class="row">
                      <div class="col-auto">
                    
                    
                      <h6 class="mb-1 text-dark text-sm" > {{$ikey+1}} . {{$packagesdata->name}}</h6>
                      <span class="text-xs"><?php echo date('d F Y', strtotime($packagesdata->created_at)); ?>, at <?php echo date('h:m a', strtotime($packagesdata->created_at)); ?></span>
                     
                      <p class="text-sm text-primary mt-1 font-weight-bolder">
                       @if($packagesdata->price > 0) $ {{$packagesdata->price}}.00 AUD @else Free Plan @endif  <span class="mb-1 badge badge-sm bg-success mb-0 mt-2 ms-1"> InActive </span> </p>
                      
                     
                      
                    </div>
                  
                  
                </div>
              </li>
              </div>
              </ul>
            </div>
              @endforeach
            
        
        </div>
        

        
      </div>
    </div>
</div>  
@endif
 <hr class="horizontal dark" />
  
    <div class="card mt-4" >
       <div class="card-header">
        <p class="text-sm mb-0 font-weight-bold">Usage Statistics</p>
       </div>
       <div class="card-body">
        
          <div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
      <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1 ">

        <div class="text-center p-3"><h6>Total  Clients</h6></div>
        <div class="circlechart" data-percentage=""><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="0,100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5"><?php echo isset($totalclients) && !empty($totalclients) ? $totalclients : 0;?></text><text class="circle-chart__subline" x="16.91549431" y="22">

        </text> </g></svg>

        <p class="text-xs h-20 text-dark">

         Number of clients signed up by members

       </p>


     </div>

   </div>
 </div>
 <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
  <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1 ">

    <div class="text-center p-3"><h6>Total  Members</h6></div>
    <div class="circlechart" data-percentage=""><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="0,100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5"><?php echo isset($memberscount) && !empty($memberscount) ? $memberscount : 0;?></text><text class="circle-chart__subline" x="16.91549431" y="22">

    </text> </g></svg>

    <p class="text-xs h-20 text-dark">

     Number of members using the service

   </p>
  

 </div>

</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
  <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1 ">

    <div class="text-center p-3"><h6>Total Pre Purchase Packages</h6></div>
    <div class="circlechart" data-percentage=""><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="0,100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5"><?php echo isset($totalpackages) && !empty($totalpackages) ? $totalpackages : 0;?></text><text class="circle-chart__subline" x="16.91549431" y="22">

    </text> </g></svg>

    <p class="text-xs h-20 text-dark">

     Number of Pre Purchase Packages

   </p>
   

 </div>

</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
  <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1 ">

    <div class="text-center p-3"><h6>Total Coupons</h6></div>
    <div class="circlechart" data-percentage=""><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="0,100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5"><?php echo isset($totalcoupons) && !empty($totalcoupons) ? $totalcoupons : 0;?></text><text class="circle-chart__subline" x="16.91549431" y="22">

    </text> </g></svg>

    <p class="text-xs h-20 text-dark">

     Number of Coupons

   </p>
   

 </div>

</div>
</div>
</div>
        
      </div> 
      </div>                                          
  <hr class="horizontal dark" />
  <div>
  <div class="card mt-4" id="billing-info">
    <div class="card-header">
      <p class="text-sm mb-0 font-weight-bold">Billing History</p>
    </div>
    <div class="card-body pt-0 ps-5">
        @if(isset($receipts) && count($receipts)>0)
        <div class="row">

        
              @foreach($receipts as $receiptdata)
            <div class="col-5 card card shadow-lg mx-2 mt-2">

         
            <ul class="list-group">
               <div class="card-body">
              <li class="list-group-item border-0 justify-content-between p-0 m-0 border-radius-lg">
                <div class="row">
                  <div class="col-auto">
                    
                    
                      <h6 class="mb-1 text-dark text-sm" ><i class="fa fa-check-circle" aria-hidden="true"></i> Receipt #{{$receiptdata->number}}</h6>
                      <span class="text-xs"><?php echo date('d F Y', strtotime($receiptdata->created_at)); ?>, at <?php echo date('h:m a', strtotime($receiptdata->created_at)); ?></span>
                      @if($receiptdata->status=='succeeded')
                      <span class="text-sm text-primary mt-1 ">
                        $ {{$receiptdata->amount}}.00 AUD <span class="mb-1 badge badge-sm bg-success mb-0 mt-2 ms-1">{{$receiptdata->status}} </span> </span>
                      @endif
                      @if($receiptdata->status!=='succeeded')
                      <span class="text-sm text-primary mt-1 ">
                        $ {{$receiptdata->amount_remaining}}.00 AUD <span class="mb-1 badge badge-sm bg-danger mb-0 mt-2 ms-1">Due amount</span> </span>
                      @endif
                    </div>
                  
                  <div class="col-auto">
                    <div class="row">
                      @if($receiptdata->status=='succeeded')
                      <div class="col-12"> <a href="{{$receiptdata->receipt_url}}"> <span class="badge badge-lg bg-primary mb-0 mt-2">View</span> </a> </div>
                      @endif
                      @if($receiptdata->status!=='succeeded')
                      <div class="col-12"> <a href="{{$receiptdata->receipt_url}}"> <span class="badge badge-lg bg-primary mb-0 mt-2">Pay Now</span> </a> </div>
                      @endif
                      
                    </div>
                  </div>
                </div>
              </li>
              </div>
              </ul>
          </div>
              @endforeach
            
        
        </div>
        @else
        <blockquote class="text-sm"> - Sorry no Receipts Found !</blockquote>
        @endif
      </div>
    </div>
</div>


<script src="{{asset('assets/js/js-address-lookup.js')}}?v=1.0.3"></script>


@endsection