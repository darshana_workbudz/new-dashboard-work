@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Our B2B Clients')

@section('headercommon')
<x-main.header 
  icon="contact_card" 
  title="Our B2B Clients" 
  subtitle="Manage your account details"
  button
  buttonText="Invite members"
  buttonIcon="add"
  data-route="{{route('dashboard')}}"
/>
@endsection

@section('content')
<div>
  <div class="row">
    <div class="col-lg-12 position-relative z-index-2">

      <!-- <div class="row mt-4">
            <div class="col-md-6">
              <div class="card bg-primary shadow ">
                <div class="card-body text-center">
                  <h1 class="text-white"><span id="status1" countto="21"> 21</span> </h1>
                  <h6 class="mb-0 font-weight-bolder text-light">Number of our members using the service</h6>
                  <p class="opacity-8 mb-0 text-sm text-warning"></p>
                </div>
              </div>
            </div>
            <div class="col-md-6 mt-md-0 mt-4">
              <div class="card bg-orange">
                <div class="card-body text-center">
                  <h1 class="text-white"> <span id="status2" countto="44"> 44</span> </h1>
                  <h6 class="mb-0 font-weight-bolder text-light">Number of clients who have been signedup by our members</h6>
                  <p class="opacity-8 mb-0 text-sm text-warning"></p>
                </div>
              </div>
            </div>
          </div>
          <div class="row mt-4">
            <div class="col-md-6">
              <div class="card bg-orange">
                <div class="card-body text-center">
                  <h1 class="text-white"><span id="status1" countto="21"> 21</span> </h1>
                  <h6 class="mb-0 font-weight-bolder text-light">The packages our memebers' clients are using</h6>
                  <p class="opacity-8 mb-0 text-sm text-warning"></p>
                </div>
              </div>
            </div>
            <div class="col-md-6 mt-md-0 mt-4">
              <div class="card bg-primary shadow ">
                <div class="card-body text-center">
                  <h1 class="text-white"> <span id="status2" countto="44"> 44</span> </h1>
                  <h6 class="mb-0 font-weight-bolder text-light">Our members' "check-in" email response speed</h6>
                  <p class="opacity-8 mb-0 text-sm text-warning"></p>
                </div>
              </div>
            </div>
          </div> -->


      <div class="col-12 mt-3">
        <div class="card">
          <div class="table-responsive">
            <table class="table align-items-center mb-0">
              <thead>
                <tr>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">First Name</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Last Name</th>
                  <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Email</th>
                  <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Phone</th>
                  <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Total Clients</th>

                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <div class="d-flex px-2 py-1">
                      <div>
                        <img src="../../../assets/img/team-2.jpg" class="avatar avatar-sm me-3" alt="avatar image">
                      </div>
                      <div class="d-flex flex-column justify-content-center">
                        <h6 class="mb-0 text-sm">John</h6>
                      </div>
                    </div>
                  </td>
                  <td>
                    <p class="text-sm text-secondary mb-0">Michael</p>
                  </td>
                  <td>
                    <span class="badge badge-dot me-4">
                      <i class="bg-info"></i>
                      <span class="text-dark text-xs">john@user.com</span>
                    </span>
                  </td>
                  <td class="align-middle text-center text-sm">
                    <p class="text-secondary mb-0 text-sm">1234567890</p>
                  </td>
                  <td class="align-middle text-center">
                    <span class="text-secondary text-sm">3</span>
                  </td>
                </tr>

                <tr>
                  <td>
                    <div class="d-flex px-2 py-1">
                      <div>
                        <img src="../../../assets/img/team-2.jpg" class="avatar avatar-sm me-3" alt="avatar image">
                      </div>
                      <div class="d-flex flex-column justify-content-center">
                        <h6 class="mb-0 text-sm">John</h6>
                      </div>
                    </div>
                  </td>
                  <td>
                    <p class="text-sm text-secondary mb-0">Michael</p>
                  </td>
                  <td>
                    <span class="badge badge-dot me-4">
                      <i class="bg-info"></i>
                      <span class="text-dark text-xs">john@user.com</span>
                    </span>
                  </td>
                  <td class="align-middle text-center text-sm">
                    <p class="text-secondary mb-0 text-sm">1234567890</p>
                  </td>
                  <td class="align-middle text-center">
                    <span class="text-secondary text-sm">3</span>
                  </td>
                </tr>
                <tr>
                  <td>
                    <div class="d-flex px-2 py-1">
                      <div>
                        <img src="../../../assets/img/team-2.jpg" class="avatar avatar-sm me-3" alt="avatar image">
                      </div>
                      <div class="d-flex flex-column justify-content-center">
                        <h6 class="mb-0 text-sm">John</h6>
                      </div>
                    </div>
                  </td>
                  <td>
                    <p class="text-sm text-secondary mb-0">Michael</p>
                  </td>
                  <td>
                    <span class="badge badge-dot me-4">
                      <i class="bg-info"></i>
                      <span class="text-dark text-xs">john@user.com</span>
                    </span>
                  </td>
                  <td class="align-middle text-center text-sm">
                    <p class="text-secondary mb-0 text-sm">1234567890</p>
                  </td>
                  <td class="align-middle text-center">
                    <span class="text-secondary text-sm">3</span>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@section('script')
<script src="{{asset('assets/js/plugins/threejs.js')}}"></script>
<script src="{{asset('assets/js/plugins/orbit-controls.js')}}"></script>
<script type="text/javascript">
  (function() {
    const container = document.getElementById("globe");
    const canvas = container.getElementsByTagName("canvas")[0];

    const globeRadius = 100;
    const globeWidth = 4098 / 2;
    const globeHeight = 1968 / 2;

    function convertFlatCoordsToSphereCoords(x, y) {
      let latitude = ((x - globeWidth) / globeWidth) * -180;
      let longitude = ((y - globeHeight) / globeHeight) * -90;
      latitude = (latitude * Math.PI) / 180;
      longitude = (longitude * Math.PI) / 180;
      const radius = Math.cos(longitude) * globeRadius;

      return {
        x: Math.cos(latitude) * radius,
        y: Math.sin(longitude) * globeRadius,
        z: Math.sin(latitude) * radius
      };
    }

    function makeMagic(points) {
      const {
        width,
        height
      } = container.getBoundingClientRect();

      // 1. Setup scene
      const scene = new THREE.Scene();
      // 2. Setup camera
      const camera = new THREE.PerspectiveCamera(45, width / height);
      // 3. Setup renderer
      const renderer = new THREE.WebGLRenderer({
        canvas,
        antialias: true
      });
      renderer.setSize(width, height);
      // 4. Add points to canvas
      // - Single geometry to contain all points.
      const mergedGeometry = new THREE.Geometry();
      // - Material that the dots will be made of.
      const pointGeometry = new THREE.SphereGeometry(0.5, 1, 1);
      const pointMaterial = new THREE.MeshBasicMaterial({
        color: "#989db5",
      });

      for (let point of points) {
        const {
          x,
          y,
          z
        } = convertFlatCoordsToSphereCoords(
          point.x,
          point.y,
          width,
          height
        );

        if (x && y && z) {
          pointGeometry.translate(x, y, z);
          mergedGeometry.merge(pointGeometry);
          pointGeometry.translate(-x, -y, -z);
        }
      }

      const globeShape = new THREE.Mesh(mergedGeometry, pointMaterial);
      scene.add(globeShape);

      container.classList.add("peekaboo");

      // Setup orbital controls
      camera.orbitControls = new THREE.OrbitControls(camera, canvas);
      camera.orbitControls.enableKeys = false;
      camera.orbitControls.enablePan = false;
      camera.orbitControls.enableZoom = false;
      camera.orbitControls.enableDamping = false;
      camera.orbitControls.enableRotate = true;
      camera.orbitControls.autoRotate = true;
      camera.position.z = -265;

      function animate() {
        // orbitControls.autoRotate is enabled so orbitControls.update
        // must be called inside animation loop.
        camera.orbitControls.update();
        requestAnimationFrame(animate);
        renderer.render(scene, camera);
      }
      animate();
    }

    function hasWebGL() {
      const gl =
        canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
      if (gl && gl instanceof WebGLRenderingContext) {
        return true;
      } else {
        return false;
      }
    }

    function init() {
      if (hasWebGL()) {
        window
        window.fetch("https://raw.githubusercontent.com/creativetimofficial/public-assets/master/soft-ui-dashboard-pro/assets/js/points.json")
          .then(response => response.json())
          .then(data => {
            makeMagic(data.points);
          });
      }
    }
    init();
  })();
</script>
@stop

@endsection
