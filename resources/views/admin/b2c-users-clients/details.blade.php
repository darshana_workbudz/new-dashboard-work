@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','User Dashboard')

@section('headercommon')
<x-main.header 
  icon="contact_card" 
  title="User Dashboard" 
  subtitle="Manage Users Account Details" 
/>
@endsection


@section('content')
<section class="px-3 py-2">
      <div class="row mt-4">

        <!-- Add-On Features -->
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <a data-route="{{route('b2c-users-clients.details.account-details',$user_id)}}" class="link-handle" class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon icon-md border-radius-xl bg-yellow-pale text-center me-2 d-flex align-items-center justify-content-center">
              <span class="fi fi_contact_card text-yellow" style="font-size: 28px"></span>
            </div>
                <div class="description ps-2">
                  <h5>Users Account Details</h5>
                  <span class="text-primary icon-move-right">
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </a>
        </div>

        <!-- Upgrade My Plan -->
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <a data-route="{{route('b2c-users-clients.details.preferences',$user_id)}}" class="link-handle" class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon icon-md border-radius-xl bg-yellow-pale text-center me-2 d-flex align-items-center justify-content-center">
              <span class="fi fi_shield_task text-yellow" style="font-size: 28px"></span>
            </div>
                <div class="description ps-2">
                  <h5>Users Check-in Preferences</h5>
                  <span class="text-primary icon-move-right">
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </a>
        </div>

        <!-- Free up minutes -->
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <a data-route="{{route('b2c-users-clients.details.death-details',$user_id)}}" class="link-handle" class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon icon-md border-radius-xl bg-yellow-pale text-center d-flex align-items-center justify-content-center">
                <span class="fi fi_person_available text-yellow" style="font-size: 28px"></span>
              </div>
                <div class="description ps-2">
                  <h5>Users Details of death</h5>
                  <span class="text-primary icon-move-right">
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </a>
        </div>

        <!-- My Billing &amp; Subscription -->
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <a data-route="{{route('b2c-users-clients.details.recipients',$user_id)}}" class="link-handle" class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon icon-md border-radius-xl bg-yellow-pale text-center d-flex align-items-center justify-content-center">
                <span class="fi fi_people text-yellow" style="font-size: 28px"></span>
              </div>
                <div class="description ps-2">
                  <h5>Users Recipients</h5>
                  <span class="text-primary icon-move-right">
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </a>
        </div>

        

      

      </div>
    </section>
    
    
</div>  
</div>
</div>
<script type="text/javascript">

  $(document).ready(function () {
  $("#confimationdelete").keyup(function () {
    $('.invalid-validation').remove();
  $(this).val($(this).val().toUpperCase());
  });
  });

  function  OpenRedeemModal()
  {
    $('#RedeemModal').modal('show');
  }


  function  HideRedeemModal()
  {
    $('#RedeemModal').modal('hide');
  }

  function RedeemCodeSubmit()
  {         
                $('.invalid-validation').remove();
                var code=$('#evaheld-code').val();
                $.ajax({
                          url: "{{route('my-account.redeem-code-submit',$user_id)}}",
                          dataType: 'json',
                          type: 'POST',
                          data:{code:code},
                          headers: {
                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                          beforeSend: function() {
                            jQuery(document).find('.loadingOverlay').show();
                            
                            jQuery(document).find('.loader').show();
                        },
                        success: function(response) {

                          if(response.status=='success')
                          {
                            HideRedeemModal();
                            swal({
                              title: "Well done!",
                              text: "Code Redeemed Successfully",
                              icon: "success",
                              button: "Ok",
                            });
                            
                          }

                          else{
                           $('#evaheld-code').after('<span class="text-danger text-xs invalid-validation" > '+response.custommessage+' </span>'); 
                          }
                        },
                        error: function(xhr) { // if error occured
                            alert("Error occured.please try again");
                        },
                        complete: function() {

                            $('.select2').select2();
                            jQuery(document).find('.loadingOverlay').hide();
                             jQuery(document).find('.loader').hide();
                            

                        },
                      });
                       
      

  }

  </script>
@endsection