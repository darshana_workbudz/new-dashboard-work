  @extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
  @section('page-type','list-page')
  @section('page-breadcrumb','User Preferences')

  @section('headercommon')
  <x-main.header icon="shield_task" title="User Preferences" subtitle="Manage User Preferences" />
  @endsection

  @section('content')
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <div class="d-lg-flex">
                      <nav aria-label="breadcrumb">
                          <ol class="breadcrumb p-0 bg-white">
                              <li class="breadcrumb-item"><a data-route="{{route('b2c-users-clients.details',$user_id)}}" class="link-handle h5 text-sm font-weight-bolder text-primary">User Dashboard</a></li>
                              @if($default_prefrence=='1')
                              <li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page">My Wellbeing System Preferences</li>
                              @else
                              <li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page">My Trusted Third Party</li>
                              @endif
                          </ol>
                      </nav>
                  </div>
              </div>
              @if($default_prefrence=='1')
              <div class="card">
                  <div class="card-body col-12 pt-0">
                      <div>
                          <div class="info-horizontal">
                              <div class="icon">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                                      <g id="Group_1867" data-name="Group 1867" transform="translate(-16342 -14028)">
                                          <rect id="Rectangle_648" data-name="Rectangle 648" width="54.083" height="55" rx="16" transform="translate(16342 14028)" fill="#f3eef6"></rect>
                                          <path id="Path_3903" data-name="Path 3903" d="M15.668,2a3.59,3.59,0,1,0,3.59,3.59A3.59,3.59,0,0,0,15.668,2ZM27.651,8.486a2.887,2.887,0,0,0-3.751-1.8l-2.308.862a1.077,1.077,0,0,0-.6.547,5.868,5.868,0,0,1-10.607.014,1.077,1.077,0,0,0-.6-.546L7.453,6.688a2.882,2.882,0,0,0-2.1,5.365l4.573,1.881v4.521l-2.832,8.2a2.81,2.81,0,1,0,5.289,1.9l2.651-7.12a.689.689,0,0,1,1.293,0l2.7,7.194a2.787,2.787,0,1,0,5.245-1.889l-2.859-8.3v-4.5L26,12.061A2.887,2.887,0,0,0,27.651,8.486Z" transform="translate(16353.41 14039.002)" fill="#662d91"></path>
                                      </g>
                                  </svg>
                              </div>
                              <div class="description ps-5">
                                  <h5>My Wellbeing System</h5>
                                  @if($default_prefrence=='1')
                                  <span class="badge bg-success text-capitalize">Currently active</span>
                                  @else
                                  <span class="badge bg-secondary text-capitalize">Not active</span>
                                  @endif
                              </div>
                              <p class="text-dark pt-3">Evaheld's Wellbeing System offers a safe and independent method for ensuring your content is released at the right time.</p>
                              <p class="text-dark pt-3">By joining Evaheld, you are automatically enrolled in our Wellbeing System which periodically contacts you. By responding, we know to keep your account active and to continue safeguarding your messages.</p>
                              <p class="text-dark pt-3">If you wish to opt-out, you may nominate a Trusted-Third Party who can notify us when to release your content. <a data-route="{{route('my-authorised-3p-access')}}" class="link-handle text-primary icon-move-right">Learn more</a></p>
                          </div>
                      </div>
                      <hr class="my-4" />
                      <h5>User Preferences</h5>

                      <div class="row">
                          <div class="col-12">
                              <p class="text-sm text-primary font-weight-bolder">How often would you like us to send you Check-in emails?</p>
                              @if(isset($followup_period) && count($followup_period)>0)

                              @foreach($followup_period as $datafollowup)
                              <div class="form-check  form-check-inline">

                                  <input disabled disabled id="followup_period_id" class="form-check-input" type="radio" name="followup_period_id" id="customRadio{{$datafollowup->followup_period_id}}" value="{{$datafollowup->followup_period_id}}" @if((isset($checkinsetting->followup_period_id) && $checkinsetting->followup_period_id==$datafollowup->followup_period_id))
                                  {{'checked'}} @endif
                                  >
                                  <p class="text-dark" for="customRadio{{$datafollowup->followup_period_id}}">{{$datafollowup->name}}</p>
                              </div>
                              @endforeach

                              @endif


                          </div>
                          <div class="col-12">
                              <p class="text-sm text-primary">We rely on you to check for and respond to Evaheld's Wellbeing System's periodic correspondences to safeguard your content and know when to release them.</p>
                              <div class="border-dashed border-1 border-secondary border-radius-md p-3">
                                  <div class="form-check">
                                      <input disabled disabled class=" form-check-input" type="checkbox" name="responsibility_acknowledge" @if((isset($checkinsetting->responsibility_acknowledge) && $checkinsetting->responsibility_acknowledge=='1'))
                                      {{'checked'}} @endif value="1" id="responsibility_acknowledge" >
                                      <p class="text-sm text-dark" for="responsibility_acknowledge">I accept responsibility for confirming that my account is still active and confirm that when I stop responding to Evahelds Wellbeing System correspondences, Evaheld has my permission to release my content according to my preferences.</p>
                                  </div>
                              </div>


                          </div>
                          <div class="mt-3" id="showstep2" @if((isset($checkinsetting->responsibility_acknowledge) && $checkinsetting->responsibility_acknowledge=='1')) style="display: block;" @else style="display: none;" @endif>
                              <div class="row">
                                  <div class="col-lg-12 col-sm-12 mt-3">
                                      <p class="text-sm text-primary font-weight-bolder">If we dont receive your response to periodic Wellbeing System correspondences, what would you like us to do before we release your content?</p>
                                      @if(isset($failed_response_action) && count($failed_response_action)>0)
                                      @foreach($failed_response_action as $datafailed_response_action)
                                      <div class="form-check">
                                          <input disabled disabled value="{{$datafailed_response_action->failed_response_action_id}}" class=" form-check-input" type="radio" name="failed_response_action_id" id="failed_response_action{{$datafailed_response_action->failed_response_action_id}}" @if((isset($checkinsetting->failed_response_action_id) && $checkinsetting->failed_response_action_id==$datafailed_response_action->failed_response_action_id))
                                          {{'checked'}} @endif
                                          >
                                          <p class="text-sm text-dark" for="failed_response_action{{$datafailed_response_action->failed_response_action_id}}">{{$datafailed_response_action->description}}</p>
                                      </div>

                                      @endforeach

                                      @endif

                                  </div>



                                  <div class="col-lg-12 col-sm-12">
                                      <div class="border-dashed border-1 border-secondary border-radius-md p-3">
                                          <div class="form-check">
                                              <input disabled disabled class="form-check-input " type="checkbox" value="1" id="release_message_acknowledge" name="release_message_acknowledge" @if((isset($checkinsetting->release_message_acknowledge) && $checkinsetting->release_message_acknowledge=='1'))
                                              {{'checked'}} @endif

                                              >
                                              <p class="text-sm text-dark">I accept responsibility for confirming that my account is still active and confirm that when I stop responding to Evahelds Wellbeing System correspondences, Evaheld has my permission to release my messages according
                                                  to my message preferences.</p>
                                          </div>
                                      </div>

                                  </div>
                                  <div class="col-lg-12 col-sm-12 mt-3">
                                      <div class="border-dashed border-1 border-secondary border-radius-md p-3">
                                          <div class="form-check">
                                              <input disabled disabled class="form-check-input" type="checkbox" name="final_acknowledge" value="1" @if( (isset($checkinsetting->final_acknowledge) && $checkinsetting->final_acknowledge=='1'))
                                              {{'checked'}} @endif>
                                              <p class="font-weight-bolder text-md text-dark">I confirm that once Evaheld has completed all of the prerequisites above, they have my permission to release all my messages according to my Message preferences. Evaheld does not need to perform any other actions to verify my death. However, if they choose to, they also have my permission to contact the Department of Births, Deaths and Marriages in the applicable state and country, and to use my personal information to confirm my death.</p>
                                          </div>
                                      </div>

                                  </div>
                              </div>
                          </div>

                      </div>

                  </div>
              </div>
              @endif

              @if($default_prefrence=='2')
              <div class="card">

                 <div class="card-body col-12 pt-0">
                      <div>
                          <div class="info-horizontal">
                              <div class="icon">
                                  <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                                      <g id="Group_1864" data-name="Group 1864" transform="translate(-16401 -14025)">
                                          <rect id="Rectangle_649" data-name="Rectangle 649" width="54.083" height="55" rx="16" transform="translate(16401 14025)" fill="#f3eef6"></rect>
                                          <path id="Path_3904" data-name="Path 3904" d="M23.261,15.716a7.545,7.545,0,1,1-7.545,7.545,7.545,7.545,0,0,1,7.545-7.545Zm-7.514,2.743a8.918,8.918,0,0,0,.782,10.646,18.051,18.051,0,0,1-3.556.329c-4.691,0-8.154-1.471-10.269-4.44A3.772,3.772,0,0,1,2,22.807V21.545a3.085,3.085,0,0,1,3.085-3.086H15.748Zm3.885,4.316a.686.686,0,1,0-.97.97L21.4,26.488a.686.686,0,0,0,.97,0L27.861,21a.686.686,0,1,0-.97-.97l-5,5ZM12.972,2A6.859,6.859,0,1,1,6.114,8.863,6.859,6.859,0,0,1,12.972,2Z" transform="translate(16411.637 14036.096)" fill="#662d91"></path>
                                      </g>
                                  </svg>
                              </div>
                              <div class="description ps-5">
                                  <h5>My Trusted Third Party</h5>
                                  @if($default_prefrence=='2') <span class="badge bg-success text-capitalize">Currently Active</span> @else<span class="badge bg-secondary text-capitalize">Not active</span> @endif
                              </div>
                              <p class="text-dark pt-3">Evaheld's Wellbeing System offers a safe and independent method for ensuring your content is released at the right time.</p>
                              <p class="text-dark pt-3">By joining Evaheld, you are automatically enrolled in our Wellbeing System which periodically contacts you. By responding, we know to keep your account active and to continue safeguarding your messages from premature release. <a data-route="{{route('my-checkin-process')}}" class="link-handle text-primary icon-move-right">Learn more about the Wellbeing System.<i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i></a></p>
                              <p class="text-dark pt-3">If you wish to opt-out, you may nominate a Trusted-Third Party who can notify us when to release your content. Complete the form below to get started.</p>
                          </div>
                      </div>
                      <hr class="my-4" />
                      <h5>User Preferences</h5>
                      
                  

                  





                      <div class="row">
                          <input disabled type="hidden" id="step1confirmed" name="step1confirmed" value="<?php isset($authoriseddata) && !empty($authoriseddata) ? $authoriseddata->step1confirmed : 0; ?>">
                          <input disabled type="hidden" id="step2confirmed" name="step2confirmed" value="<?php isset($authoriseddata) && !empty($authoriseddata) ? $authoriseddata->step2confirmed : 0; ?>">
                          <div class="parent-div" id="authorised-profile-details">

                              <input disabled type="hidden" name="user_id" value="{{$userdata->id}}">
                              @if(isset($authoriseddata) && !empty($authoriseddata))
                              <input disabled type="hidden" name="showdiv" value="1">
                              @endif
                              <div class="row p-3">



                                  <div class="col-lg-12 col-sm-12">
                                      <p class="text-sm text-primary font-weight-bolder">Would you like to appoint a Trusted Party to advise us of your death?</p>
                                  </div>
                                  <div class="col-lg-12 col-sm-12">
                                      <div class="form-check form-check-inline">

                                          <input disabled class="nominated_authorised_person_access form-check-input" type="radio" name="nominated_authorised_person_access" id="customRadio1" value="1" @if($old->nominated_authorised_person_access=='1' || (isset($userdata->nominated_authorised_person_access) && $userdata->nominated_authorised_person_access=='1' && $old->nominated_authorised_person_access!=='2') ) {{'checked'}}@endif>
                                          <p class="text-dark" for="customRadio1">Yes</p>

                                      </div>

                                      <div class="form-check form-check-inline">

                                          <input disabled class="nominated_authorised_person_access  form-check-input" type="radio" name="nominated_authorised_person_access" id="customRadio2" value="2" @if($old->nominated_authorised_person_access=='2' || (isset($userdata->nominated_authorised_person_access) && $userdata->nominated_authorised_person_access=='2' && $old->nominated_authorised_person_access!=='1')){{'checked'}}@endif>
                                          <p class="text-dark" for="customRadio2">No</p>

                                      </div>





                                  </div>

                              </div>


                              @if(isset($authoriseddata) && !empty($authoriseddata) && $userdata->nominated_authorised_person_access=='1')

                              <div class="card card-body shadow-lg" id="authorised-profile">

                                  <div class="row justify-content-center align-items-center">

                                      <div class="col-sm-auto col-lg-6 col-sm-12">
                                          <div class="h-100">

                                              <h5 class="mb-1 text-primary">

                                                  {{$authoriseddata->first_name}} {{$authoriseddata->last_name}} <a href="#" class="show-update-form">

                                                  </a>

                                              </h5>

                                              <p class="mb-0 text-sm">
                                                  {{$authoriseddata->email}}
                                              </p>

                                          </div>

                                      </div>

                                      <div class="col-lg-4 col-sm-12 ms-sm-auto mt-sm-0 mt-3 d-flex">

                                          @if($authoriseddata->invitation_status==0)
                                          <div class="d-flex align-items-center">
                                              <button class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fi fi_dismiss" aria-hidden="true"></i></button>
                                              <span class="text-xs">Pending to Accept Invitation</span>
                                          </div>
                                          @endif
                                          @if($authoriseddata->invitation_status==1)
                                          <div class="d-flex align-items-center">
                                              <button class="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-2 btn-sm d-flex align-items-center justify-content-center"><i class="fas fa-check" aria-hidden="true"></i></button>
                                              <span class="text-sm">Accepted Invitation </span>
                                          </div>
                                          @endif
                                      </div>
                                      <hr class="m-2">
                                      <p class="text-xs text-dark mb-2 mt-2">If you have not given Evaheld permission to contact your Trusted Party directly, please send this link to your Trusted Party so that they can accept your appointment. While your Trusted Party does not accept their appointment, you will remain enrolled in Evahelds Wellbeing System.</p>
                                      <div class="d-flex align-items-center">
                                          <div class="form-group w-70">
                                              <div class="input-group bg-gray-200">
                                                  <input disabled id="copy-link" class="form-control form-control-sm" value="{{$authoriseddata->invitation_link}}" type="text" disabled="">

                                              </div>
                                          </div>
                                          <span onclick="copyToClipboard('#copy-link')" class="btn btn-sm bg-primary ms-2 px-3">Copy</span>
                                      </div>

                                  </div>

                                  <hr class="horizontal dark">
                              </div>
                              @endif

                              <div class="col-12 mt-4" id="showbuttondiv" @if($old->nominated_authorised_person_access=='2' || $userdata->nominated_authorised_person_access=='2' && $old->nominated_authorised_person_access!=='1') @else style="display:none;" @endif>



                              </div>

                              <div id="showstep2" class="row mt-2 p-3" @if(($old->nominated_authorised_person_access=='1' ) || ($old->showdiv=='1' ) || ($userdata->nominated_authorised_person_access=='1' && $old->nominated_authorised_person_access!=='2')) @else style="display:none;" @endif>
                                  <div class="d-flex mt-4 card-headline p-3" data-bs-toggle="collapse" data-bs-target="#showauthdetails">
                                      <div class="icon icon-shape icon-sm shadow border-radius-md bg-primary text-center d-flex align-items-center justify-content-center mt-2">
                                          <span class="fi fi_person_available" style="color: #ffffff; font-size: 30px"></span>
                                      </div>
                                      <div class="my-auto ms-3">
                                          <div class="h-100">
                                              <h6 class="mb-0">My Trusted Party's Contact Details &nbsp; <i class="fa fa-edit" style="cursor: pointer;"></i></h6>
                                              <p class="mb-0 text-sm">Step 1</p>
                                          </div>
                                      </div>

                                      @if(isset($authoriseddata) && !empty($authoriseddata) )
                                      <i data-bs-toggle="collapse" data-bs-target="#showauthdetails" class="fi fi_person-edit text-dark ms-auto p-2" aria-hidden="true"></i>
                                      @endif

                                  </div>

                                  <div id="showauthdetails" class="row @if(isset($authoriseddata) && !empty($authoriseddata) && $old->showdiv!==1) collapse @endif">
                                      <h5 class="text-sm text-primary font-weight-bolder mt-3">Provide details about your Trusted Party</h5>
                                      <div class="col-12 col-sm-6  mt-3">
                                          <label class="form-label">Given name <span class="text-danger">*</span></label>
                                          <input disabled placeholder="John" class="form-control " type="text" name="first_name" value="<?php echo isset($authoriseddata->first_name) ? $authoriseddata->first_name : $old->first_name; ?>">

                                      </div>
                                      <div class="col-12 col-sm-6  mt-3">
                                          <label class="form-label">Family name <span class="text-danger">*</span></label>
                                          <input disabled placeholder="Smith" class="form-control " type="text" name="last_name" value="<?php echo isset($authoriseddata->last_name) ? $authoriseddata->last_name : $old->last_name; ?>">

                                      </div>
                                      <div class="col-12 col-sm-6  mt-1">
                                          <label class="form-label">Email address <span class="text-danger">*</span></label>
                                          <input disabled placeholder="example@email.com" class="form-control " type="email" name="email" value="<?php echo isset($authoriseddata->email) ? $authoriseddata->email : $old->email; ?>">

                                      </div>
                                      <div class="col-12 col-sm-6 mt-1">
                                          <div class="row">
                                              <label class="form-label">Phone number <span class="text-danger">*</span></label>
                                              <div class="col-3 mt-2 mt-sm-0 pr-0 mr-0">
                                                  <select name="phone_code" class="phonecodeselect2 form-control" style="width: 100%;">
                                                      @if(isset($phonecodes) && !empty($phonecodes))
                                                      @foreach($phonecodes as $data)
                                                      <option value="{{$data->id}}" data-code="{{$data->iso2}}" @if(isset($authoriseddata->phone_code) && $authoriseddata->phone_code==$data->id){{"selected"}} @elseif($data->iso2=='AU') {{"selected"}} @endif >+{{$data->phone_code}} {{$data->iso2}} </option>
                                                      @endforeach
                                                      @endif

                                                  </select>
                                              </div>
                                              <div class="col-9 mt-2 mt-sm-0 pl-0 ml-0">
                                                  <input disabled placeholder="0412345678" id="phone_number" class="required form-control " type="number" name="phone_number" value="<?php echo isset($authoriseddata->phone_number) ? $authoriseddata->phone_number : $old->phone_number; ?>">

                                              </div>

                                          </div>
                                      </div>
                                      <div class="col-12 col-sm-6  mt-1">
                                          <label class="form-label">Relation to me</label>
                                          <select disabled name="relationship" id="relationship" class="select2 form-control" style="width: 100%;">
                                              @if(isset($relationship) && !empty($relationship))
                                              <option value="">Select</option>
                                              @foreach($relationship as $relationshipdata)
                                              <option value="{{$relationshipdata->relationship_id}}" @if(isset($authoriseddata->relationship_id) && $authoriseddata->relationship_id==$relationshipdata->relationship_id){{"selected"}} @endif >{{ucfirst($relationshipdata->relationship_name)}}</option>
                                              @endforeach
                                              @endif

                                          </select>
                                      </div>
                                      <div class="col-12 col-sm-6  mt-1 otherrelationship" @if($old->relationship=='18' || (isset($authoriseddata->relationship) && $authoriseddata->relationship=='18')) @else style="display:none"; @endif>
                                          <label class="form-label">Other (Relation)</label>
                                          <input disabled placeholder="Daughter-in-law" class="form-control " name="other_relationship" type="text" value="<?php echo isset($authoriseddata->other_relationship) ? $authoriseddata->other_relationship : $old->other_relationship; ?>">

                                      </div>


                                      <div class="col-12 col-sm-6"></div>
                                      <div class="row">
                                          <div class="col-12 col-sm-6 field-wrapper address-lookup mt-1">
                                              <label class="form-label">Address </label>


                                              {{$userdata->address_1}}, {{$userdata->address_2}} {{$userdata->address_3}}, {{$userdata->postcode}}, {{$userdata->country}}


                                          </div>
                                      </div>


                                      <div class="row show-manual-div" style="display: none;">
                                          <input disabled class="form-control" id="manual-address" name="manual" type="hidden" value="0">
                                          <input disabled class="form-control" name="city" type="hidden">
                                          <input disabled class="form-control" name="county" type="hidden">
                                          <div class="col-12 col-sm-6">
                                              <label class="form-label">Address Line 1</label>
                                              <input disabled placeholder="98 Shirley Street" class="form-control " type="text" name="address_line_1" value="<?php echo isset($authoriseddata->address_1) ? $authoriseddata->address_1 : ''; ?>">

                                          </div>

                                          <div class="col-12 col-sm-6">
                                              <label class="form-label">Address Line 2</label>
                                              <input disabled placeholder="PIMPAMA" class="form-control " type="text" name="address_line_2" value="<?php echo isset($authoriseddata->address_2) ? $authoriseddata->address_2 : ''; ?>">

                                          </div>
                                          <div class="col-12 col-sm-6">
                                              <label class="form-label">Postcode</label>
                                              <input disabled placeholder="4209" class="form-control " name="postcode" type="number" value="<?php echo isset($authoriseddata->postcode) ? $authoriseddata->postcode : ''; ?>">

                                          </div>
                                          <div class="col-12 col-sm-6">
                                              <label class="form-label">State</label>
                                              <input disabled placeholder="QLD" class="form-control" name="state" type="text" value="<?php echo isset($authoriseddata->state) ? $authoriseddata->state : ''; ?>">

                                          </div>
                                          <div class="col-12 col-sm-6">
                                              <label class="form-label">Country</label>
                                              <select name="country" id="country" class="select2 form-control " style="width: 100%;">
                                                  @if(isset($phonecodes) && !empty($phonecodes))
                                                  <option value="">Select</option>
                                                  @foreach($phonecodes as $data)
                                                  <option value="{{$data->iso2}}" @if(isset($authoriseddata->country) && $authoriseddata->country==$data->iso2){{"selected"}} @endif > {{$data->name}} </option>
                                                  @endforeach
                                                  @endif
                                              </select>
                                          </div>
                                      </div>
                                      <hr class="mt-4" />
                                      <div class="row">
                                          <div class="col-12 col-sm-12 mt-3">
                                              <p class="text-sm text-primary font-weight-bolder">Is your Trusted Party also your Solicitor/Lawyer or your Power of Attorney? <span class="text-danger">*</span></p>
                                          </div>

                                          <div class="col-12 col-sm-6">
                                              <div class="form-check form-check-inline">

                                                  <input disabled class="islawyer  form-check-input islawyer" type="radio" name="islawyer" id="customRadio3" value="1" @if(( isset($authoriseddata->islawyer) && $authoriseddata->islawyer=='1' && $old->islawyer!=='2')){{'checked'}}@endif>
                                                  <p class="text-dark" for="customRadio3">Yes</p>
                                              </div>

                                              <div class="form-check form-check-inline">

                                                  <input disabled class="islawyer  form-check-input islawyer" type="radio" name="islawyer" id="customRadio4" value="2" @if($old->islawyer=='2' || ( isset($authoriseddata->islawyer) && $authoriseddata->islawyer=='2' && $old->islawyer!=='1')){{'checked'}}@endif>
                                                  <p class="text-dark" for="customRadio4">No</p>
                                              </div>



                                          </div>

                                      </div>

                                      <div class="row organisation-div" @if($old->islawyer=='1' || ( isset($authoriseddata->islawyer) && $authoriseddata->islawyer=='1')) @else style="display: none;" @endif>
                                          <h5 class="text-sm text-primary font-weight-bolder mt-3"><i class="text-success"></i>Organisation Details</h5>
                                          <div class="col-12 col-sm-6  mt-1">
                                              <label class="form-label">Business name <span class="text-danger">*</span></label>
                                              <input disabled placeholder="Acme Solicitors" class="form-control " name="organisation" type="text" value="<?php echo isset($authoriseddata->organisation) ? $authoriseddata->organisation : ''; ?>">

                                          </div>
                                          <div class="col-12 col-sm-6 mt-1">
                                              <div class="row">
                                                  <label class="form-label">Phone number <span class="text-danger">*</span></label>
                                                  <div class="col-3 mt-2 mt-sm-0 pr-0 mr-0">
                                                      <select name="organisation_phonecode" class="phonecodeselect1 form-control" style="width: 100%;">
                                                          @if(isset($phonecodes) && !empty($phonecodes))
                                                          @foreach($phonecodes as $data)
                                                          <option value="{{$data->id}}" data-code="{{$data->iso2}}" @if(isset($authoriseddata->phone_code) && $authoriseddata->phone_code==$data->id){{"selected"}} @elseif($data->iso2=='AU') {{"selected"}} @endif >+{{$data->phone_code}} {{$data->iso2}} </option>
                                                          @endforeach
                                                          @endif

                                                      </select>
                                                  </div>
                                                  <div class="col-9 mt-2 mt-sm-0 pl-0 ml-0">
                                                      <input disabled placeholder="0712345678" id="organisation_phonenumber" class="required form-control " type="number" name="organisation_phonenumber" value="<?php echo isset($authoriseddata->organisation_phonenumber) ? $authoriseddata->organisation_phonenumber : $old->organisation_phonenumber; ?>">

                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-12 col-sm-6 field-wrapper address-lookup mt-1">
                                              <label class="form-label">Address <span class="text-danger">*</span></label>
                                              <input disabled placeholder="98 Shirley Street PIMPAMA QLD 4209" class="form-control address-lookup-organisation " type="text" name="organisation_address" autocomplete="off" value="<?php echo isset($authoriseddata->organisation_address) ? $authoriseddata->organisation_address : null; ?>">

                                          </div>

                                      </div>
                                      <div class="col-12 mt-4">



                                      </div>

                                  </div>
                              </div>





                          </div>
                          @if(isset($authoriseddata) && !empty($authoriseddata) && $userdata->nominated_authorised_person_access=='1')
                          <div id="showdiv3" class="sub-div mt-3">

                              <div class="d-flex card-headline p-3" data-bs-toggle="collapse" data-bs-target="#showprovisiondetails">
                                  <div class="icon icon-shape icon-sm shadow border-radius-md bg-primary text-center d-flex align-items-center justify-content-center  me-2 mt-2">
                                      <span class="fi fi_shield_task" style="color: #ffffff; font-size: 30px"></span>
                                  </div>
                                  <div class="my-auto ms-3">
                                      <div class="h-100">
                                          <h6 class="mb-0">My Trusted Party's Permissions &nbsp; <i class="fa fa-edit" style="cursor: pointer;"></i></h6>
                                          <p class="mb-0 text-sm">Step 2</p>
                                      </div>
                                  </div>
                                  @if($authoriseddata->step2confirmed==1)
                                  <i class="fi fi_person-edit text-dark ms-auto p-2" data-bs-toggle="collapse" data-bs-target="#showprovisiondetails" aria-hidden="true"></i>
                                  @endif
                              </div>
                              <hr class="horizontal dark mt-2">
                              <div id="showprovisiondetails" class="p-3 @if($authoriseddata->step2confirmed==1 && $old->showdiv!==2) collapse @endif">
                              <input disabled type="hidden" name="user_id" value="{{$userdata->id}}">

                              <input disabled type="hidden" name="showdiv" value="2">
                              
                              <div class="row mt-3">
                                  <div class="col-12 col-sm-12 ">
                                      <p class="text-sm text-primary font-weight-bolder">Would you like your Trusted Party to have access to your account after your death?</p>
                                  </div>

                                  <div class="col-lg-12 col-sm-12">
                                      <div class="form-check form-check-inline">

                                          <input disabled class="giveaccountaccess  form-check-input" type="radio" name="giveaccountaccess" id="customRadio1" value="1" @if((isset($authoriseddata->giveaccountaccess) && $authoriseddata->giveaccountaccess=='1')) {{'checked'}}@endif>
                                          <p class="text-dark" for="customRadio1">Yes</p>

                                      </div>

                                      <div class="form-check form-check-inline">

                                          <input disabled class="giveaccountaccess  form-check-input" type="radio" name="giveaccountaccess" id="customRadio2" value="2" @if((isset($authoriseddata->giveaccountaccess) && $authoriseddata->giveaccountaccess=='2')){{'checked'}}@endif>
                                          <p class="text-dark" for="customRadio2">No</p>

                                      </div>

                                  </div>
                              </div>

                              <div class="row" id="authorisedaccessdiv" @if((isset($authoriseddata->giveaccountaccess) && $authoriseddata->giveaccountaccess=='1' && $old->giveaccountaccess!=='2')) @else style="display: none;" @endif>
                                  <div class="col-12 col-sm-12 ">
                                      <p class="text-sm text-primary font-weight-bolder">What kind of access would you like your Trusted Party to have?</p>
                                  </div>
                                  <div class="col-lg-12 col-sm-12  ">
                                      <select name="authorised_access_type" id="authorised_access_type" class="select2 form-control select2" style="width: 100%;">
                                          <option value="">Select</option>
                                          <option value="1" @if((isset($authoriseddata->authorised_access_type) && $authoriseddata->authorised_access_type=='1')){{"selected"}}@endif >They can not have any access to my account upon advising of my death, and at this time Evaheld can release my messages directly to their allocated recipients</option>

                                          <option value="2" @if((isset($authoriseddata->authorised_access_type) && $authoriseddata->authorised_access_type=='2')){{"selected"}}@endif >They can not view any recipient details or messages, they are only allowed to give authorisation for my messages to be released to their allocated recipients directly by Evaheld</option>

                                          <option value="3" @if((isset($authoriseddata->authorised_access_type) && $authoriseddata->authorised_access_type=='3')){{"selected"}}@endif >They can only view recipients and update their details as well as release their message/s, if the recipient is to receive a message that was selected as being able to be released by the Trusted Party</option>

                                          <option value="4" @if((isset($authoriseddata->authorised_access_type) && $authoriseddata->authorised_access_type=='4')){{"selected"}}@endif >They can have full account access, except the ability to view messages and to see the recipients and messages for which i have chosen a direct link to be sent from Evaheld</option>

                                          <option value="5" @if((isset($authoriseddata->authorised_access_type) && $authoriseddata->authorised_access_type=='5')){{"selected"}}@endif >They can have full account access, including viewing messages, except for the recipients and messages for which i have chosen a direct link to be sent from Evaheld</option>

                                          <option value="6" @if((isset($authoriseddata->authorised_access_type) && $authoriseddata->authorised_access_type=='2')){{"selected"}}@endif >They can have full account access, including viewing all messages and all recipients, but they cannot delete any messages or recipients</option>

                                          <option value="7" @if((isset($authoriseddata->authorised_access_type) && $authoriseddata->authorised_access_type=='7')){{"selected"}}@endif >They can have full account access and control, including adding or deleting recipients and messages</option>

                                      </select>

                                  </div>

                                  <div class="row mt-3" id="aigcanemaildiv" @if((isset($authoriseddata->authorised_access_type) && !empty($authoriseddata->authorised_access_type))) @else style="display:none;" @endif>
                                      <div class="col-12 col-sm-12 ">
                                          <p class="text-sm text-primary font-weight-bolder">Can Evaheld email your Trusted Party on your behalf?</p>
                                      </div>

                                      <div class="col-lg-12 col-sm-12">
                                          <div class="form-check form-check-inline">

                                              <input disabled class="aigcanemail  form-check-input" type="radio" name="aigcanemail" id="customRadio1" value="1" @if($old->aigcanemail=='1' || (isset($authoriseddata->aigcanemail) && $authoriseddata->aigcanemail=='1')) {{'checked'}}@endif>
                                              <p class="text-dark" for="customRadio1">Yes</p>

                                          </div>

                                          <div class="form-check form-check-inline">

                                              <input disabled class="aigcanemail  form-check-input" type="radio" name="aigcanemail" id="customRadio2" value="2" @if((isset($authoriseddata->aigcanemail) && $authoriseddata->aigcanemail=='2')){{'checked'}}@endif>
                                              <p class="text-dark" for="customRadio2">No</p>

                                          </div>


                                      </div>
                                  </div>


                              </div>

                              <div class="row" id="verificationprocessdiv" @if(( !empty($authoriseddata->authorised_access_type)) || (isset($authoriseddata->giveaccountaccess) && !empty($authoriseddata->giveaccountaccess))) @else style="display: none;" @endif>
                                  <div class="col-lg-6 col-sm-12 mt-4">
                                      <p class="text-sm text-primary font-weight-bolder">If your Trusted Party contacts us to advise us of your passing - but does not use the link provided link-what should we do? </p>
                                  </div>

                                  <div class="col-lg-6 col-sm-12 mt-4">
                                      <select name="verification_process_step_id" id="verification_process_step" class="select2 form-control " style="width: 100%;">
                                          @if(isset($verification_process_step) && !empty($verification_process_step))
                                          <option value="">Select</option>
                                          @foreach($verification_process_step as $verification_process_stepdata)
                                          <option value="{{$verification_process_stepdata->verification_process_step_id}}" @if($old->verification_process_step_id==$verification_process_stepdata->verification_process_step_id || (isset($authoriseddata->verification_process_step_id) && $authoriseddata->verification_process_step_id==$verification_process_stepdata->verification_process_step_id)) {{'selected'}} @endif

                                              >{{$verification_process_stepdata->description}}</option>
                                          @endforeach
                                          @endif



                                      </select>


                                  </div>
                              </div>

                              <div id="showverificationstep1" class="mt-4" @if(isset($authoriseddata->verification_process_step_id) && ($authoriseddata->verification_process_step_id=='1')) @else style="display: none;" @endif>
                                  <p class="text-sm text-primary font-weight-bolder">Set up your Trusted Partys verification password and dont forget to give it to them!</p>
                                  <div class="row">
                                      <div class="col-12 col-sm-6 ">
                                          <label class="form-label">Create Password </label>
                                          <input disabled class="password form-control " type="text" name="password" id="pwd" value="<?php echo isset($authoriseddata->password) && !empty($authoriseddata->password) ? $authoriseddata->password : $old->password; ?>">


                                          <div id="pwd_strength_wrap" class="arrow-top">
                                              <div id="passwordDescription" class="text-sm">Password not entered</div>


                                          </div>


                                      </div>

                                      <div class="col-12 col-sm-6 ">
                                          <label class="form-label">Confirm Password </label>
                                          <input disabled class="password_confirmation form-control" type="text" name="password_confirmation" value="<?php echo isset($authoriseddata->password) && !empty($authoriseddata->password) ? $authoriseddata->password : $old->password_confirmation; ?>">

                                      </div>
                                      <div class="col-12 col-sm-6 ">
                                          <div id="progress-strength">
                                              <div id="progress-strength-bar-individual"></div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                              <div id="showverificationstep2" class="row mt-4" @if(isset($authoriseddata->verification_process_step_id) && ($authoriseddata->verification_process_step_id=='2')) @else style="display: none;" @endif>
                                  <p class="text-lg text-dark mb-0">Verification Questions</p>
                                  <p class="text-sm text-primary font-weight-bolder mt-2">
                                      Set up your recipients verification questions and dont forget to give it to them!</p>
                                  <div class="col-12 col-sm-6">
                                      <label class="form-label">Verification Question 1</label>
                                      <select name="verification_question_1" class="form-control " style="width: 100%;">
                                          @if(isset($verification_questions) && !empty($verification_questions))
                                          <option value="">Please Select</option>
                                          @foreach($verification_questions as $verification_questionsdata)
                                          <option value="{{$verification_questionsdata->verification_question_id}}" @if($old->verification_question_1==$verification_questionsdata->verification_question_id || isset($authoriseddata->verification_question_1) && $authoriseddata->verification_question_1==$verification_questionsdata->verification_question_id){{'selected'}} @endif
                                              >{{$verification_questionsdata->description}}</option>
                                          @endforeach
                                          @endif
                                      </select>

                                  </div>

                                  <div class="col-12 col-sm-6">
                                      <label class="form-label">Answer Question 1</label>
                                      <input disabled class="form-control " type="text" name="verification_answer_1" value="<?php echo isset($authoriseddata->verification_answer_1) && !empty($authoriseddata->verification_answer_1) ? $authoriseddata->verification_answer_1 : $old->verification_answer_1; ?>">

                                  </div>
                                  <div class="col-12 col-sm-6">
                                      <label class="form-label">Verification Question 2</label>
                                      <select name="verification_question_2" class="form-control " style="width: 100%;">
                                          @if(isset($verification_questions) && !empty($verification_questions))
                                          <option value="">Please Select</option>
                                          @foreach($verification_questions as $verification_questionsdata2)
                                          <option value="{{$verification_questionsdata2->verification_question_id}}" @if($old->verification_question_2==$verification_questionsdata2->verification_question_id || isset($authoriseddata->verification_question_2) && $authoriseddata->verification_question_2==$verification_questionsdata2->verification_question_id){{'selected'}} @endif>{{$verification_questionsdata2->description}}</option>
                                          @endforeach
                                          @endif
                                      </select>

                                  </div>
                                  <div class="col-12 col-sm-6">
                                      <label class="form-label">Answer Question 2</label>
                                      <input disabled class="form-control " type="text" name="verification_answer_2" value="<?php echo isset($authoriseddata->verification_answer_2) && !empty($authoriseddata->verification_answer_2) ? $authoriseddata->verification_answer_2 : $old->verification_answer_2; ?>">

                                  </div>
                                  <div class="col-12 col-sm-6">
                                      <label class="form-label">Verification Question 3</label>
                                      <select name="verification_question_3" class="form-control " style="width: 100%;">
                                          @if(isset($verification_questions) && !empty($verification_questions))
                                          <option value="">Please Select</option>
                                          @foreach($verification_questions as $verification_questionsdata3)
                                          <option value="{{$verification_questionsdata3->verification_question_id}}" @if($old->verification_question_3==$verification_questionsdata3->verification_question_id || isset($authoriseddata->verification_question_3) && $authoriseddata->verification_question_3==$verification_questionsdata3->verification_question_id){{'selected'}} @endif>{{$verification_questionsdata3->description}}</option>
                                          @endforeach
                                          @endif
                                      </select>

                                  </div>
                                  <div class="col-12 col-sm-6">
                                      <label class="form-label">Answer Question 3</label>
                                      <input disabled class="form-control " type="text" name="verification_answer_3" value="<?php echo isset($authoriseddata->verification_answer_3) && !empty($authoriseddata->verification_answer_3) ? $authoriseddata->verification_answer_3 : $old->verification_answer_3; ?>">

                                  </div>
                              </div>
                              <div id="verification_to_releasemessagediv" @if(!empty($authoriseddata->verification_to_releasemessage)) @else style="display: none;" @endif >
                                  <div class="col-lg-12 col-sm-12 mt-4">
                                      <p class="text-sm text-primary font-weight-bolder"> If my Trusted Party is not verified and cannot pass any of the Verification Questions, Evaheld has permission to contact the Department of Births, Deaths and Marriages in the applicable state and country, to confirm my death using my name and date of birth. If my death is confirmed, I agree that Evaheld can release my messages as per the preferences I selected for each message.</p>
                                  </div>
                                  <div class="col-lg-12 col-sm-12">
                                      <div class="form-check form-check-inline">

                                          <input disabled class="verification_to_releasemessage  form-check-input" type="radio" name="verification_to_releasemessage" id="customRadio1" value="1" @if((isset($authoriseddata->verification_to_releasemessage) && $authoriseddata->verification_to_releasemessage=='1')) {{'checked'}}@endif>
                                          <p class="text-dark" for="customRadio1">Yes</p>
                                      </div>
                                      <div class="form-check form-check-inline">
                                          <input disabled class="verification_to_releasemessage  form-check-input" type="radio" name="verification_to_releasemessage" id="customRadio2" value="2" @if((isset($authoriseddata->verification_to_releasemessage) && $authoriseddata->verification_to_releasemessage=='2')){{'checked'}}@endif>
                                          <p class="text-dark" for="customRadio2">No</p>
                                      </div>

                                  </div>
                              </div>

                          </div>
                          </div>

                          @endif
                      
                  </div>
              </div>
              @endif


          </div>
      </div>


      @endsection