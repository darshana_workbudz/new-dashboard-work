@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Our b2cusers')

@section('headercommon')
<x-main.header 
icon="people" 
title="B2C User Clients" 
subtitle="Manage B2C User Clients"
/>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-12 mt-lg-0">

		<div class="row mt-3">
			@if(count($b2cusers)>0)
			@foreach ($b2cusers as $rkey => $b2cuserdata)
			<div class="col-lg-4 col-md-6 mb-4">
				<div class="card bg-white-100 shadow-lg">
					<div class="card-body p-3">
						<div class="d-flex">
							<!-- Avatar Large -->
							<div class="avatar avatar-xl position-relative border-radius-xl bg-blue-pale d-none d-lg-block d-xl-block cursor-pointer" >
								<div class="card-body z-index-3 text-center p-3">
									<i class="fi fi_person_available text-blue" style="font-size: 42px;" aria-hidden="true"></i>
								</div>
							</div>
							<!-- Avatar Small -->
							<div class="avatar avatar-lg position-relative border-radius-xl bg-blue-pale d-block d-lg-none d-xl-none cursor-pointer" >
								<div class="card-body z-index-3 text-center p-2 py-3">
									<i class="fi fi_person_available text-blue" style="font-size: 28px" aria-hidden="true"></i>
								</div>
							</div>
							<div class="ms-3 my-auto">
								<div class="d-flex flex-column justify-content-center">

									<h6 class="mb-0 ">{{ucfirst($b2cuserdata->organisation_name
									)}} </h6>


								</div>
								<div class="avatar-group">


									<p class="mb-0 mt-0 text-sm">{{ucfirst($b2cuserdata->first_name
										)}} {{ucfirst($b2cuserdata->last_name)}}</p>

										<p class="text-sm text-primary mb-0">{{$b2cuserdata->email}}</p>
										<div class="col-6 ">
									@if($b2cuserdata->phone_number)
									<h6 class="text-xs mb-0 mx-1"><i class="fa fa-mobile" aria-hidden="true"></i>&nbsp; +{{$b2cuserdata->phone_code}} {{$b2cuserdata->phone_number}} </h6>
									@endif

									@if($b2cuserdata->isactive==1)

									<i class="fa fa-check-circle-o text-success mt-1" aria-hidden="true"></i>
									<span class="text-xs text-dark">Active</span>

									@elseif($b2cuserdata->isactive==2)

									<i class="fa fa-times-circle-o text-danger mt-1" aria-hidden="true"></i>
									<span class="text-xs text-dark">De Activated</span>

									@elseif($b2cuserdata->isactive==3)

									<i class="fa fa-times-circle-o text-danger mt-1" aria-hidden="true"></i>
									<span class="text-xs text-dark">Passed Out</span>

									@else

									<i class="fa fa-times-circle-o text-danger mt-1" aria-hidden="true"></i>
									<span class="text-xs text-dark">In Active</span>

									@endif
								</div>
									</div>
								</div>

								
								<div class="ms-auto">
									<div class="dropdown">
										<a href="{{route('b2c-users-clients.details',$b2cuserdata->id)}}" class="btn text-secondary  border-shadow p-2" >
											<i class="fa fa-arrow-right text-lg text-alternate-primary" aria-hidden="true"></i>
										</a>

									</div>
								</div>
							</div>
							<p class="text-sm mt-3">{{$b2cuserdata->address_1}} {{$b2cuserdata->address_2}} {{$b2cuserdata->address_3}}  {{$b2cuserdata->postcode}} {{$b2cuserdata->country}}</p>
							<hr class="horizontal dark">
							

							<div class="row mt-4">
								<div class="col-6 text-start">
									<h6 class="text-xs mb-0">{{date('d.m.Y',strtotime($b2cuserdata->created_at))}}</h6>
									<p class="text-primary text-xs font-weight-bold mb-0">Registeration date</p>
								</div>
								@if($b2cuserdata->passed_date)
								<div class="col-6 text-end">
									<h6 class="text-xs mb-0">{{date('d.m.Y',strtotime($b2cuserdata->passed_date))}}</h6>
									<p class="text-primary text-xs font-weight-bold mb-0">Passed Date</p>
								</div>
								@endif
								@if($b2cuserdata->deactivation_date)
								<div class="col-6 text-end">
									<h6 class="text-xs mb-0">{{date('d.m.Y',strtotime($b2cuserdata->deactivation_date))}}</h6>
									<p class="text-primary text-xs font-weight-bold mb-0">Deactivation Date</p>
								</div>
								@endif
							</div>
							
						</div>
					</div>
				</div>
				@endforeach

				@else
				<section class="px-3 py-2">

					<div class="row mt-4">


						<div class="col-lg-12 col-md-12 d-flex flex-column mt-3 border-radius-lg text-center">

							<div class="card h-100 card-background">
								<div class="full-background"></div>
								<div class="card-body z-index-3 text-center">
									<h6 class="text-dark mb-4 mt-3">No B2C User Clients Found</h6> 

								</div>

							</div>
						</div>
					</div>
				</section>

				@endif

				{!! $b2cusers->appends($old->all)->links('pagination')!!}

			</div>
		</div>

	</div>


	
	
	@stop
