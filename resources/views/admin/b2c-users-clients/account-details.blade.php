@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','User Dashboard')

@section('headercommon')
<x-main.header icon="contact_card" title="User Account Details" subtitle="Here you can manage User Account Details" />
@endsection

@section('content')
<div>
  <div class="card mt-4" id="basic-info">
    <div class="card-header pb-0">
        <nav style="--bs-breadcrumb-divider: '›';" aria-label="breadcrumb">
          <ol class="breadcrumb p-0 bg-white">
            <li class="breadcrumb-item"><a data-route="{{route('b2c-users-clients.details',$user_id)}}" class="link-handle h5 text-sm font-weight-bolder text-primary">User Dashboard</a></li>
            <li class="breadcrumb-item active h5 text-sm font-weight-bolder" aria-current="page">User Account Details</li>
          </ol>
        </nav>
      </div>
    <div class="card-body pt-0">
      
        <div class="row">
          <div class="col-12 col-sm-6">
            <label>Given name <span class="text-danger">*</span></label>
            <input class="form-control @if(isset($error['first_name'][0]) && !empty($error['first_name'][0])) is-invalid @endif" type="text" name="first_name" value="{{$userdata->first_name}}" placeholder="Jane" disabled />
            @if(isset($error['first_name'][0]) && !empty($error['first_name'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['first_name'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 col-sm-6">
            <label>Family name <span class="text-danger">*</span></label>
            <input class="form-control @if(isset($error['last_name'][0]) && !empty($error['last_name'][0])) is-invalid @endif" type="text" name="last_name" value="{{$userdata->last_name}}" placeholder="Smith" disabled />
            @if(isset($error['last_name'][0]) && !empty($error['last_name'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['last_name'][0]}}
            </p>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-sm-6  mt-1">
            <label>Email address <span class="text-danger">*</span></label>
            <input class="form-control" type="text" name="email" value="{{$userdata->email}}"  placeholder="example@email.com" disabled />
            @if(isset($error['email'][0]) && !empty($error['email'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['email'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 col-sm-6  mt-1">
            <div class="row">
              <label>Phone number</label>
              <div class="col-3 mt-3 mt-sm-0 pr-0 mr-0">
                <select name="phone_code" class="phonecodeselect2 form-control" style="width: 100%;" disabled>
                  @if(isset($phonecodes) && !empty($phonecodes))
                  @foreach($phonecodes as $data)
                  <option value="{{$data->id}}" data-code="{{$data->iso2}}" @if(isset($userdata->phone_code) && $userdata->phone_code==$data->iso2){{"selected"}} @elseif($data->iso2=='AU') {{"selected"}} @endif >+{{$data->phone_code}} {{$data->iso2}} </option>
                  @endforeach
                  @endif

                </select>
              </div>
              <div class="col-9 mt-3 mt-sm-0 pl-0 ml-0 mb-4">
                <input disabled id="phone_number" class="required form-control @if(isset($error['phone_number'][0]) && !empty($error['phone_number'][0])) is-invalid @endif" type="number" name="phone_number" value="{{$userdata->phone_number}}" placeholder="0412345678" />
                @if(isset($error['phone_number'][0]) && !empty($error['phone_number'][0]))
                <p class="form-text text-danger text-xs mb-1">
                  {{$error['phone_number'][0]}}
                </p>
                @endif
              </div>
            </div>
          </div>
        </div>
        <hr class="horizontal dark" />
        
        <div class="row mt-1">
          <div class="col-12 col-sm-6">
            <label>Date of birth</label>
            <div id="datepicker" class="input-group-alternative date custom-datepicker" data-date-format="dd-mm-yyyy">
              <input required type="text" id="dob-date" value="<?php echo isset($userdata->dob) && !empty($userdata->dob) ? date('d-m-Y', strtotime($userdata->dob)) : '' ?>" name="dob" class=" date-validation form-control  @if(isset($error['dob'][0]) && !empty($error['dob'][0])) is-invalid @endif" placeholder="dd-mm-yyyy" / disabled>
              <span class="input-group-addon">
              </span>
            </div>
            @if(isset($error['dob'][0]) && !empty($error['dob'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['dob'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 col-sm-6"></div>
          <div class="col-12 col-sm-6">
            <label>Reason I am creating my account</label>
            <select name="reason" id="reason" class="select2 form-control" style="width: 100%;" disabled>
              @if(isset($reason) && count($reason)>0)
              @foreach($reason as $reasondata)
              <option value="{{$reasondata->reason_id}}" @if(isset($userdata) && $userdata->reason==$reasondata->reason_id) {{'selected'}} @endif>{{$reasondata->description}}</option>
              @endforeach
              @endif
            </select>
          </div>
          <div class="col-12 col-sm-6">
            <label>My status</label>
            <select name="mystatus" id="mystatus" class="select2 form-control" style="width: 100%;" disabled>
              @if(isset($mystatus) && count($mystatus)>0)
              @foreach($mystatus as $mystatusdata)
              <option value="{{$mystatusdata->status_id}}" @if(isset($userdata) && $userdata->mystatus==$mystatusdata->status_id) {{'selected'}} @endif>{{$mystatusdata->description}}</option>
              @endforeach
              @endif
            </select>
          </div>
          <div class="col-12 col-sm-6 other_reason" @if($userdata->reason=='7') style="display: block;" @else style="display: none;" @endif >
            <label>Other (Reason for creating the Account)</label>
            <textarea class="form-control @if(isset($error['other_reason'][0]) && !empty($error['other_reason'][0])) is-invalid @endif" type="text" name="other_reason"> {{$userdata->other_reason}}</textarea>
            @if(isset($error['other_reason'][0]) && !empty($error['other_reason'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['other_reason'][0]}}
            </p>
            @endif
          </div>
          <div class="col-12 col-sm-6 other_mystatus" @if($userdata->mystatus=='7') style="display: block;" @else style="display: none;" @endif >
            <label>Other Option (My Status)</label>
            <textarea class="form-control @if(isset($error['other_mystatus'][0]) && !empty($error['other_mystatus'][0])) is-invalid @endif" type="text" name="other_mystatus" col="2"> {{$userdata->other_mystatus}}</textarea>
            @if(isset($error['other_mystatus'][0]) && !empty($error['other_mystatus'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['other_mystatus'][0]}}
            </p>
            @endif
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-sm-6 field-wrapper address-lookup mt-1">
            <label> Your address </label>
            <p class="text-sm">
            {{$userdata->address_1}}, {{$userdata->address_2}} {{$userdata->address_3}}, {{$userdata->postcode}}, {{$userdata->country}}
            </p>
          </div>
        </div>
        
        
      
    </div>
  </div>
</div>


 <hr class="horizontal dark" />
<div>
  <div class="card mt-4" id="billing-info">
    <div class="card-header">
      <p class="text-sm mb-0 font-weight-bold">User Plan Details</p>
    </div>
    <div class="card-body pt-0 ps-5">
        <div class="row">
        <blockquote class="text-sm"> <i class="fa fa-check-circle text-success" aria-hidden="true"></i>   &nbsp;Active Plans </blockquote>
        @if(isset($user_active_packages) && !empty($user_active_packages))
        <div class="col-3 card card shadow-lg mx-2 mt-2">

         
            <ul class="list-group">
               <div class="card-body">
              <li class="list-group-item border-0 justify-content-between p-0 m-0 border-radius-lg">
                <div class="row">
                      <div class="col-auto">
                    
                    
                      <h6 class="mb-1 text-dark text-sm" > 1 . {{$user_active_packages->name}}</h6>
                      <span class="text-xs"><?php echo date('d F Y', strtotime($user_active_packages->created_at)); ?>, at <?php echo date('h:m a', strtotime($user_active_packages->created_at)); ?></span>
                     
                      <p class="text-sm text-primary mt-1 font-weight-bolder">
                       @if($user_active_packages->price > 0) $ {{$user_active_packages->price}}.00 AUD @else Free Plan @endif  <span class="mb-1 badge badge-sm bg-success mb-0 mt-2 ms-1"> Active </span> </p>
                      
                     
                      
                    </div>
                  
                  
                </div>
              </li>
              </div>
              </ul>
            </div>
        @endif

        </div>

        @if(isset($useraddons) && count($useraddons)>0)
        <div class="row mt-4">

              <blockquote class="text-sm"> <i class="fa fa-check-circle text-success" aria-hidden="true"></i>  &nbsp; Extra Add-ons </blockquote>
            
              @foreach($useraddons as $akey=>$useraddonsdata)
                  
                <div class="col-3 card card shadow-lg mx-2 mt-2">

         
                  <ul class="list-group">
                     <div class="card-body">
                    <li class="list-group-item border-0 justify-content-between p-0 m-0 border-radius-lg">
                      <div class="row">
                            <div class="col-auto">
                          
                          
                            <h6 class="mb-1 text-dark text-sm" > {{$akey+1}} . {{$useraddonsdata->name}}</h6>
                            <span class="text-xs"><?php echo date('d F Y', strtotime($useraddonsdata->created_at)); ?>, at <?php echo date('h:m a', strtotime($useraddonsdata->created_at)); ?></span>
                           
                             <p class="text-sm text-primary mt-1 font-weight-bolder">
                             @if($useraddonsdata->price > 0) $ {{$useraddonsdata->price}}.00 AUD @else @endif </p> 
                            
                           
                            
                          </div>
                        
                        
                      </div>
                    </li>
                    </div>
                    </ul>
              </div>
              @endforeach
            
        
        </div>
        @endif

        @if(isset($user_inactive_packages) && count($user_inactive_packages)>0)
        <div class="row mt-4">

            <blockquote class="text-sm"> <i class="fa fa-times-circle text-danger" aria-hidden="true"></i> &nbsp; Previous Packages History </blockquote>
            @foreach($user_inactive_packages as $ikey=>$packagesdata)
            <div class="col-3 card card shadow-lg mx-2 mt-2">

         
            <ul class="list-group">
               <div class="card-body">
              <li class="list-group-item border-0 justify-content-between p-0 m-0 border-radius-lg">
                <div class="row">
                      <div class="col-auto">
                    
                    
                      <h6 class="mb-1 text-dark text-sm" > {{$ikey+1}} . {{$packagesdata->name}}</h6>
                      <span class="text-xs"><?php echo date('d F Y', strtotime($packagesdata->created_at)); ?>, at <?php echo date('h:m a', strtotime($packagesdata->created_at)); ?></span>
                     
                      <p class="text-sm text-primary mt-1 font-weight-bolder">
                       @if($packagesdata->price > 0) $ {{$packagesdata->price}}.00 AUD @else Free Plan @endif  <span class="mb-1 badge badge-sm bg-danger mb-0 mt-2 ms-1"> InActive </span> </p>
                      
                     
                      
                    </div>
                  
                  
                </div>
              </li>
              </div>
              </ul>
            </div>
              @endforeach
            
        
        </div>
        @endif

        
      </div>
    </div>
</div>  
 <hr class="horizontal dark" />
  
    <div class="card mt-4" >
       <div class="card-header">
        <p class="text-sm mb-0 font-weight-bold">User Usage Statistics</p>
       </div>
       <div class="card-body">
        <div class="row">
         <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1 ">
            <?php $personalisedmessagepercentage=0;?>
            <?php $totalpersonalisedmessage=isset($user_active_packages->personalised_message_value) ? $user_active_packages->personalised_message_value : 0;?>
            <?php if($totalpersonalisedmessage>0): ?>
              <?php $personalisedmessagepercentage=($usedtotalpersonalisedmessage/$totalpersonalisedmessage)*100;?>
            <?php endif;?>
            <div class="text-center p-3"><h6>Personalised Messages</h6></div>
            <div class="circlechart" data-percentage="{{$personalisedmessagepercentage}}"><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="{{$personalisedmessagepercentage}},100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5">{{$usedtotalpersonalisedmessage}}</text><text class="circle-chart__subline" x="16.91549431" y="22">

            </text> </g></svg>

            <p class="text-xs h-20 text-dark">
              @if(($totalpersonalisedmessage-$usedtotalpersonalisedmessage)<=0 && $totalpersonalisedmessage>0)
                <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M10.909 2.782a2.25 2.25 0 0 1 2.975.74l.083.138 7.759 14.009a2.25 2.25 0 0 1-1.814 3.334l-.154.006H4.242A2.25 2.25 0 0 1 2.2 17.812l.072-.143L10.03 3.66a2.25 2.25 0 0 1 .879-.878ZM12 16.002a.999.999 0 1 0 0 1.997.999.999 0 0 0 0-1.997Zm-.002-8.004a1 1 0 0 0-.993.884L11 8.998 11 14l.007.117a1 1 0 0 0 1.987 0l.006-.117L13 8.998l-.007-.117a1 1 0 0 0-.994-.883Z" fill="#f2ac3b"/></svg>@endif
                @if($totalpersonalisedmessage>0)
                {{$totalpersonalisedmessage-$usedtotalpersonalisedmessage}}  personalised messages remaining 
                @else
                Unlimited messages remaining
                @endif 
              </p>

             
            </div>

          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1">
            <?php $minutespercentage=0;
            if($totalminutes>0): ?>
              <?php $minutespercentage=($usedtotalminutes/$totalminutes)*100;?>
            <?php endif;?>
            <div class="text-center p-3"><h6>Audio and Video Minutes</h6></div>
            <div class="circlechart" data-percentage="{{$minutespercentage}}"><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="{{$minutespercentage}},100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5">{{$usedtotalminutes}}</text><text class="circle-chart__subline" x="16.91549431" y="22">

            </text> </g></svg>
            <p class="text-xs h-20 text-dark">
              @if(($totalminutes-$usedtotalminutes)<=0)<svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M10.909 2.782a2.25 2.25 0 0 1 2.975.74l.083.138 7.759 14.009a2.25 2.25 0 0 1-1.814 3.334l-.154.006H4.242A2.25 2.25 0 0 1 2.2 17.812l.072-.143L10.03 3.66a2.25 2.25 0 0 1 .879-.878ZM12 16.002a.999.999 0 1 0 0 1.997.999.999 0 0 0 0-1.997Zm-.002-8.004a1 1 0 0 0-.993.884L11 8.998 11 14l.007.117a1 1 0 0 0 1.987 0l.006-.117L13 8.998l-.007-.117a1 1 0 0 0-.994-.883Z" fill="#f2ac3b"/></svg> @endif {{$totalminutes-$usedtotalminutes}} minutes remaining </p>

               

              </div>


            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1">
              <?php $pagespercentage=0;
              if($totalpagecount>0): ?>
                <?php $pagespercentage=($usedpagecount/$totalpagecount)*100;?>
              <?php endif;?>
              <div class="text-center p-3"><h6>Written Pages</h6></div>
              <div class="circlechart" data-percentage="{{$pagespercentage}}"><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="{{$pagespercentage}},100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5">{{$usedpagecount}}</text><text class="circle-chart__subline" x="16.91549431" y="22">

              </text> </g></svg>
              <p class="text-xs h-20 text-dark">
                @if(($totalpagecount-$usedpagecount)<=0)<svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M10.909 2.782a2.25 2.25 0 0 1 2.975.74l.083.138 7.759 14.009a2.25 2.25 0 0 1-1.814 3.334l-.154.006H4.242A2.25 2.25 0 0 1 2.2 17.812l.072-.143L10.03 3.66a2.25 2.25 0 0 1 .879-.878ZM12 16.002a.999.999 0 1 0 0 1.997.999.999 0 0 0 0-1.997Zm-.002-8.004a1 1 0 0 0-.993.884L11 8.998 11 14l.007.117a1 1 0 0 0 1.987 0l.006-.117L13 8.998l-.007-.117a1 1 0 0 0-.994-.883Z" fill="#f2ac3b"/></svg> @endif{{$totalpagecount-$usedpagecount}} pages remaining </p>

                 
                </div>

              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
              <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1">
                <?php $recipientpercentage=0;
                if($totalrecipients>0):?>
                  <?php $recipientpercentage=($usedrecipients/$totalrecipients)*100;?>
                <?php endif;?>
                <div class="text-center p-3"><h6>Recipient Spaces</h6></div>
                <div class="circlechart" data-percentage="{{$recipientpercentage}}"><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="{{$recipientpercentage}},100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5">{{$usedrecipients}}</text><text class="circle-chart__subline" x="16.91549431" y="22">

                </text> </g></svg>

                <p class="text-xs h-20 text-dark">
                  @if(($hasrecipients)<=0)<svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M10.909 2.782a2.25 2.25 0 0 1 2.975.74l.083.138 7.759 14.009a2.25 2.25 0 0 1-1.814 3.334l-.154.006H4.242A2.25 2.25 0 0 1 2.2 17.812l.072-.143L10.03 3.66a2.25 2.25 0 0 1 .879-.878ZM12 16.002a.999.999 0 1 0 0 1.997.999.999 0 0 0 0-1.997Zm-.002-8.004a1 1 0 0 0-.993.884L11 8.998 11 14l.007.117a1 1 0 0 0 1.987 0l.006-.117L13 8.998l-.007-.117a1 1 0 0 0-.994-.883Z" fill="#f2ac3b"/></svg> @endif {{$hasrecipients}} recipient spaces remaining </p>


                  
                  </div>

                </div>
              </div>
            </div>
            </div>
    </div>                                           
<hr class="horizontal dark" />
<div>
  <div class="card mt-4" id="billing-info">
    <div class="card-header">
      <p class="text-sm mb-0 font-weight-bold">Billing History</p>
    </div>
    <div class="card-body pt-0 ps-5">
        @if(isset($receipts) && count($receipts)>0)
        <div class="row">

        
              @foreach($receipts as $receiptdata)
            <div class="col-5 card card shadow-lg mx-2 mt-2">

         
            <ul class="list-group">
               <div class="card-body">
              <li class="list-group-item border-0 justify-content-between p-0 m-0 border-radius-lg">
                <div class="row">
                  <div class="col-auto">
                    
                    
                      <h6 class="mb-1 text-dark text-sm" ><i class="fa fa-check-circle" aria-hidden="true"></i> Receipt #{{$receiptdata->number}}</h6>
                      <span class="text-xs"><?php echo date('d F Y', strtotime($receiptdata->created_at)); ?>, at <?php echo date('h:m a', strtotime($receiptdata->created_at)); ?></span>
                      @if($receiptdata->status=='succeeded')
                      <span class="text-sm text-primary mt-1 ">
                        $ {{$receiptdata->amount}}.00 AUD <span class="mb-1 badge badge-sm bg-success mb-0 mt-2 ms-1">{{$receiptdata->status}} </span> </span>
                      @endif
                      @if($receiptdata->status!=='succeeded')
                      <span class="text-sm text-primary mt-1 ">
                        $ {{$receiptdata->amount_remaining}}.00 AUD <span class="mb-1 badge badge-sm bg-danger mb-0 mt-2 ms-1">Due amount</span> </span>
                      @endif
                    </div>
                  
                  <div class="col-auto">
                    <div class="row">
                      @if($receiptdata->status=='succeeded')
                      <div class="col-12"> <a href="{{$receiptdata->receipt_url}}"> <span class="badge badge-lg bg-primary mb-0 mt-2">View</span> </a> </div>
                      @endif
                      @if($receiptdata->status!=='succeeded')
                      <div class="col-12"> <a href="{{$receiptdata->receipt_url}}"> <span class="badge badge-lg bg-primary mb-0 mt-2">Pay Now</span> </a> </div>
                      @endif
                      
                    </div>
                  </div>
                </div>
              </li>
              </div>
              </ul>
          </div>
              @endforeach
            
        
        </div>
        @else
        <blockquote class="text-sm"> - Sorry no Receipts Found !</blockquote>
        @endif
      </div>
    </div>
</div>


<script src="{{asset('assets/js/js-address-lookup.js')}}?v=1.0.3"></script>


@endsection