@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','Add New Promotional Code')

@section('headercommon')
<x-main.header icon="people" title="Add New Promotional Code" subtitle="Promotional Codes can be used to discount invoices or entire customer accounts." />
@endsection

@section('content')

<style type="text/css">
  .swal-footer {
    text-align: center;
  }
</style>

<div class="row">
  <div class="card">
    <div class="card-body">
      <h5 class="font-weight-bolder text-sm text-primary"> <a data-route="{{route('manage-promotional-codes.index',$coupons->coupon_id)}}" class="link-handle"><i class="fi fi_chevron_left text-primary" aria-hidden="true"></i></a> &nbsp; Add New Promotional Code </h5>

      <form method="post" action="{{route('manage-promotional-codes.store',$coupons->coupon_id)}}">
        @csrf
        <div class="row mt-5" >
          <div class="col-12 col-sm-6 ">
            <label>Promotional Codes Name <span class="text-danger">*</span></label>
            <input class="form-control @if(isset($error['name'][0]) && !empty($error['name'][0])) is-invalid @endif" type="text" name="name" value="{{$old->name}}"  />
            @if(isset($error['name'][0]) && !empty($error['name'][0]))
            <p class="form-text text-danger text-xs mb-1">
              {{$error['name'][0]}}
            </p>
            @endif
          </div>
         
		</div>
 

          
 <div class="col-12 mt-4 ">
  <div class="button-row float-end d-flex mt-4">
    <button class="submit-button btn bg-primary btn-sm mb-0 me-2" type="button" name="button">Save</button>
    <a data-route="{{route('manage-promotional-codes.index',$coupons->coupon_id)}}" class="link-handle btn btn-outline-dark btn-sm mb-0" type="button" name="button">Back</a>
  </div>
</div>
</form>
</div>
</div>

</div>


<script type="text/javascript">
	$('.coupontype').change(function() {
        var coupontype = $('input[name="type"]:checked').val();
        if (coupontype == 1) {

        	$('#percenttype').show();
        	$('#discounttype').hide();
        }
        if (coupontype == 2) {

        	$('#discounttype').show();
        	$('#percenttype').hide();
        }
    });
    $('.coupontype').trigger('change');
</script>

@stop
