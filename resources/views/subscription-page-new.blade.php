
@extends(isset($isajax) && $isajax==true ? 'layouts-frontend.ajax' : 'layouts-frontend.master')
@section('headercommon')
<header>
		<div class="top-1">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="logo">
							<a href="index.php"><img src="images/logo.png" alt="" title=""/></a>
						</div>
					</div>
					<div class="col-md-9">
						<div class="menu-con">
							<nav class="navbar navbar-expand-lg navbar-dark">
							  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
							  <div class="collapse navbar-collapse" id="navbarSupportedContent">
								<ul class="navbar-nav mr-auto">
								  <li class="nav-item"><a href="#">Beneficiary </a></li>
								  <li class="nav-item"><a href="#">Beneficiaries </a></li>
								  <li class="nav-item"><a href="#">Our story </a></li>
								  <li class="nav-item"><a href="#">Pricing </a></li>
								  <li class="nav-item"><a href="#">Contact </a></li>
								  <li class="nav-item"><a href="#">Login </a></li>
								</ul>
							  </div>
							</nav>
							<a href="#" class="btn-1">Get Started</a>
						  </div>
					</div>
				</div>
			</div>
		</div>
	</header>
@endsection()
@section('content')


<div class="pricing-page-heading">
  <figure> <img src="{{asset('assets/img/inner-page-banner.jpg')}}" alt="" title=""/> </figure>
  <div class="pricing-heading-content">
    <div class="container">
      <div class="row">
        <div class="col-md-8 offset-md-2 text-center text-white">
          <h1>Limited time only introductory offers!</h1>
          <p>Memorialisation happens in so many different ways, we’re here to support that journey for you and your loved ones.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="pricing-page-wraper">
  <div class="pricing-page-menu">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
		<nav>
			<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
			  <a class="nav-item nav-link active" id="nav-monthly-ConnectionContent-tab" data-toggle="tab" href="#nav-ConnectionContent" role="tab" aria-controls="nav-ConnectionContent" aria-selected="true">Connection Content Plans </a>
			  <a class="nav-item nav-link" id="nav-MemorialQR-tab" data-toggle="tab" href="#nav-MemorialQR" role="tab" aria-controls="nav-MemorialQR" aria-selected="false">Memorial QR Codes</a>
			  <a class="nav-item nav-link" id="nav-HeirloomKeepsake-tab" data-toggle="tab" href="#nav-HeirloomKeepsake" role="tab" aria-controls="nav-HeirloomKeepsake" aria-selected="false">Heirloom Keepsake</a>
			</div>
		</nav>
        </div>
      </div>
    </div>
  </div>
  <section class="pricing-box-con">
    <div class="container">
		<div class="row">
				<div class="col-md-12">
                  <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-ConnectionContent" role="tabpanel" aria-labelledby="nav-ConnectionContent-tab">
						<div class="pricing-box-wrapper">
							<div class="heading-group">
							  <h2>Connection Content Plans</h2>
							  <h5>Empowering you to leave less unsaid</h5>
							  <h6><span>LIMITED TIME OFFERS</span></h6>
							</div>
							<div class="row">
							  <div class="col-md-4">
								<div class="pricing-box">
								  <div class="icon"><i class="fa fa-heart" aria-hidden="true"></i></div>
								  <h4>Community Package</h4>
								  <p>Join the Evaheld Community</p>
								  <h5>$0</h5>
								  <h6><span>FREE</span></h6>
								  <div class="content">
									<p>Perfect for creating messages for that special Someone.</p>
								  </div>
								  <div class="list">
									<p><strong>Inclusions</strong></p>
									<ul>
									  <li>1 personalised message</li>
									  <li>Video, audio, or written</li>
									  <li>Secure storage<sup>1</sup></li>
									  <li>Access Evaheld Community benefits
										<ul>
										  <li>Discounted Package Upgrade</li>
										</ul>
									  </li>
									</ul>
									<p>Coming soon</p>
									<ul class="sub">
									  <li>Memorial page</li>
									</ul>
								  </div>
								  <div class="button-con">
									<button>Get started now</button>
								  </div>
								</div>
								<div class="pricing-expand-con">
									<p>Community Package Terms</p>
									<div class="pricing-expand-content">
										<div><span id="dots"></span>
											<div class="expand-content" id="more">
												<h6>Community Package Terms</h6>
												<p><sup>1</sup> Until the date of delivery of the first message, which must be within 1 year.</p>
												<p><sup>2</sup> Must indicate future date</p>
											</div>
										</div>
										<button onclick="myFunction()" id="myBtn">Click to expand</button>
									</div>
								</div>
							  </div>
							  <div class="col-md-4">
								<div class="pricing-box">
								  <div class="icon"><i class="fa fa-heart" aria-hidden="true"></i></div>
								  <h4>Connection Package</h4>
								  <p>Recommended</p>
								  <h5>$99</h5>
								  <h6><span>SAVE! $99 Now, will be $299</span></h6>
								  <div class="content">
									<p>Suited for creating messages for a family or close circle of friends.</p>
								  </div>
								  <div class="list">
									<p><strong>Inclusions</strong></p>
									<ul>
									  <li>10 personalised messages</li>
									  <li>Video, audio or witten</li>
									  <li>Lifetime secure storage<sup>2</sup></li>
									  <li>Access to our Wellbeing System<sup>™</sup></li>
									  <li>Private in-browser content creation</li>
									  <li>Evaheld digital delivery to 3 recipients</li>
									  <li>Access Evaheld Community benefits
										<ul>
										  <li>Discounted QR code<sup>3</sup></li>
										  <li>Discounted Heirloom product</li>
										  <li>Discounted Extra minutes</li>
										</ul>
									  </li>
									  <p>Coming soon</p>
									  <ul class="sub">
										<li>Memorial page</li>
									  </ul>
									</ul>
								  </div>
								  <div class="button-con">
									<button>Get started now</button>
									<button>Start a 14-day free trial</button>
								  </div>
								</div>
								<div class="pricing-expand-con">
									<p>Community Package Terms</p>
									<div class="pricing-expand-content">
										<div><span id="dots_2"></span>
											<div class="expand-content" id="more_2">
												<h6>Community Package Terms</h6>
												<p><sup>1</sup> Until the date of delivery of the first message, which must be within 1 year.</p>
												<p><sup>2</sup> Must indicate future date</p>
											</div>
										</div>
										<button onclick="myFunction()" id="myBtn_2">Click to expand</button>
									</div>
								</div>
							  </div>
							  <div class="col-md-4">
								<div class="pricing-box active">
								  <div class="icon"><i class="fa fa-heart" aria-hidden="true"></i></div>
								  <h4>Legacy Package</h4>
								  <p>Best value</p>
								  <h5>$299</h5>
								  <h6><span>SAVE! $299 Now, will be $499</span></h6>
								  <div class="content">
									<p>Everything you need to create messages for your family and friendship circles.</p>
								  </div>
								  <div class="list">
									<p><strong>Inclusions</strong></p>
									<ul>
									  <li>Unlimited personalised messages</li>
									  <li>Video, audio or witten</li>
									  <li>Lifetime secure storage<sup>2</sup></li>
									  <li>Access to our Wellbeing System<sup>™</sup></li>
									  <li>Private in-browser content creation</li>
									  <li>Evaheld digital delivery to 10 recipients</li>
									  <li>Access Evaheld Community benefits
										<ul>
										  <li>Discounted QR code<span>3</span></li>
										  <li>Discounted Heirloom product<span>4</span></li>
										  <li>Discounted Extra minutes</li>
										</ul>
									  </li>
									  <li>Access to all new features</li>
									</ul>
									<p>Coming soon</p>
									<ul class="sub">
									  <li>Memorial page</li>
									  <li>Life Story Books</li>
									  <li>Family history capsule</li>
									  <li>Digital photo albums</li>
									  <li>Life timelines</li>
									  <li>Funeral content templates</li>
									  <li>More to come</li>
									</ul>
								  </div>
								  <div class="button-con">
									<button>Get started now</button>
									<button>Start a 14-day free trial</button>
								  </div>
								</div>
								<div class="pricing-expand-con">
									<p>Legacy Package Terms</p>
									<div class="pricing-expand-content">
										<div><span id="dots_3"></span>
											<div class="expand-content" id="more_3">
												<h6>Community Package Terms</h6>
												<p><sup>1</sup> Until the date of delivery of the first message, which must be within 1 year.</p>
												<p><sup>2</sup> Must indicate future date</p>
											</div>
										</div>
										<button onclick="myFunction()" id="myBtn_3">Click to expand</button>
									</div>
								</div>
							  </div>
							
							</div>
						  </div>
                    </div>
                    <div class="tab-pane fade" id="nav-MemorialQR" role="tabpanel" aria-labelledby="nav-MemorialQR-tab">
						<div class="pricing-box-wrapper">
							<div class="heading-group">
							  <h2>Memorial QR Codes</h2>
							  <h5>Be remembered how you want to be remembered</h5>
							  <h6><span>LIMITED TIME OFFERS</span></h6>
							</div>
							<div class="row">
							  <div class="col-md-6">
								<div class="pricing-box">
								  <div class="icon"><i class="fa fa-gift" aria-hidden="true"></i></div>
								  <h4>Connections QR Code</h4>
								  <p>Most popular</p>
								  <h5>$199</h5>
								  <h6><span>SAVE! Now $199, will be $399</span></h6>
								  <div class="content">
									<p>Linked to a Connections Package, the Connections QR code…</p>
								  </div>
								  <div class="list">
									<p><strong>Inclusions</strong></p>
									<ul>
									  <li>Includes all the features of the Connections Package</li>
									  <li>Users and families can create and link biography, eulogies and more</li>
									  <li>Print of memorialisation keepsakes, such as funeral service booklets</li>
									  <li>Indefinite storage through our Community Memorial portal</li>
									  <li>Suitable for cemetery and crematorium remembrance headstones and plaques</li>
									</ul>
									<p>Coming soon</p>
									<ul class="sub">
									  <li>Customised online Memorial page account with third party administrator access available</li>
									</ul>
								  </div>
								  <div class="button-con">
									<button>Add item</button>
								  </div>
								</div>
							  </div>
							  <div class="col-md-6">
								<div class="pricing-box active">
								  <div class="icon"><i class="fa fa-qrcode" aria-hidden="true"></i></div>
								  <h4>Legacy QR Code</h4>
								  <p>Best value</p>
								  <h5>$399</h5>
								  <h6><span>SAVE! Now $399, will be $599</span></h6>
								  <div class="content">
									<p>Linked to a Legacy Package, the Legacy QR code…</p>
								  </div>
								  <div class="list">
									<p><strong>Inclusions</strong></p>
									<ul>
									  <li>Includes all the features of the Legacy Package</li>
									  <li>Users and families can create and link biography, eulogies and more</li>
									  <li>Print of memorialisation keepsakes, such as funeral service booklets</li>
									  <li>Indefinite storage through our Community Memorial portal</li>
									  <li>Suitable for cemetery and crematorium remembrance headstones and plaques</li>
									</ul>
									<p>Coming soon</p>
									<ul class="sub">
									  <li>Customised online Memorial page account with third party administrator access available</li>
									</ul>
								  </div>
								  <div class="button-con">
									<button>Add item</button>
								  </div>
								</div>
							  </div>
							</div>
						  </div>
                    </div>
					<div class="tab-pane fade" id="nav-HeirloomKeepsake" role="tabpanel" aria-labelledby="nav-HeirloomKeepsake-tab">
						<div class="pricing-box-wrapper">
							<div class="row">
							  <div class="col-md-6">
								<div class="pricing-box text-left">
								  <div class="icon"><i class="fa fa-gift" aria-hidden="true"></i></div>
								  <h4>Heirloom Keepsake<span>LIMITED TIME OFFER</span></h4>
								  <p>The gift of remembrance</p>
								  <h5>$349</h5>
								  <h6><span>SAVE! Now $349, will be $449</span></h6>
								  <div class="content">
									<p>Give your loved ones a beautiful heirloom keepsake that will keep you connected to your family for generations to come.</p>
								  </div>
								  <div class="list">
									<p><strong>Inclusions</strong></p>
									<ul>
									  <li>Customised heirloom box for 1 recipient only</li>
									  <li>Includes all the content features of the Connections Package</li>
									  <li>USB Storage</li>
									  <li>Includes digital delivery</li>
									  <li>Courier Delivered</li>
									</ul>
								  </div>
								  <div class="button-con">
									<button>Add item</button>
								  </div>
								</div>
								<div class="pricing-expand-con">
									<p>Legacy Package Terms</p>
									<!--<div class="pricing-expand-content">
										<div><span id="dots_4"></span>
											<div class="expand-content" id="more_4">
												<h6>Heirloom Keepsake Terms</h6>
												<p><sup>1</sup> Until the date of delivery of the first message, which must be within 1 year.</p>
												<p><sup>2</sup> Must indicate future date</p>
											</div>
										</div>
										<button onclick="myFunction()" id="myBtn_4">Click to expand</button>
									</div>-->
								</div>
							  </div>
							  <div class="col-md-6">
								<div class="heirloom-keepsake">
									<img src="{{asset('assets/img/img-4.jpg')}}" alt="" title=""/>
								</div>
							  </div>
							</div>
						  </div>
					</div>
					
					
					
                  </div>
				</div>
			</div>
    </div>
	<div class="buttom-bg">
		<figure>
			<img src="{{asset('assets/img/faq-top-bg.png')}}" alt="" title=""/>
		</figure>
	</div>
  </section>
  <section class="faq-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				<h3 class="heading-1 text-center">Faq<h3>
				 <div class="pxlr-club-faq">
					<div class="content">
						<div class="panel-group" id="accordion" role="tablist"
							 aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" id="headingOne" role="tab">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse"
										   data-parent="#accordion" href="#collapseOne"
										   aria-expanded="false" aria-controls="collapseOne">How do I diam nonumy eirmod tempor? <i
													class="pull-right fa fa-plus"></i></a>
									</h4>
								</div>
								<div class="panel-collapse collapse" id="collapseOne" role="tabpanel"
									 aria-labelledby="headingOne">
									<div class="panel-body pxlr-faq-body">
										<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
										invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et
										justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem
										ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
										eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
										eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
										sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" id="headingTwo" role="tab">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse"
										   data-parent="#accordion" href="#collapseTwo"
										   aria-expanded="false" aria-controls="collapseTwo">What can takimata sanctus est?<i class="pull-right fa fa-plus"></i></a>
									</h4>
								</div>
								<div class="panel-collapse collapse" id="collapseTwo" role="tabpanel"
									 aria-labelledby="headingTwo">
									<div class="panel-body pxlr-faq-body">
										<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
										invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et
										justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem
										ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
										eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
										eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
										sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" id="headingThree" role="tab">
									<h4 class="panel-title"><a class="collapsed" role="button"
															   data-toggle="collapse"
															   data-parent="#accordion"
															   href="#collapseThree"
															   aria-expanded="false"
															   aria-controls="collapseThree">Problem
											How do I dolor sit amet consetetur sadipscing? <i class="pull-right fa fa-plus"></i></a></h4>
								</div>
								<div class="panel-collapse collapse" id="collapseThree" role="tabpanel"
									 aria-labelledby="headingThree">
									<div class="panel-body pxlr-faq-body">
										<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
										invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et
										justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem
										ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
										eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
										eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
										sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
										</p>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" id="headingFour" role="tab">
									<h4 class="panel-title"><a class="collapsed" role="button"
															   data-toggle="collapse"
															   data-parent="#accordion"
															   href="#collapseFour"
															   aria-expanded="false"
															   aria-controls="collapseFour">When does the recipient clita kasd gubergren no sea?<i class="pull-right fa fa-plus"></i></a></h4>
								</div>
								<div class="panel-collapse collapse" id="collapseFour" role="tabpanel"
									 aria-labelledby="headingFour">
									<div class="panel-body pxlr-faq-body">
										<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
										invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et
										justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem
										ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
										eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
										eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
										sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr,
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  </section>
  <section class="pricing-box-con-2">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
			<div class="left">
				<h2 class="heading-1">Even if I miss significant life events, I know my my messages and presence will be felt and everlasting in those moments.</h2>
				<div class="btn-group"> <a href="#" class="btn btn-2">Our story</a> <a href="#" class="btn btn-2">How we help</a> </div>
			</div>
        </div>
		<div class="col-md-6 text-center">
			<figure>
				<img src="{{asset('assets/img/EvenifImiss.jpg')}}" alt="" title=""/>
			</figure>
		</div>
      </div>
    </div>
  </section>

</div>
<footer>
		<div class="footer-1">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="f-box">
							<div class="f-logo">
								<img src="{{asset('assets/img/f-logo.png')}}" alt="" title=""/>
							</div>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							<div class="fsocialmedia">
								<a href="#" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
								<a href="#" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
								<a href="#" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
								<a href="#" title="Youtube"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="row">
							<div class="col-md-4">
								<div class="f-box">
									<h4>Product </h4>
									<ul>
										<li><a href="#">How it works</a></li>
										<li><a href="#">Features</a></li>
										<li><a href="#">Pricing</a></li>
										<li><a href="#">Security</a></li>
										<li><a href="#">Product tour</a></li>
									</ul>
								</div>	
							</div><div class="col-md-4">
								<div class="f-box">
									<h4>Resources </h4>
									<ul>
										<li><a href="#">How it works</a></li>
										<li><a href="#">Features</a></li>
										<li><a href="#">Pricing</a></li>
										<li><a href="#">Security</a></li>
										<li><a href="#">Product tour</a></li>
									</ul>
								</div>	
							</div>
							<div class="col-md-4">
								<div class="f-box">
									<h4>Company</h4>
									<ul>
										<li><a href="#">How it works</a></li>
										<li><a href="#">Features</a></li>
										<li><a href="#">Pricing</a></li>
										<li><a href="#">Security</a></li>
										<li><a href="#">Product tour</a></li>
									</ul>
								</div>	
							</div>
							
						</div>
					</div>
					<div class="col-md-4">
						<div class="f-box">
							<h4>Suscribe to receive our newsletter</h4>
							<p>The latest news, articles, and resources, sent to your inbox weekly</p>
							<div class="news-letter">
								<p>Enter your email address</p>
								<form>
									<input type="mail" placeholder="example@gmail.com"/>
									<input type="submit" value="submit"/>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="footer-2">
						<div class="row">
							<div class="col-md-6">
								<div class="left">
									<ul>
										<li>Copyright 2022 Evaheld</li>
										<li>ABN 37123456789</li>
									</ul>
								</div>
							</div>
							<div class="col-md-6">
								<div class="right">
									<ul>
										<li><a href="#">Diversity & Inclusion</a></li>
										<li><a href="#">Term of services</a></li>
										<li><a href="#">Privacy Policy</a></li>
										<li><a href="#">Sitemap</a></li>
									</ul>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</footer>
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-3.2.1.slim.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/aos.js"></script>
<!--Darte Picker-->
<script src="js/datepicker.min.js" type="text/javascript"></script>
<!--Darte Picker End-->
<!--AOS Animation-->
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<!--AOS Animation End-->
<!--Custom Js-->
<script src="js/custom-js.js"></script>
<style>
    /*Website Name:Evaheld, File: Layout CSS, Author:MKM, Creation Date : 04/05/2022*/
/* @import url('bootstrap.min.css');
@import url('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css');
@import url('https://unpkg.com/aos@2.3.1/dist/aos.css');
@import url('owl.carousel.min.css');
@import url('owl.theme.min.css');
@import url('datepicker.min.css');
@import url('https://unpkg.com/aos@2.3.1/dist/aos.css'); */
/*Fonts*/
/* @import url('https://fonts.googleapis.com/css2?family=Libre+Bodoni:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap'); */
/*font-family: 'Libre Bodoni', serif;*/
/* @import url('https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap'); */
/*font-family: 'Poppins', sans-serif;*/
/*Fonts*/
html{scroll-behavior:smooth;}
body{ padding: 0; margin:0; font-family: 'Poppins', sans-serif; font-size:14px; font-weight: 500;}
*{ list-style: none; outline:none; stroke:none; }
ul{ padding: 0; margin:0; }
.img-full-width{ width: 100%; }
h1, h2, h3, h4, h5, h6{}
h3{ font-size:22px; }
.no-padding{padding:0!important;}
img{max-width:100%;}
section{ width:100%; float:left; }
.btn:hover {
    opacity: 0.8;
}
p {
    font-weight: normal;
}
/*======
Header
============*/
header {
    width: 100%;
    float: left;
}
.top-1 {
    width: 100%;
    float: left;
    padding: 10px 0;
    box-shadow: 0 0 2px #00000069;
}
.logo a img {
    max-width: 160px;
}
/*Nav Menu*/
.menu-con {
    display: inline-block;
    width: auto;
    float: right;
}
.menu-con .navbar-dark .navbar-nav .nav-link {
    padding: 10px 15px;
}
.menu-con nav.navbar.navbar-expand-lg.navbar-dark {
    display: inline-block;
}
.menu-con .dropdown-toggle::after{ display:none; }


.menu-con .navbar-dark .navbar-nav .nav-link {
    color: rgb(0 0 0);
    text-transform: uppercase;
    font-size: 15px;
    font-weight: 400;
}
.menu-con .navbar-toggler {
    background: #f16b23;
}
.menu-con .navbar-dark .navbar-nav .nav-link:hover {
    color: #09ad8b;
}
.menu-con .dropdown-menu ul li a {
    text-transform: none!important;
}
 /*Hide drop down arrow*/
 .menu-con .dropdown-toggle::after{ display:none;}
 /**/
.menu-con .navbar .dropdown-menu div[class*="col"] {
    margin-bottom: 0;
}

 /* breakpoint and up - mega dropdown styles */
 @media screen and (min-width: 992px) {
   
   /* remove the padding from the navbar so the dropdown hover state is not broken */
.menu-con .navbar {
   padding-top:0px;
   padding-bottom:0px;
 }
 
 /* remove the padding from the nav-item and add some margin to give some breathing room on hovers */
.menu-con .navbar .nav-item {
   padding:.5rem .5rem;
   margin:0 .25rem;
 }
 
 /* makes the dropdown full width  */
.menu-con .navbar .dropdown {position:static;}
 
.menu-con .navbar .dropdown-menu {
    visibility: hidden;
    opacity: 0;
    position: absolute;
    top: 98%;
    left: 0;
    width: 100%;
    transform: translateY(-2em);
    z-index: -1;
    transition: all 0.3s ease-in-out 0s, visibility 0s linear 0.3s, z-index 0s linear 0.01s;
    display: inline-block;
    border-radius: 5px;
    box-shadow: 0 3px 6px #00000026;
}
   /* shows the dropdown menu on hover */
.menu-con .navbar .dropdown:hover .dropdown-menu, .navbar .dropdown .dropdown-menu:hover {
    visibility: visible;
    opacity: 1;
    z-index: 1;
    transform: translateY(0%);
    transition-delay: 0s, 0s, 0.3s;
}
.menu-con .navbar .dropdown-menu {
    border: none;
    background-color: #fffffff2;
}
 }
 /*M I L A N*/
 .dropdown-menu ul li {
    padding: 0!important;
    margin: 0!important;
}
.dropdown-menu ul li a {
    font-size: 13px!important;
    display: inline-block;
    padding: 4px!important;
}
.menu-con .navbar .nav-item a {
    padding: 6px 0;
    display: inline-block;
    width: 100%;
    color: #545656;
    text-decoration: none;
    position: relative;
}
.menu-con .navbar .nav-item a:hover{color:#8c61ac;}
.menu-con .navbar .nav-item a:before {
    content: '';
    width: 0;
    height: 2px;
    position: absolute;
    background: #d0bedd;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
    transition: all ease-in-out 1.0s;
}
.menu-con .navbar .nav-item:hover a:before {width: 100%;}
span.menu-heading {
    color: #568800;
    margin-bottom: 8px;
    padding-bottom: 6px;
    display: inline-block;
    position: relative;
    font-weight: 500;
}
span.menu-heading:before {
    content: '';
    width: 70px;
    background: #86553e;
    height: 2px;
    position: absolute;
    bottom: 0;
}
.menu-con .menu-con .dropdown-menu li.nav-item {
    padding: 5px 0 0;
}
.menu-con .menu-con .dropdown-menu li.nav-item a{padding: 0; }
/* adds some margin below the link sets  */
.navbar .dropdown-menu div[class*="col"] {
   margin-bottom:1rem;
}

.navbar .dropdown-menu {
    border: 1px solid #ebebeb!important;
    background-color: #ffffff!important;
}
.menu-con .dropdown-menu .nav-link {
    text-transform: none!important;
    padding: 6px 0!important;
    line-height: 15px;
}
.dropdown-menu span.text-uppercase {
    color: #09ad8b;
    display: inline-block;
    margin-bottom: 11px;
}
/* breakpoint and up - mega dropdown styles */
@media screen and (min-width: 992px) {
  
  /* remove the padding from the navbar so the dropdown hover state is not broken */
.navbar {
  padding-top:0px;
  padding-bottom:0px;
}

/* remove the padding from the nav-item and add some margin to give some breathing room on hovers */
.dropdown-menu .nav-item {
    font-size: 14px;
    text-transform: none;
}

/* makes the dropdown full width  */
.navbar .dropdown {position:static;}

.navbar .dropdown-menu {
  width:100%;
  left:0;
  right:0;
/*  height of nav-item  */
  top:45px;
}
  
  /* shows the dropdown menu on hover */
.navbar .dropdown:hover .dropdown-menu, .navbar .dropdown .dropdown-menu:hover {
  display:block!important;
}
  
  .navbar .dropdown-menu {
    border: 1px solid rgba(0,0,0,.15);
    background-color: #fff;
  }

}

.btn-1 {
    background: #f3eff6;
    color: #662d91;
    padding: 10px 17px;
    border: 1px solid #f3eff6;
    border-radius: 30px;
}
.btn-1:hover{ background:none; color:#662d91; text-decoration: none; }
/*Nav Menu End*/
/*=========
Header End
============*/
/*Pricing Page*/
.pricing-page-heading {
    width: 100%;
    float: left;
    padding: 0;
    color: #ffffff;
    position: relative;
}
.pricing-heading-content {
    width: 100%;
    padding: 100px 0;
}
.pricing-heading-content h1 {
    font-size: 28px;
}
.pricing-page-heading figure {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
}
.pricing-page-heading figure img {
    object-fit: cover;
    height: 100%;
	min-width:100%;
}
/*Pricing Page End*/
.pricing-page-wraper {
    width: 100%;
    background: #f3eef6;
    padding: 0 0 60px;
    float: left;
}
.pricing-page-menu {
    width: 100%;
    float: left;
    text-align: center;
    background: #fff;
    box-shadow: 0 3px 8px #0000003b;
    margin: 0;
    padding: 19px 0;
}
 .pricing-page-menu nav { display:inline-block; }
.pricing-page-menu ul li a, .pricing-page-menu nav .nav-fill .nav-item {
    display: inline-block;
    position: relative;
    padding: 0 0 1px;
    margin: 0 15px;
    color: #662d91;
    transition: all ease-in-out 1.0s;
    border: none;
}
.pricing-page-menu .nav-tabs {
    border-bottom: none;
}
.pricing-page-menu ul li a:before, .pricing-page-menu nav .nav-fill .nav-item:before  {
    content: '';
    width: 0;
    height: 2px;
    position: absolute;
    background: #662d91;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
    transition: all ease-in-out 1.0s;
}
.pricing-page-menu ul li a:hover, .pricing-page-menu nav .nav-fill .nav-item:before {color:#662d91; text-decoration:none;}
.pricing-page-menu ul li:hover a:before, .pricing-page-menu ul li.active a:before, 
.pricing-page-menu nav .nav-fill .nav-item:hover:before, .pricing-page-menu nav .nav-fill .nav-item.active:before{width: 100%;}
.pricing-page-menu ul {
    display: inline-block;
}
.pricing-page-menu ul li{display:inline-block;}
section.pricing-box-con {
    text-align: center;
}
section.pricing-box-con {
    text-align: center;
    width: 100%;
    float: left;
    padding: 30px 0 0;
    position: relative;
    background: url(../assets/img/BackgroundLineWhite.svg) no-repeat;
    background-size: 60%;
}
.pricing-box-wrapper {
    width: 100%;
    float: left;
    background: #fff;
    padding: 30px;
    border-radius: 10px;
    box-shadow: 0 0 8px #0000003b;
    margin-bottom: 30px;
}
.heirloom-keepsake {
    margin-top: 50px;
}
div#nav-ConnectionContent .pricing-box-wrapper {
    background-image: url(../assets/img/bg1.jpg);
    background-repeat: no-repeat;
    background-position: bottom left;
    background-size: 519px;
    position: relative;
}
.pricing-box-wrapper:before {
    content: '';
    position: absolute;
    top: 5%;
    right: 20%;
    background:url(../assets/img/element-1.png) no-repeat;
    width: 95px;
    height: 77px;
}
#nav-HeirloomKeepsake .pricing-box-wrapper:before{display:none;}
.heading-group {
    width: 100%;
    float: left;
}	
.heading-group h5 {
    font-size: 18px;
    color: #662d91;
    margin: 20px 0;
}
.heading-group h6 {
    margin: 0 0 30px;
}
.heading-group h6 span {
    display: inline-block;
    padding: 10px 21px;
    background: #f3eef6;
    border-radius: 10px;
    font-size: 14px;
    color: #662d91;
}
.heading-group h2 {
    margin-bottom: 0;
}
section.pricing-box-con nav {
    display: inline-block;
    border: 1px solid #f2f3f4;
    border-radius: 30px;
    overflow: hidden;
    background: #fbfbfc;
}
section.pricing-box-con nav  .nav-tabs .nav-link {
    border-radius: 30px!important;
    color: #979899;
}
section.pricing-box-con nav  .nav-tabs .nav-link  span {
    background: #eef0f2;
    padding: 2px 5px;
    border-radius: 5px;
    font-size: 11px;
}
section.pricing-box-con.nav-tabs .nav-item.show .nav-link, section.pricing-box-con .nav-tabs .nav-link.active, section.pricing-box-con.nav-tabs .nav-item.show .nav-link:hover {
    color: #7d4da1;
    background-color: #fff;
    border-color: #7d4da1;
}
section.pricing-box-con-2 {
    width: 100%;
    float: left;
    padding: 60px 0;
}
section.pricing-box-con-2 .left {
    padding: 60px 0 0;
    position: relative;
}
section.pricing-box-con-2 .left .btn {
    background: #662d91;
    color: #fff;
}
section.pricing-box-con-2 .left .btn.btn-2 {
    background: #fff;
    color: #662d91;
    margin-right: 10px;
    padding: 7px 22px;
    border-radius: 30px;
}
section.pricing-box-con-2 h2 {
    color: #662d91;
    margin-bottom: 30px;
    background: url(../assets/img/b1.jpg) no-repeat;
    background-position: left bottom;
    padding-bottom: 0px;
}

.pricing-box {
    width: 100%;
    border: 2px solid #edeff1;
    padding: 30px;
    border-radius: 10px;
    margin-bottom: 30px;
}
.pricing-box {
    width: 100%;
    border: 2px solid #edeff1;
    padding: 30px;
    border-radius: 10px;
    margin-bottom: 30px;
    text-align: center;
    transition: all ease-in-out 0.5s;
}
.pricing-box:hover, .pricing-box.active {
    border-color: #662d91;
}
.pricing-box .button-con button:hover {
    background: #662d91;
    color: #fff;
}
.pricing-box .button-con button:first-child{background:#662d91; color:#fff;}
.pricing-box .button-con button:first-child:hover{background:#f3eef6; color:#662d91;}
.pricing-box .icon i {
    color: #662d91;
    font-size: 37px;
}
.pricing-box h4, .pricing-box h5 {
    font-family: 'Poppins', sans-serif;
    margin: 20px 0;
    font-size: 20px;
}
.pricing-box h4 span {
    display: inline-block;
    background: #f3eef6;
    padding: 9px 15px;
    border-radius: 10px;
    margin: 0 0 0 13px;
    color: #662d91;
    font-size: 14px;
    float: right;
}
.pricing-box h5 {
    font-size: 33px;
    color: #662d91;
    margin: 0 0;
}
.pricing-box  h6 span {
    display: inline-block;
    background: #f3eef6;
    padding: 9px 22px;
    border-radius: 10px;
    margin: 10px 0;
    color: #662d91;
    font-size: 14px;
}
.pricing-box .list {
    text-align: left;
}
.pricing-box .list ul li {
    position: relative;
    padding-left: 24px;
    margin-bottom: 10px;
}
.pricing-box .list ul li ul {
    margin: 10px 0;
}
.pricing-box p {
    margin-top: 25px;
    margin-bottom: 5px;
}
.pricing-box .list ul li:before {
    font: normal normal normal 14px/1 FontAwesome;
    content: "\f00c";
    width: 15px;
    height: 15px;
    border-radius: 20px;
    background: #662d91;
    color: #fff;
    position: absolute;
    left: 0;
    top: 2px;
    font-size: 11px;
    text-align: center;
    line-height: 15px;
}
.pricing-box .list ul.sub li:before {
    background: #8b9197!important;
}
.pricing-box .button-con {
    margin-top: 30px;
}
.pricing-box .button-con button {
    background: #f3eef6;
    border: 1px solid #f3eef6;
    width: 100%;
    padding: 10px;
    border-radius: 23px;
    color: #662d91;
    margin-top: 13px;
}
.pricing-expand-con {
    text-align: left;
    color: #9aa0a5;
}
#more, #more_2, #more_3, #more_4{display: none;}
.pricing-expand-content {
    position: relative;
    width: 100%;
    padding-top: 21px;
}
.pricing-expand-con p {
    margin-bottom: 8px;
    color: #9aa0a5;
}
.expand-content {
    border-top: 1px solid #edeeef;
    width: 100%;
    float: left;
    padding-top: 10px;
    margin: 10px 0 0;
}
.pricing-expand-content button {
    position: absolute;
    background: none;
    border: none;
    padding: 0;
    margin: 0;
    top: 0;
    left: 0;
    color: #9aa0a5;
}
.pricing-expand-content button:before{ content:''; }
.pricing-expand-content button:focus {
    border: none;
    outline: none;
    stroke: none;
}
.pricing-expand-content button {
    position: absolute;
    background: none;
    border: none;
    padding: 0;
    margin: 0;
    top: 0;
    left: 0;
}

/*FAQ Section*/
.buttom-bg figure {
    padding: 0;
    margin: 0;
}
.buttom-bg figure img{width:100%;}

section.faq-section {
    width: 100%;
    float: left;
    padding: 60px 0 50px;
    background: #fff;
}
.panel-body.pxlr-faq-body p {
    font-size: 14px;
    text-align: justify;
    color: #4e4f50;
    padding-left: 60px;
    line-height: 23px;
    font-weight: normal;
}
.heading-1 {
    font-size: 22px;
    color: #662d91;
    margin-bottom: 30px;
}


/**/

section.faq-section .panel-group {
    margin-bottom: 0;
}
section.faq-section .panel-group .panel {
    border-radius: 0;
    box-shadow: none;
}
section.faq-section .panel-group .panel .panel-heading {
    padding: 0;
}
section.faq-section .panel-group .panel .panel-heading h4 a {
    background: #ffffff;
    display: block;
    font-size: 15px;
    line-height: 20px;
    padding: 15px;
    text-decoration: none;
    transition: 0.15s all ease-in-out;
    border-top: 1px solid #f7f8f8;
    color: #662d91;
    position: relative;
}
section.faq-section .panel-group .panel .panel-heading h4 a:before {
    content: '';
    position: absolute;
    left: 0;
    top: 21px;
    background: #662d91;
    width: 8px;
    height: 8px;
    border-radius: 30px;
}
section.faq-section .panel-group .panel:last-child .panel-heading h4 a{border-bottom: 1px solid #f7f8f8;}
section.faq-section .panel-group .panel .panel-heading h4 a:hover, .panel-group .panel .panel-heading h4 a:not(.collapsed) {
    /*background: #fff;*/
    transition: 0.15s all ease-in-out;
}
section.faq-section .panel-group .panel .panel-heading h4 a:not(.collapsed) i:before {
    content: "-";
    font-size: 30px;
    line-height: 10px;
}
section.faq-section .panel-group .panel .panel-heading h4 a i {
    color: #999;
    font-size: 12px;
    float: right;
}
section.faq-section .panel-group .panel .panel-body {
    padding-top: 0;
}
section.faq-section .panel-group .panel .panel-heading + .panel-collapse > .list-group,
section.faq-section .panel-group .panel .panel-heading + .panel-collapse > .panel-body {
    border-top: none;
}
section.faq-section .panel-group .panel + .panel {
    border-top: none;
    margin-top: 0;
}

/*FAQ Section End*/
/*getstarted section*/

/*Have aq section*/
section.have-aq-section {
    width: 100%;
    float: left;
    padding: 160px 0;
    background: #f3eef6;
    text-align: center;
    color: #662d91;
}
section.have-aq-section h2 {
    width: 100%;
    float: left;
}
button.btn {
    color: #662d91;
    background: #fff;
    margin-top: 30px;
}

/*Have aq section End*/
/*Footer*/
footer {
    width: 100%;
    float: left;
    padding: 0;
    margin: 0;
    background: #662d91;
    color: #f4f0f7;
}
.footer-1 {
    width: 100%;
    float: left;
    padding: 130px 0 115px;
}
.f-logo{margin-bottom:10px;}
.f-logo img {
    width: 140px;
}
.footer-1 a{color:#f4f0f7;}
.footer-1 h4 {
    font-family: 'Poppins', sans-serif;
    font-size: 16px;
    margin-bottom: 25px;
	color:#fff;
}
.footer-1 .news-letter {
    position: relative;
    width: 100%;
}
.news-letter form {
    position: relative;
    width: 100%;
    padding-right: 127px;
}
.news-letter form:before {
    content: '';
    position: absolute;
    width: 30px;
    height: 22px;
    right: 0;
    top: -25px;
    background:url(../assets/img/nl-shape.png);
	
}
.news-letter input {
    border-radius: 5px;
    padding: 10px;
    width: 100%;
}
.f-box ul li {
    width: 100%;
    margin-bottom: 10px;
}
.news-letter form input[type="submit"] {
    width: 123px;
    position: absolute;
    top: 1px;
    right: 0;
    background: #f58675;
    color: #fff;
    border: 1px solid #f58675;
    cursor: pointer;
}
.fsocialmedia a {
    display: inline-block;
    background: #fff;
    width: 30px;
    height: 30px;
    text-align: center;
    color: #662d91;
    border-radius: 30px;
    margin-right: 6px;
    align-content: center;
    line-height: 31px;
}
.fsocialmedia a:hover{ opacity:0.8; }
.news-letter input {
    border-radius: 5px;
    padding: 10px;
    background: #fff;
    border-color: transparent;
}
.news-letter input {
    border-radius: 5px;
    padding: 10px;
}
.footer-2 {
    width: 100%;
    float: left;
    padding: 30px 0;
    border-top: 1px solid #8558a8;
}
.footer-2 ul{display:inline-block;}
.footer-2 ul li {
    display: inline-block;
    margin-right: 12px;
    font-size: 13px;
}
.footer-2 ul li a{ color:#fff; }
.footer-2 ul li:last-child{ margin-right:0; }
.footer-2  .right {
    text-align: right;
}
/*Footer End*/

/*Responsive*/
@media (min-width: 1200px){
.container {
    max-width: 1200px;
}
}
</style>
<script>
    //Copy to clip board 
function myFunction() {
  var copyText = document.getElementById("myInput");
  copyText.select();
  copyText.setSelectionRange(0, 99999);
  navigator.clipboard.writeText(copyText.value);
  
  var tooltip = document.getElementById("myTooltip");
  tooltip.innerHTML = "Copied: " + copyText.value;
}

function outFunc() {
  var tooltip = document.getElementById("myTooltip");
  tooltip.innerHTML = "Copy to clipboard";
}
//Copy to clip board End
//Sticky Search box
$(document).on("scroll", function(){
	if ($(document).scrollTop() > 400){
		$(".search-con").addClass("shrink");
	} else {
		$(".search-con").removeClass("shrink");
	}
	
});
//Sticky Search bar end

//Nav Menu
$(document).ready(function() {
	// executes when HTML-Document is loaded and DOM is ready
   
   // breakpoint and up  
   $(window).resize(function(){
	   if ($(window).width() >= 980){	
   
		 // when you hover a toggle show its dropdown menu
		 $(".navbar .dropdown-toggle").hover(function () {
			$(this).parent().toggleClass("show");
			$(this).parent().find(".dropdown-menu").toggleClass("show"); 
		  });
   
		   // hide the menu when the mouse leaves the dropdown
		 $( ".navbar .dropdown-menu" ).mouseleave(function() {
		   $(this).removeClass("show");  
		 });
	 
		   // do something here
	   }	
   });  
	 
	 
   
   // document ready  
   });
//Nav Menu End

//Nav Menu

(function($){
	$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
	  if (!$(this).next().hasClass('show')) {
		$(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
	  }
	  var $subMenu = $(this).next(".dropdown-menu");
	  $subMenu.toggleClass('show');

	  $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
		$('.dropdown-submenu .show').removeClass("show");
	  });

	  return false;
	});
})(jQuery)

//Nav Menu End

//AOS animation
AOS.init();
//AOS animation end
//Box Slider
 $(document).ready(function() {
    $("#slider-a").owlCarousel({
        loop: true,
  		margin: 10,
  		nav: true,
  		navText: [ "<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
  		autoplay: true,
  		autoplayHoverPause: true,
  		responsive: {
  		0: {items: 1},
  		600: {items: 1},
  		1000: {items:1}
  		} 
    });
	 $("#slider-b").owlCarousel({
        loop: true,
  		margin: 10,
  		nav: true,
  		navText: [ "<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
  		autoplay: true,
  		autoplayHoverPause: true,
  		responsive: {
  		0: {items: 1},
  		600: {items: 1},
  		1000: {items:1}
  		} 
    });
	$("#slider-c").owlCarousel({
        loop: true,
  		margin: 10,
  		nav: true,
  		navText: [ "<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
  		autoplay: true,
  		autoplayHoverPause: true,
  		responsive: {
  		0: {items: 1},
  		600: {items: 1},
  		1000: {items:1}
  		} 
    });
	$("#slider-d").owlCarousel({
        loop: true,
  		margin: 10,
  		nav: true,
  		navText: [ "<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
  		autoplay: true,
  		autoplayHoverPause: true,
  		responsive: {
  		0: {items: 1},
  		600: {items: 1},
  		1000: {items:1}
  		} 
    });
	$("#slider-e").owlCarousel({
        loop: true,
  		margin: 10,
  		nav: true,
  		navText: [ "<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
  		autoplay: true,
  		autoplayHoverPause: true,
  		responsive: {
  		0: {items: 1},
  		600: {items: 1},
  		1000: {items:1}
  		} 
    });
});




//Box Slider End


$(function(){
    $('#myModal').modal({
        show: false
    }).on('hidden.bs.modal', function(){
        $(this).find('video')[0].pause();
    });
});

//Read More red less destination page
function myFunction() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");
  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Click to expand"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Click to hide"; 
    moreText.style.display = "inline";
  }
}
//Click to expand_2
function myFunction() {
  var dots = document.getElementById("dots_2");
  var moreText = document.getElementById("more_2");
  var btnText = document.getElementById("myBtn_2");
  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Click to expand"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Click to hide"; 
    moreText.style.display = "inline";
  }
}
//Click to expand_3
function myFunction() {
  var dots = document.getElementById("dots_3");
  var moreText = document.getElementById("more_3");
  var btnText = document.getElementById("myBtn_3");
  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Click to expand"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Click to hide"; 
    moreText.style.display = "inline";
  }
}
//Click to expand_3
function myFunction() {
  var dots = document.getElementById("dots_4");
  var moreText = document.getElementById("more_4");
  var btnText = document.getElementById("myBtn_4");
  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Click to expand"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Click to hide"; 
    moreText.style.display = "inline";
  }
}
//Read More red less
//Add class on body on loading
	window.onload = function() {
    document.body.className += " loaded";
}
//Add class on body on loading
//AOS Animation
  AOS.init(
  {duration: 1200,}
  );
 //AOS Animation End 
 
 

</script>


@endsection

