@if(Session::has('success'))
	<script type="text/javascript">
		swal({
		  title: "Good job!",
		  text: "<?php echo Session::get('success');?>",
		  icon: "success",
		  button: "Ok",
		});
	</script>
@endif
@if(Session::has('message'))
	<script type="text/javascript">
		swal({
		  title: "Good job!",
		  text: "<?php echo Session::get('message');?>",
		  icon: "success",
		  button: "Ok",
		});
	</script>

@endif
@if(Session::has('warning'))
	<script type="text/javascript">
		swal({
		  title: "Warning !",
		  text: "<?php echo Session::get('warning');?>",
		  icon: "warning",
		  button: "Ok",
		});
	</script>
@endif
@if(Session::has('alert'))
	<script type="text/javascript">
		swal({
		  title: "Oops!",
		  text: "<?php echo Session::get('alert');?>",
		  icon: "danger",
		  button: "Ok",
		});
	</script>
@endif

@if(isset($old->successmessage) && !empty($old->successmessage))
	<script type="text/javascript">
		swal({
		  title: "Well done!",
		  text: "<?php echo $old->successmessage;?>",
		  icon: "success",
		  button: "Ok",
		});
	</script>
@endif

@if(isset($old->failuremessage) && !empty($old->failuremessage))
	<script type="text/javascript">
		swal({
		  title: "Oops!",
		  text: "<?php echo $old->failuremessage;?>",
		  icon: "warning",
		  button: "Ok",
		});
	</script>
@endif