 @if(
 Route::currentRouteName()=="login" ||
 Route::currentRouteName()=="verification-process"|| Route::currentRouteName()=="password.reset" || Route::currentRouteName()=="password.request" ||
 Route::currentRouteName()=="verify-account" ||
 Route::currentRouteName()=="invitation-authorised-person-request"|| Route::currentRouteName()=="subscription-packages" ||
 Route::currentRouteName()=="verification-process-step" ||
 Route::currentRouteName()=="stripe-success-page" ||
 Route::currentRouteName()=="stripe-cancel-page"
 )
 @else
 <footer class="footer py-5">
   <div class="container">

     <div class="row">
       <div class="col-lg-6 mx-auto text-center">
         <a href="https://evaheld.com/terms-of-service.html" target="_blank" class="text-gradient text-dark  me-xl-4 me-3 mb-sm-0 mb-2">
           Terms of service
         </a>
         <a href="https://evaheld.com/privacy-policy.html" target="_blank" class="text-gradient text-dark  me-xl-4 me-3 mb-sm-0 mb-2">
           Privacy Policy
         </a>
       </div>
       <div class="col-lg-8 mx-auto text-center mb-4 mt-2">
         <a href="https://www.linkedin.com/company/evaheld/" target="_blank" class="text-dark me-xl-4 me-4">
           <span class="text-lg fab fa-linkedin"></span>
         </a>
         <a href="https://www.instagram.com/evaheld.io/" target="_blank" class="text-dark me-xl-4 me-4">
           <span class="text-lg fab fa-instagram"></span>
         </a>
         <a href="https://www.facebook.com/Evaheld-102025732470816" target="_blank" class="text-dark me-xl-4 me-4">
           <span class="text-lg fab fa-facebook"></span>
         </a>

       </div>
     </div>
     <div class="row">
       <div class="col-8 mx-auto text-center mt-1">
         <p class="mb-0 text-dark">
           © <?php echo date('Y'); ?>,
           made with <i class="fi fi_heart" style="color: #662D91; font-size: 14px"></i> by
           <a href="https://evaheld.com" class="font-weight-bold" target="_blank">Evaheld Dev Team</a>

         </p>
       </div>
     </div>
   </div>
 </footer>
 @endif