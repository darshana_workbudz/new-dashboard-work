@yield('content')
@include('layouts-frontend.alerts')

<script type="text/javascript">
	PREVIOUSPAGESESSIONURL = '<?php echo Session::get('previouspageurl');?>';
	ACTIVEROUTE = '<?php echo Session::get('activeroute');?>';
	$(document).find('.navbar-parent').show();

	if(
    ACTIVEROUTE=='login' || 
    ACTIVEROUTE=='verification-process' ||
    ACTIVEROUTE=='password.reset' ||
    ACTIVEROUTE=='password.request' ||
    ACTIVEROUTE=='verify-account' ||
    ACTIVEROUTE=='invitation-authorised-person-request' ||
    ACTIVEROUTE=="purchase-subscription" || 
    ACTIVEROUTE=="subscription-packages" || 
    ACTIVEROUTE=="dashboard.purchase-subscription" ||  
    ACTIVEROUTE=="stripe-success-page" || 
    ACTIVEROUTE=="stripe-cancel-page" ||
    ACTIVEROUTE=="message-verification" ||
    ACTIVEROUTE=="verification-process-step" ||
    ACTIVEROUTE=="verify-registration-member" ||
    ACTIVEROUTE=="verify-registration-partner" ||
    ACTIVEROUTE=="verify-registration-client" ||
    ACTIVEROUTE=="notify-user-status-verification" ||
    ACTIVEROUTE=="user-report.update-status"
    )
	{
		$(document).find('.navbar-parent').hide();
	}
	else
	{
		$(document).find('.navbar-parent').show();
	}

	$('.select2').select2();

	function formatState (state) {
    if (!state.id) {
      return state.text;
    }
    console.log(state);
    var baseUrl = "{{Request::root()}}/assets/img/flag";
    var $state = $(
      '<span><img class="img-flag"/> <span></span></span>'
    );

     var code=$('.phonecodeselect2 option:selected').data('code');

    // Use .text() instead of HTML string concatenation to avoid script injection issues
    $state.find("span").text(state.text);
    $state.find("img").attr("src", baseUrl + "/" + code + "-32.png");

    return $state;
  };

  $(".phonecodeselect2").select2({
    templateSelection: formatState
  }); 


</script>