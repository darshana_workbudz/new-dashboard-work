@extends(isset($isajax) && $isajax==true ? 'layouts-frontend.ajax' : 'layouts-frontend.master')
@section('content')
<div class="page-header min-vh-100">
  <div class="container">
    <div class="row">

      <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
        <div class="card card-plain mt-0">

          <div class="card-header pb-0 text-center">
            <img src="{{asset('assets/img/evaheld-logo.svg')}}" style="width:45%" alt="Evaheld logo" />
            @if(isset($payment) && $payment==true)
            <h4 class="font-weight-bolder mt-4 text-center">Payment Successful. </h4>
            @else
            <h4 class="font-weight-bolder mt-4 text-center">Verification Email Sent</h4>
            @endif
             <p class="mb-0 text-sm mx-auto text-dark">Please verify your email by clicking on the link in your email within 24 hours</p>
          </div>
          <div class="card-body">


            
           
            <div class="buttons">

              <button type="button" class="btn bg-primary w-100 mt-4 p-3 mb-0 text-uppercase text-white" onclick="$('.resend-email').show();$(this).hide()">
                I did not receive a verification email
              </button>
              <button type="button" data-route="{{route('verification-process')}}" style="display:none;" class="resend-email btn bg-purple w-100 mt-4 p-3 mb-0 text-uppercase text-white link-handle">
                Resend Email
              </button>
            </div>

          </div>
          <!-- <div class="card-footer text-center pt-0 p-0">
                  <span class="mb-4 text-sm mx-auto">
                    Go back to  
                    <a data-route="{{route('home')}}" class="link-handle text-primary font-weight-bold"> Home Page !</a>
                  </span><br>
                  
                </div> -->
        </div>
      </div>
      <div class="col-7 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 end-0 text-center justify-content-center flex-column" style="background-image: url('{{asset('assets/img/cover/happy-seniors-networking-together-at-home.jpg')}}'); background-size:cover;">
        <span class="mask bg-primary"></span>
        <div class="position-relative h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center ">

          <div class="container mt-10">
            <h2 class="mt-10 text-white font-weight-bolder">Leave less unsaid</h2>
            <p class="text-white text-lg">Privately create and safe-keep messages and memorialisation content, resting assured that they will be securely and independently delivered to your loved ones and memorialised as intended, posthumously.</p>
          </div>
        </div>
      </div>


    </div>

  </div>
</div>
@endsection
