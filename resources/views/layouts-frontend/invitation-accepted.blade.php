@extends(isset($isajax) && $isajax==true ? 'layouts-frontend.ajax' : 'layouts-frontend.master')
@section('content')
<div class="page-header min-vh-100">
  <div class="container">
    <div class="row">

     <div class="col-xl-4 col-lg-5 col-md-7 d-flex flex-column mx-lg-0 mx-auto">
        <div class="card card-plain mt-0">

          <div class="card-header pb-0 text-center">
              <a class="navbar-brand font-weight-bolder m-0 link-handle" data-route="{{route('home')}}" rel="tooltip" data-placement="bottom" target="_blank">
              <img src="{{asset('assets/img/evaheld-logo.png')}}" style="width:45%" alt="Evaheld logo">
              </a>
             <h4 class="font-weight-bolder mt-4 text-center">Trusted Party Appointment Accepted Successfully</h4>
              <p class="mb-0 text-sm mx-auto text-dark"> You are now the Trusted Party on "{{ucwords($userdata->ownerfname)}} {{ucwords($userdata->ownerlname)}}" Evaheld account. </p>

          </div>
         <div class="card-body text-center">
                 
                 <a href="{{$redirecturl}}" class="text-center btn bg-primary mb-0 mt-3 mb-3" type="button">Report Advised of Death </a>
                </div>
                
        </div>
      </div>
      <div class="col-7 d-lg-flex d-none h-100 my-auto pe-0 position-absolute top-0 end-0 text-center justify-content-center flex-column" style="background-image: url('{{asset('assets/img/cover/happy-seniors-networking-together-at-home.jpg')}}'); background-size:cover;">
        <span class="mask bg-primary"></span>
        <div class="position-relative h-100 m-3 px-7 border-radius-lg d-flex flex-column justify-content-center ">

          <div class="container mt-10">
            <h2 class="mt-10 text-white font-weight-bolder">Leave less unsaid</h2>
            <p class="text-white text-lg">Privately create and safe-keep messages and memorialisation content, resting assured that they will be securely and independently delivered to your loved ones and memorialised as intended, posthumously.</p>
          </div>
        </div>
      </div>



    </div>

  </div>
</div>
@endsection