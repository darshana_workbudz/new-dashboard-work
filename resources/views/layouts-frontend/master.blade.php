<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/favicons/apple-touch-icon.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/favicons/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/favicons/favicon-16x16.png')}}">
  <link rel="manifest" href="{{asset('assets/favicons/site.webmanifest')}}">
  <link rel="mask-icon" href="{{asset('assets/favicons/safari-pinned-tab.svg')}}" color="#662d91">
  <meta name="msapplication-TileColor" content="#662d91">
  <meta name="theme-color" content="#662d91">
  
  <title>{{ config('app.name', 'Evaheld') }}</title>
  
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <link href="{{asset('assets/css/nucleo-icons.css')}}" rel="stylesheet" />
  <link href="{{asset('assets/css/nucleo-svg.css')}}" rel="stylesheet" />
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <link href="{{asset('assets/css/nucleo-svg.css')}}" rel="stylesheet" />
  <link id="pagestyle" href="{{asset('assets/css/soft-ui-dashboard.css?v=1.0.9')}}" rel="stylesheet" />
  <link rel="stylesheet" href="{{asset('assets/js/plugins/datepicker.css')}}" >
  <link href="{{asset('assets/css/common-style.css')}}?v=1.0.9" rel="stylesheet" />
 
  <link href="{{asset('assets/js/plugins/sweetalert/css/sweetalert.css') }}" rel="stylesheet">
   <link rel="stylesheet" href="{{asset('assets/js/plugins/select2/css/select2.min.css')}}" >
  <script type="text/javascript">
   var BASEURL = "<?php echo url('/'); ?>";
   let PAGESESSIONNAME = '<?php echo Session::get('activeroute');?>';
   let PAGESESSIONURL = '<?php echo Session::get('activepageurl');?>';
   let PREVIOUSPAGESESSIONURL = '<?php echo Session::get('previouspageurl');?>';
 </script>
 
 @yield('style')
 <script type="text/javascript">
  (function() {
    window.sib = { equeue: [], client_key: "nfn0k19liint7azebbmqnv39" };
    /* OPTIONAL: email to identify request*/
    // window.sib.email_id = 'example@domain.com';
    /* OPTIONAL: to hide the chat on your script uncomment this line (0 = chat hidden; 1 = display chat) */
    // window.sib.display_chat = 0;
    // window.sib.display_logo = 0;
    /* OPTIONAL: to overwrite the default welcome message uncomment this line*/
    // window.sib.custom_welcome_message = 'Hello, how can we help you?';
    /* OPTIONAL: to overwrite the default offline message uncomment this line*/
    // window.sib.custom_offline_message = 'We are currently offline. In order to answer you, please indicate your email in your messages.';
    window.sendinblue = {}; for (var j = ['track', 'identify', 'trackLink', 'page'], i = 0; i < j.length; i++) { (function(k) { window.sendinblue[k] = function(){ var arg = Array.prototype.slice.call(arguments); (window.sib[k] || function() { var t = {}; t[k] = arg; window.sib.equeue.push(t);})(arg[0], arg[1], arg[2]);};})(j[i]);}var n = document.createElement("script"),i = document.getElementsByTagName("script")[0]; n.type = "text/javascript", n.id = "sendinblue-js", n.async = !0, n.src = "https://sibautomation.com/sa.js?key=" + window.sib.client_key, i.parentNode.insertBefore(n, i), window.sendinblue.page();
  })();
</script>
</head>

<body class="front-body">
   <div class="loadingOverlay"></div>
  <div class="loader loader-6">
    <div class="loader-inner"></div>
  </div> 

  <!-- <div class="progress page-progress-bar" style="display: none;">
    <div class="indeterminate"></div>
  </div> -->

  @include('layouts-frontend.navbar')

  


  <!--   Core JS Files   -->
  <script src="{{asset('assets/js/core/popper.min.js')}}"></script>
  <script src="{{asset('assets/js/core/bootstrap.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins/perfect-scrollbar.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins/smooth-scrollbar.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins/chartjs.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins/sweetalert/js/sweetalert.min.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }


  </script>
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <script src="{{asset('assets/js/soft-ui-dashboard.js?v=1.0.9')}}"></script>
  <!--   Core JS Files   -->

  
  <script src="{{asset('assets/js/plugins/bootstrap-datepicker.js')}}"></script>
  <script src="{{asset('assets/js/plugins/select2/js/select2.min.js')}}"></script>
   <link rel="stylesheet" href="https://assets.ziggeo.com/v2-stable/ziggeo.css" />
  <script src="https://assets.ziggeo.com/v2-stable/ziggeo.js"></script>
  <script src="{{asset('assets/js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>

  <script src="{{asset('assets/js/common-script.js')}}?v=1.0.9"></script>
  <script type="text/javascript" async="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOIifNM3p7o0Xu9L2tIudLGhN4CQDc0Sw&libraries=places&callback="></script>
  <main class="main-content  h-100 border-radius-lg ">
    <div class="refresh-div">
      @yield('content')
    </div>
    @include('layouts-frontend.footer')
  </main>
  @yield('script')
  <script type="text/javascript">
    $('.select2').select2();

  function formatState (state) {
    if (!state.id) {
      return state.text;
    }
    console.log(state);
    var baseUrl = "{{Request::root()}}/assets/img/flag";
    var $state = $(
      '<span><img class="img-flag " /> <span class="text-sm"></span></span>'
    );

     var code=$('.phonecodeselect2 option:selected').data('code');

    // Use .text() instead of HTML string concatenation to avoid script injection issues
    $state.find("span").text(state.text);
    $state.find("img").attr("src", baseUrl + "/" + code + "-32.png");

    return $state;
  };

  $(".phonecodeselect2").select2({
    
    templateSelection: formatState
  }); 

</script>
</body>
</html>