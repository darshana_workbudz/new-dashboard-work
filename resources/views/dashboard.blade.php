<!-- LAYOUT -->
@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
<!-- HEAD.INFO -->
@section('pageTitle', 'Dashboard')
@section('pageDescription', '')
<!-- HEADER.BREADCRUMBS -->
@section('page-breadcrumb','Dashboard')

<!-- MAIN.HEADER -->
@section('headercommon')

<div class="container-fluid">

  <div class="page-header min-height-200 border-radius-xl mt-4" style="background-image: url('{{asset('assets/img/curved-images/curved0.jpg')}}'); background-position-y: 50%;">
    <span class="mask bg-primary opacity-9"></span>
  </div>
  
  <div class="card card-body blur mx-3 mt-n6 overflow-hidden">
    <div class="row gx-4">
      <div class="col-auto">
        <div class="avatar avatar-xl position-relative border-radius-xl bg-purple">
          <div class="card-body z-index-3">
            <i class="fi fi_home text-white" style="font-size: 34px" aria-hidden="true"></i>
          </div>
        </div>
      </div>
      <div class="col-auto my-auto">
        <div class="h-100">
          <h5 class="mb-1">
            Hello, {{ auth()->user()->first_name }} {{ auth()->user()->last_name }} @if(auth()->user()->user_role_type) @endif
          </h5>
          <p class="mb-0 font-weight-bold text-sm" style="font-size:12px;">
            What would you like to do today?
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

<!-- MAIN.CONTENT -->
@section('content')
<div>
  

    @role('Admin')
    <div class="col-12">
      <h6>Manage Users</h6>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('b2c-users-clients.index')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1836" data-name="Group 1836" transform="translate(-16496 -13976)">
                    <rect id="Rectangle_538" data-name="Rectangle 538" width="54.083" height="55" rx="16" transform="translate(16496 13976)" fill="#f3eef6" />
                    <path id="Path_3851" data-name="Path 3851" d="M15.335,2A13.335,13.335,0,1,1,2,15.335,13.335,13.335,0,0,1,15.335,2Zm0,6.667a1,1,0,0,0-.991.864l-.009.136v4.667H9.668a1,1,0,0,0-.136,1.991l.136.009h4.667V21a1,1,0,0,0,1.991.136L16.335,21V16.335H21a1,1,0,0,0,.136-1.991L21,14.335H16.335V9.668A1,1,0,0,0,15.335,8.667Z" transform="translate(16508 13988)" fill="#662d91" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>See Our B2C Clients</h5>
                <p>Manage B2C Clients </p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>
            </div>
          </div>
        </a>
      </div>


      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('our-partners.index')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1836" data-name="Group 1836" transform="translate(-16496 -13976)">
                    <rect id="Rectangle_538" data-name="Rectangle 538" width="54.083" height="55" rx="16" transform="translate(16496 13976)" fill="#f3eef6" />
                    <path id="Path_3851" data-name="Path 3851" d="M15.335,2A13.335,13.335,0,1,1,2,15.335,13.335,13.335,0,0,1,15.335,2Zm0,6.667a1,1,0,0,0-.991.864l-.009.136v4.667H9.668a1,1,0,0,0-.136,1.991l.136.009h4.667V21a1,1,0,0,0,1.991.136L16.335,21V16.335H21a1,1,0,0,0,.136-1.991L21,14.335H16.335V9.668A1,1,0,0,0,15.335,8.667Z" transform="translate(16508 13988)" fill="#662d91" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>See Our Partners</h5>
                <p>Invite B2B Partners and Assign Coupon Codes</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>
            </div>
          </div>
        </a>
      </div>


      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('b2b-clients.index')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1836" data-name="Group 1836" transform="translate(-16496 -13976)">
                    <rect id="Rectangle_538" data-name="Rectangle 538" width="54.083" height="55" rx="16" transform="translate(16496 13976)" fill="#f3eef6" />
                    <path id="Path_3851" data-name="Path 3851" d="M15.335,2A13.335,13.335,0,1,1,2,15.335,13.335,13.335,0,0,1,15.335,2Zm0,6.667a1,1,0,0,0-.991.864l-.009.136v4.667H9.668a1,1,0,0,0-.136,1.991l.136.009h4.667V21a1,1,0,0,0,1.991.136L16.335,21V16.335H21a1,1,0,0,0,.136-1.991L21,14.335H16.335V9.668A1,1,0,0,0,15.335,8.667Z" transform="translate(16508 13988)" fill="#662d91" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>See Our B2B Clients</h5>
                <p>Invite B2B Clients and Manage Users</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>
            </div>
          </div>
        </a>
      </div>



    </div>
    <!-- SUPPORT SECTION -->
    <div class="col-12">
      <h6>Support</h6>
    </div>
    <div class="row mb-2">
      <!-- I need help -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('get-support')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1839" data-name="Group 1839" transform="translate(-16721 -14201)">
                    <rect id="Rectangle_638" data-name="Rectangle 638" width="54.083" height="55" rx="16" transform="translate(16721 14201)" fill="#ebf6fc" />
                    <path id="Path_3892" data-name="Path 3892" d="M15.335,2A13.335,13.335,0,1,1,9.142,27.147l-5.721,1.49a1.133,1.133,0,0,1-1.383-1.381l1.488-5.719A13.339,13.339,0,0,1,15.335,2Zm0,18a1.333,1.333,0,1,0,1.333,1.333A1.333,1.333,0,0,0,15.335,20Zm0-11.668A3.667,3.667,0,0,0,11.668,12a1,1,0,0,0,1.991.136l.019-.307A1.667,1.667,0,0,1,17,12a2.192,2.192,0,0,1-.86,1.776l-.405.413a3.985,3.985,0,0,0-1.4,3.144,1,1,0,0,0,2,0,2.192,2.192,0,0,1,.86-1.776l.405-.413A3.985,3.985,0,0,0,19,12a3.667,3.667,0,0,0-3.667-3.667Z" transform="translate(16733 14213)" fill="#3ea5dd" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>I need help</h5>
                <p>Reach out to our friendly&nbsp;team.</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>

            </div>
          </div>
        </a>
      </div>
      <!-- Give feedback -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('get-support')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1846" data-name="Group 1846" transform="translate(-16796 -14201)">
                    <rect id="Rectangle_639" data-name="Rectangle 639" width="54.083" height="55" rx="16" transform="translate(16796 14201)" fill="#ebf6fc" />
                    <path id="Path_3893" data-name="Path 3893" d="M13.668,18a3,3,0,0,1,3,3v2l-.011.144c-.413,2.836-2.96,4.2-7.234,4.2-4.258,0-6.846-1.347-7.4-4.15L2,23V21a3,3,0,0,1,3-3ZM9.334,7.334A4.667,4.667,0,1,1,4.667,12,4.667,4.667,0,0,1,9.334,7.334ZM25.669,2a3,3,0,0,1,3,3V9.668a3,3,0,0,1-3,3H23.6L20.21,15.611A1.333,1.333,0,0,1,18,14.6V12.649a3,3,0,0,1-2.667-2.982V5a3,3,0,0,1,3-3Z" transform="translate(16808 14214)" fill="#3ea5dd" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>Give Feedback</h5>
                <p>Share you experience and suggest features.</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>

            </div>
          </div>
        </a>
      </div>
    </div>

    <div class="col-12">
      <h6>Account</h6>
    </div>
    <div class="row mb-2">
      <!-- Update my details -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('my-profile.my-basic-info')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1844" data-name="Group 1844" transform="translate(-17320.999 -14126)">
                    <rect id="Rectangle_633" data-name="Rectangle 633" width="54.083" height="55" rx="16" transform="translate(17321 14126)" fill="#fff7ed" />
                    <path id="Path_3887" data-name="Path 3887" d="M22.34,18a3,3,0,0,1,3,3v1.224a3.667,3.667,0,0,1-.684,2.132c-2.062,2.885-5.429,4.313-9.99,4.313s-7.928-1.43-9.984-4.317A3.667,3.667,0,0,1,4,22.225V21a3,3,0,0,1,3-3H22.338ZM14.667,2A6.667,6.667,0,1,1,8,8.671,6.667,6.667,0,0,1,14.667,2Z" transform="translate(17332.996 14137.996)" fill="#f2ac3b" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>Update my details</h5>
                <p>Keep your information up&nbsp;to&nbsp;date.</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>

            </div>
          </div>
        </a>
      </div>
      <!-- Change my Password -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('my-profile.my-change-password')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1845" data-name="Group 1845" transform="translate(-17396.338 -14126)">
                    <rect id="Rectangle_634" data-name="Rectangle 634" width="54.083" height="55" rx="16" transform="translate(17396.338 14126)" fill="#fff7ed" />
                    <path id="Path_3902" data-name="Path 3902" d="M26.318,6.055A17.382,17.382,0,0,1,15.774,2.2a1.014,1.014,0,0,0-1.217,0A17.382,17.382,0,0,1,4.014,6.055,1.014,1.014,0,0,0,3,7.069v7.1c0,6.76,4,11.728,11.794,14.8a1.014,1.014,0,0,0,.743,0c7.8-3.071,11.794-8.04,11.794-14.8v-7.1A1.014,1.014,0,0,0,26.318,6.055ZM21.6,11.872l-8.111,7.435a1.014,1.014,0,0,1-1.4-.031L8.7,15.9a1.014,1.014,0,1,1,1.433-1.433l2.694,2.693,7.394-6.779a1.014,1.014,0,0,1,1.371,1.5Z" transform="translate(17408.338 14138)" fill="#f2ac3b" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>Change my Password</h5>
                <p>Keep your account&nbsp;secure.</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>

            </div>
          </div>
        </a>
      </div>
    </div>

    <div class="col-12">
      <h6>Account</h6>
    </div>
    <div class="row mb-2">
      <!-- Update my details -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('manage-coupons.index')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1844" data-name="Group 1844" transform="translate(-17320.999 -14126)">
                    <rect id="Rectangle_633" data-name="Rectangle 633" width="54.083" height="55" rx="16" transform="translate(17321 14126)" fill="#fff7ed" />
                    <path id="Path_3887" data-name="Path 3887" d="M22.34,18a3,3,0,0,1,3,3v1.224a3.667,3.667,0,0,1-.684,2.132c-2.062,2.885-5.429,4.313-9.99,4.313s-7.928-1.43-9.984-4.317A3.667,3.667,0,0,1,4,22.225V21a3,3,0,0,1,3-3H22.338ZM14.667,2A6.667,6.667,0,1,1,8,8.671,6.667,6.667,0,0,1,14.667,2Z" transform="translate(17332.996 14137.996)" fill="#f2ac3b" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>Manage Promotional Codes</h5>
                <p>Create Coupon Codes / Promotional Codes</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>

            </div>
          </div>
        </a>
      </div>
      
    </div>

    @endrole

    @role('Client')
  


    <!-- DASHBOARD CONTENT -->
    <div class="container px-3" id="hanging-icons">
      <div class="row g-4 py-4 row-cols-1 row-cols-lg-3">
        <div class="col d-flex align-items-start">
          <div class="text-dark flex-shrink-0 me-3">
            <svg xmlns="http://www.w3.org/2000/svg" width="31.416" height="31.948" viewBox="0 0 31.416 31.948" style="height: 26px; padding-top: 3px;">
              <g id="Group_1849" data-name="Group 1849" transform="translate(-16646 -13853)">
                <rect id="Rectangle_650" data-name="Rectangle 650" width="31.415" height="31.948" rx="15.708" transform="translate(16646 13853)" fill="#662d91" />
                <text id="_1" data-name="1" transform="translate(16661 13877.5)" fill="#fff" font-size="25" font-family="ProximaNova-Semibold, Proxima Nova" font-weight="600">
                  <tspan x="-4.75" y="0">1</tspan>
                </text>
              </g>
            </svg>
          </div>
          <div>
            <span>Step 1:</span>
            <h5>Add Recipients</h5>
            <a data-route="{{route('manage-recipients.create')}}" class="text-primary icon-move-right link-handle">
              Get started
              <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
            </a>
          </div>
        </div>
        <div class="col d-flex align-items-start">
          <div class="text-dark flex-shrink-0 me-3">
            <svg xmlns="http://www.w3.org/2000/svg" width="31.416" height="31.948" viewBox="0 0 31.416 31.948" style="height: 26px; padding-top: 3px;">
              <g id="Group_1850" data-name="Group 1850" transform="translate(-16689.564 -13853)">
                <rect id="Rectangle_651" data-name="Rectangle 651" width="31.415" height="31.948" rx="15.708" transform="translate(16689.564 13853)" fill="#662d91" />
                <text id="_2" data-name="2" transform="translate(16705 13877.5)" fill="#fff" font-size="25" font-family="ProximaNova-Semibold, Proxima Nova" font-weight="600">
                  <tspan x="-7.438" y="0">2</tspan>
                </text>
              </g>
            </svg>
          </div>
          <div>
            <span>Step 2:</span>
            <h5>Create Content</h5>
            <a data-route="{{route('create-content')}}" class="text-primary icon-move-right  link-handle">
              Get started
              <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
            </a>
          </div>
        </div>
        <div class="col d-flex align-items-start">
          <div class="text-dark flex-shrink-0 me-3">
            <svg xmlns="http://www.w3.org/2000/svg" width="31.416" height="31.948" viewBox="0 0 31.416 31.948" style="height: 26px; padding-top: 3px;">
              <g id="Group_1851" data-name="Group 1851" transform="translate(-16733.129 -13853.581)">
                <rect id="Rectangle_652" data-name="Rectangle 652" width="31.415" height="31.948" rx="15.708" transform="translate(16733.129 13853.581)" fill="#662d91" />
                <text id="_3" data-name="3" transform="translate(16749 13877.5)" fill="#fff" font-size="25" font-family="ProximaNova-Semibold, Proxima Nova" font-weight="600">
                  <tspan x="-7.175" y="0">3</tspan>
                </text>
              </g>
            </svg>
          </div>
          <div>
            <span>Step 3:</span>
            <h5>Set Release Options</h5>
            <a data-route="{{route('my-preferences.index')}}" class="text-primary icon-move-right link-handle">
              Get started
              <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="col-12 py-4">
      <hr />
    </div>

    <!-- MY CONTENT SECTION -->
    <div class="col-12">
      <h6>My Content</h6>
    </div>
    <div class="row mb-2">
      <!-- Create content -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('create-content')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1836" data-name="Group 1836" transform="translate(-16496 -13976)">
                    <rect id="Rectangle_538" data-name="Rectangle 538" width="54.083" height="55" rx="16" transform="translate(16496 13976)" fill="#f3eef6" />
                    <path id="Path_3851" data-name="Path 3851" d="M15.335,2A13.335,13.335,0,1,1,2,15.335,13.335,13.335,0,0,1,15.335,2Zm0,6.667a1,1,0,0,0-.991.864l-.009.136v4.667H9.668a1,1,0,0,0-.136,1.991l.136.009h4.667V21a1,1,0,0,0,1.991.136L16.335,21V16.335H21a1,1,0,0,0,.136-1.991L21,14.335H16.335V9.668A1,1,0,0,0,15.335,8.667Z" transform="translate(16508 13988)" fill="#662d91" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>Create content</h5>
                <p>Create or upload a video, audio, or written&nbsp;content.</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>
            </div>
          </div>
        </a>
      </div>
      <!-- See my content -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('my-content')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1837" data-name="Group 1837" transform="translate(-16570.999 -13976)">
                    <rect id="Rectangle_538" data-name="Rectangle 538" width="54.083" height="55" rx="16" transform="translate(16571 13976)" fill="#f3eef6" />
                    <path id="Path_3852" data-name="Path 3852" d="M6.667,3a2,2,0,0,1,2,2V25a2,2,0,0,1-2,2H4a2,2,0,0,1-2-2V5A2,2,0,0,1,4,3Zm8,0a2,2,0,0,1,2,2V25a2,2,0,0,1-2,2H12a2,2,0,0,1-2-2V5a2,2,0,0,1,2-2Zm9.709,4.166L28.663,23.86a2,2,0,0,1-1.44,2.435l-2.5.64a2,2,0,0,1-2.435-1.44L18,8.806a2,2,0,0,1,1.44-2.435l2.5-.644a2,2,0,0,1,2.435,1.44Z" transform="translate(16583 13988)" fill="#662d91" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>Content library</h5>
                <p>View your content and select your&nbsp;preferences.</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>

            </div>
          </div>
        </a>
      </div>
    </div>

    <!-- SUPPORT SECTION -->
    <div class="col-12">
      <h6>Support</h6>
    </div>
    <div class="row mb-2">
      <!-- I need help -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('get-support')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1839" data-name="Group 1839" transform="translate(-16721 -14201)">
                    <rect id="Rectangle_638" data-name="Rectangle 638" width="54.083" height="55" rx="16" transform="translate(16721 14201)" fill="#ebf6fc" />
                    <path id="Path_3892" data-name="Path 3892" d="M15.335,2A13.335,13.335,0,1,1,9.142,27.147l-5.721,1.49a1.133,1.133,0,0,1-1.383-1.381l1.488-5.719A13.339,13.339,0,0,1,15.335,2Zm0,18a1.333,1.333,0,1,0,1.333,1.333A1.333,1.333,0,0,0,15.335,20Zm0-11.668A3.667,3.667,0,0,0,11.668,12a1,1,0,0,0,1.991.136l.019-.307A1.667,1.667,0,0,1,17,12a2.192,2.192,0,0,1-.86,1.776l-.405.413a3.985,3.985,0,0,0-1.4,3.144,1,1,0,0,0,2,0,2.192,2.192,0,0,1,.86-1.776l.405-.413A3.985,3.985,0,0,0,19,12a3.667,3.667,0,0,0-3.667-3.667Z" transform="translate(16733 14213)" fill="#3ea5dd" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>I need help</h5>
                <p>Reach out to our friendly&nbsp;team.</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>

            </div>
          </div>
        </a>
      </div>
      <!-- Give feedback -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('get-support')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1846" data-name="Group 1846" transform="translate(-16796 -14201)">
                    <rect id="Rectangle_639" data-name="Rectangle 639" width="54.083" height="55" rx="16" transform="translate(16796 14201)" fill="#ebf6fc" />
                    <path id="Path_3893" data-name="Path 3893" d="M13.668,18a3,3,0,0,1,3,3v2l-.011.144c-.413,2.836-2.96,4.2-7.234,4.2-4.258,0-6.846-1.347-7.4-4.15L2,23V21a3,3,0,0,1,3-3ZM9.334,7.334A4.667,4.667,0,1,1,4.667,12,4.667,4.667,0,0,1,9.334,7.334ZM25.669,2a3,3,0,0,1,3,3V9.668a3,3,0,0,1-3,3H23.6L20.21,15.611A1.333,1.333,0,0,1,18,14.6V12.649a3,3,0,0,1-2.667-2.982V5a3,3,0,0,1,3-3Z" transform="translate(16808 14214)" fill="#3ea5dd" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>Give Feedback</h5>
                <p>Share you experience and suggest features.</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>

            </div>
          </div>
        </a>
      </div>
    </div>

    <!-- BILLING SECTION -->
    <div class="col-12">
      <h6>Billing</h6>
    </div>
    <div class="row mb-2">
      <!-- Explore Add-ons -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('my-account.add-on-features')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1840" data-name="Group 1840" transform="translate(-16871 -14051)">
                    <rect id="Rectangle_614" data-name="Rectangle 614" width="54.083" height="55" rx="16" transform="translate(16871 14051)" fill="#fef3f1" />
                    <path id="Path_3870" data-name="Path 3870" d="M10,2H6A4,4,0,0,0,2,6v4H5.611A3.334,3.334,0,0,1,10,5.611ZM2,12v6a4,4,0,0,0,4,4h4V13.415L7.707,15.708a1,1,0,0,1-1.413-1.413L8.587,12ZM12,22h2.026A8.668,8.668,0,0,1,28.67,16.415V12H13.415l2.294,2.294a1,1,0,1,1-1.413,1.414L12,13.415ZM28.67,10H16.391A3.334,3.334,0,0,0,12,5.611V2H24.669a4,4,0,0,1,4,4ZM13.335,10H12V8.667A1.333,1.333,0,1,1,13.335,10ZM10,10H8.667A1.333,1.333,0,1,1,10,8.657ZM30,22.669A7.334,7.334,0,1,0,22.669,30,7.334,7.334,0,0,0,30,22.669Zm-6.667.667v3.338a.667.667,0,0,1-1.333,0V23.336h-3.34a.667.667,0,0,1,0-1.333H22V18.669a.667.667,0,0,1,1.333,0V22h3.33a.667.667,0,0,1,0,1.333Z" transform="translate(16882 14062)" fill="#f58675" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>Explore Add-ons</h5>
                <p>Add a QR Code, Heirloom Keepsake or extra&nbsp;minutes.</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>

            </div>
          </div>
        </a>
      </div>
      <!-- Upgrade my account -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('my-account.upgrade-plan')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1841" data-name="Group 1841" transform="translate(-17021 -14051)">
                    <rect id="Rectangle_616" data-name="Rectangle 616" width="54.083" height="55" rx="16" transform="translate(17021 14051)" fill="#fef3f1" />
                    <path id="Path_3901" data-name="Path 3901" d="M16.574,6.128l-1.1,1.107-1.11-1.11A7.24,7.24,0,1,0,4.121,16.365L14.756,27a1.01,1.01,0,0,0,1.428,0L26.827,16.362A7.244,7.244,0,0,0,16.574,6.126Z" transform="translate(17033 14062.998)" fill="#f58675" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>Upgrade my account</h5>
                <p>Increase your recipient count and time&nbsp;allocation.</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>

            </div>
          </div>
        </a>
      </div>
      <!-- Add minutes -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('my-account.free-up-minutes')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1842" data-name="Group 1842" transform="translate(-17096 -14051)">
                    <rect id="Rectangle_617" data-name="Rectangle 617" width="54.083" height="55" rx="16" transform="translate(17096 14051)" fill="#fef3f1" />
                    <path id="Path_3872" data-name="Path 3872" d="M24.835,19.169a3.334,3.334,0,0,1,3.324,3.114l.007.219v2.672a3.334,3.334,0,0,1-6.658.22l-.007-.22V22.5A3.334,3.334,0,0,1,24.835,19.169ZM14.83,5.834a11.335,11.335,0,0,1,11.3,12.185,4.65,4.65,0,0,0-4.767,1.357A2.336,2.336,0,0,0,18.622,17.9l-.2.056-2,.672-.209.083-.192.1A2.346,2.346,0,0,0,14.9,21.385l.057.2.083.209.1.192.131.2a2.4,2.4,0,0,0,1.247.895l.211.053.105.016V27.5l.015.24v.587A11.335,11.335,0,1,1,14.828,5.835Zm5.331,14.2.008.133V27.5a1,1,0,0,1-1.991.136l-.009-.136V21.557l-.684.231a1,1,0,0,1-1.213-.5l-.053-.127a1,1,0,0,1,.505-1.213l.125-.053,2-.671a1,1,0,0,1,1.309.813Zm4.671,1.133a1.333,1.333,0,0,0-1.323,1.179L23.5,22.5v2.672a1.333,1.333,0,0,0,2.656.156l.008-.156V22.5a1.333,1.333,0,0,0-1.332-1.333Zm-10-11.332a1,1,0,0,0-.991.864l-.009.136v6l.009.136a1,1,0,0,0,1.982,0l.009-.136v-6L15.82,10.7a1,1,0,0,0-.991-.864ZM24.391,6l.108.083,1.534,1.332A1,1,0,0,1,24.828,9l-.109-.081L23.187,7.589A1,1,0,0,1,24.391,6ZM17.83,2.5a1,1,0,0,1,.136,1.991L17.83,4.5h-6a1,1,0,0,1-.136-1.991l.136-.009Z" transform="translate(17107.496 14062.5)" fill="#f58675" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>Add minutes</h5>
                <p>Increase your minutes to add more&nbsp;content.</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>

            </div>
          </div>
        </a>
      </div>
      <!-- See my subscription details -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('my-account.my-billing')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1843" data-name="Group 1843" transform="translate(-17246 -14051)">
                    <rect id="Rectangle_619" data-name="Rectangle 619" width="54.083" height="55" rx="16" transform="translate(17246 14051)" fill="#fef3f1" />
                    <path id="Path_3874" data-name="Path 3874" d="M2,8.667A3.667,3.667,0,0,1,5.667,5H25A3.667,3.667,0,0,1,28.67,8.667V11H2ZM2,13H28.67v7A3.667,3.667,0,0,1,25,23.669H5.667A3.667,3.667,0,0,1,2,20Zm18.335,4.667a1,1,0,1,0,0,2h3.334a1,1,0,0,0,0-2Z" transform="translate(17258 14064)" fill="#f58675" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>Manage my package</h5>
                <p>View payments and package&nbsp;details.</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>

            </div>
          </div>
        </a>
      </div>
    </div>

    <!-- ACCOUNT SECTION -->
    <div class="col-12">
      <h6>Account</h6>
    </div>
    <div class="row mb-2">
      <!-- Update my details -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('my-profile.my-basic-info')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1844" data-name="Group 1844" transform="translate(-17320.999 -14126)">
                    <rect id="Rectangle_633" data-name="Rectangle 633" width="54.083" height="55" rx="16" transform="translate(17321 14126)" fill="#fff7ed" />
                    <path id="Path_3887" data-name="Path 3887" d="M22.34,18a3,3,0,0,1,3,3v1.224a3.667,3.667,0,0,1-.684,2.132c-2.062,2.885-5.429,4.313-9.99,4.313s-7.928-1.43-9.984-4.317A3.667,3.667,0,0,1,4,22.225V21a3,3,0,0,1,3-3H22.338ZM14.667,2A6.667,6.667,0,1,1,8,8.671,6.667,6.667,0,0,1,14.667,2Z" transform="translate(17332.996 14137.996)" fill="#f2ac3b" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>Update my details</h5>
                <p>Keep your information up&nbsp;to&nbsp;date.</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>

            </div>
          </div>
        </a>
      </div>
      <!-- Change my Password -->
      <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
        <a data-route="{{route('my-profile.my-change-password')}}" class="link-handle" class="text-primary icon-move-right">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
            <div class="info-horizontal">

              <div class="icon">

                <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1845" data-name="Group 1845" transform="translate(-17396.338 -14126)">
                    <rect id="Rectangle_634" data-name="Rectangle 634" width="54.083" height="55" rx="16" transform="translate(17396.338 14126)" fill="#fff7ed" />
                    <path id="Path_3902" data-name="Path 3902" d="M26.318,6.055A17.382,17.382,0,0,1,15.774,2.2a1.014,1.014,0,0,0-1.217,0A17.382,17.382,0,0,1,4.014,6.055,1.014,1.014,0,0,0,3,7.069v7.1c0,6.76,4,11.728,11.794,14.8a1.014,1.014,0,0,0,.743,0c7.8-3.071,11.794-8.04,11.794-14.8v-7.1A1.014,1.014,0,0,0,26.318,6.055ZM21.6,11.872l-8.111,7.435a1.014,1.014,0,0,1-1.4-.031L8.7,15.9a1.014,1.014,0,1,1,1.433-1.433l2.694,2.693,7.394-6.779a1.014,1.014,0,0,1,1.371,1.5Z" transform="translate(17408.338 14138)" fill="#f2ac3b" />
                  </g>
                </svg>

              </div>
              <div class="description ps-5">
                <h5>Change my Password</h5>
                <p>Keep your account&nbsp;secure.</p>
                <span class="text-primary icon-move-right">
                  <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                </span>
              </div>

            </div>
          </div>
        </a>
      </div>
      
  </div>
  <!-- END DASHBOARD CONTENT -->

  @endrole

  

  @role('B2B Partner')
  <div class="container px-3" id="hanging-icons">
    <div class="row g-4 py-4 row-cols-1 row-cols-lg-3">
      <div class="col d-flex align-items-start">
        <div class="text-dark flex-shrink-0 me-3">
          <svg xmlns="http://www.w3.org/2000/svg" width="31.416" height="31.948" viewBox="0 0 31.416 31.948" style="height: 26px; padding-top: 3px;">
            <g id="Group_1849" data-name="Group 1849" transform="translate(-16646 -13853)">
              <rect id="Rectangle_650" data-name="Rectangle 650" width="31.415" height="31.948" rx="15.708" transform="translate(16646 13853)" fill="#662d91" />
              <text id="_1" data-name="1" transform="translate(16661 13877.5)" fill="#fff" font-size="25" font-family="ProximaNova-Semibold, Proxima Nova" font-weight="600">
                <tspan x="-4.75" y="0">1</tspan>
              </text>
            </g>
          </svg>
        </div>
        <div>
          <span>Step 1:</span>
          <h5>Buy Pre purchased packages</h5>
          <a data-route="{{route('partner.buy-packages')}}" class="text-primary icon-move-right link-handle">
            Get started
            <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
          </a>
        </div>
      </div>
      <div class="col d-flex align-items-start">
        <div class="text-dark flex-shrink-0 me-3">
          <svg xmlns="http://www.w3.org/2000/svg" width="31.416" height="31.948" viewBox="0 0 31.416 31.948" style="height: 26px; padding-top: 3px;">
            <g id="Group_1850" data-name="Group 1850" transform="translate(-16689.564 -13853)">
              <rect id="Rectangle_651" data-name="Rectangle 651" width="31.415" height="31.948" rx="15.708" transform="translate(16689.564 13853)" fill="#662d91" />
              <text id="_2" data-name="2" transform="translate(16705 13877.5)" fill="#fff" font-size="25" font-family="ProximaNova-Semibold, Proxima Nova" font-weight="600">
                <tspan x="-7.438" y="0">2</tspan>
              </text>
            </g>
          </svg>
        </div>
        <div>
          <span>Step 2:</span>
          <h5>Invite Members</h5>
          <a data-route="{{route('partner.our-members')}}" class="text-primary icon-move-right link-handle">
            Get started
            <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
          </a>
        </div>
      </div>
      <div class="col d-flex align-items-start">
        <div class="text-dark flex-shrink-0 me-3">
          <svg xmlns="http://www.w3.org/2000/svg" width="31.416" height="31.948" viewBox="0 0 31.416 31.948" style="height: 26px; padding-top: 3px;">
            <g id="Group_1851" data-name="Group 1851" transform="translate(-16733.129 -13853.581)">
              <rect id="Rectangle_652" data-name="Rectangle 652" width="31.415" height="31.948" rx="15.708" transform="translate(16733.129 13853.581)" fill="#662d91" />
              <text id="_3" data-name="3" transform="translate(16749 13877.5)" fill="#fff" font-size="25" font-family="ProximaNova-Semibold, Proxima Nova" font-weight="600">
                <tspan x="-7.175" y="0">3</tspan>
              </text>
            </g>
          </svg>
        </div>
        <div>
          <span>Step 3:</span>
          <h5>Share Packages to Members</h5>
          <a data-route="{{route('partner.pre-purchase-packages')}}" class="text-primary icon-move-right  link-handle">
            Get started
            <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 py-4">
    <hr />
  </div>
  <div class="col-12">
    <h6>Support</h6>
  </div>
  <div class="row mb-2">
    <!-- I need help -->
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
      <a data-route="{{route('get-support')}}" class="link-handle" class="text-primary icon-move-right">
        <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
          <div class="info-horizontal">

            <div class="icon">

              <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                <g id="Group_1839" data-name="Group 1839" transform="translate(-16721 -14201)">
                  <rect id="Rectangle_638" data-name="Rectangle 638" width="54.083" height="55" rx="16" transform="translate(16721 14201)" fill="#ebf6fc" />
                  <path id="Path_3892" data-name="Path 3892" d="M15.335,2A13.335,13.335,0,1,1,9.142,27.147l-5.721,1.49a1.133,1.133,0,0,1-1.383-1.381l1.488-5.719A13.339,13.339,0,0,1,15.335,2Zm0,18a1.333,1.333,0,1,0,1.333,1.333A1.333,1.333,0,0,0,15.335,20Zm0-11.668A3.667,3.667,0,0,0,11.668,12a1,1,0,0,0,1.991.136l.019-.307A1.667,1.667,0,0,1,17,12a2.192,2.192,0,0,1-.86,1.776l-.405.413a3.985,3.985,0,0,0-1.4,3.144,1,1,0,0,0,2,0,2.192,2.192,0,0,1,.86-1.776l.405-.413A3.985,3.985,0,0,0,19,12a3.667,3.667,0,0,0-3.667-3.667Z" transform="translate(16733 14213)" fill="#3ea5dd" />
                </g>
              </svg>

            </div>
            <div class="description ps-5">
              <h5>I need help</h5>
              <p>Reach out to our friendly&nbsp;team.</p>
              <span class="text-primary icon-move-right">
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
              </span>
            </div>

          </div>
        </div>
      </a>
    </div>
    <!-- Give feedback -->
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
      <a data-route="{{route('get-support')}}" class="link-handle" class="text-primary icon-move-right">
        <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
          <div class="info-horizontal">

            <div class="icon">

              <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                <g id="Group_1846" data-name="Group 1846" transform="translate(-16796 -14201)">
                  <rect id="Rectangle_639" data-name="Rectangle 639" width="54.083" height="55" rx="16" transform="translate(16796 14201)" fill="#ebf6fc" />
                  <path id="Path_3893" data-name="Path 3893" d="M13.668,18a3,3,0,0,1,3,3v2l-.011.144c-.413,2.836-2.96,4.2-7.234,4.2-4.258,0-6.846-1.347-7.4-4.15L2,23V21a3,3,0,0,1,3-3ZM9.334,7.334A4.667,4.667,0,1,1,4.667,12,4.667,4.667,0,0,1,9.334,7.334ZM25.669,2a3,3,0,0,1,3,3V9.668a3,3,0,0,1-3,3H23.6L20.21,15.611A1.333,1.333,0,0,1,18,14.6V12.649a3,3,0,0,1-2.667-2.982V5a3,3,0,0,1,3-3Z" transform="translate(16808 14214)" fill="#3ea5dd" />
                </g>
              </svg>

            </div>
            <div class="description ps-5">
              <h5>Give Feedback</h5>
              <p>Share you experience and suggest features.</p>
              <span class="text-primary icon-move-right">
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
              </span>
            </div>

          </div>
        </div>
      </a>
    </div>
  </div>
  <!-- ACCOUNT SECTION -->
  <div class="col-12">
    <h6>Account</h6>
  </div>
  <div class="row mb-2">
    <!-- Update my details -->
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
      <a data-route="{{route('partner.our-organisational-details')}}" class="link-handle" class="text-primary icon-move-right">
        <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
          <div class="info-horizontal">

            <div class="icon">

              <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                <g id="Group_1844" data-name="Group 1844" transform="translate(-17320.999 -14126)">
                  <rect id="Rectangle_633" data-name="Rectangle 633" width="54.083" height="55" rx="16" transform="translate(17321 14126)" fill="#fff7ed" />
                  <path id="Path_3887" data-name="Path 3887" d="M22.34,18a3,3,0,0,1,3,3v1.224a3.667,3.667,0,0,1-.684,2.132c-2.062,2.885-5.429,4.313-9.99,4.313s-7.928-1.43-9.984-4.317A3.667,3.667,0,0,1,4,22.225V21a3,3,0,0,1,3-3H22.338ZM14.667,2A6.667,6.667,0,1,1,8,8.671,6.667,6.667,0,0,1,14.667,2Z" transform="translate(17332.996 14137.996)" fill="#f2ac3b" />
                </g>
              </svg>

            </div>
            <div class="description ps-5">
              <h5>Update my details</h5>
              <p>Keep your information up&nbsp;to&nbsp;date.</p>
              <span class="text-primary icon-move-right">
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
              </span>
            </div>

          </div>
        </div>
      </a>
    </div>
    <!-- Change my Password -->
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
      <a data-route="{{route('partner.change-password')}}" class="link-handle" class="text-primary icon-move-right">
        <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
          <div class="info-horizontal">

            <div class="icon">

              <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                <g id="Group_1845" data-name="Group 1845" transform="translate(-17396.338 -14126)">
                  <rect id="Rectangle_634" data-name="Rectangle 634" width="54.083" height="55" rx="16" transform="translate(17396.338 14126)" fill="#fff7ed" />
                  <path id="Path_3902" data-name="Path 3902" d="M26.318,6.055A17.382,17.382,0,0,1,15.774,2.2a1.014,1.014,0,0,0-1.217,0A17.382,17.382,0,0,1,4.014,6.055,1.014,1.014,0,0,0,3,7.069v7.1c0,6.76,4,11.728,11.794,14.8a1.014,1.014,0,0,0,.743,0c7.8-3.071,11.794-8.04,11.794-14.8v-7.1A1.014,1.014,0,0,0,26.318,6.055ZM21.6,11.872l-8.111,7.435a1.014,1.014,0,0,1-1.4-.031L8.7,15.9a1.014,1.014,0,1,1,1.433-1.433l2.694,2.693,7.394-6.779a1.014,1.014,0,0,1,1.371,1.5Z" transform="translate(17408.338 14138)" fill="#f2ac3b" />
                </g>
              </svg>

            </div>
            <div class="description ps-5">
              <h5>Change my Password</h5>
              <p>Keep your account&nbsp;secure.</p>
              <span class="text-primary icon-move-right">
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
              </span>
            </div>

          </div>
        </div>
      </a>
    </div>
    <!-- My Notification Preferences -->
    <!-- <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
      <a data-route="###########" class="link-handle" class="text-primary icon-move-right">
        <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
          <div class="info-horizontal">
            <div class="icon icon-md border-radius-xl bg-yellow-pale text-center me-2 d-flex align-items-center justify-content-center">
              <span class="fi fi_alert text-yellow" style="font-size: 28px"></span>
            </div>
            <div class="description ps-2">
              <h5>My Notification Preferences</h5>
              <p>Manage your notification settings</p>
              <span class="text-primary icon-move-right">
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
              </span>
            </div>
          </div>
        </div>
      </a>
    </div> -->
  </div>  
  <div class="col-12">
    <h6>Our Usage </h6>
  </div>
  <div class="row mt-4">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
      <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1 ">

        <div class="text-center p-3"><h6>Total  Clients</h6></div>
        <div class="circlechart" data-percentage=""><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="0,100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5"><?php echo isset($totalclients) && !empty($totalclients) ? $totalclients : 0;?></text><text class="circle-chart__subline" x="16.91549431" y="22">

        </text> </g></svg>

        <p class="text-xs h-20 text-dark">

         Number of clients signed up by members

       </p>


     </div>

   </div>
 </div>
 <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
  <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1 ">

    <div class="text-center p-3"><h6>Total  Members</h6></div>
    <div class="circlechart" data-percentage=""><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="0,100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5"><?php echo isset($memberscount) && !empty($memberscount) ? $memberscount : 0;?></text><text class="circle-chart__subline" x="16.91549431" y="22">

    </text> </g></svg>

    <p class="text-xs h-20 text-dark">

     Number of members using the service

   </p>
   <p class="text-xs text-info link-handle" data-route="{{route('partner.our-members')}}">View Members</p>

 </div>

</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
  <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1 ">

    <div class="text-center p-3"><h6>Total Pre Purchase Packages</h6></div>
    <div class="circlechart" data-percentage=""><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="0,100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5"><?php echo isset($totalpackages) && !empty($totalpackages) ? $totalpackages : 0;?></text><text class="circle-chart__subline" x="16.91549431" y="22">

    </text> </g></svg>

    <p class="text-xs h-20 text-dark">

     Number of Pre Purchase Packages

   </p>
   <p class="text-xs text-info link-handle" data-route="{{route('partner.pre-purchase-packages')}}">View Pre Purchase Packages</p>

 </div>

</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
  <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1 ">

    <div class="text-center p-3"><h6>Total Coupons</h6></div>
    <div class="circlechart" data-percentage=""><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="0,100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5"><?php echo isset($totalcoupons) && !empty($totalcoupons) ? $totalcoupons : 0;?></text><text class="circle-chart__subline" x="16.91549431" y="22">

    </text> </g></svg>

    <p class="text-xs h-20 text-dark">

     Number of Coupons

   </p>
   <p class="text-xs text-info link-handle" data-route="{{route('partner.coupon-codes')}}">View Coupons</p>

 </div>

</div>
</div>
</div>
@endrole

  

    @role('Client')
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 mt-4">
      @if(isset($usersubscription) && !empty($usersubscription))
      <h5>@if($usersubscription->package_id==4 || $usersubscription->package_id==5 ) Connection Package @elseif($usersubscription->package_id==8) Legacy Package @else {{$usersubscription->name}} @endif</h5>
      <div class="description">
        <p class="mt-2 mb-4">{{$usersubscription->usage_description}}</div>
          @endif
          <h6>My Usage</h6>
        </div>
        <div class="row mt-4">
         <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1 ">
            <?php $personalisedmessagepercentage=0;?>
            <?php $totalpersonalisedmessage=isset($usersubscription->personalised_message_value) ? $usersubscription->personalised_message_value : 0;?>
            <?php if($totalpersonalisedmessage>0): ?>
              <?php $personalisedmessagepercentage=($usedtotalpersonalisedmessage/$totalpersonalisedmessage)*100;?>
            <?php endif;?>
            <div class="text-center p-3"><h6>Personalised Messages</h6></div>
            <div class="circlechart" data-percentage="{{$personalisedmessagepercentage}}"><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="{{$personalisedmessagepercentage}},100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5">{{$usedtotalpersonalisedmessage}}</text><text class="circle-chart__subline" x="16.91549431" y="22">

            </text> </g></svg>

            <p class="text-xs h-20 text-dark">
              @if(($totalpersonalisedmessage-$usedtotalpersonalisedmessage)<=0 && $totalpersonalisedmessage>0)
                <svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M10.909 2.782a2.25 2.25 0 0 1 2.975.74l.083.138 7.759 14.009a2.25 2.25 0 0 1-1.814 3.334l-.154.006H4.242A2.25 2.25 0 0 1 2.2 17.812l.072-.143L10.03 3.66a2.25 2.25 0 0 1 .879-.878ZM12 16.002a.999.999 0 1 0 0 1.997.999.999 0 0 0 0-1.997Zm-.002-8.004a1 1 0 0 0-.993.884L11 8.998 11 14l.007.117a1 1 0 0 0 1.987 0l.006-.117L13 8.998l-.007-.117a1 1 0 0 0-.994-.883Z" fill="#f2ac3b"/></svg>@endif
                @if($totalpersonalisedmessage>0)
                {{$totalpersonalisedmessage-$usedtotalpersonalisedmessage}}  personalised messages remaining 
                @else
                Unlimited messages remaining
                @endif 
              </p>

              @if(isset($usersubscription->upgradable) && !empty($usersubscription->upgradable))
              <p class="text-xs text-info link-handle" data-route="{{route('my-account.upgrade-plan')}}">Upgrade to create more messages</p>
              @endif
            </div>

          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1">
            <?php $minutespercentage=0;
            if($totalminutes>0): ?>
              <?php $minutespercentage=($usedtotalminutes/$totalminutes)*100;?>
            <?php endif;?>
            <div class="text-center p-3"><h6>Audio and Video Minutes</h6></div>
            <div class="circlechart" data-percentage="{{$minutespercentage}}"><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="{{$minutespercentage}},100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5">{{$usedtotalminutes}}</text><text class="circle-chart__subline" x="16.91549431" y="22">

            </text> </g></svg>
            <p class="text-xs h-20 text-dark">
              @if(($totalminutes-$usedtotalminutes)<=0)<svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M10.909 2.782a2.25 2.25 0 0 1 2.975.74l.083.138 7.759 14.009a2.25 2.25 0 0 1-1.814 3.334l-.154.006H4.242A2.25 2.25 0 0 1 2.2 17.812l.072-.143L10.03 3.66a2.25 2.25 0 0 1 .879-.878ZM12 16.002a.999.999 0 1 0 0 1.997.999.999 0 0 0 0-1.997Zm-.002-8.004a1 1 0 0 0-.993.884L11 8.998 11 14l.007.117a1 1 0 0 0 1.987 0l.006-.117L13 8.998l-.007-.117a1 1 0 0 0-.994-.883Z" fill="#f2ac3b"/></svg> @endif {{$totalminutes-$usedtotalminutes}} minutes remaining </p>

                @if(isset($usersubscription->upgradable) && !empty($usersubscription->upgradable) && $usersubscription->demopackage=='1')
                <p class="text-xs text-info link-handle" data-route="{{route('my-account.upgrade-plan')}}">Upgrade to record extra minutes</p>
                @else
                <p class="text-xs text-info link-handle" data-route="{{route('my-account.add-on-features')}}">Buy extra minutes</p>
                @endif

              </div>


            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1">
              <?php $pagespercentage=0;
              if($totalpagecount>0): ?>
                <?php $pagespercentage=($usedpagecount/$totalpagecount)*100;?>
              <?php endif;?>
              <div class="text-center p-3"><h6>Written Pages</h6></div>
              <div class="circlechart" data-percentage="{{$pagespercentage}}"><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="{{$pagespercentage}},100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5">{{$usedpagecount}}</text><text class="circle-chart__subline" x="16.91549431" y="22">

              </text> </g></svg>
              <p class="text-xs h-20 text-dark">
                @if(($totalpagecount-$usedpagecount)<=0)<svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M10.909 2.782a2.25 2.25 0 0 1 2.975.74l.083.138 7.759 14.009a2.25 2.25 0 0 1-1.814 3.334l-.154.006H4.242A2.25 2.25 0 0 1 2.2 17.812l.072-.143L10.03 3.66a2.25 2.25 0 0 1 .879-.878ZM12 16.002a.999.999 0 1 0 0 1.997.999.999 0 0 0 0-1.997Zm-.002-8.004a1 1 0 0 0-.993.884L11 8.998 11 14l.007.117a1 1 0 0 0 1.987 0l.006-.117L13 8.998l-.007-.117a1 1 0 0 0-.994-.883Z" fill="#f2ac3b"/></svg> @endif{{$totalpagecount-$usedpagecount}} pages remaining </p>

                  @if(isset($usersubscription->upgradable) && !empty($usersubscription->upgradable)  && $usersubscription->demopackage=='1')
                  <p class="text-xs text-info link-handle" data-route="{{route('my-account.upgrade-plan')}}">Upgrade to write a page</p>
                  @else
                  <p class="text-xs text-info link-handle" data-route="{{route('my-account.add-on-features')}}">Buy extra pages</p>
                  @endif
                </div>

              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
              <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1">
                <?php $recipientpercentage=0;
                if($totalrecipients>0):?>
                  <?php $recipientpercentage=($usedrecipients/$totalrecipients)*100;?>
                <?php endif;?>
                <div class="text-center p-3"><h6>Recipient Spaces</h6></div>
                <div class="circlechart" data-percentage="{{$recipientpercentage}}"><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="{{$recipientpercentage}},100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5">{{$usedrecipients}}</text><text class="circle-chart__subline" x="16.91549431" y="22">

                </text> </g></svg>

                <p class="text-xs h-20 text-dark">
                  @if(($hasrecipients)<=0)<svg width="24" height="24" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M10.909 2.782a2.25 2.25 0 0 1 2.975.74l.083.138 7.759 14.009a2.25 2.25 0 0 1-1.814 3.334l-.154.006H4.242A2.25 2.25 0 0 1 2.2 17.812l.072-.143L10.03 3.66a2.25 2.25 0 0 1 .879-.878ZM12 16.002a.999.999 0 1 0 0 1.997.999.999 0 0 0 0-1.997Zm-.002-8.004a1 1 0 0 0-.993.884L11 8.998 11 14l.007.117a1 1 0 0 0 1.987 0l.006-.117L13 8.998l-.007-.117a1 1 0 0 0-.994-.883Z" fill="#f2ac3b"/></svg> @endif {{$hasrecipients}} recipient spaces remaining </p>


                    @if(isset($usersubscription->upgradable) && !empty($usersubscription->upgradable))
                    <p class="text-xs text-info link-handle" data-route="{{route('my-account.upgrade-plan')}}">Upgrade to add extra recipients</p>
                    @endif
                  </div>

                </div>
              </div>
            </div>


            <h6>My Add-Ons</h6>
            <div class="row mb-2 mt-4">

              <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">

                <div class="card h-100 bg-white shadow-lg border-dark border-radius-md ">
                  <div class="info-horizontal">

                    <div class="d-flex p-2 mt-2">
                      <div class="avatar avatar-sm">
                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="55" viewBox="0 0 54.084 55">
                          <g id="Group_1855" data-name="Group 1855" transform="translate(-17175 -14322)">
                            <rect id="Rectangle_654" data-name="Rectangle 654" width="54.083" height="55" rx="16" transform="translate(17175 14322)" fill="#f3eef6"></rect>
                            <g id="Group_1852" data-name="Group 1852" transform="translate(17189 14337)">
                              <path id="Path_3906" data-name="Path 3906" d="M8.889,6H6V8.889H8.889Z" transform="translate(-1.667 -1.667)" fill="#f58675"></path>
                              <path id="Path_3907" data-name="Path 3907" d="M3,6.611A3.611,3.611,0,0,1,6.611,3h4.333a3.611,3.611,0,0,1,3.611,3.611v4.333a3.611,3.611,0,0,1-3.611,3.611H6.611A3.611,3.611,0,0,1,3,10.945Zm3.611-.722a.722.722,0,0,0-.722.722v4.333a.722.722,0,0,0,.722.722h4.333a.722.722,0,0,0,.722-.722V6.611a.722.722,0,0,0-.722-.722Zm.722,15.889h2.889v2.889H7.333Z" transform="translate(-3 -3)" fill="#f58675"></path>
                              <path id="Path_3908" data-name="Path 3908" d="M3,19.722a3.611,3.611,0,0,1,3.611-3.611h4.333a3.611,3.611,0,0,1,3.611,3.611v4.333a3.611,3.611,0,0,1-3.611,3.611H6.611A3.611,3.611,0,0,1,3,24.056ZM6.611,19a.722.722,0,0,0-.722.722v4.333a.722.722,0,0,0,.722.722h4.333a.722.722,0,0,0,.722-.722V19.722A.722.722,0,0,0,10.945,19ZM24.667,6H21.778V8.889h2.889Z" transform="translate(-3 -1.667)" fill="#f58675"></path>
                              <path id="Path_3909" data-name="Path 3909" d="M16.611,3A3.611,3.611,0,0,0,13,6.611v4.333a3.611,3.611,0,0,0,3.611,3.611h4.333a3.611,3.611,0,0,0,3.611-3.611V6.611A3.611,3.611,0,0,0,20.945,3Zm-.722,3.611a.722.722,0,0,1,.722-.722h4.333a.722.722,0,0,1,.722.722v4.333a.722.722,0,0,1-.722.722H16.611a.722.722,0,0,1-.722-.722ZM13,17.445h3.972v3.972H13Zm7.583,3.972H16.972v3.611H13V29h3.972V25.028h3.611V29h3.972V25.028H20.583Zm0,0V17.445h3.972v3.972Z" transform="translate(1.445 -3)" fill="#f58675"></path>
                            </g>
                          </g>
                        </svg>
                      </div>
                      <div class="ms-2 my-auto">
                        <h6 class="text-xs m-0 p-0">QR Code</h6>
                        <p class="mt-1 mb-0 text-xst">Make sharing your memory easy.</p>

                      </div>
                      <h6 class="mt-2 ps-2">{{$qraddoncount}}</h6>
                    </div>
                    <hr class="m-0">
                    @if(isset($usersubscription->demopackage) && $usersubscription->demopackage=='1')

                    <p class="text-xst text-center text-info p-1 mt-2 link-handle "  data-route="{{route('my-account.upgrade-plan')}}" >Upgrade to purchase a QR code</p>

                    @elseif($qraddoncount>0)

                    <p class="text-xst text-center text-info link-handle p-1 mt-2 " data-route="{{route('my-account.add-on-features')}}">Purchase more QR code</p>

                    @else

                    <p class="text-xst text-center text-info link-handle p-1 mt-2 " data-route="{{route('my-account.add-on-features')}}">Purchase a QR code</p>

                    @endif



                  </div>
                </div>

              </div>

              <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">

                <div class="card h-100 bg-white shadow-lg border-dark border-radius-md ">
                  <div class="info-horizontal">

                    <div class="d-flex p-2 mt-2">
                      <div class="avatar avatar-sm">
                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="55" viewBox="0 0 54.084 55">
                          <g id="Group_1854" data-name="Group 1854" transform="translate(-17101 -14322)">
                            <rect id="Rectangle_655" data-name="Rectangle 655" width="54.083" height="55" rx="16" transform="translate(17101 14322)" fill="#f3eef6"></rect>
                            <path id="Path_3905" data-name="Path 3905" d="M14.917,17.89v13H9.139A4.7,4.7,0,0,1,4.444,26.2V17.89Zm12.639,0V26.2a4.7,4.7,0,0,1-4.695,4.695H17.084v-13ZM19.611,2a4.695,4.695,0,0,1,3.955,7.225l3.629,0A1.676,1.676,0,0,1,29,10.728V14.94a1.676,1.676,0,0,1-1.806,1.505H17.084V9.223H14.917v7.221H4.806A1.676,1.676,0,0,1,3,14.94V10.728A1.676,1.676,0,0,1,4.806,9.223l3.629,0A4.695,4.695,0,0,1,16,3.7,4.68,4.68,0,0,1,19.611,2ZM12.389,4.167a2.528,2.528,0,0,0-.208,5.047l.208.009h2.528V6.695l-.009-.208A2.528,2.528,0,0,0,12.389,4.167Zm7.222,0a2.528,2.528,0,0,0-2.528,2.528V9.223h2.528a2.528,2.528,0,1,0,0-5.056Z" transform="translate(17112 14333.001)" fill="#f58675"></path>
                          </g>
                        </svg>
                      </div>
                      <div class="ms-2 my-auto">
                        <h6 class="text-xs m-0 p-0">Hand Delivered Heirloom </h6>
                        <p class="mt-1 mb-0 text-xst">A gift for generations to come.</p>

                      </div>
                      <h6 class="mt-2 ps-3">{{$heliroomaddoncount}}</h6>
                    </div>
                    <hr class="m-0">
                    @if(isset($usersubscription->demopackage) && $usersubscription->demopackage=='1')

                    <p class="text-xst text-center text-info p-1 mt-2 link-handle" data-route="{{route('my-account.upgrade-plan')}}">Upgrade to purchase an Heirloom Keepsake</p>

                    @elseif($heliroomaddoncount>0)

                    <p class="text-xst text-center text-info link-handle p-1 mt-2 " data-route="{{route('my-account.add-on-features')}}">Purchase more Heirloom Keepsake</p>

                    @else

                    <p class="text-xst text-center text-info link-handle p-1 mt-2 " data-route="{{route('my-account.add-on-features')}}">Purchase an Heirloom Keepsake</p>

                    @endif




                  </div>
                </div>

              </div>

              <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">

                <div class="card h-100 bg-white shadow-lg border-dark border-radius-md ">
                  <div class="info-horizontal">

                    <div class="d-flex p-2 mt-2">
                      <div class="avatar avatar-sm">
                       <svg xmlns="http://www.w3.org/2000/svg" width="40" height="55" viewBox="0 0 54.084 55">
                        <g id="Group_1842" data-name="Group 1842" transform="translate(-17096 -14051)">
                          <rect id="Rectangle_617" data-name="Rectangle 617" width="54.083" height="55" rx="16" transform="translate(17096 14051)" fill="#fef3f1"></rect>
                          <path id="Path_3872" data-name="Path 3872" d="M24.835,19.169a3.334,3.334,0,0,1,3.324,3.114l.007.219v2.672a3.334,3.334,0,0,1-6.658.22l-.007-.22V22.5A3.334,3.334,0,0,1,24.835,19.169ZM14.83,5.834a11.335,11.335,0,0,1,11.3,12.185,4.65,4.65,0,0,0-4.767,1.357A2.336,2.336,0,0,0,18.622,17.9l-.2.056-2,.672-.209.083-.192.1A2.346,2.346,0,0,0,14.9,21.385l.057.2.083.209.1.192.131.2a2.4,2.4,0,0,0,1.247.895l.211.053.105.016V27.5l.015.24v.587A11.335,11.335,0,1,1,14.828,5.835Zm5.331,14.2.008.133V27.5a1,1,0,0,1-1.991.136l-.009-.136V21.557l-.684.231a1,1,0,0,1-1.213-.5l-.053-.127a1,1,0,0,1,.505-1.213l.125-.053,2-.671a1,1,0,0,1,1.309.813Zm4.671,1.133a1.333,1.333,0,0,0-1.323,1.179L23.5,22.5v2.672a1.333,1.333,0,0,0,2.656.156l.008-.156V22.5a1.333,1.333,0,0,0-1.332-1.333Zm-10-11.332a1,1,0,0,0-.991.864l-.009.136v6l.009.136a1,1,0,0,0,1.982,0l.009-.136v-6L15.82,10.7a1,1,0,0,0-.991-.864ZM24.391,6l.108.083,1.534,1.332A1,1,0,0,1,24.828,9l-.109-.081L23.187,7.589A1,1,0,0,1,24.391,6ZM17.83,2.5a1,1,0,0,1,.136,1.991L17.83,4.5h-6a1,1,0,0,1-.136-1.991l.136-.009Z" transform="translate(17107.496 14062.5)" fill="#f58675"></path>
                        </g>
                      </svg>
                    </div>
                    <div class="ms-2 my-auto">
                      <h6 class="text-xs m-0 p-0">Extra Minutes </h6>
                      <p class="mt-1 mb-0 text-xst">Create more recorded content.</p>

                    </div>
                    <h6 class="mt-2 ps-3">{{$minutesaddoncount}}</h6>
                  </div>
                  <hr class="m-0">
                  <p class="text-xst text-center text-info link-handle p-1 mt-2 " data-route="{{route('my-account.add-on-features')}}">Purchase extra minutes</p>

                </div>
              </div>

            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">

              <div class="card h-100 bg-white shadow-lg border-dark border-radius-md ">
                <div class="info-horizontal">

                  <div class="d-flex p-2 mt-2">
                    <div class="avatar avatar-sm">
                      <svg xmlns="http://www.w3.org/2000/svg" width="40" height="55" viewBox="0 0 54.084 55">
                        <g id="Group_1842" data-name="Group 1842" transform="translate(-17096 -14051)">
                          <rect id="Rectangle_617" data-name="Rectangle 617" width="54.083" height="55" rx="16" transform="translate(17096 14051)" fill="#fef3f1"></rect>
                          <path id="Path_3872" data-name="Path 3872" d="M24.835,19.169a3.334,3.334,0,0,1,3.324,3.114l.007.219v2.672a3.334,3.334,0,0,1-6.658.22l-.007-.22V22.5A3.334,3.334,0,0,1,24.835,19.169ZM14.83,5.834a11.335,11.335,0,0,1,11.3,12.185,4.65,4.65,0,0,0-4.767,1.357A2.336,2.336,0,0,0,18.622,17.9l-.2.056-2,.672-.209.083-.192.1A2.346,2.346,0,0,0,14.9,21.385l.057.2.083.209.1.192.131.2a2.4,2.4,0,0,0,1.247.895l.211.053.105.016V27.5l.015.24v.587A11.335,11.335,0,1,1,14.828,5.835Zm5.331,14.2.008.133V27.5a1,1,0,0,1-1.991.136l-.009-.136V21.557l-.684.231a1,1,0,0,1-1.213-.5l-.053-.127a1,1,0,0,1,.505-1.213l.125-.053,2-.671a1,1,0,0,1,1.309.813Zm4.671,1.133a1.333,1.333,0,0,0-1.323,1.179L23.5,22.5v2.672a1.333,1.333,0,0,0,2.656.156l.008-.156V22.5a1.333,1.333,0,0,0-1.332-1.333Zm-10-11.332a1,1,0,0,0-.991.864l-.009.136v6l.009.136a1,1,0,0,0,1.982,0l.009-.136v-6L15.82,10.7a1,1,0,0,0-.991-.864ZM24.391,6l.108.083,1.534,1.332A1,1,0,0,1,24.828,9l-.109-.081L23.187,7.589A1,1,0,0,1,24.391,6ZM17.83,2.5a1,1,0,0,1,.136,1.991L17.83,4.5h-6a1,1,0,0,1-.136-1.991l.136-.009Z" transform="translate(17107.496 14062.5)" fill="#f58675"></path>
                        </g>
                      </svg>
                    </div>
                    <div class="ms-2 my-auto">
                      <h6 class="text-xs m-0 p-0">Extra Pages </h6>
                      <p class="mt-1 mb-0 text-xst">Create more written content.</p>

                    </div>
                    <h6 class="mt-2 ps-3">{{$pagesaddoncount}}</h6>
                  </div>
                  <hr class="m-0">
                  <p class="text-xst text-center text-info link-handle p-1 mt-2 " data-route="{{route('my-account.add-on-features')}}">Purchase extra pages</p>



                </div>
              </div>

            </div>

          </div>

          <h6>Preferences</h6>

          <div class="row mb-2 mt-4 ps-2">
            <div class="card h-100 bg-primary shadow-lg border-dark border-radius-md p-4 col-10">



              <div class="row">
                @if($userdata->default_prefrence=="1")
                <div class="info-horizontal col-7">
                  <div class="icon">
                   <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                    <g id="Group_1867" data-name="Group 1867" transform="translate(-16342 -14028)">
                      <rect id="Rectangle_648" data-name="Rectangle 648" width="54.083" height="55" rx="16" transform="translate(16342 14028)" fill="#ffffff"></rect>
                      <path id="Path_3903" data-name="Path 3903" d="M15.668,2a3.59,3.59,0,1,0,3.59,3.59A3.59,3.59,0,0,0,15.668,2ZM27.651,8.486a2.887,2.887,0,0,0-3.751-1.8l-2.308.862a1.077,1.077,0,0,0-.6.547,5.868,5.868,0,0,1-10.607.014,1.077,1.077,0,0,0-.6-.546L7.453,6.688a2.882,2.882,0,0,0-2.1,5.365l4.573,1.881v4.521l-2.832,8.2a2.81,2.81,0,1,0,5.289,1.9l2.651-7.12a.689.689,0,0,1,1.293,0l2.7,7.194a2.787,2.787,0,1,0,5.245-1.889l-2.859-8.3v-4.5L26,12.061A2.887,2.887,0,0,0,27.651,8.486Z" transform="translate(16353.41 14039.002)" fill="#662d91"></path>
                    </g>
                  </svg>
                </div>
                @if(isset($checkinsetting) && !empty($checkinsetting))
                <div class="description ps-6">
                  <span class="text-sm mb-0">You are enrolled in:</span>
                  <h5 class="text-white ">Evaheld's Wellbeing System</h5>
                  <div class="mt-3 text-xs">

                    <svg width="20" height="20" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2Zm3.22 6.97-4.47 4.47-1.97-1.97a.75.75 0 0 0-1.06 1.06l2.5 2.5a.75.75 0 0 0 1.06 0l5-5a.75.75 0 1 0-1.06-1.06Z" fill="#ffffff"/></svg>
                    <?php $checkindate=date('d-M-Y');?>
                    @if($checkinsetting->stage1_completed=='0')
                    <?php $checkindate=$checkinsetting->stage1_date;?>
                    @elseif($checkinsetting->stage2_completed=='0')
                    <?php $checkindate=$checkinsetting->stage2_date;?>
                    @elseif($checkinsetting->stage3_completed=='0')
                    <?php $checkindate=$checkinsetting->stage3_date;?>
                    @endif

                    Your next check-in date is: <?php echo date('d-M-Y',strtotime($checkindate));?> </div>

                  </div>

                  @else

                  <div class="description ps-6">
                  <span class="text-sm mb-0">Please Enroll Now in our:</span>
                  <h5 class="text-white ">Evaheld's Wellbeing System </h5>
                  <div class="mt-3 text-xs">
                  </div>
                </div>

                @endif
                </div>
                @endif
                @if($userdata->default_prefrence=="2" && isset($authoriseddata) && !empty($authoriseddata))  
                <div class="info-horizontal col-7">
                  <div class="icon">
                   <svg xmlns="http://www.w3.org/2000/svg" width="54.083" height="55" viewBox="0 0 54.083 55">
                    <g id="Group_1864" data-name="Group 1864" transform="translate(-16401 -14025)">
                      <rect id="Rectangle_649" data-name="Rectangle 649" width="54.083" height="55" rx="16" transform="translate(16401 14025)" fill="#ffffff"></rect>
                      <path id="Path_3904" data-name="Path 3904" d="M23.261,15.716a7.545,7.545,0,1,1-7.545,7.545,7.545,7.545,0,0,1,7.545-7.545Zm-7.514,2.743a8.918,8.918,0,0,0,.782,10.646,18.051,18.051,0,0,1-3.556.329c-4.691,0-8.154-1.471-10.269-4.44A3.772,3.772,0,0,1,2,22.807V21.545a3.085,3.085,0,0,1,3.085-3.086H15.748Zm3.885,4.316a.686.686,0,1,0-.97.97L21.4,26.488a.686.686,0,0,0,.97,0L27.861,21a.686.686,0,1,0-.97-.97l-5,5ZM12.972,2A6.859,6.859,0,1,1,6.114,8.863,6.859,6.859,0,0,1,12.972,2Z" transform="translate(16411.637 14036.096)" fill="#662d91"></path>
                    </g>
                  </svg>
                </div>
                <div class="description ps-6">
                  <span class="text-sm mb-0">You are enrolled in:</span>
                  <h5 class="text-white ">Trusted Party</h5>

                  <div class="mt-3 text-xs">

                    <svg width="20" height="20" fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2Zm3.22 6.97-4.47 4.47-1.97-1.97a.75.75 0 0 0-1.06 1.06l2.5 2.5a.75.75 0 0 0 1.06 0l5-5a.75.75 0 1 0-1.06-1.06Z" fill="#ffffff"/></svg>

                    {{$authoriseddata->first_name}} {{$authoriseddata->last_name}} is your Trusted party</div>

                  </div>
                </div>


                @endif

                <div class="info-horizontal col-5 mt-2">
                  <a data-route="{{route('my-preferences.index')}}" class="link-handle"><button class="btn-lg bg-white  border-radius-md text-sm" style="border:none;">Manage my preferences</button></a>
                </div>



              </div>

            </div>

          </div>
          


          @endrole


          

          @role('Member')

          <div class="container px-3" id="hanging-icons">
            <div class="row g-4 py-4 row-cols-1 row-cols-lg-3">
              <div class="col d-flex align-items-start">
                <div class="text-dark flex-shrink-0 me-3">
                  <svg xmlns="http://www.w3.org/2000/svg" width="31.416" height="31.948" viewBox="0 0 31.416 31.948" style="height: 26px; padding-top: 3px;">
                    <g id="Group_1849" data-name="Group 1849" transform="translate(-16646 -13853)">
                      <rect id="Rectangle_650" data-name="Rectangle 650" width="31.415" height="31.948" rx="15.708" transform="translate(16646 13853)" fill="#662d91" />
                      <text id="_1" data-name="1" transform="translate(16661 13877.5)" fill="#fff" font-size="25" font-family="ProximaNova-Semibold, Proxima Nova" font-weight="600">
                        <tspan x="-4.75" y="0">1</tspan>
                      </text>
                    </g>
                  </svg>
                </div>
                <div>
                  <span>Step 1:</span>
                  <h5>Invite Clients</h5>
                  <a data-route="{{route('member.our-clients')}}" class="text-primary icon-move-right link-handle">
                    Get started
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
              <div class="col d-flex align-items-start">
                <div class="text-dark flex-shrink-0 me-3">
                  <svg xmlns="http://www.w3.org/2000/svg" width="31.416" height="31.948" viewBox="0 0 31.416 31.948" style="height: 26px; padding-top: 3px;">
                    <g id="Group_1850" data-name="Group 1850" transform="translate(-16689.564 -13853)">
                      <rect id="Rectangle_651" data-name="Rectangle 651" width="31.415" height="31.948" rx="15.708" transform="translate(16689.564 13853)" fill="#662d91" />
                      <text id="_2" data-name="2" transform="translate(16705 13877.5)" fill="#fff" font-size="25" font-family="ProximaNova-Semibold, Proxima Nova" font-weight="600">
                        <tspan x="-7.438" y="0">2</tspan>
                      </text>
                    </g>
                  </svg>
                </div>
                <div>
                  <span>Step 2:</span>
                  <h5>Share Packages to Clients</h5>
                  <a data-route="{{route('member.pre-purchase-packages')}}" class="text-primary icon-move-right link-handle">
                    Get started
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
              <div class="col d-flex align-items-start">
                <div class="text-dark flex-shrink-0 me-3">
                  <svg xmlns="http://www.w3.org/2000/svg" width="31.416" height="31.948" viewBox="0 0 31.416 31.948" style="height: 26px; padding-top: 3px;">
                    <g id="Group_1851" data-name="Group 1851" transform="translate(-16733.129 -13853.581)">
                      <rect id="Rectangle_652" data-name="Rectangle 652" width="31.415" height="31.948" rx="15.708" transform="translate(16733.129 13853.581)" fill="#662d91" />
                      <text id="_3" data-name="3" transform="translate(16749 13877.5)" fill="#fff" font-size="25" font-family="ProximaNova-Semibold, Proxima Nova" font-weight="600">
                        <tspan x="-7.175" y="0">3</tspan>
                      </text>
                    </g>
                  </svg>
                </div>
                <div>
                  <span>Step 3:</span>
                  <h5>Share Codes to Clients</h5>
                  <a data-route="{{route('member.coupon-codes')}}" class="text-primary icon-move-right  link-handle">
                    Get started
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 py-4">
            <hr />
          </div>
          <div class="col-12">
            <h6>Support</h6>
          </div>
          <div class="row mb-2">
            <!-- I need help -->
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
              <a data-route="{{route('get-support')}}" class="link-handle" class="text-primary icon-move-right">
                <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
                  <div class="info-horizontal">

                    <div class="icon">

                      <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                        <g id="Group_1839" data-name="Group 1839" transform="translate(-16721 -14201)">
                          <rect id="Rectangle_638" data-name="Rectangle 638" width="54.083" height="55" rx="16" transform="translate(16721 14201)" fill="#ebf6fc" />
                          <path id="Path_3892" data-name="Path 3892" d="M15.335,2A13.335,13.335,0,1,1,9.142,27.147l-5.721,1.49a1.133,1.133,0,0,1-1.383-1.381l1.488-5.719A13.339,13.339,0,0,1,15.335,2Zm0,18a1.333,1.333,0,1,0,1.333,1.333A1.333,1.333,0,0,0,15.335,20Zm0-11.668A3.667,3.667,0,0,0,11.668,12a1,1,0,0,0,1.991.136l.019-.307A1.667,1.667,0,0,1,17,12a2.192,2.192,0,0,1-.86,1.776l-.405.413a3.985,3.985,0,0,0-1.4,3.144,1,1,0,0,0,2,0,2.192,2.192,0,0,1,.86-1.776l.405-.413A3.985,3.985,0,0,0,19,12a3.667,3.667,0,0,0-3.667-3.667Z" transform="translate(16733 14213)" fill="#3ea5dd" />
                        </g>
                      </svg>

                    </div>
                    <div class="description ps-5">
                      <h5>I need help</h5>
                      <p>Reach out to our friendly&nbsp;team.</p>
                      <span class="text-primary icon-move-right">
                        <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                      </span>
                    </div>

                  </div>
                </div>
              </a>
            </div>
            <!-- Give feedback -->
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
              <a data-route="{{route('get-support')}}" class="link-handle" class="text-primary icon-move-right">
                <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
                  <div class="info-horizontal">

                    <div class="icon">

                      <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                        <g id="Group_1846" data-name="Group 1846" transform="translate(-16796 -14201)">
                          <rect id="Rectangle_639" data-name="Rectangle 639" width="54.083" height="55" rx="16" transform="translate(16796 14201)" fill="#ebf6fc" />
                          <path id="Path_3893" data-name="Path 3893" d="M13.668,18a3,3,0,0,1,3,3v2l-.011.144c-.413,2.836-2.96,4.2-7.234,4.2-4.258,0-6.846-1.347-7.4-4.15L2,23V21a3,3,0,0,1,3-3ZM9.334,7.334A4.667,4.667,0,1,1,4.667,12,4.667,4.667,0,0,1,9.334,7.334ZM25.669,2a3,3,0,0,1,3,3V9.668a3,3,0,0,1-3,3H23.6L20.21,15.611A1.333,1.333,0,0,1,18,14.6V12.649a3,3,0,0,1-2.667-2.982V5a3,3,0,0,1,3-3Z" transform="translate(16808 14214)" fill="#3ea5dd" />
                        </g>
                      </svg>

                    </div>
                    <div class="description ps-5">
                      <h5>Give Feedback</h5>
                      <p>Share you experience and suggest features.</p>
                      <span class="text-primary icon-move-right">
                        <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                      </span>
                    </div>

                  </div>
                </div>
              </a>
            </div>
          </div>
          <!-- ACCOUNT SECTION -->
          <div class="col-12">
            <h6>Account</h6>
          </div>
          <div class="row mb-2">
            <!-- Update my details -->
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
              <a data-route="{{route('member.our-organisational-details')}}" class="link-handle" class="text-primary icon-move-right">
                <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
                  <div class="info-horizontal">

                    <div class="icon">

                      <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                        <g id="Group_1844" data-name="Group 1844" transform="translate(-17320.999 -14126)">
                          <rect id="Rectangle_633" data-name="Rectangle 633" width="54.083" height="55" rx="16" transform="translate(17321 14126)" fill="#fff7ed" />
                          <path id="Path_3887" data-name="Path 3887" d="M22.34,18a3,3,0,0,1,3,3v1.224a3.667,3.667,0,0,1-.684,2.132c-2.062,2.885-5.429,4.313-9.99,4.313s-7.928-1.43-9.984-4.317A3.667,3.667,0,0,1,4,22.225V21a3,3,0,0,1,3-3H22.338ZM14.667,2A6.667,6.667,0,1,1,8,8.671,6.667,6.667,0,0,1,14.667,2Z" transform="translate(17332.996 14137.996)" fill="#f2ac3b" />
                        </g>
                      </svg>

                    </div>
                    <div class="description ps-5">
                      <h5>Update my details</h5>
                      <p>Keep your information up&nbsp;to&nbsp;date.</p>
                      <span class="text-primary icon-move-right">
                        <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                      </span>
                    </div>

                  </div>
                </div>
              </a>
            </div>
            <!-- Change my Password -->
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
              <a data-route="{{route('member.change-password')}}" class="link-handle" class="text-primary icon-move-right">
                <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
                  <div class="info-horizontal">

                    <div class="icon">

                      <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                        <g id="Group_1845" data-name="Group 1845" transform="translate(-17396.338 -14126)">
                          <rect id="Rectangle_634" data-name="Rectangle 634" width="54.083" height="55" rx="16" transform="translate(17396.338 14126)" fill="#fff7ed" />
                          <path id="Path_3902" data-name="Path 3902" d="M26.318,6.055A17.382,17.382,0,0,1,15.774,2.2a1.014,1.014,0,0,0-1.217,0A17.382,17.382,0,0,1,4.014,6.055,1.014,1.014,0,0,0,3,7.069v7.1c0,6.76,4,11.728,11.794,14.8a1.014,1.014,0,0,0,.743,0c7.8-3.071,11.794-8.04,11.794-14.8v-7.1A1.014,1.014,0,0,0,26.318,6.055ZM21.6,11.872l-8.111,7.435a1.014,1.014,0,0,1-1.4-.031L8.7,15.9a1.014,1.014,0,1,1,1.433-1.433l2.694,2.693,7.394-6.779a1.014,1.014,0,0,1,1.371,1.5Z" transform="translate(17408.338 14138)" fill="#f2ac3b" />
                        </g>
                      </svg>

                    </div>
                    <div class="description ps-5">
                      <h5>Change my Password</h5>
                      <p>Keep your account&nbsp;secure.</p>
                      <span class="text-primary icon-move-right">
                        <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                      </span>
                    </div>

                  </div>
                </div>
              </a>
            </div>

  </div>  
   <div class="col-12">
    <h6>Our Usage </h6>
  </div>
 <div class="row mt-4">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
      <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1 ">

        <div class="text-center p-3"><h6>Total  Clients</h6></div>
        <div class="circlechart" data-percentage=""><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="0,100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5"><?php echo isset($totalclients) && !empty($totalclients) ? $totalclients : 0;?></text><text class="circle-chart__subline" x="16.91549431" y="22">

        </text> </g></svg>

        <p class="text-xs h-20 text-dark">

         Number of clients signed up by members

       </p>
       <p class="text-xs text-info link-handle" data-route="{{route('member.our-clients')}}">View My Clients</p>

     </div>

   </div>
 </div>
 <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
  <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1 ">

    <div class="text-center p-3"><h6>Total  Messages</h6></div>
    <div class="circlechart" data-percentage=""><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="0,100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5"><?php echo isset($totalclientsmessage) && !empty($totalclientsmessage) ? $totalclientsmessage : 0;?></text><text class="circle-chart__subline" x="16.91549431" y="22">

    </text> </g></svg>

    <p class="text-xs h-20 text-dark">

     Number of videos my clients have created

   </p>
   

 </div>

</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
  <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1 ">

    <div class="text-center p-3"><h6>My Clients Packages</h6></div>
    <div class="circlechart"  data-percentage=""><svg style="width: 60px;" class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="0,100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5"><?php echo isset($totalpackages) && !empty($totalpackages) ? $totalpackages : 0;?></text><text class="circle-chart__subline" x="16.91549431" y="22">

    Connection Package</text> </g></svg>
    <svg class="circle-chart" style="width: 60px;" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="0,100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5"><?php echo isset($totalpackages) && !empty($totalpackages) ? $totalpackages : 0;?></text><text class="circle-chart__subline" x="16.91549431" y="22">

    Legacy Package</text> </g></svg>
    <svg class="circle-chart" style="width: 60px;" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="0,100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5"><?php echo isset($totalpackages) && !empty($totalpackages) ? $totalpackages : 0;?></text><text class="circle-chart__subline" x="16.91549431" y="22">

    Connection QR Code</text> </g></svg>
    <svg class="circle-chart" style="width: 60px;" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="0,100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5"><?php echo isset($totalpackages) && !empty($totalpackages) ? $totalpackages : 0;?></text><text class="circle-chart__subline" x="16.91549431" y="22">

    Legacy QR Code</text> </g></svg>

    <p class="text-xs h-20 text-dark">

    The packages my clients are using

   </p>
   <p class="text-xs text-info link-handle" data-route="{{route('member.pre-purchase-packages')}}">View My Pre Purchase Packages</p>

 </div>

</div>
</div>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
  <div class="card h-100 bg-white shadow-lg border-dark border-radius-md p-1 ">

    <div class="text-center p-3"><h6>Total Coupons</h6></div>
    <div class="circlechart" data-percentage=""><svg class="circle-chart" viewBox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg"><circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9"></circle><circle class="circle-chart__circle success-stroke" stroke-dasharray="0,100" cx="16.9" cy="16.9" r="15.9"></circle><g class="circle-chart__info">   <text class="circle-chart__percent" x="16.9" y="15.5"><?php echo isset($totalcoupons) && !empty($totalcoupons) ? $totalcoupons : 0;?></text><text class="circle-chart__subline" x="16.91549431" y="22">

    </text> </g></svg>

    <p class="text-xs h-20 text-dark">

     Number of Coupons

   </p>
   <p class="text-xs text-info link-handle" data-route="{{route('member.coupon-codes')}}">View My Coupons</p>

 </div>

</div>
</div>
</div>
  @endrole



  <!-- Statistics   END -->

</div>




      @section('script')
      <script type="text/javascript">
        var updateCheckinsetting="<?php echo isset($updateCheckinsetting) && !empty($updateCheckinsetting) ? $updateCheckinsetting : null;?>";
        if(updateCheckinsetting)
        {
          swal({
            title: "Well done!",
            text: "Your Check In Process Completed Successfully",
            icon: "success",
            button: "ok",
          });
        }
      </script>
      <script src="{{asset('assets/js/plugins/threejs.js')}}"></script>
      <script src="{{asset('assets/js/plugins/orbit-controls.js')}}"></script>
      <script type="text/javascript">
        (function() {
          const container = document.getElementById("globe");
          const canvas = container.getElementsByTagName("canvas")[0];

          const globeRadius = 100;
          const globeWidth = 4098 / 2;
          const globeHeight = 1968 / 2;

          function convertFlatCoordsToSphereCoords(x, y) {
            let latitude = ((x - globeWidth) / globeWidth) * -180;
            let longitude = ((y - globeHeight) / globeHeight) * -90;
            latitude = (latitude * Math.PI) / 180;
            longitude = (longitude * Math.PI) / 180;
            const radius = Math.cos(longitude) * globeRadius;

            return {
              x: Math.cos(latitude) * radius,
              y: Math.sin(longitude) * globeRadius,
              z: Math.sin(latitude) * radius
            };
          }

          function makeMagic(points) {
            const {
              width,
              height
            } = container.getBoundingClientRect();

      // 1. Setup scene
      const scene = new THREE.Scene();
      // 2. Setup camera
      const camera = new THREE.PerspectiveCamera(45, width / height);
      // 3. Setup renderer
      const renderer = new THREE.WebGLRenderer({
        canvas,
        antialias: true
      });
      renderer.setSize(width, height);
      // 4. Add points to canvas
      // - Single geometry to contain all points.
      const mergedGeometry = new THREE.Geometry();
      // - Material that the dots will be made of.
      const pointGeometry = new THREE.SphereGeometry(0.5, 1, 1);
      const pointMaterial = new THREE.MeshBasicMaterial({
        color: "#989db5",
      });

      for (let point of points) {
        const {
          x,
          y,
          z
        } = convertFlatCoordsToSphereCoords(
          point.x,
          point.y,
          width,
          height
          );

        if (x && y && z) {
          pointGeometry.translate(x, y, z);
          mergedGeometry.merge(pointGeometry);
          pointGeometry.translate(-x, -y, -z);
        }
      }

      const globeShape = new THREE.Mesh(mergedGeometry, pointMaterial);
      scene.add(globeShape);

      container.classList.add("peekaboo");

      // Setup orbital controls
      camera.orbitControls = new THREE.OrbitControls(camera, canvas);
      camera.orbitControls.enableKeys = false;
      camera.orbitControls.enablePan = false;
      camera.orbitControls.enableZoom = false;
      camera.orbitControls.enableDamping = false;
      camera.orbitControls.enableRotate = true;
      camera.orbitControls.autoRotate = true;
      camera.position.z = -265;

      function animate() {
        // orbitControls.autoRotate is enabled so orbitControls.update
        // must be called inside animation loop.
        camera.orbitControls.update();
        requestAnimationFrame(animate);
        renderer.render(scene, camera);
      }
      animate();
    }

    function hasWebGL() {
      const gl =
      canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
      if (gl && gl instanceof WebGLRenderingContext) {
        return true;
      } else {
        return false;
      }
    }

    function init() {
      if (hasWebGL()) {
        window
        window.fetch("https://raw.githubusercontent.com/creativetimofficial/public-assets/master/soft-ui-dashboard-pro/assets/js/points.json")
        .then(response => response.json())
        .then(data => {
          makeMagic(data.points);
        });
      }
    }
    init();
  })();

  function makesvg(percentage, inner_text=""){

    var abs_percentage = Math.abs(percentage).toString();
    var percentage_str = percentage.toString();
    var classes = ""

    if(percentage < 0){
      classes = "danger-stroke circle-chart__circle--negative";
    } else if(percentage > 0 && percentage <= 30){
      classes = "warning-stroke";
    } else{
      classes = "success-stroke";
    }

    var svg = '<svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">'
    + '<circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9" />'
    + '<circle class="circle-chart__circle '+classes+'"'
    + 'stroke-dasharray="'+ abs_percentage+',100"    cx="16.9" cy="16.9" r="15.9" />'
    + '<g class="circle-chart__info">'
    + '   <text class="circle-chart__percent" x="16.9" y="15.5">'+percentage_str+'%</text>';

    if(inner_text){
      svg += '<text class="circle-chart__subline" x="16.91549431" y="22">'+inner_text+'</text>'
    }
    
    svg += ' </g></svg>';
    
    return svg
  }

  (function( $ ) {

    $.fn.circlechart = function() {
      this.each(function() {
        var percentage = $(this).data("percentage");
        var inner_text = $(this).text();
        $(this).html(makesvg(percentage, inner_text));
      });
      return this;
    };

  }( jQuery ));

  $(function () {
   $('.circlechart').circlechart();
 });
</script>


@stop
@endsection