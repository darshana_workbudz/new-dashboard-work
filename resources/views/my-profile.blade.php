@extends(isset($isajax) && $isajax==true ? 'layouts-backend.ajax' : 'layouts-backend.app')
@section('page-type','list-page')
@section('page-breadcrumb','My Profile')

@section('headercommon')
<x-main.header icon="person" title="My Profile" subtitle="Manage your profile details" />
@endsection

@section('content')
<div>
  <div class="row mt-4">
    <!-- Update my details -->
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
      <a data-route="{{route('my-profile.my-basic-info')}}" class="link-handle" class="text-primary icon-move-right">
        <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
          <div class="info-horizontal">
            <div class="icon icon-md border-radius-xl bg-yellow-pale text-center me-2 d-flex align-items-center justify-content-center">
              <span class="fi fi_person text-yellow" style="font-size: 28px"></span>
            </div>
            <div class="description ps-2">
              <h5>Update my details</h5>
              <p>Keep your information up&nbsp;to&nbsp;date</p>
              <span class="text-primary icon-move-right">
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
              </span>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!-- Change my Password -->
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
      <a data-route="{{route('my-profile.my-change-password')}}" class="link-handle" class="text-primary icon-move-right">
        <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
          <div class="info-horizontal">
            <div class="icon icon-md border-radius-xl bg-yellow-pale text-center me-2 d-flex align-items-center justify-content-center">
              <span class="fi fi_shield_task text-yellow" style="font-size: 28px"></span>
            </div>
            <div class="description ps-2">
              <h5>Change my Password</h5>
              <p>Keep your account&nbsp;secure</p>
              <span class="text-primary icon-move-right">
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
              </span>
            </div>
          </div>
        </div>
      </a>
    </div>
    <!-- My Notification Preferences -->
    <!-- <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
      <a data-route="###########" class="link-handle" class="text-primary icon-move-right">
        <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
          <div class="info-horizontal">
            <div class="icon icon-md border-radius-xl bg-yellow-pale text-center me-2 d-flex align-items-center justify-content-center">
              <span class="fi fi_alert text-yellow" style="font-size: 28px"></span>
            </div>
            <div class="description ps-2">
              <h5>My Notification Preferences</h5>
              <p>Manage your notification settings</p>
              <span class="text-primary icon-move-right">
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
              </span>
            </div>
          </div>
        </div>
      </a>
    </div> -->
  </div>
</div>

<h6>Manage My Account </h6>

 <div class="row mt-4">

        <!-- Add-On Features -->
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3 " style="cursor: pointer" onclick="OpenConfirmModal()">
          <a class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1845" data-name="Group 1845" transform="translate(-17396.338 -14126)">
                    <rect id="Rectangle_634" data-name="Rectangle 634" width="54.083" height="55" rx="16" transform="translate(17396.338 14126)" fill="#fef3f1"></rect>
                    <path id="Path_3902" data-name="Path 3902" d="M26.318,6.055A17.382,17.382,0,0,1,15.774,2.2a1.014,1.014,0,0,0-1.217,0A17.382,17.382,0,0,1,4.014,6.055,1.014,1.014,0,0,0,3,7.069v7.1c0,6.76,4,11.728,11.794,14.8a1.014,1.014,0,0,0,.743,0c7.8-3.071,11.794-8.04,11.794-14.8v-7.1A1.014,1.014,0,0,0,26.318,6.055ZM21.6,11.872l-8.111,7.435a1.014,1.014,0,0,1-1.4-.031L8.7,15.9a1.014,1.014,0,1,1,1.433-1.433l2.694,2.693,7.394-6.779a1.014,1.014,0,0,1,1.371,1.5Z" transform="translate(17408.338 14138)" fill="#f58675"></path>
                  </g>
                </svg>


                </div>
                <div class="description ps-5">
                  <h5>Close My Account</h5>
                  <span class="text-primary icon-move-right">
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </a>
        </div>

        <!-- Upgrade My Plan -->
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 px-lg-2 mt-lg-0 mt-4 mb-3">
          <a data-route="{{route('my-preferences.index')}}" class="link-handle" class="text-primary icon-move-right">
            <div class="card h-100 bg-white shadow-lg border-dark border-radius-xl p-4">
              <div class="info-horizontal">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="54.084" height="55" viewBox="0 0 54.084 55">
                  <g id="Group_1845" data-name="Group 1845" transform="translate(-17396.338 -14126)">
                    <rect id="Rectangle_634" data-name="Rectangle 634" width="54.083" height="55" rx="16" transform="translate(17396.338 14126)" fill="#fef3f1"></rect>
                    <path id="Path_3902" data-name="Path 3902" d="M26.318,6.055A17.382,17.382,0,0,1,15.774,2.2a1.014,1.014,0,0,0-1.217,0A17.382,17.382,0,0,1,4.014,6.055,1.014,1.014,0,0,0,3,7.069v7.1c0,6.76,4,11.728,11.794,14.8a1.014,1.014,0,0,0,.743,0c7.8-3.071,11.794-8.04,11.794-14.8v-7.1A1.014,1.014,0,0,0,26.318,6.055ZM21.6,11.872l-8.111,7.435a1.014,1.014,0,0,1-1.4-.031L8.7,15.9a1.014,1.014,0,1,1,1.433-1.433l2.694,2.693,7.394-6.779a1.014,1.014,0,0,1,1.371,1.5Z" transform="translate(17408.338 14138)" fill="#f58675"></path>
                  </g>
                </svg>
                </div>
                <div class="description ps-5">
                  <h5>Manage Preferences</h5>
                  <span class="text-primary icon-move-right">
                    <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i>
                  </span>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>

<div class="modal fade" id="ConfirmModal" tabindex="-1" role="dialog" aria-labelledby="MessageDetailsLabel" aria-hidden="true" >
      <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content p-0" >
          <div class="modal-body p-5" id="ConfirmModalBody" >

          <h4>Are you sure you want to delete your account?</h4>

          <div class="description ">
            <p class="text-sm">After submiting this form you have <b>14 days</b>   to log back into your account to restore it before its <b>permanently deleted</b>.</p>
            <p class="text-sm mt-3 p-0 mb-0">
              The following will be deleted as well.
             </p>
              <span class="text-xs"><b>1. Your Uploaded messages</b></span><br>
              <span class="text-xs"><b>2. Your Recipients</b></span><br>
              <span class="text-xs"><b>3. Your Trusted Party Users</b></span><br><br>

              <span class="text-sm mt-4">Type "DELETE" Below if you want to proceed</span>

            <input type="text" name="confimationdelete" id="confimationdelete" class="confimationdelete form-control p-2 mt-2">


          </div>
          </div>

          <div class="modal-footer  d-flex" style="border:none;">

            <div class="ms-auto">
                <button type="button" class="btn bg-secondary btn-md mx-2 text-white" onclick="HideConfirmModal()">Cancel</button>
                <button type="button" class="btn bg-danger btn-md text-white" onclick="CloseMyAccount()" >Delete Account</button>
            </div>
        </div>
</div>  
</div>
</div>
<script type="text/javascript">

  $(document).ready(function () {
  $("#confimationdelete").keyup(function () {
    $('.invalid-validation').remove();
  $(this).val($(this).val().toUpperCase());
  });
  });

  function  OpenConfirmModal()
  {
    $('#ConfirmModal').modal('show');
  }


  function  HideConfirmModal()
  {
    $('#ConfirmModal').modal('hide');
  }

  function CloseMyAccount()
  {
    $('.invalid-validation').remove();
    var confimation=$('#confimationdelete').val();
    if(confimation=="DELETE")
    {
              $.ajax({
                          url: "{{route('my-account.close-my-account')}}",
                          dataType: 'json',
                          type: 'POST',
                          headers: {
                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                          beforeSend: function() {
                            jQuery(document).find('.loadingOverlay').show();
                            
                            jQuery(document).find('.loader').show();
                        },
                        success: function(response) {

                          if(response.status=='success')
                          {
                            
                            location.reload();
                           
                          }
                        },
                        error: function(xhr) { // if error occured
                            alert("Error occured.please try again");
                        },
                        complete: function() {

                            $('.select2').select2();
                            jQuery(document).find('.loadingOverlay').hide();
                             jQuery(document).find('.loader').hide();
                            

                        },
                      });
                       
      }
      else
      {
        $('#confimationdelete').after('<span class="text-danger text-xs invalid-validation" > Please type proper keyword: "DELETE" </span>');
      }

  }

  $('.scrolltodiv').click(function (e) {
    var scrollto=$(this).data('scroll');
    $('html, body').animate({
      scrollTop: $(scrollto).offset().top
    }, 2000);
  });

  $(function() {
    var target = $('#myaccount-billing');
    if (target.length) {


      $('html,body').animate({
        scrollTop: target.offset().top
      }, 1000);
      return false;
    }
  });
</script>
@endsection