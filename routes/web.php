<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\UnderMantainenceController;
use App\Http\Controllers\Auth\GoogleLoginController;
use App\Http\Controllers\Auth\FacebookLoginController;
use App\Http\Controllers\SubscribePackagesController;
use App\Http\Controllers\MyMessagesController;
use App\Http\Controllers\WebhookController;
use App\Http\Controllers\ZiggeoWebhookController;
use App\Http\Controllers\CouponsController;
use App\Http\Controllers\MyRecipientsController;
use App\Http\Controllers\MyPreferenceController;
use App\Http\Controllers\MyAccountController;
use App\Http\Controllers\CreateContentController;
use App\Http\Controllers\GetSupportController;
use App\Http\Controllers\MyContentController;
use App\Http\Controllers\MyProfileController;
use App\Http\Controllers\MemorialPageController;
use App\Http\Controllers\MemorialPageExampleController;
use App\Http\Controllers\MyAccountControllerNew;
use App\Http\Controllers\PricingPackagesController;
use App\Http\Controllers\PromotionalCodesController;
use App\Http\Controllers\OrganisationControllers\OurPartnersController;
use App\Http\Controllers\OrganisationControllers\OurAccountPartnerController;
use App\Http\Controllers\OrganisationControllers\OurProfileController;
use App\Http\Controllers\MembersController\OurMembersController;
use App\Http\Controllers\MembersController\MyAccountMemberController;
use App\Http\Controllers\MembersController\MyClientsController;
use App\Http\Controllers\MembersController\MyProfileController as MembersProfileController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\OurB2CUserClientsController;
use App\Http\Controllers\OurB2BClientsController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['page.session.control', 'cors']], function ($CommonUtils) {
  Auth::routes();
  Route::get('/', [HomeController::class, 'index'])->name('home');
  Route::get('/home', [HomeController::class, 'index'])->name('home');
  Route::get('/terms-and-conditions', [HomeController::class, 'TermsAndCondtion'])->name('terms-and-conditions');
  Route::get('/privacy-policy', [HomeController::class, 'PrivacyPolicy'])->name('privacy-policy');
 Route::get('/user-checkin-status/confirmation/{id}', [HomeController::class, 'MailConfirmation'])->name('user-checkin-status-confirmaion');
  
  Route::get('/messages', [UnderMantainenceController::class, 'index'])->name('messages');
  Route::get('/packages', [UnderMantainenceController::class, 'index'])->name('packages');
  Route::get('/about-us', [UnderMantainenceController::class, 'index'])->name('about-us');
  Route::get('/verification-process', [HomeController::class, 'VerificationProcess'])->name('verification-process');
  Route::get('/verification-inprocess', [HomeController::class, 'VerificationProcessStep'])->name('verification-process-step');
  Route::post('submit-for-verification', [HomeController::class, 'SubmitForVerification'])->name('submit-for-verification');
  Route::get('verify-registration-partner', [HomeController::class, 'VerifyRegisterationPartner'])->name('verify-registration-partner');
  Route::get('verify-registration-member', [HomeController::class, 'VerifyRegisterationMember'])->name('verify-registration-member');
  Route::post('submit-registeration-member-form', [HomeController::class, 'SubmitRegisterationMemberForm'])->name('submit-registeration-member-form');
  Route::post('submit-registeration-partner-form', [HomeController::class, 'SubmitRegisterationPartnerForm'])->name('submit-registeration-partner-form');

  Route::get('verify-registration-client', [HomeController::class, 'VerifyRegisterationClient'])->name('verify-registration-client');
   Route::post('submit-registeration-client-form', [HomeController::class, 'SubmitRegisterationClientForm'])->name('submit-registeration-client-form');

  Route::get('verify-account', [HomeController::class, 'VerifyRegisteration'])->name('verify-account');
  Route::post('submit-registeration-form', [HomeController::class, 'SubmitRegisterationForm'])->name('submit-registeration-form');


  Route::get('auth/redirect', [GoogleLoginController::class, 'redirect'])->name('auth.redirect');
  Route::get('auth/callback', [GoogleLoginController::class, 'callback'])->name('auth.callback');

  Route::get('facebook-login/redirect', [FacebookLoginController::class, 'redirect'])->name('facebook.auth.redirect');
  Route::get('facebook-login/callback', [FacebookLoginController::class, 'callback'])->name('facebook.auth.callback');

  Route::get('invitation-authorised-person-request', [HomeController::class, 'invitationauthorisedperson'])->name('invitation-authorised-person-request');

  Route::get('notify-user-status-verification/{code}/{otp?}', [MyPreferenceController::class, 'NotifyUserStatusVerification'])->name('notify-user-status-verification');

  Route::post('notify-user-status-verification-post/{code}/{otp?}', [MyPreferenceController::class, 'NotifyUserStatusVerificationPost'])->name('notify-user-status-verification-post');

  Route::post('user-report/update-status/{id}/{otp?}', [MyPreferenceController::class, 'UserUpdateStatus'])->name('user-report.update-status');
  Route::post('user-report/upload-files/{id}', [MyPreferenceController::class, 'UserUploadFiles'])->name('user-report.upload-files');
  Route::post('user-report/delete-files/{id}', [MyPreferenceController::class, 'UserDeleteFiles'])->name('user-report.delete-files');
  Route::post('user-report/get-uploaded-files/{id}', [MyPreferenceController::class, 'UserGetFiles'])->name('user-report.get-uploaded-files');
  Route::get('user-report/stream-files/{id}/{filename}', [MyPreferenceController::class, 'UserStreamFiles'])->name('user-report.stream-files');

});



Route::group(['middleware' => ['page.session.control','auth','cors']], function($CommonUtils)
{
  
  Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
  Route::get('/subscription-packages', [SubscribePackagesController::class, 'index'])->name('subscription-packages');
  Route::post('/purchase-subscription/{id}', [SubscribePackagesController::class, 'GetPurchaseSubscription'])->name('purchase-subscription');
  Route::post('/dashboard/purchase-subscription/{id}', [SubscribePackagesController::class, 'DashboardPurchaseSubscription'])->name('dashboard.purchase-subscription');

  Route::get('/subscription-purchase-success', [SubscribePackagesController::class, 'StripeSuccessPage'])->name('stripe-success-page');
  Route::get('/subscription-purchase-cancel', [SubscribePackagesController::class, 'StripeCancelPage'])->name('stripe-cancel-page');
  Route::get('/memorial-page', [MemorialPageController::class, 'MemorialPage'])->name('memorial-page');
  Route::get('/remembering/anna-downey-example', [MemorialPageExampleController::class, 'MemorialPageExample'])->name('memorial-page.example');


  Route::prefix('roles')->group(function () {
    Route::get('/', [RoleController::class, 'index'])->name('list-roles');
    Route::get('/create', [RoleController::class, 'create'])->name('add-roles');
    Route::get('/edit/{id}', [RoleController::class, 'edit'])->name('edit-roles');
    Route::post('/store', [RoleController::class, 'store'])->name('add-roles-post');
    Route::post('/update', [RoleController::class, 'update'])->name('edit-roles-post');
  });

  Route::group(array('prefix' => 'permissions'), function () {
    Route::get('/', [PermissionController::class, 'index'])->name('list-permissions');
    Route::get('/add-permissions', [PermissionController::class, 'create'])->name('add-permissions');
    Route::post('/add-permissions-post', [PermissionController::class, 'store'])->name('add-permissions-post');
    Route::get('/edit-permissions/{id}', [PermissionController::class, 'edit'])->name('edit-permissions');
    Route::post('/edit-permissions-post', [PermissionController::class, 'editPost'])->name('edit-permissions-post');
    Route::get('/view-permissions/{id}', [PermissionController::class, 'view'])->name('view-permissions');
  });

  Route::group(array('prefix' => 'my-preferences'), function () {
    Route::get('/', [MyPreferenceController::class, 'index'])->name('my-preferences.index');

    Route::get('/my-checkin-process', [MyPreferenceController::class, 'CheckInProcess'])->name('my-checkin-process');

    Route::get('/my-authorised-3p-access', [MyPreferenceController::class, 'Authorised3pProcess'])->name('my-authorised-3p-access');

    Route::post('/save-checkin-process', [MyPreferenceController::class, 'SaveChekinProcess'])->name('save-checkin-process');

    Route::post('/save-authorised-3p-access', [MyPreferenceController::class, 'SaveAuthorisedAccess'])->name('save-authorised-3p-access');
    Route::post('/update-authorised-3p-access', [MyPreferenceController::class, 'UpdateAuthorisedAccess'])->name('update-authorised-3p-access');
  });

  Route::group(array('prefix' => 'manage-recipients'), function () {

    Route::get('/', [MyRecipientsController::class, 'index'])->name('manage-recipients.index');
    Route::get('/create', [MyRecipientsController::class, 'create'])->name('manage-recipients.create');
    Route::get('/edit/{id}', [MyRecipientsController::class, 'edit'])->name('manage-recipients.edit');
    Route::post('/store', [MyRecipientsController::class, 'store'])->name('manage-recipients.store');
    Route::post('/update/{id}', [MyRecipientsController::class, 'update'])->name('manage-recipients.update');
  });

  Route::get('/create-content',[CreateContentController::class, 'CreateContent'])->name('create-content');
   Route::group(array('prefix' => 'my-content'), function () {

    Route::get('/', [MyContentController::class, 'MyContent'])->name('my-content');
    Route::get('/my-messages-filter', [MyMessagesController::class, 'index'])->name('my-messages.index');

    Route::get('/create-content-filter', [MyMessagesController::class, 'ChooseMessageType'])->name('my-messages.create');

    Route::get('/message-delete/{id}/{type}', [MyMessagesController::class, 'DeleteActionMessageDetails'])->name('my-messages.delete');

    Route::get('/video-create', [MyMessagesController::class, 'Videocreate'])->name('my-messages.video-create');
    Route::get('/video-upload', [MyMessagesController::class, 'Videoupload'])->name('my-messages.video-upload');
    Route::get('/video-update/{id}', [MyMessagesController::class, 'VideoUpdate'])->name('my-messages.video-update');
    Route::get('/video-verify', [MyMessagesController::class, 'VerifyVideodetails'])->name('my-messages.video-verify');
    Route::post('/process-created-video', [MyMessagesController::class, 'ProcessCreatedVideo'])->name('my-messages.process-created-video');


    Route::get('/audio-create', [MyMessagesController::class, 'Audiocreate'])->name('my-messages.audio-create');
    Route::get('/audio-upload', [MyMessagesController::class, 'Audioupload'])->name('my-messages.audio-upload');
    Route::get('/audio-update/{id}', [MyMessagesController::class, 'AudioUpdate'])->name('my-messages.audio-update');
    Route::get('/audio-verify', [MyMessagesController::class, 'VerifyAudiodetails'])->name('my-messages.audio-verify');
    Route::post('/process-created-audio', [MyMessagesController::class, 'ProcessCreatedAudio'])->name('my-messages.process-created-audio');


    Route::get('/text-create', [MyMessagesController::class, 'TextCreate'])->name('my-messages.text-create');
    Route::get('/text-stream.{id}', [MyMessagesController::class, 'TextStream'])->name('my-messages.text-stream');
    Route::get('/text-upload', [MyMessagesController::class, 'TextUpload'])->name('my-messages.text-upload');
    Route::get('/text-show/{id}', [MyMessagesController::class, 'TextShow'])->name('my-messages.text-show');
    Route::get('/text-update/{id}', [MyMessagesController::class, 'TextUpdate'])->name('my-messages.text-update');
    Route::post('/text-verify', [MyMessagesController::class, 'VerifyTextdetails'])->name('my-messages.text-verify');
    Route::post('/upload-files', [MyMessagesController::class, 'UploadFiles'])->name('my-messages.upload-files');
    Route::get('/download-files/{filename}', [MyMessagesController::class, 'DownloadFiles'])->name('my-messages.download-files');
    Route::get('/get-uploaded-files/{id}', [MyMessagesController::class, 'GetUploadedFiles'])->name('my-messages.get-uploaded-files');
    Route::post('/delete-uploaded-files', [MyMessagesController::class, 'DeleteUploadedFiles'])->name('my-messages.delete-uploaded-files');



    Route::get('/text-create', [MyMessagesController::class, 'TextCreate'])->name('my-messages.text-create');

    Route::get('/show-message-details', [MyMessagesController::class, 'ShowMessageDetails'])->name('my-messages.show-message-details');

    Route::post('/show-message-qr', [MyMessagesController::class, 'ShowMessageQRDetails'])->name('my-messages.show-message-qr');

    Route::post('/download-message-qr', [MyMessagesController::class, 'DownloadMessageQRDetails'])->name('my-messages.download-message-qr');

    Route::post('/manage-action-message-details', [MyMessagesController::class, 'ManageActionMessageDetails'])->name('my-messages.manage-action-message-details');

    Route::post('/take-action-message-details', [MyMessagesController::class, 'TakeActionMessageDetails'])->name('my-messages.take-action-message-details');
  });


  Route::group(array('prefix' => 'manage-coupons'), function () {

    Route::get('/', [CouponsController::class, 'index'])->name('manage-coupons.index');
    Route::get('/create', [CouponsController::class, 'create'])->name('manage-coupons.create');
   
    Route::post('/store', [CouponsController::class, 'store'])->name('manage-coupons.store');
    
  });

  Route::group(array('prefix' => 'shared-coupons'), function () {

    Route::get('/partners-details', [CouponsController::class, 'ShowPartnerCouponsDetails'])->name('shared-coupons-partners.details');
    Route::get('/members-details', [CouponsController::class, 'ShowMemberCouponsDetails'])->name('shared-coupons-members.details');
    Route::get('/clients-details', [CouponsController::class, 'ShowClientCouponsDetails'])->name('shared-coupons-clients.details');
    Route::get('/b2b-clients-details', [CouponsController::class, 'ShowPartnerCouponsDetails'])->name('shared-coupons-b2b-clients.details');
    Route::post('/partners-store', [OurPartnersController::class, 'ShareCouponsPartnersPost'])->name('shared-coupons-partners.store');
    Route::post('/b2b-clients-store', [OurB2BClientsController::class, 'ShareCouponsB2BClientPost'])->name('shared-coupons-b2b-clients.store');
     Route::post('/members-store', [OurMembersController::class, 'ShareCouponsMembersPost'])->name('shared-coupons-members.store');
      Route::post('/clients-store', [MyClientsController::class, 'ShareCouponsClientsPost'])->name('shared-coupons-clients.store');
    
    
  });

  Route::group(array('prefix' => 'our-partners'), function () {

    Route::get('/', [OurPartnersController::class, 'index'])->name('our-partners.index');
    Route::get('/create/{id}', [OurPartnersController::class, 'create'])->name('our-partners.create');
   
    Route::post('/store/{id}', [OurPartnersController::class, 'store'])->name('our-partners.store');
    Route::get('/details/{id}', [OurPartnersController::class, 'details'])->name('our-partners.details');
    Route::get('/details/user-account-details/{id}', [OurPartnersController::class, 'PartnerAccountDetails'])->name('our-partners.details.account-details');
  });

  Route::group(array('prefix' => 'b2b-clients'), function () {

    Route::get('/', [OurB2BClientsController::class, 'index'])->name('b2b-clients.index');
    Route::get('/create/{id}', [OurB2BClientsController::class, 'create'])->name('b2b-clients.create');
   
    Route::post('/store/{id}', [OurB2BClientsController::class, 'store'])->name('b2b-clients.store');
    Route::get('/details/{id}', [OurB2BClientsController::class, 'details'])->name('b2b-clients.details');
    Route::get('/details/user-account-details/{id}', [OurB2BClientsController::class, 'B2BClientsAccountDetails'])->name('b2b-clients.details.account-details');
  });

  Route::group(array('prefix' => 'b2c-users'), function () {

    Route::get('/', [OurB2CUserClientsController::class, 'index'])->name('b2c-users-clients.index');

    Route::get('/details/{id}', [OurB2CUserClientsController::class, 'details'])->name('b2c-users-clients.details');

    Route::get('/details/user-account-details/{id}', [OurB2CUserClientsController::class, 'UserAccountDetails'])->name('b2c-users-clients.details.account-details');

    Route::get('/details/user-preferences/{id}', [OurB2CUserClientsController::class, 'UserPreferences'])->name('b2c-users-clients.details.preferences');

    Route::get('/details/user-death-details/{id}', [OurB2CUserClientsController::class, 'UserDeathDetails'])->name('b2c-users-clients.details.death-details');

    Route::get('/details/user-recipients/{id}', [OurB2CUserClientsController::class, 'UserRecipients'])->name('b2c-users-clients.details.recipients');

    Route::get('/details/user-authorised-parties/{id}', [OurB2CUserClientsController::class, 'UserAuthorisedParties'])->name('b2c-users-clients.details.authorised-parties');

    Route::get('/details/user-statistics/{id}', [OurB2CUserClientsController::class, 'UserStatistics'])->name('b2c-users-clients.details.statistics');
    
    Route::post('/details/update-status/{id}', [OurB2CUserClientsController::class, 'UserUpdateStatus'])->name('b2c-users-clients.details.update-status');

  Route::post('/details/upload-files/{id}', [OurB2CUserClientsController::class, 'UserUploadFiles'])->name('b2c-users-clients.details.upload-files');

  Route::post('/details/update-status/{id}', [OurB2CUserClientsController::class, 'UserUpdateStatus'])->name('b2c-users-clients.details.update-status');

  Route::post('/details/delete-files/{id}', [OurB2CUserClientsController::class, 'UserDeleteFiles'])->name('b2c-users-clients.details.delete-files');

  Route::post('/details/get-uploaded-files/{id}', [OurB2CUserClientsController::class, 'UserGetFiles'])->name('b2c-users-clients.details.get-uploaded-files');

  Route::get('details/stream-files/{id}/{filename}', [OurB2CUserClientsController::class, 'UserStreamFiles'])->name('b2c-users-clients.details.stream-files');
    
  });

  Route::group(array('prefix' => 'manage-promotional-codes'), function () {

    Route::get('/{id}', [PromotionalCodesController::class, 'index'])->name('manage-promotional-codes.index');
    Route::get('/create/{id}', [PromotionalCodesController::class, 'create'])->name('manage-promotional-codes.create');
   
    Route::post('/store/{id}', [PromotionalCodesController::class, 'store'])->name('manage-promotional-codes.store');
    
  });

  Route::group(array('prefix' => 'my-account'), function () {

    Route::get('/', [MyAccountController::class, 'index'])->name('my-account');
    Route::post('/upgrade-account/{id}', [MyAccountController::class, 'UpgradeAccount'])->name('my-account.upgrade');
    Route::get('/my-billing', [MyAccountController::class, 'mybilling'])->name('my-account.my-billing');
    Route::post('/close-my-account', [MyAccountController::class, 'CloseMyAccount'])->name('my-account.close-my-account');
    Route::get('/add-on-features', [MyAccountController::class, 'addonfeatures'])->name('my-account.add-on-features');
    Route::get('/free-up-minutes', [MyAccountController::class, 'freeupminutes'])->name('my-account.free-up-minutes');
    Route::get('/upgrade-plan', [MyAccountController::class, 'upgradeplan'])->name('my-account.upgrade-plan');
    Route::get('/back-to-my-account', [MyAccountController::class, 'BacktoMyAccount'])->name('my-account.back-to-my-account');
    Route::get('/redeem-code', [MyAccountController::class, 'RedeemCode'])->name('my-account.redeem-code');
    Route::post('/redeem-code-submit', [MyAccountController::class, 'RedeemCodeSubmit'])->name('my-account.redeem-code-submit');
    
  });

  //Partner Dashboard

  Route::group(array('prefix' => 'organisation-account'), function () {

    Route::get('/coupon-codes', [OurAccountPartnerController::class, 'OurOrganisationalCodes'])->name('partner.coupon-codes');
    Route::get('/buy-packages', [OurAccountPartnerController::class, 'BuyPackages'])->name('partner.buy-packages');
    Route::get('/pre-purchase-packages', [OurAccountPartnerController::class, 'PrePurchasePackages'])->name('partner.pre-purchase-packages');
    Route::get('/billings', [OurAccountPartnerController::class, 'OurBilling'])->name('partner.billings');
    Route::get('/', [OurAccountPartnerController::class, 'index'])->name('partner.our-account');

    Route::get('/shared-packages-details', [OurAccountPartnerController::class, 'ShowPackageDetails'])->name('partner.shared-packages-details');
    Route::post('/shared-packages-details-store', [OurAccountPartnerController::class, 'ShowPackageDetailsPost'])->name('partner.shared-packages-details.store');


   
    
  });

  Route::get('organisation-members', [OurMembersController::class, 'index'])->name('partner.our-members');


  Route::group(array('prefix' => 'organisation-profile'), function () {

    Route::get('/', [OurProfileController::class,'index'])->name('partner.our-profile');
    Route::get('/change-password', [OurProfileController::class, 'ChangePassword'])->name('partner.change-password');
    Route::get('/our-organisational-details', [OurProfileController::class, 'OurOrganisationDetails'])->name('partner.our-organisational-details');
    Route::post('/update',[OurProfileController::class, 'UpdateProfile'])->name('partner.our-organisational-details.update');
     Route::post('/update-password',[OurProfileController::class, 'UpdatePassword'])->name('partner.update-password');

  });




  //Partner Dashboard End



  //Member Dashboard

  Route::group(array('prefix' => 'member-account'), function () {

    Route::get('/coupon-codes', [MyAccountMemberController::class, 'OurOrganisationalCodes'])->name('member.coupon-codes');
     Route::get('/request-packages', [MyAccountMemberController::class, 'RequestPackages'])->name('member.request-packages');
     Route::post('/request-packages-post/{id}', [MyAccountMemberController::class, 'RequestPackagesPost'])->name('member.request-packages-post');
    Route::get('/pre-purchase-packages', [MyAccountMemberController::class, 'PrePurchasePackages'])->name('member.pre-purchase-packages');
    Route::get('/billings', [MyAccountMemberController::class, 'OurBilling'])->name('member.billings');
    Route::get('/', [MyAccountMemberController::class, 'index'])->name('member.our-account');

    Route::get('/shared-packages-details', [MyAccountMemberController::class, 'ShowPackageDetails'])->name('member.shared-packages-details');
    Route::post('/shared-packages-details-store', [MyAccountMemberController::class, 'ShowPackageDetailsPost'])->name('member.shared-packages-details.store');


   
    
  });

  Route::get('member-clients', [MyClientsController::class, 'index'])->name('member.our-clients');
  Route::get('member-clients/statistics', [MyClientsController::class, 'MyClientStatistics'])->name('member-clients.statistics');
  Route::post('member-clients/update-status/{id}', [MyClientsController::class, 'MyClientUpdateStatus'])->name('member-clients.update-status');
  Route::post('member-clients/upload-files/{id}', [MyClientsController::class, 'MyClientUploadFiles'])->name('member-clients.upload-files');
  Route::post('member-clients/update-status/{id}', [MyClientsController::class, 'MyClientUpdateStatus'])->name('member-clients.update-status');
  Route::post('member-clients/delete-files/{id}', [MyClientsController::class, 'MyClientDeleteFiles'])->name('member-clients.delete-files');
  Route::post('member-clients/get-uploaded-files/{id}', [MyClientsController::class, 'MyClientGetFiles'])->name('member-clients.get-uploaded-files');
  Route::get('member-clients/stream-files/{id}/{filename}', [MyClientsController::class, 'MyClientStreamFiles'])->name('member-clients.stream-files');


  Route::group(array('prefix' => 'member-profile'), function () {

    Route::get('/', [MembersProfileController::class,'index'])->name('member.our-profile');
    Route::get('/change-password', [MembersProfileController::class, 'ChangePassword'])->name('member.change-password');
    Route::get('/our-organisational-details', [MembersProfileController::class, 'OurOrganisationDetails'])->name('member.our-organisational-details');
    Route::post('/update',[MembersProfileController::class, 'UpdateProfile'])->name('member.our-organisational-details.update');
     Route::post('/update-password',[MembersProfileController::class, 'UpdatePassword'])->name('member.update-password');

  });




  //Member Dashboard End



 


  Route::group(array('prefix' => 'my-profile'), function()
  {
    Route::get('/',[MyProfileController::class, 'MyProfile'])->name('my-profile');
    Route::get('/my-basic-info',[MyProfileController::class, 'MyBasicInfo'])->name('my-profile.my-basic-info');
    Route::get('/my-change-password',[MyProfileController::class, 'MyChangePassword'])->name('my-profile.my-change-password');
     Route::post('/update',[MyProfileController::class, 'UpdateProfile'])->name('my-profile.update');
     Route::post('/update-password',[MyProfileController::class, 'UpdatePassword'])->name('my-profile.update-password');
  });



  Route::group(array('prefix' => 'delegate-access'), function () {
      Route::get('/', [MyAccountController::class,'DelegateAccess'])->name('my-account.delegate-access');
      Route::get('/{id}', [MyAccountController::class,'SwitchAccountAccess'])->name('my-account.switch-account-access');
  });





  Route::get('/my-profile', [MyProfileController::class, 'MyProfile'])->name('my-profile');
  Route::get('/my-account', [MyAccountController::class, 'index'])->name('my-account');
  Route::get('/new-pricing-packages', [PricingPackagesController::class, 'ViewPricing'])->name('new-pricing-packages');
  Route::get('/get-support', [GetSupportController::class, 'GetSupport'])->name('get-support');
  Route::post('/request-support',[GetSupportController::class, 'RequestSupport'])->name('request-support');
  
});

Route::get('/message-verification/{code}/{verificationtoken?}', [HomeController::class, 'MessageVerification'])->name('message-verification');
Route::post('/message-verification-post', [HomeController::class, 'MessageVerificationPost'])->name('message-verification-post');
Route::post('/message-confirmemail-post', [HomeController::class, 'MessageConfirmEmailPost'])->name('message-confirmemail-post');
Route::get('/batch-creation-email-stageone', [MyPreferenceController::class, 'BatchCreationEmailStageOne'])->name('batch-creation-email-stageone');
Route::get('/batch-creation-email-stagetwo', [MyPreferenceController::class, 'BatchCreationEmailStageTwo'])->name('batch-creation-email-stagetwo');
Route::get('/batch-despatch-email-stageone', [MyPreferenceController::class, 'DespatchEmailStageOne'])->name('batch-despatch-email-stageone');
Route::get('/batch-despatch-email-stagetwo', [MyPreferenceController::class, 'DespatchEmailStageTwo'])->name('batch-despatch-email-stagetwo');
Route::get('/batch-creation-email-stagethree', [MyPreferenceController::class, 'BatchCreationEmailStageThree'])->name('batch-creation-email-stagethree');
Route::get('/batch-creation-thirdperson', [MyPreferenceController::class, 'BatchCreationEmailThirdPerson'])->name('batch-creation-thirdperson');
Route::get('/process-email-stagethree', [MyPreferenceController::class, 'ProcessEmailStageThree'])->name('process-email-stagethree');
Route::get('/despatch-final-email-messages', [MyPreferenceController::class, 'DespatchFinalEmailMessages'])->name('despatch-final-email-messages');
Route::get('/maintenance-dashboard', [UnderMantainenceController::class, 'dashboard'])->name('maintenance-dashboard');


Route::group(['middleware' => ['cors']], function ($CommonUtils) {

  Route::post('webhook-fire/ziggeo/on-video-create', [ZiggeoWebhookController::class, 'OnVideoCreate'])->name('ziggeo.on-video-create');

  Route::post('webhook-fire/ziggeo/on-video-deleted', [ZiggeoWebhookController::class, 'OnVideoDeleted'])->name('ziggeo.on-video-deleted');

  Route::get('ziggeo/process-failed-created-video/{id}', [ZiggeoWebhookController::class, 'ProcessFailedCreatedVideoWebhook'])->name('ziggeo.process-failed-created-video');

  Route::get('ziggeo/process-failed-deleted-video/{id}', [ZiggeoWebhookController::class, 'ProcessFailedDeletedVideoWebhook'])->name('ziggeo.process-failed-deleted-video');

  Route::post('webhook-fire/ziggeo/on-audio-create', [ZiggeoWebhookController::class, 'OnAudioCreate'])->name('ziggeo.on-audio-create');

  Route::post('webhook-fire/ziggeo/on-audio-deleted', [ZiggeoWebhookController::class, 'OnAudioDeleted'])->name('ziggeo.on-audio-deleted');

  Route::get('ziggeo/process-failed-created-audio/{id}', [ZiggeoWebhookController::class, 'ProcessFailedCreatedAudioWebhook'])->name('ziggeo.process-failed-created-audio');

  Route::get('ziggeo/process-failed-deleted-audio/{id}', [ZiggeoWebhookController::class, 'ProcessFailedDeletedAudioWebhook'])->name('ziggeo.process-failed-deleted-audio');

  Route::post('stripe/subscription-changes', [WebhookController::class, 'handleWebhook'])->name('stripe.subscription-changes');
});


Route::group(array('prefix' => 'our-profile.index'), function () {
  Route::get('/', [OurProfileController::class, 'OurProfile'])->name('our-profile.index');
  Route::get('/change-password', [OurProfileController::class, 'ChangePassword'])->name('our-profile.change-password');
  Route::get('/our-organisational-details', [OurProfileController::class, 'OurOrganisationDetails'])->name('our-profile.our-organisational-details');
});




Route::get('/my-profile-member', [MyClientsController::class, 'ViewProfile'])->name('my-profile-member');
Route::get('/my-account-member', [MyClientsController::class, 'ViewAccount'])->name('my-account-member');
Route::get('/admin.b2c-user-clients', [AdminController::class, 'EndUsers'])->name('admin.b2c-user-clients');
Route::get('/admin.b2b-client', [AdminController::class, 'B2Bclients'])->name('admin.b2b-client');
Route::get('/admin.b2c-partner', [AdminController::class, 'Partners'])->name('admin.b2c-partner');
Route::get('/subscription-page-new', [SubscribePackagesController::class, 'newScreens'])->name('subscription-page-new');
