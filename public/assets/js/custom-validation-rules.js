$(function () {
    
    $('#ValidateForm').validate({
      rules:{

      mobile_number:
      {
        NotExistNo:true,
        minlength:10,
        maxlength:10,
      }


      },
      messages: {
        minlength:"Please enter valid Phone Number",
        maxlength:"Please enter valid Phone Number"
      }
    errorElement: 'span',

    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').find('.field-box').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    },
    onfocusout: function(element) {
      $(element).valid();
    }
  });


});
// Number Input rules ------------------------------------------ Start

$.validator.addMethod("Differenceval",
    function (value, element, param) {
          if(value>0){
           var param1=($(param[0]).val()) ? parseInt($(param[0]).val()) :0;
           var param2=($(param[1]).val()) ? parseInt($(param[1]).val()) :0;
       
          return parseInt(value, 10) <= (param1)-(param2);
          }
          return true;
     });

$.validator.addMethod("greaterThan",
    function (value, element, param) {
          if(value>0){
            var $otherElement =  ($(param).val()) ? parseInt($(param).val()) : 0;
            return parseInt(value, 10) > parseInt($otherElement, 10);
          }
          return true;
    });

$.validator.addMethod("lessThan",
    function (value, element, param) {
          if(value>0){
            var $otherElement =  ($(param).val()) ? parseInt($(param).val()) : 0;
            return parseInt(value, 10) < parseInt($otherElement, 10);
          }
          return true;
    });

$.validator.addMethod("CompareNotSame",
    function (value, element, param) {
          if(value>0){
            var $otherElement =  ($(param).val()) ? parseInt($(param).val()) : 0;
            return parseInt(value, 10) !== parseInt($otherElement, 10);
          }
          return true;
     });

// Number Input rules ------------------------------------------ END


// Date Input rules ------------------------------------------ Start

$.validator.addMethod("DateBeforeorEqualToday",
    function (value, element, param) {
    if(value)
    {  
      var date= value.split("-");
      var date = date[2].trim() + '-' + date[1].trim() + '-' + date[0].trim();
     
      var date = new Date(date);
      var currentdate = new Date();

      if(date>currentdate)
      {
        return false;
      }
      else
      {
          return true;
      }
    }

    });

$.validator.addMethod("Datelessthan",
    function (value, element, param) {
    if(value && $(param).val())
    {

        var $otherElement = $(param);
        var date= value.split("-");
        var date = date[2].trim() + '-' + date[1].trim() + '-' + date[0].trim();
        var otherdate= $otherElement.val().split("-");
        var otherdate = otherdate[2].trim() + '-' + otherdate[1].trim() + '-' + otherdate[0].trim();
        console.log(date);
        console.log(otherdate);
        var date = new Date(date);
        var otherdate = new Date(otherdate);
        

        if(date>=otherdate)
        {
          return false;
        }
        else
        {
            return true;
        }
    }

    });

  $.validator.addMethod("DateGreaterthan",
    function (value, element, param) {
    if(value && $(param).val())
    {

        var $otherElement = $(param);
        var date= value.split("-");
        var date = date[2].trim() + '-' + date[1].trim() + '-' + date[0].trim();
        var otherdate= $otherElement.val().split("-");
        var otherdate = otherdate[2].trim() + '-' + otherdate[1].trim() + '-' + otherdate[0].trim();
        console.log(date);
        console.log(otherdate);
        var date = new Date(date);
        var otherdate = new Date(otherdate);
        

        if(date<=otherdate)
        {
          return false;
        }
        else
        {
            return true;
        }
    }

    });


  $.validator.addMethod("DateGreaterthanEqualto",
    function (value, element, param) {
    if(value && $(param).val())
    {

        var $otherElement = $(param);
        var date= value.split("-");
        var date = date[2].trim() + '-' + date[1].trim() + '-' + date[0].trim();
        var otherdate= $otherElement.val().split("-");
        var otherdate = otherdate[2].trim() + '-' + otherdate[1].trim() + '-' + otherdate[0].trim();
        var date = new Date(date);
        var otherdate = new Date(otherdate);
        

        if(date<otherdate)
        {
          return false;
        }
        else
        {
            return true;
        }
    }

    });

  $.validator.addMethod("DatelessthanEqualto",
    function (value, element, param) {
    if(value && $(param).val())
    {

        var $otherElement = $(param);
        var date= value.split("-");
        var date = date[2].trim() + '-' + date[1].trim() + '-' + date[0].trim();
        var otherdate= $otherElement.val().split("-");
        var otherdate = otherdate[2].trim() + '-' + otherdate[1].trim() + '-' + otherdate[0].trim();
        var date = new Date(date);
        var otherdate = new Date(otherdate);
        

        if(date>otherdate)
        {
          return false;
        }
        else
        {
            return true;
        }
    }

    });


  $.validator.addMethod("Datelessthanmaxdate",
    function (value, element, param) {
    if(value && $(param).val())
    {

        var $otherElement = $(param);
        var date= value.split("-");
        var date = date[2].trim() + '-' + date[1].trim() + '-' + date[0].trim();
        var otherdate= $otherElement.val().split("-");
        var otherdate = otherdate[2].trim() + '-' + otherdate[1].trim() + '-' + otherdate[0].trim();
        var date = new Date(date);
        var otherdate = new Date(otherdate);
        

        if(date>=otherdate)
        {
          return false;
        }
        else
        {
            return true;
        }
    }

    });


  $.validator.addMethod("ValidatePhoneNoExist",
   function (value, element, param){
    let result = false;
    if(value.length==10)
    {
      data = { "phoneno" : value , "id" : $('#beneficiary-id-hidden').val()};
      $.ajax(
      {
        type: "POST",
        url: BASEURL+'/admin/check-phoneno-registered',
        dataType: "json",
        data: data,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
          if (data.status === 'fail') {
            
            result = false;

          } else {
           
            result = true;
          }
        },
        async: false
      });
    }
    return result;
  });

// Date Input rules ------------------------------------------ END

