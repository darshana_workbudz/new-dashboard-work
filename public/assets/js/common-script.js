$(document).on('click','.link-handle', function(e) {
e.preventDefault();
var obj =$(this);
var route=$(this).data('route');
route=removeParam('ajax',route);
route=removeParam('_token',route);

window.history.pushState('page',"After I Go",route);

var refreshdiv=$('body').find('.refresh-div');

if (route.indexOf('?')== -1) {
    route = route + '?ajax=true';
}
else {
    route = route + '&ajax=true';
}

jQuery.ajax({
  	url: route,
  	method: 'GET',
  	dataType: "json",
  	beforeSend: function() {
        AjaxbeforeSend();
    },
	success: function(response) {

  		if(response.event && response.event=='redirect')
  		{
  			Redirect(response.routetoredirect);
  		}

        else if(response.event=='refresh')
        {
        	refreshdiv.html(response.html);
     		
        }
    },
    error: function(xhr) { // if error occured
        alert("Error occured.please try again");
    },
    complete: function() {
        Ajaxcomplete();
    },
});
});

$(document).on('click','.submit-button', function(e) {
	e.preventDefault();
	var obj =$(this);
	form = obj.closest('form');
    var route = form.attr('action');
    var data = form.serialize();
    var refreshdiv=$('body').find('.refresh-div');

    $.ajax({
        url: route + '?ajax=true',
        dataType: 'json',
        type: 'POST',
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function() {
	        AjaxbeforeSend();
	    },
	  	success: function(response) {

	  		if(response.event && response.event=='redirect')
	  		{
	  			Redirect(response.routetoredirect);
	  		}

	        else if(response.event=='refresh')
	        {
	        	refreshdiv.html(response.html);

     		}
	    },
	    error: function(xhr) { // if error occured
	        alert("Error occured.please try again");
	    },
	    complete: function() {
	        Ajaxcomplete();
	    },
});
});



function Redirect(route)
{
		window.location.replace(route);
		return true;
		var refreshdiv=$('body').find('.refresh-div');
		window.history.pushState('page',"After I Go",route);
		jQuery.ajax({
		  	url: route+"?ajax=true",
		  	method: 'GET',
		  	beforeSend: function() {
		        AjaxbeforeSend();
		    },
		  	success: function(response) {

		  		if(response.event=='refresh')
		        {
		        	refreshdiv.html(response.html);
		     		
		        }
		    },
		    error: function(xhr) { // if error occured
		        alert("Error occured.please try again");
		    },
		    complete: function() {
		        Ajaxcomplete();
		    },
		});

}

function Ajaxcomplete()
{
	window.scrollTo({ top: 0, behavior: 'smooth' });
	setTimeout(function() {
	jQuery(document).find('.loadingOverlay').hide();
	jQuery(document).find('.page-progress-bar').hide();
	
	jQuery(document).find('.loader').hide();
	}, 1000);

} 

function AjaxbeforeSend()
{
	jQuery(document).find('.loadingOverlay').show();
	jQuery(document).find('.page-progress-bar').show();
	jQuery(document).find('.loader').show();
}  
function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

jQuery(document).ready(function($) {

	

	if (window.history && window.history.pushState) {

    $(window).on('popstate', function () {
        var route = PREVIOUSPAGESESSIONURL;
        window.history.pushState('page',"After I Go",route);
		var refreshdiv=$('body').find('.refresh-div');
		jQuery.ajax({
		  	url: route+"?ajax=true",
		  	method: 'GET',
		  	beforeSend: function() {
		        AjaxbeforeSend();
		    },
		  	success: function(response) {

		  		if(response.event && response.event=='redirect')
		  		{
		  			Redirect(response.routetoredirect);
		  		}

		        else if(response.event=='refresh')
		        {
		        	refreshdiv.html(response.html);
		     		
		        }
		    },
		    error: function(xhr) { // if error occured
		        alert("Error occured.please try again");
		    },
		    complete: function() {
		        Ajaxcomplete();
		    },
		});
	});
  }
});

$(document).on('keypress','.form-control', function(e) {
  if (e.which == 13) {
    form =$(this).closest('form');
    form.find('.submit-button').trigger('click');
    return false;    //<---- Add this line
  }
});

$(document).on('click','.switch-registration-type', function(e) {
	$('.switch-registration-type').removeClass('active');
 	$(this).addClass('active');
 	var showform=$(this).attr('showform');

 	if(showform=="individual-form")
 	{
 		$('.organisation-form').hide();
 		$('.individual-form').show();
 	}

 	if(showform=="organisation-form")
 	{
 		$('.individual-form').hide();
 		$('.organisation-form').show();
 	}


});



$(document).on('keydown','input', function(e) {
   $(this).removeClass('is-invalid');
   $(this).next().remove("p");
});







         
    

   
