 Dropzone.options.dropzone =
         {
	    maxFiles: 4, 
            maxFilesize: 5,
            //~ renameFile: function(file) {
                //~ var dt = new Date();
                //~ var time = dt.getTime();
               //~ return time+"-"+file.name;    // to rename file name but i didn't use it. i renamed file with php in controller.
            //~ },
            acceptedFiles: ".pdf,.jpeg,.png,.jpg",
            addRemoveLinks: true,
            timeout: 50000,
            init:function() {

				
			},
            removedfile: function(file) 
            {
				if (this.options.dictRemoveFile) {
				  
				  	swal({
			            text: "Are You Sure You want To Remove Files",
			            icon: "warning",
			            buttons: {
			                confirm : {text:'Yes',className:' text-white bg-gradient-primary'},
			                cancel : "No"
			            },
			        }).then((will)=>{
					if(file.previewElement.id != ""){
						var name = file.previewElement.id;
					}else{
						var name = file.name;
					}
					//console.log(name);
					$.ajax({
						headers: {
							  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							  },
						type: 'POST',
						url: delete_url,
						data: {filename: name},
						success: function (data){
							
						},
						error: function(e) {
							console.log(e);
						}});
						var fileRef;
						return (fileRef = file.previewElement) != null ? 
						fileRef.parentNode.removeChild(file.previewElement) : void 0;
				   });
			    }		
            },
       
            success: function(file, response) 
            {
				//console.log(file); 
				// set new images names in dropzone�s preview box.
                if(response.status=='success')   
                {
                	file.previewElement.querySelector("img").alt = response.data.uploadedfilename;
					var obj;
					LoadURL(obj,routetoredirect+'&token='+response.data.text_token_id,response.data.ziggeo_user_text_id);
					$('#success-message').show();
                }

                else
                {

                	 $('.dz-preview').remove();
                	 $('.dropzone.dz-started .dz-message').show();

                	swal({
					  title: "Oops!",
					  text: response.message,
					  icon: "warning",
					  button: "Ok",
					});
                }






				
            },
            error: function(file, response)
            {
               if($.type(response) === "string")
					var message = response; //dropzone sends it's own error messages in string
				else
					var message = response.message;
				file.previewElement.classList.add("dz-error");
				_ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
				_results = [];
				for (_i = 0, _len = _ref.length; _i < _len; _i++) {
					node = _ref[_i];
					_results.push(node.textContent = message);
				}
				return _results;
            }
            
};
