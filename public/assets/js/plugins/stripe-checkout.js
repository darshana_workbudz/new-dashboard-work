$(document).on('click','.purchase-subscription', function(e) {
  e.preventDefault();
  var obj =$(this);
  form = obj.closest('form');
    var route = form.attr('action');
    var data = $('form').serialize();
    
    $.ajax({
        url: route,
        dataType: 'json',
        type: 'POST',
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function() {
          AjaxbeforeSend();
      },
      success: function(response) {
         if(response.status=='success')
         {
           var stripe = Stripe(response.pkkey);
           var session = response.checkout_session;
           stripe.redirectToCheckout({ sessionId: response.sessionId }).then(function(result) {
                
                if (result.error) {
                  swal(result.error.message);
                }

              })
              .catch(function(error) {
                swal(error);
              }); 
          } 
      },
      error: function(xhr) { // if error occured
          alert("Error occured.please try again");
      },
      complete: function() {
          Ajaxcomplete();
      },
});
});