$(document).on('click','.link-handle', function(e) {
e.preventDefault();
var obj =$(this);
var route=$(this).data('route');
window.history.pushState('page',"After I Go",route);
var refreshdiv=$('body').find('.refresh-div');
jQuery.ajax({
  	url: route+"?ajax=true",
  	method: 'GET',
  	beforeSend: function() {
        AjaxbeforeSend();
    },
  	success: function(response) {

  		if(response.event && response.event=='redirect')
  		{
  			Redirect(response.routetoredirect);
  		}

        else if(response.event=='refresh')
        {
        	refreshdiv.html(response.html);
     		
        }
    },
    error: function(xhr) { // if error occured
        alert("Error occured.please try again");
    },
    complete: function() {
        Ajaxcomplete();
    },
});
});

$(document).on('click','.submit-button', function(e) {
	e.preventDefault();
	var obj =$(this);
	form = obj.closest('form');
    var route = form.attr('action');
    var data = $('form').serialize();
    var refreshdiv=$('body').find('.refresh-div');
    $.ajax({
        url: route + '?ajax=true',
        dataType: 'json',
        type: 'POST',
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        beforeSend: function() {
	        AjaxbeforeSend();
	    },
	  	success: function(response) {

	  		if(response.event && response.event=='redirect')
	  		{
	  			Redirect(response.routetoredirect);
	  		}

	        else if(response.event=='refresh')
	        {
	        	refreshdiv.html(response.html);
     			
	        }
	    },
	    error: function(xhr) { // if error occured
	        alert("Error occured.please try again");
	    },
	    complete: function() {
	        Ajaxcomplete();
	    },
});
});

$(document).on('change','#isorganisation', function(e) {

	if($(this).is(':checked'))
	{
		$('.orgdiv').show();
	}
	else
	{
		$('.orgdiv').hide();
	}

});

function Redirect(route)
{
		window.location.replace(route);
		return true;
		var refreshdiv=$('body').find('.refresh-div');
		window.history.pushState('page',"After I Go",route);
		jQuery.ajax({
		  	url: route+"?ajax=true",
		  	method: 'GET',
		  	beforeSend: function() {
		        AjaxbeforeSend();
		    },
		  	success: function(response) {

		  		if(response.event=='refresh')
		        {
		        	refreshdiv.html(response.html);
		     		
		        }
		    },
		    error: function(xhr) { // if error occured
		        alert("Error occured.please try again");
		    },
		    complete: function() {
		        Ajaxcomplete();
		    },
		});

}

function Ajaxcomplete()
{
	window.scrollTo({ top: 0, behavior: 'smooth' });
	setTimeout(function() {
	jQuery(document).find('.loadingOverlay').hide();
	jQuery(document).find('.page-progress-bar').hide();
	
	jQuery(document).find('.loader').hide();
	}, 1000);

} 

function AjaxbeforeSend()
{
	jQuery(document).find('.loadingOverlay').show();
	jQuery(document).find('.page-progress-bar').show();
	jQuery(document).find('.loader').show();
}  

jQuery(document).ready(function($) {

	

	if (window.history && window.history.pushState) {

    $(window).on('popstate', function () {
        var route = PREVIOUSPAGESESSIONURL;
        window.history.pushState('page',"After I Go",route);
		var refreshdiv=$('body').find('.refresh-div');
		jQuery.ajax({
		  	url: route+"?ajax=true",
		  	method: 'GET',
		  	beforeSend: function() {
		        AjaxbeforeSend();
		    },
		  	success: function(response) {

		  		if(response.event && response.event=='redirect')
		  		{
		  			Redirect(response.routetoredirect);
		  		}

		        else if(response.event=='refresh')
		        {
		        	refreshdiv.html(response.html);
		     		
		        }
		    },
		    error: function(xhr) { // if error occured
		        alert("Error occured.please try again");
		    },
		    complete: function() {
		        Ajaxcomplete();
		    },
		});
	});
  }
});

$(document).on('keypress','.form-control', function(e) {
  if (e.which == 13) {
    form =$(this).closest('form');
    form.find('.submit-button').trigger('click');
    return false;    //<---- Add this line
  }
});