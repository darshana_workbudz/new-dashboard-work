$(function() {
$(document).on('click','.show-manual', function(e) {
  e.preventDefault();
  $(document).find('.show-manual-div').show();
  $(document).find('#manual-address').val(1);
});

$('.require_verification').change(function() {
  $('#pwd2').val('');
  var require_verification= $('input[name="require_verification"]:checked').val();

  if(require_verification=='2')
  {

    $('#showverificationstepprocess').hide();
    $('#showverificationstep1').hide();
    $('#showverificationstep2').hide();
    $("#verification_process_step").prop('checked',false);


    clear_form_elements('#showverificationstepprocess');
    clear_form_elements('#showverificationstep1');
    clear_form_elements('#showverificationstep2'); 

    $('#checboxacknowlegdediv').show();
    
    swal({
      text: "I confirm that I have provided the accurate details for this recipient and I accept full responsibility if my messages are not sent to this recipient and/or are sent to someone else, because I have not provided this recipient’s correct and current contact details",
      icon: "warning",
      buttons: {
        confirm : {text:'Acknowledge',className:' text-white bg-primary'},
        cancel : "Take me back"
      },
    }).then((will)=>{

      if(will)
      {
        $("#accept_recipient_responsibility").prop('checked',true);
      }
      else
      {
        
        
        $(".require_verification").prop('checked',false);
        $("#accept_recipient_responsibility").prop('checked',false);
        
      }

    });


  }
  else
  {
    $('#showverificationstepprocess').show();
    $('#checboxacknowlegdediv').hide();
    $("#accept_recipient_responsibility").prop('checked',false);
  }
  

});

$('#accept_authorised_person_responsibility').change(function() {
  $('#authorizedpersondetails').hide();
  if($(this).is(':checked'))
  {
    $('#authorizedpersondetails').show();
  }
  else
  {
    $('#authorizedpersondetails').hide();
  }

});

$('#verification_process_step').change(function() {
  
 if($('#verification_process_step option:selected').val()=='5')
 {
  $('#showverificationstep1').show();
  $('#showverificationstep2').hide();
}
else if($('#verification_process_step option:selected').val()=='6')
{
  $('#showverificationstep2').show();
  $('#showverificationstep1').hide();
}

});

function clear_form_elements(id) {
  $(id).find(':input').each(function() {
    switch(this.type) {
      case 'password':
      $(this).val('');
      break;
      case 'text':
      $(this).val('');
      break;
      case 'textarea':
      $(this).val('');
      break;
      case 'file':
      case 'select-one':
      $(this).val('').change();
      break;
      case 'select-multiple':
      case 'date':
      $(this).val('');
      break;
      case 'number':
      $(this).val('');
      break;
      case 'tel':
      $(this).val('');
      break;
      case 'email':
      $(this).val('');
      break;
      case 'checkbox':

      case 'radio':
      this.checked = false;
      break;
    }
  });
}

$('body').find('.password_confirmation').focusout(function(){
        form =$(this).closest('form');
        form.find('.validationmessage').remove();
        var pass = form.find('.password').val();
        var pass2 = form.find('.password_confirmation').val();
        if(pass != pass2){
            $(form.find('.password')).next().remove("p");
            $(form.find('.password')).addClass('is-invalid');
            $(this).addClass('is-invalid');
            $('<p class="validationmessage form-text text-danger text-xs mb-1">The password confirmation does not match</p>').insertAfter(form.find('.password'));
        }

    });

$(document).on('focus keyup','#pwd', function(e) 
{
            $(document).find('#pwd_strength_wrap').fadeIn(100);
            var score = 0;
            var a = $(this).val();
            var desc = new Array();
     
            // strength desc
            desc[0] = "Too short";
            desc[1] = "Weak";
            desc[2] = "Good";
            desc[3] = "Strong";
            desc[4] = "Best";
             
            // password length
            if (a.length >= 6) {
                $("#length").removeClass("text-danger").addClass("text-success");
                score++;
            } else {
                $("#length").removeClass("text-success").addClass("text-danger");
            }
     
            // at least 1 digit in password
            if (a.match(/\d/)) {
                $("#pnum").removeClass("text-danger").addClass("text-success");
                score++;
            } else {
                $("#pnum").removeClass("text-success").addClass("text-danger");
            }
     
            // at least 1 capital & lower letter in password
            if (a.match(/[A-Z]/) && a.match(/[a-z]/)) {
                $("#capital").removeClass("text-danger").addClass("text-success");
                score++;
            } else {
                $("#capital").removeClass("text-success").addClass("text-danger");
            }
     
            // at least 1 special character in password {
            if ( a.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) ) {
                    $("#spchar").removeClass("text-danger").addClass("text-success");
                    score++;
            } else {
                    $("#spchar").removeClass("text-success").addClass("text-danger");
            }
     
     
            if(a.length > 0) {
                    //show strength text
                    $("#passwordDescription").text(desc[score]);
                    // show indicator
                    $("#passwordStrength").removeClass().addClass("strength"+score);
            } else {
                    $("#passwordDescription").text("Password not entered");
                    $("#passwordStrength").removeClass().addClass("strength"+score);
            }
    });
     
        
     
    $(document).on('blur','#pwd', function(e) {
        $("#pwd_strength_wrap").fadeOut(400);
    });

      $(document).on('click','.toggle-password', function(e) {
      console.log('ee');

      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") == "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
});