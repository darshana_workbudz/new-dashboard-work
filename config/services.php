<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [
    'client_id' => '93681808801-dop54urg6fc6vtnbto0up1vini1iguvh.apps.googleusercontent.com',
    'client_secret' => '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',
    'redirect' => 'https://app.evaheld.com/auth/callback',
    ],

    'facebook' => [
    'client_id' => '1023985401884593',
    'client_secret' => '17f10b9fa7300b553eaceb13e72bd015',
    'redirect' => 'https://app.evaheld.com/facebook-login/callback',
    ],


];
