<?php
 
namespace App\View\Components\Main;
 
use Illuminate\View\Component;
 
class Header extends Component
{
    /**
     * The Header icon.
     *
     * @var string
     */
    public $icon;
 
    /**
     * The Header title.
     *
     * @var string
     */
    public $title;

    /**
     * The Header subtitle.
     *
     * @var string
     */
    public $subtitle;

     /**
     * The Header Button text.
     *
     * @var boolean
     */
    public $button;

    /**
     * The Header Button text.
     *
     * @var string
     */
    public $buttonText;
 
    /**
     * The Header Button url.
     *
     * @var object
     */
    public $buttonUrl;

    /**
     * The Header Button icon.
     *
     * @var string
     */
    public $buttonIcon;
 
    /**
     * Create the component instance.
     *
     * @param  string  $icon
     * @param  string  $title
     * @param  string  $subtitle
     * @param  boolean  $button
     * @param  string  $buttonText
     * @param  object  $buttonUrl
     * @param  string  $buttonIcon
     * @return void
     */
    public function __construct(
        $icon = '', 
        $title = '', 
        $subtitle = '', 
        $button = false, 
        $buttonText = '', 
        $buttonUrl = '', 
        $buttonIcon = ''
        )
    {
        $this->icon = $icon;
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->button = $button;
        $this->buttonText = $buttonText;
        $this->buttonUrl = $buttonUrl;
        $this->buttonIcon = $buttonIcon;
    }
 
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.main.header');
    }
}