<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class CheckInSetting extends Model
{
    protected $table = 'check_in_setting';
    protected $primaryKey = 'check_in_setting_id';

    protected $fillable = [
        
        'check_in_setting_id',
        'followup_period_id',
        'responsibility_acknowledge',
        'failed_response_action_id',
        'wating_period_no_of_days',
        'release_message_acknowledge',
        'link_to_access',
        'verification_process_step_id',
        'password',
        'verification_question_1',
        'verification_answer_1',
        'verification_question_2',
        'verification_answer_2',
        'verification_answer_3',
        'verification_question_3',
        'created_at',
        'created_by',
        'updated_by',
        'updated_at',
        'user_id',
        'stage1_completed',
        'stage2_completed',
        'stage3_completed',
        'stage1_date',
        'stage2_date',
        'stage2_count',
        'stage3_date',
        'final_acknowledge'
    
    ];


    
    public static function FindByUserId($id)
    {
        return CheckInSetting::where('user_id',$id)->first();
    }



    
    

}