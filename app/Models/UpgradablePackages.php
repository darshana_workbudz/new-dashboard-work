<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class UpgradablePackages extends Model
{
    protected $table = 'package_upgradable';
    protected $primaryKey = 'package_upgradable_id';

    protected $fillable = [

        'package_upgradable_id',
        'from_package_id',
        'to_package_id',
        'upgrade_price_id',
        'entry_by',
        'entry_at',
        'updated_by',
        'updated_at',
        'upgradable_price',
        'withpersonaliseaddon'

    ];


    public static function GetUpgradablePackages($from_package_id,$stripe_price_id)
    {
       $packages= UpgradablePackages::join('packages','packages.package_id','package_upgradable.to_package_id')
       ->where('from_package_id',$from_package_id)
       ->where('from_price_id',$stripe_price_id)
       ->select('packages.*','package_upgradable.*','package_upgradable.upgradable_price as price')
       ->get();



       if(isset($packages) && count($packages)>0):
        foreach ($packages as $key => $value) {
           $value->inclusions=DB::table('package_inclusions')
           ->where('package_inclusions.package_id',$value->to_package_id)
           ->orderBy('package_inclusions.package_id', 'asc')     
           ->orderBy('package_inclusions.order_no', 'asc')
           ->get()->toArray();
        }
        endif;
        
        return $packages;
    }
   

}