<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class ZiggeoUserAudios extends Model
{
    protected $table = 'ziggeo_user_audios';
    protected $primaryKey = 'ziggeo_user_audio_id';
    public $timestamps = false;
    protected $fillable = [
    
		'ziggeo_user_audio_id',
		'user_id',
		'audio_token_id',
		'audio_key',
		'capture_type',
		'audio_title',
		'audio_description',
		'tags',
		'max_duration',
		'duration',
		'state',
		'embed_audio_url',
		'device_info',
		'submission_date',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by',
		'user_recipient_id',
		'delivery_style',
		'delivery_date',
		'delivery_type',
		'authorisation_type',
		'qr_package_type',
		'qr_package_id',
		'qrid',
		'heilroom_package_type',
		'heilroom_package_id',
		'delivery_status',
        'isclone',
        'contenttype',
        'qrdeliverystyle',
        'is_sent'


        
    ];

    public  static function FindByTokenId($user_id,$token)
    {
        return $data=ZiggeoUserAudios::where('ziggeo_user_audios.user_id',$user_id)
        ->where('audio_token_id',$token)
        ->select('ziggeo_user_audios.ziggeo_user_audio_id as id')
        ->orderBy('ziggeo_user_audio_id','Desc')
        ->first();
    }

    public static function GetAudioByUserId($user_id,$contenttype=null)
    {
    	return $zigeoaudios=ZiggeoUserAudios::leftjoin('user_recipients','user_recipients.user_recipient_id','ziggeo_user_audios.user_recipient_id')
    	->where('ziggeo_user_audios.user_id',$user_id)
        ->where(function($query) use ($contenttype)
        {
             if(isset($contenttype) && !empty($contenttype) && $contenttype=='qrcode-content'):
                $query->where('contenttype','qrcode-content')->orwhereNotNull('qr_package_id');
            endif;

            if(isset($contenttype) && !empty($contenttype) && $contenttype=='heirloom-content'):
                $query->where('contenttype','heirloom-content')->orwhereNotNull('heilroom_package_id');
            endif;
        })
    	->select('ziggeo_user_audios.*','user_recipients.first_name','user_recipients.last_name')
        ->orderBy('ziggeo_user_audios.user_recipient_id','asc')
        ->orderBy('ziggeo_user_audios.preference_completed','asc')
        ->get();
    }

    public  static function GetAudioByUserIdPluck($user_id)
    {
     	return $zigeoaudios=ZiggeoUserAudios::where('user_id',$user_id)->pluck('audio_token_id');
    }

    public  static function FindByMessageId($token)
    {
        return $zigeoaudios=ZiggeoUserAudios::where('ziggeo_user_audio_id',$token)
        ->join('users','users.id','ziggeo_user_audios.user_id')
        ->leftjoin('user_recipients','user_recipients.user_recipient_id','ziggeo_user_audios.user_recipient_id')
        ->leftjoin('user_authorised_person','user_authorised_person.user_id','ziggeo_user_audios.user_id')
        ->select('ziggeo_user_audios.*',
            'users.first_name','users.last_name','users.email',
            'user_authorised_person.first_name as afirst_name',
            'user_authorised_person.last_name as alast_name',
            'user_authorised_person.email as aemail',
            'user_recipients.first_name as rfirst_name',
            'user_recipients.last_name as rlast_name',
            'user_recipients.email as remail',
            'user_recipients.message_access_code as rmessage_access_code',
            'user_recipients.code_expiry_at as rcode_expiry_at',
            'user_authorised_person.verification_process_step_id as averification_process_step_id',
            'user_authorised_person.verification_question_1 as averification_question_1',
            'user_authorised_person.verification_answer_1 as averification_answer_1',
            'user_authorised_person.verification_question_2 as averification_question_2',
            'user_authorised_person.verification_answer_2 as averification_answer_2',
            'user_authorised_person.verification_question_3 as averification_question_3',
            'user_authorised_person.verification_answer_3 as averification_answer_3',
            'user_authorised_person.password as apassword',
            'user_authorised_person.message_access_code as amessage_access_code',
            'user_authorised_person.code_expiry_at as acode_expiry_at',
            'user_recipients.verification_process_step_id as rverification_process_step_id',
            'user_recipients.verification_question_1 as rverification_question_1',
            'user_recipients.verification_answer_1 as rverification_answer_1',
            'user_recipients.verification_question_2 as rverification_question_2',
            'user_recipients.verification_answer_2 as rverification_answer_2',
            'user_recipients.verification_question_3 as rverification_question_3',
            'user_recipients.verification_answer_3 as rverification_answer_3',
            'user_recipients.password as rpassword',
            'user_authorised_person_id',
            'ziggeo_user_audios.audio_token_id as token',


        )
        ->first();
    }
    
    public  static function FindAudioByUserId($user_id,$id)
    {
    	return $zigeoaudios=ZiggeoUserAudios::leftjoin('user_recipients','user_recipients.user_recipient_id','ziggeo_user_audios.user_recipient_id')
    	->where('ziggeo_user_audios.user_id',$user_id)->where('ziggeo_user_audio_id',$id)
    	->select('ziggeo_user_audios.*','user_recipients.first_name','user_recipients.last_name')
    	->first();
    }



    public  static function ToBeDeliveredMessages($userid)
    {

    	$vdata=ZiggeoUserVideos::where('preference_completed',1)
        ->join('users','users.id','ziggeo_user_videos.user_id')
        ->leftjoin('check_in_setting','users.id','check_in_setting.user_id')
        ->join('user_recipients','users.id','user_recipients.user_id')
        ->leftjoin('user_authorised_person','users.id','user_authorised_person.user_id')
    	->where('ziggeo_user_videos.delivery_status',0)
    	->where('ziggeo_user_videos.delivery_style',1)
        ->where('ziggeo_user_videos.is_sent','!=',1)
    	->where('ziggeo_user_videos.user_id',$userid)
    	->select(
            'ziggeo_user_video_id as message_id', DB::raw('1 as mtype'),'video_token_id as token',
            'ziggeo_user_videos.authorisation_type',
            'ziggeo_user_videos.user_recipient_id',
            'ziggeo_user_videos.delivery_type',
            'ziggeo_user_videos.delivery_date',
            'ziggeo_user_videos.qrid',
            'users.email',
            'users.default_prefrence',
            'user_authorised_person.user_authorised_person_id',
            'user_authorised_person.step1confirmed',
            'user_authorised_person.step2confirmed',
            'user_authorised_person.email as thirdpersonemail',
            'user_authorised_person.invitation_status as thirdpersoninvitation_status',
            'user_recipients.user_recipient_id',
            'user_recipients.email as recipientemail'
        )->get()->toArray();


    	$adata=ZiggeoUserAudios::where('preference_completed',1)
        ->join('users','users.id','ziggeo_user_audios.user_id')
        ->leftjoin('check_in_setting','users.id','check_in_setting.user_id')
        ->join('user_recipients','users.id','user_recipients.user_id')
        ->leftjoin('user_authorised_person','users.id','user_authorised_person.user_id')
    	->where('ziggeo_user_audios.delivery_status',0)
    	->where('ziggeo_user_audios.delivery_style',1)
        ->where('ziggeo_user_audios.is_sent','!=',1)
    	->where('ziggeo_user_audios.user_id',$userid)
    	->select(
            'ziggeo_user_audio_id as message_id', DB::raw('2 as mtype'),'audio_token_id as token',
            'ziggeo_user_audios.authorisation_type',
    		'ziggeo_user_audios.user_recipient_id',
            'ziggeo_user_audios.delivery_type',
            'ziggeo_user_audios.delivery_date',
            'ziggeo_user_audios.qrid',
            'users.email',
            'users.default_prefrence',
            'user_authorised_person.user_authorised_person_id',
            'user_authorised_person.step1confirmed',
            'user_authorised_person.step2confirmed',
            'user_authorised_person.email as thirdpersonemail',
            'user_authorised_person.invitation_status as thirdpersoninvitation_status',
            'user_recipients.user_recipient_id',
            'user_recipients.email as recipientemail'
        )->get()->toArray();

    	$tdata=ZiggeoUserTexts::where('preference_completed',1)
        ->join('users','users.id','ziggeo_user_texts.user_id')
        ->join('user_recipients','users.id','user_recipients.user_id')
        ->leftjoin('check_in_setting','users.id','check_in_setting.user_id')
        ->leftjoin('user_authorised_person','users.id','user_authorised_person.user_id')
    	->where('ziggeo_user_texts.delivery_status',0)
        ->where('ziggeo_user_texts.delivery_style',1)
        ->where('ziggeo_user_texts.is_sent','!=',1)
        ->where('ziggeo_user_texts.user_id',$userid)
    	->select(
            'ziggeo_user_text_id as message_id', DB::raw('3 as mtype'),'text_token_id as token',
            'ziggeo_user_texts.authorisation_type',
            'ziggeo_user_texts.user_recipient_id',
            'ziggeo_user_texts.delivery_type',
            'ziggeo_user_texts.delivery_date',
            'ziggeo_user_texts.qrid',
            'users.email',
            'users.default_prefrence',
            'user_authorised_person.user_authorised_person_id',
            'user_authorised_person.step1confirmed',
            'user_authorised_person.step2confirmed',
            'user_authorised_person.email as thirdpersonemail',
            'user_authorised_person.invitation_status as thirdpersoninvitation_status',
            'user_recipients.user_recipient_id',
            'user_recipients.email as recipientemail'
        )->get()->toArray();
        
    	return $messages=array_merge($vdata,$adata,$tdata);


	}

    

}