<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserMembers extends Model
{
    protected $table = 'user_members';
    protected $primaryKey = 'users_member_id';

    protected $fillable = [
        'users_member_id',
        'user_id',
        'member_user_id',
        'isactive',
        'updated_by',
        'updated_at',
        'created_at',
        'created_by'
    ];


    public static function GetActive($user_id)
    {

    	return UserMembers::join('users','users.id','user_members.member_user_id')
        ->leftjoin('user_clients','user_clients.user_id','user_members.member_user_id')
        ->leftjoin('shared_coupons_codes','shared_coupons_codes.user_id','user_members.member_user_id')
        ->where('user_members.user_id',$user_id)
        ->where('user_members.isactive',1)
        ->select('users.*',DB::raw('Count(distinct user_clients.user_client_id) as clientcount'),DB::raw('Count(distinct shared_coupons_codes.shared_coupon_code_id) as couponcount'))
        ->groupBy('users.id')
        ->get();

    }

    public static function GetAllMembers($user_id)
    {

        return UserMembers::join('users','users.id','user_members.member_user_id')
        ->where('user_members.user_id',$user_id)
        ->where('user_members.isactive',1)
        ->select('users.*')
        ->get();

    }

    public static function GetAllMembersCount($user_id)
    {

        return UserMembers::join('users','users.id','user_members.member_user_id')
        ->where('user_members.user_id',$user_id)
        ->where('user_members.isactive',1)
        ->count();

    }


    public static function FindByMemberId($user_id)
    {

        return UserMembers::where('user_members.member_user_id',$user_id)->first();

    }




    

}