<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;
class UserClients extends Model
{
    protected $table = 'user_clients';
    protected $primaryKey = 'user_client_id';

    protected $fillable = [
        'user_client_id',
        'user_id',
        'client_user_id',
        'isactive',
        'updated_by',
        'updated_at',
        'created_at',
        'created_by',
        'organisation_code'
    ];


    public static function FindById($user_id)
    {

        return UserClients::where('user_clients.client_user_id',$user_id)->first();

    }

    public static function GetActive($user_id)
    {

    	return UserClients::join('users','users.id','user_clients.client_user_id')
        ->leftjoin('shared_coupons_codes','shared_coupons_codes.user_id','user_clients.client_user_id')
        ->leftjoin('check_in_setting','check_in_setting.user_id','users.id')
        ->leftjoin('packages','packages.package_id','users.user_package_id')
        ->where('user_clients.user_id',$user_id)
        ->where('user_clients.isactive',1)
        ->select('users.*',DB::raw('Count(distinct shared_coupons_codes.shared_coupon_code_id) as couponcount','packages.name'),
            'check_in_setting.stage1_date','check_in_setting.stage1_completed',
            'check_in_setting.stage2_date','check_in_setting.stage2_completed',
            'check_in_setting.stage3_date','check_in_setting.stage3_completed')
        ->groupBy('users.id')
        ->get();

    }

    public static function GetClientStatistics($user_id,$id)
    {
        return UserClients::join('users','users.id','user_clients.client_user_id')
        ->leftjoin('shared_coupons_codes','shared_coupons_codes.user_id','user_clients.client_user_id')
        ->leftjoin('check_in_setting','check_in_setting.user_id','users.id')
        ->leftjoin('packages','packages.package_id','users.user_package_id')
        ->where('user_clients.user_id',$user_id)
        ->where('user_clients.client_user_id',$id)
        ->where('user_clients.isactive',1)
        ->select('users.*','users.id as user_id',DB::raw('Count(distinct shared_coupons_codes.shared_coupon_code_id) as couponcount','packages.name'),)
        ->first();

    }


    public static function GetMessageCountofClient($user_id)
    {
        $totalcount=0;
        $data= UserClients::
        leftjoin('ziggeo_user_videos','ziggeo_user_videos.user_id','user_clients.client_user_id')
        ->leftjoin('ziggeo_user_audios','ziggeo_user_audios.user_id','user_clients.client_user_id')
        ->leftjoin('ziggeo_user_texts','ziggeo_user_texts.user_id','user_clients.client_user_id')
        ->where('user_clients.user_id',$user_id)
        ->where('user_clients.isactive',1)
        ->select(DB::raw('Count(distinct ziggeo_user_videos.ziggeo_user_video_id) as videocount'),DB::raw('Count(distinct ziggeo_user_texts.ziggeo_user_text_id) as textcount'),DB::raw('Count(distinct ziggeo_user_audios.ziggeo_user_audio_id) as audiocount'))
        ->first();

        if(isset($data) && !empty($data)):
            $totalcount=$data->videocount+$data->textcount+$data->audiocount;
        endif;
        return $totalcount;

    }

    

    

}