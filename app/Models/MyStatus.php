<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class MyStatus extends Model
{
    protected $table = 'mystatus';
    protected $primaryKey = 'status_id';

    protected $fillable = [
    
        'status_id',
        'description',
	    'isactive'
        
    ];


    
	public static function GetActive()
	{
		return MyStatus::where('isactive',1)->get();
	}
    

}