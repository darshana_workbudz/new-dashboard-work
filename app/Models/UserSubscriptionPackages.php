<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserSubscriptionPackages extends Model
{
    protected $table = 'user_subscription_packages';
    protected $primaryKey = 'user_subscription_package_id';

    protected $fillable = [

        'user_subscription_package_id',
		'user_id',
		'package_id',
		'stripe_subscription_id',
		'stripe_product_id',
		'stripe_price_id',
		'message_value',
        'recipient_value',
        'written_value',
		'application_fee_percent',
		'billing_cycle_anchor',
		'cancel_at',
		'current_period_start',
		'cancel_at_period_end',
		'canceled_at',
		'collection_method',
		'current_period_end',
		'stripe_customer_id',
		'days_until_due',
		'default_payment_method',
		'default_source',
		'default_tax_rates',
		'discount',
		'latest_invoice',
		'quantity',
		'schedule',
		'start_date',
		'status',
		'trial_end',
		'plan_interval_count',
		'plan_interval',
		'plan_amount',
		'plan_active',
		'plan_trial_period_days',
		'ended_at',
		'created_at',
		'updated_at',
		'package_code'



    ];


    public static function GetActive()
    {

    	return UserSubscriptionPackages::where('status',1)->get();

    }

    public static function FindByStripe($stripe_subscription_id)
    {

    	return UserSubscriptionPackages::where('stripe_subscription_id',$stripe_subscription_id)->first();

    }

    public static function FindByUser($user_id,$user_subscription_package_id)
    {

    	return UserSubscriptionPackages::where('user_id',$user_id)
    	->where('user_subscription_package_id',$user_subscription_package_id)
    	->first();

    }

    public static function GetActiveSubscriptionPlan($user_id)
    {

    	return UserSubscriptionPackages::join('packages','packages.package_id','user_subscription_packages.package_id')
    	->where('user_id',$user_id)
    	->whereIn('user_subscription_packages.status',['active','trialing'])
    	->select(
    		'user_subscription_packages.*',
    		'packages.name',
    		'packages.message_item',
    		'packages.recipient_item',
    		'packages.written_item',
    		'user_subscription_packages.message_value',
    		'user_subscription_packages.recipient_value',
    		'user_subscription_packages.written_value',
    		'packages.linked_to_package_id',
    		'packages.upgradable'
    	)
    	->first();

    }

    public static function GetActiveStandAlone($user_id)
    {

        return UserStandAlonePackages::join('packages','packages.package_id','user_standalone_packages.package_id')
        ->where('user_id',$user_id)
        ->where('user_standalone_packages.status',1)
        ->select(
            'user_standalone_packages.*',
            'packages.name',
            'packages.message_item',
            'packages.recipient_item',
            'packages.written_item',
            'user_standalone_packages.message_value',
            'user_standalone_packages.recipient_value',
            'user_standalone_packages.written_value',
            'packages.linked_to_package_id',
            'packages.upgradable',  
            'packages.price as plan_amount',
            'packages.slug'
        )
        ->get();

    }

    

    public static function GetAllActiveSubscriptionPlan($user_id)
    {

    	return UserSubscriptionPackages::where('user_id',$user_id)
    	->whereIn('user_subscription_packages.status',['active','trialing'])
    	->get();

    }
    

}