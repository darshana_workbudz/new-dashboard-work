<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class EmailScheduler extends Model
{
    protected $table = 'email_scheduler';
    protected $primaryKey = 'email_scheduler_id';

    protected $fillable = [

        'email_scheduler_id',
		'message_id',
		'user_id',
		'date',
		'created_at',
		'dispatch_date',
		'updated_at',
		'stageno',
		'status',
		'check_in_setting_id',
		'acknowledge',
		'isprocessed',
		'acknowledge'



    ];

    public static function GetLastCheckIn($user_id)
    {
    	$data=EmailScheduler::where('user_id',$user_id)->where('isprocessed',1)
    	->OrderBy('email_scheduler_id','Desc')->first();
    	return $data;
    }


}