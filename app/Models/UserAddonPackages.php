<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserAddonPackages extends Model
{
    protected $table = 'user_addon_packages';
    protected $primaryKey = 'user_addon_package_id';

    protected $fillable = [

        'user_addon_package_id',
        'package_id',
        'package_start_date',
        'package_end_date',
        'status',
        'message_value',
        'recipient_value',
        'written_value',
        'stripe_price_id',
        'stripe_product_id',
        'created_at',
        'updated_at',
        'user_id',
        'line_item_id',
        'stripe_subscription_id',
        'package_code',
        'linked_message_type',
        'linked_message_id',
        'qrid',
        'user_package_id',
        'is_organisation_package',
        'organisation_user_id',
        'price'

    ];


    public static function FindByUser($user_id,$user_addon_package_id)
    {

        return UserAddonPackages::where('user_id',$user_id)
        ->where('user_addon_package_id',$user_addon_package_id)
        ->first();

    }

    public static function FindByUserId($user_id)
    {

        return UserAddonPackages::join('packages','packages.package_id','user_addon_packages.package_id')
        ->where('user_id',$user_id)
        ->selectRaw('count(user_addon_package_id) as quantity
            ,user_addon_packages.package_id,
            packages.name,
            user_addon_packages.message_value,
            user_addon_packages.recipient_value,
            user_addon_packages.written_value,
            user_addon_packages.created_at'
            
        )
        ->GroupBy('user_addon_packages.package_id')
        ->get();

    }

    public static function FindByStripe($user_id)
    {

        return UserAddonPackages::join('packages','packages.package_id','user_addon_packages.package_id')
        ->where('user_addon_packages.user_id',$user_id)
        ->selectRaw('count(user_addon_package_id) as quantity
            ,user_addon_packages.package_id,
            packages.name,
            user_addon_packages.message_value,
            user_addon_packages.recipient_value,
            user_addon_packages.written_value,
            packages.type'
            
        )
        ->GroupBy('user_addon_packages.package_id')
        ->get();

    }


    public static function HasQrCodeAddon($user_id)
    {

        return UserAddonPackages::where('user_addon_packages.status',1)
                        ->where('user_addon_packages.user_id',$user_id)
                        ->join('packages','user_addon_packages.package_id','packages.package_id')
                        ->where('packages.slug','addon-qr')
                        ->count();

    }

    public static function HasHeirloomAddon($user_id)
    {

        return UserAddonPackages::where('user_addon_packages.status',1)
                        ->where('user_addon_packages.user_id',$user_id)
                        ->join('packages','user_addon_packages.package_id','packages.package_id')
                        ->where('packages.slug','addon-heirloom')
                        ->count();

    }


    public static function HasExtraPagesAddon($user_id)
    {

        return UserAddonPackages::where('user_addon_packages.status',1)
                        ->where('user_addon_packages.user_id',$user_id)
                        ->join('packages','user_addon_packages.package_id','packages.package_id')
                        ->where('packages.slug','addon-extrapages')
                        ->count();

    }

    public static function HasExtraMinutesAddon($user_id)
    {

        return UserAddonPackages::where('user_addon_packages.status',1)
                        ->where('user_addon_packages.user_id',$user_id)
                        ->join('packages','user_addon_packages.package_id','packages.package_id')
                        ->where('packages.slug','addon-extraminutes')
                        ->count();

    }

}