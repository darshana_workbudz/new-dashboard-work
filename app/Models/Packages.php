<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Packages extends Model
{
    protected $table = 'packages';
    protected $primaryKey = 'package_id';

    protected $fillable = [
        'package_id',
        'name',
        'message_item',
        'recipient_item',
        'written_item',
        'amount',
        'message_value',
        'recipient_value',
        'written_value',
        'stripe_price_id',
        'stripe_product_id',
        'isactive','type'
    ];


    public static function GetActive()
    {

        return Packages::where('isactive',1)->get();

    }


    public static function getSubscriptionPackages(){

        return  DB::table('package_inclusions')
        ->leftjoin('packages','packages.package_id','package_inclusions.package_id')
        ->select('packages.name','packages.priceannually','packages.issubscription','packages.demopackage','package_inclusions.*')
        ->where('packages.issubscription',1)
        ->orderBy('packages.package_id', 'asc')     
        ->orderBy('package_inclusions.order_no', 'asc')->get()->groupby('package_id');

    }

    public static function getQRPackages(){

    return DB::table('package_inclusions')
        ->leftjoin('packages','packages.package_id','package_inclusions.package_id')
        ->select('packages.name','packages.priceannually','packages.issubscription','packages.demopackage','packages.price','package_inclusions.*')
        ->where('slug','standalone-qr')
        ->where('packages.isactive',1)
        ->orderBy('packages.package_id', 'asc')
        ->orderBy('package_inclusions.order_no', 'asc')->get()->groupby('package_id');

    }


    public static function getHeirLoomPackages(){
        
    return DB::table('package_inclusions')
        ->leftjoin('packages','packages.package_id','package_inclusions.package_id')
        ->select('packages.name','packages.priceannually','packages.issubscription','packages.demopackage','packages.price','package_inclusions.*')
        ->where('slug','standalone-heirloom')
        ->where('packages.isactive',1)
        ->orderBy('packages.package_id', 'asc')
        ->orderBy('package_inclusions.order_no', 'asc')->get()->groupby('package_id');

    }

    public static function getAllComingSoonPackageData(){
        return DB::table('package_coming_soon')
        ->orderBy('package_coming_soon.order_no', 'asc')
        ->get()->groupby('package_id');
   
    }


    public static function getHeirLoomAddonPackages($package_id){
        
        $packages=Packages::find($package_id);

        if($packages->type=='1'):
        
        return Packages::join('package_addons','package_addons.addon_id','packages.package_id')
        ->where('package_addons.package_id',$package_id)
        ->where('slug','addon-heirloom')
        ->select('packages.price','packages.name','packages.stripe_price_id','package_addons.addon_id as package_id')
        ->first();
        endif; 

        if($packages->type=='3'):
        return Packages::join('package_addons','package_addons.addon_id','packages.package_id')
        ->where('package_addons.package_id',$packages->linked_to_package_id)
        ->where('slug','addon-heirloom')
        ->select('packages.price','packages.name','packages.stripe_price_id','package_addons.addon_id as package_id')
        ->first();
        endif;                                                                                                                                                                                                  

    }


    public static function getQrAddonPackages($package_id){
        
        $packages=Packages::find($package_id);

        if($packages->type=='1'):
        return Packages::join('package_addons','package_addons.addon_id','packages.package_id')
        ->where('package_addons.package_id',$package_id)
        ->where('slug','addon-qr')
        ->select('packages.price','packages.name','packages.stripe_price_id','package_addons.addon_id as package_id')
        ->first();
        endif; 

        if($packages->type=='3'):
        return Packages::join('package_addons','package_addons.addon_id','packages.package_id')
        ->where('package_addons.package_id',$packages->linked_to_package_id)
        ->where('slug','addon-qr')
        ->select('packages.price','packages.name','packages.stripe_price_id','package_addons.addon_id as package_id')
        ->first();
        endif;                                                                                                                                                                                                  

    }

    public static function getExtraMinutesAddonPackages($package_id){
        
        $packages=Packages::find($package_id);

        if($packages->type=='1'):
        return Packages::join('package_addons','package_addons.addon_id','packages.package_id')
        ->where('package_addons.package_id',$package_id)
        ->where('slug','addon-extraminutes')
        ->select('packages.price','packages.name','packages.stripe_price_id','package_addons.addon_id as package_id')
        ->first();
        endif; 

        if($packages->type=='3'):
        return Packages::join('package_addons','package_addons.addon_id','packages.package_id')
       ->where('package_addons.package_id',$packages->linked_to_package_id)
        ->where('slug','addon-extraminutes')
        ->select('packages.price','packages.name','packages.stripe_price_id','package_addons.addon_id as package_id')
        ->first();
        endif;                                                                                                                                                                                                 

    }

    public static function getExtraPagesAddonPackages($package_id){
        
        $packages=Packages::find($package_id);

        if($packages->type=='1'):
        return Packages::join('package_addons','package_addons.addon_id','packages.package_id')
        ->where('package_addons.package_id',$package_id)
        ->where('slug','addon-extrapages')
        ->select('packages.price','packages.name','packages.stripe_price_id','package_addons.addon_id as package_id')
        ->first();
        endif; 

        if($packages->type=='3'):
        return Packages::join('package_addons','package_addons.addon_id','packages.package_id')
       ->where('package_addons.package_id',$packages->linked_to_package_id)
        ->where('slug','addon-extrapages')
        ->select('packages.price','packages.name','packages.stripe_price_id','package_addons.addon_id as package_id')
        ->first();
        endif;                                                                                                                                                                                                 

    }


    public static function AllOnceoffPackages()
    {
       $packages=Packages::where('type',1)->where('demopackage','!=',1)->where('isactive',1)->get();

         if(isset($packages) && !empty($packages)):
         foreach($packages as $packagedata):
             $packagedata->inclusions=DB::table('package_inclusions')
             ->where('package_inclusions.package_id',$packagedata->package_id)
            ->orderBy('package_inclusions.package_id', 'asc')     
            ->orderBy('package_inclusions.order_no', 'asc')
            ->get()->toArray();
         endforeach;
         endif;

         return $packages;
    }

    public static function AllStandAloneQrPackages()
    {
        $packages=Packages::where('type',3)->where('slug','standalone-qr')->where('isactive',1)->get();



         if(isset($packages) && !empty($packages)):
         foreach($packages as $packagedata):
             $packagedata->inclusions=DB::table('package_inclusions')
             ->where('package_inclusions.package_id',$packagedata->package_id)
            ->orderBy('package_inclusions.package_id', 'asc')     
            ->orderBy('package_inclusions.order_no', 'asc')
            ->get()->toArray();
         endforeach;
         endif;

         return $packages;
    }

    public static function AllStandAloneHeirloomPackages()
    {
        $packages=Packages::where('type',3)->where('slug','standalone-heirloom')->where('isactive',1)->get();

        

         if(isset($packages) && !empty($packages)):
         foreach($packages as $packagedata):
             $packagedata->inclusions=DB::table('package_inclusions')
             ->where('package_inclusions.package_id',$packagedata->package_id)
            ->orderBy('package_inclusions.package_id', 'asc')     
            ->orderBy('package_inclusions.order_no', 'asc')
            ->get()->toArray();
         endforeach;
         endif;

         return $packages;
    }

}