<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserPackages extends Model
{
    protected $table = 'user_packages';
    protected $primaryKey = 'user_package_id';

    protected $fillable = [

        'user_package_id',
        'package_id',
        'package_start_date',
        'package_end_date',
        'status',
        'message_value',
        'recipient_value',
        'written_value',
        'stripe_price_id',
        'stripe_product_id',
        'created_at',
        'updated_at',
        'user_id',
        'line_item_id',
        'package_code',
        'type',
        'price',
        'is_organisation_package',
        'organisation_user_id',
        'created_by'


    ];


    public static function GetActive()
    {

    	return UserPackages::where('status',1)->get();

    }

    public static function GetInActiveByUserId($id)
    {

        return UserPackages::where('user_packages.user_id',$id)
        ->join('packages','packages.package_id','user_packages.package_id')
        ->select(
            'user_packages.*',
            'packages.name',
            'packages.message_item',
            'packages.recipient_item',
            'packages.written_item',
            'user_packages.message_value',
            'user_packages.recipient_value',
            'user_packages.written_value',
            'user_packages.price',
            'packages.linked_to_package_id',
            'packages.upgradable',
            'packages.quotes',
            'packages.salequotes',
            'packages.saleheadline',
            'packages.demopackage',
            'packages.usage_description',
            'packages.personalised_message_value'

        )
        ->where('status','!=',1)
        ->orderBy('user_package_id','Desc')
        ->get();

    }

     public static function GetActiveByUserId($id)
    {

        return UserPackages::where('user_packages.user_id',$id)
        ->join('packages','packages.package_id','user_packages.package_id')
        ->select(
            'user_packages.*',
            'packages.name',
            'packages.message_item',
            'packages.recipient_item',
            'packages.written_item',
            'user_packages.message_value',
            'user_packages.recipient_value',
            'user_packages.written_value',
            'user_packages.price',
            'packages.linked_to_package_id',
            'packages.upgradable',
            'packages.quotes',
            'packages.salequotes',
            'packages.saleheadline',
            'packages.demopackage',
            'packages.usage_description',
            'packages.personalised_message_value'

        )
        ->where('status',1)
        ->orderBy('user_package_id','Desc')
        ->get();

    }
    

    public static function FindByUser($user_id,$user_package_id)
    {

        return UserPackages::where('user_id',$user_id)
        ->where('user_package_id',$user_package_id)
        ->first();

    }

    public static function GetCurrentActivePlan()
    {

        return UserPackages::where('status',1)->orderBy('user_package_id','Desc')->first();

    }

    public static function GetActivePlan($user_id)
    {

        $userpackages= UserPackages::join('packages','packages.package_id','user_packages.package_id')
        ->where('user_packages.user_id',$user_id)
        ->where('user_packages.status',1)
        ->select(
            'user_packages.*',
            'packages.name',
            'packages.message_item',
            'packages.recipient_item',
            'packages.written_item',
            'user_packages.message_value',
            'user_packages.recipient_value',
            'user_packages.written_value',
            'user_packages.price',
            'packages.linked_to_package_id',
            'packages.upgradable',
            'packages.quotes',
            'packages.salequotes',
            'packages.saleheadline',
            'packages.demopackage',
            'packages.usage_description',
            'packages.personalised_message_value'

        )
        ->OrderBy('user_package_id','Desc')
        ->first();

        if(isset($userpackages) && !empty($userpackages)):
            $userpackages->inclusions=DB::table('package_inclusions')
           ->where('package_inclusions.package_id',$userpackages->package_id)
           ->orderBy('package_inclusions.package_id', 'asc')     
           ->orderBy('package_inclusions.order_no', 'asc')
           ->get()->toArray();
        endif;

        return $userpackages;

    }

    public static function GetPartnersAllActivePlanCount($user_id)
    {
        return $userpackages= UserPackages::where('user_packages.organisation_user_id',$user_id)
        ->where('user_packages.is_organisation_package',1)
        ->where('user_packages.package_id','!=',1)
        ->where('user_packages.status',1)
        ->count();
    }

    public static function GetPartnersAllActivePlan($user_id)
    {

        $userpackages= UserPackages::join('packages','packages.package_id','user_packages.package_id')
        ->leftjoin('users','user_packages.user_id','users.id')
        ->leftjoin('shared_organisation_packages','user_packages.user_package_id','shared_organisation_packages.user_package_id')
        ->where('user_packages.organisation_user_id',$user_id)
        ->where('user_packages.is_organisation_package',1)
        ->where('user_packages.package_id','!=',1)
        ->where('user_packages.status',1)
        ->where('packages.slug','package')
        ->select(
            'user_packages.*',
            'packages.name',
            'packages.message_item',
            'packages.recipient_item',
            'packages.written_item',
            'user_packages.message_value',
            'user_packages.recipient_value',
            'user_packages.written_value',
            'user_packages.price',
            'packages.linked_to_package_id',
            'packages.upgradable',
            'packages.quotes',
            'packages.salequotes',
            'packages.saleheadline',
            'packages.demopackage',
            'packages.usage_description',
            'packages.personalised_message_value',
            'users.first_name',
            'users.last_name',
            DB::raw("sum(Case when shared_organisation_packages.organisation_package_id is not null then 1 else 0 end) as memberscount")
        )
        ->groupBy('user_packages.user_package_id')
        ->OrderBy('user_packages.user_package_id','Desc')
        ->get();
        return $userpackages;
    }

    public static function GetPartnersHeirloomActivePlan($user_id)
    {

        $userpackages= UserPackages::join('packages','packages.package_id','user_packages.package_id')
        ->leftjoin('users','user_packages.user_id','users.id')
        ->leftjoin('shared_organisation_packages','user_packages.user_package_id','shared_organisation_packages.user_package_id')
        ->where('user_packages.organisation_user_id',$user_id)
        ->where('user_packages.is_organisation_package',1)
        ->where('user_packages.package_id','!=',1)
        ->where('user_packages.status',1)
        ->where('packages.slug','standalone-heirloom')
        ->select(
            'user_packages.*',
            'packages.name',
            'packages.message_item',
            'packages.recipient_item',
            'packages.written_item',
            'user_packages.message_value',
            'user_packages.recipient_value',
            'user_packages.written_value',
            'user_packages.price',
            'packages.linked_to_package_id',
            'packages.upgradable',
            'packages.quotes',
            'packages.salequotes',
            'packages.saleheadline',
            'packages.demopackage',
            'packages.usage_description',
            'packages.personalised_message_value',
            'users.first_name',
            'users.last_name',
            DB::raw("sum(Case when shared_organisation_packages.organisation_package_id is not null then 1 else 0 end) as memberscount")
        )
        ->groupBy('user_packages.user_package_id')
        ->OrderBy('user_packages.user_package_id','Desc')
        ->get();
        return $userpackages;
    }

    public static function GetPartnersQrActivePlan($user_id)
    {

        $userpackages= UserPackages::join('packages','packages.package_id','user_packages.package_id')
        ->leftjoin('user_addon_packages','user_addon_packages.user_package_id','user_packages.user_package_id')
        ->leftjoin('shared_organisation_packages','user_packages.user_package_id','shared_organisation_packages.user_package_id')
        ->leftjoin('users','user_packages.user_id','users.id')
        ->where('user_packages.organisation_user_id',$user_id)
        ->where('user_packages.is_organisation_package',1)
        ->where('user_packages.package_id','!=',1)
        ->where('user_packages.status',1)
        ->where('packages.slug','standalone-qr')
        ->select(
            'user_packages.*',
            'packages.name',
            'packages.message_item',
            'packages.recipient_item',
            'packages.written_item',
            'user_packages.message_value',
            'user_packages.recipient_value',
            'user_packages.written_value',
            'user_packages.price',
            'packages.linked_to_package_id',
            'packages.upgradable',
            'packages.quotes',
            'packages.salequotes',
            'packages.saleheadline',
            'packages.demopackage',
            'packages.usage_description',
            'packages.personalised_message_value',
            'users.first_name',
            'users.last_name',
            DB::raw("Count('user_addon_packages.user_addon_package_id') as qraddonacount"),
            DB::raw("sum(Case when shared_organisation_packages.organisation_package_id is not null then 1 else 0 end) as memberscount")

        )
        ->groupBy('user_packages.user_package_id')
        ->OrderBy('user_packages.user_package_id','Desc')
        ->get();
        return $userpackages;
    }


    public static function GetMembersAllActivePlan($user_id)
    {

        $userpackages= SharedOrganisationPackages::join('user_packages','user_packages.user_package_id','shared_organisation_packages.user_package_id')
        ->join('packages','packages.package_id','user_packages.package_id')
        ->leftjoin('users','user_packages.user_id','users.id')
        ->where('shared_organisation_packages.member_user_id',$user_id)
        ->where('user_packages.is_organisation_package',1)
        ->where('user_packages.package_id','!=',1)
        ->where('user_packages.status',1)
        ->where('packages.slug','package')
        ->select(
            'user_packages.*',
            'packages.name',
            'packages.message_item',
            'packages.recipient_item',
            'packages.written_item',
            'user_packages.message_value',
            'user_packages.recipient_value',
            'user_packages.written_value',
            'user_packages.price',
            'packages.linked_to_package_id',
            'packages.upgradable',
            'packages.quotes',
            'packages.salequotes',
            'packages.saleheadline',
            'packages.demopackage',
            'packages.usage_description',
            'packages.personalised_message_value',
            'users.first_name',
            'users.last_name',
            DB::raw("sum(Case when shared_organisation_packages.organisation_package_id is not null then 1 else 0 end) as memberscount")
        )
        ->groupBy('user_packages.user_package_id')
        ->OrderBy('user_packages.user_package_id','Desc')
        ->get();
        return $userpackages;
    }

    public static function GetMembersHeirloomActivePlan($user_id)
    {

        $userpackages= SharedOrganisationPackages::join('user_packages','user_packages.user_package_id','shared_organisation_packages.user_package_id')
        ->join('packages','packages.package_id','user_packages.package_id')
        ->leftjoin('users','user_packages.user_id','users.id')
        ->where('shared_organisation_packages.member_user_id',$user_id)
        ->where('user_packages.is_organisation_package',1)
        ->where('user_packages.package_id','!=',1)
        ->where('user_packages.status',1)
        ->where('packages.slug','standalone-heirloom')
        ->select(
            'user_packages.*',
            'packages.name',
            'packages.message_item',
            'packages.recipient_item',
            'packages.written_item',
            'user_packages.message_value',
            'user_packages.recipient_value',
            'user_packages.written_value',
            'user_packages.price',
            'packages.linked_to_package_id',
            'packages.upgradable',
            'packages.quotes',
            'packages.salequotes',
            'packages.saleheadline',
            'packages.demopackage',
            'packages.usage_description',
            'packages.personalised_message_value',
            'users.first_name',
            'users.last_name',
            DB::raw("sum(Case when shared_organisation_packages.organisation_package_id is not null then 1 else 0 end) as memberscount")
        )
        ->groupBy('user_packages.user_package_id')
        ->OrderBy('user_packages.user_package_id','Desc')
        ->get();
        return $userpackages;
    }

    public static function GetMembersQrActivePlan($user_id)
    {

        $userpackages= SharedOrganisationPackages::join('user_packages','user_packages.user_package_id','shared_organisation_packages.user_package_id')
        ->join('packages','packages.package_id','user_packages.package_id')
        ->leftjoin('users','user_packages.user_id','users.id')
        ->where('shared_organisation_packages.member_user_id',$user_id)
        ->where('user_packages.is_organisation_package',1)
        ->where('user_packages.package_id','!=',1)
        ->where('user_packages.status',1)
        ->where('packages.slug','standalone-qr')
        ->select(
            'user_packages.*',
            'packages.name',
            'packages.message_item',
            'packages.recipient_item',
            'packages.written_item',
            'user_packages.message_value',
            'user_packages.recipient_value',
            'user_packages.written_value',
            'user_packages.price',
            'packages.linked_to_package_id',
            'packages.upgradable',
            'packages.quotes',
            'packages.salequotes',
            'packages.saleheadline',
            'packages.demopackage',
            'packages.usage_description',
            'packages.personalised_message_value',
            'users.first_name',
            'users.last_name',
            DB::raw("Count('user_addon_packages.user_addon_package_id') as qraddonacount"),
            DB::raw("sum(Case when shared_organisation_packages.organisation_package_id is not null then 1 else 0 end) as memberscount")

        )
        ->groupBy('user_packages.user_package_id')
        ->OrderBy('user_packages.user_package_id','Desc')
        ->get();
        return $userpackages;
    }

    

}