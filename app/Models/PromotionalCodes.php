<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class PromotionalCodes extends Model
{
    protected $table = 'promotional_codes';
    protected $primaryKey = 'promotional_code_id';

    protected $fillable = [

	'promotional_code_id',
	'stripe_coupon_id',
	'coupon_id',
	'object',
	'created',
	'livemode',
	'max_redemptions',
	'metadata',
	'code',
	'restrictions',
	'times_redeemed',
	'active',
	'created_at',
	'created_by',
	'updated_at',
	'updated_by',
	'expires_at',
	'customer'


    ];




    public static function GetActiveByCouponId($id)
    {

        return PromotionalCodes::where('promotional_codes.coupon_id',$id)
        ->where('promotional_codes.active','=',1)
        ->where('coupons.valid','=',1)
        ->join('coupons','coupons.coupon_id','promotional_codes.coupon_id')
        ->select('promotional_codes.*')
        ->get();

    }


    public static function GetActive()
    {

        return PromotionalCodes::where('promotional_codes.active','=',1)
        ->join('coupons','coupons.coupon_id','promotional_codes.coupon_id')
        ->where('coupons.valid','=',1)
        ->select('promotional_codes.*')
        ->get();

    }





    

}