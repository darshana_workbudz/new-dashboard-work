<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class OrganisationRoles extends Model
{
    protected $table = 'organisation_roles';
    protected $primaryKey = 'organisation_role_id';

    protected $fillable = [
    
        'organisation_role_id',
        'description',
        'header_id',
	    'isactive'
        
    ];


    
	public static function GetActive()
	{
		$data= OrganisationRoles::where('organisation_roles.isactive',1)
        ->leftjoin('organisation_roles_header','organisation_roles_header.header_id','organisation_roles.header_id')
        ->where('organisation_roles_header.isactive',1)
        ->select('organisation_roles_header.description as label','organisation_roles.*')
        ->get()->groupBy('label');
        return $data;

	}
    

}