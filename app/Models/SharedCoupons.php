<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class SharedCoupons extends Model
{
    protected $table = 'shared_coupons_codes';
    protected $primaryKey = 'shared_coupon_id';

    protected $fillable = [
        'shared_coupon_code_id',
		'promotional_code_id',
		'user_id',
		'created_at',
		'created_by',
		'updated_at',
		'updated_by'

    ];


    public static function GetActive($user_id)
    {

        $sharedcouponscodes=SharedCoupons::where('promotional_codes.active','=',1)
        ->where('coupons.valid','=',1)
        ->where('shared_coupons_codes.user_id','=',$user_id)
        ->join('promotional_codes','promotional_codes.promotional_code_id','shared_coupons_codes.promotional_code_id')
        ->join('coupons','coupons.coupon_id','promotional_codes.coupon_id')
        ->pluck('shared_coupons_codes.promotional_code_id')->toArray();

        return isset($sharedcouponscodes) && !empty($sharedcouponscodes) ? $sharedcouponscodes : array();
    }


    public static function GetAllActiveCount($user_id)
    {

        return $sharedcouponscodes=SharedCoupons::where('promotional_codes.active','=',1)
        ->where('coupons.valid','=',1)
        ->where('shared_coupons_codes.user_id','=',$user_id)
        ->join('promotional_codes','promotional_codes.promotional_code_id','shared_coupons_codes.promotional_code_id')
        ->join('coupons','coupons.coupon_id','promotional_codes.coupon_id')
        ->count();

        
    }


    public static function GetActiveByUser($user_id)
    {

        $sharedcouponscodes=SharedCoupons::where('promotional_codes.active','=',1)
        ->where('coupons.valid','=',1)
        ->where('shared_coupons_codes.user_id','=',$user_id)
        ->join('promotional_codes','promotional_codes.promotional_code_id','shared_coupons_codes.promotional_code_id')
        ->join('coupons','coupons.coupon_id','promotional_codes.coupon_id')
        ->select('promotional_codes.*','coupons.name')
        ->get();

        return $sharedcouponscodes;
    }



    
}