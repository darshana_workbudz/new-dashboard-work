<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserStandAlonePackages extends Model
{
    protected $table = 'user_standalone_packages';
    protected $primaryKey = 'user_standalone_package_id';

    protected $fillable = [

        'user_standalone_package_id',
        'package_id',
        'package_start_date',
        'package_end_date',
        'status',
        'message_value',
        'recipient_value',
        'written_value',
        'stripe_price_id',
        'stripe_product_id',
        'created_at',
        'updated_at',
        'user_id',
        'line_item_id',
        'package_code',
        'linked_message_type',
        'linked_message_id',
        'qrid'


    ];


    public static function GetActive()
    {

    	return UserStandAlonePackages::where('status',1)->get();

    }

    public static function FindByUser($user_id,$user_standalone_package_id)
    {

        return UserStandAlonePackages::where('user_id',$user_id)
        ->where('user_standalone_package_id',$user_standalone_package_id)
        ->first();

    }

}