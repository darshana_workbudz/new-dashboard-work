<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserOrganisation extends Model
{
    protected $table = 'user_organisation';
    protected $primaryKey = 'user_organisation_id';

    protected $fillable = [
        'user_organisation_id',
        'user_id',
        'organisation_name',
        'organisation_role',
        'organisation_code',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];


    public static function FindByUser($user_id)
    {

    	return UserOrganisation::where('user_id',$user_id)->first();

    }

    

}