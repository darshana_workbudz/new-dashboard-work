<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class OrganisationDepartment extends Model
{
    protected $table = 'organisation_department';
    protected $primaryKey = 'department_id';

    protected $fillable = [
    
        'department_id',
        'description',
	    'isactive'
        
    ];


    
	public static function GetActive()
	{
		return OrganisationDepartment::where('isactive',1)->get();
	}
    

}