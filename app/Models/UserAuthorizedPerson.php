<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserAuthorizedPerson extends Model
{
    protected $table = 'user_authorised_person';
    protected $primaryKey = 'user_authorised_person_id';

    protected $fillable = [
    
            'user_authorised_person_id',
            'user_id',
            'first_name',
            'last_name',
            'email',
            'phone_code',
            'phone_number',
            'address_1',
            'address_2',
            'address_3',
            'postcode',
            'country',
            'status',
            'verification_process_step_id',
            'sib_contact_id',
            'relationship_id',
            'other_relationship',
            'password',
            'verification_question_1',
            'verification_answer_1',
            'verification_question_2',
            'verification_answer_2',
            'verification_question_3',
            'verification_answer_3',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
            'islawyer',
            'organisation',
            'invitation_status',
            'invitation_link',
            'authorised_access_type',
            'organisation_address',
            'organisation_phonecode',
            'organisation_phonenumber',
            'giveaccountaccess',
            'aigcanemail',
            'verification_to_releasemessage',
            'state',
            'step1confirmed',
            'step2confirmed',
            'message_access_code',
            'code_expiry_at'



    ];


    

    public static function FindByUser($userid)
    {

        return UserAuthorizedPerson::where('user_id',$userid)
        ->first();

    }



    public static function FindById($userid,$id)
    {

        return UserAuthorizedPerson::where('user_id',$userid)->where('user_authorised_person_id',$id)
        ->first();

    }



}