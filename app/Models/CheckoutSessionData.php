<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class CheckoutSessionData extends Model
{
    protected $table = 'checkout_session';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'checkout_session_id',
        'package_id',
        'payment_status',
        'subscription',
        'stripe_customer_id',
        'user_id',
        'expires_at',
        'lineitems',
        'created_by',
        'updated_by',
        'updated_at',
        'created_at'
    ];


    

    

}