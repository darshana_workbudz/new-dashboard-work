<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class SharedOrganisationPackages extends Model
{
    protected $table = 'shared_organisation_packages';
    protected $primaryKey = 'shared_organisation_package_id';

    protected $fillable = [

        'shared_organisation_package_id',
		'type',
		'user_package_id',
		'member_user_id',
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'




    ];


    public static function GetActiveByUser($user_id,$user_package_id)
    {

        $shared=UserPackages::where('user_packages.status','=',1)
        ->where('user_packages.organisation_user_id','=',$user_id)
        ->where('user_packages.user_package_id','=',$user_package_id)
        ->leftjoin('shared_organisation_packages','shared_organisation_packages.user_package_id','user_packages.user_package_id')
        ->pluck('shared_organisation_packages.member_user_id')->toArray();

        return isset($shared) && !empty($shared) ? $shared : array();
    }


   

    

}