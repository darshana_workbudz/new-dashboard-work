<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class ClientsDocuments extends Model
{
    protected $table = 'client_documents';
    protected $primaryKey = 'client_document_id';
    public $timestamps = false;
    protected $fillable = [
    
	
			'client_document_id',
			'user_id',
			'title',
			'capture_type',
			'path',
			'uploadedfilename',
			'created_at',
			'created_by',
			'updated_at',
			'updated_by',
			'filetype'
			


        
    ];
}