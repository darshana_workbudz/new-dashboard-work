<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserInvoices extends Model
{
    protected $table = 'user_invoices';
    protected $primaryKey = 'user_invoice_id';

    protected $fillable = [

        "user_invoice_id",
		"stripe_invoice_id",
		"user_id",
		"stripe_customer_id",
		"account_country",
		"account_name",
		"amount_due",
		"amount_paid",
		"amount_remaining",
		"attempt_count",
		"attempted",
		"collection_method",
		"created_at",
		"customer_address",
		"customer_email",
		"customer_name",
		"customer_phone",
		"customer_shipping",
		"discount",
		"discounts",
		"due_date",
		"hosted_invoice_url",
		"invoice_pdf",
		"lineitems",
		"next_payment_attempt",
		"number",
		"paid",
		"payment_intent",
		"period_end",
		"period_start",
		"receipt_number",
		"status",
		"status_transitions",
		"subscription",
		"subtotal",
		"total",
		"total_discount_amounts",
		"total_tax_amounts"


    ];

    public static function FindByStripe($stripe_invoice_id)
    {

    	return UserInvoices::where('stripe_invoice_id',$stripe_invoice_id)->first();

    }

    public static function GetByUser($user_id)
    {

    	return UserInvoices::where('user_id',$user_id)->get();

    }
    

}