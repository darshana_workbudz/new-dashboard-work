<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class HasAccessLogin extends Model
{
    protected $table = 'has_access_login';
    protected $primaryKey = 'has_access_login_id';

    protected $fillable = [
    
        'has_access_login_id',
        'main_user_id',
        'user_authorised_person_id',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by',
        'invitation_status',
        'email',
        'isactive',
        'password'
        
    ];


    
	public static function GetActive()
	{
		return HasAccessLogin::where('isactive',1)->get();
	}

    public static function Checkemailexist($email)
    {
        return HasAccessLogin::where('has_access_login.email',$email)
        ->join('user_authorised_person','user_authorised_person.user_authorised_person_id','has_access_login.user_authorised_person_id')
        ->where('has_access_login.isactive',1)
        ->where('has_access_login.invitation_status',1)
        ->select('has_access_login.*','user_authorised_person.email as myaccountemail')
        ->first();
    }

    public static function GetByUser($email)
    {
        return UserAuthorizedPerson::
        join('users','users.id','user_authorised_person.user_id')
        ->join('has_access_login','user_authorised_person.user_authorised_person_id','has_access_login.user_authorised_person_id')
        ->where('has_access_login.isactive',1)
        ->where('user_authorised_person.email',$email)
        ->where('has_access_login.invitation_status',1)
        ->select('user_authorised_person.*','users.first_name as ufirstname','users.last_name as ulastname','users.email as uemail')
        
        ->get();
    }
    

}