<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Coupons extends Model
{
    protected $table = 'coupons';
    protected $primaryKey = 'coupon_id';

    protected $fillable = [
        'coupon_id',
		'stripe_coupon_id',
		'object',
		'amount_off',
		'created',
		'currency',
		'duration',
		'livemode',
		'max_redemptions',
		'metadata',
		'name',
		'percent_off',
		'redeem_by',
		'times_redeemed',
		'valid',
		'type',
		'created_at',
        'created_by',
        'updated_at',
        'updated_by'

    ];




    public static function GetActive()
    {

        return Coupons::where('coupons.valid','=',1)->get();

    }



    





    

}