<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class MessageScheduler extends Model
{
    protected $table = 'message_scheduler';
    protected $primaryKey = 'message_scheduler_id';

    protected $fillable = [

        'message_scheduler_id',
        'message_id',
		'message_token_id',
		'message_type',
		'authorisation_type',
		'user_recipient_id',
		'user_id',
		'date',
		'created_at',
		'updated_at',
		'dispatch_date',
		'status',
		'email_scheduler_id',
		'check_in_setting_id',
		'delivery_type',
		'delivery_date',
		'recipient_url',
		'thirdperson_url',
		'user_authorised_person_id',
		'qrid',
		'sendinblue_message_id'
		



    ];




}