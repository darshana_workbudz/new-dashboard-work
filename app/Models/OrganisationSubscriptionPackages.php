<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class OrganisationSubscriptionPackages extends Model
{
    protected $table = 'organisation_subscription_packages';
    protected $primaryKey = 'organisation_subscription_package_id';

    protected $fillable = [

        'organisation_subscription_package_id',
		'user_subscription_package_id',
		'user_id',
		'package_id',
		'stripe_subscription_id',
		'stripe_customer_id',
		'package_code',
		'created_by',
		'created_at',
		'updated_at',
		'updated_by',
		'shared_to_user',
		'invitation_status'




    ];


    

     public static function FindByStripe($stripe_subscription_id)
    {

    	return OrganisationSubscriptionPackages::where('stripe_subscription_id',$stripe_subscription_id)->first();

    }
    

}