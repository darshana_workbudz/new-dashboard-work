<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class UserReceipts extends Model
{
    protected $table = 'user_receipts';
    protected $primaryKey = 'user_receipt_id';

    protected $fillable = [

        "user_receipt_id",
		"receipt_number",
		"receipt_url",
		"receipt_email",
		"stripe_customer_id",
		"user_id",
		"amount",
		"amount_charged",
		"amount_refunded",
		"customer_address",
		"customer_email",
		"customer_name",
		"customer_phone",
		"payment_intent",
		"paid",
		"status",
		"created_at",
		"updated_at",
		"stripe_charge_id"


    ];

    

    

    public static function GetByUser($user_id)
    {

    	return UserReceipts::where('user_id',$user_id)->get();

    }
    

}