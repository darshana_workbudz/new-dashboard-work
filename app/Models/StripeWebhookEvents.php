<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class StripeWebhookEvents extends Model
{
    protected $table = 'stripewebhookevent';
    protected $primaryKey = 'stripewebhookevent_id';
    public $timestamps = false;
    protected $fillable = [
    
			'stripewebhookevent_id',
			'jobstatus',
			'eventjson',
			'created_at',
			'eventtype',
			'event_id'
        
    ];





    

    

}