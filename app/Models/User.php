<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Session;
use DB;
use Carbon\Carbon;
use Spatie\Permission\Traits\HasRoles;
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'phone_code',
        'phone_number',
        'dob',
        'email',
        'email_verified_at',
        'address_1',
        'address_2',
        'address_3',
        'postcode',
        'country',
        'reason',
        'isorganisation',
        'organisation_name',
        'organisation_code',
        'organisation_role',
        'password',
        'remember_token',
        'isactive',
        'user_stage_id',
        'user_role_type',
        'sib_contact_id',
        'stripe_customer_id',
        'hasdemopackage',
        'verification_link',
        'verification_expiry_time',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by',
        'invited_by',
        'invitation_status',
        'nominated_authorised_person_access',
        'ziggeo_access_rights',
        'ziggeo_access_token',
        'ziggeo_access_session_limit',
        'ziggeo_access_token_expiry',
        'ziggeo_access_token_created',
        'mystatus',
        'state',
        'organisation_department',
        'other_role',
        'other_mystatus',
        'other_reason',
        'default_prefrence',
        'stripe_subscription_id',
        'usercheckinstatus',
        'demopackage_email_sent'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public static function CheckEligibleForverification($email)
    {
        $userdata=User::where('email',$email)->first();

        if(isset($userdata) && !empty($userdata)):

            if($userdata->isactive==0 || is_null($userdata->isactive)):
                
                $userdata->email_verified_at=Carbon::now();
                $userdata->user_stage_id=2;
                $userdata->save();

                if(strtotime(date('Y-m-d H:i:s'))<strtotime(date($userdata->verification_expiry_time)) || !empty($userdata->email_verified_at)):

                    return ['status'=>'success'];

                else:

                    return ['status'=>'fail','event'=>'linkexpired','message'=>'Sorry link expired please try again'];

                endif;

            else:

                    return ['status'=>'fail','event'=>'active','message'=>'Sorry your account is already active please login to continue']; 

            endif;

            else:

                return ['status'=>'fail','event'=>'invaliduser','message'=>'Sorry no user found for following email'];

        endif;
    }

    public static function CheckEligibleForMemberVerificationRegistration($email)
    {
        $userdata=User::where('email',$email)->first();

        if(isset($userdata) && !empty($userdata)):

            if($userdata->isactive==1 && ($userdata->user_role_type==4 || $userdata->user_role_type==3)):


                 return ['status'=>'success','userdata'=>$userdata];

                
            endif;    

        endif;

        return ['status'=>'fail','event'=>'linkexpired','message'=>'Sorry link expired please try again'];
    }

    public static function CheckEligibleForClientVerificationRegistration($email)
    {
        $userdata=User::where('email',$email)->first();

        if(isset($userdata) && !empty($userdata)):

            if($userdata->isactive==1 && ($userdata->user_role_type==4 || $userdata->user_role_type==3 || $userdata->user_role_type==5)):


                 return ['status'=>'success','userdata'=>$userdata];

                
            endif;    

        endif;

        return ['status'=>'fail','event'=>'linkexpired','message'=>'Sorry link expired please try again'];
    }

    public static function CheckEligibleForPartnerVerificationRegistration($email)
    {
        $userdata=User::where('email',$email)->first();

        if(isset($userdata) && !empty($userdata)):

            if($userdata->isactive==1 && ($userdata->user_role_type==1)):


                 return ['status'=>'success','userdata'=>$userdata];

                
            endif;    

        endif;

        return ['status'=>'fail','event'=>'linkexpired','message'=>'Sorry link expired please try again'];
    }

    public static function GetAuthID($userdata)
    {
        return $userdata->hasRole(['Admin']) ? null : $userdata->id; 
    }


    public static function CheckEligibleUserId($userdata,$user_id)
    {

        return $userdata->hasRole(['Admin']) || $userdata->id==$user_id ? true : false; 
    }

    public static function FindByStripe($stripe_customer_id)
    {
       return $userdata=User::where('stripe_customer_id',$stripe_customer_id)->first();
    }

    public static function FindBySendinBlue($sib_contact_id)
    {
       return $userdata=User::where('sib_contact_id',$sib_contact_id)->first();
    }


    public static function GetTotalResource($userid)
    {
       $totalminutes=0;
       $totalorgminutes=0;
       $totalstandaloneminutes=0;

        $user=User::find($userid);

        if($user->hasRole('Client')):

               $usersubscription=UserPackages::where('user_id',$userid)->where('status',1)->orderBy('user_package_id','DESC')->take(1)->get();
               
               if(isset($usersubscription) && !empty($usersubscription)):

                    foreach ($usersubscription as $uskey => $usvalue) {

                        $totalsubscriptionminutes=isset($usvalue->message_value) && !empty($usvalue->message_value) ? $usvalue->message_value :0;

                        $useraddons=UserAddonPackages::where('status',1)
                        ->where('user_id',$userid)
                        ->where('stripe_subscription_id',$usvalue->stripe_subscription_id)
                        ->selectRaw('sum(message_value) as message_value')
                        ->GroupBy('user_id')
                        ->first();

                        $totaladdonminutes=isset($useraddons) && !empty($useraddons) ? $useraddons->message_value : 0;

                        $overalltotalsubscriptionminutes=$totaladdonminutes+$totalsubscriptionminutes;

                        $totalminutes=$totalminutes+$overalltotalsubscriptionminutes;
                    }

               endif;


               

               

        endif;

        $overalltotalminutes=$totalminutes;

       return $overalltotalminutes;

    }


    public static function GetQrCodeAddon($userid)
    {
        $data=UserAddonPackages::where('user_addon_packages.status',1)
                        ->where('user_addon_packages.user_id',$userid)
                        ->join('packages','user_addon_packages.package_id','packages.package_id')
                        ->where('packages.slug','addon-qr')
                        ->whereNull('user_addon_packages.linked_message_id')
                        ->whereNull('user_addon_packages.linked_message_type')
                        ->select('user_addon_packages.*')
                        ->get();

        return $data;

        

    }

    
    public static function FindQrCodeAddon($userid)
    {
        $data=UserAddonPackages::where('user_addon_packages.status',1)
                        ->where('user_addon_packages.user_id',$userid)
                        ->join('packages','user_addon_packages.package_id','packages.package_id')
                        ->where('packages.slug','addon-qr')
                        ->whereNull('user_addon_packages.linked_message_id')
                        ->whereNull('user_addon_packages.linked_message_type')
                        ->select('user_addon_packages.*')
                        ->first();

        return $data;

        

    }


    public static function GetQrCodeStandalone()
    {
        $data=Packages::where('packages.slug','standalone-qr')
                        ->where('type',3)
                        ->get();

        if(isset($data) && !empty($data)):
            foreach($data as $datavalue):

                        $datavalue->inclusions=DB::table('package_inclusions')
                       ->where('package_inclusions.package_id',$datavalue->package_id)
                       ->orderBy('package_inclusions.package_id', 'asc')     
                       ->orderBy('package_inclusions.order_no', 'asc')
                       ->get()->toArray();
                   

            
            endforeach;
         endif;               

         return $data;
    }

    
    public static function GetHanddeliveredAddon($userid)
    {
        $data=UserAddonPackages::where('user_addon_packages.status',1)
                        ->where('user_addon_packages.user_id',$userid)
                        ->join('packages','user_addon_packages.package_id','packages.package_id')
                        ->where('packages.slug','addon-heirloom')
                        ->whereNull('user_addon_packages.linked_message_id')
                        ->whereNull('user_addon_packages.linked_message_type')
                        ->select('user_addon_packages.*')
                        ->get();

       return $data;

        

    }

    


    public static function FindHanddeliveredAddon($userid)
    {
        $data=UserAddonPackages::where('user_addon_packages.status',1)
                        ->where('user_addon_packages.user_id',$userid)
                        ->join('packages','user_addon_packages.package_id','packages.package_id')
                        ->where('packages.slug','addon-heirloom')
                        ->whereNull('user_addon_packages.linked_message_id')
                        ->whereNull('user_addon_packages.linked_message_type')
                        ->select('user_addon_packages.*')
                        ->first();

       return $data;

        

    }


    public static function GetHanddeliveredStandalone()
    {
         $data=Packages::where('packages.slug','standalone-heirloom')
                        ->where('type',3)
                        ->get();

         if(isset($data) && !empty($data)):
            foreach($data as $datavalue):

                        $datavalue->inclusions=DB::table('package_inclusions')
                       ->where('package_inclusions.package_id',$datavalue->package_id)
                       ->orderBy('package_inclusions.package_id', 'asc')     
                       ->orderBy('package_inclusions.order_no', 'asc')
                       ->get()->toArray();
                   

            
            endforeach;
         endif;               

         return $data;
     }



     public static function GetTotalMessagesCount($userid)
    {
        $audiodata=ZiggeoUserAudios::where('user_id',$userid)->count();

        $videodata=ZiggeoUserVideos::where('user_id',$userid)->count();

        $pagedata=ZiggeoUserTexts::where('user_id',$userid)->count();

        $totalcount=$audiodata+$videodata+$pagedata;

        return $totalcount;
    }
   



    public static function GetUsedMinutes($userid)
    {
        $audiodata=ZiggeoUserAudios::where('user_id',$userid)->selectRaw('sum(duration) as audioduration')->GroupBy('user_id')->first();

        $videodata=ZiggeoUserVideos::where('user_id',$userid)->selectRaw('sum(duration) as videoduration')->GroupBy('user_id')->first();

        $audioduration=isset($audiodata) && !empty($audiodata) ? $audiodata->audioduration/60 : 0;
        $videoduration=isset($videodata) && !empty($videodata) ? $videodata->videoduration/60 : 0;

        return round($audioduration+$videoduration,2);
    }


    public static function GetUsedDocLimit($userid)
    {
        $pagecount=ZiggeoUserTexts::where('user_id',$userid)->selectRaw('sum(pagecount) as pagecount')->GroupBy('user_id')->first();

        $pagecount=isset($pagecount) && !empty($pagecount) ? (int) $pagecount->pagecount : 0;
        return $pagecount;
    }


    public static function GetTotalDocLimit($userid)
    {

       $totalpagecount=0;
       $totalorgpagecount=0;
       $totalstandalonepagecount=0;

        $user=User::find($userid);

        if($user->hasRole('Client')):

               $usersubscription=UserPackages::where('user_id',$userid)->where('status',1)->orderBy('user_package_id','DESC')->take(1)->get();
               
               if(isset($usersubscription) && !empty($usersubscription)):

                    foreach ($usersubscription as $uskey => $usvalue) {

                        $totalsubscriptionpagecount=isset($usvalue->written_value) && !empty($usvalue->written_value) ? $usvalue->written_value :0;

                        $useraddons=UserAddonPackages::where('status',1)
                        ->where('user_id',$userid)
                        ->where('stripe_subscription_id',$usvalue->stripe_subscription_id)
                        ->selectRaw('sum(written_value) as written_value')
                        ->GroupBy('user_id')
                        ->first();

                        $totaladdonpagecount=isset($useraddons) && !empty($useraddons) ? $useraddons->written_value : 0;

                        $overalltotalsubscriptionpagecount=$totaladdonpagecount+$totalsubscriptionpagecount;

                        $totalpagecount=$totalpagecount+$overalltotalsubscriptionpagecount;
                    }

               endif;
        endif;       

              


       $overalltotalpagecount=$totalpagecount;

       return $overalltotalpagecount;
        
    }



    public static function GetUsedRecipientsLimit($userid)
    {
        return MyRecipients::where('user_id',$userid)->count();
    }


    public static function GetTotalRecipients($userid)
    {
       $totalrecipients=0;
       $totalorgrecipients=0;
       $totalstandalonerecipients=0;

        $user=User::find($userid);

        if($user->hasRole('Client')):

               $usersubscription=UserPackages::where('user_id',$userid)->where('status',1)->orderBy('user_package_id','DESC')->take(1)->get();
               
               if(isset($usersubscription) && !empty($usersubscription)):

                    foreach ($usersubscription as $uskey => $usvalue) {

                        $totalsubscriptionrecipients=isset($usvalue->recipient_value) && !empty($usvalue->recipient_value) ? $usvalue->recipient_value :0;

                        $useraddons=UserAddonPackages::where('status',1)
                        ->where('user_id',$userid)
                        ->where('stripe_subscription_id',$usvalue->stripe_subscription_id)
                        ->selectRaw('sum(recipient_value) as recipient_value')
                        ->GroupBy('user_id')
                        ->first();

                        $totaladdonrecipients=isset($useraddons) && !empty($useraddons) ? $useraddons->recipient_value : 0;

                        $overalltotalsubscriptionrecipients=$totaladdonrecipients+$totalsubscriptionrecipients;

                        $totalrecipients=$totalrecipients+$overalltotalsubscriptionrecipients;
                    }

               endif;

        endif;       
              

       
       $overalltotalrecipients=$totalrecipients;

       return $overalltotalrecipients;

    }


    public static function GetOurPartners()
    {
        return $userdata=User::where('user_role_type',4)
        ->leftjoin('user_members','user_members.user_id','users.id')
        ->leftjoin('user_clients','user_members.user_id','user_clients.user_id')
        ->select('users.*',DB::raw('Count("distinct user_members.user_id") as membercount'),DB::raw('Count("distinct user_clients.user_id") as clientcount'))
        ->groupBy('users.id')
        ->get();

    }

    public static function GetOurB2CUserClients()
    {
        return $userdata=User::where('user_role_type',2)
        ->select('users.*')
        ->paginate(10);

    }

    

    public static function GetOurB2BUsers()
    {
        return $userdata=User::where('user_role_type',3)
        ->leftjoin('user_members','user_members.user_id','users.id')
        ->leftjoin('user_clients','user_members.user_id','user_clients.user_id')
        ->select('users.*',DB::raw('Count("distinct user_members.user_id") as membercount'),DB::raw('Count("distinct user_clients.user_id") as clientcount'))
        ->groupBy('users.id')
        ->get();

    }



}

