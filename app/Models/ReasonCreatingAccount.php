<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class ReasonCreatingAccount  extends Model
{
    protected $table = 'reason_creating_account';
    protected $primaryKey = 'reason_id';

    protected $fillable = [
    
        'reason_id',
        'description',
	'isactive'
        
    ];


    
	public static function GetActive()
	{
		return ReasonCreatingAccount::where('isactive',1)->get();
	}
    

}