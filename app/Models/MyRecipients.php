<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class MyRecipients extends Model
{
    protected $table = 'user_recipients';
    protected $primaryKey = 'user_recipient_id';

    protected $fillable = [
        'user_recipient_id',
        'user_authorised_person_id',
        'user_id',
        'first_name',
        'last_name',
        'email',
        'phone_code',
        'phone_number',
        'address_1',
        'address_2',
        'address_3',
        'postcode',
        'country',
        'status',
        'verification_process_step_id',
        'sib_contact_id',
        'relationship_id',
        'other_relationship',
        'recieve_message_by',
        'require_verification',
        'password',
        'verification_question_1',
        'verification_answer_1',
        'verification_question_2',
        'verification_answer_2',
        'verification_question_3',
        'verification_answer_3',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'accept_recipient_responsibility',
        'accept_authorised_person_responsibility',
        'state',
        'message_access_code',
        'code_expiry_at'
    ];




    public static function search($firstname,$lastname,$email,$phone_number,$status,$authid=null)
    {

        return MyRecipients::leftjoin('relationship','user_recipients.relationship_id','relationship.relationship_id')
        ->where(function($query) use ($firstname,$lastname,$email,$phone_number,$status,$authid)
        {
                    if(isset($firstname) && !empty($firstname)):
                    $query->where('user_recipients.first_name','=',$firstname);
                    endif;
                    if(isset($lastname) && !empty($lastname)):
                    $query->where('user_recipients.last_name','=',$lastname);
                    endif;
                    if(isset($email) && !empty($email)):
                    $query->where('user_recipients.email','=',$email);
                    endif;
                    if(isset($phone_number) && !empty($phone_number)):
                    $query->where('user_recipients.phone_number','=',$phone_number);
                    endif;  
                    if(isset($status) && !empty($status)):
                    $query->where('user_recipients.status','=',$status);
                    endif; 
                    if(isset($authid) && !empty($authid)):
                    $query->where('user_recipients.user_id','=',$authid);
                    endif;   

        })
        ->select('user_recipients.*','relationship.relationship_name')
        ->OrderBy('user_recipients.user_recipient_id','desc')
        ->paginate(5);

    }

    public static function GetActive($authid=null)
    {

    	return MyRecipients::leftjoin('relationship','user_recipients.relationship_id','relationship.relationship_id')
        ->where(function($query) use ($authid){

                    if(isset($authid) && !empty($authid)):
                    $query->where('user_recipients.user_id','=',$authid);
                    endif;
        })
        ->where('user_recipients.status',1)
        ->select('user_recipients.*','relationship.relationship_name')
        ->OrderBy('user_recipients.user_recipient_id','Desc')
        ->get();

    }

    public static function All($authid=null)
    {

        return MyRecipients::leftjoin('relationship','user_recipients.relationship_id','relationship.relationship_id')
        ->where(function($query) use ($authid){

                    if(isset($authid) && !empty($authid)):
                    $query->where('user_recipients.user_id','=',$authid);
                    endif;

        })
        ->select('user_recipients.*','relationship.relationship_name')
        ->OrderBy('user_recipients.user_recipient_id','Desc')
        ->paginate(5);

    }

    public static function AllWithoutPaginate($authid=null)
    {

        return MyRecipients::leftjoin('relationship','user_recipients.relationship_id','relationship.relationship_id')
        ->where(function($query) use ($authid){

                    if(isset($authid) && !empty($authid)):
                    $query->where('user_recipients.user_id','=',$authid);
                    endif;

        })
        ->select('user_recipients.*','relationship.relationship_name')
        ->OrderBy('user_recipients.user_recipient_id','Desc')
        ->get();

    }

    public static function CheckEligible($authid,$reciepientid)
    {

        return MyRecipients::where(function($query) use ($authid,$reciepientid){

                    if(isset($authid) && !empty($authid)):
                    $query->where('user_recipients.user_id','=',$authid);
                    endif;

                    $query->where('user_recipients.user_recipient_id','=',$reciepientid);
        })
        ->first();

    }

    public static function GetActiveAll($authid)
    {

        return MyRecipients::where(function($query) use ($authid){

                    if(isset($authid) && !empty($authid)):
                    $query->where('user_recipients.user_id','=',$authid);
                    endif;
                    $query->where('user_recipients.status','=',1);
                    
        })
        ->get();

    }





    

}