<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class ZiggeoUserTexts extends Model
{
    protected $table = 'ziggeo_user_texts';
    protected $primaryKey = 'ziggeo_user_text_id';
    public $timestamps = false;
    protected $fillable = [
    
	
			'ziggeo_user_text_id',
			'user_id',
			'title',
			'descriptioncontenthtml',
			'descriptioncontent',
			'capture_type',
			'path',
			'uploadedfilename',
			'created_at',
			'created_by',
			'updated_at',
			'updated_by',
			'pagecount',
			'charactercount',
			'user_recipient_id',
			'delivery_style',
			'delivery_date',
			'delivery_type',
			'authorisation_type',
			'preference_completed',
			'qr_package_type',
			'qr_package_id',
			'qrid',
			'heilroom_package_type',
			'heilroom_package_id',
			'delivery_status',
			'text_token_id',
            'isclone',
            'contenttype',
            'qrdeliverystyle',
            'filetype',
            'is_sent'


        
    ];



    public static function GetTextMessageByUserId($user_id,$contenttype=null)
    {
    	return $data=ZiggeoUserTexts::leftjoin('user_recipients','user_recipients.user_recipient_id','ziggeo_user_texts.user_recipient_id')
    	->where('ziggeo_user_texts.user_id',$user_id)
        ->where(function($query) use ($contenttype)
        {
            if(isset($contenttype) && !empty($contenttype) && $contenttype=='qrcode-content'):
                 $query->where('contenttype','qrcode-content')->orwhereNotNull('qr_package_id');
            endif;

            if(isset($contenttype) && !empty($contenttype) && $contenttype=='heirloom-content'):
                $query->where('contenttype','heirloom-content')->orwhereNotNull('heilroom_package_id');
            endif;
        })
        ->select('ziggeo_user_texts.*','user_recipients.first_name','user_recipients.last_name')
        ->orderBy('ziggeo_user_texts.user_recipient_id','asc')
        ->orderBy('ziggeo_user_texts.preference_completed','asc')
        ->get();
    }


    public  static function FindByTokenId($user_id,$token)
    {
        return $data=ZiggeoUserTexts::where('ziggeo_user_texts.user_id',$user_id)
        ->where('text_token_id',$token)
        ->select('ziggeo_user_texts.ziggeo_user_text_id as id')
        ->orderBy('ziggeo_user_texts.ziggeo_user_text_id','Desc')
        ->first();
    }

    public  static function FindByMessageId($token)
    {
        return $data=ZiggeoUserTexts::where('ziggeo_user_text_id',$token)
        ->join('users','users.id','ziggeo_user_texts.user_id')
        ->leftjoin('user_recipients','user_recipients.user_recipient_id','ziggeo_user_texts.user_recipient_id')
        ->leftjoin('user_authorised_person','user_authorised_person.user_id','ziggeo_user_texts.user_id')
        ->select('ziggeo_user_texts.*',
        	'users.first_name','users.last_name','users.email',
            'user_authorised_person.first_name as afirst_name',
            'user_authorised_person.last_name as alast_name',
            'user_authorised_person.email as aemail',
            'user_recipients.first_name as rfirst_name',
            'user_recipients.last_name as rlast_name',
            'user_recipients.email as remail',
            'user_recipients.message_access_code as rmessage_access_code',
            'user_recipients.code_expiry_at as rcode_expiry_at',
            'user_authorised_person.verification_process_step_id as averification_process_step_id',
            'user_authorised_person.verification_question_1 as averification_question_1',
            'user_authorised_person.verification_answer_1 as averification_answer_1',
            'user_authorised_person.verification_question_2 as averification_question_2',
            'user_authorised_person.verification_answer_2 as averification_answer_2',
            'user_authorised_person.verification_question_3 as averification_question_3',
            'user_authorised_person.verification_answer_3 as averification_answer_3',
            'user_authorised_person.password as apassword',
            'user_authorised_person.message_access_code as amessage_access_code',
            'user_authorised_person.code_expiry_at as acode_expiry_at',
            'user_recipients.verification_process_step_id as rverification_process_step_id',
            'user_recipients.verification_question_1 as rverification_question_1',
            'user_recipients.verification_answer_1 as rverification_answer_1',
            'user_recipients.verification_question_2 as rverification_question_2',
            'user_recipients.verification_answer_2 as rverification_answer_2',
            'user_recipients.verification_question_3 as rverification_question_3',
            'user_recipients.verification_answer_3 as rverification_answer_3',
            'user_recipients.password as rpassword',
            'user_authorised_person_id',
            'ziggeo_user_texts.text_token_id as token'

        )
        ->first();
        
    }

    
    public static function FindTextMessageByUserId($user_id,$id)
    {
    	return $data=ZiggeoUserTexts::leftjoin('user_recipients','user_recipients.user_recipient_id','ziggeo_user_texts.user_recipient_id')
    	->where('ziggeo_user_texts.user_id',$user_id)->where('ziggeo_user_text_id',$id)
    	->select('ziggeo_user_texts.*','user_recipients.first_name','user_recipients.last_name')
    	->first();
    }	

    

}