<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class FailedResponseAction extends Model
{
    protected $table = 'failed_response_action';
    protected $primaryKey = 'failed_response_action_id';

    protected $fillable = [
        'failed_response_action_id',
        'description'
       
    ];



    

}