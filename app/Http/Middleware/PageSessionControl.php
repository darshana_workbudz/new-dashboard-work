<?php

namespace App\Http\Middleware;
use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\DB;
use Route;
use Auth;
use App\Models\User;
use Session;

class PageSessionControl
{
    
    public function handle($request, Closure $next)
    {
    	$isajax=$request->ajax;
    	$route=Route::currentRouteName();
    	
        $method = $request->method();
        $urlarray=array();
    	if($method=='GET'):
            $urlarray=Session::get('urlarray');

            if(isset($urlarray) && !empty($urlarray)):
                
            if(end($urlarray)!==\URL::full()):
                $urlarray[]=\URL::full();
            endif;

            else:
                $urlarray[]=\URL::full();
            endif;
            

            $urlarraysession=Session::put('urlarray',$urlarray);
            $previouspageurl=Session::get('activepageurl');
            
            if(isset($previouspageurl) && !empty($previouspageurl)):
                Session::put('previouspageurl',$previouspageurl);
            endif;

            
    		Session::put('activeroute',$route);
            $activepageurl = end($urlarray);
    		Session::put('activepageurl',$activepageurl);

	    	if(!Auth::user()):


	    			if($request->is('dashboard/*') && $isajax==true)
	    			{
	    				$data['sessionexpired']=true;
	    				$data['isajax']=true;
	    				$html = view('auth.login',$data)->render();
						return response()->json(['status'=>'success','html'=>$html]);
	    			}


	    	endif;
    	endif;

    	return $next($request);
    }

}