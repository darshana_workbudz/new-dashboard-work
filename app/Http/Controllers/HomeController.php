<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use SendinBlue;
use GuzzleHttp;
use App\Models\User;
use App\Models\Packages;
use App\Models\UserPackages;
use App\Models\UserOrganisation;
use App\Models\UserAuthorizedPerson;
use App\Models\HasAccessLogin;
use App\Models\CheckoutSessionData;
use App\Models\ReasonCreatingAccount;
use App\Models\EmailScheduler;
use Carbon\Carbon;
use DB;
use Hash;
use Session;
use App\Http\Controllers\SendInBlueController;
use Auth;
use App\Rules\NameRule;
use App\Models\MyStatus;
use App\Models\OrganisationDepartment;
use App\Models\OrganisationRoles;
use App\Models\ZiggeoUserVideos;
use App\Models\ZiggeoUserAudios;
use App\Models\ZiggeoUserTexts;
use App\Models\VerficationQuestions;
use App\Models\UserMembers;
use App\Models\UserClients;
use App\Models\FollowupPeriod;
use App\Models\CheckInSetting;
use App\Models\MyRecipients;
use Ziggeo;
use \Stripe\Stripe;
use Redirect;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
        $isajax = $request->ajax;
        $data['isajax'] = $isajax;
        
        if ($isajax == 'true') :
            return response()->json(['status' => 'success', 'event' => 'redirect', 'routetoredirect' => route('login')]);
            $html = view('maintenance.index', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);

        else :
            return redirect('login');
            return view('maintenance.index', $data);
        endif;
    }

    public function login(Request $request)
    {
        $user = Auth::id();
        $isajax = $request->ajax;
        $data['isajax'] = $isajax;

        if ($isajax == 'true') :

            $html = view('auth.login', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);

        else :
            return view('auth.login', $data);
        endif;
    }


    // Verification Process ---------Start

    public function VerificationProcess(Request $request)
    {
        $isajax = $request->ajax;
        $data['isajax'] = $isajax;
        $data['success'] = false;
        $data['old'] = $request;
        
        if(isset($request->package) && !empty($request->package)):
            Session::put('flowpackageid',$request->package);
            $flowpackageid=$request->package;
        else:
            Session::forget('flowpackageid');
        endif;
        

       
        if(isset($flowpackageid) && !empty($flowpackageid)):

                    $packages=Packages::where('package_id',$flowpackageid)->where('demopackage','!=','1')->whereIn('type',[1,3])->first();

                    if(isset($packages) && !empty($packages)):

                            $sessionarray=array();

                            $sessionarray['line_items']=array();
                            $sessionarray['success_url']=route('verification-process-step');
                            $sessionarray['cancel_url']=route('stripe-cancel-page').'?session_id={CHECKOUT_SESSION_ID}&package='.$packages->package_id;
                            $sessionarray['payment_method_types']=['card'];
                
                            $sessionarray['mode']='payment';
                            
                            
                            $sessionarray['allow_promotion_codes']=true;
                            $line_items=array();
                            
                            $line_items['price']=$packages->stripe_price_id;
                            $line_items['quantity']=1;
                            $sessionarray['line_items'][]=$line_items;

                            Stripe::setApiKey(env('API_KEY_STRIPE'));
                            $checkout_session = \Stripe\Checkout\Session::create($sessionarray);
                            
                            if(null!==$checkout_session['id'] && !empty($checkout_session['id'])):

                             header("HTTP/1.1 303 See Other");
                             header('Location: '.$checkout_session['url']);
                
                            endif;

                    endif;

        endif;                                                                

        
        
        if ($isajax == 'true') :
            
            $html = view('auth.verification', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);

        else :
            return view('auth.verification', $data);
        endif;
    }


     public function VerificationProcessStep(Request $request)
    {
        $user = Auth::id();
        $isajax = $request->ajax;
        $data['isajax'] = $isajax;
        $data['success'] = true;
        $data['payment'] = true;
        if ($isajax == 'true') :

             $html = view('layouts-frontend.success-page', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);

        else :
            return view('layouts-frontend.success-page', $data);
        endif;
    }


    public function SubmitForVerification(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        $email = $request->email;
        $isajax = $request->ajax;
        $data['success'] = false;
        $data['isajax'] = $isajax;
        $data['old'] = $request;

        if ($validator->fails()) :

            $data['error'] = $validator->getMessageBag()->toArray();

            $html = view('auth.verification', $data)->render();

            return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);

        endif;

        try {

            DB::beginTransaction();

            

            $userdata=User::where('email',$email)->first();

            if(!empty($userdata) && $userdata->isactive>0):

                $validator->errors()->add('email', 'The email has already been taken');

                $data['error'] = $validator->getMessageBag()->toArray();
                
                $html = view('auth.verification', $data)->render();

                return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);

            endif;

            $newcreated=false;

            if(!$userdata):

            $url = $this->GenerateVerificationlink(uniqid().'-'.$email);

            $userdata = User::create([

                'email' => $email,
                'password' => Hash::make('EVAHELDUNKNOWN'),
                'isactive' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'user_stage_id' => 1,
                'verification_link' => $url,
                'verification_expiry_time' => Carbon::now()->addHours(24)->format('Y-m-d H:i:s')
                
            ]);

            $newcreated=true;

            endif;

            if ($userdata) :

                if(strtotime($userdata->verification_expiry_time)<=strtotime(date('Y-m-d H:i:s'))):
                    
                    $url = $this->GenerateVerificationlink(uniqid().'-'.$email);
                    $userdata->verification_link=$url;
                    $userdata->verification_expiry_time= Carbon::now()->addHours(24)->format('Y-m-d H:i:s');
                else:
                    $url = $userdata->verification_link;
                endif;

                $params = array(
                    'url' => $url,
                    'subject' => 'Activate Account'
                );
                
                SendInBlueController::SendTransactionEmail((int) env('EMAIL_VERIFICATION_TEMPLATE'),$email,$params);

                $sib_contact_id=null;
                $contactcreated = SendInBlueController::CreateContact($email);

                if(isset($contactcreated['code']) && $contactcreated['code']==200):
                
                    $sib_contact_id=$contactcreated['response']['id'];

                endif;

                if(isset($contactcreated['code']) && $contactcreated['code']!==200) :

                    if($contactcreated['response']['code']=='duplicate_parameter'):

                    $contactdetails=SendInBlueController::GetContactDetails($email);

                        if(isset($contactdetails['code']) && $contactdetails['code']==200):
                            $sib_contact_id=$contactdetails['response']['id'];
                        endif;

                    endif;

                endif;

                
                
                $userdata->sib_contact_id = $sib_contact_id;
                $userdata->save();
                
                
                
                $data['success'] = true;
                $html = view('layouts-frontend.success-page', $data)->render();

                DB::commit();

                return response()->json(['status' => 'success', 'event' => 'refresh', 'html'=> $html]);

            endif;

        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }




        return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);
    }

    function GenerateVerificationlink($email)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_encrypt($email, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);

        $url = URL('verify-account') . "?key=" . $output;

        return $url;
    }

    function Encrypt($text)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_encrypt($text, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }


    function Decrypt($text)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_decrypt(base64_decode($text), $encrypt_method, $key, 0, $iv);
        return $output;
    }

    // Verification Process ---------End



    // Registeration Form Process ---------Start


    function InviteMember(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [
            'email' => 'unique:users,email'
            ]);


            if($validator->fails()) :

                

            endif;




           
        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }
    }


    function InviteClient(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [
            'email' => 'unique:users,email'
            ]);



            if($validator->fails()) :


            endif;





           
        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }
    }


    function VerifyRegisteration(Request $request)
    {
        
        try {

            $key = $request->key;

            if (isset($key) && !empty($key)) :

                $key = $this->Decrypt($key);
                $keyexplode=explode('-',$key);
                $email=isset($keyexplode[1]) && !empty($keyexplode[1]) ? $keyexplode[1] : '';
                DB::beginTransaction();
                $eligible = User::CheckEligibleForverification($email);
                DB::commit();

                $request->email = $email;


                $data['phonecodes'] = DB::table('tbl_country')->orderBy('name','asc')->get();
                if ($eligible['status'] == 'success') :

                    Session::put('verificationemail', $email);

                    $isajax = $request->ajax;
                    $data['isajax'] = $isajax;
                    $data['email'] = $email;
                    $data['old'] = $request;
                    $data['reason'] = ReasonCreatingAccount::GetActive();
                    $data['mystatus'] = MyStatus::GetActive();
                    $data['organisation_department'] = OrganisationDepartment::GetActive();
                    $data['organisation_roles'] = OrganisationRoles::GetActive();

                    if ($isajax == 'true') :
                        $html = view('auth.register', $data)->render();
                        return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);
                    else :
                        return view('auth.register', $data);
                    endif;

                else :

                    if ($eligible['event'] == 'linkexpired') :
                        return redirect()->route('login')->with('alert', $eligible['message']);
                    elseif ($eligible['event'] == 'active') :
                        return redirect()->route('login')->with('alert', $eligible['message']);
                    elseif ($eligible['event'] == 'invaliduser') :
                        return redirect()->route('verification-process')->with('alert', $eligible['message']);
                    endif;


                endif;

            endif;
        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }
    }


    function VerifyRegisterationMember(Request $request)
    {

        try {

            $key = $request->key;

            if (isset($key) && !empty($key)) :

                $key = $this->Decrypt($key);

                $keyexplode=explode('-',$key);
                $email=isset($keyexplode[0]) && !empty($keyexplode[0]) ? $keyexplode[0] : '';
                DB::beginTransaction();
                $eligible = User::CheckEligibleForMemberVerificationRegistration($email);
                DB::commit();

                $request->email = $email;


                $data['phonecodes'] = DB::table('tbl_country')->orderBy('name','asc')->get();
                if ($eligible['status'] == 'success') :

                    Session::put('verificationemail', $email);

                    $isajax = $request->ajax;
                    $data['isajax'] = $isajax;
                    $data['userdata'] = $eligible['userdata'];
                    $data['old'] = $request;
                    $data['reason'] = ReasonCreatingAccount::GetActive();
                    $data['mystatus'] = MyStatus::GetActive();
                    $data['organisation_department'] = OrganisationDepartment::GetActive();
                    $data['organisation_roles'] = OrganisationRoles::GetActive();

                    if ($isajax == 'true') :
                        $html = view('our-members.registration', $data)->render();
                        return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);
                    else :
                        return view('our-members.registration', $data);
                    endif;

                else :

                    if ($eligible['event'] == 'linkexpired') :
                        return redirect()->route('login')->with('alert', $eligible['message']);
                    elseif ($eligible['event'] == 'active') :
                        return redirect()->route('login')->with('alert', $eligible['message']);
                    elseif ($eligible['event'] == 'invaliduser') :
                        return redirect()->route('verification-process')->with('alert', $eligible['message']);
                    endif;


                endif;

            endif;
        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }
    }


    function VerifyRegisterationPartner(Request $request)
    {

        try {

            $key = $request->key;

            if (isset($key) && !empty($key)) :

                $key = $this->Decrypt($key);

                $keyexplode=explode('-',$key);
                $email=isset($keyexplode[0]) && !empty($keyexplode[0]) ? $keyexplode[0] : '';
                DB::beginTransaction();
                $eligible = User::CheckEligibleForPartnerVerificationRegistration($email);
                DB::commit();

                $request->email = $email;


                $data['phonecodes'] = DB::table('tbl_country')->orderBy('name','asc')->get();
                if ($eligible['status'] == 'success') :

                    Session::put('verificationemail', $email);

                    $isajax = $request->ajax;
                    $data['isajax'] = $isajax;
                    $data['userdata'] = $eligible['userdata'];
                    $data['old'] = $request;
                    $data['reason'] = ReasonCreatingAccount::GetActive();
                    $data['mystatus'] = MyStatus::GetActive();
                    $data['organisation_department'] = OrganisationDepartment::GetActive();
                    $data['organisation_roles'] = OrganisationRoles::GetActive();

                    if ($isajax == 'true') :
                        $html = view('our-partners.registration', $data)->render();
                        return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);
                    else :
                        return view('our-partners.registration', $data);
                    endif;

                else :

                    if ($eligible['event'] == 'linkexpired') :
                        return redirect()->route('login')->with('alert', $eligible['message']);
                    elseif ($eligible['event'] == 'active') :
                        return redirect()->route('login')->with('alert', $eligible['message']);
                    elseif ($eligible['event'] == 'invaliduser') :
                        return redirect()->route('verification-process')->with('alert', $eligible['message']);
                    endif;


                endif;

            endif;
        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }
    }


    function VerifyRegisterationClient(Request $request)
    {

        try {

            $key = $request->key;

            if (isset($key) && !empty($key)) :

                $key = $this->Decrypt($key);

                $keyexplode=explode('-',$key);
                $email=isset($keyexplode[0]) && !empty($keyexplode[0]) ? $keyexplode[0] : '';
                DB::beginTransaction();
                $eligible = User::CheckEligibleForClientVerificationRegistration($email);
                DB::commit();

                


                $data['phonecodes'] = DB::table('tbl_country')->orderBy('name','asc')->get();
                if ($eligible['status'] == 'success') :

                    Session::put('verificationemail', $email);

                    $isajax = $request->ajax;
                    $data['isajax'] = $isajax;
                    $data['userdata'] = $eligible['userdata'];
                    $data['old'] = $request;
                    $data['reason'] = ReasonCreatingAccount::GetActive();
                    $data['mystatus'] = MyStatus::GetActive();
                    $data['organisation_department'] = OrganisationDepartment::GetActive();
                    $data['organisation_roles'] = OrganisationRoles::GetActive();

                    if ($isajax == 'true') :
                        $html = view('my-clients.registration', $data)->render();
                        return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);
                    else :
                        return view('my-clients.registration', $data);
                    endif;

                else :

                    if ($eligible['event'] == 'linkexpired') :
                        return redirect()->route('login')->with('alert', $eligible['message']);
                    elseif ($eligible['event'] == 'active') :
                        return redirect()->route('login')->with('alert', $eligible['message']);
                    elseif ($eligible['event'] == 'invaliduser') :
                        return redirect()->route('verification-process')->with('alert', $eligible['message']);
                    endif;


                endif;

            endif;
        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }
    }


    function SubmitRegisterationForm(Request $request)
    {
        
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'first_name' => ['required', 'min:2', 'max:255'],
            'last_name' => ['required','min:2', 'max:255'],
            'phone_code' => ['required_if:isorganisation,==,1','nullable'],
            'phone_number' => ['required_if:isorganisation,==,1','nullable','min:10','max:10'],
            'address_line_1' => ['required_if:isorganisation,==,1','nullable','min:10', 'max:255'],
            'postcode' => ['required_if:isorganisation,==,1','nullable','min:4', 'max:255'],
            'country' => ['required_if:isorganisation,==,1','nullable','string','min:2', 'max:255'],
            'state' => ['required_if:isorganisation,==,1','nullable','string','min:2', 'max:255'],
            'organisation_role' => "required_if:isorganisation,==,1|max:255",
            'organisation_name' => "required_if:isorganisation,==,1|max:255",
            'organisation_department' => "required_if:isorganisation,==,1|max:255",
            'password' => ['required', 'string', 'min:6',
            'regex:/[a-z]/',      // must contain at least one lowercase letter
            'regex:/[A-Z]/',      // must contain at least one uppercase letter
            'regex:/[0-9]/',      // must contain at least one digit
            'regex:/[@$!%*#?&]/', // must contain at least one Charater
            'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:6'],
            'dob'=>['required_if:isorganisation,==,0','before:22 years ago'],
            'other_reason' => ['required_if:reason,==,7','nullable','string','min:2','max:255'],
            'other_role' => ['required_if:organisation_role,==,75','nullable','string','min:2','max:255']
            


        ],
        [
        'company_address.required_if' => 'The company address field is required',
        'organisation_role.required_if' => 'The organisation role field is required',
        'postcode.required_if' => 'The postcode field is required',
        'country.required_if' => 'The country field is required',
        'state.required_if' => 'The state field is required',
        'organisation_name.required_if' => 'The organisation name field is required',
        'phone_code.required_if' => 'The phone code field is required',
        'phone_number.required_if' => 'The phone number field is required',
        'address_line_1.required_if' => 'The address line 1 field is required',
        'other_role.required_if' => 'please fill your role in above field',
        'other_reason.required_if' => 'please fill your reason to create account',
        'organisation_department.required_if' => 'The organisation department field is required'
        ]
        );

        $isajax = $request->ajax;

        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['phonecodes'] = DB::table('tbl_country')->orderBy('name','asc')->get();
        
        if ($validator->fails()) :
            
            $data['error'] = $validator->getMessageBag()->toArray();
           
            $data['mystatus'] = MyStatus::GetActive();
            $data['organisation_department'] = OrganisationDepartment::GetActive();
            $data['organisation_roles'] = OrganisationRoles::GetActive();
            $data['reason'] = ReasonCreatingAccount::GetActive();
            $html = view('auth.register', $data)->render();

            
            return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);

        endif;

        try {

            DB::beginTransaction();

            $user = User::where('email',$request->email)
                ->where('isactive', 0)
                ->whereNotNull('email_verified_at')
                ->first();



            if (isset($user) && !empty($user)) :

                $isorganisation = isset($request->isorganisation) && !empty($request->isorganisation) ? $request->isorganisation : 0;

                if (empty($user->user_role_type) || is_null($user->user_role_type) ) :
                    

                    $user->user_role_type = 2; // End User Role

                    if ($isorganisation == 1) :
                        $user->user_role_type = 4; // B2B Client
                    endif;

                    if(!empty($user->sib_contact_id)):

                    SendInBlueController::UpdateContact($user->email,$request->first_name,$request->last_name);
                    // $sendinbluedetails = SendInBlueController::GetContactDetails($user->email);

                    
                    // if(isset($sendinbluedetails['code']) && $sendinbluedetails['code']==200):
                            

                    //     if (isset($sendinbluedetails['response']['listIds']) && is_array($sendinbluedetails['response']['listIds']) && in_array(7,$sendinbluedetails['response']['listIds'])) :

                    //         $user->user_role_type = 4; // B2B Partner

                    //     endif;

                    // endif;

                    

                    endif;

                    if ($user->user_role_type == 2) :

                        $user->assignRole('Client');

                    elseif ($user->user_role_type == 3) :

                        $user->assignRole('B2B Client');

                    elseif ($user->user_role_type == 4) :

                        $user->assignRole('B2B Partner');

                    endif;


                endif;

                


                $email = $user->email;
                $date = str_replace('/', '-', $request->dob);
                $user->isactive = 1;
                $user->first_name = isset($request->first_name) && !empty($request->first_name) ? $request->first_name : null;
                $user->last_name = isset($request->last_name) && !empty($request->last_name) ? $request->last_name : null;
                $user->phone_code = isset($request->phone_code) && !empty($request->phone_code) ? $request->phone_code : null;
                $user->phone_number = isset($request->phone_number) && !empty($request->phone_number) ? $request->phone_number : null;
                $user->dob = isset($request->dob) &&  !empty($request->dob) ? date('Y-m-d',strtotime($date)) : null;
                $user->address_1 = isset($request->address_line_1) && !empty($request->address_line_1) ? $request->address_line_1 : null;
                $user->address_2 = isset($request->address_line_2) && !empty($request->address_line_2) ? $request->address_line_2 : null;
                $user->address_3 = isset($request->address_line_3) && !empty($request->address_line_3) ? $request->address_line_3 : null;
                $user->country = isset($request->country) && !empty($request->country) ? $request->country : null;
                $user->postcode = isset($request->postcode) && !empty($request->postcode) ? $request->postcode : null;
                $user->organisation_role = isset($request->organisation_role) && !empty($request->organisation_role) ? $request->organisation_role : null;
                $user->organisation_name = isset($request->organisation_name) && !empty($request->organisation_name) ? $request->organisation_name : null;
                $user->other_role = isset($request->other_role) && !empty($request->other_role) ? $request->other_role : null;
                $user->organisation_department = isset($request->organisation_department) && !empty($request->organisation_department) ? $request->organisation_department : null;
                $user->state = isset($request->state) && !empty($request->state) ? $request->state : null;
                $user->other_reason = isset($request->other_reason) && !empty($request->other_reason) ? $request->other_reason : null;
                $user->mystatus = isset($request->mystatus) && !empty($request->mystatus) ? $request->mystatus : null;
                $user->other_mystatus = isset($request->other_mystatus) && !empty($request->other_mystatus) ? $request->other_mystatus : null;
                $user->reason = isset($request->reason) && !empty($request->reason) ? $request->reason : 0;
                $user->updated_by = $user->id;
                $user->updated_at = Carbon::now();
                $user->user_stage_id = 3; // Registeration Stage Completed
                $user->hasdemopackage = 1;
                $user->password = \Hash::make(trim($request->password));
                $user->save();

                if($isorganisation!==1 && empty($user->user_package_id)):
                    
                    $packages=Packages::where('demopackage',1)->first();
                    $userpackage=UserPackages::create([

                    'user_id'=>$user->id,
                    'package_id'=>$packages->package_id,
                    'package_start_date'=>date('Y-m-d H:i:s'),
                    'package_end_date'=>date('Y-m-d H:i:s', strtotime('+1 year')),
                    'status'=>1,
                    'message_value'=>$packages->message_value,
                    'recipient_value'=>$packages->recipient_value,
                    'written_value'=>$packages->written_value,
                    'stripe_price_id'=>$packages->stripe_price_id,
                    'stripe_product_id'=>$packages->stripe_product_id,
                    'stripe_subscription_id'=>null,
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now(),

                ]);
                $user->user_package_id=$userpackage->package_id;
                $user->save();

                endif;

                if($isorganisation==1):
                        $packagesall=UserPackages::where('created_by',$user->id)->get();
                        if(isset($packagesall) && !empty($packagesall)):
                            foreach($packagesall as $packagesalldata):
                                $packagesalldata->organisation_user_id=$user->id;
                                $packagesalldata->is_organisation_package=1;
                                $packagesalldata->user_id=null;
                                $packagesalldata->save();
                            endforeach;
                        endif;

                        $user->hasdemopackage = 0;
                        $user->isorganisation = 1;
                        $user->organisation_code='ORG-EVAHELD-00'.$user->id; 
                        $user->save();
                    else:

                        $packagesall=UserPackages::where('created_by',$user->id)->get();
                        if(isset($packagesall) && !empty($packagesall)):
                            foreach($packagesall as $packagesalldata):
                                $packagesalldata->organisation_user_id=null;
                                $packagesalldata->is_organisation_package=null;
                                $packagesalldata->user_id=$user->id;
                                $packagesalldata->save();
                            endforeach;
                        endif;

                endif;
                
                $flowpackageid=Session::get('flowpackageid');
                
                

                $followup_period_id=12;
                
                

                $stage1_date=date('Y-m-d',strtotime($followup_period_id." month"));
                $stage2_date=date('Y-m-d', strtotime($stage1_date. ' +1 days'));
                $datatosave['stage1_completed']=0;
                $datatosave['stage2_completed']=0;
                $datatosave['stage3_completed']=0;
                $datatosave['stage1_date']=$stage1_date;
                $datatosave['stage2_date']=$stage2_date;
                $datatosave['stage2_count']=0;
                $datatosave['stage3_date']=null;
                $datatosave['responsibility_acknowledge']=1;
                $datatosave['release_message_acknowledge']=1;
                $datatosave['final_acknowledge']=1;
                $datatosave['user_id']=$user->id;
                $datatosave['followup_period_id']=4;
                $datatosave['failed_response_action_id']=1;

                CheckInSetting::create($datatosave);



                DB::commit();

                Auth::login($user);
                
                if(isset($flowpackageid) && !empty($flowpackageid)):
                    $packages=Packages::find($flowpackageid);
                    if(isset($packages) && !empty($packages)):
                        
                    if($flowpackageid=='1'):
                        
                        return response()->json(['status' => 'success', 'event' => 'redirect', 'routetoredirect' => route('dashboard')]);
                        
                        elseif(empty($user->user_package_id) && $flowpackageid!=='1'):
                            
                            $sessionarray=array();
                			$sessionarray['line_items']=array();
                			$sessionarray['success_url']=route('stripe-success-page').'?session_id={CHECKOUT_SESSION_ID}&package='.$packages->package_id;
                			$sessionarray['cancel_url']=route('stripe-cancel-page').'?session_id={CHECKOUT_SESSION_ID}&package='.$packages->package_id;
                			$sessionarray['payment_method_types']=['card'];
                
                			$sessionarray['mode']='payment';
                			
                			
                			$sessionarray['allow_promotion_codes']=true;
                			$line_items=array();
                
                			$line_items['price']=$packages->stripe_price_id;
                			$line_items['quantity']=1;
                			Stripe::setApiKey(env('API_KEY_STRIPE'));

                			$customerId=$user->stripe_customer_id;
                
                			if(empty($customerId) || is_null($customerId)):
                
                					$customer = \Stripe\Customer::create([
                						"description" => $user->first_name.' '.$user->last_name,
                						"email" => $user->email,
                						"metadata" => [
                							"first_name" => $user->first_name,
                							"last_name" => $user->last_name,
                							"aig_user_id" => $user->id,
                							"phone_number" => $user->phone_number,
                						],
                					]);
                
                				$customerId=$customer->id;
                
                				DB::beginTransaction();
                				$user->stripe_customer_id=$customerId;
                				$user->save();
                				DB::commit();
                
                			endif;
                			$sessionarray['line_items'][]=$line_items;
                			$sessionarray['customer']=$customerId;
                
                			Stripe::setApiKey(env('API_KEY_STRIPE'));
                			$checkout_session = \Stripe\Checkout\Session::create($sessionarray);
                
                			if(null!==$checkout_session['id'] && !empty($checkout_session['id'])):
                
                
                
                					$data['checkout_session']=$checkout_session['id'];
                					$pkkey=env('PK_KEY_STRIPE');
                
                					// DB::beginTransaction();
                
                					// CheckoutSessionData::create([
                					// 	'checkout_session_id'=>$checkout_session['id'],
                				 //        'package_id'=>$flowpackageid,
                				 //        'payment_status'=>$checkout_session['payment_status'],
                				 //        'subscription'=>$checkout_session['subscription'],
                				 //        'stripe_customer_id'=>$customerId,
                				 //        'user_id'=>$user->id,
                				 //        'expires_at'=>$checkout_session['expires_at'],
                				 //        'status'=>$checkout_session['status'],
                				 //        'lineitems'=>json_encode($line_items),
                				 //        'created_at'=>Carbon::now(),
                				 //        'created_by'=>$user->id,
                				 //        'updated_at'=>Carbon::now(),
                				 //        'updated_by'=>$user->id
                					// ]);
                
                					// DB::commit();
                
                					
                                    return response()->json(['status' => 'success', 'event' => 'redirect', 'routetoredirect' => $checkout_session['url']]);
                
                			endif;

                        else:

                            return response()->json(['status' => 'success', 'event' => 'redirect', 'routetoredirect' => route('dashboard')]);

                			
                		endif;
                		
                    endif;
                    
                    
                endif;

                Session::forget('flowpackageid');

                if($isorganisation==1):
                     return response()->json(['status' => 'success', 'event' => 'redirect', 'routetoredirect' => route('dashboard')]);
                else:
                     return response()->json(['status' => 'success', 'event' => 'redirect', 'routetoredirect' => route('subscription-packages')]);
                endif;

            else :


                return response()->json(['status' => 'fail', 'event' => 'redirect', 'routetoredirect' => route('login')]);

            endif;


        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }



        return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);
    }



    function SubmitRegisterationMemberForm(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'key' => 'required',
            'email' => 'required|email|unique:users',
            'first_name' => ['required', 'alpha','min:2', 'max:255'],
            'last_name' => ['required', 'alpha','min:2', 'max:255'],
            'phone_code' => ['required_if:isorganisation,==,1','nullable'],
            'phone_number' => ['required_if:isorganisation,==,1','nullable','min:10','max:10'],
            'address_line_1' => ['required_if:isorganisation,==,1','nullable','min:10', 'max:255'],
            'postcode' => ['required_if:isorganisation,==,1','nullable','min:4', 'max:255'],
            'country' => ['required_if:isorganisation,==,1','nullable','string','min:2', 'max:255'],
            'state' => ['required_if:isorganisation,==,1','nullable','string','min:2', 'max:255'],
            'password' => ['required', 'string', 'min:6',
            'regex:/[a-z]/',      // must contain at least one lowercase letter
            'regex:/[A-Z]/',      // must contain at least one uppercase letter
            'regex:/[0-9]/',      // must contain at least one digit
            'regex:/[@$!%*#?&]/', // must contain at least one Charater
            'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:6'],
            'dob'=>['before:22 years ago','nullable'],
            'other_reason' => ['required_if:reason,==,7','nullable','string','min:2','max:255'],
            'other_role' => ['required_if:organisation_role,==,75','nullable','string','min:2','max:255']
            


        ],
        [
        'company_address.required_if' => 'The company address field is required',
        'organisation_role.required_if' => 'The organisation role field is required',
        'postcode.required_if' => 'The postcode field is required',
        'country.required_if' => 'The country field is required',
        'state.required_if' => 'The state field is required',
        'organisation_name.required_if' => 'The organisation name field is required',
        'phone_code.required_if' => 'The phone code field is required',
        'phone_number.required_if' => 'The phone number field is required',
        'address_line_1.required_if' => 'The address line 1 field is required',
        'other_role.required_if' => 'please fill your role in above field',
        'other_reason.required_if' => 'please fill your reason to create account',
        'organisation_department.required_if' => 'The organisation department field is required'
        ]
        );

        $isajax = $request->ajax;
        $key=$request->key;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['phonecodes'] = DB::table('tbl_country')->orderBy('name','asc')->get();
        if (isset($key) && !empty($key)) :

            $key = $this->Decrypt($key);
            $keyexplode=explode('-',$key);
            $email=isset($keyexplode[0]) && !empty($keyexplode[0]) ? $keyexplode[0] : '';
            $eligible = User::CheckEligibleForMemberVerificationRegistration($email);
        
        endif;
        if ($validator->fails()) :
            
            $data['error'] = $validator->getMessageBag()->toArray();
            $data['userdata'] = $eligible['userdata'];
            $data['reason'] = ReasonCreatingAccount::GetActive();
            $data['mystatus'] = MyStatus::GetActive();
            $data['organisation_department'] = OrganisationDepartment::GetActive();
            $data['organisation_roles'] = OrganisationRoles::GetActive();
            
            $html = view('our-members.registration', $data)->render();

            
            return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);

            
            return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);

        endif;

        try {

            DB::beginTransaction();

                $userorganisationdata=User::where('email',$email)->first();



                if(isset($userorganisationdata) && !empty($userorganisationdata)):
                    SendInBlueController::UpdateContact($userorganisationdata->email,$request->first_name,$request->last_name);
                    $user = new User();
                    $user->email=isset($request->email) && !empty($request->email) ? $request->email : null;
                    $user->isactive = 1;
                    $user->first_name = isset($request->first_name) && !empty($request->first_name) ? $request->first_name : null;
                    $user->last_name = isset($request->last_name) && !empty($request->last_name) ? $request->last_name : null;
                    $user->phone_code = isset($request->phone_code) && !empty($request->phone_code) ? $request->phone_code : null;
                    $user->phone_number = isset($request->phone_number) && !empty($request->phone_number) ? $request->phone_number : null;
                    $user->dob = isset($request->dob) && !empty($request->dob) ? date('Y-m-d',strtotime($request->dob)) : null;
                    $user->address_1 = isset($request->address_line_1) && !empty($request->address_line_1) ? $request->address_line_1 : null;
                    $user->address_2 = isset($request->address_line_2) && !empty($request->address_line_2) ? $request->address_line_2 : null;
                    $user->address_3 = isset($request->address_line_3) && !empty($request->address_line_3) ? $request->address_line_3 : null;
                    $user->country = isset($request->country) && !empty($request->country) ? $request->country : null;
                    $user->postcode = isset($request->postcode) && !empty($request->postcode) ? $request->postcode : null;
                    $user->organisation_role = isset($request->organisation_role) && !empty($request->organisation_role) ? $request->organisation_role : null;
                    $user->organisation_name = isset($userorganisationdata->organisation_name) && !empty($userorganisationdata->organisation_name) ? $userorganisationdata->organisation_name : null;
                    $user->organisation_code = isset($userorganisationdata->organisation_code) && !empty($userorganisationdata->organisation_code) ? $userorganisationdata->organisation_code : null;
                    $user->other_role = isset($request->other_role) && !empty($request->other_role) ? $request->other_role : null;
                    $user->organisation_department = isset($request->organisation_department) && !empty($request->organisation_department) ? $request->organisation_department : null;
                    $user->state = isset($request->state) && !empty($request->state) ? $request->state : null;
                    $user->other_reason = isset($request->other_reason) && !empty($request->other_reason) ? $request->other_reason : null;
                    $user->mystatus = isset($request->mystatus) && !empty($request->mystatus) ? $request->mystatus : null;
                    $user->other_mystatus = isset($request->other_mystatus) && !empty($request->other_mystatus) ? $request->other_mystatus : null;
                    $user->reason = isset($request->reason) && !empty($request->reason) ? $request->reason : 0;
                    $user->updated_by = $user->id;
                    $user->updated_at = Carbon::now();
                    $user->user_stage_id = 3; // Registeration Stage Completed
                    $user->user_role_type = 5; 
                    $user->hasdemopackage = 1;
                    $user->password = \Hash::make(trim($request->password));
                    $user->save();

                    $user->assignRole('Member');

                    $usermember=UserMembers::create([

                        'user_id'=>$userorganisationdata->id,
                        'member_user_id'=>$user->id,
                        'isactive'=>1,
                        'updated_by'=>$user->id,
                        'updated_at'=>Carbon::now(),
                        'created_at'=>Carbon::now(),
                        'created_by'=>$user->id

                    ]);

                endif;


                DB::commit();

                Auth::login($user);

                return response()->json(['status' => 'success', 'event' => 'redirect', 'routetoredirect' => route('dashboard')]);

        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }



        return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);
    }


    function SubmitRegisterationPartnerForm(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'first_name' => ['required', 'alpha','min:2', 'max:255'],
            'last_name' => ['required', 'alpha','min:2', 'max:255'],
            'phone_code' => ['required_if:isorganisation,==,1','nullable'],
            'phone_number' => ['required_if:isorganisation,==,1','nullable','min:10','max:10'],
            'address_line_1' => ['required_if:isorganisation,==,1','nullable','min:10', 'max:255'],
            'postcode' => ['required_if:isorganisation,==,1','nullable','min:4', 'max:255'],
            'country' => ['required_if:isorganisation,==,1','nullable','string','min:2', 'max:255'],
            'state' => ['required_if:isorganisation,==,1','nullable','string','min:2', 'max:255'],
            'organisation_role' => "required_if:isorganisation,==,1|max:255",
            'organisation_name' => "required_if:isorganisation,==,1|max:255",
            'organisation_department' => "required_if:isorganisation,==,1|max:255",
            'password' => ['required', 'string', 'min:6',
            'regex:/[a-z]/',      // must contain at least one lowercase letter
            'regex:/[A-Z]/',      // must contain at least one uppercase letter
            'regex:/[0-9]/',      // must contain at least one digit
            'regex:/[@$!%*#?&]/', // must contain at least one Charater
            'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:6'],
            'dob'=>['required_if:isorganisation,==,0','before:22 years ago'],
            'other_reason' => ['required_if:reason,==,7','nullable','string','min:2','max:255'],
            'other_role' => ['required_if:organisation_role,==,75','nullable','string','min:2','max:255']
            


        ],
        [
        'company_address.required_if' => 'The company address field is required',
        'organisation_role.required_if' => 'The organisation role field is required',
        'postcode.required_if' => 'The postcode field is required',
        'country.required_if' => 'The country field is required',
        'state.required_if' => 'The state field is required',
        'organisation_name.required_if' => 'The organisation name field is required',
        'phone_code.required_if' => 'The phone code field is required',
        'phone_number.required_if' => 'The phone number field is required',
        'address_line_1.required_if' => 'The address line 1 field is required',
        'other_role.required_if' => 'please fill your role in above field',
        'other_reason.required_if' => 'please fill your reason to create account',
        'organisation_department.required_if' => 'The organisation department field is required'
        ]
        );

        $isajax = $request->ajax;

        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['phonecodes'] = DB::table('tbl_country')->orderBy('name','asc')->get();
        
        if ($validator->fails()) :
            
           $data['error'] = $validator->getMessageBag()->toArray();
           
            $data['mystatus'] = MyStatus::GetActive();
            $data['organisation_department'] = OrganisationDepartment::GetActive();
            $data['organisation_roles'] = OrganisationRoles::GetActive();
            $data['reason'] = ReasonCreatingAccount::GetActive();
            $html = view('our-partners.registration', $data)->render();

            
            return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);

        endif;

        try {

            DB::beginTransaction();

                
                    $isorganisation = 1;
                
                    $user = new User();
                    $user->email=isset($request->email) && !empty($request->email) ? $request->email : null;
                    $user->isactive = 1;
                    $user->first_name = isset($request->first_name) && !empty($request->first_name) ? $request->first_name : null;
                    $user->last_name = isset($request->last_name) && !empty($request->last_name) ? $request->last_name : null;
                    $user->phone_code = isset($request->phone_code) && !empty($request->phone_code) ? $request->phone_code : null;
                    $user->phone_number = isset($request->phone_number) && !empty($request->phone_number) ? $request->phone_number : null;
                    $user->dob = isset($request->dob) && !empty($request->dob) ? date('Y-m-d',strtotime($request->dob)) : null;
                    $user->address_1 = isset($request->address_line_1) && !empty($request->address_line_1) ? $request->address_line_1 : null;
                    $user->address_2 = isset($request->address_line_2) && !empty($request->address_line_2) ? $request->address_line_2 : null;
                    $user->address_3 = isset($request->address_line_3) && !empty($request->address_line_3) ? $request->address_line_3 : null;
                    $user->country = isset($request->country) && !empty($request->country) ? $request->country : null;
                    $user->postcode = isset($request->postcode) && !empty($request->postcode) ? $request->postcode : null;
                    $user->organisation_role = isset($request->organisation_role) && !empty($request->organisation_role) ? $request->organisation_role : null;
                    $user->organisation_name = isset($request->organisation_name) && !empty($request->organisation_name) ? $request->organisation_name : null;
                    $user->other_role = isset($request->other_role) && !empty($request->other_role) ? $request->other_role : null;
                    $user->organisation_department = isset($request->organisation_department) && !empty($request->organisation_department) ? $request->organisation_department : null;
                    $user->state = isset($request->state) && !empty($request->state) ? $request->state : null;
                    $user->other_reason = isset($request->other_reason) && !empty($request->other_reason) ? $request->other_reason : null;
                    $user->mystatus = isset($request->mystatus) && !empty($request->mystatus) ? $request->mystatus : null;
                    $user->other_mystatus = isset($request->other_mystatus) && !empty($request->other_mystatus) ? $request->other_mystatus : null;
                    $user->reason = isset($request->reason) && !empty($request->reason) ? $request->reason : 0;
                    $user->updated_by = $user->id;
                    $user->updated_at = Carbon::now();
                    $user->user_stage_id = 3; // Registeration Stage Completed
                    $user->user_role_type = 4; 
                    $user->hasdemopackage = 1;
                    $user->password = \Hash::make(trim($request->password));
                    $user->save();

                    $user->assignRole('B2B Partner');

                    if($isorganisation==1):
                            $user->hasdemopackage = 0;
                            $user->isorganisation = 1;
                            $user->organisation_code='ORG-EVAHELD-00'.$user->id; 
                            $user->save();
                    endif;
                    
                    
                    SendInBlueController::UpdateContact($user->email,$request->first_name,$request->last_name);
                    // SendInBlueController::UpdateContacttolist($user->email,73);


                DB::commit();

                Auth::login($user);

                return response()->json(['status' => 'success', 'event' => 'redirect', 'routetoredirect' => route('dashboard')]);

        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }



        return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);
    }


    function SubmitRegisterationClientForm(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'key' => 'required',
            'email' => 'required|email|unique:users',
            'first_name' => ['required', 'min:2', 'max:255'],
            'last_name' => ['required','min:2', 'max:255'],
            'phone_code' => ['required_if:isorganisation,==,1','nullable'],
            'phone_number' => ['required_if:isorganisation,==,1','nullable','min:10','max:10'],
            'address_line_1' => ['required_if:isorganisation,==,1','nullable','min:10', 'max:255'],
            'postcode' => ['required_if:isorganisation,==,1','nullable','min:4', 'max:255'],
            'country' => ['required_if:isorganisation,==,1','nullable','string','min:2', 'max:255'],
            'state' => ['required_if:isorganisation,==,1','nullable','string','min:2', 'max:255'],
            'organisation_role' => "required_if:isorganisation,==,1|max:255",
            'organisation_name' => "required_if:isorganisation,==,1|max:255",
            'organisation_department' => "required_if:isorganisation,==,1|max:255",
            'password' => ['required', 'string', 'min:6',
            'regex:/[a-z]/',      // must contain at least one lowercase letter
            'regex:/[A-Z]/',      // must contain at least one uppercase letter
            'regex:/[0-9]/',      // must contain at least one digit
            'regex:/[@$!%*#?&]/', // must contain at least one Charater
            'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:6'],
            'dob'=>['required_if:isorganisation,==,0','before:22 years ago'],
            'other_reason' => ['required_if:reason,==,7','nullable','string','min:2','max:255'],
            'other_role' => ['required_if:organisation_role,==,75','nullable','string','min:2','max:255']
            


        ],
        [
        'company_address.required_if' => 'The company address field is required',
        'organisation_role.required_if' => 'The organisation role field is required',
        'postcode.required_if' => 'The postcode field is required',
        'country.required_if' => 'The country field is required',
        'state.required_if' => 'The state field is required',
        'organisation_name.required_if' => 'The organisation name field is required',
        'phone_code.required_if' => 'The phone code field is required',
        'phone_number.required_if' => 'The phone number field is required',
        'address_line_1.required_if' => 'The address line 1 field is required',
        'other_role.required_if' => 'please fill your role in above field',
        'other_reason.required_if' => 'please fill your reason to create account',
        'organisation_department.required_if' => 'The organisation department field is required'
        ]
        );

        $isajax = $request->ajax;
        $key = $request->key;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['phonecodes'] = DB::table('tbl_country')->orderBy('name','asc')->get();
        if (isset($key) && !empty($key)) :

            $key = $this->Decrypt($key);
            $keyexplode=explode('-',$key);
            $email=isset($keyexplode[0]) && !empty($keyexplode[0]) ? $keyexplode[0] : '';
            $eligible = User::CheckEligibleForClientVerificationRegistration($email);
        
        endif;

        if ($validator->fails()) :
            
            $data['error'] = $validator->getMessageBag()->toArray();
            $data['userdata'] = $eligible['userdata'];
            $data['reason'] = ReasonCreatingAccount::GetActive();
            $data['mystatus'] = MyStatus::GetActive();
            $data['organisation_department'] = OrganisationDepartment::GetActive();
            $data['organisation_roles'] = OrganisationRoles::GetActive();
            $html = view('my-clients.registration', $data)->render();

            
            return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);

        endif;

        try {

            DB::beginTransaction();

                $userorganisationdata=User::where('email',$email)->first();



                if(isset($userorganisationdata) && !empty($userorganisationdata)):

                    $user = new User();
                    $user->email=isset($request->email) && !empty($request->email) ? $request->email : null;
                    $user->isactive = 1;
                    $user->first_name = isset($request->first_name) && !empty($request->first_name) ? $request->first_name : null;
                    $user->last_name = isset($request->last_name) && !empty($request->last_name) ? $request->last_name : null;
                    $user->phone_code = isset($request->phone_code) && !empty($request->phone_code) ? $request->phone_code : null;
                    $user->phone_number = isset($request->phone_number) && !empty($request->phone_number) ? $request->phone_number : null;
                    $user->dob = isset($request->dob) && !empty($request->dob) ? date('Y-m-d',strtotime($request->dob)) : null;
                    $user->address_1 = isset($request->address_line_1) && !empty($request->address_line_1) ? $request->address_line_1 : null;
                    $user->address_2 = isset($request->address_line_2) && !empty($request->address_line_2) ? $request->address_line_2 : null;
                    $user->address_3 = isset($request->address_line_3) && !empty($request->address_line_3) ? $request->address_line_3 : null;
                    $user->country = isset($request->country) && !empty($request->country) ? $request->country : null;
                    $user->postcode = isset($request->postcode) && !empty($request->postcode) ? $request->postcode : null;
                    $user->organisation_role = isset($request->organisation_role) && !empty($request->organisation_role) ? $request->organisation_role : null;
                    $user->organisation_name = isset($userorganisationdata->organisation_name) && !empty($userorganisationdata->organisation_name) ? $userorganisationdata->organisation_name : null;
                    $user->organisation_code = isset($userorganisationdata->organisation_code) && !empty($userorganisationdata->organisation_code) ? $userorganisationdata->organisation_code : null;
                    $user->other_role = isset($request->other_role) && !empty($request->other_role) ? $request->other_role : null;
                    $user->organisation_department = isset($request->organisation_department) && !empty($request->organisation_department) ? $request->organisation_department : null;
                    $user->state = isset($request->state) && !empty($request->state) ? $request->state : null;
                    $user->other_reason = isset($request->other_reason) && !empty($request->other_reason) ? $request->other_reason : null;
                    $user->mystatus = isset($request->mystatus) && !empty($request->mystatus) ? $request->mystatus : null;
                    $user->other_mystatus = isset($request->other_mystatus) && !empty($request->other_mystatus) ? $request->other_mystatus : null;
                    $user->reason = isset($request->reason) && !empty($request->reason) ? $request->reason : 0;
                    $user->updated_by = $user->id;
                    $user->updated_at = Carbon::now();
                    $user->user_stage_id = 3; // Registeration Stage Completed
                    $user->user_role_type = 2; 
                    $user->hasdemopackage = 1;
                    $user->password = \Hash::make(trim($request->password));
                    $user->save();
                    SendInBlueController::UpdateContact($user->email,$request->first_name,$request->last_name);
                    $user->assignRole('Client');

                    $usermember=UserClients::create([

                        'user_id'=>$userorganisationdata->id,
                        'client_user_id'=>$user->id,
                        'isactive'=>1,
                        'updated_by'=>$user->id,
                        'updated_at'=>Carbon::now(),
                        'created_at'=>Carbon::now(),
                        'created_by'=>$user->id,
                        'organisation_code'=>$userorganisationdata->organisation_code

                    ]);

                    if(empty($user->user_package_id)):
                    
                        $packages=Packages::where('demopackage',1)->first();
                        $userpackage=UserPackages::create([

                        'user_id'=>$user->id,
                        'package_id'=>$packages->package_id,
                        'package_start_date'=>date('Y-m-d H:i:s'),
                        'package_end_date'=>date('Y-m-d H:i:s', strtotime('+1 year')),
                        'status'=>1,
                        'message_value'=>$packages->message_value,
                        'recipient_value'=>$packages->recipient_value,
                        'written_value'=>$packages->written_value,
                        'stripe_price_id'=>$packages->stripe_price_id,
                        'stripe_product_id'=>$packages->stripe_product_id,
                        'stripe_subscription_id'=>null,
                        'created_at'=>Carbon::now(),
                        'updated_at'=>Carbon::now(),

                    ]);
                    $user->user_package_id=$userpackage->package_id;
                    $user->save();
                    endif;
                    
                    $followup_period_id=12;
                    $stage1_date=date('Y-m-d',strtotime($followup_period_id." month"));
                    $stage2_date=date('Y-m-d', strtotime($stage1_date. ' +1 days'));
                    $datatosave['stage1_completed']=0;
                    $datatosave['stage2_completed']=0;
                    $datatosave['stage3_completed']=0;
                    $datatosave['stage1_date']=$stage1_date;
                    $datatosave['stage2_date']=$stage2_date;
                    $datatosave['stage2_count']=0;
                    $datatosave['stage3_date']=null;
                    $datatosave['responsibility_acknowledge']=1;
                    $datatosave['release_message_acknowledge']=1;
                    $datatosave['final_acknowledge']=1;
                    $datatosave['user_id']=$user->id;
                    $datatosave['followup_period_id']=4;
                    $datatosave['failed_response_action_id']=1;

                    CheckInSetting::create($datatosave);

                    

                endif;    

                DB::commit();

                Auth::login($user);

                return response()->json(['status' => 'success', 'event' => 'redirect', 'routetoredirect' => route('dashboard')]);

        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }



        return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);
    }





    // Registeration Form Process ---------END



    public function invitationauthorisedperson(Request $request)
    {
            $code = $request->key;

                if (isset($code) && !empty($code)) :

                $key = $this->Decrypt($code);
                $keyexplode=explode('--',$key);
                $email=isset($keyexplode[0]) && !empty($keyexplode[0]) ? $keyexplode[0] : '';
                $user_id=isset($keyexplode[3]) && !empty($keyexplode[3]) ? $keyexplode[3] :'';

                $eligible = UserAuthorizedPerson::join('users','users.id','user_authorised_person.user_id')->where('user_authorised_person.email',$email)->where('user_authorised_person.user_id',$user_id)->select('user_authorised_person.*','users.first_name as ownerfname','users.last_name as ownerlname')->first();
                if(isset($eligible) && !empty($eligible)):

                    DB::beginTransaction();
                    $eligible->status=1;
                    $eligible->invitation_status=1;
                    $eligible->save();

                    $access=HasAccessLogin::where('main_user_id',$user_id)->first();
                    if(isset($access) && !empty($access)):
                    $access->invitation_status=1;
                    if(!empty($eligible->authorised_access_type)):
                        $access->isactive=1;
                    endif;
                    $access->save();
                    endif;
                    DB::commit();
                   
                    $isajax = $request->ajax;
                    $data['isajax'] = $isajax;
                    $data['success'] = false;
                    $data['userdata'] = $eligible;
                    $data['redirecturl'] = url('/').'/notify-user-status-verification/'.$code;

                    if ($isajax == 'true') :

                        $html = view('layouts-frontend.invitation-accepted', $data)->render();

                        return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);

                    else :
                        return view('layouts-frontend.invitation-accepted', $data);
                    endif;

                endif;

                

                endif;

                
    }



    public function MessageVerification(Request $request,$code,$verificationtoken= null)
    {
        $isajax = $request->ajax;
        $data['isajax'] = $isajax;

        $user=Auth::user();
        $decode=$this->Decrypt($code);


        if(isset($decode) && !empty($decode)):

            $splitcode=explode("-/-",$decode);

            $usertype=isset($splitcode[0]) && !empty($splitcode[0]) ? $splitcode[0] : null;
            $messagetype=isset($splitcode[1]) && !empty($splitcode[1]) ? $splitcode[1] : null;
            $message_id=isset($splitcode[2]) && !empty($splitcode[2]) ? $splitcode[2] : null;

            $user_recipient_id=isset($splitcode[3]) && !empty($splitcode[3]) && $usertype=='recipient' ? $splitcode[3] : null;

            $user_authorised_person_id=isset($splitcode[3]) && !empty($splitcode[3]) && $usertype=='thirdperson' ? $splitcode[3] : null;



        endif;

        $data['type']=$messagetype;
        $data['messageid']=$message_id;
        $data['error'] = $request->error;
        $data['old'] = $request;
        $data['code'] = $code;
        $data['usertype'] = $usertype;

        if($messagetype==1):


           $messagedata=ZiggeoUserVideos::FindByMessageId($message_id);
           $data['messagetoken']=$messagedata->video_token_id;

        endif;

        if($messagetype==2):


           $messagedata=ZiggeoUserAudios::FindByMessageId($message_id);
           $data['messagetoken']=$messagedata->audio_token_id;

        endif;


        if($messagetype==3):


           $messagedata=ZiggeoUserTexts::FindByMessageId($message_id);
           $data['messagetoken']=$messagedata->text_token_id;

        endif;

       if(isset($messagedata) && !empty($messagedata)):
        
        if($user && $messagedata->email==$user->email):

            if($messagetype==1 || $messagetype==2):

                $data['ziggeoauthtoken']=$this->GenerateAuthtoken($messagedata->token);

            endif;

            $data['showmessage']=true;
            $data['messagedata']=$messagedata;
            if ($isajax == 'true') :

                $html = view('my-messages.message-verification', $data)->render();

                return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);

            else :

                return view('my-messages.message-verification', $data);

            endif;

        else:

        if($usertype=='thirdperson'):

            $data['verification_process_step_id']=$messagedata->averification_process_step_id;
            $data['verification_question_1']=$messagedata->averification_question_1;
            $data['verification_question_2']=$messagedata->averification_question_2;
            $data['verification_question_3']=$messagedata->averification_question_3;
            $data['verification_questions'] = VerficationQuestions::all();
            $data['messagedata']=$messagedata;
            $canaccess=($messagedata->amessage_access_code==$verificationtoken) && strtotime($messagedata->acode_expiry_at) > strtotime(date('Y-m-d H:i:s') && $verificationtoken==Session::get('verificationtoken') ) ? true : false;
        endif;

        if($usertype=='recipient'):

        $data['verification_process_step_id']=$messagedata->rverification_process_step_id;
        $data['verification_question_1']=$messagedata->rverification_question_1;
        $data['verification_question_2']=$messagedata->rverification_question_2;
        $data['verification_question_3']=$messagedata->rverification_question_3;
        $data['verification_questions'] = VerficationQuestions::all();
        $data['messagedata']=$messagedata;
        
        $canaccess=($messagedata->rmessage_access_code==$verificationtoken) && strtotime($messagedata->rcode_expiry_at) > strtotime(date('Y-m-d H:i:s') && $verificationtoken==Session::get('verificationtoken')) || (isset($messagedata->user_recipient_id) && !empty($messagedata->user_recipient_id) && $messagedata->rverification_process_step_id==null) ? true : false;

        
        endif;
      
        if($canaccess):

        $data['ziggeoauthtoken']=$this->GenerateAuthtoken($messagedata->token);    
        $data['showmessage']=true;

        if ($isajax == 'true') :

            $html = view('my-messages.message-verification', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);

        else :

            return view('my-messages.message-verification', $data);

        endif;


    else:

        if ($isajax == 'true') :

            $html = view('my-messages.message-verification', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);

        else :

            return view('my-messages.message-verification', $data);

        endif;


    endif;





endif;  



endif;

}


public function MessageConfirmEmailPost(Request $request)
{   

       $request->validate([

            'email' => 'required'
            
            
            
        ]);

        
        $code = $request->code;
        if(isset($code) && !empty($code)):
            $decode=$this->Decrypt($code);
            $splitcode=explode("-/-",$decode);

            $usertype=isset($splitcode[0]) && !empty($splitcode[0]) ? $splitcode[0] : null;
            $messagetype=isset($splitcode[1]) && !empty($splitcode[1]) ? $splitcode[1] : null;
            $message_id=isset($splitcode[2]) && !empty($splitcode[2]) ? $splitcode[2] : null;

            $user_recipient_id=isset($splitcode[3]) && !empty($splitcode[3]) && $usertype=='recipient' ? $splitcode[3] : null;

            $user_authorised_person_id=isset($splitcode[3]) && !empty($splitcode[3]) && $usertype=='thirdperson' ? $splitcode[3] : null;



        endif;
        $type=$messagetype;
        $user=Auth::user();
        $data['old'] = $request;

        if($type==1):


           $messagedata=ZiggeoUserVideos::FindByMessageId($message_id);

        endif;

        if($type==2):


           $messagedata=ZiggeoUserAudios::FindByMessageId($message_id);

        endif;


        if($type==3):

        $messagedata=ZiggeoUserTexts::FindByMessageId($message_id);

        endif;

        if(isset($messagedata) && !empty($messagedata)):


                    if($usertype=='thirdperson'):


                         $authoriseddata=UserAuthorizedPerson::find($messagedata->user_authorised_person_id);

                         if($authoriseddata->email==$request->email):

                            $verificationtoken=$messagedata->token.uniqid();
                            $authoriseddata->message_access_code=$verificationtoken;
                            $authoriseddata->code_expiry_at=date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' + 3 hours'));
                            $authoriseddata->save();

                         endif;


                    endif;

                    if($usertype=='recipient'):

                        
                        $authoriseddata=UserAuthorizedPerson::find($messagedata->user_authorised_person_id);

                        if($authoriseddata->email==$request->email):

                            

                        endif;
                        
                    endif;

                
        endif;

        return  \Redirect::to(url('/').'/message-verification/'.$code);
}

public function MessageVerificationPost(Request $request)
{   

       $request->validate([

            'code' => 'required'
            
            
            
        ]);

        
        $code = $request->code;
        if(isset($code) && !empty($code)):
            $decode=$this->Decrypt($code);
            $splitcode=explode("-/-",$decode);

            $usertype=isset($splitcode[0]) && !empty($splitcode[0]) ? $splitcode[0] : null;
            $messagetype=isset($splitcode[1]) && !empty($splitcode[1]) ? $splitcode[1] : null;
            $message_id=isset($splitcode[2]) && !empty($splitcode[2]) ? $splitcode[2] : null;

            $user_recipient_id=isset($splitcode[3]) && !empty($splitcode[3]) && $usertype=='recipient' ? $splitcode[3] : null;

            $user_authorised_person_id=isset($splitcode[3]) && !empty($splitcode[3]) && $usertype=='thirdperson' ? $splitcode[3] : null;



        endif;
        $type=$messagetype;
        $user=Auth::user();
        $data['old'] = $request;

        if($type==1):


           $messagedata=ZiggeoUserVideos::FindByMessageId($message_id);

        endif;

        if($type==2):


           $messagedata=ZiggeoUserAudios::FindByMessageId($message_id);

        endif;


        if($type==3):


           $messagedata=ZiggeoUserTexts::FindByMessageId($message_id);

        endif;

        if(isset($messagedata) && !empty($messagedata)):


                if($usertype=='thirdperson'):


                    $verification_process_step_id=$messagedata->averification_process_step_id;
                    $verification_answer_1=$messagedata->averification_answer_1;
                    $verification_answer_2=$messagedata->averification_answer_2;
                    $verification_answer_3=$messagedata->averification_answer_3;
                    $password=$messagedata->password;


                    if($verification_process_step_id==1):

                        if($request->password===$password):

                            $authoriseddata=UserAuthorizedPerson::find($messagedata->user_authorised_person_id);
                    
                            $verificationtoken=$messagedata->token.uniqid();
                            $authoriseddata->message_access_code=$verificationtoken;
                            $authoriseddata->code_expiry_at=date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' + 3 hours'));
                            $authoriseddata->save();
                            Session::put('verificationtoken', $verificationtoken);
                            
                            return  \Redirect::to(url('/').'/message-verification/'.$code.'/'.$verificationtoken); 

                        endif;

                    endif;


                    if($verification_process_step_id==2):
                        if($request->verification_answer_1===$verification_answer_1 && $request->verification_answer_2===$verification_answer_2 && $request->verification_answer_3===$verification_answer_3):

                            $authoriseddata=UserAuthorizedPerson::find($messagedata->user_authorised_person_id);
                    
                            $verificationtoken=$messagedata->token.uniqid();
                            $authoriseddata->message_access_code=$verificationtoken;
                            $authoriseddata->code_expiry_at=date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' + 3 hours'));
                            $authoriseddata->save();
                            Session::put('verificationtoken', $verificationtoken);
                            
                            return  \Redirect::to(url('/').'/message-verification/'.$code.'/'.$verificationtoken);

                       endif;
                    endif;


                    // if($verification_process_step_id!==1 && $verification_process_step_id!==2):
                    // if($request->otp===$messagedata->amessage_access_code && strtotime($messagedata->acode_expiry_at) > strtotime(date('Y-m-d H:i:s'))):

                    //         $verificationtoken=$messagedata->amessage_access_code;
                    //         Session::put('verificationtoken', $verificationtoken);
                            
                    //         return  \Redirect::to(url('/').'/message-verification/'.$code.'/'.$messagedata->amessage_access_code);

                    // endif;
                    // endif;



                endif;

                if($usertype=='recipient'):

                    $verification_process_step_id=$messagedata->rverification_process_step_id;
                    $verification_answer_1=$messagedata->rverification_answer_1;
                    $verification_answer_2=$messagedata->rverification_answer_2;
                    $verification_answer_3=$messagedata->rverification_answer_3;
                    $password=$messagedata->rpassword;
                    
                    
                endif;

                if($verification_process_step_id==5):
                    if($request->password===$password):

                            $recipientsdata=MyRecipients::find($messagedata->user_recipient_id);
                    
                            $verificationtoken=$messagedata->token.uniqid();
                            $recipientsdata->message_access_code=$verificationtoken;
                            $recipientsdata->code_expiry_at=date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' + 3 hours'));
                            $recipientsdata->save();
                            Session::put('verificationtoken', $verificationtoken);
                            
                            return  \Redirect::to(url('/').'/message-verification/'.$code.'/'.$verificationtoken);

                   endif;

                endif;


                if($verification_process_step_id==6):
                    if($request->verification_answer_1===$verification_answer_1 && $request->verification_answer_2===$verification_answer_2 && $request->verification_answer_3===$verification_answer_3):

                            $recipientsdata=MyRecipients::find($messagedata->user_recipient_id);
                    
                            $verificationtoken=$messagedata->token.uniqid();
                            $recipientsdata->message_access_code=$verificationtoken;
                            $recipientsdata->code_expiry_at=date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' + 3 hours'));
                            $recipientsdata->save();
                            Session::put('verificationtoken', $verificationtoken);
                            
                            return  \Redirect::to(url('/').'/message-verification/'.$code.'/'.$verificationtoken); 

                   endif;
                endif;

                if($verification_process_step_id!==5 && $verification_process_step_id!==6):
                    if($request->otp===$messagedata->rmessage_access_code && strtotime($messagedata->rcode_expiry_at) > strtotime(date('Y-m-d H:i:s'))):

                            $verificationtoken=$messagedata->rmessage_access_code;
                            Session::put('verificationtoken', $verificationtoken);
                            
                            return  \Redirect::to(url('/').'/message-verification/'.$code.'/'.$messagedata->rmessage_access_code);

                    endif;

                endif;



        endif;

        return  \Redirect::to(url('/').'/message-verification/'.$code);
}


public static function GenerateAuthtoken($alltokens)
{   
    $token=null;
    $arguments = array();

    $alltokens=array($alltokens);
    $arguments=array(
        "grants" => array(
            "create" => array(
                "session_owned" => TRUE
            ),
            "update" => array(
                "resources" =>$alltokens
            ),
            "destroy" => array(
                "resources" =>$alltokens
            ),
            "read" => array(
                "resources" =>$alltokens
            )

        ),
        "session_limit" => 1,
        "usage_expiration_time"=>86400

    );

    $ziggeo = new Ziggeo(env('ZIGGEOTOKEN'),env('ZIGGEOPRIVATEKEY'));
    $tokencreated = $ziggeo->authtokens()->create($arguments);

    return $tokencreated['token'];

}



public function PrivacyPolicy(Request $request)
{

}

public function TermsAndCondtion(Request $request)
{

}


    public function MailConfirmation(Request $request,$id)
    {   

            $emailscheduler=EmailScheduler::find($id);
            
            if($emailscheduler->date==date('Y-m-d')):

            Session::put('updateCheckinsetting',$emailscheduler->user_id);
            
            $user=Auth::user();

            if($user && $emailscheduler->user_id==$user->id):

            $checkinsettingdata=CheckInSetting::FindByUserId($user->id);
            
            
            $followup_period_id=$checkinsettingdata->followup_period_id;

            $followup_period_data=FollowupPeriod::find($followup_period_id);

            $stage1_date=date('Y-m-d',strtotime($followup_period_id." month"));
            $stage2_date=date('Y-m-d', strtotime($stage1_date. ' +1 days'));
            $datatosave['stage1_completed']=0;
            $datatosave['stage2_completed']=0;
            $datatosave['stage3_completed']=0;
            $datatosave['stage1_date']=$stage1_date;
            $datatosave['stage2_date']=$stage2_date;
            $datatosave['stage2_count']=0;
            $datatosave['stage3_date']=null;

            $updated=$checkinsettingdata->update($datatosave);

            

            return redirect()->route('dashboard');

            endif;

            endif;   

            return redirect()->route('login');

    }

}
