<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use Auth;
use Validator;
use Storage;
use File;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Coupons;
use App\Models\UserInvoices;
use App\Models\ReasonCreatingAccount;
use App\Models\MyStatus;
use App\Models\OrganisationDepartment;
use App\Models\OrganisationRoles;
use App\Models\FollowupPeriod;
use App\Models\FailedResponseAction;
use App\Models\VerificationProcessStep;
use App\Models\VerficationQuestions;
use App\Models\WatingPeriod;
use App\Models\CheckInSetting;
use App\Models\UserAuthorizedPerson;
use App\Models\UserReceipts;
use App\Models\UserPackages;
use App\Models\ClientsDocuments;
use App\Models\UserAddonPackages;
use App\Models\MyRecipients;
use App\Http\Controllers\SendInBlueController;
class OurB2CUserClientsController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
        
    }



    public function index(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['error'] = $request->error;
        $data['b2cusers'] = User::GetOurB2CUserClients();
       
        if ($isajax == 'true'):

            $html = view('admin.b2c-users-clients.index', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('b2c-users-clients.index')]);

        else:

            return view('admin.b2c-users-clients.index', $data);

        endif;
    }


    public function details(Request $request,$id)
    {   
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $id;
        $data['error'] = $request->error;
        
       
        if ($isajax == 'true'):

            $html = view('admin.b2c-users-clients.details', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('b2c-users-clients.details',$id)]);

        else:

            return view('admin.b2c-users-clients.details', $data);

        endif;
    }


    public function UserAccountDetails(Request $request,$id)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $id;
        $data['error'] = $request->error;
        $data['userdata']=User::find($id);
        $data['phonecodes'] = DB::table('tbl_country')->orderBy('name','asc')->get();
        $data['reason'] = ReasonCreatingAccount::GetActive();
        $data['mystatus'] = MyStatus::GetActive();
        $data['organisation_department'] = OrganisationDepartment::GetActive();
        $data['organisation_roles'] = OrganisationRoles::GetActive();
        $data['receipts'] = UserReceipts::GetByUser($id);
        $data['user_active_packages'] = UserPackages::GetActivePlan($id);
        $data['useraddons']=UserAddonPackages::FindByUserId($id);    
        $data['user_inactive_packages'] = UserPackages::GetInActiveByUserId($id);
        $data['usedtotalpersonalisedmessage']=User::GetTotalMessagesCount($id);
        $totalminutes=User::GetTotalResource($id);
        $usedtotalminutes=User::GetUsedMinutes($id);
        $usedrecipients=User::GetUsedRecipientsLimit($id);
        $totalrecipients=User::GetTotalRecipients($id);
        $usedpagecount=User::GetUsedDocLimit($id);
        $totalpagecount=User::GetTotalDocLimit($id);
        $haspagecount=$totalpagecount-$usedpagecount;
        $hasrecipients=($totalrecipients-$usedrecipients);
        $data['hasrecipients']=$hasrecipients;
        $data['usedrecipients']=$usedrecipients;
        $data['totalrecipients']=$totalrecipients;
        $data['usedtotalminutes']=$usedtotalminutes;
        $data['totalminutes']=$totalminutes;
        $data['haspagecount']=$haspagecount;
        $data['usedpagecount']=$usedpagecount;
        $data['totalpagecount']=$totalpagecount;
        
        if ($isajax == 'true'):

            $html = view('admin.b2c-users-clients.account-details', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('b2c-users-clients.details.account-details',$user->id)]);

        else:

            return view('admin.b2c-users-clients.account-details', $data);

        endif;
    }

    public function UserPreferences(Request $request,$id)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $id;
        $data['state'] = '2';
        $data['error'] = $request->error;
        $userdata=User::find($id);
        $data['followup_period'] = FollowupPeriod::all();
        $data['failed_response_action'] = FailedResponseAction::all();
        $data['verification_process_step'] = VerificationProcessStep::where('recipient',0)->get();
        $data['verification_questions'] = VerficationQuestions::all();
        $data['wating_period'] = WatingPeriod::all();
        $data['checkinsetting']=CheckInSetting::FindByUserId($id);
        $data['authoriseddata'] = UserAuthorizedPerson::FindByUser($id);
        $data['userdata']=$userdata;
        $data['default_prefrence']=$userdata->default_prefrence;
        $data['authoriseddata'] = UserAuthorizedPerson::FindByUser($id);
        $data['relationship'] = DB::table('relationship')->orderBy('relationship_name','asc')->get();
        $data['phonecodes'] = DB::table('tbl_country')->orderBy('name','asc')->get();

        if ($isajax == 'true'):

            $html = view('admin.b2c-users-clients.preferences', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('b2c-users-clients.details.preferences',$user->id)]);

        else:

            return view('admin.b2c-users-clients.preferences', $data);

        endif;
    }

    public function UserDeathDetails(Request $request,$id)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $id;
        $data['error'] = $request->error;
        $data['userinvoices']=UserInvoices::GetByUser($id);
        $data['userdata']=User::find($id);
        $data['clientdocuments']=ClientsDocuments::where('user_id',$id)
        ->where('capture_type','uploaded')
        ->get();
        if ($isajax == 'true'):

            $html = view('admin.b2c-users-clients.death-details', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('b2c-users-clients.details.death-details',$user->id)]);

        else:

            return view('admin.b2c-users-clients.death-details', $data);

        endif;
    }

    public function UserRecipients(Request $request,$id)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $id;
        $data['error'] = $request->error;
        
        $firstname = $request->first_name;
        $lastname = $request->last_name;
        $email = $request->email;
        $phone_number = $request->phone_number;
        $status = $request->status;

        $recipients = MyRecipients::search($firstname,$lastname,$email,$phone_number,$status,$id);
        $recipients->setPath(route('manage-recipients.index'));
        
        $data['recipients'] = $recipients;
        if ($isajax == 'true'):

            $html = view('admin.b2c-users-clients.recipients', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('b2c-users-clients.details.recipients',$user->id)]);

        else:

            return view('admin.b2c-users-clients.recipients', $data);

        endif;
    }

    public function UserAuthorisedParties(Request $request,$id)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $id;
        $data['error'] = $request->error;
        $data['userinvoices']=UserInvoices::GetByUser($id);
        $data['userdetails']=User::find($id);
       
        if ($isajax == 'true'):

            $html = view('admin.b2c-users-clients.authorised-parties', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('b2c-users-clients.details.authorised-parties',$user->id)]);

        else:

            return view('admin.b2c-users-clients.authorised-parties', $data);

        endif;
    }

    public function UserUpdateStatus(Request $request,$id)
    {
        
        $user=Auth::user();
        $isajax=$request->ajax;
        $code=$request->code;
        

        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['error'] = $request->error;
        $acknowledge = $request->acknowledge;
        $passed_date = $request->passed_date;
        $data['acknowledge'] = $acknowledge;
        $userdata=User::find($id);
        $userdata->passed_date=date('Y-m-d',strtotime($passed_date));
        $userdata->save();


        $request = new \Illuminate\Http\Request();
        $request->ajax='true';
        $request->successmessage='Client Status Updates Successfully';
        return $this->UserDeathDetails($request,$id);

      }

    public function UserStatistics(Request $request,$id)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $id;
        $data['error'] = $request->error;
        $data['userinvoices']=UserInvoices::GetByUser($id);
        $data['userdetails']=User::find($id);
       
        if ($isajax == 'true'):

            $html = view('admin.b2c-users-clients.statistics', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('b2c-users-clients.details.statistics',$user->id)]);

        else:

            return view('admin.b2c-users-clients.statistics', $data);

        endif;
    }

    public function UserUploadFiles(Request $request,$id)
    {

        try
        {   
            $user=Auth::user();

            $validator = Validator::make($request->all(), [

            'file' => 'required',
            'type' => 'required'
            
            ]);


            if ($request->hasFile('file')) {

                $document = $request->file('file');
                $fileInfo = $document->getClientOriginalName();
                $filename = pathinfo($fileInfo, PATHINFO_FILENAME);
                $extension = pathinfo($fileInfo, PATHINFO_EXTENSION);
                $file_name= $filename.'-'.time().'.'.$extension;
                Storage::disk('public')->put($file_name,File::get($document));


                if(Storage::disk('public')->exists($file_name)) {  
                    $path = Storage::disk('public')->path($file_name);

                    $allowedMimeTypes = ['image/jpeg','image/gif','image/png','image/jpg'];
                    $contentType = $document->getClientMimeType();

                    if(! in_array($contentType, $allowedMimeTypes) ){
                      
                      $filetype='pdf';
                    }
                    else
                    {
                      $filetype='image';
                    }

                    $updated=ClientsDocuments::create([

                        'title'=>$filename,
                        'user_id'=>$id,
                        'capture_type'=>'uploaded',
                        'path'=>$path,
                        'uploadedfilename'=>$file_name,
                        'created_at'=>Carbon::now(),
                        'created_by'=>$user->id,
                        'updated_at'=>Carbon::now(),
                        'updated_by'=>$user->id,
                        'filetype'=>$filetype
                    ]);

                    return array('status'=>'success','data'=>$updated);
                    

                
                            
                }

            }


             


        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

    }

    public function UserDeleteFiles(Request $request,$id)
    {
        $user=Auth::user();
        $filename =  $request->get('filename');
        
        $pdffiles = ClientsDocuments::where('user_id',$id)
        ->where('uploadedfilename',$filename)
        ->where('capture_type','uploaded')
        ->first();

        if(isset($pdffiles) && !empty($pdffiles)):

           $pdffiles->delete();
           unlink($pdffiles->path);

        endif;

        $request = new \Illuminate\Http\Request();
        $request->ajax='true';
        $request->id=$id;
        $request->successmessage='Document Deleted Successfully';
        return $this->UserDeathDetails($request,$id);
    } 

    public function UserStreamFiles(Request $request,$id,$filename)
    {
        $document = ClientsDocuments::where('user_id',$id)
        ->where('uploadedfilename',$filename)
        ->where('capture_type','uploaded')
        ->first();
        $data['textdata'] = $document;
        return response()->file($document->path);


       
    }


    function Encrypt($text)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_encrypt($text, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }


    function Decrypt($text)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_decrypt(base64_decode($text), $encrypt_method, $key, 0, $iv);
        return $output;
    }

    

   

}