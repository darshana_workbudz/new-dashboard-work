<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Http\Controllers\SendInBlueController;
use App\Http\Controllers\ZiggeoController;
use App\Models\UserAuthorizedPerson;
use App\Models\ZiggeoUserVideos;
use App\Models\ZiggeoUserAudios;
use App\Models\ZiggeoUserTexts;
use App\Models\MyRecipients;
use App\Models\UserAddonPackages;
use App\Models\UserStandAlonePackages;
use Storage;
use File;
use Ziggeo;
use Log;

class CreateContentController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
        
    }



    public function CreateContent(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type :'all-content';
        $data['contenttype'] = $contenttype;
        if ($isajax == 'true'):

            $html = view('my-content.create', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('create-content').$contenttype]);

        else:

            return view('my-content.create', $data);

        endif;
    }


    


    

}