<?php

namespace App\Http\Controllers;
use App\Models\UserSubscriptionPackages;
use App\Models\OrganisationSubscriptionPackages;
use App\Models\UserAddonPackages;
use App\Models\UserPackages;
use App\Models\Packages;
use App\Models\PromotionalCodes;
use App\Models\User;
use App\Models\UserInvoices;
use App\Models\UserReceipts;
use App\Models\StripeWebhookEvents;
use App\Models\ZiggeoUserVideos;
use App\Models\ZiggeoUserTexts;
use App\Models\ZiggeoUserAudios;
use App\Models\Coupons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Log;
use \Stripe\Stripe;
use \Carbon\Carbon;
use Hash;
use Session;
use App\Http\Controllers\SendInBlueController;

class WebhookController extends Controller
{
    public function handleWebhook()
    {

        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey(env('API_KEY_STRIPE'));

        // You can find your endpoint's secret in your webhook settings
        $endpoint_secret = env('API_WEBHOOK_SECRET_STRIPE');
        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch (\UnexpectedValueException $e) {
            
            
            http_response_code(400);
            exit();
        } catch (\Stripe\Exception\SignatureVerificationException $e) {
            
            
            http_response_code(400);
            exit();
        }

        try {



            $stripedata=StripeWebhookEvents::where('event_id',$event['id'])->first();

            if(isset($stripedata) && !empty($stripedata)):

              if($stripedata->jobstatus!==1):
                $event=$this->checkEventType($event->type,$event);

            else:

               return response(['status' => 'success', 'message' =>"Event Already Completed"], 200);

           endif;



       else:

        DB::beginTransaction();

        $stripedata=StripeWebhookEvents::create([   
            'event_id'=>$event->id,
            'jobstatus'=>0,
            'eventjson'=>json_encode($event->data->object),
            'eventtype'=>$event->type,
        ]);

        DB::commit();

        $event=$this->checkEventType($event->type,$event);

    endif;

    

    if($event==200):

        DB::beginTransaction();    

        $stripedata->jobstatus=1;
        $eventupdated=$stripedata->save();

        DB::commit();

        if($eventupdated):
            return response(['status' => 'success', 'message' =>"Event Completed Successfully"], 200);  
        endif;

    endif;


}
catch(Exception $e) { 

    $exception = $e->getMessage(); 
    return response(['status' => 'fail', 'message' =>$exception], 500);
}

}

public function checkEventType($type, $event)
{
    switch ($type) {
        case 'customer.subscription.created':
        return  $this->customer_subscription_created($event->data->object,$type);
        break;
        case 'customer.subscription.updated':
        return  $this->customer_subscription_updated($event->data->object,$type);
        break;
        case 'customer.subscription.deleted':
        return $this->customer_subscription_deleted($event->data->object,$type);
        break;
        case 'invoice.created':
        return $this->invoice_created($event->data->object,$type);
        break;
        case 'invoice.updated':
        return  $this->invoice_updated($event->data->object,$type);
        break;
        case 'invoice.upcoming':
        return $this->invoice_upcoming($event->data->object,$type);
        break;
        case 'customer.subscription.trial_will_end':
        return $this->customer_subscription_trial_will_end($event->data->object,$type);
        break;
        case 'checkout.session.completed':
        return $this->checkout_session_completed($event->data->object,$type);
        break;
        case 'payment_intent.payment_failed':
        return $this->payment_intent_payment_failed($event->data->object,$type);
        break;
        case 'payment_intent.succeeded':
        return $this->payment_intent_succeeded($event->data->object,$type);
        break;
        case 'charge.succeeded':
        return $this->charge_succeeded($event->data->object,$type);
        break;
        case 'coupon.updated':
        return $this->coupon_updated($event->data->object,$type);
        break;
        case 'coupon.created':
        return $this->coupon_created($event->data->object,$type);
        break;
        case 'promotion_code.updated':
        return $this->promotion_code_updated($event->data->object,$type);
        break;
        case 'promotion_code.created':
        return $this->promotion_code_created($event->data->object,$type);
        break;
        break;
    }
}

public function checkout_session_completed($object,$type)
{

    $session_id=$object['id'];

    try {


        $checkoutsession = \Stripe\Checkout\Session::retrieve([
          'id' =>$session_id,
          'expand' => ['line_items'],
      ]);
        

        if(isset($checkoutsession) && !empty($checkoutsession)):
        Log::info($checkoutsession);
        $user=User::FindByStripe($checkoutsession['customer']);

        if(empty($user)):

                $email=$checkoutsession['customer_details']['email'];

                DB::beginTransaction();
                $url = $this->GenerateVerificationlink(uniqid().'-'.$email);

                $user = User::create([

                    'email' => $email,
                    'password' => Hash::make('EVAHELD-UNKNOWN-USER'),
                    'isactive' => 0,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'user_stage_id' => 1,
                    'verification_link' => $url,
                    'verification_expiry_time' => Carbon::now()->addHours(24)->format('Y-m-d H:i:s'),
                    'stripe_customer_id'=>$checkoutsession['customer']
                ]);

                $params = array(
                    'url' => $url,
                    'subject' => 'Activate Account'
                );

                SendInBlueController::SendTransactionEmail((int) env('EMAIL_VERIFICATION_TEMPLATE'),$email,$params);

                $sib_contact_id=null;
                $contactcreated = SendInBlueController::CreateContact($email);

                if(isset($contactcreated['code']) && $contactcreated['code']==200):
                
                    $sib_contact_id=$contactcreated['response']['id'];

                endif;

                if(isset($contactcreated['code']) && $contactcreated['code']!==200) :

                    if($contactcreated['response']['code']=='duplicate_parameter'):

                    $contactdetails=SendInBlueController::GetContactDetails($email);

                        if(isset($contactdetails['code']) && $contactdetails['code']==200):
                            $sib_contact_id=$contactdetails['response']['id'];
                        endif;

                    endif;

                endif;

                
                $user->sib_contact_id = $sib_contact_id;
                $user->save();

                DB::commit();
        endif;

        $mode=$checkoutsession['mode'];

        $payment_status=$checkoutsession['payment_status'];

        $line_items=$checkoutsession['line_items'];

        $metadata=$checkoutsession['metadata'];

        if(isset($line_items['data']) && !empty($line_items['data'])):

        $productlist=$line_items['data'];
       
        if(isset($productlist) && count($productlist)>0):

            DB::beginTransaction();

            foreach ($productlist as $key => $valueproduct):

                $line_item_id=$valueproduct['line_item_id'];
                $quantity=$valueproduct['quantity'];
                $price=$valueproduct['price'];
                $productid=$price['product'];
                $stripe_price_id=$price['id'];
                $amountsubtotal=$price['unit_amount']/100;
                $type=$price['type'];

                

                $productdata=Packages::where('stripe_product_id',$productid)->first();

                
               
                if(isset($productdata) && !empty($productdata)):

                    $slug=$productdata->slug;
                    $type=$productdata->type;

                    $message_value=$productdata->message_value;
                    $recipient_value=$productdata->recipient_value;
                    $written_value=$productdata->written_value;

                    if(($type==3) && isset($productdata->linked_to_package_id) && !empty($productdata->linked_to_package_id)):

                      $linkproductdata=Packages::find($productdata->linked_to_package_id);

                      if(isset($linkproductdata) && !empty($linkproductdata)):
                        $message_value=$linkproductdata->message_value;
                        $recipient_value=$linkproductdata->recipient_value;
                        $written_value=$linkproductdata->written_value;
                      endif;

                    endif;



                    

                    for ($productcounter = 1; $productcounter <= $quantity; $productcounter++) {

                        $qrid=null;



                        if($productdata->slug=='addon-qr' || $productdata->slug=='standalone-qr'):
                            
                              if(isset($metadata['messageid']) && !empty($metadata['messageid'])):
                                $messagetype=null;

                                if($metadata['messagetype']=='video'):
                                  $messagetype=1;
                                endif;
                                if($metadata['messagetype']=='audio'):
                                  $messagetype=2;
                                endif;
                                if($metadata['messagetype']=='letter'):
                                  $messagetype=3;
                                endif;

                                $recipientencodedcode=$this->Encrypt('recipient-/-'.$messagetype."-/-".$metadata['messageid']);

                                $dataurl=url('/')."/message-verification/".$recipientencodedcode;
                                
                                else:

                                $dataurl=url('/')."/message-verification/";

                              endif;

                              $curl = curl_init();

                              $CURLOPT_POSTFIELDS=[
                                  "apikey"=>"eWpSNXijB0c26wtsG3HQ",
                                  "data"=>$dataurl,
                                  "transparent"=>"on",
                                  "frontcolor"=>"#000000",
                                  "marker_out_color"=>"#000000",
                                  "marker_in_color"=>"#000000",
                                  "pattern"=>"default",
                                  "marker"=>"default",
                                  "marker_in"=>"default",
                                  "title"=>$productdata->slug.'-'.date('Ymdhis'),
                                  "optionlogo"=>"none",
                                  "qrtype"=>"dynamic"
                              ];

                              curl_setopt_array($curl, array(
                                CURLOPT_URL => 'https://api.qr.io/v1/create',
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => '',
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 0,
                                CURLOPT_FOLLOWLOCATION => true,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => 'POST',
                                CURLOPT_POSTFIELDS =>json_encode($CURLOPT_POSTFIELDS),
                                CURLOPT_HTTPHEADER => array(
                                  'Content-Type: application/json'
                                ),
                              ));

                              $response = curl_exec($curl);
                              $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                              $response=json_decode($response,true);
                              
                              curl_close($curl);

                              if($http_status==200 && isset($response['qrid']) && !empty($response['qrid'])):

                                 $qrid=$response['qrid'];

                              endif;


                        endif;

                        $package=[

                            'user_id'=>isset($user->user_role_type) && $user->user_role_type==2 ? $user->id : null,
                            'package_id'=>$productdata->package_id,
                            'package_start_date'=>date('Y-m-d H:i:s'),
                            'package_end_date'=>date('Y-m-d H:i:s', strtotime('+1 year')),
                            'status'=>1,
                            'message_value'=>$message_value,
                            'recipient_value'=>$recipient_value,
                            'written_value'=>$written_value,
                            'stripe_price_id'=>$stripe_price_id,
                            'stripe_product_id'=>$productdata->stripe_product_id,
                            'stripe_subscription_id'=>null,
                            'created_at'=>Carbon::now(),
                            'created_by'=>$user->id,
                            'updated_at'=>Carbon::now(),
                            'qrid'=>isset($qrid) && !empty($qrid) ? $qrid :null,
                            'is_organisation_package'=>isset($user->user_role_type) && $user->user_role_type!==2 ? 1 : 0,
                            'organisation_user_id'=>isset($user->user_role_type) && $user->user_role_type!==2 ? $user->id : null

                        ];

                        
                       

                        if($productdata->type==2):

                                $created=UserAddonPackages::create($package);
                                
                                if($created):

                                  $created->package_code=$productdata->slug.'-'.$this->generate_uuid().$created->user_addon_package_id;
                                  $created->price=$amountsubtotal;
                                  if(isset($metadata['messageid']) && !empty($metadata['messageid'])):
                                    $created->linked_message_id=$metadata['messageid'];
                                    $created->linked_message_type=$metadata['messagetype'];
                                  endif;
                                    
                                    $created->save();


                                    if(isset($metadata['messageid']) && !empty($metadata['messageid'])):

                                      if($metadata['messagetype']=='letter'):

                                        $messagedata=ZiggeoUserTexts::find($metadata['messageid']);

                                      endif;

                                      if($metadata['messagetype']=='video'):

                                        $messagedata=ZiggeoUserVideos::find($metadata['messageid']);

                                      endif;

                                      if($metadata['messagetype']=='audio'):

                                        $messagedata=ZiggeoUserAudios::find($metadata['messageid']);

                                      endif;

                                      if($productdata->slug=='addon-qr'):
                                        
                                        $messagedata->qr_package_id=$created->user_addon_package_id;
                                        $messagedata->qr_package_type=$productdata->type;
                                        $messagedata->qrid=$qrid;
                                        $messagedata->qrdeliverystyle=2;

                                      endif;

                                      if($productdata->slug=='addon-heirloom'):
                                        $messagedata->delivery_style=2;
                                        $messagedata->heilroom_package_id=$created->user_addon_package_id;
                                        $messagedata->heilroom_package_type=$productdata->type;

                                      endif;

                                      $messagedata->save();

                                    endif;

                                endif;

                        endif;    

                       

                        if($productdata->type==3 || $productdata->type==1):

                            $created=UserPackages::create($package);

                            if($created):



                               $created->package_code=$productdata->slug.'-'.$this->generate_uuid().$created->user_package_id; 
                               $created->type=$productdata->type;
                               $created->price=$amountsubtotal;
                               $created->save();

                               $user->user_package_id=$productdata->package_id;
                               $user->save();
                               
                               if(isset($productdata->include_addon_package_id) && !empty($productdata->include_addon_package_id)):

                                 $addonpackagedata=Packages::find($productdata->include_addon_package_id);


                                 $addonpackage=[
                                    'user_package_id'=>$created->user_package_id,
                                    'user_id'=>isset($user->user_role_type) && $user->user_role_type==2 ? $user->id : null,
                                    'package_id'=>$addonpackagedata->package_id,
                                    'package_start_date'=>date('Y-m-d H:i:s'),
                                    'package_end_date'=>date('Y-m-d H:i:s', strtotime('+1 year')),
                                    'status'=>1,
                                    'message_value'=>0,
                                    'recipient_value'=>0,
                                    'written_value'=>0,
                                    'stripe_price_id'=>$addonpackagedata->stripe_price_id,
                                    'stripe_product_id'=>$addonpackagedata->stripe_product_id,
                                    'stripe_subscription_id'=>null,
                                    'created_at'=>Carbon::now(),
                                    'updated_at'=>Carbon::now(),
                                    'qrid'=>isset($qrid) && !empty($qrid) ? $qrid :null,
                                    'is_organisation_package'=>isset($user->user_role_type) && $user->user_role_type!==2 ? 1 : 0,
                                    'organisation_user_id'=>isset($user->user_role_type) && $user->user_role_type!==2 ? $user->id : null

                                ];

                                $addoncreated=UserAddonPackages::create($addonpackage);
                                if($addoncreated):
                                  $addoncreated->package_code=$addonpackagedata->slug.'-'.$this->generate_uuid().$addoncreated->user_addon_package_id;
                                  $addoncreated->save();
                                endif;  

                               endif;



                                if($productdata->package_id=='1')
                                {
                                  $listid=env('Free_Community_Users_List');
                                }

                                if($productdata->package_id=='2')
                                {
                                  $listid=env('Connection_Users_List');
                                }

                                if($productdata->package_id=='3')
                                {
                                  $listid=env('Legacy_Users_List');
                                }

                                if($productdata->package_id=='4')
                                {
                                  $listid=env('Heirloom_Connection_Users_List');
                                }

                                if($productdata->package_id=='5')
                                {
                                  $listid=env('QR_Connection_Users_List');
                                }

                                if($productdata->package_id=='8')
                                {
                                  $listid=env('QR_Legacy_Users_List');
                                }

                                if(isset($listid)):
                                  SendInBlueController::UpdateContacttolist($user->email,$listid);
                                endif;
                                 
                                

                            endif;

                        endif;

                    }


                endif;


            endforeach;
            DB::commit();

        return http_response_code(200);
        endif;

    endif;

endif;

return http_response_code(500);

}catch(Exception $e) {
    
    DB::rollback();

    $api_error = $e->getMessage();

    return http_response_code(500);
}




}


public function customer_subscription_created($object,$type)
{

    
    try {   
            DB::beginTransaction();  

            $planobject=isset($object['items']['data'][0]['plan']) ? $object['items']['data'][0]['plan'] : array();



            $user=User::FindByStripe($object['customer']); 

            $productdata=Packages::where('stripe_product_id',$planobject['product'])->where('type',1)->first();


            if(isset($productdata) && !empty($productdata) && !empty($user)):

            $subscription=array(

                  "user_id"=>$user->id,
                  "package_id"=>$productdata->package_id,
                  "stripe_customer_id"=>$object['customer'],
                  "stripe_subscription_id"=>$object['id'],
                  "stripe_product_id"=>isset($planobject['product']) ? $planobject['product'] :null,
                  "stripe_price_id"=>isset($planobject['id']) ? $planobject['id'] :null,
                  "application_fee_percent"=>$object['application_fee_percent'],
                  "billing_cycle_anchor"=>$object['billing_cycle_anchor'],
                  "cancel_at"=>$object['cancel_at'],
                  "cancel_at_period_end"=>$object['cancel_at_period_end'],
                  "canceled_at"=>$object['canceled_at'],
                  "collection_method"=>$object['collection_method'],
                  "created_at"=>date('Y-m-d H:i:s',($object['created'])),
                  "current_period_end"=>$object['current_period_end'],
                  "current_period_start"=>$object['current_period_start'],
                  "days_until_due"=>$object['days_until_due'],
                  "default_payment_method"=>$object['default_payment_method'],
                  "default_source"=>$object['default_source'],
                  "default_tax_rates"=>json_encode($object['default_tax_rates']),
                  "discount"=>(is_array($object['discount'])) ? json_encode($object['discount']) : $object['discount'],
                  "ended_at"=>$object['ended_at'],
                  "latest_invoice"=>$object['latest_invoice'],
                  "quantity"=>$object['quantity'],
                  "schedule"=>$object['schedule'],
                  "start_date"=>$object['start_date'],
                  "status"=>$object['status'],
                  "trial_end"=>$object['trial_end'],
                  "trial_start"=>$object['trial_start'],
                  "plan_interval"=>isset($planobject['interval']) ? $planobject['interval'] :null,
                  "plan_interval_count"=>isset($planobject['interval_count']) ? $planobject['interval_count'] :null,
                  "plan_amount"=>isset($planobject['amount']) && !empty($planobject['amount']) ? $planobject['amount']/100 :0,
                  "plan_active"=>isset($planobject['active']) ? $planobject['active'] :null,
                  "plan_trial_period_days"=>isset($planobject['trial_period_days']) ? $planobject['trial_period_days'] :null,
                  "message_value"=>$productdata->message_value,
                  "recipient_value"=>$productdata->recipient_value,
                  "written_value"=>$productdata->written_value,
                  "updated_at"=>Carbon::now()
                  

            );

            $created=UserSubscriptionPackages::create($subscription);

            if($created):
              $created->package_code=$productdata->slug.'-'.$this->generate_uuid().$created->user_subscription_package_id;
              $created->save();
              $user->stripe_subscription_id=$object['id'];
              $user->hasdemopackage=0;
              $user->save();

            endif;

            if($created && ($user->hasRole('B2B Client') || $user->hasRole('B2B Partner'))):

                    $quantity=$object['quantity'];

                    for ($productcounter = 1; $productcounter <= $quantity; $productcounter++) {

                       

                        $package=[

                            "user_subscription_package_id"=>$created->user_subscription_package_id,
                            "user_id"=>$user->id,
                            "package_id"=>$productdata->package_id,
                            "stripe_subscription_id"=>$object['id'],
                            "stripe_customer_id"=>$object['customer'],
                           
                            "created_by"=>$user->id,
                            "updated_by"=>$user->id,
                            "created_at"=>date('Y-m-d H:i:s',($object['created'])),
                            "updated_at"=>Carbon::now()
                            
                        ];



                        $created=OrganisationSubscriptionPackages::create($package); 

                        if($created):

                         $created->package_code=$productdata->slug.'-'.$this->generate_uuid().$created->organisation_subscription_package_id;
                          
                         $created->save();

                        endif;

                        

                    }


            endif;

            DB::commit();

            if($created)
            {
              return http_response_code(200);
            }

          endif;

        }
        catch(Exception $e) {
    
            DB::rollback();

            $api_error = $e->getMessage();

            return http_response_code(500);
        }
    
}


public function customer_subscription_updated($object,$type)
{

    
    try {   
            DB::beginTransaction();  

            $stripe_subscription_id=$object['id'];
            $planobject=isset($object['items']['data'][0]['plan']) ? $object['items']['data'][0]['plan'] : array();
            $user=User::FindByStripe($object['customer']); 
            $productdata=Packages::where('stripe_product_id',$planobject['product'])->where('type',1)->first();

            $subscriptiondata=UserSubscriptionPackages::FindByStripe($stripe_subscription_id);

            if(isset($productdata) && !empty($productdata) && !empty($user) && !empty($subscriptiondata)):

            $subscription=array(
                  "package_id"=>$productdata->package_id,
                  "stripe_product_id"=>isset($planobject['product']) ? $planobject['product'] :null,
                  "stripe_price_id"=>isset($planobject['id']) ? $planobject['id'] :null,
                  "application_fee_percent"=>$object['application_fee_percent'],
                  "billing_cycle_anchor"=>$object['billing_cycle_anchor'],
                  "cancel_at"=>$object['cancel_at'],
                  "cancel_at_period_end"=>$object['cancel_at_period_end'],
                  "canceled_at"=>$object['canceled_at'],
                  "collection_method"=>$object['collection_method'],
                  "current_period_end"=>$object['current_period_end'],
                  "current_period_start"=>$object['current_period_start'],
                  "days_until_due"=>$object['days_until_due'],
                  "default_payment_method"=>$object['default_payment_method'],
                  "default_source"=>$object['default_source'],
                  "default_tax_rates"=>json_encode($object['default_tax_rates']),
                  "discount"=>(is_array($object['discount'])) ? json_encode($object['discount']) : $object['discount'],
                  "ended_at"=>$object['ended_at'],
                  "latest_invoice"=>$object['latest_invoice'],
                  "quantity"=>$object['quantity'],
                  "schedule"=>$object['schedule'],
                  "start_date"=>$object['start_date'],
                  "status"=>$object['status'],
                  "trial_end"=>$object['trial_end'],
                  "trial_start"=>$object['trial_start'],
                  "plan_interval"=>isset($planobject['interval']) ? $planobject['interval'] :null,
                  "plan_interval_count"=>isset($planobject['interval_count']) ? $planobject['interval_count'] :null,
                  "plan_amount"=>isset($planobject['amount']) && !empty($planobject['amount']) ? $planobject['amount']/100 :0,
                  "plan_active"=>isset($planobject['active']) ? $planobject['active'] :null,
                  "plan_trial_period_days"=>isset($planobject['trial_period_days']) ? $planobject['trial_period_days'] :null,
                  'message_value'=>$productdata->message_value,
                  'recipient_value'=>$productdata->recipient_value,
                  'written_value'=>$productdata->written_value,
                  "updated_at"=>Carbon::now()

            );

            
            
            $updated=$subscriptiondata->update($subscription);

            DB::commit();

            if($updated)
            {

              return http_response_code(200);
            }

          endif;
        }
        catch(Exception $e) {
    
            DB::rollback();

            $api_error = $e->getMessage();

            return http_response_code(500);
        }
    
}

public function customer_subscription_deleted($object,$type)
{

    
    try {   
            DB::beginTransaction();  

            $stripe_subscription_id=$object['id'];
            $planobject=isset($object['items']['data'][0]['plan']) ? $object['items']['data'][0]['plan'] : array();
            $user=User::FindByStripe($object['customer']); 
            $productdata=Packages::where('stripe_product_id',$planobject['product'])->where('type',1)->first();

            $subscriptiondata=UserSubscriptionPackages::FindByStripe($stripe_subscription_id);

            if(isset($productdata) && !empty($productdata) && !empty($user) && !empty($subscriptiondata)):

            $subscription=array(

                  "package_id"=>$productdata->package_id,
                  "stripe_product_id"=>isset($planobject['product']) ? $planobject['product'] :null,
                  "stripe_price_id"=>isset($planobject['id']) ? $planobject['id'] :null,
                  "application_fee_percent"=>$object['application_fee_percent'],
                  "billing_cycle_anchor"=>$object['billing_cycle_anchor'],
                  "cancel_at"=>$object['cancel_at'],
                  "cancel_at_period_end"=>$object['cancel_at_period_end'],
                  "canceled_at"=>$object['canceled_at'],
                  "collection_method"=>$object['collection_method'],
                  "current_period_end"=>$object['current_period_end'],
                  "current_period_start"=>$object['current_period_start'],
                  "days_until_due"=>$object['days_until_due'],
                  "default_payment_method"=>$object['default_payment_method'],
                  "default_source"=>$object['default_source'],
                  "default_tax_rates"=>json_encode($object['default_tax_rates']),
                  "discount"=>(is_array($object['discount'])) ? json_encode($object['discount']) : $object['discount'],
                  "ended_at"=>$object['ended_at'],
                  "latest_invoice"=>$object['latest_invoice'],
                  "quantity"=>$object['quantity'],
                  "schedule"=>$object['schedule'],
                  "start_date"=>$object['start_date'],
                  "status"=>$object['status'],
                  "trial_end"=>$object['trial_end'],
                  "trial_start"=>$object['trial_start'],
                  "plan_interval"=>isset($planobject['interval']) ? $planobject['interval'] :null,
                  "plan_interval_count"=>isset($planobject['interval_count']) ? $planobject['interval_count'] :null,
                  "plan_amount"=>isset($planobject['amount']) && !empty($planobject['amount']) ? $planobject['amount']/100 :0,
                  "plan_active"=>isset($planobject['active']) ? $planobject['active'] :null,
                  "plan_trial_period_days"=>isset($planobject['trial_period_days']) ? $planobject['trial_period_days'] :null,
                  'message_value'=>$productdata->message_value,
                  'recipient_value'=>$productdata->recipient_value,
                  'written_value'=>$productdata->written_value,
                  "updated_at"=>Carbon::now()

            );

            $updated=$subscriptiondata->update($subscription);

            if($updated)
            {
              $user->stripe_subscription_id=null;
              $user->save();
            }
            DB::commit();

            if($updated)
            {
              return http_response_code(200);
            }

          endif;
        }
        catch(Exception $e) {
    
            DB::rollback();

            $api_error = $e->getMessage();

            return http_response_code(500);
        }
    
}

public function payment_intent_succeeded($object,$type)
{
 return http_response_code(200);
 
}

public function charge_succeeded($object,$type)
{

    
    try {   
             
            DB::beginTransaction();   
            $user=User::FindByStripe($object['customer']); 

            if(isset($user) && !empty($user)):

            $recieptdata=array(

                  "stripe_charge_id"=>$object['id'],
                  "user_id"=>$user->id,
                  "stripe_customer_id"=>$object['customer'],
                  "reciept_number"=>$object['reciept_number'],
                  "receipt_url"=>$object['receipt_url'],
                  "amount_captured"=>isset($object['amount_captured']) && !empty($object['amount_captured']) ? $object['amount_captured']/100 :0,
                  "amount"=>isset($object['amount']) && !empty($object['amount']) ? $object['amount']/100 :0,
                  "amount_refunded"=>isset($object['amount_refunded']) && !empty($object['amount_refunded']) ? $object['amount_refunded']/100 :0,
                  "receipt_email"=>$object['receipt_email'],
                  "created_at"=> date('Y-m-d H:i:s',($object['created'])),
                  "customer_address"=>$object['customer_address'],
                  "customer_email"=>$object['receipt_email'],
                  "customer_name"=>$object['customer_name'],
                  "customer_phone"=>$object['customer_phone'],
                  "paid"=>$object['paid'],
                  "payment_intent"=>$object['payment_intent'],
                  "status"=>$object['status'],
                  "updated_at"=>Carbon::now()
                  
            );

            $created=UserReceipts::create($recieptdata);

            DB::commit();

            if($created)
            {
              return http_response_code(200);
            }

            endif;

            
            
        }
        catch(Exception $e) {
    
            
            $api_error = $e->getMessage();

            return http_response_code(500);
        }
    
}


public function invoice_created($object,$type)
{

 try {   
            DB::beginTransaction();  

            $user=User::FindByStripe($object['customer']); 

            if(isset($user) && !empty($user)):

            $invoicedata=array(

                  "stripe_invoice_id"=>$object['id'],
                  "user_id"=>$user->id,
                  "stripe_customer_id"=>$object['customer'],
                  "account_country"=>$object['account_country'],
                  "account_name"=>$object['account_name'],
                  "amount_due"=>isset($object['amount_due']) && !empty($object['amount_due']) ? $object['amount_due']/100 :0,
                  "amount_paid"=>isset($object['amount_paid']) && !empty($object['amount_paid']) ? $object['amount_paid']/100 :0,
                  "amount_remaining"=>isset($object['amount_remaining']) && !empty($object['amount_remaining']) ? $object['amount_remaining']/100 :0,
                  "attempt_count"=>$object['attempt_count'],
                  "attempted"=>$object['attempted'],
                  "collection_method"=>$object['collection_method'],
                  "created_at"=> date('Y-m-d H:i:s',($object['created'])),
                  "customer_address"=>$object['customer_address'],
                  "customer_email"=>$object['customer_email'],
                  "customer_name"=>$object['customer_name'],
                  "customer_phone"=>$object['customer_phone'],
                  "customer_shipping"=>$object['customer_shipping'],
                  "discount"=>$object['discount'],
                  "discounts"=>isset($object['discounts']) ? json_encode($object['discounts']) : null,
                  "due_date"=>$object['due_date'],
                  "hosted_invoice_url"=>$object['hosted_invoice_url'],
                  "invoice_pdf"=>$object['invoice_pdf'],
                  "lineitems"=>isset($object['lines']) ? json_encode($object['lines']) : null,
                  "next_payment_attempt"=>$object['next_payment_attempt'],
                  "number"=>$object['number'],
                  "paid"=>$object['paid'],
                  "payment_intent"=>$object['payment_intent'],
                  "period_end"=>$object['period_end'],
                  "period_start"=>$object['period_start'],
                  "receipt_number"=>$object['receipt_number'],
                  "status"=>$object['status'],
                  "status_transitions"=>isset($object['status_transitions']) ? json_encode($object['status_transitions']) : null,
                  "subscription"=>$object['subscription'],
                  "subtotal"=>isset($object['subtotal']) && !empty($object['subtotal']) ? $object['subtotal']/100 :0,
                  "total"=>isset($object['total']) && !empty($object['total']) ? $object['total']/100 :0,
                  "total_discount_amounts"=>isset($object['total_discount_amounts']) ? json_encode($object['total_discount_amounts']) : null,
                  "total_tax_amounts"=>isset($object['total_tax_amounts']) ? json_encode($object['total_tax_amounts']) : null,
                  "updated_at"=>Carbon::now()

            );

            $created=UserInvoices::create($invoicedata);

            DB::commit();

            if($created)
            {
              return http_response_code(200);
            }

          endif;

        }
        catch(Exception $e) {
    
            DB::rollback();

            $api_error = $e->getMessage();

            return http_response_code(500);
        }
 
}

public function payment_intent_created($object,$type)
{
  return http_response_code(200);
 
}



public function payment_intent_payment_failed($object,$type)
{

  return http_response_code(200);
}

public function coupon_updated($object,$type)
{
  Log::info($object);
  try {   
            DB::beginTransaction();  

           

            $couponsdata=array(

                  'stripe_coupon_id'=>$object['id'],
                  'object'=>$object['object'],
                  'amount_off'=>$object['amount_off'],
                  'created'=>$object['created'],
                  'currency'=>$object['currency'],
                  'duration'=>$object['duration'],
                  'livemode'=>$object['livemode'],
                  'max_redemptions'=>$object['max_redemptions'],
                  'metadata'=>json_encode($object['metadata']),
                  'name'=>$object['name'],
                  'percent_off'=>$object['percent_off'],
                  'redeem_by'=>$object['redeem_by'],
                  'times_redeemed'=>$object['times_redeemed'],
                  'valid'=>$object['valid'],
                  'type'=>(isset($object['amount_off']) && !empty($object['amount_off'])) ? 2: 1

            );

            $updated=Coupons::where('stripe_coupon_id',$object['id'])->update($couponsdata);

            DB::commit();

            if($updated)
            {
              return http_response_code(200);
            }

         

        }
        catch(Exception $e) {
    
            DB::rollback();

            $api_error = $e->getMessage();

            return http_response_code(500);
        }
}

public function coupon_created($object,$type)
{
  Log::info($object);

  try {   
            DB::beginTransaction();  

           

            $couponsdata=array(

                  'stripe_coupon_id'=>$object['id'],
                  'object'=>$object['object'],
                  'amount_off'=>$object['amount_off'],
                  'created'=>$object['created'],
                  'currency'=>$object['currency'],
                  'duration'=>$object['duration'],
                  'livemode'=>$object['livemode'],
                  'max_redemptions'=>$object['max_redemptions'],
                  'metadata'=>json_encode($object['metadata']),
                  'name'=>$object['name'],
                  'percent_off'=>$object['percent_off'],
                  'redeem_by'=>$object['redeem_by'],
                  'times_redeemed'=>$object['times_redeemed'],
                  'valid'=>$object['valid'],
                  'type'=>(isset($object['amount_off']) && !empty($object['amount_off'])) ? 2: 1

            );

            $created=Coupons::create($couponsdata);

            DB::commit();

            if($created)
            {
              return http_response_code(200);
            }

         

        }
        catch(Exception $e) {
    
            DB::rollback();

            $api_error = $e->getMessage();

            return http_response_code(500);
        }
}

public function promotion_code_updated($object,$type)
{
 Log::info($object);
 try {   
            DB::beginTransaction();  

           

             $couponsdata=array(

                    'object'=>$object['object'],
                    'created'=>$object['created'],
                    'livemode'=>$object['livemode'],
                    'max_redemptions'=>$object['max_redemptions'],
                    'metadata'=>$object['metadata'],
                    'code'=>$object['code'],
                    'restrictions'=>$object['restrictions'],
                    'times_redeemed'=>$object['times_redeemed'],
                    'active'=>$object['active'],
                    'expires_at'=>$object['expires_at'],
                    'customer'=>$object['customer']
                    
                  

            );

            $updated=PromotionalCodes::where('code',$object['code'])->update($couponsdata);

            DB::commit();

            if($updated)
            {
              return http_response_code(200);
            }

         

        }
        catch(Exception $e) {
    
            DB::rollback();

            $api_error = $e->getMessage();

            return http_response_code(500);
        }
}

public function promotion_code_created($object,$type)
{
 Log::info($object);
  
 try {   
            DB::beginTransaction();  

           

            $couponsdata=array(

                    'object'=>$object['object'],
                    'created'=>$object['created'],
                    'livemode'=>$object['livemode'],
                    'max_redemptions'=>$object['max_redemptions'],
                    'metadata'=>$object['metadata'],
                    'code'=>$object['code'],
                    'restrictions'=>$object['restrictions'],
                    'times_redeemed'=>$object['times_redeemed'],
                    'active'=>$object['active'],
                    'expires_at'=>$object['expires_at'],
                    'customer'=>$object['customer'],
                    'stripe_coupon_id'=>$object['coupon']['id']
                  

            );

            $created=PromotionalCodes::create($couponsdata);
            if($created):
                $coupons=Coupons::where('stripe_coupon_id',$object['coupon']['id'])->first();
                $created->coupon_id=$coupons->coupon_id;
                $created->save();
            endif;

            DB::commit();

            if($created)
            {
              return http_response_code(200);
            }

         

        }
        catch(Exception $e) {
    
            DB::rollback();

            $api_error = $e->getMessage();

            return http_response_code(500);
        }
  
}

public function invoice_updated($object,$type)
{

      try {   
            DB::beginTransaction();  

            $user=User::FindByStripe($object['customer']); 
            $invoice=UserInvoices::FindByStripe($object['id']); 

            if(isset($user) && !empty($user) && !empty($invoice)):

            $invoicedata=array(

                  "amount_due"=>isset($object['amount_due']) && !empty($object['amount_due']) ? $object['amount_due']/100 :0,
                  "amount_paid"=>isset($object['amount_paid']) && !empty($object['amount_paid']) ? $object['amount_paid']/100 :0,
                  "amount_remaining"=>isset($object['amount_remaining']) && !empty($object['amount_remaining']) ? $object['amount_remaining']/100 :0,
                  "attempt_count"=>$object['attempt_count'],
                  "attempted"=>$object['attempted'],
                  "collection_method"=>$object['collection_method'],
                  "customer_address"=>$object['customer_address'],
                  "customer_email"=>$object['customer_email'],
                  "customer_name"=>$object['customer_name'],
                  "customer_phone"=>$object['customer_phone'],
                  "customer_shipping"=>$object['customer_shipping'],
                  "discount"=>$object['discount'],
                  "discounts"=>isset($object['discounts']) ? json_encode($object['discounts']) : null,
                  "due_date"=>$object['due_date'],
                  "hosted_invoice_url"=>$object['hosted_invoice_url'],
                  "invoice_pdf"=>$object['invoice_pdf'],
                  "lineitems"=>isset($object['lines']) ? json_encode($object['lines']) : null,
                  "next_payment_attempt"=>$object['next_payment_attempt'],
                  "number"=>$object['number'],
                  "paid"=>$object['paid'],
                  "payment_intent"=>$object['payment_intent'],
                  "period_end"=>$object['period_end'],
                  "period_start"=>$object['period_start'],
                  "receipt_number"=>$object['receipt_number'],
                  "status"=>$object['status'],
                  "status_transitions"=>isset($object['status_transitions']) ? json_encode($object['status_transitions']) : null,
                  "subscription"=>$object['subscription'],
                  "subtotal"=>isset($object['subtotal']) && !empty($object['subtotal']) ? $object['subtotal']/100 :0,
                  "total"=>isset($object['total']) && !empty($object['total']) ? $object['total']/100 :0,
                  "total_discount_amounts"=>$object['total_discount_amounts'],
                  "total_tax_amounts"=>$object['total_tax_amounts'],
                  "updated_at"=>Carbon::now()

            );

            $updated=$invoice->update($invoicedata);

            DB::commit();

            if($updated)
            {
              return http_response_code(200);
            }

          endif;

        }
        catch(Exception $e) {
    
            DB::rollback();

            $api_error = $e->getMessage();

            return http_response_code(500);
        }
 
}

public function invoice_payment_failed($object,$type)
{
 return http_response_code(200);
 
}



public function invoice_upcoming($object,$type)
{
 return http_response_code(200);
   
}

public function customer_subscription_trial_will_end($object,$type)
{

    return http_response_code(200);
}

    public function GenerateVerificationlink($email)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_encrypt($email, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);

        $url = URL('verify-account') . "?key=" . $output;

        return $url;
    }


    public function Encrypt($text)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_encrypt($text, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }


    public function Decrypt($text)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_decrypt(base64_decode($text), $encrypt_method, $key, 0, $iv);
        return $output;
    }

    function generate_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        mt_rand( 0, 0xffff ),
        mt_rand( 0, 0x0C2f ) | 0x4000,
        mt_rand( 0, 0x3fff ) | 0x8000,
        mt_rand( 0, 0x2Aff ), mt_rand( 0, 0xffD3 ), mt_rand( 0, 0xff4B )
    );

}


}