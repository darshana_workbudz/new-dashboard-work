<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use SendinBlue;
use GuzzleHttp;
use SendinBlue\Client\ApiException;
class SendInBlueController extends Controller
{

	protected $config;
	protected $apiInstance;
	

	public static function SendTransactionEmail($templateId,$email,$params)
	{		
		$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key',env('API_KEY_SENDINBLUE'));

		$apiInstance=new SendinBlue\Client\Api\TransactionalEmailsApi(
			new GuzzleHttp\Client(['verify' => false ]),
			$config
		);

		$sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
		$sendSmtpEmail['sender'] = array(
			'name' => 'Evaheld Team',
			'email' => env('FROM_EMAIL_SENDINBLUE')
		);
		$sendSmtpEmail['to'] = array(
			array(
				'email' => $email,
				'name' => $email
			)
		);
		$sendSmtpEmail['templateId'] = $templateId;
		$sendSmtpEmail['params'] = $params;

		try 
	    {
	    	$result = $apiInstance->sendTransacEmail($sendSmtpEmail);
			
			return ['code'=>200,'status'=>'success','response'=>$result];
	    } 
	    catch (ApiException $e) {
	    			
            return ['code'=>$e->getCode(),'status'=>'fail','response'=>json_decode($e->getResponseBody(),true)];
        }
	}



	public static function CreateContact($email)
	{
		$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key',env('API_KEY_SENDINBLUE'));

		$apiInstance=new SendinBlue\Client\Api\ContactsApi(
			new GuzzleHttp\Client(['verify' => false ]),
			$config
		);
	    $createContact = new \SendinBlue\Client\Model\CreateContact(); // Values to create a contact
	    $createContact['email'] = $email;

	    

	    try 
	    {
	    	$result = $apiInstance->createContact($createContact);
			
			return ['code'=>200,'status'=>'success','response'=>$result];
	    } 
	    catch (ApiException $e) {
	    			
            return ['code'=>$e->getCode(),'status'=>'fail','response'=>json_decode($e->getResponseBody(),true)];

        }
	}


	public static function UpdateContact($email,$firstname,$lastname)
	{
		$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key',env('API_KEY_SENDINBLUE'));

		$apiInstance=new SendinBlue\Client\Api\ContactsApi(
			new GuzzleHttp\Client(['verify' => false ]),
			$config
		);
	    $identifier = $email;
		$updateContact = new \SendinBlue\Client\Model\UpdateContact();
		$updateContact['attributes'] = array('EMAIL'=>$email, 'FIRSTNAME'=>$firstname,'LASTNAME'=>$lastname);

	    

	    try 
	    {
	    	$result = $apiInstance->updateContact($identifier, $updateContact);
			
			return ['code'=>200,'status'=>'success','response'=>$result];
	    } 
	    catch (ApiException $e) {
	    			
            return ['code'=>$e->getCode(),'status'=>'fail','response'=>json_decode($e->getResponseBody(),true)];

        }
	}


	public static function GetContactDetails($identifier)
	{

		$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key',env('API_KEY_SENDINBLUE'));

		$apiInstance = new SendinBlue\Client\Api\ContactsApi(
			new GuzzleHttp\Client(['verify' => false ]),
			$config
		);

		try 
	    {
			$result = $apiInstance->getContactInfo($identifier);
			

			return ['code'=>200,'status'=>'success','response'=>$result];
		} 
	    catch (ApiException $e) {
	    			
            return ['code'=>$e->getCode(),'status'=>'fail','response'=>json_decode($e->getResponseBody(),true)];
                    
        }	
	}





	public static function UpdateContacttolist($identifier,$listId)
	{

		$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key',env('API_KEY_SENDINBLUE'));

		$apiInstance = new SendinBlue\Client\Api\ContactsApi(
			new GuzzleHttp\Client(['verify' => false ]),
			$config
		);

		try 
	    {
							$sendinbluedetails = $apiInstance->getContactInfo($identifier);

						
                            

	                        if(isset($sendinbluedetails['listIds']) && is_array($sendinbluedetails['listIds']) && !in_array($listId,$sendinbluedetails['listIds'])) :

	                            $contactIdentifiers = new \SendinBlue\Client\Model\AddContactToList();
								$contactIdentifiers['emails'] = array($identifier);

								$result = $apiInstance->addContactToList($listId, $contactIdentifiers);

								return ['code'=>200,'status'=>'success','response'=>$result];

	                        endif;

            			
			

			
		} 
	    catch (ApiException $e) {
	    			
            return ['code'=>$e->getCode(),'status'=>'fail','response'=>json_decode($e->getResponseBody(),true)];
                    
        }	
	}



	public static function SendSupportTransactionEmail($templateId,$email,$params,$firstname,$lastname)
	{		
		$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key',env('API_KEY_SENDINBLUE'));

		$apiInstance=new SendinBlue\Client\Api\TransactionalEmailsApi(
			new GuzzleHttp\Client(['verify' => false ]),
			$config
		);

		$sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
		$sendSmtpEmail['sender'] = array(
			'name' => $firstname.$lastname,
			'email' => $email
		);
		$sendSmtpEmail['to'] = array(
			array(
				'email' => 'support@evaheld.com',
				'name' => 'Support Evaheld' 
			)
		);
		$sendSmtpEmail['templateId'] = $templateId;
		$sendSmtpEmail['params'] = $params;

		try 
	    {
	    	$result = $apiInstance->sendTransacEmail($sendSmtpEmail);
			
			return ['code'=>200,'status'=>'success','response'=>$result];
	    } 
	    catch (ApiException $e) {
	    			
            return ['code'=>$e->getCode(),'status'=>'fail','response'=>json_decode($e->getResponseBody(),true)];
        }
	}


	

}