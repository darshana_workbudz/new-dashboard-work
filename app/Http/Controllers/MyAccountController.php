<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Http\Controllers\SendInBlueController;
use App\Http\Controllers\ZiggeoController;
use App\Models\UserAuthorizedPerson;
use App\Models\ZiggeoUserVideos;
use App\Models\ZiggeoUserAudios;
use App\Models\ZiggeoUserTexts;
use App\Models\HasAccessLogin;
use App\Models\MyRecipients;
use App\Models\UserAddonPackages;
use App\Models\UserStandAlonePackages;
use App\Models\UserPackages;
use App\Models\Packages;
use App\Models\UserInvoices;
use App\Models\UpgradablePackages;
use App\Models\UserReceipts;
use Storage;
use File;
use Ziggeo;
use Log;
use \Stripe\Stripe;


class MyAccountController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
        
    }


    public function index(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['hasdemopackage']=$user->hasdemopackage;
        if ($isajax == 'true'):

            $html = view('my-account-new', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-account')]);

        else:

            return view('my-account-new', $data);

        endif;
    }


    public function addonfeatures(Request $request)
    {

        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['error'] = $request->error;
        
        $usersubscription = UserPackages::GetActivePlan($user->id);
        $data['usersubscription']=$usersubscription;
        $data['qrcodestandalone'] = User::GetQrCodeStandalone();
        $data['handdeliveredstandalone'] = User::GetHanddeliveredStandalone();
        $data['community_packages_coming_soon'] = Packages::getAllComingSoonPackageData();
        $data['gettotalminutes']=User::GetTotalResource($user->id);
        $data['usedtotalminutes']=User::GetUsedMinutes($user->id);
        $data['userinvoices']=UserInvoices::GetByUser($user->id);
        $data['hasdemopackage']=$user->hasdemopackage;
        $data['useraddons']=UserAddonPackages::FindByStripe($user->id);
        if(isset($usersubscription) && !empty($usersubscription)):
        $data['extraaddons']=Packages::getExtraMinutesAddonPackages($usersubscription->package_id);
        $data['pagesaddons']=Packages::getExtraPagesAddonPackages($usersubscription->package_id);
        $data['heliroomaddon'] = Packages::getHeirLoomAddonPackages($usersubscription->package_id);
        $data['qraddon'] = Packages::getQrAddonPackages($usersubscription->package_id);
        endif;
        
        

        
        if ($isajax == 'true'):

            $html = view('my-account.add-on-features', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-account.add-on-features')]);

        else:

            return view('my-account.add-on-features', $data);

        endif;

    }


     public function mybilling(Request $request)
    {

        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['error'] = $request->error;
        // $usersubscription = UserPackages::GetActivePlan($user->id);
        // $data['usersubscription']=$usersubscription;
        // $data['gettotalminutes']=User::GetTotalResource($user->id);
        // $data['usedtotalminutes']=User::GetUsedMinutes($user->id);
        $data['receipts'] = UserReceipts::GetByUser($user->id);
        $data['hasdemopackage']=$user->hasdemopackage;
        // if(isset($usersubscription) && !empty($usersubscription)):
        // $data['extraaddons']=Packages::getExtraMinutesAddonPackages($usersubscription->package_id);
        // $data['pagesaddons']=Packages::getExtraPagesAddonPackages($usersubscription->package_id);
        // $data['heliroomaddon'] = Packages::getHeirLoomAddonPackages($usersubscription->package_id);
        // $data['qraddon'] = Packages::getQrAddonPackages($usersubscription->package_id);
        // endif;
        

        if ($isajax == 'true'):

            $html = view('my-account.my-billing', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-account.my-billing')]);

        else:

            return view('my-account.my-billing', $data);

        endif;

    }

     public function upgradeplan(Request $request)
    {

        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['error'] = $request->error;
        
        $usersubscription = UserPackages::GetActivePlan($user->id);
        $data['usersubscription']=$usersubscription;
        $data['gettotalminutes']=User::GetTotalResource($user->id);
        $data['usedtotalminutes']=User::GetUsedMinutes($user->id);
        $data['userinvoices']=UserInvoices::GetByUser($user->id);
        $data['community_packages_coming_soon'] = Packages::getAllComingSoonPackageData();
        $data['hasdemopackage']=$user->hasdemopackage;
        
        $data['useraddons']=UserAddonPackages::FindByStripe($user->id);
        if(isset($usersubscription) && !empty($usersubscription)):
        $data['upgradablepackages']=UpgradablePackages::GetUpgradablePackages($usersubscription->package_id,$usersubscription->stripe_price_id);
        $data['extraaddons']=Packages::getExtraMinutesAddonPackages($usersubscription->package_id);
        $data['pagesaddons']=Packages::getExtraPagesAddonPackages($usersubscription->package_id);
        $data['heliroomaddon'] = Packages::getHeirLoomAddonPackages($usersubscription->package_id);
        $data['qraddon'] = Packages::getQrAddonPackages($usersubscription->package_id);
        endif;

        
        if ($isajax == 'true'):

            $html = view('my-account.upgrade-plan', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-account.upgrade-plan')]);

        else:

            return view('my-account.upgrade-plan', $data);

        endif;

    }

     public function freeupminutes(Request $request)
    {

        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['error'] = $request->error;
        
        $usersubscription = UserPackages::GetActivePlan($user->id);
        $data['usersubscription']=$usersubscription;
        $data['gettotalminutes']=User::GetTotalResource($user->id);
        $data['usedtotalminutes']=User::GetUsedMinutes($user->id);
        $data['userinvoices']=UserInvoices::GetByUser($user->id);
        $data['hasdemopackage']=$user->hasdemopackage;
        $data['useraddons']=UserAddonPackages::FindByStripe($user->id);
        if(isset($usersubscription) && !empty($usersubscription)):
        $data['extraaddons']=Packages::getExtraMinutesAddonPackages($usersubscription->package_id);
        $data['pagesaddons']=Packages::getExtraPagesAddonPackages($usersubscription->package_id);
        $data['heliroomaddon'] = Packages::getHeirLoomAddonPackages($usersubscription->package_id);
        $data['qraddon'] = Packages::getQrAddonPackages($usersubscription->package_id);
        endif;
        

        
        if ($isajax == 'true'):

            $html = view('my-account.free-up-minutes', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-account.free-up-minutes')]);

        else:

            return view('my-account.free-up-minutes', $data);

        endif;

    }


    public function UpgradeAccount(Request $request,$id) 
    {

        $user=Auth::user();
        $packages=Packages::find($id);
        $productarrayall=array();
        if(isset($packages->upgradable) && $packages->upgradable==1):

            $upgradepackages=Packages::whereIn('package_id',explode(',',$packages->upgradable_to))->get();
            if(isset($upgradepackages) && count($upgradepackages)>0):

                foreach ($upgradepackages as $ugkey => $ugvalue):
                    $productarraysingle=array();
                    $productarraysingle['product']=$ugvalue->stripe_product_id;
                    $productarraysingle['prices'][]=$ugvalue->stripe_price_annual_id;
                    $productarrayall[]=$productarraysingle;
                endforeach;

            endif;

        endif;
        
        

        $stripe = new \Stripe\StripeClient(env('API_KEY_STRIPE'));
        
        $configuration=$stripe->billingPortal->configurations->create([
          'features' => [
            'invoice_history' => ['enabled' => true],
            'payment_method_update'=> ['enabled' => true],
            'subscription_cancel'=> [
                'enabled' => true,
                'cancellation_reason'=>[
                    'enabled' => true,
                     "options"=>['too_expensive','missing_features','switched_service','other']
                ],
                "mode"=>"at_period_end",
                "proration_behavior"=>"none"

            ],
            'subscription_pause'=> [
                'enabled' => false,
            ],
            "subscription_update"=> [
                  "default_allowed_updates"=>["price","promotion_code"],
                  "enabled"=> 'true',
                  "products"=>$productarrayall
          ]
         ],
          'business_profile' => [
            'privacy_policy_url' => env('APP_URL').'/my-account',
            'terms_of_service_url' => env('APP_URL').'/my-account',
          ],
        ]);



        if(isset($configuration) && !empty($configuration)):

            $session = $stripe->billingPortal->sessions->create([
              'customer' => $user->stripe_customer_id,
              'configuration' => $configuration->id,
              'return_url' => env('APP_URL').'/my-account'
            ]);

             return response()->json(['status' => 'success', 'event' => 'redirect','routetoredirect'=>$session->url]);

        endif;



       

        
        
    }

    public function CloseMyAccount(){

        $user=Auth::user();

        $user->isactive=3;
        $user->deactivation_date=date('Y-m-d');
        $user->save();

        Auth::logout();

        return response()->json(['status' => 'success', 'event' => 'redirect','routetoredirect'=>'login']);
    }

    public function DelegateAccess(Request $request){

        

        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['hasdemopackage']=$user->hasdemopackage;
        $data['allaccess']=HasAccessLogin::GetByUser($user->email);

        if ($isajax == 'true'):

            $html = view('my-account.account-access', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-account.delegate-access')]);

        else:

            return view('my-account.account-access', $data);

        endif;
    }

    public function SwitchAccountAccess(Request $request,$id){

        $user=Auth::user();
        $checkdata=UserAuthorizedPerson::where('user_authorised_person_id',$id)
        ->where('email',$user->email)
        ->first();

        if(isset($checkdata) && !empty($checkdata)):

            $userdata=User::find($checkdata->user_id);
            Session::put('trustedparty-delegateaccess',$user->email);
            Auth::login($userdata);
            return response()->json(['status'=>'success','event'=>'redirect','routetoredirect'=>route('dashboard')]);

        endif;
        
        Auth::logout();
        return response()->json(['status'=>'success','event'=>'redirect','routetoredirect'=>route('login')]);

    }


    public function BacktoMyAccount(){

        if(null !==Session::get('trustedparty-delegateaccess') && !empty(Session::get('trustedparty-delegateaccess'))):
            $userdata = User::where('email',Session::get('trustedparty-delegateaccess'))->first();

            if(!empty($userdata) && $userdata->isactive==1):
                Session::forget('trustedparty-delegateaccess');
                Auth::logout();
                Auth::login($userdata);

                return response()->json(['status' => 'success', 'event' => 'redirect','routetoredirect'=>'dashboard']);

            else:
                Session::forget('trustedparty-delegateaccess');
                Auth::logout();

                return response()->json(['status' => 'success', 'event' => 'redirect','routetoredirect'=>'login']);

            endif;

        endif;

    }

    public function RedeemCode()
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        if ($isajax == 'true'):

            $html = view('my-account.redeem-code', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-account')]);

        else:

            return view('my-account.redeem-code', $data);

        endif;
    }


    public function RedeemCodeSubmit(Request $request)
    {


    try {

        $validator = Validator::make($request->all(), [
            'code' => 'required'
        ]);

        if ($validator->fails()) :
            
            $request->error = $validator->getMessageBag()->toArray();

            return response()->json(['status' => 'fail','error'=>$request->error,'code'=>401,'custommessage'=>'please enter a valid code']);

        endif;
        
        $packages=UserPackages::where('package_code',$request->code)->whereNull('user_id')->first();
        
        if(isset($packages) && !empty($packages)):

            DB::beginTransaction();

            $userdata=User::find(Auth::id());
            $userdata->user_package_id=$packages->package_id;
            $userdata->save();
            $packages->user_id=Auth::id();
            $packages->save();

            DB::commit();


            $request = new \Illuminate\Http\Request();
            $request->ajax='true';
            $request->successmessage='Code Redeem Successfully';
            return $this->index($request);

        endif;
        
        return response()->json(['status' => 'fail','error'=>$request->error,'code'=>401,'custommessage'=>'please enter a valid code']);

    }
    catch(Exception $e) 
    {
    
        DB::rollback();

        return http_response_code(500);
    }


    }

}