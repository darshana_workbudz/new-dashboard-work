<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Http\Controllers\SendInBlueController;
use App\Http\Controllers\ZiggeoController;
use App\Models\UserAuthorizedPerson;
use App\Models\ZiggeoUserVideos;
use App\Models\ZiggeoUserAudios;
use App\Models\ZiggeoUserTexts;
use App\Models\MyRecipients;
use App\Models\UserAddonPackages;
use App\Models\Packages;
use App\Models\UserPackages;
use Storage;
use File;
use Ziggeo;
use Log;
use Response;

class MyMessagesController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
        
    }


    public function index(Request $request)
    {
       
        $user=Auth::user();
        $isajax=$request->ajax;
        $messagetype=isset($request->messagetype) && !empty($request->messagetype) ? $request->messagetype :'all';
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type :'all-content';
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['messagetype'] = $messagetype;
        $data['contenttype'] = $contenttype;
        $data['hasdemopackage'] = $user->hasdemopackage;
        $usersubscription = UserPackages::GetActivePlan($user->id);
        $data['usersubscription'] =$usersubscription;
        $data['qrcodestandalone'] = User::GetQrCodeStandalone();
        $data['handdeliveredstandalone'] = User::GetHanddeliveredStandalone();
        $data['community_packages_coming_soon'] = Packages::getAllComingSoonPackageData();
        $data['ziggeoauthtoken']=ZiggeoController::GenerateAuthtoken($user);
        $data['showpopupnew']=false;

        if($messagetype=='all'):
        $data['videolist']=ZiggeoUserVideos::GetVideoByUserId($user->id,$contenttype);
        $data['audiolist']=ZiggeoUserAudios::GetAudioByUserId($user->id,$contenttype);
        $data['textlist']=ZiggeoUserTexts::GetTextMessageByUserId($user->id,$contenttype);
        $data['showpopupnew']=true;
        endif;

        if($messagetype=='video'):
        $data['videolist']=ZiggeoUserVideos::GetVideoByUserId($user->id,$contenttype);
        if(isset($request->token) && !empty($request->token)):
        $data['ziggeomessagedata']=ZiggeoUserVideos::FindByTokenId($user->id,$request->token);
        $data['showpopupnew']=true;
        endif;
        endif;

        if($messagetype=='audio'):
        $data['audiolist']=ZiggeoUserAudios::GetAudioByUserId($user->id,$contenttype);
        if(isset($request->token) && !empty($request->token)):
        $data['ziggeomessagedata']=ZiggeoUserAudios::FindByTokenId($user->id,$request->token);
        $data['showpopupnew']=true;
        endif;

        endif;

        if($messagetype=='letter'):
        $data['textlist']=ZiggeoUserTexts::GetTextMessageByUserId($user->id,$contenttype);
        if(isset($request->token) && !empty($request->token)):
        $data['ziggeomessagedata']=ZiggeoUserTexts::FindByTokenId($user->id,$request->token);
        $data['showpopupnew']=true;
        endif;

        endif;



        if ($isajax == 'true'):

            $html = view('my-messages.index', $data)->render();

            return response()->json([
                'status' => 'success',
                'event' => 'refresh',
                'hasdemopackage'=>$user->hasdemopackage,
                'html' => $html,
                'popstate'=>true,
                'routetoredirect'=>route('my-messages.index').'?type='.$contenttype.'&messagetype='.$messagetype
                ]);

        else:

            return view('my-messages.index', $data);

        endif;

       
    }

    public function ChooseMessageType(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type :'all-content';
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['contenttype'] = $contenttype;

        if ($isajax == 'true'):

            $html = view('my-messages.create', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-messages.create')]);

        else:

            return view('my-messages.create', $data);

        endif;
    }


  

    public function Videocreate(Request $request)
    {
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type : 'personalised-content';
        $isajax = $request->ajax;
        $user=Auth::user();
        $aigvideoid='EVAHELDVIDEO'.date('Ymdhis').rand();
        $data['user_id']=$user->id;
        $data['videoname']=$aigvideoid;
        $data['contenttype']=$contenttype;
        $data['hasdemopackage']=$user->hasdemopackage;


        $usedminutes=User::GetUsedMinutes($user->id);
        $totalminutes=User::GetTotalResource($user->id);

        $hasminutes=($totalminutes-$usedminutes);
        $hasminutesseconds=($hasminutes*60);
        $hasminutesseconds=$hasminutesseconds+1;
        if($hasminutesseconds<0)
        {
            $hasminutesseconds=0;
        }
        $ziggeoauthtoken=ZiggeoController::GenerateAuthtoken($user);

        $tags=implode(',',array($aigvideoid.'-'.$user->id));

        define('ZIGGEO_DEFAULTS_RECORDER', 'ziggeo-responsive display-timer="true" ziggeo-theme="modern" ziggeo-themecolor="red" ziggeo-width="640" ziggeo-height="480" ziggeo-flashincognitosupport="true" ');


        $ziggeowidgetcreation="<ziggeorecorder ziggeo-timelimit='".$hasminutesseconds."' server-auth='".$ziggeoauthtoken."' custom-data='".json_encode($data)."'  title='".$aigvideoid."'  id='ziggeorecorderID'   ziggeo-responsive display-timer='true' ziggeo-flashincognitosupport='true' ziggeo-allowrecord='true' ziggeo-allowupload='false' ziggeo-tags='".$tags."' ziggeo-width='100%' '".ZIGGEO_DEFAULTS_RECORDER."'></ziggeorecorder>";

        $data['ziggeowidgetcreation']=$ziggeowidgetcreation;

        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['hasminutesseconds'] = $hasminutesseconds;
        if ($isajax == 'true'):

            $html = view('my-messages.video-create', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-messages.video-create')]);

        else:

            return view('my-messages.video-create', $data);

        endif;


    }

    public function VideoUpdate(Request $request,$id)
    {
        
        $isajax = $request->ajax;
        $user=Auth::user();

        $ziggeomessagedata=ZiggeoUserVideos::FindVideoByUserId($user->id,$id);

        if(isset($ziggeomessagedata) && !empty($ziggeomessagedata)):


        $aigvideoid='EVAHELDVIDEO'.date('Ymdhis').rand();
        $data['user_id']=$user->id;
        $data['videoname']=$aigvideoid;
        $data['id']=$id;
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type : 'personalised-content';
        $data['contenttype']=$contenttype;

        $usedminutes=User::GetUsedMinutes($user->id);
        $totalminutes=User::GetTotalResource($user->id);

        $hasminutes=($totalminutes-$usedminutes)+($ziggeomessagedata->duration/60);
        $hasminutesseconds=($hasminutes*60);
        $hasminutesseconds=$hasminutesseconds+1;
        if($hasminutesseconds<0)
        {
            $hasminutesseconds=0;
        }

        $ziggeoauthtoken=ZiggeoController::GenerateAuthtoken($user);

        $tags=implode(',',array($aigvideoid.'-'.$user->id));

        define('ZIGGEO_DEFAULTS_RECORDER', 'ziggeo-responsive display-timer="true" ziggeo-theme="modern" ziggeo-themecolor="red" ziggeo-width="640" ziggeo-height="480" ziggeo-flashincognitosupport="true" ');


         $ziggeowidgetcreation="<ziggeorecorder ziggeo-timelimit='".$hasminutesseconds."' server-auth='".$ziggeoauthtoken."' custom-data='".json_encode($data)."'  title='".$aigvideoid."'  id='ziggeorecorderID'   ziggeo-responsive display-timer='true' ziggeo-flashincognitosupport='true' ziggeo-allowrecord='true' ziggeo-allowupload='true' ziggeo-tags='".$tags."' ziggeo-width='100%' '".ZIGGEO_DEFAULTS_RECORDER."'></ziggeorecorder>";

        $data['ziggeowidgetcreation']=$ziggeowidgetcreation;

        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['hasminutesseconds'] = $hasminutesseconds;
        if ($isajax == 'true'):

            $html = view('my-messages.video-update', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-messages.video-update',$id).'?type='.$contenttype.'&messagetype=video']);

        else:

            return view('my-messages.video-update', $data);

        endif;

        endif;


    }

    public function Videoupload(Request $request)
    {
        
        $isajax = $request->ajax;
        

       

        $user=Auth::user();
        $aigvideoid='EVAHELDVIDEO'.date('Ymdhis').rand();
        $data['user_id']=$user->id;
        $data['videoname']=$aigvideoid;
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type : 'personalised-content';
        $data['contenttype']=$contenttype;
        $ziggeoauthtoken=ZiggeoController::GenerateAuthtoken($user);

        $tags=implode(',',array($aigvideoid.'-'.$user->id));

        $usedminutes=User::GetUsedMinutes($user->id);
        $totalminutes=User::GetTotalResource($user->id);

        $hasminutes=($totalminutes-$usedminutes);
        $hasminutesseconds=($hasminutes*60);
        $hasminutesseconds=$hasminutesseconds+1;
        if($hasminutesseconds<0)
        {
            $hasminutesseconds=0;
        }
        
        
        
        define('ZIGGEO_DEFAULTS_RECORDER', 'ziggeo-responsive display-timer="true" ziggeo-theme="modern" ziggeo-themecolor="red" ziggeo-width="640" ziggeo-height="480" ziggeo-flashincognitosupport="true" ');


        $ziggeowidgetcreation="<ziggeorecorder ziggeo-timelimit='".$hasminutesseconds."' ziggeo-enforce-duration='true'  server-auth='".$ziggeoauthtoken."' custom-data='".json_encode($data)."'  title='".$aigvideoid."'  id='ziggeorecorderID' ziggeo-allowedextensions='mp4,avi,mov,ogv' ziggeo-flashincognitosupport='true' ziggeo-allowrecord='false' ziggeo-allowupload='true' ziggeo-tags='".$tags."' ziggeo-width='100%' '".ZIGGEO_DEFAULTS_RECORDER."'></ziggeorecorder>";

        $data['ziggeowidgetcreation']=$ziggeowidgetcreation;

        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['hasminutesseconds'] = $hasminutesseconds;
        if ($isajax == 'true'):

            $html = view('my-messages.video-upload', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-messages.video-upload')]);

        else:

            return view('my-messages.video-upload', $data);

        endif;


    }

    // public function Audiocreate(Request $request)
    // {
    //     $user=Auth::user();
    //     $isajax=$request->ajax;
    //     $data['isajax'] = $isajax;
    //     $data['old'] = $request;
    //     $data['user_id'] = $user->id;
    //     $data['audioname']=$request->audioname;
    //     $data['audiotime']=$request->audiotime;
    //     $data['audiodescription']=$request->audiodescription;
    //     $data['error'] = $request->error;

    //     if ($isajax == 'true'):

    //         $html = view('my-messages.audio-create', $data)->render();

    //         return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-messages.audio-create')]);

    //     else:

    //         return view('my-messages.audio-create', $data);

    //     endif;

       
    // }

    public function Audiocreate(Request $request)
    {
        
        $isajax = $request->ajax;
        $user=Auth::user();
        $aigaudioid='EVAHELDAUDIO'.date('Ymdhis').rand();
        $data['user_id']=$user->id;
        $data['audioname']=$aigaudioid;
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type : 'personalised-content';
        $data['contenttype']=$contenttype;
        
        $ziggeoauthtoken=ZiggeoController::GenerateAuthtoken($user);
        $tags=implode(',',array($aigaudioid.'-'.$user->id));

        $usedminutes=User::GetUsedMinutes($user->id);
        $totalminutes=User::GetTotalResource($user->id);

        $hasminutes=($totalminutes-$usedminutes);
        $hasminutesseconds=($hasminutes*60);
        $hasminutesseconds=$hasminutesseconds+1;
        if($hasminutesseconds<0)
        {
            $hasminutesseconds=0;
        }

        define('ZIGGEO_DEFAULTS_RECORDER', 'ziggeo-responsive display-timer="true" ziggeo-theme="modern" ziggeo-themecolor="red" ziggeo-width="640" ziggeo-height="480" ziggeo-flashincognitosupport="true" ');

        $ziggeowidgetcreation="<ziggeoaudiorecorder ziggeo-timelimit='".$hasminutesseconds."' ziggeo-enforce-duration='true'  server-auth='".$ziggeoauthtoken."' custom-data='".json_encode($data)."' ziggeo-popup title='".$aigaudioid."'  id='ziggeorecorderID' ziggeo-allowupload='false'  ziggeo-tags='".$tags."' ".ZIGGEO_DEFAULTS_RECORDER."></ziggeoaudiorecorder>";

        $data['ziggeowidgetcreation']=$ziggeowidgetcreation;

        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['hasminutesseconds'] = $hasminutesseconds;
        if ($isajax == 'true'):

            $html = view('my-messages.audio-create', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-messages.audio-create')]);

        else:

            return view('my-messages.audio-create', $data);

        endif;


    }

    public function AudioUpdate(Request $request,$id)
    {
        
        $isajax = $request->ajax;
        $user=Auth::user();
        $aigaudioid='EVAHELDAUDIO'.date('Ymdhis').rand();
        $data['user_id']=$user->id;
        $data['audioname']=$aigaudioid;
        $data['id']=$id;
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type : 'personalised-content';
        $data['contenttype']=$contenttype;
        $ziggeoauthtoken=ZiggeoController::GenerateAuthtoken($user);
        $tags=implode(',',array($aigaudioid.'-'.$user->id));

        $usedminutes=User::GetUsedMinutes($user->id);
        $totalminutes=User::GetTotalResource($user->id);

        $hasminutes=($totalminutes-$usedminutes);
        $hasminutesseconds=($hasminutes*60);
        $hasminutesseconds=$hasminutesseconds+1;
        if($hasminutesseconds<0)
        {
            $hasminutesseconds=0;
        }
        


        define('ZIGGEO_DEFAULTS_RECORDER', 'ziggeo-responsive display-timer="true" ziggeo-theme="modern" ziggeo-themecolor="red" ziggeo-width="640" ziggeo-height="480" ziggeo-flashincognitosupport="true" ');

        $ziggeowidgetcreation="<ziggeoaudiorecorder ziggeo-timelimit='".$hasminutesseconds."' ziggeo-enforce-duration='true'  server-auth='".$ziggeoauthtoken."' custom-data='".json_encode($data)."' ziggeo-popup title='".$aigaudioid."'  id='ziggeorecorderID' ziggeo-allowupload='true' ziggeo-allowrecord='true'  ziggeo-tags='".$tags."' ".ZIGGEO_DEFAULTS_RECORDER."></ziggeoaudiorecorder>";

        $data['ziggeowidgetcreation']=$ziggeowidgetcreation;

        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['hasminutesseconds'] = $hasminutesseconds;
        if ($isajax == 'true'):

            $html = view('my-messages.audio-update', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-messages.audio-update',$id).'?type='.$contenttype.'&messagetype=audio']);

        else:

            return view('my-messages.audio-update', $data);

        endif;


    }




    public function Audioupload(Request $request)
    {
        
        $isajax = $request->ajax;
        $user=Auth::user();
        $aigaudioid='EVAHELDAUDIO'.date('Ymdhis').rand();
        $data['user_id']=$user->id;
        $data['audioname']=$aigaudioid;
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type : 'personalised-content';
        $data['contenttype']=$contenttype;
        
        $ziggeoauthtoken=ZiggeoController::GenerateAuthtoken($user);
        $tags=implode(',',array($aigaudioid.'-'.$user->id));

        $usedminutes=User::GetUsedMinutes($user->id);
        $totalminutes=User::GetTotalResource($user->id);
        $hasminutes=($totalminutes-$usedminutes);
        $hasminutesseconds=($hasminutes*60);
        $hasminutesseconds=$hasminutesseconds+1;
        if($hasminutesseconds<0)
        {
            $hasminutesseconds=0;
        }

        
       

        define('ZIGGEO_DEFAULTS_RECORDER', 'ziggeo-responsive display-timer="true" ziggeo-theme="modern" ziggeo-themecolor="red"  ziggeo-width="640" ziggeo-height="480" ziggeo-flashincognitosupport="true" ');

        $ziggeowidgetcreation="<ziggeoaudiorecorder ziggeo-timelimit='".$hasminutesseconds."' ziggeo-enforce-duration='true'  server-auth='".$ziggeoauthtoken."' custom-data='".json_encode($data)."' ziggeo-allowrecord='false' ziggeo-allowupload='true'  title='".$aigaudioid."'  id='ziggeorecorderID' ziggeo-allowedextensions='wav,mp3' ziggeo-tags='".$tags."' ".ZIGGEO_DEFAULTS_RECORDER."></ziggeoaudiorecorder>";

        $data['ziggeowidgetcreation']=$ziggeowidgetcreation;

        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['hasminutesseconds'] = $hasminutesseconds;
        if ($isajax == 'true'):

            $html = view('my-messages.audio-upload', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-messages.audio-upload')]);

        else:

            return view('my-messages.audio-upload', $data);

        endif;


    }



    public function ProcessCreatedVideo(Request $request)
    {
        
        $title = $request->videoname;
        $token = $request->token;
         $id = $request->id;
        // $user=Auth::user();

        // // $ziggeomessagedata=ZiggeoUserVideos::FindVideoByUserId($user->id,$id);

        // // if(isset($ziggeomessagedata) && !empty($ziggeomessagedata)):

        // //     // $ziggeomessagedata->delete();


        // // endif;

       
        return response()->json(['status' => 'success']);
       
    }


    public function ProcessCreatedAudio(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'token' => 'required',
            
        ]);
        $title = $request->audioname;
        $token = $request->token;
        $id = $request->id;
        $user=Auth::user();


        $ziggeo = new Ziggeo(env('ZIGGEOTOKEN'),env('ZIGGEOPRIVATEKEY'));

        $resultset = $ziggeo->audios()->get($token);

        

        if(isset($resultset['audio']) && !empty($resultset['audio'])):
                $result=$resultset['audio'];

                        $ziggeoarray=array(

                            'user_id'=>isset($result['data']['user_id']) && !empty($result['data']['user_id']) ? $result['data']['user_id'] : null,
                            'audio_token_id'=>isset($result['token']) && !empty($result['token']) ? $result['token'] : null,
                            'audio_key'=>isset($result['key']) && !empty($result['key']) ? $result['key'] : null,
                            'audio_title'=>isset($result['title']) && !empty($result['title']) ? $result['title'] : null,
                            'audio_description'=>isset($result['description']) && !empty($result['description']) ? $result['description'] : null,
                            'tags'=>isset($result['tags'][0]) && !empty($result['tags'][0]) ? $result['tags'][0] : null,
                            'max_duration'=>isset($result['max_duration']) && !empty($result['max_duration']) ? $result['max_duration'] : null,
                            'duration'=>isset($result['duration']) && !empty($result['duration']) ? $result['duration'] : null,
                            'state'=>isset($result['state']) && !empty($result['state']) ? $result['state'] : null,
                            'embed_audio_url'=>isset($result['embed_audio_url']) && !empty($result['embed_audio_url']) ? $result['embed_audio_url'] : null,
                            'device_info'=>isset($result['device_info']) && !empty($result['device_info']) ? json_encode($result['device_info']) : null,
                            'submission_date'=>isset($result['submission_date']) && !empty($result['submission_date']) ? $result['submission_date'] : null,
                            'capture_type'=>isset($result['device_info']['capture_type']) && !empty($result['device_info']['capture_type']) ? $result['device_info']['capture_type'] : null,
                            'created_at'=>Carbon::now(),
                            'created_by'=>isset($result['data']['user_id']) && !empty($result['data']['user_id']) ? $result['data']['user_id'] : null,
                            'updated_by'=>isset($result['data']['user_id']) && !empty($result['data']['user_id']) ? $result['data']['user_id'] : null,
                            'updated_at'=>Carbon::now(),
                            'contenttype'=>isset($result['data']['contenttype']) && !empty($result['data']['contenttype']) ? $result['data']['contenttype'] : null



                        );

        $ziggeoevent=ZiggeoUserAudios::where('audio_token_id',$token)->first();

        $usedminutes=User::GetUsedMinutes($user->id);
        $totalminutes=User::GetTotalResource($user->id);

        $hasminutes=($totalminutes-$usedminutes);
        $hasminutesseconds=($hasminutes*60);
        $hasminutesseconds=$hasminutesseconds+1;
        if($hasminutesseconds<0)
        {
            $hasminutesseconds=0;
        }


        if(isset($id) && !empty($id)):
            $ziggeomessagedata=ZiggeoUserAudios::FindAudioByUserId($user->id,$id);
            $hasminutesseconds=$hasminutesseconds+$ziggeomessagedata->duration;
        endif;
        
        if(isset($ziggeoevent) && !empty($ziggeoevent) && ($hasminutesseconds>=$result['duration'])):

             $ziggeoevent->update($ziggeoarray);

        elseif($hasminutesseconds>=$result['duration']):
        
            
                ZiggeoUserAudios::create($ziggeoarray);
           
        endif;

        endif;



        if(isset($ziggeomessagedata) && !empty($ziggeomessagedata)):

            $ziggeomessagedata->delete();


        endif;


        return response()->json(['status' => 'success']);
    }









    // Text Uplod Message Code ----Start


    public function TextCreate(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type : 'personalised-content';
        $data['contenttype']=$contenttype;
        if ($isajax == 'true'):

            $html = view('my-messages.text-create', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-messages.text-create')]);

        else:

            return view('my-messages.text-create', $data);

        endif;
    }

    public function TextUpload(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type : 'personalised-content';
        $data['contenttype']=$contenttype;
        if ($isajax == 'true'):

            $html = view('my-messages.text-upload', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-messages.text-upload')]);

        else:

            return view('my-messages.text-upload', $data);

        endif;
    }

    public function TextUpdate(Request $request,$id)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['error'] = $request->error;
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type : 'personalised-content';
        $data['contenttype']=$contenttype;

        $textdata = ZiggeoUserTexts::FindTextMessageByUserId($user->id,$id);
        $data['textdata'] = $textdata;
        

        if ($isajax == 'true'):

            $html = view('my-messages.text-update', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-messages.text-update',$id).'?type='.$contenttype.'&messagetype=letter']);

        else:

            return view('my-messages.text-update', $data);

        endif;
    }

    public function TextShow(Request $request,$id)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['error'] = $request->error;
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type : 'personalised-content';
        $data['contenttype']=$contenttype;

        $textdata = ZiggeoUserTexts::FindTextMessageByUserId($user->id,$id);
        $data['textdata'] = $textdata;
        $data['loadurl'] = $request->loadurl;



        if ($isajax == 'true'):

            $html = view('my-messages.text-show', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-messages.text-show',$id)]);

        else:

            return view('my-messages.text-show', $data);

        endif;
    }


     public function TextStream(Request $request,$id)
    {
        $user=Auth::user();
        $textdata = ZiggeoUserTexts::FindTextMessageByUserId($user->id,$id);
        $data['textdata'] = $textdata;
        return response()->file($textdata->path);


       
    }


    public function UploadFiles(Request $request)
    {

        try
        {   
            $user=Auth::user();

            $validator = Validator::make($request->all(), [

            'file' => 'required',
            'type' => 'required'
            
            ]);


            if ($request->hasFile('file')) {

                $document = $request->file('file');
                $fileInfo = $document->getClientOriginalName();
                $filename = pathinfo($fileInfo, PATHINFO_FILENAME);
                $extension = pathinfo($fileInfo, PATHINFO_EXTENSION);
                $file_name= $filename.'-'.time().'.'.$extension;
                Storage::disk('public')->put($file_name,File::get($document));


                if(Storage::disk('public')->exists($file_name)) {  
                    $path = Storage::disk('public')->path($file_name);

                    $allowedMimeTypes = ['image/jpeg','image/gif','image/png','image/jpg'];
                    $contentType = $document->getClientMimeType();

                    if(! in_array($contentType, $allowedMimeTypes) ){
                      
                      $totalPages = $this->countPages($path);
                      $filetype='pdf';
                    }
                    else
                    {
                      $totalPages=1;
                      $filetype='image';
                    }


                    $usedpagecount=User::GetUsedDocLimit($user->id);
                    $totalpagecount=User::GetTotalDocLimit($user->id);
                    $haspagecount=($totalpagecount-$usedpagecount);
                    if($totalPages>$haspagecount):

                        return array('status'=>'fail','message'=>'Sorry Upload Failed! Limit Exceed for Page Count');

                    endif;


                    
                    $ziggeo_user_text_id=$request->ziggeo_user_text_id;
                    if(isset($ziggeo_user_text_id) && !empty($ziggeo_user_text_id)):
                                
                            $textdata=ZiggeoUserTexts::find($ziggeo_user_text_id);
                            $updated=$textdata->update([

                                'title'=>$filename,
                                'descriptioncontenthtml'=>null,
                                'descriptioncontent'=>null,
                                'capture_type'=>'uploaded',
                                'path'=>$path,
                                'uploadedfilename'=>$file_name,
                                'created_at'=>Carbon::now(),
                                'created_by'=>$user->id,
                                'updated_at'=>Carbon::now(),
                                'updated_by'=>$user->id,
                                'pagecount'=>$totalPages,
                                'filetype'=>$filetype
                            ]);

                        else:
                            
                            $updated=ZiggeoUserTexts::create([

                                'user_id'=>$user->id,
                                'title'=>$filename,
                                'descriptioncontenthtml'=>null,
                                'descriptioncontent'=>null,
                                'capture_type'=>'uploaded',
                                'path'=>$path,
                                'uploadedfilename'=>$file_name,
                                'created_at'=>Carbon::now(),
                                'created_by'=>$user->id,
                                'updated_at'=>Carbon::now(),
                                'updated_by'=>$user->id,
                                'contenttype'=>isset($request->type) && !empty($request->type) ? $request->type :'personalised-content',
                                'text_token_id'=>rand().date('Ymdhis'),
                                'pagecount'=>$totalPages,
                                'filetype'=>$filetype

                            ]);

                    endif;

                    return array('status'=>'success','data'=>$updated);
                    

                
                            
                }

            }


             


        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

    }


    function countPages($path) {
      $pdftext = file_get_contents($path);
      $num = preg_match_all("/\/Page\W/", $pdftext, $dummy);
      return $num;
    }

    public function VerifyTextdetails(Request $request)
    {
        
        try
        {   
            $validator = Validator::make($request->all(), [

           
            'descriptionhtml' => 'required',
            'descriptioncontent' => 'required'
            
            ]);

            $isajax = $request->ajax;
            $user=Auth::user();
           
            if ($validator->fails()) :

                $request = new \Illuminate\Http\Request();
                $request->ajax='true';
                $request->messagetype='letter';
                $request->type=$request->type;
                $request->error=$validator->getMessageBag()->toArray();
                return $this->TextCreate($request);

            endif;

            $charactercount=strlen($request->descriptionhtml)-7;
            $totalPages=round($charactercount/1800)>=1 ? round($charactercount/1800) : 1;



            $usedpagecount=User::GetUsedDocLimit($user->id);
            $totalpagecount=User::GetTotalDocLimit($user->id);
            $haspagecount=($totalpagecount-$usedpagecount);
            
           
            



                $ziggeo_user_text_id=$request->ziggeo_user_text_id;


                if(isset($ziggeo_user_text_id) && !empty($ziggeo_user_text_id)):

                    $textdata = ZiggeoUserTexts::where('user_id',$user->id)->where('ziggeo_user_text_id',$ziggeo_user_text_id)->first();
                    
                    if($totalPages>($haspagecount+$textdata->pagecount)):

                        $request = new \Illuminate\Http\Request();
                        $request->ajax='true';
                        $request->failuremessage='Sorry Upload Failed! Limit Exceed for Page Count';
                        $request->type=$request->type;
                        $request->messagetype='letter';

                        return $this->TextUpdate($request,$ziggeo_user_text_id);

                    endif;


                    $textdata->update([

                            
                            'descriptioncontenthtml'=>$request->descriptionhtml,
                            'descriptioncontent'=>$request->descriptioncontent,
                            'capture_type'=>'written',
                            'path'=>null,
                            'uploadedfilename'=>null,
                            'updated_at'=>Carbon::now(),
                            'updated_by'=>$user->id,
                            'pagecount'=>$totalPages,
                            'charactercount'=>$charactercount

                    ]);

                        $request = new \Illuminate\Http\Request();
                        $request->ajax='true';
                        $request->successmessage='Letter Updated Successfully';
                        $request->type=$textdata->contenttype;
                        $request->messagetype='letter';

                        return $this->index($request);

                else:


                    if($totalPages>$haspagecount):

                    $request = new \Illuminate\Http\Request();
                    $request->ajax='true';
                    $request->failuremessage='Sorry Upload Failed! Limit Exceed for Page Count';
                    $request->type=$request->type;
                    $request->messagetype='letter';

                    return $this->TextCreate($request);

                endif;


                    $created=ZiggeoUserTexts::create([

                                'user_id'=>$user->id,
                                'title'=>'EVAHELDTEXT'.date('Ymdhis').rand(),
                                'descriptioncontenthtml'=>$request->descriptionhtml,
                                'descriptioncontent'=>$request->descriptioncontent,
                                'capture_type'=>'written',
                                'path'=>null,
                                'uploadedfilename'=>null,
                                'created_at'=>Carbon::now(),
                                'created_by'=>$user->id,
                                'updated_at'=>Carbon::now(),
                                'updated_by'=>$user->id,
                                'contenttype'=>isset($request->type) && !empty($request->type) ? $request->type :'personalised-content',
                                'text_token_id'=>rand().date('Ymdhis'),
                                'pagecount'=>$totalPages,
                                'charactercount'=>$charactercount

                    ]);

                    if($created):


                        $request->ajax='true';
                        $request->loadurl=1;
                        $request->type=$request->type;
                        $request->messagetype='letter';
                        return $this->TextShow($request,$created->ziggeo_user_text_id);


                    endif;


                endif;


                    



        
        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }
    }


    public function GetUploadedFiles($id)
    {
        $user=Auth::user();
        $pdffiles = ZiggeoUserTexts::where('user_id',$user->id)
        ->where('ziggeo_user_text_id',$id)
        ->where('capture_type','uploaded')
        ->get()->toArray();

        if(isset($pdffiles) && !empty($pdffiles)):

        foreach($pdffiles as $pdffilesdata){
            $pdffilesdatalist[] = $pdffilesdata['uploadedfilename'];
        }
        $storeFolder = public_path('uploads/gallery');
        $file_path = public_path('uploads/gallery/');
        $files = scandir($storeFolder);

        foreach ( $files as $file ) {
            if ($file !='.' && $file !='..' && in_array($file,$pdffilesdatalist)) {       
                $obj['name'] = $file;
                $file_path = public_path('uploads/gallery/').$file;
                $obj['size'] = filesize($file_path);          
                $obj['path'] = url('public/uploads/gallery/'.$file);
                $data[] = $obj;
            }
            
        }
        
        return response()->json($data);
        endif;
    }


    public function DeleteUploadedFiles(Request $request)
    {
        $user=Auth::user();
        $filename =  $request->get('filename');
        
        $pdffiles = ZiggeoUserTexts::where('user_id',$user->id)
        ->where('uploadedfilename',$filename)
        ->where('capture_type','uploaded')
        ->first();

        if(isset($pdffiles) && !empty($pdffiles)):

           $pdffiles->delete();
           unlink($pdffiles->path);

        endif;

        

        
        
        return response()->json(['success'=>$filename]);
    } 

     public function DownloadFiles($filename)
     {
        $user=Auth::user();
        $pdffiles = ZiggeoUserTexts::where('user_id',$user->id)
        ->where('uploadedfilename',$filename)
        ->where('capture_type','uploaded')
        ->first();

        
           if(isset($pdffiles) && !empty($pdffiles)):

            if (file_exists($pdffiles->path)):
                return \Response::download($pdffiles->path);
            endif;
           
            endif;
        
     } 


    public function ShowMessageDetails(Request $request)
    {
        $user=Auth::user();
        $authid=User::GetAuthID($user);
        
        
       try
        {   
            $validator = Validator::make($request->all(), [

            'messagetype' => 'required',
            'id' => 'required',
            
            
            ]);

            if ($validator->fails()) :

            return response()->json(['status' => 'fail']);

            endif;

            $messagetype=$request->messagetype;
            $id=$request->id;
            $actiontype=$request->actiontype;

            if($messagetype=='video')
            {
               
               $ziggeomessagedata=ZiggeoUserVideos::FindVideoByUserId($user->id,$id);

            }

            if($messagetype=='audio')
            {
                $ziggeomessagedata=ZiggeoUserAudios::FindAudioByUserId($user->id,$id);
            }

            if($messagetype=='letter')
            {
               $ziggeomessagedata=ZiggeoUserTexts::FindTextMessageByUserId($user->id,$id);
            }

            if(isset($ziggeomessagedata) && !empty($ziggeomessagedata)):

                $data['messagedata']=$ziggeomessagedata;
                $data['messagetype']=$messagetype;
                $data['messageid']=$id;
                $data['actiontype']=$actiontype;
                $recipients = MyRecipients::GetActiveAll($authid);
                $data['recipients'] = $recipients;
                $data['hasdemopackage']=$user->hasdemopackage;
                $usersubscription = UserPackages::GetActivePlan($user->id);
                $data['usersubscription'] =$usersubscription;
                $data['qrcodestandalone'] = User::GetQrCodeStandalone($user->id);
                $data['handdeliveredstandalone'] = User::GetHanddeliveredStandalone($user->id);
                $html = view('my-messages.message-body',$data)->render();

                return response()->json(['status' => 'success','html' => $html]);

            endif;

            


        
        } catch (TrawableException $th) {
           
           return response()->json(['status' => 'fail']);

        } catch (QueryException $qe) {
            
           return response()->json(['status' => 'fail']);
        }
        

        

       
    }

    public function DeleteActionMessageDetails(Request $request,$id,$type)
    {
        $user=Auth::user();
        $authid=User::GetAuthID($user);
        
        
       try
        {   
           

            $messagetype=$type;
            $id=$id;
            $actiontype=$request->actiontype;

            if($messagetype=='video')
            {
               
               $ziggeomessagedata=ZiggeoUserVideos::FindVideoByUserId($user->id,$id);

            }

            if($messagetype=='audio')
            {
                $ziggeomessagedata=ZiggeoUserAudios::FindAudioByUserId($user->id,$id);
            }

            if($messagetype=='letter')
            {
               $ziggeomessagedata=ZiggeoUserTexts::FindTextMessageByUserId($user->id,$id);
            }

            if(isset($ziggeomessagedata) && !empty($ziggeomessagedata)):

                $ziggeomessagedata->delete();

                $request = new \Illuminate\Http\Request();
                $request->ajax='true';
                $request->successmessage='Message Deleted Successfully';
                return $this->index($request);

            endif;

            


        
        } catch (TrawableException $th) {
           
           return response()->json(['status' => 'fail']);

        } catch (QueryException $qe) {
            
           return response()->json(['status' => 'fail']);
        }
        

        

       
    }


    public function ManageActionMessageDetails(Request $request)
    {
        $user=Auth::user();
        $authid=User::GetAuthID($user);
        
        
       try
        {   
            $validator = Validator::make($request->all(), [

            'messagetype' => 'required',
            'id' => 'required',
            'actiontype' => 'required',
            
            
            ]);

            if ($validator->fails()) :

            return response()->json(['status' => 'fail']);

            endif;

            $messagetype=$request->messagetype;
            $id=$request->id;
            $actiontype=$request->actiontype;

            if($messagetype=='video')
            {
               
               $ziggeomessagedata=ZiggeoUserVideos::FindVideoByUserId($user->id,$id);

            }

            if($messagetype=='audio')
            {
                $ziggeomessagedata=ZiggeoUserAudios::FindAudioByUserId($user->id,$id);
            }

            if($messagetype=='letter')
            {
               $ziggeomessagedata=ZiggeoUserTexts::FindTextMessageByUserId($user->id,$id);
            }

            if(isset($ziggeomessagedata) && !empty($ziggeomessagedata)):

                $data['messagedata']=$ziggeomessagedata;
                $data['messagetype']=$messagetype;
                $data['messageid']=$id;
                $data['actiontype']=$actiontype;
                $recipients = MyRecipients::GetActiveAll($authid);
                $data['recipients'] = $recipients;
                $data['qrcodeaddon'] = User::GetQrCodeAddon($user->id);
                $data['handdeliveredaddon'] = User::GetHanddeliveredAddon($user->id);
                $data['hasdemopackage']=$user->hasdemopackage;
                $usersubscription = UserPackages::GetActivePlan($user->id);
                $data['heliroomaddon'] = Packages::getHeirLoomAddonPackages($usersubscription->package_id);

                $data['qraddon'] = Packages::getQrAddonPackages($usersubscription->package_id);
                
                $data['error'] = $request->error;
                $data['old'] = $request;

                $html = view('my-messages.message-action',$data)->render();

                return response()->json(['status' => 'success','html' => $html]);

            endif;

            


        
        } catch (TrawableException $th) {
           
           return response()->json(['status' => 'fail']);

        } catch (QueryException $qe) {
            
           return response()->json(['status' => 'fail']);
        }
        

        

       
    }

    public function TakeActionMessageDetails(Request $request)
    {
            $user=Auth::user();
            $validator = Validator::make($request->all(), [

                'messagetype' => 'required',
                'id' => 'required',
                'actiontype' => 'required',
                'recipient'=> ['required_if:actiontype,==,update-recipients'],
                'deliverytype'=> ['required_if:actiontype,==,update-deliverydate','in:1,2'],
                'deliverydate'=> ['required_if:deliverytype,==,1'],
                'deliverystyle'=> ['required_if:actiontype,==,update-deliverystyle','in:1,2'],
                'authorisationtype'=> ['required_if:actiontype,==,update-authorisation','in:1,2'],
                'qrdeliverystyle'=> ['required_if:actiontype,==,update-qrcode','in:1,2']
                
            
            ],
            [
            'recipient.required_if'=>'please select recipient from above list',
            'deliverytype.required_if'=>'This field is required to proceed',
            'deliverydate.required_if'=>'please choose delivery date from calendar',
            'deliverystyle.required_if'=>'please choose delivery style from above options',
            'qrdeliverystyle.required_if'=>'please choose delivery style from above options',
            'authorisationtype.required_if'=>'please choose one of the authorisation way from given options'
            ]
        );

            if ($validator->fails()) :

                    $request->error=$validator->getMessageBag()->toArray();

                    return $this->ManageActionMessageDetails($request);

            endif;
        try
        {  

            $messagetype=$request->messagetype;
            $id=$request->id;
            $actiontype=$request->actiontype;
            $recipient=$request->recipient;
            $deliverytype=$request->deliverytype;
            $deliverydate=$request->deliverydate;
            $deliverystyle=$request->deliverystyle;
            $authorisationtype=$request->authorisationtype;
            $qrdeliverystyle=$request->qrdeliverystyle;

            if($messagetype=='video')
            {
               $type=1;
               $ziggeomessagedata=ZiggeoUserVideos::FindVideoByUserId($user->id,$id);
               $token=$ziggeomessagedata->video_token_id;
            }

            if($messagetype=='audio')
            {   $type=2;
                $ziggeomessagedata=ZiggeoUserAudios::FindAudioByUserId($user->id,$id);
                $token=$ziggeomessagedata->audio_token_id;
            }

            if($messagetype=='letter')
            {
               $type=3;
               $ziggeomessagedata=ZiggeoUserTexts::FindTextMessageByUserId($user->id,$id);
               $token=$ziggeomessagedata->text_token_id;
            }
 
            if(isset($ziggeomessagedata) && !empty($ziggeomessagedata)):

                $data['messagedata']=$ziggeomessagedata;
                $data['messagetype']=$messagetype;
                $data['messageid']=$id;
                $data['actiontype']=$actiontype;
                $recipients = MyRecipients::GetActiveAll($user->id);
                $data['recipients'] = $recipients;

                
                
                if($actiontype=='update-recipients'):
                    DB::beginTransaction();
                    $ziggeomessagedata->user_recipient_id=$recipient;
                    $ziggeomessagedata->save();
                    DB::commit();

                endif;

                if($actiontype=='update-deliverdate'):
                    DB::beginTransaction();
                    $ziggeomessagedata->delivery_type=$deliverytype;
                    $ziggeomessagedata->delivery_date=isset($deliverydate) && !empty($deliverydate) ? date('Y-m-d',strtotime($deliverydate)) : null;
                    $ziggeomessagedata->save();
                    DB::commit();

                endif;

                if($actiontype=='update-authorisation'):
                    DB::beginTransaction();
                    $ziggeomessagedata->authorisation_type=$authorisationtype;
                    $ziggeomessagedata->save();
                    DB::commit();
                    

                endif;

                if($actiontype=='update-deliverystyle'):
                     DB::beginTransaction();


                    if($deliverystyle==2 && empty($ziggeomessagedata->heilroom_package_id)):

                       $FindHanddeliveredAddon= User::FindHanddeliveredAddon($user->id);
                        if(isset($FindHanddeliveredAddon) && !empty($FindHanddeliveredAddon)):

                            $FindHanddeliveredAddon->linked_message_type=$messagetype;
                            $FindHanddeliveredAddon->linked_message_id=$id;
                            $FindHanddeliveredAddon->save();
                            $ziggeomessagedata->heilroom_package_id=$FindHanddeliveredAddon->user_addon_package_id;
                            $ziggeomessagedata->heilroom_package_type=2;
                            $ziggeomessagedata->delivery_style=$deliverystyle;

                        else:


                            $FindStandaloneHanddelivered= User::FindStandaloneHanddelivered($user->id);

                            $FindStandaloneHanddelivered->linked_message_type=$messagetype;
                            $FindStandaloneHanddelivered->linked_message_id=$id;
                            $FindStandaloneHanddelivered->save();
                            $ziggeomessagedata->heilroom_package_id=$FindStandaloneHanddelivered->user_standalone_package_id;
                            $ziggeomessagedata->heilroom_package_type=3;
                            $ziggeomessagedata->delivery_style=$deliverystyle;

                        endif;


                    endif;

                    if($deliverystyle==1 && empty($ziggeomessagedata->heilroom_package_id)):

                         $ziggeomessagedata->delivery_style=$deliverystyle;

                    endif;

                    if($deliverystyle==1 && !empty($ziggeomessagedata->heilroom_package_id)):

                        $packageid=$ziggeomessagedata->heilroom_package_id;
                        $packagetype=$ziggeomessagedata->heilroom_package_type;
                     

                        if(!empty($packageid) && !empty($packagetype)):
                            if($packagetype==2):
                                $packagedata=UserAddonPackages::find($packageid);
                            endif;

                            if(isset($packagedata) && !empty($packagedata)):
                                $packagedata->linked_message_type=null;
                                $packagedata->linked_message_id=null;
                                $packagedata->save();
                            endif;

                            $ziggeomessagedata->heilroom_package_type=null;
                            $ziggeomessagedata->heilroom_package_id=null;
                            $ziggeomessagedata->delivery_style=$deliverystyle;

                        endif;

                    endif;

                    
                    $ziggeomessagedata->save();
                    DB::commit();
                    

                endif;

                

                if($actiontype=='update-message'):

                    if(isset($ziggeomessagedata->ziggeo_user_video_id)):

                        $request = new \Illuminate\Http\Request();
                        $request->ajax='true';
                        $request->messagetype='video';
                        $request->contenttype=$ziggeomessagedata->contenttype;
                        return $this->VideoUpdate($request,$ziggeomessagedata->ziggeo_user_video_id);

                    endif;

                    if(isset($ziggeomessagedata->ziggeo_user_audio_id)):
                        $request = new \Illuminate\Http\Request();
                        $request->ajax='true';
                        $request->messagetype='audio';
                        $request->contenttype=$ziggeomessagedata->contenttype;
                        return $this->AudioUpdate($request,$ziggeomessagedata->ziggeo_user_audio_id);

                    endif;


                    if(isset($ziggeomessagedata->ziggeo_user_text_id)):

                        $request = new \Illuminate\Http\Request();
                        $request->ajax='true';
                        $request->messagetype='letter';
                        $request->contenttype=$ziggeomessagedata->contenttype;
                        return $this->TextUpdate($request,$ziggeomessagedata->ziggeo_user_text_id);

                    endif;

                   

                endif;

                if($actiontype=='clone-message'):
                     DB::beginTransaction();
                    if($messagetype=='video' || $messagetype=='audio'):
                            $usedminutes=User::GetUsedMinutes($user->id);
                            $totalminutes=User::GetTotalResource($user->id);

                            $hasminutes=($totalminutes-$usedminutes);
                            $hasminutesseconds=($hasminutes*60);
                            $hasminutesseconds=$hasminutesseconds+1;
                            if($hasminutesseconds<0)
                            {
                                $hasminutesseconds=0;
                            }

                            if($hasminutesseconds<($ziggeomessagedata->duration)):
                                $request = new \Illuminate\Http\Request();
                                $request->ajax='true';
                                $request->messagetype=$messagetype;
                                $request->contenttype=$ziggeomessagedata->contenttype;
                                $request->failuremessage='Sorry Message Failed to Cloned! Time Limit Exceed';
                                return $this->index($request);
                            endif;
                    endif;

                    if($messagetype=='letter'):

                    if($ziggeomessagedata->capture_type=='written'):
                        $charactercount=strlen($ziggeomessagedata->descriptioncontenthtml)-7;
                        $totalPages=round($charactercount/1800)>=1 ? round($charactercount/1800) : 1;
                        $usedpagecount=User::GetUsedDocLimit($user->id);
                        $totalpagecount=User::GetTotalDocLimit($user->id);
                        $haspagecount=($totalpagecount-$usedpagecount);

                        if($totalPages>$haspagecount):
                                $request = new \Illuminate\Http\Request();
                                $request->ajax='true';
                                $request->messagetype=$messagetype;
                                $request->contenttype=$ziggeomessagedata->contenttype;
                                $request->failuremessage='Sorry Upload Failed! Limit Exceed for Page Count';
                                return $this->index($request);
                        endif;
                    endif;

                     if($ziggeomessagedata->capture_type=='uploaded'):

                            $totalPages=$ziggeomessagedata->pagecount;
                            if($totalPages>$haspagecount):
                                $request = new \Illuminate\Http\Request();
                                $request->ajax='true';
                                $request->messagetype=$messagetype;
                                $request->contenttype=$ziggeomessagedata->contenttype;
                                $request->failuremessage='Sorry Upload Failed! Limit Exceed for Page Count';
                                return $this->index($request);
                            endif;


                     endif;



                    endif;

                    if($messagetype=='video')
                    {   

                        
                        

                        $newziggeomessagedata = $ziggeomessagedata->replicate();
                        unset($newziggeomessagedata->first_name);
                        unset($newziggeomessagedata->last_name);
                        $newziggeomessagedata->user_recipient_id = null;
                        $newziggeomessagedata->delivery_style = 0;
                        $newziggeomessagedata->delivery_date = null;
                        $newziggeomessagedata->delivery_type = 0;
                        $newziggeomessagedata->authorisation_type = 0;
                        $newziggeomessagedata->preference_completed = 0;
                        $newziggeomessagedata->qrid = null;
                        $newziggeomessagedata->qr_package_type = null;
                        $newziggeomessagedata->qr_package_id = null;
                        $newziggeomessagedata->heilroom_package_type = null;
                        $newziggeomessagedata->heilroom_package_id = null;
                        $newziggeomessagedata->created_at = Carbon::now();
                        $newziggeomessagedata->isclone = 1;
                        $newziggeomessagedata->save();

                        $ziggeomessagedatatoken=$ziggeomessagedata->video_token_id;
                    }

                    if($messagetype=='audio')
                    {   
                        $newziggeomessagedata = $ziggeomessagedata->replicate();
                        unset($newziggeomessagedata->first_name);
                        unset($newziggeomessagedata->last_name);
                        $newziggeomessagedata->user_recipient_id = null;
                        $newziggeomessagedata->delivery_style = 0;
                        $newziggeomessagedata->delivery_date = null;
                        $newziggeomessagedata->delivery_type = 0;
                        $newziggeomessagedata->authorisation_type = 0;
                        $newziggeomessagedata->preference_completed = 0;
                        $newziggeomessagedata->qrid = null;
                        $newziggeomessagedata->qr_package_type = null;
                        $newziggeomessagedata->qr_package_id = null;
                        $newziggeomessagedata->heilroom_package_type = null;
                        $newziggeomessagedata->heilroom_package_id = null;
                        $newziggeomessagedata->created_at = Carbon::now();
                        $newziggeomessagedata->isclone = 1;
                        $newziggeomessagedata->save();

                        $ziggeomessagedatatoken=$ziggeomessagedata->audio_token_id;
                    }

                    if($messagetype=='letter')
                    {   
                        $newziggeomessagedata = $ziggeomessagedata->replicate();
                        unset($newziggeomessagedata->first_name);
                        unset($newziggeomessagedata->last_name);
                        $newziggeomessagedata->user_recipient_id = null;
                        $newziggeomessagedata->delivery_style = 0;
                        $newziggeomessagedata->delivery_date = null;
                        $newziggeomessagedata->delivery_type = 0;
                        $newziggeomessagedata->authorisation_type = 0;
                        $newziggeomessagedata->preference_completed = 0;
                        $newziggeomessagedata->qrid = null;
                        $newziggeomessagedata->qr_package_type = null;
                        $newziggeomessagedata->qr_package_id = null;
                        $newziggeomessagedata->heilroom_package_type = null;
                        $newziggeomessagedata->heilroom_package_id = null;
                        $newziggeomessagedata->created_at = Carbon::now();
                        $newziggeomessagedata->isclone = 1;
                        $newziggeomessagedata->save();

                        $ziggeomessagedatatoken=$ziggeomessagedata->text_token_id;
                        
                    }

                    DB::commit();
                    $request = new \Illuminate\Http\Request();
                    $request->ajax='true';
                    $request->messagetype=$messagetype;
                    $request->contenttype=$ziggeomessagedata->contenttype;
                    $request->token=$ziggeomessagedatatoken;
                    $request->successmessage='Message Cloned Successfully';
                    return $this->index($request);
                    

                endif;


                if($actiontype=='delete-message'):
                    DB::beginTransaction();
                   
                     $ziggeomessagedata->delete();

                    DB::commit();

                    $request = new \Illuminate\Http\Request();
                    $request->ajax='true';
                    $request->successmessage='Message Deleted Successfully';
                    return $this->index($request);

                endif;

                if($actiontype=='update-qrcode'):

                   DB::beginTransaction();

                   
                    if($qrdeliverystyle=='2' && empty($ziggeomessagedata->qr_package_id)):


                       $FindQrCodeAddon= User::FindQrCodeAddon($user->id);

                        if(isset($FindQrCodeAddon) && !empty($FindQrCodeAddon)):

                            $FindQrCodeAddon->linked_message_type=$messagetype;
                            $FindQrCodeAddon->linked_message_id=$id;
                            $FindQrCodeAddon->save();
                            $ziggeomessagedata->qr_package_id=$FindQrCodeAddon->user_addon_package_id;
                            $ziggeomessagedata->qr_package_type=2;
                            $ziggeomessagedata->qrid=$FindQrCodeAddon->qrid;
                            $ziggeomessagedata->qrdeliverystyle=$qrdeliverystyle;

                        else:


                            $FindStandaloneQrCode= User::FindStandaloneQrCode($user->id);

                            if(isset($FindStandaloneQrCode) && !empty($FindStandaloneQrCode)):

                            $FindStandaloneQrCode->linked_message_type=$messagetype;
                            $FindStandaloneQrCode->linked_message_id=$id;
                            $FindStandaloneQrCode->save();
                            $ziggeomessagedata->qr_package_id=$FindStandaloneQrCode->user_standalone_package_id;
                            $ziggeomessagedata->qr_package_type=3;
                            $ziggeomessagedata->qrid=$FindStandaloneQrCode->qrid;
                            $ziggeomessagedata->qrdeliverystyle=$qrdeliverystyle;

                            endif;

                        endif;

                            $recipientencodedcode=$this->Encrypt('recipient-/-'.$type."-/-".$id);

                            $dataurl=url('/')."/message-verification/".$recipientencodedcode;
                            

                            $curl = curl_init();

                              $CURLOPT_POSTFIELDS=[
                                  "apikey"=>"eWpSNXijB0c26wtsG3HQ",
                                  "qrid"=>$ziggeomessagedata->qrid,
                                  "websiteurl"=>$dataurl
                                 
                              ];

                              curl_setopt_array($curl, array(
                                CURLOPT_URL => 'https://api.qr.io/v1/edit',
                                CURLOPT_RETURNTRANSFER => true,
                                CURLOPT_ENCODING => '',
                                CURLOPT_MAXREDIRS => 10,
                                CURLOPT_TIMEOUT => 0,
                                CURLOPT_FOLLOWLOCATION => true,
                                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                CURLOPT_CUSTOMREQUEST => 'POST',
                                CURLOPT_POSTFIELDS =>json_encode($CURLOPT_POSTFIELDS),
                                CURLOPT_HTTPHEADER => array(
                                  'Content-Type: application/json'
                                ),
                              ));

                              $response = curl_exec($curl);
                              $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                              
                              $response=json_decode($response,true);

                              curl_close($curl);

                            if($http_status==200 && isset($response['qrid']) && !empty($response['qrid'])):

                                 $qrid=$response['qrid'];

                            endif;


                    endif;


                    if($qrdeliverystyle=='1' && empty($ziggeomessagedata->qr_package_id)):

                        $ziggeomessagedata->qrdeliverystyle=$qrdeliverystyle;

                    endif;

                    if($qrdeliverystyle=='1' && !empty($ziggeomessagedata->qr_package_id)):

                        $packageid=$ziggeomessagedata->qr_package_id;
                        $packagetype=$ziggeomessagedata->qr_package_type;
                        $qrid=$ziggeomessagedata->qrid;

                        if(!empty($packageid) && !empty($packagetype) && !empty($qrid)):
                            
                            $packagedata=UserAddonPackages::find($packageid);
                        
                             if(isset($packagedata) && !empty($packagedata)):
                                $packagedata->linked_message_type=null;
                                $packagedata->linked_message_id=null;
                                $packagedata->save();
                            endif;

                            $ziggeomessagedata->qr_package_type=null;
                            $ziggeomessagedata->qr_package_id=null;
                            $ziggeomessagedata->qrid=null;
                            $ziggeomessagedata->qrdeliverystyle=$qrdeliverystyle;

                        endif;

                    endif;

                    

                    

                endif;

                $step1=isset($ziggeomessagedata->user_recipient_id) && !empty($ziggeomessagedata->user_recipient_id) ? 1 : 0;
                $step2=isset($ziggeomessagedata->delivery_type) && !empty($ziggeomessagedata->delivery_type) ? 1 : 0;
                $step3=isset($ziggeomessagedata->authorisation_type) && !empty($ziggeomessagedata->authorisation_type) ? 1 : 0;
                $step4=isset($ziggeomessagedata->delivery_style) && !empty($ziggeomessagedata->delivery_style) ? 1 : 0;

                $stepcount=$step1+$step2+$step3+$step4;
                $data['stepcount']=0;
                if($stepcount==4 && ($actiontype!=='update-message' || $actiontype!=='clone-message' || $actiontype!=='delete-message')):
                    $ziggeomessagedata->preference_completed=1;
                    
                    $data['stepcount']=$stepcount;
                endif;
                $data['hasdemopackage']=$user->hasdemopackage;
                $usersubscription = UserPackages::GetActivePlan($user->id);
                $data['usersubscription'] =$usersubscription;
                $data['qrcodestandalone'] = User::GetQrCodeStandalone($user->id);
                $data['handdeliveredstandalone'] = User::GetHanddeliveredStandalone($user->id);
                $html = view('my-messages.message-body',$data)->render();

                $ziggeomessagedata->save();


                DB::commit();
                return response()->json(['status' => 'success','html' => $html]);

                

            endif; 
            } catch (TrawableException $th) {
           DB::rollback();
           return response()->json(['status' => 'fail']);

        } catch (QueryException $qe) {
            DB::rollback();
           return response()->json(['status' => 'fail']);
        }
           

    }


    public function ShowMessageQRDetails(Request $request)
    {
        $user=Auth::user();
        $authid=User::GetAuthID($user);
        
        
       try
        {   
            $validator = Validator::make($request->all(), [

            'messagetype' => 'required',
            'id' => 'required',
            
            
            ]);

            if ($validator->fails()) :

            return response()->json(['status' => 'fail']);

            endif;

            $messagetype=$request->messagetype;
            $id=$request->id;
            $actiontype=$request->actiontype;

            if($messagetype=='video')
            {
               
               $ziggeomessagedata=ZiggeoUserVideos::FindVideoByUserId($user->id,$id);

            }

            if($messagetype=='audio')
            {
                $ziggeomessagedata=ZiggeoUserAudios::FindAudioByUserId($user->id,$id);
            }

            if($messagetype=='letter')
            {
               $ziggeomessagedata=ZiggeoUserTexts::FindTextMessageByUserId($user->id,$id);
            }

            if(isset($ziggeomessagedata) && !empty($ziggeomessagedata)):

                $data['messagedata']=$ziggeomessagedata;
                $data['messagetype']=$messagetype;
                $data['messageid']=$id;
                $data['actiontype']=$actiontype;
                $recipients = MyRecipients::GetActiveAll($authid);
                $data['recipients'] = $recipients;
                $html = view('my-messages.message-qr',$data)->render();

                return response()->json(['status' => 'success','html' => $html]);

            endif;

            


        
        } catch (TrawableException $th) {
           
           return response()->json(['status' => 'fail']);

        } catch (QueryException $qe) {
            
           return response()->json(['status' => 'fail']);
        }
        

        

       
    }

    function DownloadMessageQRDetails(Request $request)
    {
        $link=$request->link;

        $filename = 'qr.jpg';
        $tempImage = tempnam(sys_get_temp_dir(), $filename);
        copy($link,$tempImage);

        return response()->download($tempImage, $filename);

    }
    

 function Encrypt($text)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_encrypt($text, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }


    function Decrypt($text)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_decrypt(base64_decode($text), $encrypt_method, $key, 0, $iv);
        return $output;
    }

    

}