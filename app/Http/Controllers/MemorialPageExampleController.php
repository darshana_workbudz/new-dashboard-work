<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  Auth;

class MemorialPageExampleController extends Controller
{

     function __construct()
    {
        $this->middleware('auth');
        
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $user=Auth::id();
        $isajax= $request->ajax;
        $data['isajax']=$isajax;
        $data['old'] = $request;
        if($isajax=='true'):

            $html = view('dashboard',$data)->render();

            return response()->json(['status'=>'success','event'=>'refresh','html'=>$html]);

        else:
            return view('dashboard',$data);
        endif;
    }


    public function MemorialPageExample(Request $request)
    {
        $isajax= $request->ajax;
        $data['isajax']=$isajax;
        if($isajax=='true'):

        $html = view('app.memorial-page.example',$data)->render();

        return response()->json(['status'=>'success','event'=>'refresh','html'=>$html]);

        else:
            return view('app.memorial-page.example',$data);
        endif;
    }
}
