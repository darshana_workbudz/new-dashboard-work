<?php
namespace App\Http\Controllers\OrganisationControllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Coupons;
use App\Models\SharedCoupons;
use App\Models\SharedOrganisationPackages;
use App\Models\UserPackages;
use App\Models\Packages;
use App\Models\PromotionalCodes;
use App\Models\UserMembers;
use App\Models\UserReceipts;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Http\Controllers\SendInBlueController;
class OurAccountPartnerController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
        
    }



    public function index(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        
        if ($isajax == 'true'):

            $html = view('our-partners.our-account', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('partner.our-account')]);

        else:

            return view('our-partners.our-account', $data);

        endif;
    }

    public function OurBilling(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['receipts'] = UserReceipts::GetByUser($user->id);
        if ($isajax == 'true'):

            $html = view('our-partners.our-billing', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('partner.billings')]);

        else:

            return view('our-partners.our-billing', $data);

        endif;
    }
    
    public function OurOrganisationalCodes(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['promotionalcodes'] = SharedCoupons::GetActiveByUser($user->id);
        if ($isajax == 'true'):

            $html = view('our-partners.our-coupon-codes', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('partner.coupon-codes')]);

        else:

            return view('our-partners.our-coupon-codes', $data);

        endif;
    }
    
    public function BuyPackages(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['prepurchasepackages'] = UserPackages::GetPartnersAllActivePlan($user->id);
        $data['allonceoffplans'] = Packages::AllOnceoffPackages();
        $data['allstandaloneqrplans'] = Packages::AllStandAloneQrPackages();
        $data['allstandaloneheirloomplans'] = Packages::AllStandAloneHeirloomPackages();

        if ($isajax == 'true'):

            $html = view('our-partners.buy-packages', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('partner.buy-packages')]);

        else:

            return view('our-partners.buy-packages', $data);

        endif;
    }

    public function PrePurchasePackages(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['prepurchasepackages'] = UserPackages::GetPartnersAllActivePlan($user->id);
        $data['prepurchaseheirloompackages'] = UserPackages::GetPartnersHeirloomActivePlan($user->id);
         $data['prepurchaseqrpackages'] = UserPackages::GetPartnersQrActivePlan($user->id);
        
        if ($isajax == 'true'):

            $html = view('our-partners.our-pre-purchase-packages', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('partner.pre-purchase-packages')]);

        else:

            return view('our-partners.our-pre-purchase-packages', $data);

        endif;
    }


    public function ShowPackageDetails(Request $request)
    {
        try
        {   
         
            $validator = Validator::make($request->all(), [
                'user_package_id'=>'required'
            ]);

            if ($validator->fails()) :

                return response()->json(['status' => 'fail']);

            endif;

            $user=Auth::user();
            $members=UserMembers::GetAllMembers($user->id);
            $sharedpackages=SharedOrganisationPackages::GetActiveByUser($user->id,$request->user_package_id);
           
            $data['sharedpackages']=$sharedpackages;
            $data['members']=$members;
            $data['user_package_id']=$request->user_package_id;
            
            $html = view('our-partners.share-package-to-member',$data)->render();

            return response()->json(['status' => 'success','html' => $html]);

        } catch (TrawableException $th) {
         
         return response()->json(['status' => 'fail']);

     } catch (QueryException $qe) {
        
         return response()->json(['status' => 'fail']);
     }        
    }


    public function ShowPackageDetailsPost(Request $request)
    {
         $validator = Validator::make($request->all(), [
                'user_package_id'=>'required',
                'members'=>'required'
            ]);

            if ($validator->fails()) :

            return response()->json(['status' => 'fail']);

            endif;
        try
        { 
            $user=Auth::user();
            $members=$request->members;
            $user_package_id=$request->user_package_id;
            if(isset($members) && !empty($members)):
                foreach($members as $datavalue):
                    $userdata=User::find($datavalue);
                    $checkexist=SharedOrganisationPackages::where('user_package_id',$user_package_id)->where('member_user_id',$datavalue)->first();

                    if(!$checkexist):

                        SharedOrganisationPackages::create([
                            'created_by'=>$user->id,
                            'updated_by'=>$user->id,
                            'updated_at'=>Carbon::now(),
                            'created_at'=>Carbon::now(),
                            'member_user_id'=>$datavalue,
                            'user_package_id'=>$user_package_id
                        ]);

                        $user_package=UserPackages::where('user_package_id',$user_package_id)
                        ->first();

                        $params = array(
                        'codes' => $user_package->package_code,
                        );

                        $email=$userdata->email;
                        SendInBlueController::SendTransactionEmail(44,$email,$params);
                    endif;



                endforeach;

               
            endif;

            $request = new \Illuminate\Http\Request();
            $request->ajax='true';
            $request->successmessage='Packages Shared Successfully';
            return $this->PrePurchasePackages($request);
            

        } catch (TrawableException $th) {
           
           return response()->json(['status' => 'fail']);

        } catch (QueryException $qe) {
            
           return response()->json(['status' => 'fail']);
        }        
     
    }
    


    

}