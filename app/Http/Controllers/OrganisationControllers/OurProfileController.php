<?php
namespace App\Http\Controllers\OrganisationControllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use Hash;
use Session;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Models\ReasonCreatingAccount;
use App\Models\MyStatus;
use App\Models\OrganisationDepartment;
use App\Models\OrganisationRoles;
use Storage;
use File;
use Ziggeo;
use Log;

class OurProfileController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
        
    }



    public function index(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        
        if ($isajax == 'true'):

            $html = view('our-partners.our-profile', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('partner.our-profile')]);

        else:

            return view('our-partners.our-profile', $data);

        endif;
    }

    public function ChangePassword(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        
        if ($isajax == 'true'):

            $html = view('our-partners.change-password', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('partner.change-password')]);

        else:

            return view('our-partners.change-password', $data);

        endif;
    }
    
    public function OurOrganisationDetails(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['userdata']=$user;
        $data['phonecodes'] = DB::table('tbl_country')->orderBy('name','asc')->get();
        $data['reason'] = ReasonCreatingAccount::GetActive();
        $data['mystatus'] = MyStatus::GetActive();
        $data['organisation_department'] = OrganisationDepartment::GetActive();
        $data['organisation_roles'] = OrganisationRoles::GetActive();
      
        if ($isajax == 'true'):

            $html = view('our-partners.our-organisational-details', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('partner.our-organisational-details')]);

        else:

            return view('our-partners.our-organisational-details', $data);

        endif;
    }
  
    

    public function UpdateProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'first_name' => ['required', 'alpha','min:2', 'max:255'],
            'last_name' => ['required', 'alpha','min:2', 'max:255'],
            'phone_code' => ['required_if:isorganisation,==,1','nullable'],
            'phone_number' => ['required_if:isorganisation,==,1','nullable','min:10','max:10'],
            'address_line_1' => ['required_if:isorganisation,==,1','nullable','min:10', 'max:255'],
            'postcode' => ['required_if:isorganisation,==,1','nullable','min:4', 'max:255'],
            'country' => ['required_if:isorganisation,==,1','nullable','string','min:2', 'max:255'],
            'state' => ['required_if:isorganisation,==,1','nullable','string','min:2', 'max:255'],
            'dob'=>['before:22 years ago','nullable'],
            'other_reason' => ['required_if:reason,==,7','nullable','string','min:2','max:255'],
            'organisation_role'=>['required_if:isorganisation,==,1','nullable'],
            


        ],
        [
        'company_address.required_if' => 'The company address field is required',
        'organisation_role.required_if' => 'The organisation role field is required',
        'postcode.required_if' => 'The postcode field is required',
        'country.required_if' => 'The country field is required',
        'state.required_if' => 'The state field is required',
        'organisation_name.required_if' => 'The organisation name field is required',
        'phone_code.required_if' => 'The phone code field is required',
        'phone_number.required_if' => 'The phone number field is required',
        'address_line_1.required_if' => 'The address line 1 field is required',
        'other_role.required_if' => 'please fill your role in above field',
        'other_reason.required_if' => 'please fill your reason to create account',
        'organisation_department.required_if' => 'The organisation department field is required'
        ]
        );
       
        $isajax = $request->ajax;

        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['phonecodes'] = DB::table('tbl_country')->orderBy('name','asc')->get();
        
        if ($validator->fails()) :
            
             $request->error = $validator->getMessageBag()->toArray();
            
             return $this->OurOrganisationDetails($request);

        endif;

        try {

            DB::beginTransaction();

                $user = Auth::user();
                $date = str_replace('/', '-', $request->dob);
                $user->first_name = isset($request->first_name) && !empty($request->first_name) ? $request->first_name : null;
                $user->last_name = isset($request->last_name) && !empty($request->last_name) ? $request->last_name : null;
                $user->phone_code = isset($request->phone_code) && !empty($request->phone_code) ? $request->phone_code : null;
                $user->phone_number = isset($request->phone_number) && !empty($request->phone_number) ? $request->phone_number : null;
                
                $user->address_1 = isset($request->address_line_1) && !empty($request->address_line_1) ? $request->address_line_1 : null;
                $user->address_2 = isset($request->address_line_2) && !empty($request->address_line_2) ? $request->address_line_2 : null;
                $user->address_3 = isset($request->address_line_3) && !empty($request->address_line_3) ? $request->address_line_3 : null;
                $user->country = isset($request->country) && !empty($request->country) ? $request->country : null;
                $user->postcode = isset($request->postcode) && !empty($request->postcode) ? $request->postcode : null;
                $user->organisation_role = isset($request->organisation_role) && !empty($request->organisation_role) ? $request->organisation_role : null;
                $user->other_role = isset($request->other_role) && !empty($request->other_role) ? $request->other_role : null;
                $user->dob = isset($request->dob) &&  !empty($request->dob) ? date('Y-m-d',strtotime($date)) : null;
                $user->organisation_department = isset($request->organisation_department) && !empty($request->organisation_department) ? $request->organisation_department : null;
                $user->state = isset($request->state) && !empty($request->state) ? $request->state : null;
                $user->other_reason = isset($request->other_reason) && !empty($request->other_reason) ? $request->other_reason : null;
                $user->mystatus = isset($request->mystatus) && !empty($request->mystatus) ? $request->mystatus : null;
                $user->other_mystatus = isset($request->other_mystatus) && !empty($request->other_mystatus) ? $request->other_mystatus : null;
                $user->reason = isset($request->reason) && !empty($request->reason) ? $request->reason : 0;
                $user->updated_by = $user->id;
                $user->updated_at = Carbon::now();
                $user->password = \Hash::make(trim($request->password));

                $user->save();

                 DB::commit();
                $request = new \Illuminate\Http\Request();
                $request->ajax='true';
                $request->successmessage='Organisation Details Updated Successfully';
                return $this->OurOrganisationDetails($request);

               

        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }
    }


    public function UpdatePassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'current_password' => ['required'],
            'new_password' => ['required'],
            'new_confirm_password' => ['required','same:new_password'],
        ]);


        if ($validator->fails()) :
            
             $request->error = $validator->getMessageBag()->toArray();
            
             return $this->ChangePassword($request);

        endif;


        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
          
                $validator->errors()->add('current_password', 'Your current password does not matches with the password');

                $request->error = $validator->getMessageBag()->toArray();
            
                return $this->ChangePassword($request);
        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){


            $validator->errors()->add('new_password', 'New Password cannot be same as your current password.');

            $request->error = $validator->getMessageBag()->toArray();

             return $this->ChangePassword($request);
            
        }


        $user = Auth::user();
        $user->password = Hash::make($request->get('new_password'));
        $user->save();

        $request = new \Illuminate\Http\Request();
        $request->ajax='true';
        $request->successmessage='Password successfully changed!';

       return $this->ChangePassword($request);
        


    }
    

}