<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Packages;
use App\Models\Coupons;
use App\Models\PromotionalCodes;
use App\Models\MyRecipients;
use App\Http\Controllers\SendInBlueController;
use App\Models\VerficationQuestions;
use App\Models\UserAuthorizedPerson;
use App\Models\VerificationProcessStep;
use \Stripe\Stripe;
class PromotionalCodesController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
    	
    }


    
    public function index(Request $request,$id)
    {
        $user=Auth::user();
        $authid=User::GetAuthID($user);
        $isajax=$request->ajax;

		$data['isajax'] = $isajax;
        $data['old'] = $request;
        $coupons = Coupons::find($id);
        $data['coupons']=$coupons;
        $data['promotionalcodes'] = PromotionalCodes::GetActiveByCouponId($id);
        
        if ($isajax == 'true'):

            $html = view('promotional-codes.index', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('manage-promotional-codes.index',$coupons->coupon_id)]);

        else:

            return view('promotional-codes.index', $data);

        endif;
    }


    public function create(Request $request,$id)
    {   
        $user=Auth::user();
        $authid=User::GetAuthID($user);
        $isajax=$request->ajax;

        $data['isajax'] = $isajax;
        $data['old'] = $request; 
        $coupons = Coupons::find($id);
        $data['coupons']=$coupons;

        if ($isajax == 'true'):

            $html = view('promotional-codes.create',$data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('manage-promotional-codes.create',$coupons->coupon_id)]);

        else:

            return view('promotional-codes.create',$data);

        endif;
            
    }


    public function store(Request $request,$id)
    {
       
    	$validator = Validator::make($request->all(), [
            'name' => 'required',
        ]
    	);

        try
        {

        $user=Auth::user();
        
        if ($validator->fails()) :
            
            $request->error = $validator->getMessageBag()->toArray();
            
            return $this->create($request,$id);

        endif;

        $coupons = Coupons::find($id);
        $stripe = new \Stripe\StripeClient(env('API_KEY_STRIPE'));

        $promotionalcodesarray=array(
		  'code'=>$request->name,
          'coupon' => $coupons->stripe_coupon_id
		);
        
        
		$response=$stripe->promotionCodes->create($promotionalcodesarray);

        
		if($response):
        sleep(05);
        $request = new \Illuminate\Http\Request();
        $request->ajax='true';
        $request->successmessage='New Promotional Code Added Successfully';
        return $this->index($request,$id);

    	endif;
        
        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

        $request->failuremessage='Oops! Something went wrong please try again';
        return $this->create($request,$id);
        
        
    }


    
    
}
