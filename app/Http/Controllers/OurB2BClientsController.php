<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Models\UserMembers;
use App\Models\User;
use App\Models\Coupons;
use App\Models\SharedCoupons;
use App\Models\PromotionalCodes;
use App\Models\ReasonCreatingAccount;
use App\Models\MyStatus;
use App\Models\OrganisationDepartment;
use App\Models\OrganisationRoles;
use App\Models\UserReceipts;
use App\Models\UserPackages;
use App\Models\UserClients;
use App\Http\Controllers\SendInBlueController;

class OurB2BClientsController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
        
    }



    public function index(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['b2bclients'] = User::GetOurB2BUsers();
       
        $useridentity=$user->email;
        $useridentity=$this->Encrypt($useridentity);
        $invitation_link =  route('verify-registration-partner').'?key='.$useridentity;
        $data['invitation_link']=$invitation_link;
        Session::put('invitation_link', $invitation_link);
        if ($isajax == 'true'):

            $html = view('admin.b2b-clients.index', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('b2b-clients.index')]);

        else:

            return view('admin.b2b-clients.index', $data);

        endif;
    }

  

    public function B2BClientsAccountDetails(Request $request,$id)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $id;
        $data['error'] = $request->error;
        $userdata=User::find($id);
        $data['userdata']=$userdata;
        $data['phonecodes'] = DB::table('tbl_country')->orderBy('name','asc')->get();
        $data['reason'] = ReasonCreatingAccount::GetActive();
        $data['mystatus'] = MyStatus::GetActive();
        $data['organisation_department'] = OrganisationDepartment::GetActive();
        $data['organisation_roles'] = OrganisationRoles::GetActive();
        $data['receipts'] = UserReceipts::GetByUser($id);
        $data['activepackages'] = UserPackages::GetActiveByUserId($id);
        $data['totalclients'] = UserClients::where('organisation_code',$userdata->organisation_code)->count();
        $data['memberscount'] = UserMembers::GetAllMembersCount($id);
        $data['totalpackages'] = UserPackages::GetPartnersAllActivePlanCount($id);
        $data['totalcoupons'] = SharedCoupons::GetAllActiveCount($id);
        
        if ($isajax == 'true'):

            $html = view('admin.b2b-clients.account-details', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('b2b-clients.details.account-details',$id)]);

        else:

            return view('admin.b2b-clients.account-details', $data);

        endif;
    }




    function Encrypt($text)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_encrypt($text, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }


    function Decrypt($text)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_decrypt(base64_decode($text), $encrypt_method, $key, 0, $iv);
        return $output;
    }

    

    public function ShareCouponsB2BClientPost(Request $request)
    {
          
            
            $validator = Validator::make($request->all(), [
                'user_id'=>'required',
                'promotionalcodes'=>'required'
            ]);

            if ($validator->fails()) :

            return response()->json(['status' => 'fail']);

            endif;
        try
        { 
            $user=Auth::user();
            $promotionalcodes=$request->promotionalcodes;
            $user_id=$request->user_id;
            $userdata=User::find($user_id);
            if(isset($promotionalcodes) && !empty($promotionalcodes)):
                foreach($promotionalcodes as $datavalue):

                    $checkexist=SharedCoupons::where('user_id',$user_id)->where('promotional_code_id',$datavalue)->first();

                    if(!$checkexist):
                        SharedCoupons::create([
                            'created_by'=>$user->id,
                            'updated_by'=>$user->id,
                            'updated_at'=>Carbon::now(),
                            'created_at'=>Carbon::now(),
                            'promotional_code_id'=>$datavalue,
                            'user_id'=>$user_id
                        ]);
                    endif;

                endforeach;
                $codes=PromotionalCodes::whereIn('promotional_code_id',$promotionalcodes)->select('code')->get()->toArray();
                if(isset($codes) && !empty($codes)):

                    $params = array(
                    'codes' => $codes,
                    'subject' => 'Special off%: find your Discount Codes inside'
                    );
                
                $email=$userdata->email;
                SendInBlueController::SendTransactionEmail(44,$email,$params);
                endif;
            endif;

            $request = new \Illuminate\Http\Request();
            $request->ajax='true';
            $request->successmessage='Coupon Code Shared Successfully';
            return $this->index($request);
            

        } catch (TrawableException $th) {
           
           return response()->json(['status' => 'fail']);

        } catch (QueryException $qe) {
            
           return response()->json(['status' => 'fail']);
        }        
    }

}