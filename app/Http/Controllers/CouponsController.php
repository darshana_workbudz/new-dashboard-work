<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Packages;
use App\Models\Coupons;
use App\Models\SharedCoupons;
use App\Models\PromotionalCodes;
use App\Models\MyRecipients;
use App\Http\Controllers\SendInBlueController;
use App\Models\VerficationQuestions;
use App\Models\UserAuthorizedPerson;
use App\Models\VerificationProcessStep;
use \Stripe\Stripe;
class CouponsController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
        
    }


    
    public function index(Request $request)
    {
        $user=Auth::user();
        $authid=User::GetAuthID($user);
        $isajax=$request->ajax;

        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['coupons'] = Coupons::GetActive();
        
        if ($isajax == 'true'):

            $html = view('coupons.index', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('manage-coupons.index')]);

        else:

            return view('coupons.index', $data);

        endif;
    }


    public function create(Request $request)
    {   
        $user=Auth::user();
        $authid=User::GetAuthID($user);
        $isajax=$request->ajax;

        $data['isajax'] = $isajax;
        $data['old'] = $request; 
        
        if ($isajax == 'true'):

            $html = view('coupons.create',$data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('manage-coupons.create')]);

        else:

            return view('coupons.create',$data);

        endif;
        
    }


    public function store(Request $request)
    {
     
    	$validator = Validator::make($request->all(), [

            'name' => 'required',
            'type' => 'required',
            'percent_off' => ['required_if:type,==,1'],
            'amount_off' => ['required_if:type,==,2'],
            
        ],
        [
         
            'percent_off.required_if'=>'This Field is required',
            'amount_off.required_if'=>'This Field is required '
            
            

        ]
    );

       try
       {

        $user=Auth::user();
        
        if ($validator->fails()) :
            
            $request->error = $validator->getMessageBag()->toArray();
            
            return $this->create($request);

        endif;
        
        $stripe = new \Stripe\StripeClient(env('API_KEY_STRIPE'));

        $couponsarray=array(
            'duration' => 'forever',
            'name'=>$request->name,
        );
        $type=$request->type;
        if(isset($type) && $type=='1'):
        	$couponsarray['percent_off']=$request->percent_off;
        endif;
        if(isset($type) && $type=='2'):
        	$couponsarray['amount_off']=$request->amount_off;
        endif;
        
        $response=$stripe->coupons->create($couponsarray);

        
        if($response):
            sleep(05);    
            $request = new \Illuminate\Http\Request();
            $request->ajax='true';
            $request->successmessage='New Coupon Code Added Successfully';
            return $this->index($request);

        endif;
        
    } catch (TrawableException $th) {
        DB::rollback();
        
    } catch (QueryException $qe) {
        DB::rollback();
        
    }

    $request->failuremessage='Oops! Something went wrong please try again';
    return $this->create($request);
    
    
}




public function ShowPartnerCouponsDetails(Request $request)
{
    try
    {   
     
        $validator = Validator::make($request->all(), [
            'user_id'=>'required'
        ]);

        if ($validator->fails()) :

            return response()->json(['status' => 'fail']);

        endif;

        $user=Auth::user();
        $codes=PromotionalCodes::GetActive();
        $sharedcouponscodes=SharedCoupons::GetActive($request->user_id);
        $data['sharedcouponscodes']=$sharedcouponscodes;
        $data['codes']=$codes;
        $data['user_id']=$request->user_id;
        $html = view('coupons.share-to-partner',$data)->render();

        return response()->json(['status' => 'success','html' => $html]);

    } catch (TrawableException $th) {
     
     return response()->json(['status' => 'fail']);

 } catch (QueryException $qe) {
    
     return response()->json(['status' => 'fail']);
 }        
}

public function ShowB2BClientsCouponsDetails(Request $request)
{
    try
    {   
     
        $validator = Validator::make($request->all(), [
            'user_id'=>'required'
        ]);

        if ($validator->fails()) :

            return response()->json(['status' => 'fail']);

        endif;

        $user=Auth::user();
        $codes=PromotionalCodes::GetActive();
        $sharedcouponscodes=SharedCoupons::GetActive($request->user_id);
        $data['sharedcouponscodes']=$sharedcouponscodes;
        $data['codes']=$codes;
        $data['user_id']=$request->user_id;
        $html = view('coupons.share-to-b2bclients',$data)->render();

        return response()->json(['status' => 'success','html' => $html]);

    } catch (TrawableException $th) {
     
     return response()->json(['status' => 'fail']);

 } catch (QueryException $qe) {
    
     return response()->json(['status' => 'fail']);
 }        
}


public function ShowMemberCouponsDetails(Request $request)
{
    try
    {   
     
        $validator = Validator::make($request->all(), [
            'user_id'=>'required'
        ]);

        if ($validator->fails()) :

            return response()->json(['status' => 'fail']);

        endif;

        $user=Auth::user();
        $codes=SharedCoupons::GetActiveByUser($user->id);
        $sharedcouponscodes=SharedCoupons::GetActive($request->user_id);
        $data['sharedcouponscodes']=$sharedcouponscodes;
        $data['codes']=$codes;
        $data['user_id']=$request->user_id;
        $html = view('coupons.share-to-member',$data)->render();

        return response()->json(['status' => 'success','html' => $html]);

    } catch (TrawableException $th) {
     
     return response()->json(['status' => 'fail']);

 } catch (QueryException $qe) {
    
     return response()->json(['status' => 'fail']);
 }        
}


public function ShowClientCouponsDetails(Request $request)
{
    try
    {   
     
        $validator = Validator::make($request->all(), [
            'user_id'=>'required'
        ]);

        if ($validator->fails()) :

            return response()->json(['status' => 'fail']);

        endif;

        $user=Auth::user();
        $codes=SharedCoupons::GetActiveByUser($user->id);
        $sharedcouponscodes=SharedCoupons::GetActive($request->user_id);
        $data['sharedcouponscodes']=$sharedcouponscodes;
        $data['codes']=$codes;
        $data['user_id']=$request->user_id;
        $html = view('coupons.share-to-client',$data)->render();

        return response()->json(['status' => 'success','html' => $html]);

    } catch (TrawableException $th) {
     
     return response()->json(['status' => 'fail']);

 } catch (QueryException $qe) {
    
     return response()->json(['status' => 'fail']);
 }        
}



}
