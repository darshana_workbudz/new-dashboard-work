<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Models\ZiggeoWebhookEvents;
use App\Models\ZiggeoUserVideos;
use App\Models\ZiggeoUserAudios;
use Log;
use Ziggeo;
use Carbon\Carbon;
use App\Models\User;

class ZiggeoWebhookController extends Controller
{
   
    
    

    public function OnVideoCreate(Request $request)
    {

        try
        { 
            DB::beginTransaction();

                 $eventsdata=ZiggeoWebhookEvents::create([

                    'jobstatus'=>0,
                    'eventjson'=>json_encode($request->all()),
                    'created_at'=>Carbon::now(),
                    'eventtype'=>'videocreate'

                 ]);


            DB::commit();  

            $event=$this->ProcessFailedCreatedVideoWebhook($eventsdata->ziggeowebhookevent_id);

            if($event==200):

                DB::beginTransaction();    

                $eventsdata->jobstatus=1;
                $eventupdated=$eventsdata->save();

                DB::commit();

                if($eventupdated):
                    return response(['status' => 'success', 'message' =>"Event Completed Successfully"], 200);  
                endif;

            endif;

        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }
        
        
    }

    public function OnVideoDeleted(Request $request)
    {

        try
        { 
            DB::beginTransaction();

                 $eventsdata=ZiggeoWebhookEvents::create([

                    'jobstatus'=>0,
                    'eventjson'=>json_encode($request->all()),
                    'created_at'=>Carbon::now(),
                    'eventtype'=>'videodelete'
                 ]);

            DB::commit();

        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }   


    }

    public function ProcessFailedCreatedVideoWebhook($id)
    {

        $ziggeoevent=ZiggeoWebhookEvents::where('ziggeowebhookevent_id',$id)
        ->where('eventtype','videocreate')
        ->where('jobstatus',0)
        ->first();

        try
        {    

            DB::beginTransaction();
        
                if(isset($ziggeoevent) && !empty($ziggeoevent)):

                    $webhook_data = json_decode($ziggeoevent->eventjson, true );

                    if(isset($webhook_data['data']['video'])) {
                        $video_data = $webhook_data['data']['video'];
                    }

                    $ziggeo = new Ziggeo(env('ZIGGEOTOKEN'),env('ZIGGEOPRIVATEKEY')); 

                    $result = $ziggeo->videos()->get($video_data['token']);
                    
                    
                    if(isset($result) && !empty($result)):

                        $user_id=isset($result['data']['user_id']) && !empty($result['data']['user_id']) ? $result['data']['user_id'] : null;

                        $messageid=isset($result['data']['id']) && !empty($result['data']['id']) ? $result['data']['id'] : null;


                        $ziggeoarray=array(

                            'user_id'=>isset($result['data']['user_id']) && !empty($result['data']['user_id']) ? $result['data']['user_id'] : null,
                            'video_token_id'=>isset($result['token']) && !empty($result['token']) ? $result['token'] : null,
                            'video_key'=>isset($result['key']) && !empty($result['key']) ? $result['key'] : null,
                            'video_title'=>isset($result['title']) && !empty($result['title']) ? $result['title'] : null,
                            'description'=>isset($result['description']) && !empty($result['description']) ? $result['description'] : null,
                            'tags'=>isset($result['tags'][0]) && !empty($result['tags'][0]) ? $result['tags'][0] : null,
                            'video_file_name'=>isset($result['video_file_name']) && !empty($result['video_file_name']) ? $result['video_file_name'] : null,
                            'only_audio'=>$result['only_audio'],
                            'max_duration'=>isset($result['max_duration']) && !empty($result['max_duration']) ? $result['max_duration'] : null,
                            'duration'=>isset($result['duration']) && !empty($result['duration']) ? floor($result['duration']) : null,
                            'state'=>isset($result['state']) && !empty($result['state']) ? $result['state'] : null,
                            'embed_video_url'=>isset($result['embed_video_url']) && !empty($result['embed_video_url']) ? $result['embed_video_url'] : null,
                            'embed_image_url'=>isset($result['embed_image_url']) && !empty($result['embed_image_url']) ? $result['embed_image_url'] : null,
                            'device_info'=>isset($result['device_info']) && !empty($result['device_info']) ? json_encode($result['device_info']) : null,
                            'submission_date'=>isset($result['submission_date']) && !empty($result['submission_date']) ? $result['submission_date'] : null,
                            'capture_type'=>isset($result['device_info']['capture_type']) && !empty($result['device_info']['capture_type']) ? $result['device_info']['capture_type'] : null,
                            'created_at'=>Carbon::now(),
                            'created_by'=>isset($result['data']['user_id']) && !empty($result['data']['user_id']) ? $result['data']['user_id'] : null,
                             'updated_by'=>isset($result['data']['user_id']) && !empty($result['data']['user_id']) ? $result['data']['user_id'] : null,
                             'updated_at'=>Carbon::now(),
                             'contenttype'=>isset($result['data']['contenttype']) && !empty($result['data']['contenttype']) ? $result['data']['contenttype'] : null,




                        );
                        log::info($result);
                    $user=User::find($user_id);
                    $ziggeoevent=ZiggeoUserVideos::where('video_token_id',$result['token'])->first();

                    // $usedminutes=User::GetUsedMinutes($user->id);
                    // $totalminutes=User::GetTotalResource($user->id);
                    // $hasminutes=($totalminutes-$usedminutes);

                    // if($user->hasdemopackage==1):
                    //     $packages=Packages::where('demopackage',1)->where('slug','subscription')->first();
                    //     $hasminutes=($packages->message_value-$usedminutes);
                    // endif;

                    // $hasminutesseconds=($hasminutes*60);

                    if(isset($messageid) && !empty($messageid)):
                        $ziggeomessagedata=ZiggeoUserVideos::FindVideoByUserId($user->id,$messageid);
                        
                    endif;
        
                    if(isset($ziggeoevent) && !empty($ziggeoevent)):

                        $ziggeoevent->update($ziggeoarray);

                    else:
                    
                        ZiggeoUserVideos::create($ziggeoarray);  

                    endif;

                        

                    if(isset($ziggeomessagedata) && !empty($ziggeomessagedata)):

                        $ziggeomessagedata->delete();


                    endif;

                    



                endif;

            endif;

            DB::commit();



        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }





    }


    public function ProcessFailedDeletedVideoWebhook($id)
    {

        $ziggeoevent=ZiggeoWebhookEvents::where('ziggeowebhookevent_id',$id)
        ->where('jobstatus',0)
        ->first();

        try
        {    

            DB::beginTransaction();
        
                if(isset($ziggeoevent) && !empty($ziggeoevent)):

                        $webhook_data = json_decode($ziggeoevent->eventjson, true );

                        if(isset($webhook_data['data']['video'])) 
                        {
                            $video_data = $webhook_data['data']['video'];
                        }

                        $ziggeoevent=ZiggeoUserVideos::where('video_token_id',$video_data['token'])->first();
            
                        if(isset($ziggeoevent) && !empty($ziggeoevent)):

                            $ziggeoevent->delete($ziggeoarray);

                        endif;

                    

                endif;

            DB::commit();

        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }





    }



    // Audio Webhooks

    public function OnAudioCreate(Request $request)
    {
       try
        { 
            DB::beginTransaction();

                 $eventsdata=ZiggeoWebhookEvents::create([

                    'jobstatus'=>0,
                    'eventjson'=>json_encode($request->all()),
                    'created_at'=>Carbon::now(),
                    'eventtype'=>'audiocreate'

                 ]);

            DB::commit();  

            //$event= $this->ProcessFailedCreatedAudioWebhook($eventsdata->ziggeowebhookevent_id);

            // if($event==200):

            //     DB::beginTransaction();    

            //     $eventsdata->jobstatus=1;
            //     $eventupdated=$eventsdata->save();

            //     DB::commit();

            //     if($eventupdated):
            //         return response(['status' => 'success', 'message' =>"Event Completed Successfully"], 200);  
            //     endif;

            // endif;

        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }
        
        
    }

    public function OnAudioDeleted(Request $request)
    {
        Log::info($request->all());
        try
        { 
            DB::beginTransaction();

                 $eventsdata=ZiggeoWebhookEvents::create([

                    'jobstatus'=>0,
                    'eventjson'=>json_encode($request->all()),
                    'created_at'=>Carbon::now(),
                    'eventtype'=>'audiodelete'
                 ]);

            DB::commit();

        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }   


    }

    public function ProcessFailedCreatedAudioWebhook($id)
    {

        $ziggeoevent=ZiggeoWebhookEvents::where('ziggeowebhookevent_id',$id)
        ->where('eventtype','audiocreate')
        ->where('jobstatus',0)
        ->first();

        try
        {    

            DB::beginTransaction();
        
                if(isset($ziggeoevent) && !empty($ziggeoevent)):

                    $webhook_data = json_decode($ziggeoevent->eventjson, true );

                    if(isset($webhook_data['data']['audio'])) {
                        $audio_data = $webhook_data['data']['audio'];
                    }

                    $ziggeo = new Ziggeo(env('ZIGGEOTOKEN'),env('ZIGGEOPRIVATEKEY'));

                    $result = $ziggeo->audios()->get($audio_data['token']);
                    log::info('audiomessage-------------------START');
                    log::info($result);
                    log::info('audiomessage-------------------END');
                    
                    if(isset($result) && !empty($result)):

                        $ziggeoarray=array(

                            'user_id'=>isset($result['data']['user_id']) && !empty($result['data']['user_id']) ? $result['data']['user_id'] : null,
                            'audio_token_id'=>isset($result['token']) && !empty($result['token']) ? $result['token'] : null,
                            'audio_key'=>isset($result['key']) && !empty($result['key']) ? $result['key'] : null,
                            'audio_title'=>isset($result['title']) && !empty($result['title']) ? $result['title'] : null,
                            'audio_description'=>isset($result['description']) && !empty($result['description']) ? $result['description'] : null,
                            'tags'=>isset($result['tags'][0]) && !empty($result['tags'][0]) ? $result['tags'][0] : null,
                            'max_duration'=>isset($result['max_duration']) && !empty($result['max_duration']) ? $result['max_duration'] : null,
                            'duration'=>isset($result['duration']) && !empty($result['duration']) ? floor($result['duration']) : null,
                            'state'=>isset($result['state']) && !empty($result['state']) ? $result['state'] : null,
                            'embed_audio_url'=>isset($result['embed_audio_url']) && !empty($result['embed_audio_url']) ? $result['embed_audio_url'] : null,
                            'device_info'=>isset($result['device_info']) && !empty($result['device_info']) ? json_encode($result['device_info']) : null,
                            'submission_date'=>isset($result['submission_date']) && !empty($result['submission_date']) ? $result['submission_date'] : null,
                            'capture_type'=>isset($result['device_info']['capture_type']) && !empty($result['device_info']['capture_type']) ? $result['device_info']['capture_type'] : null,
                            'created_at'=>Carbon::now(),
                            'created_by'=>isset($result['data']['user_id']) && !empty($result['data']['user_id']) ? $result['data']['user_id'] : null,
                             'updated_by'=>isset($result['data']['user_id']) && !empty($result['data']['user_id']) ? $result['data']['user_id'] : null,
                             'updated_at'=>Carbon::now(),



                        );

                    $ziggeoevent=ZiggeoUserAudios::where('audio_token_id',$result['token'])->first();
        
                    if(isset($ziggeoevent) && !empty($ziggeoevent)):

                        $ziggeoevent->update($ziggeoarray);

                    else:
                    
                        ZiggeoUserVideos::create($ziggeoarray);                                
                    endif;

                endif;

            endif;

            DB::commit();

        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }





    }


    public function ProcessFailedDeletedAudioWebhook($id)
    {

        $ziggeoevent=ZiggeoWebhookEvents::where('ziggeowebhookevent_id',$id)
        ->where('jobstatus',0)
        ->first();

        try
        {    

            DB::beginTransaction();
        
                if(isset($ziggeoevent) && !empty($ziggeoevent)):

                        $webhook_data = json_decode($ziggeoevent->eventjson, true );

                        if(isset($webhook_data['data']['video'])) 
                        {
                            $video_data = $webhook_data['data']['video'];
                        }

                        $ziggeoevent=ZiggeoUserVideos::where('video_token_id',$video_data['token'])->first();
            
                        if(isset($ziggeoevent) && !empty($ziggeoevent)):

                            $ziggeoevent->delete($ziggeoarray);

                        endif;

                    

                endif;

            DB::commit();

        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }





    }
}
