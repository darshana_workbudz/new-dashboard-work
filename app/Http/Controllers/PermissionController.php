<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Auth;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Session;
use Spatie\Permission\Traits\HasRoles;

class PermissionController extends Controller {

     public function __construct()
  {
      $this->middleware('auth');
  }
    
    public function index() {
        $permissions = Permission::all(); //Get all permissions
        
        return view('permissions/index')->with('permissions', $permissions);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
        $roles = Role::get(); //Get all roles

        return view('permissions/create')->with('roles', $roles);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {
       $this->validate($request, [
            'name'=>'required|max:255|unique:permissions,name',
           
        ]);
        
        $name = $request['name'];
        $permission = new Permission();
        $permission->name = $name;

        $roles = $request['roles'];

        $permission->save();

        if (!empty($request['roles'])) { //If one or more role is selected
            foreach ($roles as $role) {
                $r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record

                $permission = Permission::where('name', '=', $name)->first(); //Match input //permission to db record
                $r->givePermissionTo($permission);
            }
        }

        return redirect('permissions')->with('flash_message',
             'Permission'. $permission->name.' added!');

    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id) {
        return redirect('permissions');
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        $permission = Permission::findOrFail($id);

        return view('permissions.edit', compact('permission'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function editPost(Request $request) {

         $id=$request->input('permission_id');
        $permission = Permission::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:40|unique:permissions,name,'.$id,
           
        ]);
        
         $permission_data = array(
        'name' => $request->input('name'), 
    
  );
        $countrycode_id = Permission::where('id', '=', $id)->update($permission_data);
       
         return redirect('permissions')->with('message', 'Permissions Updated successfully');
       
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id) {
        $permission = Permission::findOrFail($id);

    //Make it impossible to delete this specific permission    
    if ($permission->name == "Administer roles & permissions") {
            return redirect()->route('permissions.index')
            ->with('flash_message',
             'Cannot delete this Permission!');
        }

        $permission->delete();

        return redirect('permissions')->with('flash_message',
             'Permission deleted!');

    }
}