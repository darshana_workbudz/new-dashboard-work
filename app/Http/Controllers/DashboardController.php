<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use SendinBlue;
use GuzzleHttp;
use App\Models\User;
use App\Models\Packages;
use App\Models\UserPackages;
use App\Models\UserAddonPackages;
use App\Models\UserAuthorizedPerson;
use App\Models\CheckInSetting;
use App\Models\UserMembers;
use App\Models\UserClients;
use App\Models\SharedCoupons;
use Carbon\Carbon;
use DB;
use Hash;
use Session;
use App\Http\Controllers\SendInBlueController;
use Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
class DashboardController extends Controller
{


    public function index(Request $request)
    {
        
        $isajax= $request->ajax;
        $data['isajax']=$isajax;

        $userdata=Auth::user();
        $totalminutes=User::GetTotalResource($userdata->id);
        $usedtotalminutes=User::GetUsedMinutes($userdata->id);
        $usedrecipients=User::GetUsedRecipientsLimit($userdata->id);
        $totalrecipients=User::GetTotalRecipients($userdata->id);
        $usedpagecount=User::GetUsedDocLimit($userdata->id);
        $totalpagecount=User::GetTotalDocLimit($userdata->id);
        $haspagecount=$totalpagecount-$usedpagecount;
        $hasrecipients=($totalrecipients-$usedrecipients);

        if($userdata->user_package_id==1 && $userdata->demopackage_email_sent!==1):
           SendInBlueController::UpdateContacttolist($userdata->email,env('Free_Community_Users_List')); //Demo_users

           $userdata->demopackage_email_sent=1;
           $userdata->save();

        endif;

        
        $usersubscription = UserPackages::GetActivePlan($userdata->id);
        $data['usedtotalpersonalisedmessage']=User::GetTotalMessagesCount($userdata->id);
        $data['usersubscription']=$usersubscription;
        $data['hasrecipients']=$hasrecipients;
        $data['usedrecipients']=$usedrecipients;
        $data['totalrecipients']=$totalrecipients;
        $data['usedtotalminutes']=$usedtotalminutes;
        $data['totalminutes']=$totalminutes;
        $data['haspagecount']=$haspagecount;
        $data['usedpagecount']=$usedpagecount;
        $data['totalpagecount']=$totalpagecount;
        $data['qraddoncount']=UserAddonPackages::HasQrCodeAddon($userdata->id);
        $data['heliroomaddoncount']=UserAddonPackages::HasHeirloomAddon($userdata->id);
        $data['pagesaddoncount']=UserAddonPackages::HasExtraPagesAddon($userdata->id);
        $data['minutesaddoncount']=UserAddonPackages::HasExtraMinutesAddon($userdata->id);
        $data['userdata']=$userdata;
        $data['authoriseddata'] = UserAuthorizedPerson::FindByUser($userdata->id);

        if($userdata->user_role_type==3 || $userdata->user_role_type==4):
        $data['totalclients'] = UserClients::where('organisation_code',$userdata->organisation_code)->count();
        $data['memberscount'] = UserMembers::GetAllMembersCount($userdata->id);
        $data['totalpackages'] = UserPackages::GetPartnersAllActivePlanCount($userdata->id);
        $data['totalcoupons'] = SharedCoupons::GetAllActiveCount($userdata->id);
        endif;

        if($userdata->user_role_type==5):
        $data['totalclients'] = UserClients::where('organisation_code',$userdata->organisation_code)->where('user_id',$userdata->id)->count();
        $data['totalclientsmessage'] = UserClients::GetMessageCountofClient($userdata->id);
        $data['totalpackages'] = UserPackages::GetPartnersAllActivePlanCount($userdata->id);
        $data['totalcoupons'] = SharedCoupons::GetAllActiveCount($userdata->id);
        endif;

        $data['checkinsetting']=CheckInSetting::FindByUserId($userdata->id);
        $data['updateCheckinsetting'] = Session::get('updateCheckinsetting');
        Session::forget('updateCheckinsetting');
        if(isset($usersubscription) && !empty($usersubscription)):
        $data['extraaddons']=Packages::getExtraMinutesAddonPackages($usersubscription->package_id);
        $data['pagesaddons']=Packages::getExtraPagesAddonPackages($usersubscription->package_id);
        $data['heliroomaddon'] = Packages::getHeirLoomAddonPackages($usersubscription->package_id);
        $data['qraddon'] = Packages::getQrAddonPackages($usersubscription->package_id);
        endif;
        if($isajax=='true'):

            $html = view('dashboard',$data)->render();

            return response()->json(['status'=>'success','event'=>'refresh','html'=>$html]);

        else:
            return view('dashboard',$data);
        endif;
    }

}
