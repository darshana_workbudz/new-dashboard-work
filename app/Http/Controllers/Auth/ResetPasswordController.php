<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Str;
use DB;
use Carbon\Carbon;
use Illuminate\Auth\Events\PasswordReset;
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */

    public function showResetForm(Request $request, $token = null)
    {
        
        $isajax= $request->ajax;
        $data['isajax']=$isajax;
        $data['old'] = $request;
        $data['token'] = $token;
        $data['email'] = $request->email;

        $checktoken=DB::table('password_resets')
        ->where('email',$request->email)->where('token',$request->token)
        ->where('used',0)->first();

        if(empty($checktoken)):
             return redirect('login');
        endif;

        if($isajax=='true'):

        $html = view('auth.passwords.reset',$data)->render();

        return response()->json(['status'=>'success','event'=>'refresh','html'=>$html]);

        else:
            return view('auth.passwords.reset',$data);
        endif;
    }


    public function reset(Request $request)
    {

        $validator = Validator::make($request->all(), [
                              'token' => 'required',
                              'email' => 'required|email',
                              'password' => 'required|confirmed|min:6',
                           
                        ]);

        $isajax = $request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['token'] =$request->token;
        $data['email'] = $request->email;
  
        if ($validator->fails()) :

            $data['error'] = $validator->getMessageBag()->toArray();

            $html = view('auth.passwords.reset', $data)->render();

            return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);

        endif;

        
        $checktoken=DB::table('password_resets')
        ->where('email',$request->email)->where('token',$request->token)
        ->where('used',0)->first();

        if(empty($checktoken)):

        $validator->errors()->add('token','Sorry invalid Token Found Please try Again');

        $data['error'] = $validator->getMessageBag()->toArray();

        $html = view('auth.passwords.reset', $data)->render();

        return response()->json(['status' => 'fail', 'event' => 'redirect',' routetoredirect' => route('password.request')]);

        endif;


        $userdata=User::where('email',$request->email)->where('isactive','!=',0)->first();

        if(empty($userdata)):

            $validator->errors()->add('email', 'Sorry No Account Found with this Email');

            $data['error'] = $validator->getMessageBag()->toArray();

            $html = view('auth.passwords.reset', $data)->render();

            return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);

        endif;

            DB::table('password_resets')
                ->where('email',$request->email)->where('token',$request->token)
                ->where('used',0)
                ->update(['used'=>1]);

        
        

        $this->setUserPassword($userdata, $request->password);

        $userdata->setRememberToken(Str::random(60));

        $userdata->save();

        event(new PasswordReset($userdata));

        $data['success']="Your Password has been successfully reset";

        $html = view('auth.passwords.reset', $data)->render();

        return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);

    }


    protected $redirectTo = RouteServiceProvider::HOME;
}
