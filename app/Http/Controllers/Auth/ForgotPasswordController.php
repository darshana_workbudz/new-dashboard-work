<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\SendInBlueController;
use App\Models\User;
use Str;
use DB;
use Carbon\Carbon;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    public function showLinkRequestForm(Request $request)
    {

        
        $isajax= $request->ajax;
        $data['isajax']=$isajax;
        $data['old'] = $request;
        if($isajax=='true'):

        $html = view('auth.passwords.email',$data)->render();

        return response()->json(['status'=>'success','event'=>'refresh','html'=>$html]);

        else:
            return view('auth.passwords.email',$data);
        endif;

        
    }

    public function sendResetLinkEmail(Request $request)
    {
         $validator = Validator::make($request->all(), [
                             'email' => 'required|email',
                           
                        ]);

        $isajax = $request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
  
            if ($validator->fails()) :

                $data['error'] = $validator->getMessageBag()->toArray();

                $html = view('auth.passwords.email', $data)->render();

                return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);

            endif;

            $userdata=User::where('email',$request->email)->where('isactive','!=',0)->first();

            if(empty($userdata)):

                $validator->errors()->add('email', 'Sorry No Account Found with this Email');

                $data['error'] = $validator->getMessageBag()->toArray();

                $html = view('auth.passwords.email', $data)->render();

                return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);

            endif;



            $token = Str::random(64);
  
            DB::table('password_resets')->insert([
              'email' => $request->email, 
              'token' => $token, 
              'created_at' => Carbon::now()
            ]);

            $first_name=isset($userdata->first_name) && !empty($userdata->first_name) ? $userdata->first_name : 'there';
            $last_name=isset($userdata->last_name) && !empty($userdata->last_name) ? $userdata->last_name : null;

            $params = array(
                    'url' => route('password.reset',$token).'?email='.$request->email,
                    'FIRSTNAME' => $first_name,
                    'LASTTNAME' => $last_name
                    
            );

            SendInBlueController::SendTransactionEmail((int) env('PASSWORD_RESET_TEMPLATE'),$request->email,$params);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        
        $data['success']="Password Reset Link Sent";

        $html = view('auth.passwords.email', $data)->render();

        return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);
    }

    use SendsPasswordResetEmails;
}
