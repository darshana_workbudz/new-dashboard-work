<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use App\Models\CheckInSetting;
use App\Models\EmailScheduler;
use App\Models\HasAccessLogin;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;
use DateTime;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function showLoginForm(Request $request)
    {
        
        $user=Auth::id();
        $isajax= $request->ajax;
        $data['isajax']=$isajax;
        $data['old'] = $request;
        if($isajax=='true'):

        $html = view('auth.login',$data)->render();

        return response()->json(['status'=>'success','event'=>'refresh','html'=>$html]);

        else:
            return view('auth.login',$data);
        endif;
    }



    public function login(Request $request)
    {   
        $validator = Validator::make($request->all(), [
                            'email' => 'required|string',
                            'password' => 'required|min:6',
                        ]);

        $isajax = $request->ajax;
        $data['success'] = false;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        Session::forget('trustedparty-delegateaccess');
        if ($validator->fails()) :

            $data['error'] = $validator->getMessageBag()->toArray();

            $html = view('auth.login', $data)->render();

            return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);

        endif;

        $userdata = User::where('email',$request->email)->first();

        if(empty($userdata)):


                $delegateaccess=HasAccessLogin::Checkemailexist($request->email);

                if(isset($delegateaccess) && !empty($delegateaccess) && Hash::check($request->password,$delegateaccess->password)):


                    $userdata=User::find($delegateaccess->main_user_id);
                    if(!empty($userdata)):
                        Session::put('trustedparty-delegateaccess',$delegateaccess->myaccountemail);
                        Auth::login($userdata);
                        return response()->json(['status'=>'success','event'=>'redirect','routetoredirect'=>route('dashboard')]);
                    endif;

                endif;


                // $validator->errors()->add('email','Sorry No Account Found with This Email!');

                // $data['error'] = $validator->getMessageBag()->toArray();
                
                // $html = view('auth.login',$data)->render();

                // return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);


        endif;

        if(!empty($userdata) && $userdata->isactive==3 && !empty($userdata->deactivation_date)):
                $datetime1 =new DateTime(date('Y-m-d'));
                $datetime2 =new DateTime($userdata->deactivation_date);
                $difference = $datetime1->diff($datetime2);
                
                if(isset($difference->d) && $difference->d > 14):

                $validator->errors()->add('email','Sorry Your Account is Deactivated For Some Reason Please Contact Support to Resolve!');

                $data['error'] = $validator->getMessageBag()->toArray();
                
                $html = view('auth.login',$data)->render();

                return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);
                endif;

        endif;

        if(!empty($userdata) && $userdata->isactive==2):

                $validator->errors()->add('email','Sorry Your Account is Deactivated For Some Reason Please Contact Support to Resolve!');

                $data['error'] = $validator->getMessageBag()->toArray();
                
                $html = view('auth.login',$data)->render();

                return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);

        endif;

        if(!empty($userdata) && $userdata->isactive==0):

                $validator->errors()->add('email','Sorry Your Email is Not Activated. Please Confirm Your Verification Process');

                $data['error'] = $validator->getMessageBag()->toArray();
                
                $html = view('auth.login',$data)->render();

                return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);

        endif;

        if(Auth::guard('aig')->attempt([
            'email' => $request->email,
            'password' => $request->password],
            $request->filled('remember'))):

            $emailschedulerid=Session::get('updateCheckinsetting');

            $emailscheduler=EmailScheduler::find($emailschedulerid);
                       
            if(isset($emailscheduler) && !empty($emailscheduler)):

            $user=Auth::user();

            if($emailscheduler->user_id==$user->id):

                    $checkinsettingdata=CheckInSetting::FindByUserId($user->id);
            
                    $followup_period_id=$checkinsettingdata->followup_period_id;

                    $followup_period_data=FollowupPeriod::find($followup_period_id);

                    $stage1_date=date('Y-m-d',strtotime($followup_period_id." month"));
                    $stage2_date=date('Y-m-d', strtotime($stage1_date. ' +1 days'));
                    $datatosave['stage1_completed']=0;
                    $datatosave['stage2_completed']=0;
                    $datatosave['stage3_completed']=0;
                    $datatosave['stage1_date']=$stage1_date;
                    $datatosave['stage2_date']=$stage2_date;
                    $datatosave['stage2_count']=0;
                    $datatosave['stage3_date']=null;

                    $updated=$checkinsettingdata->update($datatosave);

                    return response()->json(['status'=>'success','event'=>'redirect','routetoredirect'=>route('dashboard')]);

                    endif;

            

            endif;
        
            return response()->json(['status'=>'success','event'=>'redirect','routetoredirect'=>route('dashboard')]);
        
        else:
           
            $validator->errors()->add('password','Sorry the password you entered is incorrect Please try Again!');

            $data['error'] = $validator->getMessageBag()->toArray();
                
            $html = view('auth.login',$data)->render();

            return response()->json(['status' => 'fail', 'event' => 'refresh', 'html' => $html]);

        endif;
        
    }


    
}
