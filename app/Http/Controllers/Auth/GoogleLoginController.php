<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use Illuminate\Validation\ValidationException;
use App\Models\User;
use App\Http\Controllers\SendInBlueController;
use Carbon\Carbon;
use SendinBlue;
use GuzzleHttp;
use Socialite;
use Session;
use Auth;
use Hash;
use DB;


class GoogleLoginController extends Controller
{

public function redirect()
{
    return Socialite::driver('google')->redirect();
}

public function callback()
{
        try
        {
            $authuserdata = Socialite::driver('google')->user();
        } 
        catch (\Exception $e) {

           return redirect('/login')->with('alert','Sorry ! Invalid Credentials Please try Again');
        }
        
        if(isset($authuserdata) && count($authuserdata->user)>0):

                $userdataarray=$authuserdata->user;

                $email=isset($userdataarray['email']) ? $userdataarray['email'] : '';
                $email_verified=isset($userdataarray['verified_email']) ? $userdataarray['verified_email'] : false;

                if(isset($email) && !empty($email) && $email_verified==true): 
                    
                    $userdata = User::where('email',$email)->first();

                    if(!empty($userdata) && $userdata->isactive==2):

                            return redirect('/login')->with('alert','Sorry Your is Not Active,Please Confirm Your Verification Process');

                    endif;

                    if(!empty($userdata) && $userdata->isactive==0):

                        if(strtotime($userdata->verification_expiry_time)<=strtotime(date('Y-m-d H:i:s'))):
                    
                            $url = $this->GenerateVerificationlink(uniqid().'-'.$email);
                            $userdata->verification_link=$url;
                            $userdata->verification_expiry_time= Carbon::now()->addHours(24)->format('Y-m-d H:i:s');
                            $userdata->save();
                        else:
                            $url = $userdata->verification_link;
                        endif;
                           
                            return redirect($url);
                    endif;
                    
                    if($userdata):
                    
                        Auth::login($userdata);
                        return redirect()->intended(route('dashboard'));

                    else:
                        $url = $this->GenerateVerificationlink(uniqid().'-'.$email);
                        $newuserdata = User::create([

                            'email' => $email,
                            'password' => Hash::make('AIGUNKNOWN'),
                            'isactive' => 0,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                            'user_stage_id' => 1,
                            'verification_link' => $url,
                            'verification_expiry_time' => Carbon::now()->addHours(24)->format('Y-m-d H:i:s')

                        ]);

                        if($newuserdata):

                            $params = array(
                                'url' => $url,
                                'subject' => 'Activate Account'
                            );

                           

                            $sib_contact_id=null;
                            $contactcreated = SendInBlueController::CreateContact($email);

                            if(isset($contactcreated['code']) && $contactcreated['code']==200):
                            
                                $sib_contact_id=$contactcreated['response']['id'];

                            endif;

                            if(isset($contactcreated['code']) && $contactcreated['code']!==200) :

                                if($contactcreated['response']['code']=='duplicate_parameter'):

                                $contactdetails=SendInBlueController::GetContactDetails($email);

                                    if(isset($contactdetails['code']) && $contactdetails['code']==200):
                                        $sib_contact_id=$contactdetails['response']['id'];
                                    endif;

                                endif;

                            endif;

                            
                            
                            $newuserdata->sib_contact_id = $sib_contact_id;
                            $newuserdata->save();

                            return redirect($url);

                        endif;    

                    endif;
                    
                    return redirect('/login')->with('alert','Oops! Invalid Credentials');

                endif;

        endif;

        

   }

    function GenerateVerificationlink($email)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_encrypt($email, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);

        $url = URL('verify-account') . "?key=" . $output;

        return $url;
    }
}