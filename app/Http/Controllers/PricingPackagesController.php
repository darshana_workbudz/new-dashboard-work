<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use SendinBlue;
use GuzzleHttp;
use App\Models\User;
use App\Models\CheckoutSessionData;
use App\Models\Packages;
use Carbon\Carbon;
use DB;
use Hash;
use Session;
use App\Http\Controllers\SendInBlueController;
use Auth;
use \Stripe\Stripe;


class PricingPackagesController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
        
    }



    public function ViewPricing(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        
        if ($isajax == 'true'):

            $html = view('new-pricing-packages', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('new-pricing-packages')]);

        else:

            return view('new-pricing-packages', $data);

        endif;
    }

    public function GetPurchaseSubscription(Request $request,$id)
	{

		$request->validate([
			'package_id' => 'required',
			'purchasesubscription'=>"required|in:freetrial,notrial"
		],
		[
			'purchasesubscription.in' => 'please select valid subscription to proceed'
		]);
		try {

			$isajax = $request->ajax;
			$isanually= $request->isanually;

			$data['isajax'] = $isajax;
			$packages=Packages::find($id);

			$istrial=0;
			if($request->purchasesubscription=='freetrial'):
				$istrial=1;
			endif;	

			if(isset($packages) && !empty($packages)):

				$userdata=Auth::user();

			$sessionarray=array();
			$sessionarray['line_items']=array();
			$sessionarray['success_url']=route('stripe-success-page').'?session_id={CHECKOUT_SESSION_ID}&package='.$packages->package_id.'&istrial='.$istrial;
			$sessionarray['cancel_url']=route('stripe-cancel-page').'?session_id={CHECKOUT_SESSION_ID}&package='.$packages->package_id.'&istrial='.$istrial;
			$sessionarray['payment_method_types']=['card'];

			if($packages->issubscription==1):
				$sessionarray['mode']='subscription';
			else:
				$sessionarray['mode']='payment';
			endif;
			
			$sessionarray['allow_promotion_codes']=true;
			$line_items=array();

			if($packages->issubscription==1 && $isanually=='1'):
				$line_items['price']=$packages->stripe_price_annual_id;
			elseif($packages->issubscription==1 && $isanually!=='1'):
				$line_items['price']=$packages->stripe_price_monthly_id;
			else:
				$line_items['price']=$packages->stripe_price_id;
			endif;

			
			$line_items['quantity']=1;

			if($userdata->user_role_type==3):

				$line_items['adjustable_quantity']=array(
					'enabled' => true,
					'minimum' => 1,
					'maximum' => 10,
				);

			endif;

			if($request->purchasesubscription=='freetrial'):
				$sessionarray['subscription_data']=array('trial_end' => strtotime("+15 day",strtotime(date('Y-m-d'))));
			endif;

			

			Stripe::setApiKey(env('API_KEY_STRIPE'));

			$customerId=$userdata->stripe_customer_id;

			if(empty($customerId) || is_null($customerId)):

					$customer = \Stripe\Customer::create([
						"description" => $userdata->first_name.' '.$userdata->last_name,
						"email" => $userdata->email,
						"metadata" => [
							"first_name" => $userdata->first_name,
							"last_name" => $userdata->last_name,
							"aig_user_id" => $userdata->id,
							"phone_number" => $userdata->phone_number,
						],
					]);

				$customerId=$customer->id;

				DB::beginTransaction();
				$userdata->stripe_customer_id=$customerId;
				$userdata->save();
				DB::commit();

			endif;
			$sessionarray['line_items'][]=$line_items;
			$sessionarray['customer']=$customerId;
			Stripe::setApiKey(env('API_KEY_STRIPE'));
			$checkout_session = \Stripe\Checkout\Session::create($sessionarray);

			if(null!==$checkout_session['id'] && !empty($checkout_session['id'])):



					$data['checkout_session']=$checkout_session['id'];
					$data['pkkey']=env('PK_KEY_STRIPE');

					DB::beginTransaction();

					CheckoutSessionData::create([
						'checkout_session_id'=>$checkout_session['id'],
				        'package_id'=>$id,
				        'payment_status'=>$checkout_session['payment_status'],
				        'subscription'=>$checkout_session['subscription'],
				        'stripe_customer_id'=>$customerId,
				        'user_id'=>$userdata->id,
				        'expires_at'=>$checkout_session['expires_at'],
				        'status'=>$checkout_session['status'],
				        'lineitems'=>json_encode($line_items),
				        'created_at'=>Carbon::now(),
				        'created_by'=>$userdata->id,
				        'updated_at'=>Carbon::now(),
				        'updated_by'=>$userdata->id
					]);

					DB::commit();

					return view('packages.stripe-checkout',$data);

			endif;

	endif;



	} catch (TrawableException $th) {
		DB::rollback();

	} catch (QueryException $qe) {
		DB::rollback();

	}

	return redirect()->route('subscription-packages');
	}
    


    

}