<?php
namespace App\Http\Controllers\MembersController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Coupons;
use App\Models\SharedCoupons;
use App\Models\SharedOrganisationPackages;
use App\Models\UserPackages;
use App\Models\Packages;
use App\Models\PromotionalCodes;
use App\Models\UserMembers;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Http\Controllers\SendInBlueController;
class MyAccountMemberController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
        
    }



    public function index(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        
        if ($isajax == 'true'):

            $html = view('our-members.our-account', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('member.our-account')]);

        else:

            return view('our-members.our-account', $data);

        endif;
    }

    public function OurBilling(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        
        if ($isajax == 'true'):

            $html = view('our-members.our-billing', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('member.billings')]);

        else:

            return view('our-members.our-billing', $data);

        endif;
    }
    
    public function OurOrganisationalCodes(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['promotionalcodes'] = SharedCoupons::GetActiveByUser($user->id);
        if ($isajax == 'true'):

            $html = view('our-members.our-coupon-codes', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('member.coupon-codes')]);

        else:

            return view('our-members.our-coupon-codes', $data);

        endif;
    }
    
    public function RequestPackages(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['prepurchasepackages'] = UserPackages::GetMembersAllActivePlan($user->id);
        $data['allonceoffplans'] = Packages::AllOnceoffPackages();
        $data['allstandaloneqrplans'] = Packages::AllStandAloneQrPackages();
        $data['allstandaloneheirloomplans'] = Packages::AllStandAloneHeirloomPackages();

        if ($isajax == 'true'):

            $html = view('our-members.request-packages', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('member.request-packages')]);

        else:

            return view('our-members.request-packages', $data);

        endif;
    }

    public function RequestPackagesPost(Request $request,$id)
    {
            $validator = Validator::make($request->all(), [
                'quantity'=>'required'
            ]);

            if ($validator->fails()) :

            return response()->json(['status' => 'fail']);

            endif;

            $packages=Packages::find($id);

            if(isset($packages) && !empty($packages)):

                   $params = array(
                    'quantity' => $request->quantity,
                    'packages' => $packages->name
                    
                    );

                $member=UserMembers::where('member_user_id',Auth::id())->first();
                $partner_id=isset($member) && !empty($member) ? $member->user_id : null;
                if(isset($partner_id) && !empty($partner_id)):

                    $userdata=User::find($partner_id);
                    SendInBlueController::SendTransactionEmail(44,$userdata->email,$params);

                endif;

                

            endif;

           
                
            

            $request = new \Illuminate\Http\Request();
            $request->ajax='true';
            $request->successmessage='Packages Requested Successfully';
            return $this->RequestPackages($request);




    }

    public function PrePurchasePackages(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['prepurchasepackages'] = UserPackages::GetMembersAllActivePlan($user->id);
        $data['prepurchaseheirloompackages'] = UserPackages::GetMembersHeirloomActivePlan($user->id);
        $data['prepurchaseqrpackages'] = UserPackages::GetMembersQrActivePlan($user->id);
        
        if ($isajax == 'true'):

            $html = view('our-members.our-pre-purchase-packages', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('member.pre-purchase-packages')]);

        else:

            return view('our-members.our-pre-purchase-packages', $data);

        endif;
    }


    public function ShowPackageDetails(Request $request)
    {
        try
        {   
         
            $validator = Validator::make($request->all(), [
                'user_package_id'=>'required'
            ]);

            if ($validator->fails()) :

                return response()->json(['status' => 'fail']);

            endif;

            $user=Auth::user();
            $members=UserMembers::GetAllMembers($user->id);
            $sharedpackages=SharedOrganisationPackages::GetActiveByUser($user->id,$request->user_package_id);
           
            $data['sharedpackages']=$sharedpackages;
            $data['members']=$members;
            $data['user_package_id']=$request->user_package_id;
            
            $html = view('our-members.share-package-to-member',$data)->render();

            return response()->json(['status' => 'success','html' => $html]);

        } catch (TrawableException $th) {
         
         return response()->json(['status' => 'fail']);

     } catch (QueryException $qe) {
        
         return response()->json(['status' => 'fail']);
     }        
    }


    public function ShowPackageDetailsPost(Request $request)
    {
         $validator = Validator::make($request->all(), [
                'user_package_id'=>'required',
                'members'=>'required'
            ]);

            if ($validator->fails()) :

            return response()->json(['status' => 'fail']);

            endif;
        try
        { 
            $user=Auth::user();
            $members=$request->members;
            $user_package_id=$request->user_package_id;
            if(isset($members) && !empty($members)):
                foreach($members as $datavalue):
                    $userdata=User::find($datavalue);
                    $checkexist=SharedOrganisationPackages::where('user_package_id',$user_package_id)->where('member_user_id',$datavalue)->first();

                    if(!$checkexist):

                        SharedOrganisationPackages::create([
                            'created_by'=>$user->id,
                            'updated_by'=>$user->id,
                            'updated_at'=>Carbon::now(),
                            'created_at'=>Carbon::now(),
                            'member_user_id'=>$datavalue,
                            'user_package_id'=>$user_package_id
                        ]);

                        $email=$userdata->email;
                         SendInBlueController::SendTransactionEmail(44,$email,$params);
                    endif;



                endforeach;

               
            endif;

            $request = new \Illuminate\Http\Request();
            $request->ajax='true';
            $request->successmessage='Packages Shared Successfully';
            return $this->PrePurchasePackages($request);
            

        } catch (TrawableException $th) {
           
           return response()->json(['status' => 'fail']);

        } catch (QueryException $qe) {
            
           return response()->json(['status' => 'fail']);
        }        
     
    }
    


    

}