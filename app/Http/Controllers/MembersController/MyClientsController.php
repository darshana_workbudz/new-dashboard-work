<?php
namespace App\Http\Controllers\membersController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Models\UserMembers;
use App\Models\UserClients;
use App\Models\User;
use App\Models\Coupons;
use App\Models\SharedCoupons;
use App\Models\PromotionalCodes;
use App\Models\ClientsDocuments;
use App\Models\EmailScheduler;
use App\Http\Controllers\SendInBlueController;
use Storage;
use File;

class MyClientsController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
        
    }



    public function index(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $data['clients'] = UserClients::GetActive($user->id);
        $data['checkinemail'] = EmailScheduler::GetLastCheckIn($user->id);
        $useridentity=$user->email;
        $useridentity=$this->Encrypt($useridentity);
        $invitation_link =  route('verify-registration-client').'?key='.$useridentity;
        $data['invitation_link']=$invitation_link;
        Session::put('invitation_link', $invitation_link);
        if ($isajax == 'true'):
            $html = view('my-clients.index', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('member.our-clients')]);

        else:

            return view('my-clients.index', $data);

        endif;
    }



    public function MyClientStatistics(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $id=$request->id;

        if(isset($id) && !empty($id)):
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $acknowledge = $request->acknowledge;
        $data['acknowledge'] = $acknowledge;
        $clientdata = UserClients::GetClientStatistics($user->id,$id);
        $data['clientdata']=$clientdata;
        $data['clientdocuments']=ClientsDocuments::where('user_id',$id)
        ->where('capture_type','uploaded')
        ->get();

        if ($isajax == 'true'):

            $html = view('my-clients.details', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('member-clients.statistics')."?id=".$id."&acknowledge=1"]);

        else:

            return view('my-clients.details', $data);

        endif;


        if(isset($clientdata) && !empty($clientdata)):

            return view('my-clients.details', $data);

        endif;

        endif;

        abort(401);
        
        
       
    }


    public function MyClientUpdateStatus(Request $request,$id)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        if(isset($id) && !empty($id)):
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $acknowledge = $request->acknowledge;
        $passed_date = $request->passed_date;
        $data['acknowledge'] = $acknowledge;
        $clientdata = UserClients::GetClientStatistics($user->id,$id);
        $data['clientdata']=$clientdata;
        if(isset($clientdata) && !empty($clientdata)):

            $userdata=User::find($clientdata->user_id);
            $userdata->passed_date=date('Y-m-d',strtotime($passed_date));
            $userdata->save();


            $request = new \Illuminate\Http\Request();
            $request->ajax='true';
            $request->successmessage='Client Status Updates Successfully';
            return $this->index($request);

        endif;


        endif;

        $request = new \Illuminate\Http\Request();
        $request->ajax='true';
        $request->failedmessage='Oops! Client Not found Please Try again';
        return $this->index($request);
        
        
       
    }

    public function ViewClients(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        
        if ($isajax == 'true'):

            $html = view('my-clients.client-statistics', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-clients.client-statistics')]);

        else:

            return view('my-clients.client-statistics', $data);

        endif;
    }

    
    public function ViewProfile(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        
        if ($isajax == 'true'):

            $html = view('my-profile-member', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-profile-member')]);

        else:

            return view('my-profile-member', $data);

        endif;
    }
    


    public function ViewAccount(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        
        if ($isajax == 'true'):

            $html = view('my-account-member', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-account-member')]);

        else:

            return view('my-account-member', $data);

        endif;
    }

    public function ShareCouponsClientsPost(Request $request)
    {
          
            
            $validator = Validator::make($request->all(), [
                'user_id'=>'required',
                'promotionalcodes'=>'required'
            ]);

            if ($validator->fails()) :

            return response()->json(['status' => 'fail']);

            endif;
        try
        { 
            $user=Auth::user();
            $promotionalcodes=$request->promotionalcodes;
            $user_id=$request->user_id;
            $userdata=User::find($user_id);
            if(isset($promotionalcodes) && !empty($promotionalcodes)):
                foreach($promotionalcodes as $datavalue):
                    $partner=UserClients::FindById($user_id);
                    
                    if(isset($partner) && !empty($partner)):

                        $partnercoupons=SharedCoupons::GetActive($partner->user_id);

                    endif;

                    if(in_array($datavalue,$partnercoupons)):
                    
                        $checkexist=SharedCoupons::where('user_id',$user_id)->where('promotional_code_id',$datavalue)->first();

                        if(!$checkexist):
                            SharedCoupons::create([
                                'created_by'=>$user->id,
                                'updated_by'=>$user->id,
                                'updated_at'=>Carbon::now(),
                                'created_at'=>Carbon::now(),
                                'promotional_code_id'=>$datavalue,
                                'user_id'=>$user_id
                            ]);
                        endif;

                    endif;

                endforeach;
                $codes=PromotionalCodes::whereIn('promotional_code_id',$promotionalcodes)->select('code')->get()->toArray();
                if(isset($codes) && !empty($codes)):

                    $params = array(
                    'codes' => $codes,
                    'subject' => 'Special off%: find your Discount Codes inside'
                    );
                
                $email=$userdata->email;
                 SendInBlueController::SendTransactionEmail(44,$email,$params);
                endif;
            endif;

            $request = new \Illuminate\Http\Request();
            $request->ajax='true';
            $request->successmessage='Coupon Code Shared Successfully';
            return $this->index($request);
            

        } catch (TrawableException $th) {
           
           return response()->json(['status' => 'fail']);

        } catch (QueryException $qe) {
            
           return response()->json(['status' => 'fail']);
        }        
    }


     function Encrypt($text)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_encrypt($text, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }


    function Decrypt($text)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_decrypt(base64_decode($text), $encrypt_method, $key, 0, $iv);
        return $output;
    }


    public function MyClientUploadFiles(Request $request,$id)
    {

        try
        {   
            $user=Auth::user();

            $validator = Validator::make($request->all(), [

            'file' => 'required',
            'type' => 'required'
            
            ]);


            if ($request->hasFile('file')) {

                $document = $request->file('file');
                $fileInfo = $document->getClientOriginalName();
                $filename = pathinfo($fileInfo, PATHINFO_FILENAME);
                $extension = pathinfo($fileInfo, PATHINFO_EXTENSION);
                $file_name= $filename.'-'.time().'.'.$extension;
                Storage::disk('public')->put($file_name,File::get($document));


                if(Storage::disk('public')->exists($file_name)) {  
                    $path = Storage::disk('public')->path($file_name);

                    $allowedMimeTypes = ['image/jpeg','image/gif','image/png','image/jpg'];
                    $contentType = $document->getClientMimeType();

                    if(! in_array($contentType, $allowedMimeTypes) ){
                      
                      $filetype='pdf';
                    }
                    else
                    {
                      $filetype='image';
                    }

                    $updated=ClientsDocuments::create([

                        'title'=>$filename,
                        'user_id'=>$id,
                        'capture_type'=>'uploaded',
                        'path'=>$path,
                        'uploadedfilename'=>$file_name,
                        'created_at'=>Carbon::now(),
                        'created_by'=>$user->id,
                        'updated_at'=>Carbon::now(),
                        'updated_by'=>$user->id,
                        'filetype'=>$filetype
                    ]);

                    return array('status'=>'success','data'=>$updated);
                    

                
                            
                }

            }


             


        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

    }

    public function MyClientDeleteFiles(Request $request,$id)
    {
        $user=Auth::user();
        $filename =  $request->get('filename');
        
        $pdffiles = ClientsDocuments::where('user_id',$id)
        ->where('uploadedfilename',$filename)
        ->where('capture_type','uploaded')
        ->first();

        if(isset($pdffiles) && !empty($pdffiles)):

           $pdffiles->delete();
           unlink($pdffiles->path);

        endif;

        $request = new \Illuminate\Http\Request();
        $request->ajax='true';
        $request->id=$id;
        $request->successmessage='Document Deleted Successfully';
        return $this->MyClientStatistics($request);
    } 

    public function MyClientStreamFiles(Request $request,$id,$filename)
    {
        $document = ClientsDocuments::where('user_id',$id)
        ->where('uploadedfilename',$filename)
        ->where('capture_type','uploaded')
        ->first();
        $data['textdata'] = $document;
        return response()->file($document->path);


       
    }

}