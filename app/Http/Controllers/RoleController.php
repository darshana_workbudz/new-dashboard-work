<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use App\Models\Modules_model;
use DB;
use Session;
use Validator;


class RoleController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
    	
    }


    
    public function index(Request $request)
    {
        
        $isajax = $request->ajax;
        $data['isajax'] = $isajax;
        $name = $request->name;
        $query = Role::select('id','name','guard_name');

        if(isset($name) && !empty($name)) {
            $query->where('name','LIKE','%'.$name.'%'); 
        }

        $data['roles'] = $query->orderBy('id','asc')->get();
        $data['old'] = $request;

        if ($isajax == 'true'):

            $html = view('roles.index', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);

        else:

            return view('roles.index', $data);

        endif;
    }


   public function edit(Request $request,$id)
    {   
        $role = Role::findOrFail($id);

        if(isset($role) && !empty($role))
        {
            $permissions = Permission::all();
            $permission_array=array();
            $Modules = Modules_model::Orderby('serialorder','asc')->get();
            
            $isajax = $request->ajax;
            $data['isajax'] = $isajax;
            $data['old'] = $request;
            $data['role'] = $role;
            $data['rolePermissions'] = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
                ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
                ->all();

            if(isset($Modules) && !empty($Modules)):
                foreach ($Modules as $mkey => $mvalue):
                   
                    $mvalue->allpermission=Permission::where('module_id',$mvalue->module_id)
                    ->get()
                    ->toArray();

                endforeach;
            endif;
            
            $data['Modules'] = $Modules;
            
            if ($isajax == 'true'):

                $html = view('roles.edit',$data)->render();

                return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);

            else:

                return view('roles.edit',$data);

            endif;
            
        }



    }


    
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'role_id' => 'required',
          
        ]);

        $id=$request->input('role_id');

        if ($validator->fails()) :
            
            $request->error = $validator->getMessageBag()->toArray();
            $request->failuremessage='Validation Issue! Please Check All Required Fields';
            return $this->edit($request,$id);

        endif;

        
        $role = Role::find($id);
        

        if($role->save())
        {
            $role->syncPermissions($request->input('permissions'));

            $request->successmessage='Roles Updated Successfully';
            

            return $this->index($request,$id);

        } 
        

        
    }
    
    
}
