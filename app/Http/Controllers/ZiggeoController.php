<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp;
use DB;
use Hash;
use Session;
use Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\ZiggeoUserVideos;
use App\Models\ZiggeoUserAudios;
use Ziggeo;

class ZiggeoController extends Controller
{


	public static function GenerateAuthtoken($user)
	{	
		$token=null;
		$arguments = array();

			$getvideotokens=ZiggeoUserVideos::GetVideoByUserIdPluck($user->id)->toArray();
			$getaudiotokens=ZiggeoUserAudios::GetAudioByUserIdPluck($user->id)->toArray();
			$alltokens=array_merge($getvideotokens,$getaudiotokens);
			$arguments=array(
				"grants" => array(
					"create" => array(
						"session_owned" => TRUE
					),
					"update" => array(
						"resources" =>$alltokens
					),
					"destroy" => array(
						"resources" =>$alltokens
					),
					"read" => array(
						"resources" =>$alltokens
					)

				),
			    "session_limit" => 1,
			    "usage_expiration_time"=>86400

			);

			
			$user->ziggeo_access_rights=json_encode($arguments);
			

		

		$ziggeo = new Ziggeo(env('ZIGGEOTOKEN'),env('ZIGGEOPRIVATEKEY'));
		$tokencreated = $ziggeo->authtokens()->create($arguments);
		
		if(isset($tokencreated) && !empty($tokencreated)):
			$token=$tokencreated['token'];
			$user->ziggeo_access_token==$tokencreated['token'];
			$user->ziggeo_access_token_created=$tokencreated['created'];
			$user->ziggeo_access_token_expiry=$tokencreated['created']+86400;
			$user->save();
		endif;

		

		return $token;
		
	}



	


	public function DeleteAuthtoken($user)
	{

		
		
		
	}


	///


}