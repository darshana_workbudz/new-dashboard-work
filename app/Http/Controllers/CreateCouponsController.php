<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Packages;
use App\Models\MyRecipients;
use App\Http\Controllers\SendInBlueController;
use App\Models\VerficationQuestions;
use App\Models\UserAuthorizedPerson;
use App\Models\VerificationProcessStep;

class CreateCouponsController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
    	
    }


    
    public function index(Request $request)
    {
        $user=Auth::user();
        $authid=User::GetAuthID($user);
        $isajax=$request->ajax;
		$data['isajax'] = $isajax;
        $data['old'] = $request;

        if ($isajax == 'true'):

            $html = view('coupons.index', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('manage-coupons.index')]);

        else:

            return view('coupons.index', $data);

        endif;
    }


    public function create(Request $request)
    {   
        $user=Auth::user();
        $authid=User::GetAuthID($user);
        $isajax=$request->ajax;

        $data['isajax'] = $isajax;
        $data['old'] = $request; 
           
        if ($isajax == 'true'):

            $html = view('coupons.create',$data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('manage-coupons.create')]);

        else:

            return view('coupons.create',$data);

        endif;
            
    }


    public function store(Request $request)
    {
        try
        {


        $validator = Validator::make($request->all(), [

            'email' => 'required|email|unique:user_recipients',
            'user_id' => 'required',
            'first_name' => ['required', 'alpha','min:2', 'max:255'],
            'last_name' => ['required', 'alpha','min:2', 'max:255'],
            'phone_code' => ['nullable'],
            'phone_number' => ['nullable','min:10','max:10'],
            'address_1' => ['nullable','min:10', 'max:255'],
            'postcode' => ['nullable','min:4', 'max:255'],
            'country' => ['nullable','string','min:2','max:255'],
            'require_verification' => ['required_if:recieve_message_by,==,1','nullable','in:1,2'],
            'password' => ['required_if:verification_process_step_id,==,5','nullable','string','min:6', 'confirmed'],
            'verification_question_1' => ['required_if:verification_process_step_id,==,6','nullable'],
            'verification_question_2' => ['required_if:verification_process_step_id,==,6','nullable'],
            'verification_question_3' => ['required_if:verification_process_step_id,==,6','nullable'],
            'verification_answer_1' => ['required_if:verification_process_step_id,==,6','nullable','string','min:2', 'max:255'],
            'verification_answer_2' =>['required_if:verification_process_step_id,==,6','nullable','string','min:2', 'max:255'],
            'verification_answer_3' => ['required_if:verification_process_step_id,==,6','nullable','string','min:2', 'max:255'],
            'accept_recipient_responsibility' => ['required_if:require_verification,==,2','in:1']
        ],
        [
            'require_verification.required_if'=>'Please Select verification process type for recipient',
            'password.required_if'=>'The password Field is required as for recipient verification',
            'password_confirmation.required_if'=>'The confirm password Field is required as for recipient verification',
            'verification_question_1.required_if'=>'This Field is required as for recipient verification',
            'verification_question_2.required_if'=>'This Field is required as for recipient verification',
            'verification_question_3.required_if'=>'This Field is required as for recipient verification',
            'verification_answer_1.required_if'=>'This Field is required as for recipient verification',
            'verification_answer_2.required_if'=>'This Field is required as for recipient verification',
            'verification_answer_3.required_if'=>'This Field is required as for recipient verification',
            

        ]
    );

        
        $user_id=$request->input('user_id');
        $user=Auth::user();
        
        if(!User::CheckEligibleUserId($user,$user_id)):

            $request->failuremessage="Oops! you don't Have Right access to Create Recipients";

            return $this->create($request);

        endif;

        if ($validator->fails()) :
            
            $request->error = $validator->getMessageBag()->toArray();
            
            return $this->create($request);

        endif;

        $email=$request->email; 

        $usedrecipients=User::GetUsedRecipientsLimit($user->id);
        $totalrecipients=User::GetTotalRecipients($user->id);

        $hasrecipients=($totalrecipients-$usedrecipients);

        
        
        // echo $usedrecipients.'<br>';
        // echo $totalrecipients.'<br>';
        // dd($usedrecipients < $totalrecipients);
         if($hasrecipients==0):
        // if(!($usedrecipients < $totalrecipients)):

            $request->failuremessage="Sorry Recipients limit exceeded !";

            return $this->create($request);

        endif;


        DB::beginTransaction();

        $userarray=[

            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'phone_code'=>$request->phone_code,
            'phone_number'=>$request->phone_number,
            'email'=>$request->email,
            'address_1'=>$request->address_line_1,
            'address_2'=>isset($request->address_line_2) && !empty($request->address_line_2) ? $request->address_line_2 :null,
            'address_3'=>isset($request->address_line_3) && !empty($request->address_line_3) ? $request->address_line_3 :null,
            'postcode'=>$request->postcode,
            'relationship_id'=>$request->relationship,
            'other'=>$request->other_relationship,
            'country'=>$request->country,
            'state'=>$request->state,
            'user_id'=>$user->id,
            'created_by'=>$user->id,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
            'updated_by'=>$user->id,
            'status'=>1,
            'require_verification'=>isset($request->require_verification) ? $request->require_verification :0,
            'verification_process_step_id'=>isset($request->verification_process_step_id) ? $request->verification_process_step_id :0,
             'accept_recipient_responsibility'=>isset($request->require_verification) && $request->require_verification==2 ?  $request->accept_recipient_responsibility : 0,
             'password'=>isset($request->password) && !empty($request->password) ?  ($request->password) : null,
             'verification_question_1'=>isset($request->verification_process_step_id) && !empty($request->verification_question_1) ?  $request->verification_question_1 : null, 
             'verification_question_2'=>isset($request->verification_process_step_id) && !empty($request->verification_question_2) ?  $request->verification_question_2 : null, 
             'verification_question_3'=>isset($request->verification_process_step_id) && !empty($request->verification_question_3) ?  $request->verification_question_3 : null,
             'verification_answer_1'=>isset($request->verification_process_step_id) && !empty($request->verification_answer_1) ?  $request->verification_answer_1 : null, 
             'verification_answer_2'=>isset($request->verification_process_step_id) && !empty($request->verification_answer_2) ?  $request->verification_answer_2 : null,
             'verification_answer_3'=>isset($request->verification_process_step_id) && !empty($request->verification_answer_3) ?  $request->verification_answer_3 : null,
             

        ];

        $usercreated=MyRecipients::create($userarray);


        if($usercreated):

                $sib_contact_id=null;
                $contactcreated = SendInBlueController::CreateContact($email);

                if(isset($contactcreated['code']) && $contactcreated['code']==200):
                
                    $sib_contact_id=$contactcreated['response']['id'];

                endif;

                if(isset($contactcreated['code']) && $contactcreated['code']!==200) :

                    if($contactcreated['response']['code']=='duplicate_parameter'):

                    $contactdetails=SendInBlueController::GetContactDetails($email);

                        if(isset($contactdetails['code']) && $contactdetails['code']==200):
                            $sib_contact_id=$contactdetails['response']['id'];
                        endif;

                    endif;

                endif;

                $usercreated->sib_contact_id = $sib_contact_id;
                $usercreated->save();

        endif;

       

        DB::commit();

        $request = new \Illuminate\Http\Request();
        $request->ajax='true';
        $request->successmessage='New Recipient Added Successfully';
        return $this->index($request);

        
        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

        $request->failuremessage='Oops! Something went wrong please try again';
        return $this->create($request);
        
        
    }


    
    
}
