<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use Log;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Models\MyRecipients;
use App\Models\FollowupPeriod;
use App\Models\FailedResponseAction;
use App\Models\VerificationProcessStep;
use App\Models\VerficationQuestions;
use App\Models\WatingPeriod;
use App\Models\EmailScheduler;
use App\Models\CheckInSetting;
use App\Http\Controllers\SendInBlueController;
use App\Models\UserAuthorizedPerson;
use App\Models\ZiggeoUserAudios;
use App\Models\ZiggeoUserTexts;
use App\Models\ZiggeoUserVideos;
use App\Models\MessageScheduler;
use App\Models\HasAccessLogin;
use App\Models\ClientsDocuments;
use Storage;
use File;

class MyPreferenceController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
        
    }

    public function index(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['state'] = '1';
        $data['user_id'] = $user->id;
        $data['followup_period'] = FollowupPeriod::all();
        $data['failed_response_action'] = FailedResponseAction::all();
        $data['verification_process_step'] = VerificationProcessStep::where('recipient',0)->get();
        $data['verification_questions'] = VerficationQuestions::all();
        $data['wating_period'] = WatingPeriod::all();
        $data['checkinsetting']=CheckInSetting::FindByUserId($user->id);
        $data['error'] = $request->error;
        $data['authoriseddata'] = UserAuthorizedPerson::FindByUser($user->id);
        $data['default_prefrence'] = $user->default_prefrence;
        if ($isajax == 'true'):

            $html = view('my-preferences.index', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-preferences.index')]);

        else:

            return view('my-preferences.index', $data);

        endif;

       
    }

    public function CheckInProcess(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['state'] = '1';
        $data['user_id'] = $user->id;
        $data['followup_period'] = FollowupPeriod::all();
        $data['failed_response_action'] = FailedResponseAction::all();
        $data['verification_process_step'] = VerificationProcessStep::where('recipient',0)->get();
        $data['verification_questions'] = VerficationQuestions::all();
        $data['wating_period'] = WatingPeriod::all();
        $data['checkinsetting']=CheckInSetting::FindByUserId($user->id);
        $data['error'] = $request->error;
        $data['authoriseddata'] = UserAuthorizedPerson::FindByUser($user->id);
        $data['default_prefrence'] = $user->default_prefrence;
        if ($isajax == 'true'):

            $html = view('my-preferences.chekinprocess', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-preferences.index')]);

        else:

            return view('my-preferences.chekinprocess', $data);

        endif;

       
    }

    


    public function Authorised3pProcess(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['state'] = '2';
        $data['user'] = $user;
        $data['error'] = $request->error;
        $data['default_prefrence'] = $user->default_prefrence;

        $data['authoriseddata'] = UserAuthorizedPerson::FindByUser($user->id);
        $data['verification_process_step'] = VerificationProcessStep::where('recipient',0)->get();
        $data['verification_questions'] = VerficationQuestions::all();
        $data['relationship'] = DB::table('relationship')->orderBy('relationship_name','asc')->get();
        $data['phonecodes'] = DB::table('tbl_country')->orderBy('name','asc')->get();

        if ($isajax == 'true'):

            $html = view('my-preferences.authorised3partyaccess', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('my-authorised-3p-access')]);

        else:

            return view('my-preferences.authorised3partyaccess', $data);

        endif;

       
    }


    public function SaveAuthorisedAccess(Request $request)
    {

    try
        {
        $validator = Validator::make($request->all(), [
            'nominated_authorised_person_access'=>'required|in:1,2',
            'islawyer' => ['required_if:nominated_authorised_person_access,==,1','nullable','in:1,2'],
            'organisation'=>['nullable','min:2','max:255'],

            'email' => ['required_if:nominated_authorised_person_access,==,1','nullable','email'],
            'first_name' => ['required_if:nominated_authorised_person_access,==,1', 'alpha','min:2', 'max:255','nullable'],
            'last_name' => ['required_if:nominated_authorised_person_access,==,1', 'alpha','min:2', 'max:255','nullable'],
            'phone_code' => ['required_if:nominated_authorised_person_access,==,1','nullable'],
            'phone_number' => ['required_if:nominated_authorised_person_access,==,1','nullable','min:10','max:10'],
            'address_line_1' => ['required_if:nominated_authorised_person_access,==,1','nullable','min:10', 'max:255'],
            'postcode' => ['required_if:nominated_authorised_person_access,==,1','nullable','min:4', 'max:255'],
            'country' => ['required_if:nominated_authorised_person_access,==,1','nullable','string','min:2','max:255'],
            'state' => ['required_if:nominated_authorised_person_access,==,1','nullable','string','min:2','max:255']
            
        ],
        [
            'nominated_authorised_person_access.required'=>'Please Select Yes/No from above Options',
            'email.required_if' => 'The authorised person email field is required',
            'first_name.required_if' =>'The authorised person first name field is required',
            'last_name.required_if' =>'The authorised person last name field is required',
            'phone_code.required_if' => 'The authorised person phone code field is required',
            'phone_number.required_if' => 'The phone number of authorised person field is required',
            'address_line_1.required_if' => 'The address of authorised person  field is required',
            'postcode.required_if' => 'The postcode field is required',
            'country.required_if' => 'The country field is required',
            'state.required_if' => 'The state field is required',
            'islawyer.required_if' => 'This field is required'
            
            
            
        ]);

        $user_id=$request->input('user_id');
        $user=Auth::user();

        if(!User::CheckEligibleUserId($user,$user_id)):

            $request->failuremessage="Oops! you don't Have Right access to Create Recipients";

            return $this->Authorised3pProcess($request);

        endif;

        if ($validator->fails()) :
            
            $request->error = $validator->getMessageBag()->toArray();
            $request->showdiv=1;
            return $this->Authorised3pProcess($request);

        endif;

        $email=$request->email; 
        DB::beginTransaction();

        $userarray=[

            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'phone_code'=>$request->phone_code,
            'phone_number'=>$request->phone_number,
            'email'=>$request->email,
            'address_1'=>$request->address_line_1,
            'address_2'=>isset($request->address_line_2) && !empty($request->address_line_2) ? $request->address_line_2 :null,
            'address_3'=>isset($request->address_line_3) && !empty($request->address_line_3) ? $request->address_line_3 :null,
            'postcode'=>$request->postcode,
            'relationship_id'=>$request->relationship,
            'other'=>$request->other_relationship,
            'country'=>$request->country,
            'state'=>$request->state,
            'updated_at'=>Carbon::now(),
            'updated_by'=>$user->id,
            'islawyer'=>isset($request->islawyer) ? $request->islawyer : null,
            'other_relationship'=>isset($request->other_relationship) ? $request->other_relationship :null,
            'organisation'=>isset($request->organisation) ? $request->organisation :null,
            'organisation_address'=>isset($request->organisation_address) ? $request->organisation_address :null,
            'organisation_phonecode'=>isset($request->organisation_phonecode) ? $request->organisation_phonecode :null,
            'organisation_phonenumber'=>isset($request->organisation_phonenumber) ? $request->organisation_phonenumber :null,
           'step1confirmed'=>1,
           'status'=>0

             

        ];




        

        $authorisedperson=UserAuthorizedPerson::FindByUser($user->id);
        $nominated_authorised_person_access=$request->nominated_authorised_person_access;
        if($nominated_authorised_person_access==1):

        if(isset($authorisedperson) && !empty($authorisedperson)):

            if($authorisedperson->email!==$request->email):

                $sib_contact_id=null;
                $contactcreated = SendInBlueController::CreateContact($email);

                if(isset($contactcreated['code']) && $contactcreated['code']==200):
                
                    $sib_contact_id=$contactcreated['response']['id'];

                endif;

                if(isset($contactcreated['code']) && $contactcreated['code']!==200) :

                    if($contactcreated['response']['code']=='duplicate_parameter'):

                    $contactdetails=SendInBlueController::GetContactDetails($email);

                        if(isset($contactdetails['code']) && $contactdetails['code']==200):
                            $sib_contact_id=$contactdetails['response']['id'];
                        endif;

                    endif;

                endif;
                $checksum=$request->email.'--'.'3p-auth'.'--'.rand();
                $invitation_link=$this->GenerateInvitationlink($checksum,$user->id);
                $userarray['invitation_link']=$invitation_link;
                // $userarray['invitation_status']=0;
                $userarray['sib_contact_id']=$sib_contact_id;
                $userarray['step2confirmed']=0;
                $userarray['step2confirmed']=0;
                // $userarray['status']=0;
                
                // $params = array(
                //     'url' => $invitation_link,
                //     'subject' => 'Activate Account'
                // );

                // SendInBlueController::SendTransactionEmail(20,$request->email,$params);



            endif;

            $authorisedperson->update($userarray);


            


        else:

            $checksum=$request->email.'--'.'3p-auth'.'--'.rand();
            $invitation_link=$this->GenerateInvitationlink($checksum,$user->id);
            $userarray['invitation_link']=$invitation_link;
            $userarray['invitation_status']=0;
            $userarray['status']=1;
            $userarray['user_id']=$user->id;
            $userarray['created_by']=$user->id;
            $userarray['created_at']=Carbon::now();

            $usercreated=UserAuthorizedPerson::create($userarray);


            if($usercreated):

                 HasAccessLogin::create([

                    'email' => 'trustedparty-'.$user->email,
                    'password' => Hash::make('evalheld'),
                    'invitation_status' => 0,
                    'main_user_id'=>$user->id,
                    'user_authorised_person_id'=>$usercreated->user_authorised_person_id,
                    'created_by'=>$user->id,
                    'created_by'=>Carbon::now(),
                    'isactive'=>0
                
                ]);

                // $params = array(
                //     'url' => $invitation_link,
                //     'subject' => 'Activate Account'
                // );

                // SendInBlueController::SendTransactionEmail(20,$request->email,$params);

                $sib_contact_id=null;
                $contactcreated = SendInBlueController::CreateContact($email);

                if(isset($contactcreated['code']) && $contactcreated['code']==200):
                
                    $sib_contact_id=$contactcreated['response']['id'];

                endif;

                if(isset($contactcreated['code']) && $contactcreated['code']!==200) :

                    if($contactcreated['response']['code']=='duplicate_parameter'):

                    $contactdetails=SendInBlueController::GetContactDetails($email);

                        if(isset($contactdetails['code']) && $contactdetails['code']==200):
                            $sib_contact_id=$contactdetails['response']['id'];
                        endif;

                    endif;

                endif;

                $usercreated->sib_contact_id = $sib_contact_id;
                $usercreated->save();

            endif;

        endif;
        endif;

        $user->default_prefrence=isset($request->nominated_authorised_person_access) && $request->nominated_authorised_person_access==1 ? 2 : 1;
        $user->nominated_authorised_person_access=$request->nominated_authorised_person_access;
        $user->save();
       
            
        DB::commit();

        $request = new \Illuminate\Http\Request();
        $request->ajax='true';
        $request->successmessage='Trusted Party saved successfully';
        return $this->Authorised3pProcess($request);

        
        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

        $request->failuremessage='Oops! Something went wrong please try again';
        return $this->Authorised3pProcess($request);

        
    }

    public function UpdateAuthorisedAccess(Request $request)
    {

    try
        {
        $validator = Validator::make($request->all(), [
            'giveaccountaccess'=>'required|in:1,2',
            'authorised_access_type' => ['required_if:giveaccountaccess,==,1'],
            'aigcanemail' => ['required_if:giveaccountaccess,==,1'],
            'verification_process_step_id'=>'required|in:1,2,3,4',
            'password' => ['required_if:verification_process_step_id,==,1','nullable','string', 'min:6',
            'regex:/[a-z]/',      // must contain at least one lowercase letter
            'regex:/[A-Z]/',      // must contain at least one uppercase letter
            'regex:/[0-9]/',      // must contain at least one digit
            'regex:/[@$!%*#?&]/', // must contain at least one Charater
            'confirmed'],
            'verification_question_1' => ['required_if:verification_process_step_id,==,2','nullable'],
            'verification_question_2' => ['required_if:verification_process_step_id,==,2','nullable'],
            'verification_question_3' => ['required_if:verification_process_step_id,==,2','nullable'],
            'verification_answer_1' => ['required_if:verification_process_step_id,==,2','nullable','string','min:2', 'max:255'],
            'verification_answer_2' =>['required_if:verification_process_step_id,==,2','nullable','string','min:2', 'max:255'],
            'verification_answer_3' => ['required_if:verification_process_step_id,==,2','nullable','string','min:2', 'max:255'],
            'verification_to_releasemessage' => ['required_if:verification_process_step_id,1,2,3'],

        ],
        [
            'giveaccountaccess.required'=>'Please Select Yes/No from above Options',
            'authorised_access_type.required_if' => 'Please select one above option for verfication process',
            'aigcanemail.required_if' => 'This field is require for verification process',
            'verification_process_step_id.required' => 'This field is require for to process there notice advised',
            'password.required_if'=>'The password field is required as for recipient verification',
            'password_confirmation.required_if'=>'The confirm password field is required as for recipient verification',
            'verification_question_1.required_if'=>'This field is required as for authorised person verification',
            'verification_question_2.required_if'=>'This field is required as for authorised person verification',
            'verification_question_3.required_if'=>'This field is required as for authorised person verification',
            'verification_answer_1.required_if'=>'This field is required as for authorised person verification',
            'verification_answer_2.required_if'=>'This field is required as for authorised person verification',
            'verification_answer_3.required_if'=>'This field is required as for authorised person verification',
            'verification_to_releasemessage.required_if'=>'This field is required to release message',
            
            
            
        ]);

        $user_id=$request->input('user_id');
        $user=Auth::user();

        if(!User::CheckEligibleUserId($user,$user_id)):

            $request->failuremessage="Oops! you don't Have Right access to Create Authperson";

            return $this->Authorised3pProcess($request);

        endif;

        if ($validator->fails()) :
            
            $request->error = $validator->getMessageBag()->toArray();
            $request->showdiv=2;
            return $this->Authorised3pProcess($request);

        endif;

        $email=$request->email; 
        DB::beginTransaction();

        $userarray=[

            
            'giveaccountaccess'=>isset($request->giveaccountaccess) ? $request->giveaccountaccess :null,
            'authorised_access_type'=>isset($request->authorised_access_type) && $request->giveaccountaccess==1 ? $request->authorised_access_type :null,
            'aigcanemail'=>isset($request->aigcanemail) && $request->giveaccountaccess==1 ? $request->aigcanemail :null,
            'verification_process_step_id'=>isset($request->verification_process_step_id) ? $request->verification_process_step_id :null,
             
             'password'=>isset($request->password) && !empty($request->password) ?  ($request->password) : null,
             'verification_question_1'=>isset($request->verification_question_1) && !empty($request->verification_question_1) ?  $request->verification_question_1 : null, 
             'verification_question_2'=>isset($request->verification_question_2) && !empty($request->verification_question_2) ?  $request->verification_question_2 : null, 
             'verification_question_3'=>isset($request->verification_question_3) && !empty($request->verification_question_3) ?  $request->verification_question_3 : null,
             'verification_answer_1'=>isset($request->verification_answer_1) && !empty($request->verification_answer_1) ?  $request->verification_answer_1 : null, 
             'verification_answer_2'=>isset($request->verification_answer_2) && !empty($request->verification_answer_2) ?  $request->verification_answer_2 : null,
             'verification_answer_3'=>isset($request->verification_answer_3) && !empty($request->verification_answer_3) ?  $request->verification_answer_3 : null,
            'verification_to_releasemessage'=>isset($request->verification_to_releasemessage) ? $request->verification_to_releasemessage :null,
            'updated_at'=>Carbon::now(),
            'updated_by'=>$user->id,
            'step2confirmed'=>1
           

             

        ];
        
       
        $authorisedperson=UserAuthorizedPerson::FindByUser($user->id);

        if(isset($authorisedperson) && !empty($authorisedperson)):
            $step2confirmed=$authorisedperson->step2confirmed;
            $invitationstatus=$authorisedperson->invitation_status;
            

            

            $updated=$authorisedperson->update($userarray);

            if($updated && $request->aigcanemail==1 && $invitationstatus!==1)
            {

                $params = array(
                    'url' => $authorisedperson->invitation_link,
                    'subject' => 'Third Person Invitation link',
                    'FIRSTNAME' => $user->first_name,
                    'LASTNAME' => $user->last_name
                );
                
                SendInBlueController::SendTransactionEmail((int) env('THIRD_PERSON_INVITE_TEMPLATE'),$authorisedperson->email,$params);
            }
            endif;

            if($updated && $request->giveaccountaccess==1 && $invitationstatus==1):

                $accessupdate['isactive']=1;

                else:

                $accessupdate['isactive']=0;

            endif;    
                
            $Loginaccess=HasAccessLogin::where('main_user_id',$user->id);
            if(isset($Loginaccess) && !empty($Loginaccess)):
                $Loginaccess->update($accessupdate);
            endif;
            
                 
        

        
        DB::commit();

        $request = new \Illuminate\Http\Request();
        $request->ajax='true';
        $request->successmessage="Your Trusted Party’s access preferences have been saved successfully";
        return $this->Authorised3pProcess($request);

        
        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

        $request->failuremessage='Oops! Something went wrong please try again';
        return $this->Authorised3pProcess($request);

        
    }


    public function SaveChekinProcess(Request $request)
    {


        $validator = Validator::make($request->all(), [

            'followup_period_id' => 'required',
            'responsibility_acknowledge' =>'required|in:1',
            'failed_response_action_id' => 'required|in:1,2,3,4',
            // 'wating_period_no_of_days' =>'required|in:1,2,3,4,5',
            'release_message_acknowledge' =>'required|in:1',
            'final_acknowledge'=>['required'],
            

          
        ],

        [
            'followup_period_id.required'=>'please select the above field so we can contact you',
            'responsibility_acknowledge.required'=>'please Acknowledge above responsibilty so we can proceed ahead',
            'failed_response_action_id.required'=>'please Select above options so we can take an action according to that !',
            // 'wating_period_no_of_days.required'=>'please select above options so we can release message according to this',
            'release_message_acknowledge.required'=>'please Acknowledge above responsibilty so we can proceed ahead',
             'final_acknowledge.required'=>'please acknowledge above responsibilty so we can proceed ahead'
            
        ]);

        if ($validator->fails()):
            
            $request->error = $validator->getMessageBag()->toArray();
            return $this->CheckInProcess($request);

        endif;
        
        try
        {

            DB::beginTransaction();    
            

            $user=Auth::user();
            $checkinsettingdata=CheckInSetting::FindByUserId($user->id);
            $datatosave=$request->all();
            
            $datatosave['password']=isset($request->password) && !empty($request->password) ? Hash::make($request->password) : null;

            $followup_period_id=$request->followup_period_id;

            $followup_period_data=FollowupPeriod::find($followup_period_id);

            $stage1_date=date('Y-m-d',strtotime($followup_period_id." month"));
            $stage2_date=date('Y-m-d', strtotime($stage1_date. ' +1 days'));
            $datatosave['stage1_completed']=0;
            $datatosave['stage2_completed']=0;
            $datatosave['stage3_completed']=0;
            $datatosave['stage1_date']=$stage1_date;
            $datatosave['stage2_date']=$stage2_date;
            $datatosave['stage2_count']=0;
            $datatosave['stage3_date']=null;



            if(isset($checkinsettingdata) && !empty($checkinsettingdata)):


                $updated=$checkinsettingdata->update($datatosave);

                if($updated):
                    $request->ajax='true';
                    $request->successmessage='Your Wellbeing System preferences have been updated successfully';
                endif;
                
            else:

                $created=CheckInSetting::create($datatosave);

                if($created):
                    $request->ajax='true';
                    $request->successmessage='Your Wellbeing System preferences have been updated successfully';
                endif;

            endif;

            DB::commit();

            return $this->CheckInProcess($request);

        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

        $request->failuremessage='Oops! Something went wrong please try again';
        return $this->CheckInProcess($request);
       
    }


    public function GenerateInvitationlink($email,$user_id)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_encrypt($email.'--'.$user_id, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);

        $url = URL('invitation-authorised-person-request') . "?key=" . $output;

        return $url;
    }











    // CheckIn CRON JOBS



    public function BatchCreationEmailStageOne()
    {
        try
        {

            DB::beginTransaction();    

            $user=User::join('check_in_setting','check_in_setting.user_id','users.id')
            ->where('users.default_prefrence',1)
            ->where('users.isactive',1)
            ->where('users.user_role_type',2)
            ->where('check_in_setting.stage1_completed',0)
            ->where('check_in_setting.stage1_date',date('Y-m-d'))
            ->whereNotIn('users.id', DB::table('email_scheduler')->where('date',date('Y-m-d'))->pluck('user_id'))
             ->select('users.id','check_in_setting.check_in_setting_id')
            ->get();
           

            if(isset($user) && count($user)>0):

                foreach ($user as $userkey => $uservalue):


                    EmailScheduler::create([

                        'message_id'=>null,
                        'user_id'=>$uservalue->id,
                        'date'=>date('Y-m-d'),
                        'created_at'=>date('Y-m-d H:i:s'),
                        'dispatch_date'=>null,
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'stageno'=>1,
                        'status'=>1,
                        'check_in_setting_id'=>$uservalue->check_in_setting_id

                    ]);


                endforeach;


            endif;


            DB::commit();


        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

    }


    public function DespatchEmailStageOne()
    {
        Log::info(date('Y-m-d'));
        try
        {

            DB::beginTransaction();    

            $emailscheduler=EmailScheduler::join('users','users.id','email_scheduler.user_id')
            ->where('email_scheduler.date',date('Y-m-d'))
            ->whereNull('email_scheduler.dispatch_date')
            ->where('email_scheduler.status',1)
            ->where('email_scheduler.stageno',1)
            ->select('email_scheduler.*','users.email')
            ->take(100)
            ->get();
            

            if(isset($emailscheduler) && count($emailscheduler)>0):

                foreach ($emailscheduler as $ekey => $evalue):



                    $params = array(
                        'url' => url('/').'/user-checkin-status/confirmation/'.$evalue->email_scheduler_id,
                        'subject' => 'Check Email - Process'
                    );

                   $result=SendInBlueController::SendTransactionEmail((int) env('CHECKIN_EMAIL_TEMPLATE'),$evalue->email,$params);

                    if(isset($result['code']) && $result['code']==200):

                        $response=$result['response'];
                        $evalue->message_id=isset($response['messageId']) && !empty($response['messageId']) ? $response['messageId'] :null;
                        $evalue->dispatch_date=date('Y-m-d H:i:s');
                        $evalue->save();

                        $checkinsetting=CheckInSetting::find($evalue->check_in_setting_id);

                        if(isset($checkinsetting) && !empty($checkinsetting)):

                            $checkinsetting->stage1_completed=1;
                            $checkinsetting->save();

                        endif;

                    endif;
                   


                endforeach;


            endif;


            DB::commit();


        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

    }



    public function BatchCreationEmailStageTwo()
    {
        try
        {

            DB::beginTransaction();    

            $user=User::join('check_in_setting','check_in_setting.user_id','users.id')
            ->join('failed_response_action','check_in_setting.failed_response_action_id','failed_response_action.failed_response_action_id')
            ->where('users.default_prefrence',1)
            ->where('users.isactive',1)
            ->where('users.user_role_type',2)
            ->where('check_in_setting.stage1_completed',1)
            ->where('check_in_setting.stage2_completed',0)
            ->where('check_in_setting.stage2_date',date('Y-m-d'))
            ->where('check_in_setting.stage2_count','<',4)
            ->whereNotIn('users.id', DB::table('email_scheduler')->where('date',date('Y-m-d'))->pluck('user_id'))
            ->select('users.id','check_in_setting.check_in_setting_id','failed_response_action.after_fail_message_count')
            ->get();
           
           
            if(isset($user) && count($user)>0):

                foreach ($user as $userkey => $uservalue):


                    EmailScheduler::create([

                        'message_id'=>null,
                        'user_id'=>$uservalue->id,
                        'date'=>date('Y-m-d'),
                        'created_at'=>date('Y-m-d H:i:s'),
                        'dispatch_date'=>null,
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'stageno'=>2,
                        'status'=>1,
                        'check_in_setting_id'=>$uservalue->check_in_setting_id

                    ]);


                endforeach;


            endif;


            DB::commit();


        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

    }


    public function DespatchEmailStageTwo()
    {
        try
        {

            DB::beginTransaction();    

            $emailscheduler=EmailScheduler::join('users','users.id','email_scheduler.user_id')
            ->where('email_scheduler.date',date('Y-m-d'))
            ->whereNull('email_scheduler.dispatch_date')
            ->where('email_scheduler.status',1)
            ->where('email_scheduler.stageno',2)
            ->select('email_scheduler.*','users.email')
            ->take(100)
            ->get();
           
            
            if(isset($emailscheduler) && count($emailscheduler)>0):

                foreach ($emailscheduler as $ekey => $evalue):


                    $params = array(
                        'url' => url('/').'/user-checkin-status/confirmation/'.$evalue->email_scheduler_id,
                        'subject' => 'Check Email - Process'
                    );

                   $result=SendInBlueController::SendTransactionEmail((int) env('CHECKIN_EMAIL_TEMPLATE'),$evalue->email,$params);


                   if(isset($result['code']) && $result['code']==200):

                        $response=$result['response'];
                        
                        $evalue->message_id=isset($response['messageId']) && !empty($response['messageId']) ? $response['messageId'] :null;
                        $evalue->dispatch_date=date('Y-m-d H:i:s');
                        $evalue->save();

                        $checkinsetting=CheckInSetting::find($evalue->check_in_setting_id);
                        
                        if(isset($checkinsetting) && !empty($checkinsetting)):

                            $FailedResponseAction=FailedResponseAction::find($checkinsetting->failed_response_action_id);
                            $after_fail_message_count=$FailedResponseAction->after_fail_message_count;
                            $failed_days_apart=$FailedResponseAction->failed_days_apart;
                            $failed_addtional_waiting_period=$FailedResponseAction->failed_addtional_waiting_period;

                            $checkinsetting->stage2_count=$checkinsetting->stage2_count+1;
                             if($checkinsetting->stage2_count==$after_fail_message_count):

                                $waitingperiod=WatingPeriod::find($checkinsetting->wating_period_no_of_days);

                                $checkinsetting->stage3_date=date('Y-m-d', strtotime($checkinsetting->stage2_date.' '.$waitingperiod->slug));

                                $checkinsetting->stage2_completed=1;


                            else:

                                $checkinsetting->stage2_date=date('Y-m-d', strtotime($checkinsetting->stage2_date.' +'.$failed_days_apart.' days'));
                               
                                $checkinsetting->stage2_completed=0;

                            endif;


                            $checkinsetting->save();

                            $userdata=User::find($evalue->user_id);
                            $userdata->usercheckinstatus=1;
                            $userdata->save();

                        endif;

                    endif;

                endforeach;


            endif;


            DB::commit();


        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

    }


    public function BatchCreationEmailStageThree()
    {
        try
        {

            DB::beginTransaction();    

            $user=User::join('check_in_setting','check_in_setting.user_id','users.id')
            ->join('failed_response_action','check_in_setting.failed_response_action_id','failed_response_action.failed_response_action_id')
            ->where('users.default_prefrence',1)
            ->where('users.isactive',1)
            ->where('users.user_role_type',2)
            ->where('check_in_setting.stage3_completed',0)
            ->where('users.usercheckinstatus',1)
            ->where('check_in_setting.stage3_date',date('Y-m-d'))
            ->whereNotIn('users.id', DB::table('email_scheduler')->where('date',date('Y-m-d'))->pluck('user_id'))
            ->select('users.id','check_in_setting.check_in_setting_id','failed_response_action.after_fail_message_count')
            ->get();
           
           
            if(isset($user) && count($user)>0):

                foreach ($user as $userkey => $uservalue):


                    EmailScheduler::create([

                        'message_id'=>null,
                        'user_id'=>$uservalue->id,
                        'date'=>date('Y-m-d'),
                        'created_at'=>date('Y-m-d H:i:s'),
                        'dispatch_date'=>null,
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'stageno'=>3,
                        'status'=>1,
                        'check_in_setting_id'=>$uservalue->check_in_setting_id

                    ]);


                endforeach;


            endif;


            DB::commit();


        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

    }


    public function BatchCreationEmailThirdPerson()
    {
        try
        {

            DB::beginTransaction();    

            $user=User::where('users.usercheckinstatus',1)
            ->where('users.default_prefrence',2)
            ->where('users.isactive',1)
            ->where('users.user_role_type',2)
            ->whereNotNull('users.passed_date')
            ->whereNotIn('users.id',DB::table('email_scheduler')->where('date',date('Y-m-d'))->pluck('user_id'))
            ->select('users.id')
            ->get();

            if(isset($user) && count($user)>0):

                foreach ($user as $userkey => $uservalue):


                    EmailScheduler::create([

                        'message_id'=>null,
                        'user_id'=>$uservalue->id,
                        'date'=>date('Y-m-d'),
                        'created_at'=>date('Y-m-d H:i:s'),
                        'dispatch_date'=>null,
                        'updated_at'=>date('Y-m-d H:i:s'),
                        'stageno'=>3,
                        'status'=>1
                        

                    ]);


                endforeach;


            endif;


            DB::commit();


        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

    }
    

    public function ProcessEmailStageThree()
    {
        try
        {

            DB::beginTransaction();    

            $emailscheduler=EmailScheduler::join('users','users.id','email_scheduler.user_id')
            ->where('email_scheduler.date',date('Y-m-d'))
            ->whereNull('email_scheduler.dispatch_date')
            ->where('email_scheduler.status',1)
            ->where('email_scheduler.stageno',3)
            ->where('users.usercheckinstatus',1)
            ->select('email_scheduler.*','users.email')
            ->take(10)
            ->get();
           
            if(isset($emailscheduler) && count($emailscheduler)>0):

                foreach ($emailscheduler as $ekey => $evalue):


                    $messages=ZiggeoUserAudios::ToBeDeliveredMessages($evalue->user_id);

                    
                    
                    $user_recipient_id=null;
                    $user_authorised_person_id=null;
                    $thirdperson_url=null;
                    $recipient_url=null;
                   if(isset($messages) && count($messages)>0):

                   foreach ($messages as $mkey => $mvalue):
                    $deliverdatecondition=false;

                    if(isset($mvalue['delivery_date']) && !empty($mvalue['delivery_date'])): 

                            if(strtotime($mvalue['delivery_date'])==strtotime(date('Y-m-d'))):

                            $deliverdatecondition=true;

                            endif;

                        else:

                            $deliverdatecondition=true;

                       endif;

                    if($mvalue['default_prefrence']==2):

                       if($mvalue['thirdpersoninvitation_status']=='1' && $mvalue['step2confirmed']==1 && $mvalue['step2confirmed']==1):


                            if($mvalue['authorisation_type']=='2'):

                            $thirdpersonencodedcode=$this->Encrypt('thirdperson-/-'.$mvalue['mtype']."-/-".$mvalue['message_id']."-/-".$mvalue['user_authorised_person_id']);

                            $thirdperson_url= url('/').'/message-verification/'.$thirdpersonencodedcode;

                            $user_authorised_person_id=$mvalue['user_authorised_person_id'];

                            endif;



                            $recipientencodedcode=$this->Encrypt('recipient-/-'.$mvalue['mtype']."-/-".$mvalue['message_id']."-/-".$mvalue['user_recipient_id']);

                            

                            $recipient_url= url('/').'/message-verification/'.$recipientencodedcode;

                           
                            $user_recipient_id=$mvalue['user_recipient_id'];

                            

                       endif;

                        

                    endif;

                    if($mvalue['default_prefrence']==1):  

                        $recipientencodedcode=$this->Encrypt('recipient-/-'.$mvalue['mtype']."-/-".$mvalue['message_id']."-/-".$mvalue['user_recipient_id']);

                            $recipient_url= url('/').'/message-verification/'.$recipientencodedcode;

                            $user_authorised_person_id=null;
                            $user_recipient_id=$mvalue['user_recipient_id'];

                        
                    
                    endif;
                       

                    if(isset($user_recipient_id) && !empty($user_recipient_id) && $deliverdatecondition):

                        MessageScheduler::create([

                            'message_type'=>$mvalue['mtype'],
                            'authorisation_type'=>$mvalue['authorisation_type'],
                            'user_recipient_id'=>$mvalue['user_recipient_id'],
                            'message_token_id'=>$mvalue['token'],
                            'user_id'=>$evalue->user_id,
                            'date'=>$evalue->date,
                            'created_at'=>Carbon::now(),
                            'dispatch_date'=>null,
                            'updated_at'=>Carbon::now(),
                            'status'=>1,
                            'email_scheduler_id'=>$evalue->email_scheduler_id,
                            'check_in_setting_id'=>$evalue->check_in_setting_id,
                            'delivery_type'=>$mvalue['delivery_type'],
                            'recipient_url'=>$recipient_url,
                            'thirdperson_url'=>$thirdperson_url,
                            'user_authorised_person_id'=>$user_authorised_person_id,
                            'qrid'=>$mvalue['qrid'],
                            'message_id'=>$mvalue['message_id']

                        ]);

                    endif;


                    endforeach;

                    endif;


                    $evalue->isprocessed=1;
                    $evalue->save();
                    

                    

                endforeach;


            endif;


            DB::commit();


        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

    }


    public function DespatchFinalEmailMessages()
    {
        try
        {

            

            $messagescheduler=MessageScheduler::join('users','users.id','message_scheduler.user_id')
            ->leftjoin('user_authorised_person','message_scheduler.user_authorised_person_id','user_authorised_person.user_authorised_person_id')
            ->join('user_recipients','message_scheduler.user_recipient_id','user_recipients.user_recipient_id')
            ->where('message_scheduler.date',date('Y-m-d'))
            ->whereNull('message_scheduler.dispatch_date')
            ->where('message_scheduler.status',1)
            ->where('users.usercheckinstatus',1)
            ->select('message_scheduler.*','users.email','user_recipients.email as recipientemail','user_authorised_person.email as thirdpersonemail')
            ->take(100)
            ->orderBy('message_scheduler_id','Asc')
            ->get();
            
            if(isset($messagescheduler) && count($messagescheduler)>0):

                foreach ($messagescheduler as $ekey => $mvalue):




                    if($mvalue->message_type=='1'):
                        $messagedata=ZiggeoUserVideos::find($mvalue->message_id);
                    endif;

                    if($mvalue->message_type=='2'):
                        $messagedata=ZiggeoUserAudios::find($mvalue->message_id);
                    endif;

                    if($mvalue->message_type=='3'):
                        $messagedata=ZiggeoUserTexts::find($mvalue->message_id);
                    endif;

                    

                    if(isset($mvalue->thirdpersonemail) && !empty($mvalue->thirdpersonemail)):
                        $params = array(
                            'url' =>$mvalue->thirdperson_url,
                            'subject' => 'Message Evalheld'
                        );
                       
                         $thirdpersonresult=SendInBlueController::SendTransactionEmail(31,$mvalue->thirdpersonemail,$params);

                    endif;


                    if(isset($mvalue->recipientemail) && !empty($mvalue->recipientemail)):
                        $params = array(
                            'url' =>$mvalue->recipient_url,
                            'subject' => 'Message Evalheld'
                        );
                   
                         $result=SendInBlueController::SendTransactionEmail(31,$mvalue->recipientemail,$params);

                        DB::beginTransaction();    
                         
                        if(isset($result['code']) && $result['code']==200):

                        $response=$result['response'];
                        $mvalue->sendinblue_message_id=isset($response['messageId']) && !empty($response['messageId']) ? $response['messageId'] :null;
                       
                        $mvalue->dispatch_date=date('Y-m-d H:i:s');
                        $mvalue->save();

                        $messagedata->is_sent=1;
                        $messagedata->save();
                        

                        endif;

                        DB::commit();
                        
                    endif;

                    
                   

                    
                   


                endforeach;


            endif;


            


        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

    }

    
    public function NotifyUserStatusVerification(Request $request,$code,$otp=null)
    {
                $key = $this->Decrypt($code);
                $keyexplode=explode('--',$key);

                $user_id=isset($keyexplode[3]) && !empty($keyexplode[3]) ? $keyexplode[3] :'';

                $eligible = UserAuthorizedPerson::join('users','users.id','user_authorised_person.user_id')->where('user_authorised_person.user_id',$user_id)->select('user_authorised_person.*','users.first_name as ownerfname','users.last_name as ownerlname','users.passed_date','users.id as user_id')->first();
                    $isajax = $request->ajax;
                    $data['isajax'] = $isajax;
                    $data['success'] = false;
                    $data['old'] = $request;
                    $data['userdata'] = $eligible;
                    $data['code']=$code;
                    $data['otp']=$otp;
                    $data['redirecturl']=url('/').'/notify-user-status-verification/'.$code.'/'.$otp;
                     $data['documents']=ClientsDocuments::where('user_id',$user_id)
                    ->where('capture_type','uploaded')
                    ->get();
                    $data['showform']=($eligible->message_access_code==$otp) && strtotime($eligible->code_expiry_at) || ($eligible->verification_process_step_id==3)  > strtotime(date('Y-m-d H:i:s')) ? true : false;

                if(isset($eligible) && !empty($eligible)):

                    if ($isajax == 'true') :

                    $html = view('my-preferences.notify-user-status-verification', $data)->render();

                    return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);

                    else :

                    return view('my-preferences.notify-user-status-verification', $data);

                    endif;

                endif;
    }

    public function NotifyUserStatusVerificationPost(Request $request,$code)
    {
                $key = $this->Decrypt($code);
                $keyexplode=explode('--',$key);

                $user_id=isset($keyexplode[3]) && !empty($keyexplode[3]) ? $keyexplode[3] :'';

                $authoriseddata = UserAuthorizedPerson::join('users','users.id','user_authorised_person.user_id')->where('user_authorised_person.user_id',$user_id)->select('user_authorised_person.*','users.first_name as ownerfname','users.last_name as ownerlname')->first();

                   
                if(isset($authoriseddata) && !empty($authoriseddata)):

                    $verification_process_step_id=$authoriseddata->verification_process_step_id;
                    $verification_answer_1=$authoriseddata->verification_answer_1;
                    $verification_answer_2=$authoriseddata->verification_answer_2;
                    $verification_answer_3=$authoriseddata->verification_answer_3;
                    $password=$authoriseddata->password;

                    if($verification_process_step_id==1):

                        if($request->password===$password):

                            
                    
                            $verificationtoken=uniqid();
                            $authoriseddata->message_access_code=$verificationtoken;
                            $authoriseddata->code_expiry_at=date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' + 3 hours'));
                            $authoriseddata->save();
                            Session::put('verificationtoken', $verificationtoken);
                            
                            return  \Redirect::to(url('/').'/notify-user-status-verification/'.$code.'/'.$verificationtoken); 

                        endif;

                    endif;


                    if($verification_process_step_id==2):
                        if($request->verification_answer_1===$verification_answer_1 && $request->verification_answer_2===$verification_answer_2 && $request->verification_answer_3===$verification_answer_3):

                           
                            $verificationtoken=uniqid();
                            $authoriseddata->message_access_code=$verificationtoken;
                            $authoriseddata->code_expiry_at=date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').' + 3 hours'));
                            $authoriseddata->save();
                            Session::put('verificationtoken', $verificationtoken);
                            
                            return  \Redirect::to(url('/').'/notify-user-status-verification/'.$code.'/'.$verificationtoken);

                       endif;
                    endif;

                    

                   
                endif;

                return  \Redirect::to(url('/').'/notify-user-status-verification/'.$code);

    }

    public function UserUpdateStatus(Request $request,$id,$otp=null)
    {
        
        $user=Auth::user();
        $isajax=$request->ajax;
        $code=$request->code;
        

        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['error'] = $request->error;
        $acknowledge = $request->acknowledge;
        $passed_date = $request->passed_date;
        $data['acknowledge'] = $acknowledge;
        $userdata=User::find($id);
        $userdata->passed_date=date('Y-m-d',strtotime($passed_date));
        $userdata->save();


        $params = array(
                    'url' => url('/').'/notify-user-status-verification/'.$code,
                     'subject' => 'THIRD PERSON-NOTICE ADVICE OF DEATH TEMPLATE',
                     'FIRSTNAME'=>$userdata->first_name,
                     'LASTNAME'=>$userdata->last_name
                 );


        SendInBlueController::SendTransactionEmail((int) env('NOTICE_ADVICE_OF_DEATH_TEMPLATE'),'rajesmoury67@gmail.com',$params);

        $request = new \Illuminate\Http\Request();
        $request->ajax='true';
        $request->successmessage='Client Status Updates Successfully';
        return $this->NotifyUserStatusVerification($request,$code,$otp);

      }

    public function UserUploadFiles(Request $request,$id)
    {

        try
        {   
            $user=Auth::user();

            $validator = Validator::make($request->all(), [

            'file' => 'required',
            'type' => 'required'
            
            ]);


            if ($request->hasFile('file')) {

                $document = $request->file('file');
                $fileInfo = $document->getClientOriginalName();
                $filename = pathinfo($fileInfo, PATHINFO_FILENAME);
                $extension = pathinfo($fileInfo, PATHINFO_EXTENSION);
                $file_name= $filename.'-'.time().'.'.$extension;
                Storage::disk('public')->put($file_name,File::get($document));


                if(Storage::disk('public')->exists($file_name)) {  
                    $path = Storage::disk('public')->path($file_name);

                    $allowedMimeTypes = ['image/jpeg','image/gif','image/png','image/jpg'];
                    $contentType = $document->getClientMimeType();

                    if(! in_array($contentType, $allowedMimeTypes) ){
                      
                      $filetype='pdf';
                    }
                    else
                    {
                      $filetype='image';
                    }

                    $updated=ClientsDocuments::create([

                        'title'=>$filename,
                        'user_id'=>$id,
                        'capture_type'=>'uploaded',
                        'path'=>$path,
                        'uploadedfilename'=>$file_name,
                        'created_at'=>Carbon::now(),
                        'created_by'=>$user->id,
                        'updated_at'=>Carbon::now(),
                        'updated_by'=>$user->id,
                        'filetype'=>$filetype
                    ]);

                    return array('status'=>'success','data'=>$updated);
                    

                
                            
                }

            }


             


        } catch (TrawableException $th) {
            DB::rollback();
           
        } catch (QueryException $qe) {
            DB::rollback();
           
        }

    }

    public function UserDeleteFiles(Request $request,$id)
    {
        $user=Auth::user();
        $filename =  $request->get('filename');
        
        $pdffiles = ClientsDocuments::where('user_id',$id)
        ->where('uploadedfilename',$filename)
        ->where('capture_type','uploaded')
        ->first();

        if(isset($pdffiles) && !empty($pdffiles)):

           $pdffiles->delete();
           unlink($pdffiles->path);

        endif;

        
    } 

    public function UserStreamFiles(Request $request,$id,$filename)
    {
        $document = ClientsDocuments::where('user_id',$id)
        ->where('uploadedfilename',$filename)
        ->where('capture_type','uploaded')
        ->first();
        $data['textdata'] = $document;
        return response()->file($document->path);


       
    }

    public function GenerateVerificationlink($email)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_encrypt($email, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);

        $url = URL('verify-account') . "?key=" . $output;

        return $url;
    }

    public function Encrypt($text)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_encrypt($text, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }


    public function Decrypt($text)
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        $output = openssl_decrypt(base64_decode($text), $encrypt_method, $key, 0, $iv);
        return $output;
    }






}