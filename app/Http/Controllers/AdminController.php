<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Http\Controllers\SendInBlueController;
use App\Http\Controllers\ZiggeoController;
use App\Models\UserAuthorizedPerson;
use App\Models\ZiggeoUserVideos;
use App\Models\ZiggeoUserAudios;
use App\Models\ZiggeoUserTexts;
use App\Models\MyRecipients;
use App\Models\UserAddonPackages;
use App\Models\UserStandAlonePackages;
use Storage;
use File;
use Ziggeo;
use Log;

class AdminController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
        
    }



    public function Home(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type :'all-content';
        $data['contenttype'] = $contenttype;
        if ($isajax == 'true'):

            $html = view('admin.index', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('admin.index').$contenttype]);

        else:

            return view('admin.index', $data);

        endif;
    }

    public function B2Bclients(Request $request){
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type :'all-content';
        $data['contenttype'] = $contenttype;
        if ($isajax == 'true'):

            $html = view('admin.b2b-client', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('admin.b2b-client').$contenttype]);

        else:

            return view('admin.b2b-client', $data);

        endif;
    }

    public function EndUsers(Request $request){
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type :'all-content';
        $data['contenttype'] = $contenttype;
        if ($isajax == 'true'):

            $html = view('admin.b2c-user-clients', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('admin.b2c-user-clients').$contenttype]);

        else:

            return view('admin.b2c-user-clients', $data);

        endif;
    }


    public function Partners(Request $request){
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        $contenttype=isset($request->type) && !empty($request->type) ? $request->type :'all-content';
        $data['contenttype'] = $contenttype;
        if ($isajax == 'true'):

            $html = view('admin.b2c-partner', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('admin.b2c-partner').$contenttype]);

        else:

            return view('admin.b2c-partner', $data);

        endif;
    }


    

}