<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use SendinBlue;
use GuzzleHttp;
use App\Models\User;
use App\Models\CheckoutSessionData;
use App\Models\UserSubscriptionPackages;
use App\Models\Packages;
use App\Models\UserPackages;
use Carbon\Carbon;
use DB;
use Hash;
use Session;
use App\Http\Controllers\SendInBlueController;
use Auth;
use \Stripe\Stripe;
use Redirect;

class SubscribePackagesController extends Controller
{
    

	public function index(Request $request)
	{
       
		$isajax= $request->ajax;
		$data['isajax']=$isajax;
		$userdata=Auth::user();
		$data['packages']=Packages::GetActive();
		
		// $data['qrpackages']=Packages::where('slug','standalone-qr')->where('isactive',1)->get();
		// $data['heirloompackages']=Packages::where('slug','standalone-heirloom')->where('isactive',1)->get();
		// -----------------JK------------------------------------
		$data['subscriptionpackages'] = Packages::getSubscriptionPackages();
		$data['community_packages_coming_soon'] = Packages::getAllComingSoonPackageData();
		$data['qrpackages'] = Packages::getQRPackages();
		$data['heirloompackages'] = Packages::getHeirLoomPackages();

		// -----------------JK------------------------------------


		$data['old']=$request;
		// dd($data);



		if($userdata->haspackage<=0 && $userdata->hasRole(['B2B Client','Client']) || true):

			if($isajax=='true'):

				// $html = view('packages.subscription-page-new',$data)->render();
				 $html = view('packages.subscription-packages-jk',$data)->render();

				return response()->json(['status'=>'success','event'=>'refresh','html'=>$html]);

			else:
				// return view('packages.subscription-page-new',$data);
				return view('packages.subscription-packages-jk',$data);

			endif;

		endif;

		return redirect()->route('dashboard');

	}


	public function GetPurchaseSubscription(Request $request,$id)
	{


		$request->validate([
			'package_id' => 'required',
			'purchasesubscription'=>"required|in:freetrial,notrial"
		],
		[
			'purchasesubscription.in' => 'please select valid subscription to proceed'
		]);

		try {

			$isajax = $request->ajax;
			$isanually= $request->isanually;

			$data['isajax'] = $isajax;
			$packages=Packages::find($id);
            
			$istrial=0;
			if($request->purchasesubscription=='freetrial'):
				$istrial=1;
			endif;	

			if(isset($packages) && !empty($packages)):

			$userdata=Auth::user();
			$usersubscription = UserPackages::GetActivePlan($userdata->id);
			$packageprice=$packages->price;
			$packagepriceid=$packages->stripe_price_id;
			
			if($packages->demopackage!=='1'):

			$sessionarray=array();
			$sessionarray['line_items']=array();
			$sessionarray['success_url']=route('dashboard').'?session_id={CHECKOUT_SESSION_ID}&package='.$packages->package_id.'&istrial='.$istrial;
			$sessionarray['cancel_url']=route('stripe-cancel-page').'?session_id={CHECKOUT_SESSION_ID}&package='.$packages->package_id.'&istrial='.$istrial;
			$sessionarray['payment_method_types']=['card'];

			$sessionarray['mode']='payment';
			
			
			$sessionarray['allow_promotion_codes']=true;
			$line_items=array();

			$line_items['price']=$packages->stripe_price_id;
			
			if($request->upgradepackage=='true'):

				$upgradepackagedata=DB::table('package_upgradable')
				->where('from_price_id',$usersubscription->stripe_price_id)
				->where('to_package_id',$packages->package_id)
				->first();

				
				if(isset($upgradepackagedata) && !empty($upgradepackagedata)):
				$line_items['price']=$upgradepackagedata->upgrade_price_id;

				$packageprice=$upgradepackagedata->price;
				$packagepriceid=$upgradepackagedata->upgrade_price_id;
				endif;
			endif;
			
			$line_items['quantity']=1;

			if($userdata->user_role_type!==2):

				$line_items['adjustable_quantity']=array(
					'enabled' => true,
					'minimum' => 1,
					'maximum' => 10,
				);

			endif;

			// if($request->purchasesubscription=='freetrial'):
			// 	$sessionarray['subscription_data']=array('trial_end' => strtotime("+15 day",strtotime(date('Y-m-d'))));
			// endif;

			

			Stripe::setApiKey(env('API_KEY_STRIPE'));

			$customerId=$userdata->stripe_customer_id;

			if(empty($customerId) || is_null($customerId)):

					$customer = \Stripe\Customer::create([
						"description" => $userdata->first_name.' '.$userdata->last_name,
						"email" => $userdata->email,
						"metadata" => [
							"first_name" => $userdata->first_name,
							"last_name" => $userdata->last_name,
							"aig_user_id" => $userdata->id,
							"phone_number" => $userdata->phone_number,
						],
					]);

				$customerId=$customer->id;

				DB::beginTransaction();
				$userdata->stripe_customer_id=$customerId;
				$userdata->save();
				DB::commit();

			endif;
			$sessionarray['line_items'][]=$line_items;
			$sessionarray['customer']=$customerId;

			Stripe::setApiKey(env('API_KEY_STRIPE'));
			$checkout_session = \Stripe\Checkout\Session::create($sessionarray);

			if(null!==$checkout_session['id'] && !empty($checkout_session['id'])):



					$data['checkout_session']=$checkout_session['id'];
					$data['pkkey']=env('PK_KEY_STRIPE');

					DB::beginTransaction();

					CheckoutSessionData::create([
						'checkout_session_id'=>$checkout_session['id'],
				        'package_id'=>$id,
				        'payment_status'=>$checkout_session['payment_status'],
				        'subscription'=>$checkout_session['subscription'],
				        'stripe_customer_id'=>$customerId,
				        'user_id'=>$userdata->id,
				        'expires_at'=>$checkout_session['expires_at'],
				        'status'=>$checkout_session['status'],
				        'lineitems'=>json_encode($line_items),
				        'created_at'=>Carbon::now(),
				        'created_by'=>$userdata->id,
				        'updated_at'=>Carbon::now(),
				        'updated_by'=>$userdata->id
					]);

					DB::commit();

					return view('packages.stripe-checkout',$data);

			endif;

			else:
				// DB::beginTransaction();
				// $userpackage=UserPackages::create([

				// 	'user_id'=>$userdata->id,
				// 	'package_id'=>$packages->package_id,
				// 	'package_start_date'=>date('Y-m-d H:i:s'),
    //                 'package_end_date'=>date('Y-m-d H:i:s', strtotime('+1 year')),
    //                 'status'=>1,
    //                 'message_value'=>$packages->message_value,
    //                 'recipient_value'=>$packages->recipient_value,
    //                 'written_value'=>$packages->written_value,
    //                 'stripe_price_id'=>$packages->stripe_price_id,
    //                 'stripe_product_id'=>$packages->stripe_product_id,
    //                 'stripe_subscription_id'=>null,
    //                 'created_at'=>Carbon::now(),
    //                 'updated_at'=>Carbon::now(),

				// ]);
				// $userdata->user_package_id=$userpackage->package_id;
				// $userdata->save();
				// DB::commit();
				return redirect()->route('dashboard');

			endif;

	endif;



	} catch (TrawableException $th) {
		DB::rollback();

	} catch (QueryException $qe) {
		DB::rollback();

	}

	return redirect()->route('subscription-packages');
	}


public function StripeSuccessPage(Request $request)
{	
	
	$session_id=$request->session_id;
	$package=$request->package;
	$istrial=$request->istrial;
	$checkoutsessiondata=CheckoutSessionData::where('checkout_session_id',$session_id)->first();
	if(isset($checkoutsessiondata) && !empty($checkoutsessiondata)):
	
	try
	{
		$stripe=Stripe::setApiKey(env('API_KEY_STRIPE'));
		$checkout_session = \Stripe\Checkout\Session::retrieve($session_id);
	}
	catch(Exception $e)
	{  
        $api_error = $e->getMessage();  
    } 


    if(empty($api_error) && $checkout_session):

    		$checkoutsessiondata->payment_status=$checkout_session['payment_status'];
    		$checkoutsessiondata->status=$checkout_session['status'];
    		$checkoutsessiondata->subscription=$checkout_session['subscription'];
    		$checkoutsessiondata->save();

    		if($checkout_session['payment_status']=='paid'):
    			$user=Auth::user();


    			if(isset($package) && !empty($package) && isset($istrial) && $istrial=='0' ):

    			if($package=='1')
    			{
    				$listid=77;
    			}

    			if($package=='2')
    			{
    				$listid=78;
    			}

    			if($package=='3')
    			{
    				$listid=79;
    			}
    			if(isset($listid)):
    				SendInBlueController::UpdateContacttolist($user->email,$listid);
    			endif;
    			 //Demo_users
    			endif;


    			if(isset($package) && !empty($package) && isset($istrial) &&  $istrial=='1' ):

    			if($package=='1')
    			{
    				$listid=74;
    			}

    			if($package=='2')
    			{
    				$listid=75;
    			}

    			if($package=='3')
    			{
    				$listid=76;
    			}

    			if(isset($listid)):
    				SendInBlueController::UpdateContacttolist($user->email,$listid);
    			endif; //Demo_users
    			endif;



    			
    		endif;

    endif;


	
	

	endif;
	$isajax= $request->ajax;
	$data['isajax']=$isajax;
	return view('packages.stripe-success',$data);
}

public function StripeCancelPage(Request $request)
{

	$isajax= $request->ajax;
	$package=$request->package;
	$data['isajax']=$isajax;
	$user=Auth::user();	

		if(isset($package) && !empty($package)):
			
			if($package=='1')
			{
				$listid=80;
			}

			if($package=='2')
			{
				$listid=81;
			}

			if($package=='3')
			{
				$listid=82;
			}

			if(isset($listid)):
    				SendInBlueController::UpdateContacttolist($user->email,$listid);
    		endif; //Demo_users

		endif;

	return view('packages.stripe-cancel',$data);

}


public function StripeFailPage(Request $request)
{

	$isajax= $request->ajax;
	$data['isajax']=$isajax;
	return view('packages.stripe-fail',$data);

}


public function DashboardPurchaseSubscription(Request $request,$id)
	{

		$request->validate([
			'package_id' => 'required',
			'purchasesubscription'=>"required|in:freetrial,notrial"
		],
		[
			'purchasesubscription.in' => 'please select valid subscription to proceed'
		]);
		try {
			
			$redirecturl=route('my-account.upgrade-plan');

			$redirecturl=isset($request->redirecturl) && !empty($request->redirecturl) ? $request->redirecturl : $redirecturl;

			$quantity=isset($request->quantity) && !empty($request->quantity) ? $request->quantity : 1;

			$isajax = $request->ajax;
			$isanually= $request->isanually;

			$data['isajax'] = $isajax;
			$packages=Packages::find($id);

			$istrial=0;

				

			if(isset($packages) && !empty($packages)):

			$userdata=Auth::user();
			$usersubscription = UserPackages::GetActivePlan($userdata->id);
			

			$sessionarray=array();
			$sessionarray['line_items']=array();


			if (strpos($redirecturl, '?')):

				$sessionarray['success_url']=$redirecturl.'&session_id={CHECKOUT_SESSION_ID}&package='.$packages->package_id.'&istrial='.$istrial;
				$sessionarray['cancel_url']=$redirecturl.'&session_id={CHECKOUT_SESSION_ID}&package='.$packages->package_id.'&istrial='.$istrial;

			else:

				$sessionarray['success_url']=$redirecturl.'?session_id={CHECKOUT_SESSION_ID}&package='.$packages->package_id.'&istrial='.$istrial;
				$sessionarray['cancel_url']=$redirecturl.'?session_id={CHECKOUT_SESSION_ID}&package='.$packages->package_id.'&istrial='.$istrial;

			endif;
			
			$sessionarray['payment_method_types']=['card'];

			if($packages->issubscription==1):
				$sessionarray['mode']='payment';
				$quantity=($userdata->user_role_type==2) ? 1 : $quantity;
			else:
				$sessionarray['mode']='payment';
				$sessionarray['metadata']=array('messagetype'=>$request->messagetype,'messageid'=>$request->messageid);
			endif;
			
			$sessionarray['allow_promotion_codes']=true;
			$line_items=array();

			$line_items['price']=$packages->stripe_price_id;

			if($userdata->user_role_type!==2):

				$line_items['adjustable_quantity']=array(
					'enabled' => true,
					'minimum' => 1,
					'maximum' => 10,
				);

			endif;

			if($request->upgradepackage=='true'):

				$upgradepackagedata=DB::table('package_upgradable')
				->where('from_price_id',$usersubscription->stripe_price_id)
				->where('to_package_id',$packages->package_id)
				->first();
				if(isset($upgradepackagedata) && !empty($upgradepackagedata)):
				$line_items['price']=$upgradepackagedata->upgrade_price_id;


				endif;
			endif;

			
			

			
			$line_items['quantity']=$quantity;

			if($userdata->user_role_type==3):

				$line_items['adjustable_quantity']=array(
					'enabled' => true,
					'minimum' => 1,
					'maximum' => 10,
				);

			endif;

			// if($request->purchasesubscription=='freetrial'):
			// 	$sessionarray['subscription_data']=array('trial_end' => strtotime("+15 day",strtotime(date('Y-m-d'))));
			// endif;

			

			Stripe::setApiKey(env('API_KEY_STRIPE'));

			$customerId=$userdata->stripe_customer_id;

			if(empty($customerId) || is_null($customerId)):

					$customer = \Stripe\Customer::create([
						"description" => $userdata->first_name.' '.$userdata->last_name,
						"email" => $userdata->email,
						"metadata" => [
							"first_name" => $userdata->first_name,
							"last_name" => $userdata->last_name,
							"aig_user_id" => $userdata->id,
							"phone_number" => $userdata->phone_number,
						],
					]);

				$customerId=$customer->id;

				DB::beginTransaction();
				$userdata->stripe_customer_id=$customerId;
				$userdata->save();
				DB::commit();

			endif;
			$sessionarray['line_items'][]=$line_items;
			$sessionarray['customer']=$customerId;
			Stripe::setApiKey(env('API_KEY_STRIPE'));
			
			$checkout_session = \Stripe\Checkout\Session::create($sessionarray);

			if(null!==$checkout_session['id'] && !empty($checkout_session['id'])):



					$data['checkout_session']=$checkout_session['id'];
					$data['pkkey']=env('PK_KEY_STRIPE');

					DB::beginTransaction();

					CheckoutSessionData::create([
						'checkout_session_id'=>$checkout_session['id'],
				        'package_id'=>$id,
				        'payment_status'=>$checkout_session['payment_status'],
				        'subscription'=>$checkout_session['subscription'],
				        'stripe_customer_id'=>$customerId,
				        'user_id'=>$userdata->id,
				        'expires_at'=>$checkout_session['expires_at'],
				        'status'=>$checkout_session['status'],
				        'lineitems'=>json_encode($line_items),
				        'created_at'=>Carbon::now(),
				        'created_by'=>$userdata->id,
				        'updated_at'=>Carbon::now(),
				        'updated_by'=>$userdata->id
					]);

					DB::commit();

				return view('packages.stripe-checkout',$data);

			endif;

	endif;



	} catch (TrawableException $th) {
		DB::rollback();

	} catch (QueryException $qe) {
		DB::rollback();

	}

	return redirect()->route('subscription-packages');
	}


	public function newScreens(Request $request){
		$isajax = $request->ajax;
        $data['isajax'] = $isajax;

        if ($isajax == 'true') :

            $html = view('subscription-page-new', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html]);

        else :
            return view('subscription-page-new', $data);
        endif;
	}

}
