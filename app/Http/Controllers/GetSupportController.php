<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use Auth;
use Validator;
use Carbon\Carbon;
use App\Http\Controllers\SendInBlueController;

class GetSupportController extends Controller
{
    
    function __construct()
    {
        $this->middleware('auth');
        
    }



    public function GetSupport(Request $request)
    {
        $user=Auth::user();
        $isajax=$request->ajax;
        $data['isajax'] = $isajax;
        $data['old'] = $request;
        $data['user_id'] = $user->id;
        $data['error'] = $request->error;
        
        if ($isajax == 'true'):

            $html = view('get-support', $data)->render();

            return response()->json(['status' => 'success', 'event' => 'refresh', 'html' => $html,'popstate'=>true,'routetoredirect'=>route('get-support')]);

        else:

            return view('get-support', $data);

        endif;
    }


     public function RequestSupport(Request $request)
    {
         $validator = Validator::make($request->all(), [
            'firstName' => ['required'],
            'lastName' => ['required'],
            'email' => ['required','email'],
            'message'=> ['required']
        ]);


        if ($validator->fails()) :
            
             $request->error = $validator->getMessageBag()->toArray();
            
             return $this->GetSupport($request);

        endif;


        $params = array(
                    'firstname' => $request->firstName,
                    'lastname' => $request->lastName,
                    'email' => $request->email,
                    'message' => $request->message,
                );
        $email=$request->email;

        SendInBlueController::SendSupportTransactionEmail(38,$email,$params,$request->firstName,$request->lastName);

        $request = new \Illuminate\Http\Request();
        $request->ajax='true';
        $request->successmessage='Thank you for your email we will contact you soon!';

        return $this->GetSupport($request);

    }
 


    


    

}